<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_User extends Model_Auth_User {
	/* Validation functions */
	public function getUserToken($user_id, $useragent){
 		$response = DB::select('*')->from('user_tokens')->where('user_agent', '=', $useragent)->where('user_id', '=', $user_id)->execute()->as_array();
 		return $response;
 	}

 	public function isCustomer($email) {

 	}

 	public function activate_user($values, $expected){
		$validation_results = Validation::factory($values);
		return $this->values($values, $expected)->update($validation_results);
	}

} // End User Model
