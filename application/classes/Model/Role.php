<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Role extends Model_Auth_Role {

	public function add_user_role($user, $role){
		return $user->add('roles', $this->where('name', '=', $role)->find());
	}

	public function delete_user_role($user_id){
		$response = DB::delete('roles_users')->where('user_id', '=', $user_id)->execute();
		return $response;
	}

} // End Role Model