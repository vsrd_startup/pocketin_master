<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Default auth role
 *
 * @package    Kohana/Auth
 * @author     Kohana Team
 * @copyright  (c) 2007-2009 Kohana Team
 * @license    http://kohanaphp.com/license.html
 */
class Model_Productitem extends ORM_Searchable {
    /**
     * Define the fields to index
     */
    public function get_indexable_fields()
    {
        $fields = array();

        // Store the product id but don't index it
        $fields[] = new Search_Field('id', Searchable::UNINDEXED);

        // Store the parent category ID but don't index it
        $fields[] = new Search_Field('productcategory_id', Searchable::KEYWORD);

        // Index the product name
        $fields[] = new Search_Field('title', Searchable::TEXT);

        // Index but don't store the product description
        $fields[] = new Search_Field('description', Searchable::UNSTORED, Searchable::DECODE_HTML);

        return $fields;

    }

    public function add_productitem($values, $expected){
        $validation_results = Validation::factory($values);
        $product = $this->values($values, $expected)->create($validation_results);

        return $product;
    }

    public function update_productitem($values, $expected = NULL)
    {
        $validation_results = Validation::factory($values);

        $product = $this->values($values, $expected)->update($validation_results);

        return $product;
    }

    public function getProductItemByQuality($productcategory_id, $offset, $limit, $sort, $filters = null){
        if(is_null($filters)){
            $response = DB::query(Database::SELECT, "SELECT products.name, products.modelnumber, products.description, products.origimage, productitems.mrp, productitems.technicalspec, productitems.conditionspec, productitems.longdescription, productitems.warranty, productitems.description as vendor_description, productitems.title as vendor_title, productitems.saleprice, productitems.manufacturingyear, productitems.warranty, productitems.vendorprice, products.id as product_id, productitems.id as productitem_id, images.name as image_full, images.thumbnail as image_thumb, COALESCE( ( SELECT AVG(value) FROM qualityratings WHERE qualityratings.productitem_id = productitems.id ), 0) AS average FROM `productitems` join products on products.id = productitems.product_id  left join images on images.productitem_id = productitems.id and images.isdefault = 1 where products.productcategory_id = ".$productcategory_id." order by average ".$sort." LIMIT ".$limit." OFFSET ".$offset."")->execute()->as_array();
        }
        else{
            $response = DB::query(Database::SELECT, "SELECT products.name, products.modelnumber, products.description, products.origimage, productitems.mrp, productitems.technicalspec, productitems.conditionspec, productitems.longdescription, productitems.warranty, productitems.description as vendor_description, productitems.title as vendor_title, productitems.saleprice, productitems.manufacturingyear, productitems.warranty, productitems.vendorprice, products.id as product_id, productitems.id as productitem_id, images.name as image_full, images.thumbnail as image_thumb, COALESCE( ( SELECT AVG(value) FROM qualityratings WHERE qualityratings.productitem_id = productitems.id ), 0) AS average FROM `productitems` join products on products.id = productitems.product_id join productitemfilters on productitemfilters.productitem_id = productitems.id left join images on images.productitem_id = productitems.id and images.isdefault = 1 where products.productcategory_id = ".$productcategory_id." and productitemfilters.filter_id in (".implode(",", $filters).") group by productitems.id order by average ".$sort." LIMIT ".$limit." OFFSET ".$offset."")->execute()->as_array();
        }

        return $response;
    }

    public function getProductByID($productitem_id) {
        /*$products = DB::select('products.name', 'products.description', 'products.modelnumber', 'products.instock', 'products.stockcount', 'products.origimage', 'productitems.mrp', array('productitems.title', 'product_title'), array('productitems.description', 'product_description'), array('productitems.id', 'productitem_id'), 'productitems.issold', 'productitems.isapproved', 'productitems.saleprice', 'productitems.manufacturingyear', 'productitems.warranty', 'productitems.vendorprice', 'productitems.ispaid','productitems.qualityindex','productitems.id',array('images.thumbnail', 'image_thumb'), array('images.name', 'image_full'), array('images.isdefault', 'defimage'))->from('productitems')->join('products')->on('products.id', '=', 'productitems.product_id')->join('images', 'left')->on('images.productitem_id', '=', 'productitems.id')->on('images.isdefault', '=', DB::expr('1'))->where('productitems.id', '=', $productitem_id)->execute()->as_array();*/
        $products = DB::select('products.name', 'products.description', 'products.modelnumber', 'products.instock', 'products.stockcount', 'products.origimage', 'productitems.mrp', 'productitems.technicalspec', 'productitems.conditionspec', 'productitems.isfactory','productitems.brandnew', 'productitems.longdescription', array('productitems.title', 'product_title'), array('productitems.productcategory_id', 'catid'), array('productitems.description', 'product_description'), array('productitems.id', 'productitem_id'), 'productitems.issold', 'productitems.hidden', 'productitems.isapproved', 'productitems.saleprice', 'productitems.manufacturingyear', 'productitems.warranty', 'productitems.vendorprice', 'productitems.ispaid','productitems.qualityindex', 'productitems.compressorwarranty',
        'productitems.closingoffer', 'productitems.id',  array('images.thumbnail', 'image_thumb'), array('images.name', 'image_full'), array('images.isdefault', 'defimage'))->from('productitems')->join('products')->on('products.id', '=', 'productitems.product_id')->join('images', 'left')->on('images.productitem_id', '=', 'productitems.id')->where("productitems.id", '=', $productitem_id)->execute()->as_array();
        if (empty($products)) {
            return $products;
        }

        $prod = array();
        $imagearray = array();
        $oldid = $products[0]['productitem_id'];
        $oldprod = $products[0];
        foreach ($products as $product) {
            if ($product['productitem_id'] == $oldid) {
                if ($product['defimage']) {
                    array_unshift($imagearray,array("image_full" => $product['image_full'], "image_thumb" => $product['image_thumb']));
                } else {
                    array_push($imagearray, array("image_full" => $product['image_full'], "image_thumb" => $product['image_thumb']));
                }
            }
            else {
                $p = $oldprod;
                unset($p['image_full']);
                unset($p['image_thumb']);
                $p['images'] = $imagearray;
                array_push($prod,$p);

                $oldprod = $product;
                $oldid = $product['productitem_id'];
                $imagearray = array();
                if ($product['defimage']) {
                    array_unshift($imagearray,array("image_full" => $product['image_full'], "image_thumb" => $product['image_thumb']));
                } else {
                    array_push($imagearray, array("image_full" => $product['image_full'], "image_thumb" => $product['image_thumb']));
                }
            }
        }

        // Get the last product
        $p = $oldprod;
        unset($p['image_full']);
        unset($p['image_thumb']);
        $p['images'] = $imagearray;
        array_push($prod,$p);
        $imagearray = array();
        return $prod;
    }

} // End Auth Role Model
