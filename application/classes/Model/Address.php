<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Default auth role
 *
 * @package    Kohana/Auth
 * @author     Kohana Team
 * @copyright  (c) 2007-2009 Kohana Team
 * @license    http://kohanaphp.com/license.html
 */
class Model_Address extends ORM {

	
	public function add_address($values, $expected){
		$validation_results = Validation::factory($values);
		return $this->values($values, $expected)->create($validation_results);
	}

	public function update_address($values, $expected = NULL)
	{
		$validation_results = Validation::factory($values);

		return $this->values($values, $expected)->update($validation_results);
	}

	public function delete_address($id){
		$response = DB::delete('Address')->where('id', '=', array($id))->execute();
		return $response;
	}
} // End Auth Role Model
