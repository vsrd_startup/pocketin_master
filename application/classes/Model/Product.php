<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Default auth role
 *
 * @package    Kohana/Auth
 * @author     Kohana Team
 * @copyright  (c) 2007-2009 Kohana Team
 * @license    http://kohanaphp.com/license.html
 */
class Model_Product extends ORM {

	
	public function add_product($values, $expected){
		$validation_results = Validation::factory($values);
		return $this->values($values, $expected)->create($validation_results);
	}

	public function update_product($values, $expected = NULL)
	{
		$validation_results = Validation::factory($values);

		return $this->values($values, $expected)->update($validation_results);
	}

	public function getProductsByDealer($dealer_id, $offset, $limit, $filters = null){
		if(is_null($filters)){
			$products = DB::select(array('productitems.title', 'product_title'), array('productitems.description', 'product_description'), array('productitems.id', 'productitem_id'), 'productitems.issold', 'productitems.isapproved', 'productitems.saleprice', 'productitems.manufacturingyear', 'productitems.warranty', 'productitems.vendorprice', 'productitems.ispaid', array('images.thumbnail', 'image_thumb'),  array('images.isdefault', 'defimage'), array('images.name', 'image_full'))->from('productitems')->join('images', 'left')->on('images.productitem_id', '=', 'productitems.id')->on('images.isdefault', '=', DB::expr('1'))->where('productitems.user_id', '=', $dealer_id)->order_by('productitems.id', 'DESC')->limit($limit)->offset($offset)->execute()->as_array();
		}
		if (empty($products)) {
			return $products;
		}

		// Convert image records to image array
		$prod = array();
		$imagearray = array();
		$oldid = $products[0]['productitem_id'];
		$oldprod = $products[0];
		foreach ($products as $product) {
			if ($product['productitem_id'] == $oldid) {
				if ($product['defimage']) {
					array_unshift($imagearray,array("image_full" => $product['image_full'], "image_thumb" => $product['image_thumb']));
				} else {
					array_push($imagearray, array("image_full" => $product['image_full'], "image_thumb" => $product['image_thumb']));
				}
			}
			else {
				$p = $oldprod;
				unset($p['image_full']);
				unset($p['image_thumb']);
				$p['images'] = $imagearray;
				array_push($prod,$p);

				$oldprod = $product;
				$oldid = $product['productitem_id'];
				$imagearray = array();
				if ($product['defimage']) {
					array_unshift($imagearray,array("image_full" => $product['image_full'], "image_thumb" => $product['image_thumb']));
				} else {
					array_push($imagearray, array("image_full" => $product['image_full'], "image_thumb" => $product['image_thumb']));
				}
			}
		}

		// Get the last product
		$p = $oldprod;
		unset($p['image_full']);
		unset($p['image_thumb']);
		$p['images'] = $imagearray;
		array_push($prod,$p);
		$imagearray = array();
		return $prod;
	}

	public function getPAProductsByDealer($dealer_id)
	{
		$products = DB::select(array('productitems.title', 'product_title'), array('productitems.description', 'product_description'), array('productitems.id', 'productitem_id'), 'productitems.issold', 'productitems.mrp', 'productitems.isapproved', 'productitems.saleprice', 'productitems.manufacturingyear', 'productitems.warranty', 'productitems.vendorprice', 'productitems.ispaid', array('images.thumbnail', 'image_thumb'), array('images.name', 'image_full'), array('images.isdefault', 'defimage'),  array('users.firstname', 'dealer_name'))->from('productitems')->join('users')->on('productitems.user_id', '=', 'users.id')->join('images', 'left')->on('images.productitem_id', '=', 'productitems.id')->where("productitems.isapproved", '=', 0)->where('productitems.user_id', '=', $dealer_id)->order_by('productitem_id', 'ASC')->execute()->as_array();

				if (empty($products)) {
					return $products;
				}

				// Convert image records to image array
				$prod = array();
				$imagearray = array();
				$oldid = $products[0]['productitem_id'];
				$oldprod = $products[0];
				foreach ($products as $product) {
					if ($product['productitem_id'] == $oldid) {
						if ($product['defimage']) {
							array_unshift($imagearray,array("image_full" => $product['image_full'], "image_thumb" => $product['image_thumb']));
						} else {
							array_push($imagearray, array("image_full" => $product['image_full'], "image_thumb" => $product['image_thumb']));
						}
					}
					else {
						$p = $oldprod;
						unset($p['image_full']);
						unset($p['image_thumb']);
						$p['images'] = $imagearray;
						array_push($prod,$p);

						$oldprod = $product;
						$oldid = $product['productitem_id'];
						$imagearray = array();
						if ($product['defimage']) {
							array_unshift($imagearray,array("image_full" => $product['image_full'], "image_thumb" => $product['image_thumb']));
						} else {
							array_push($imagearray, array("image_full" => $product['image_full'], "image_thumb" => $product['image_thumb']));
						}
					}
				}

				// Get the last product
				$p = $oldprod;
				unset($p['image_full']);
				unset($p['image_thumb']);
				$p['images'] = $imagearray;
				array_push($prod,$p);
				$imagearray = array();

				return $prod;
	}


	public function getPAProducts($offset, $limit, $filters = null){
		if(is_null($filters)){
			$products = DB::select(array('productitems.title', 'product_title'), array('productitems.description', 'product_description'), array('productitems.id', 'productitem_id'), 'productitems.issold', 'productitems.isapproved', 'productitems.saleprice', 'productitems.manufacturingyear', 'productitems.warranty', 'productitems.isfactory', 'productitems.brandnew', 'productitems.vendorprice', 'productitems.ispaid','productitems.mrp', 'productitems.longdescription', 'productitems.conditionspec', 'productitems.technicalspec', 'productitems.warehouseid', array('images.thumbnail', 'image_thumb'), array('images.name', 'image_full'), array('images.isdefault', 'defimage'),  array('users.firstname', 'dealer_name'))->from('productitems')->join('users')->on('productitems.user_id', '=', 'users.id')->join('images', 'left')->on('images.productitem_id', '=', 'productitems.id')->where("productitems.isapproved", '=', 0)->order_by('productitem_id', 'ASC')->limit($limit)->offset($offset)->execute()->as_array();

				if (empty($products)) {
					return $products;
				}

				// Convert image records to image array
				$prod = array();
				$imagearray = array();
				$oldid = $products[0]['productitem_id'];
				$oldprod = $products[0];
				foreach ($products as $product) {
					if ($product['productitem_id'] == $oldid) {
						if ($product['defimage']) {
							array_unshift($imagearray,array("image_full" => $product['image_full'], "image_thumb" => $product['image_thumb']));
						} else {
							array_push($imagearray, array("image_full" => $product['image_full'], "image_thumb" => $product['image_thumb']));
						}
					}
					else {
						$p = $oldprod;
						unset($p['image_full']);
						unset($p['image_thumb']);
						$p['images'] = $imagearray;
						array_push($prod,$p);

						$oldprod = $product;
						$oldid = $product['productitem_id'];
						$imagearray = array();
						if ($product['defimage']) {
							array_unshift($imagearray,array("image_full" => $product['image_full'], "image_thumb" => $product['image_thumb']));
						} else {
							array_push($imagearray, array("image_full" => $product['image_full'], "image_thumb" => $product['image_thumb']));
						}
					}
				}

				// Get the last product
				$p = $oldprod;
				unset($p['image_full']);
				unset($p['image_thumb']);
				$p['images'] = $imagearray;
				array_push($prod,$p);
				$imagearray = array();
				return $prod;
		}

		
	}

	public function getPDProducts($offset, $limit, $filters = null){
		if(is_null($filters)) {
				$products = DB::select('products.name', 'products.description', 'products.modelnumber', 'products.instock', 'products.stockcount', 'products.origimage', 'productitems.mrp','productitems.technicalspec', 'productitems.conditionspec', 'productitems.longdescription', array('productitems.title', 'product_title'), array('productitems.description', 'product_description'), array('productitems.id', 'productitem_id'), 'productitems.issold', 'productitems.isapproved', 'productitems.saleprice', 'productitems.manufacturingyear', 'productitems.warranty', 'productitems.vendorprice', 'productitems.ispaid', array('images.thumbnail', 'image_thumb'), array('images.isdefault', 'defimage'), array('images.name', 'image_full'), array('users.firstname', 'dealer_name'))->from('productitems')->join('products')->on('products.id', '=', 'productitems.product_id')->join('users')->on('productitems.user_id', '=', 'users.id')->join('images', 'left')->on('images.productitem_id', '=', 'productitems.id')->where("productitems.isapproved", '=', 1)->where('productitems.issold', '=', 1)->where('productitems.isdelivered', '=', 0)->order_by('productitems.timestamp', 'DESC')->limit($limit)->offset($offset)->execute()->as_array();
		if (empty($products)) {
					return $products;
				}

				// Convert image records to image array
				$prod = array();
				$imagearray = array();
				$oldid = $products[0]['productitem_id'];
				$oldprod = $products[0];
				foreach ($products as $product) {
					if ($product['productitem_id'] == $oldid) {
						if ($product['defimage']) {
							array_unshift($imagearray,array("image_full" => $product['image_full'], "image_thumb" => $product['image_thumb']));
						} else {
							array_push($imagearray, array("image_full" => $product['image_full'], "image_thumb" => $product['image_thumb']));
						}
					}
					else {
						$p = $oldprod;
						unset($p['image_full']);
						unset($p['image_thumb']);
						$p['images'] = $imagearray;
						array_push($prod,$p);

						$oldprod = $product;
						$oldid = $product['productitem_id'];
						$imagearray = array();
						if ($product['defimage']) {
							array_unshift($imagearray,array("image_full" => $product['image_full'], "image_thumb" => $product['image_thumb']));
						} else {
							array_push($imagearray, array("image_full" => $product['image_full'], "image_thumb" => $product['image_thumb']));
						}
					}
				}

				// Get the last product
				$p = $oldprod;
				unset($p['image_full']);
				unset($p['image_thumb']);
				$p['images'] = $imagearray;
				array_push($prod,$p);
				$imagearray = array();

		return $prod;
		}

	}

	public function getPPProducts($offset, $limit, $filters = null){
		if(is_null($filters)){
			$products = DB::select('products.name', 'products.description', 'products.modelnumber', 'products.instock', 'products.stockcount', 'products.origimage', 'productitems.mrp','productitems.technicalspec', 'productitems.conditionspec', 'productitems.longdescription', array('productitems.title', 'product_title'), array('productitems.description', 'product_description'), array('productitems.id', 'productitem_id'), 'productitems.issold', 'productitems.isapproved', 'productitems.saleprice', 'productitems.manufacturingyear', 'productitems.warranty', 'productitems.vendorprice', 'productitems.ispaid', array('images.thumbnail', 'image_thumb'), array('images.name', 'image_full'), array('images.isdefault', 'defimage'),  array('users.firstname', 'dealer_name'))->from('productitems')->join('products')->on('products.id', '=', 'productitems.product_id')->join('users')->on('productitems.user_id', '=', 'users.id')->join('images', 'left')->on('images.productitem_id', '=', 'productitems.id')->where("productitems.issold", '=', 1)->where("productitems.ispaid", '=', 0)->order_by('productitems.timestamp', 'DESC')->limit($limit)->offset($offset)->execute()->as_array();
		if (empty($products)) {
					return $products;
				}

				// Convert image records to image array
				$prod = array();
				$imagearray = array();
				$oldid = $products[0]['productitem_id'];
				$oldprod = $products[0];
				foreach ($products as $product) {
					if ($product['productitem_id'] == $oldid) {
						if ($product['defimage']) {
							array_unshift($imagearray,array("image_full" => $product['image_full'], "image_thumb" => $product['image_thumb']));
						} else {
							array_push($imagearray, array("image_full" => $product['image_full'], "image_thumb" => $product['image_thumb']));
						}
					}
					else {
						$p = $oldprod;
						unset($p['image_full']);
						unset($p['image_thumb']);
						$p['images'] = $imagearray;
						array_push($prod,$p);

						$oldprod = $product;
						$oldid = $product['productitem_id'];
						$imagearray = array();
						if ($product['defimage']) {
							array_unshift($imagearray,array("image_full" => $product['image_full'], "image_thumb" => $product['image_thumb']));
						} else {
							array_push($imagearray, array("image_full" => $product['image_full'], "image_thumb" => $product['image_thumb']));
						}
					}
				}

				// Get the last product
				$p = $oldprod;
				unset($p['image_full']);
				unset($p['image_thumb']);
				$p['images'] = $imagearray;
				array_push($prod,$p);
				$imagearray = array();
		return $prod;
		}

	}

	public function getSPProducts($offset, $limit, $filters = null){
		if(is_null($filters)){
			$products = DB::select('products.name', 'products.description', 'products.modelnumber', 'products.instock', 'products.stockcount', 'products.origimage', 'productitems.mrp','productitems.technicalspec', 'productitems.conditionspec', 'productitems.longdescription', array('productitems.title', 'product_title'), array('productitems.description', 'product_description'), array('productitems.id', 'productitem_id'), 'productitems.issold', 'productitems.isapproved', 'productitems.saleprice', 'productitems.manufacturingyear', 'productitems.warranty', 'productitems.vendorprice', 'productitems.ispaid', array('images.thumbnail', 'image_thumb'), array('images.name', 'image_full'), array('images.isdefault', 'defimage'), array('users.firstname', 'dealer_name'))->from('productitems')->join('products')->on('products.id', '=', 'productitems.product_id')->join('users')->on('productitems.user_id', '=', 'users.id')->join('images', 'left')->on('images.productitem_id', '=', 'productitems.id')->where("productitems.issold", '=', 1)->where("productitems.ispaid", '=', 1)->order_by('productitems.timestamp', 'DESC')->limit($limit)->offset($offset)->execute()->as_array();
		if (empty($products)) {
					return $products;
				}

				// Convert image records to image array
				$prod = array();
				$imagearray = array();
				$oldid = $products[0]['productitem_id'];
				$oldprod = $products[0];
				foreach ($products as $product) {
					if ($product['productitem_id'] == $oldid) {
						if ($product['defimage']) {
							array_unshift($imagearray,array("image_full" => $product['image_full'], "image_thumb" => $product['image_thumb']));
						} else {
							array_push($imagearray, array("image_full" => $product['image_full'], "image_thumb" => $product['image_thumb']));
						}
					}
					else {
						$p = $oldprod;
						unset($p['image_full']);
						unset($p['image_thumb']);
						$p['images'] = $imagearray;
						array_push($prod,$p);

						$oldprod = $product;
						$oldid = $product['productitem_id'];
						$imagearray = array();
						if ($product['defimage']) {
							array_unshift($imagearray,array("image_full" => $product['image_full'], "image_thumb" => $product['image_thumb']));
						} else {
							array_push($imagearray, array("image_full" => $product['image_full'], "image_thumb" => $product['image_thumb']));
						}
					}
				}

				// Get the last product
				$p = $oldprod;
				unset($p['image_full']);
				unset($p['image_thumb']);
				$p['images'] = $imagearray;
				array_push($prod,$p);
				$imagearray = array();
		}

		return $prod;
	}



	public function getAllProducts($offset, $limit, $filters = null){
		if(is_null($filters)) {
			$products = DB::select('products.name', 'products.description', 'products.modelnumber', 'products.instock', 'products.stockcount', 'products.origimage', 'productitems.mrp','productitems.technicalspec', 'productitems.conditionspec', 'productitems.longdescription', array('productitems.title', 'product_title'), array('productitems.description', 'product_description'), array('productitems.id', 'productitem_id'), 'productitems.issold', 'productitems.hidden', 'productitems.isapproved', 'productitems.isfactory', 'productitems.brandnew', 'productitems.saleprice', 'productitems.manufacturingyear', 'productitems.warranty', 'productitems.vendorprice', 'productitems.ispaid', array('images.thumbnail', 'image_thumb'), array('images.name', 'image_full'), array('images.isdefault', 'defimage'), array('users.firstname', 'dealer_name'))->from('productitems')->join('products')->on('products.id', '=', 'productitems.product_id')->join('users')->on('productitems.user_id', '=', 'users.id')->join('images', 'left')->on('images.productitem_id', '=', 'productitems.id')->order_by('productitems.timestamp', 'DESC')->limit($limit)->offset($offset)->execute()->as_array();
		} else{
			$products = DB::select('products.name', 'products.description', 'products.modelnumber', 'products.instock', 'products.stockcount', 'products.origimage', 'productitems.mrp','productitems.technicalspec', 'productitems.conditionspec', 'productitems.longdescription', array('productitems.title', 'product_title'), array('productitems.description', 'product_description'), array('productitems.id', 'productitem_id'), 'productitems.issold', 'productitems.isapproved', 'productitems.saleprice', 'productitems.manufacturingyear', 'productitems.warranty', 'productitems.vendorprice', 'productitems.ispaid', array('images.thumbnail', 'image_thumb'), array('images.name', 'image_full'))->from('productitems')->join('products')->on('products.id', '=', 'productitems.product_id')->join('productitemfilters')->on('productitemfilters.productitem_id', '=', 'produitems.id')->join('images', 'left')->on('images.productitem_id', '=', 'productitems.id')->on('images.isdefault', '=', DB::expr('1'))->where('productitemfilters.filter_id', 'IN', $filters)->group_by('productitems.id')->limit($limit)->offset($offset)->execute()->as_array();
		}
		$prod = array();
		if (empty($products)) {
					return $products;
				}

				// Convert image records to image array
				$imagearray = array();
				$oldid = $products[0]['productitem_id'];
				$oldprod = $products[0];
				foreach ($products as $product) {
					if ($product['productitem_id'] == $oldid) {
						if ($product['defimage']) {
							array_unshift($imagearray,array("image_full" => $product['image_full'], "image_thumb" => $product['image_thumb']));
						} else {
							array_push($imagearray, array("image_full" => $product['image_full'], "image_thumb" => $product['image_thumb']));
						}
					}
					else {
						$p = $oldprod;
						unset($p['image_full']);
						unset($p['image_thumb']);
						$p['images'] = $imagearray;
						array_push($prod,$p);

						$oldprod = $product;
						$oldid = $product['productitem_id'];
						$imagearray = array();
						if ($product['defimage']) {
							array_unshift($imagearray,array("image_full" => $product['image_full'], "image_thumb" => $product['image_thumb']));
						} else {
							array_push($imagearray, array("image_full" => $product['image_full'], "image_thumb" => $product['image_thumb']));
						}
					}
				}

				// Get the last product
				$p = $oldprod;
				unset($p['image_full']);
				unset($p['image_thumb']);
				$p['images'] = $imagearray;
				array_push($prod,$p);
				$imagearray = array();
		return $prod;
	}

	public function getLatestProducts($offset, $limit, $filters = null){
		if(is_null($filters)){
			$products = DB::select('products.name', 'products.description', 'products.modelnumber', 'products.instock', 'products.stockcount', 'products.origimage', 'productitems.mrp','productitems.technicalspec', 'productitems.conditionspec', 'productitems.longdescription', array('productitems.title', 'product_title'), array('productitems.description', 'product_description'), array('productitems.id', 'productitem_id'), 'productitems.issold', 'productitems.isapproved', 'productitems.saleprice', 'productitems.manufacturingyear', 'productitems.warranty', 'productitems.vendorprice', 'productitems.ispaid', array('images.thumbnail', 'image_thumb'), array('images.name', 'image_full'))->from('productitems')->join('products')->on('products.id', '=', 'productitems.product_id')->join('images', 'left')->on('images.productitem_id', '=', 'productitems.id')->on('images.isdefault', '=', DB::expr('1'))->order_by('productitems.saleprice', 'DESC')->limit($limit)->offset($offset)->execute()->as_array();
		}
		else{
			$products = DB::select('products.name', 'products.description', 'products.modelnumber', 'products.instock', 'products.stockcount', 'products.origimage', 'productitems.mrp','productitems.technicalspec', 'productitems.conditionspec', 'productitems.longdescription', array('productitems.title', 'product_title'), array('productitems.description', 'product_description'), array('productitems.id', 'productitem_id'), 'productitems.issold', 'productitems.isapproved', 'productitems.saleprice', 'productitems.manufacturingyear', 'productitems.warranty', 'productitems.vendorprice', 'productitems.ispaid', array('images.thumbnail', 'image_thumb'), array('images.name', 'image_full'))->from('productitems')->join('products')->on('products.id', '=', 'productitems.product_id')->join('productitemfilters')->on('productitemfilters.productitem_id', '=', 'produitems.id')->join('images', 'left')->on('images.productitem_id', '=', 'productitems.id')->on('images.isdefault', '=', DB::expr('1'))->where('productitemfilters.filter_id', 'IN', $filters)->group_by('productitems.id')->order_by('productitems.saleprice', 'DESC')->limit($limit)->offset($offset)->execute()->as_array();
		}
		return $products;
	}

	public function getProductsByModel($modelnumber, $offset, $limit, $filters = null){
		if(is_null($filters)){
			$products = DB::select('products.name', 'products.description', 'products.modelnumber', 'products.instock', 'products.stockcount', 'products.origimage', 'productitems.mrp','productitems.technicalspec', 'productitems.conditionspec', 'productitems.longdescription', array('productitems.title', 'product_title'), array('productitems.description', 'product_description'), array('productitems.id', 'productitem_id'), 'productitems.issold', 'productitems.isapproved', 'productitems.saleprice', 'productitems.manufacturingyear', 'productitems.warranty', 'productitems.vendorprice', 'productitems.ispaid', array('images.thumbnail', 'image_thumb'), array('images.name', 'image_full'))->from('productitems')->join('products')->on('products.id', '=', 'productitems.product_id')->join('images', 'left')->on('images.productitem_id', '=', 'productitems.id')->on('images.isdefault', '=', DB::expr('1'))->where('products.modelnumber', '=', $modelnumber)->order_by('productitems.timestamp', 'DESC')->limit($limit)->offset($offset)->execute()->as_array();
		}
		else{
			$products = DB::select('products.name', 'products.description', 'products.modelnumber', 'products.instock', 'products.stockcount', 'products.origimage', 'productitems.mrp','productitems.technicalspec', 'productitems.conditionspec', 'productitems.longdescription', array('productitems.title', 'product_title'), array('productitems.description', 'product_description'), array('productitems.id', 'productitem_id'), 'productitems.issold', 'productitems.isapproved', 'productitems.saleprice', 'productitems.manufacturingyear', 'productitems.warranty', 'productitems.vendorprice', 'productitems.ispaid', array('images.thumbnail', 'image_thumb'), array('images.name', 'image_full'))->from('productitems')->join('products')->on('products.id', '=', 'productitems.product_id')->join('productitemfilters')->on('productitemfilters.productitem_id', '=', 'produitems.id')->join('images', 'left')->on('images.productitem_id', '=', 'productitems.id')->on('images.isdefault', '=', DB::expr('1'))->where('products.modelnumber', '=', $modelnumber)->where('productitemfilters.filter_id', 'IN', $filters)->group_by('productitems.id')->order_by('productitems.timestamp', 'DESC')->limit($limit)->offset($offset)->execute()->as_array();
		}
		return $products;
	}

	public function getProductsByCategory($category_id, $offset, $limit, $filters = null, $showoos=1){
		if(is_null($filters)){
			if ($showoos == 0) {
				$products = DB::select('products.name', 'products.description', 'products.modelnumber', 'products.instock', 'products.stockcount', 'products.origimage', 'productitems.mrp','productitems.technicalspec', 'productitems.conditionspec', 'productitems.longdescription','productitems.isfactory','productitems.brandnew',  array('productitems.title', 'product_title'), array('productitems.description', 'product_description'), array('productitems.id', 'productitem_id'), 'productitems.issold', 'productitems.isapproved', 'productitems.saleprice', 'productitems.manufacturingyear', 'productitems.warranty', 'productitems.vendorprice', 'productitems.ispaid', array('images.thumbnail', 'image_thumb'), array('images.name', 'image_full'))->from('productitems')->join('products')->on('products.id', '=', 'productitems.product_id')->join('images', 'left')->on('images.productitem_id', '=', 'productitems.id')->on('images.isdefault', '=', DB::expr('1'))->where('productitems.productcategory_id', '=', $category_id)->where('productitems.issold', '=', 0)->order_by('productitems.timestamp', 'DESC')->limit($limit)->offset($offset)->execute()->as_array();
			} else {
				$products = DB::select('products.name', 'products.description', 'products.modelnumber', 'products.instock', 'products.stockcount', 'products.origimage', 'productitems.mrp','productitems.technicalspec', 'productitems.conditionspec', 'productitems.longdescription','productitems.isfactory','productitems.brandnew',  array('productitems.title', 'product_title'), array('productitems.description', 'product_description'), array('productitems.id', 'productitem_id'), 'productitems.issold', 'productitems.isapproved', 'productitems.saleprice', 'productitems.manufacturingyear', 'productitems.warranty', 'productitems.vendorprice', 'productitems.ispaid', array('images.thumbnail', 'image_thumb'), array('images.name', 'image_full'))->from('productitems')->join('products')->on('products.id', '=', 'productitems.product_id')->join('images', 'left')->on('images.productitem_id', '=', 'productitems.id')->on('images.isdefault', '=', DB::expr('1'))->where('productitems.productcategory_id', '=', $category_id)->order_by('productitems.timestamp', 'DESC')->limit($limit)->offset($offset)->execute()->as_array();
			}
		}
		else{
			$products = DB::select('products.name', 'products.description', 'products.modelnumber', 'products.instock', 'products.stockcount', 'products.origimage', 'productitems.mrp','productitems.technicalspec', 'productitems.conditionspec', 'productitems.longdescription', array('productitems.title', 'product_title'), array('productitems.description', 'product_description'), array('productitems.id', 'productitem_id'), 'productitems.issold', 'productitems.isapproved', 'productitems.saleprice', 'productitems.manufacturingyear', 'productitems.warranty', 'productitems.vendorprice', 'productitems.ispaid', array('images.thumbnail', 'image_thumb'), array('images.name', 'image_full'))->from('productitems')->join('products')->on('products.id', '=', 'productitems.product_id')->join('productitemfilters')->on('productitemfilters.productitem_id', '=', 'produitems.id')->join('images', 'left')->on('images.productitem_id', '=', 'productitems.id')->on('images.isdefault', '=', DB::expr('1'))->where('productitems.productcategory_id', '=', $category_id)->where('productitemfilters.filter_id', 'IN', $filters)->group_by('productitems.id')->order_by('productitems.timestamp', 'DESC')->limit($limit)->offset($offset)->execute()->as_array();
		}
		return $products;
	}

	public function getProductsByCategorySortPrice($category_id, $offset, $limit, $sort, $filters = null, $showoos=1){
		if(is_null($filters)){
			if ($showoos == 0) {
				$products = DB::select('products.name', 'products.description', 'products.modelnumber', 'products.instock', 'products.stockcount', 'products.origimage', 'productitems.mrp','productitems.technicalspec', 'productitems.conditionspec', 'productitems.longdescription', 'productitems.isfactory','productitems.brandnew',  array('productitems.title', 'product_title'), array('productitems.description', 'product_description'), array('productitems.id', 'productitem_id'), 'productitems.issold', 'productitems.isapproved', 'productitems.saleprice', 'productitems.manufacturingyear', 'productitems.warranty', 'productitems.vendorprice', 'productitems.ispaid', array('images.thumbnail', 'image_thumb'), array('images.name', 'image_full'))->from('productitems')->join('products')->on('products.id', '=', 'productitems.product_id')->join('images', 'left')->on('images.productitem_id', '=', 'productitems.id')->on('images.isdefault', '=', DB::expr('1'))->where('productitems.productcategory_id', '=', $category_id)->where('productitems.issold', '=', 0)->where('productitems.hidden', '=', 0)->order_by('productitems.saleprice', $sort)->limit($limit)->offset($offset)->execute()->as_array();
			} else {
				$products = DB::select('products.name', 'products.description', 'products.modelnumber', 'products.instock', 'products.stockcount', 'products.origimage', 'productitems.mrp','productitems.technicalspec', 'productitems.conditionspec', 'productitems.longdescription', 'productitems.isfactory','productitems.brandnew',  array('productitems.title', 'product_title'), array('productitems.description', 'product_description'), array('productitems.id', 'productitem_id'), 'productitems.issold', 'productitems.isapproved', 'productitems.saleprice', 'productitems.manufacturingyear', 'productitems.warranty', 'productitems.vendorprice', 'productitems.ispaid', array('images.thumbnail', 'image_thumb'), array('images.name', 'image_full'))->from('productitems')->join('products')->on('products.id', '=', 'productitems.product_id')->join('images', 'left')->on('images.productitem_id', '=', 'productitems.id')->on('images.isdefault', '=', DB::expr('1'))->where('productitems.productcategory_id', '=', $category_id)->where('productitems.hidden', '=', 0)
					->order_by('productitems.issold', 'ASC')
				    ->order_by('productitems.saleprice', $sort)
				    ->limit($limit)->offset($offset)->execute()->as_array();
			}
		}
		else{
			$products = DB::select('products.name', 'products.description', 'products.modelnumber', 'products.instock', 'products.stockcount', 'products.origimage', 'productitems.mrp','productitems.technicalspec', 'productitems.conditionspec', 'productitems.longdescription', array('productitems.title', 'product_title'), array('productitems.description', 'product_description'), array('productitems.id', 'productitem_id'), 'productitems.issold', 'productitems.isapproved', 'productitems.saleprice', 'productitems.manufacturingyear', 'productitems.warranty', 'productitems.vendorprice', 'productitems.ispaid', array('images.thumbnail', 'image_thumb'), array('images.name', 'image_full'))->from('productitems')->join('products')->on('products.id', '=', 'productitems.product_id')->join('productitemfilters')->on('productitemfilters.productitem_id', '=', 'produitems.id')->join('images', 'left')->on('images.productitem_id', '=', 'productitems.id')->on('images.isdefault', '=', DB::expr('1'))->where('productitems.productcategory_id', '=', $category_id)->where('productitemfilters.filter_id', 'IN', $filters)->group_by('productitems.id')->order_by('productitems.saleprice', $sort)->limit($limit)->offset($offset)->execute()->as_array();
		}
		return $products;
	}

	public function getProductsByCategorySortDate($category_id, $offset, $limit, $sort, $filters = null, $showoos=1){
		if(is_null($filters)){
			if ($showoos == 0) {
				$products = DB::select('products.name', 'products.description', 'products.modelnumber', 'products.instock', 'products.stockcount', 'products.origimage', 'productitems.mrp','productitems.technicalspec', 'productitems.conditionspec', 'productitems.longdescription','productitems.isfactory','productitems.brandnew',  array('productitems.title', 'product_title'), array('productitems.description', 'product_description'), array('productitems.id', 'productitem_id'), 'productitems.issold', 'productitems.isapproved', 'productitems.saleprice', 'productitems.manufacturingyear', 'productitems.warranty', 'productitems.vendorprice', 'productitems.ispaid', array('images.thumbnail', 'image_thumb'), array('images.name', 'image_full'))->from('productitems')->join('products')->on('products.id', '=', 'productitems.product_id')->join('images', 'left')->on('images.productitem_id', '=', 'productitems.id')->on('images.isdefault', '=', DB::expr('1'))->where('productitems.productcategory_id', '=', $category_id)->where('productitems.issold', '=', 0)->where('productitems.hidden', '=', 0)
				    ->order_by('productitems.id', $sort)->limit($limit)->offset($offset)->execute()->as_array();
			} else {
				$products = DB::select('products.name', 'products.description', 'products.modelnumber', 'products.instock', 'products.stockcount', 'products.origimage', 'productitems.mrp','productitems.technicalspec', 'productitems.conditionspec', 'productitems.longdescription','productitems.isfactory','productitems.brandnew',  array('productitems.title', 'product_title'), array('productitems.description', 'product_description'), array('productitems.id', 'productitem_id'), 'productitems.issold', 'productitems.isapproved', 'productitems.saleprice', 'productitems.manufacturingyear', 'productitems.warranty', 'productitems.vendorprice', 'productitems.ispaid', array('images.thumbnail', 'image_thumb'), array('images.name', 'image_full'))->from('productitems')->join('products')->on('products.id', '=', 'productitems.product_id')->join('images', 'left')->on('images.productitem_id', '=', 'productitems.id')->on('images.isdefault', '=', DB::expr('1'))->where('productitems.productcategory_id', '=', $category_id)->where('productitems.hidden', '=', 0)
					->order_by('productitems.issold', 'ASC')
				    ->order_by('productitems.id', $sort)->limit($limit)->offset($offset)->execute()->as_array();
		}
		}
		else{
			$products = DB::select('products.name', 'products.description', 'products.modelnumber', 'products.instock', 'products.stockcount', 'products.origimage', 'productitems.mrp','productitems.technicalspec', 'productitems.conditionspec', 'productitems.longdescription', array('productitems.title', 'product_title'), array('productitems.description', 'product_description'), array('productitems.id', 'productitem_id'), 'productitems.issold', 'productitems.isapproved', 'productitems.saleprice', 'productitems.manufacturingyear', 'productitems.warranty', 'productitems.vendorprice', 'productitems.ispaid', array('images.thumbnail', 'image_thumb'), array('images.name', 'image_full'))->from('productitems')->join('products')->on('products.id', '=', 'productitems.product_id')->join('productitemfilters')->on('productitemfilters.productitem_id', '=', 'produitems.id')->join('images', 'left')->on('images.productitem_id', '=', 'productitems.id')->on('images.isdefault', '=', DB::expr('1'))->where('productitems.productcategory_id', '=', $category_id)->where('productitemfilters.filter_id', 'IN', $filters)->group_by('productitems.id')->order_by('productitems.timestamp', $sort)->limit($limit)->offset($offset)->execute()->as_array();
		}

		return $products;
	}
	public function getProductsByCategorySortQuality($category_id, $offset, $limit, $sort, $filters = null, $showoos=1){
		if ($showoos == 0) {
			$qualityindex = DB::query(Database::SELECT, "SELECT  qualityratings.productitem_id, AVG(value) as qualityindex FROM qualityratings,productitems where productitems.`productcategory_id` = $category_id and productitems.id = qualityratings.productitem_id and productitems.issold = 0 and productitems.hidden = 0 group by qualityratings.productitem_id order by qualityindex DESC limit $limit offset $offset")->execute()->as_array();
		} else {
			$qualityindex = DB::query(Database::SELECT, "SELECT  qualityratings.productitem_id, AVG(value) as qualityindex FROM qualityratings,productitems where productitems.`productcategory_id` = $category_id and productitems.id = qualityratings.productitem_id and productitems.hidden = 0 group by qualityratings.productitem_id order by productitems.issold ASC, qualityindex DESC limit $limit offset $offset")->execute()->as_array();
		}
		$products = array();
		foreach($qualityindex as $p) {
			$id = $p['productitem_id'];
			$product = DB::select('products.name', 'products.description', 'products.modelnumber', 'products.instock','productitems.isfactory','productitems.brandnew',  'products.stockcount', 'products.origimage', 'productitems.mrp','productitems.technicalspec', 'productitems.conditionspec', 'productitems.longdescription', array('productitems.title', 'product_title'), array('productitems.description', 'product_description'), array('productitems.id', 'productitem_id'), 'productitems.issold', 'productitems.isapproved', 'productitems.saleprice', 'productitems.manufacturingyear', 'productitems.warranty', 'productitems.vendorprice', 'productitems.ispaid', array('images.thumbnail', 'image_thumb'), array('images.name', 'image_full'))->from('productitems')->join('products')->on('products.id', '=', 'productitems.product_id')->join('images', 'left')->on('images.productitem_id', '=', 'productitems.id')->on('images.isdefault', '=', DB::expr('1'))->where('productitems.id', '=', $id)->execute()->as_array();

			$product[0]['qualityindex'] = $p['qualityindex'];
			array_push($products, $product[0]);
		}

		return $products;
	}

} // End Auth Role Model
