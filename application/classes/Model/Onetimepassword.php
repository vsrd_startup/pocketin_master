<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Default auth role
 *
 * @package    Kohana/Auth
 * @author     Kohana Team
 * @copyright  (c) 2007-2009 Kohana Team
 * @license    http://kohanaphp.com/license.html
 */
class Model_Onetimepassword extends ORM {

	public function save_otp($values, $expected){
		$validation_results = Validation::factory($values);
		return $this->values($values, $expected)->create($validation_results);
	}

} // End Auth Role Model