<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Default auth role
 *
 * @package    Kohana/Auth
 * @author     Kohana Team
 * @copyright  (c) 2007-2009 Kohana Team
 * @license    http://kohanaphp.com/license.html
 */
class Model_Qualityrating extends ORM {
	public function add_qualityrating($values, $expected){
		$validation_results = Validation::factory($values);
		return $this->values($values, $expected)->create($validation_results);
	}

	public function update_qualityrating($values, $expected = NULL)
	{
		$validation_results = Validation::factory($values);

		return $this->values($values, $expected)->update($validation_results);
	}

	public function getQualityRatingByProduct($productitem_id){
		$qualityindex = DB::query(Database::SELECT, "SELECT AVG(value) as qualityindex FROM qualityratings WHERE qualityratings.productitem_id = ".$productitem_id." group by qualityratings.productitem_id")->execute()->as_array();
		return $qualityindex[0];
	}

} // End Auth Role Model
