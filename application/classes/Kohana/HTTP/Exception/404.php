<?php defined('SYSPATH') OR die('No direct script access.');

class Kohana_HTTP_Exception_404 extends HTTP_Exception {
    /**
     * Generate a Response for the 500 Exception.
     *
     * The user should be shown a nice 500 page.
     *
     * @return Response
     */
    public function get_response()
    {
        $view = View::factory('error_404');

        // Remembering that `$this` is an instance of HTTP_Exception_404
        $view->message = $this->getMessage();

        $response = Response::factory()
            ->status(404)
            ->body($view->render());

        return $response;
    }
}
