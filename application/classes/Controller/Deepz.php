<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Deepz extends Controller {

	public function action_index()
	{
		$main = View::factory('deepz');
        $this->response->body($main->render());
	}

}
