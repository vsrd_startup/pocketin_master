<?php defined('SYSPATH') or die('No direct script access.');
include_once(dirname(dirname(__FILE__)) . '../../../views/blocks/zestphpsdk/source/ZestProcessor.php');
$zest = new ZestProcessor();

class Controller_Zestmoney_Zestpay extends Controller
{
 public function action_index()
    {
    	http_response_code(400);
    }

    public function action_updateorder()
    {
    	$zest = new ZestProcessor();
        try{
        	if(!empty($_REQUEST['orderno']) && !empty($_REQUEST['status']) && !empty($_REQUEST['key'])){
        		$orderid = $_REQUEST['orderno'];
        		$status = $_REQUEST['status'];
        		$key = $_REQUEST['key'];
        		$zestmerchant = $zest->getzesttomerchant();
        		$hashkey = hash('sha512', $orderid.'|'.$zestmerchant.'|'.$status);
        		$reforder = ORM::factory('Orderdetail', $orderid);
        		if($hashkey == $key){
        				if($status == 'Approved'){
        					 //update your order as live
        					$reforder->pendingemi = 0;
        					$reforder->save();
        					$result = array('status' => 'success', 'message' => 'order updated successfully');
        				} elseif(in_array($status, $zest->_getZestCancelStatus())){
        					//update your oder as cancelled
        					$this->_cancelCompleteOrder($orderid);
        					$result = array('status' => 'success', 'message' => 'order cancelled successfully');
        				} else{
        					throw new Exception('order not updated');
        				}
        		} else {
        			throw new Exception('Unauthorized');
        		}
        	} else {
        	   throw new Exception('Improper details');
        	}
          } catch (Exception $e) {
          	   $result = array('status' => 'failed', 'message' => $e->getMessage());
          }

          if($result['status']=='failed'){
               http_response_code(400);
          }

          header('Content-type: application/json');
          echo json_encode($result);
    }

    public function action_stockavailability()
    {
    	$zest = new ZestProcessor();

    	try{
    		if(!empty($_REQUEST['orderno']) && !empty($_REQUEST['key'])){
    			$orderid = $_REQUEST['orderno'];
    			$key = $_REQUEST['key'];
    			$zestmerchant = $zest->getzesttomerchant();
    			$hashkey = hash('sha512', $orderid.'|'.$zestmerchant);
    			if($hashkey == $key){
    				   /*check the inventory availability for the order*/
    				   //VKT:TODO: implement this later
    				   $isinventoryavailable = true;
    					if($isinventoryavailable){
    						 //if inventory available for the order
    						$result = array('IsAvailable' => 'true', 'message' => 'available');
    					} else {
    						//if inventory not available for the order
    						throw new Exception('Ordered products not available in requested quantity');
    					}
    			} else {
    				throw new Exception('Unauthorized');
    			}
    		}else{
    		   throw new Exception('Improper details');
    		}
    	  }catch (Exception $e) {
    	  	   $result = array('IsAvailable' => 'false', 'message' => $e->getMessage());
    	  }

	   	$badrequest = array('Improper details','Unauthorized');
	        if(in_array($result['message'], $badrequest)){
	            http_response_code(400);
	        }

    	  header('Content-type: application/json');
    	  echo json_encode($result);
    }

    public function action_reduceinventory()
    {
    	$zest = new ZestProcessor();
    	try{
    		if(!empty($_REQUEST['orderno']) && !empty($_REQUEST['key'])){
    			$orderid = $_REQUEST['orderno'];
    			$key = $_REQUEST['key'];
    			$zestmerchant = $zest->getzesttomerchant();
    			$hashkey = hash('sha512', $orderid.'|'.$zestmerchant);
    			if($hashkey == $key){
    				   /*checking the stock availability before reducing the inventory*/
    				   $isinventoryavailable = true;
    					if($isinventoryavailable){
    						 //reduce the inventory of the product
    						//TODO: VKT: do this
    						$result = array('IsAvailable' => 'true', 'message' => 'done');
    					} else {
    						//if inventory not available for the order
    						throw new Exception('Ordered products not available in requested quantity');
    					}
    			}else{
    				throw new Exception('Unauthorized');
    			}
    		}else{
    		   throw new Exception('Improper details');
    		}
    	  } catch (Exception $e) {
    	  	   $result = array('IsAvailable' => 'false', 'message' => $e->getMessage());
    	  }

    	   $badrequest = array('Improper details','Unauthorized');
    	        if(in_array($result['message'], $badrequest)){
    	            http_response_code(400);
    	        }
    	  header('Content-type: application/json');
    	  echo json_encode($result);
    }

    private function _cancelCompleteOrder($order_id)
    {
        $items = ORM::factory('Order')->where('orderdetail_id', '=', $order_id)->find_all();
        foreach($items as $item) {
            $item->iscancelled = 1;
            $item->canceldate = date('Y-m-d H:i:s');
            $item->save();
            $p = ORM::factory('Productitem', $item->productitems_id);
            $p->issold = 0;
            $p->save();
        }

        // Cancel the order
        $order = ORM::factory('Orderdetail', $order_id);
        $order->iscancelled = 1;
        $order->canceldate = date('Y-m-d H:i:s');
        $order->save();
    }
};
