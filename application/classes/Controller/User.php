<?php defined('SYSPATH') or die('No direct script access.');


class Controller_User extends Controller
{
	public function before()
	{
		// Check if the User is logged in. If not redirect to Home page
		if (Auth::instance()->logged_in()) {
			//throw new Kohana_HTTP_Exception_401();
			//throw HTTP_Exception::factory(401)->authenticate('');
		}
	}

	public function action_myaccount()
	{
		if (!Auth::instance()->logged_in()) {
			$this->redirect('../../../login');
		}
        $p = View::factory('myaccount');
        $page = $p->render();
        $this->response->body($page);
	}
	public function action_mywishlist()
	{
		if (!Auth::instance()->logged_in()) {
			$this->redirect('../../../login');
		}
        $p = View::factory('mywishlist');
        $page = $p->render();
        $this->response->body($page);
	}
	public function action_myorders()
	{
		if (!Auth::instance()->logged_in()) {
			$this->redirect('../../../login');
		}
        $p = View::factory('myorders');
        $page = $p->render();
        $this->response->body($page);
	}

	public function action_mysellrequests()
	{
		if (!Auth::instance()->logged_in()) {
			$this->redirect('../login');
		}
        $p = View::factory('mysell');
        $page = $p->render();
        $this->response->body($page);
	}

	public function action_service()
	{
		if (!Auth::instance()->logged_in()) {
			$this->redirect('../../../login');
		}
        $p = View::factory('service');
        $page = $p->render();
        $this->response->body($page);
	}

	public function action_myreferral()
	{
		if (!Auth::instance()->logged_in()) {
			$this->redirect('../login');
		}
        $p = View::factory('myreferral');
        $page = $p->render();
        $this->response->body($page);
	}


	public function action_notifications()
	{
		if (!Auth::instance()->logged_in()) {
			$this->redirect('../../../login');
		}
        $p = View::factory('mynots');
        $page = $p->render();
        $this->response->body($page);
	}
	public function action_myaddress()
	{
		if (!Auth::instance()->logged_in()) {
			$this->redirect('../../../login');
		}
        $p = View::factory('myaddress');
        $page = $p->render();
        $this->response->body($page);
	}

	 public function action_sell()
    {
    	/*if (!Auth::instance()->logged_in()) {
			$this->redirect('../login');
		}*/
        $homep = View::factory('sale-page');
        $this->response->body($homep->render());
    }

	public function action_dealer()
	{
		// Loads the dealer page
		$p = View::factory('dealer/dealer-page');
		$page = $p->render();
		$this->response->body($page);
	}

	public function action_admin()
	{
		$p = View::factory('dashboard/admin-page');
		$page = $p->render();
		$this->response->body($page);
	}


	public function action_login()
	{
		if (Auth::instance()->logged_in()) {
			$this->redirect('/../../../cart');
		}
		$loginpage = View::factory('login');
        $this->response->body($loginpage->render());
	}

	public function action_register()
	{
		$loginpage = View::factory('register');
        $this->response->body($loginpage->render());
	}


	public function action_logout()
	{
		$status = Auth::instance()->logout(TRUE, FALSE);
		//$this->redirect('../');
		/*$homep = View::factory('home');
        $this->response->body($homep->render());*/
	}

	public function action_dealerlogout()
	{
		$status = Auth::instance()->logout(TRUE, FALSE);
		$this->redirect('../user/dealer');
	}
	public function action_adminlogout()
	{
		$status = Auth::instance()->logout(TRUE, FALSE);
		$this->redirect('../user/admin');
	}

	// Initiate a transaction for the dealer
	// Returns the next PID for storing the images uploaded by the dealer
	public function action_initiatedealerupload()
	{
		$activitylog = ORM::factory('Activitylog')
		->add_log(array('type' => 'DEALER_ADD_PRODUCT_INIT', 'user_id' => $_GET['dealer_id']), array('type', 'user_id'));

		$this->response->body(json_encode(array('list' => array('status' => 1, 'pid' => $activitylog->id))));
	}

	public function action_initiatesellproduct()
	{
		$activitylog = ORM::factory('Activitylog')
		->add_log(array('type' => 'USER_SELL_PRODUCT', 'user_id' => $_GET['user_id']), array('type', 'user_id'));

		$sell_id = $_GET['user_id'] . '' . $activitylog->id;
		$this->response->body(json_encode(array('list' => array('status' => 1, 'pid' => $sell_id))));
	}

	public function action_uploadsellimage() {
		// Get the random ID passed by angular
		$pid = $_REQUEST['pid'];
		if (!isset($_REQUEST['pid'])) {
			throw new Kohana_HTTP_Exception_500('Failed to upload image, PID not set');
		}

		if (isset($_FILES['file'])) {
	   		$ret = $this->_save_sell_image($_FILES['file'], $_FILES['file']['name'], $pid);
	   		if ($ret == FALSE) {
	   			throw new Kohana_HTTP_Exception_500('Failed to upload image, Failed to save image');
	   		}
	   		echo json_encode([
			'success' => true,
			'files' => $_FILES,
			'get' => $_GET,
			'post' => $_POST,
			//optional
			'flowTotalSize' => isset($_FILES['file']) ? $_FILES['file']['size'] : $_GET['flowTotalSize'],
			'flowIdentifier' => isset($_FILES['file']) ? $_FILES['file']['name'] . '-' . $_FILES['file']['size']
			: $_GET['flowIdentifier'],
			'flowFilename' => isset($_FILES['file']) ? $_FILES['file']['name'] : $_GET['flowFilename'],
			'flowRelativePath' => isset($_FILES['file']) ? $_FILES['file']['tmp_name'] : $_GET['flowRelativePath']]);
		}
	}

	public function action_removesellimage()
	{
		$name = $_GET['name'];
		$pid = $_GET['pid'];
		$directory = DOCROOT."assets/images/sell_requests/sell_$pid";
		$file = "$directory/$name";
		if (file_exists($file)) {
        	if (unlink($file)) {
        		return $this->response->body(json_encode(array('list' => array('status' => 1))));
        	} else {
        		return $this->response->body(json_encode(array('list' => array('status' => 0))));
        	}
    	}

    	return $this->response->body(json_encode(array('list' => array('status' => 1))));
	}

	protected function _save_sell_image($image, $imagename, $pid)
    {
        if (
            ! Upload::valid($image) OR
            ! Upload::not_empty($image) OR
            ! Upload::type($image, array('jpg', 'jpeg', 'png', 'gif')))
        {
            return FALSE;
        }

        $directory = DOCROOT."assets/images/sell_requests/sell_$pid";
        if (!file_exists($directory)) {
        	mkdir($directory, 0777, true); //TODO: check the security issue here
        	chmod($directory, 0777);
    	}

        if ($file = Upload::save($image, $imagename, $directory, 0777))
        {
            return TRUE;
            //$filename = 'demo.jpg';//strtolower(Text::random('alnum', 20)).'.jpg';

            //Image::factory($file)
                //->resize(200, 200, Image::AUTO)
            //    ->save($directory.$filename);
            // Delete the temporary file
            //unlink($file);
            //return $filename;
        }
        return FALSE;
    }


	public function action_uploadprodimage() {
		//print_r ($_FILES);
		// Accept and save product images.
		// The names are set by the client code.
		// This just accepts and saves the images of products with given name

		// Get the random ID passed by angular
		if (!isset($_POST['pid'])) {
			throw new Kohana_HTTP_Exception_500('Failed to upload image, PID not set');
		}

		$pid = $_POST['pid'];
		if (isset($_FILES['file'])) {
	   		$ret = $this->_save_image($_FILES['file'], $_FILES['file']['name'], $pid);
	   		if ($ret == FALSE) {
	   			throw new Kohana_HTTP_Exception_500('Failed to upload image, Failed to save image');
	   		}
	   		echo json_encode([
			'success' => true,
			'files' => $_FILES,
			'get' => $_GET,
			'post' => $_POST,
			//optional
			'flowTotalSize' => isset($_FILES['file']) ? $_FILES['file']['size'] : $_GET['flowTotalSize'],
			'flowIdentifier' => isset($_FILES['file']) ? $_FILES['file']['name'] . '-' . $_FILES['file']['size']
			: $_GET['flowIdentifier'],
			'flowFilename' => isset($_FILES['file']) ? $_FILES['file']['name'] : $_GET['flowFilename'],
			'flowRelativePath' => isset($_FILES['file']) ? $_FILES['file']['tmp_name'] : $_GET['flowRelativePath']]);
		}
	}

	public function action_removeprodimage()
	{
		$name = $_GET['name'];
		$pid = $_GET['pid'];
		$directory = DOCROOT."assets/images/dealerpending/prod_$pid";
		$file = "$directory/$name";
		if (file_exists($file)) {
        	if (unlink($file)) {
        		return $this->response->body(json_encode(array('list' => array('status' => 1))));
        	} else {
        		return $this->response->body(json_encode(array('list' => array('status' => 0))));
        	}
    	}

    	return $this->response->body(json_encode(array('list' => array('status' => 1))));
	}

	protected function _save_image($image, $imagename, $pid)
    {
        if (
            ! Upload::valid($image) OR
            ! Upload::not_empty($image) OR
            ! Upload::type($image, array('jpg', 'jpeg', 'png', 'gif')))
        {
            return FALSE;
        }

        $directory = DOCROOT."assets/images/dealerpending/prod_$pid";
        if (!file_exists($directory)) {
        	mkdir($directory, 0777, true); //TODO: check the security issue here
        	chmod($directory, 0777);
    	}

        if ($file = Upload::save($image, $imagename, $directory, 0777))
        {
            return TRUE;
            //$filename = 'demo.jpg';//strtolower(Text::random('alnum', 20)).'.jpg';

            //Image::factory($file)
                //->resize(200, 200, Image::AUTO)
            //    ->save($directory.$filename);
            // Delete the temporary file
            //unlink($file);
            //return $filename;
        }
        return FALSE;
    }

}
