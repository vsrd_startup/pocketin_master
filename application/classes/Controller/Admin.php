<?php defined('SYSPATH') or die('No direct script access.');


class Controller_Admin extends Controller
{
	public function action_index()
	{
		$page = View::factory('dashboard/main');
		$this->response->body($page->render());
	}
}