<?php defined('SYSPATH') or die('No direct script access.');
/* We are using the RESTful API module for Kohana given by
   https://github.com/SupersonicAds/kohana-restful-api
   Supports Kohana 3.3
 */
define("KAPSYSTEMSMSAPI", 'A962153f1df80217cfe1b5309d40f8d86');

include_once(dirname(dirname(__FILE__)) . '/razorpay-php/Razorpay.php');
include_once(dirname(dirname(__FILE__)) . '../../../views/blocks/zestphpsdk/source/ZestProcessor.php');
use Razorpay\Api\Api;


class Controller_Apiv2_User extends Controller_Rest {
    // Set the Authentication to apikey
    protected $_auth_type   = RestUser::AUTH_TYPE_APIKEY;
    protected $_auth_source = RestUser::AUTH_SOURCE_GET;

    // The currently logged in User accessing the API
    // _user will either be a logged in User or a Guest User who has limited privileged
    // TODO:: Allow Gues user to access only limited operations
    protected $_current_user;

    /**
     * Initialize the example model.
     */
    public function before()
    {
        parent::before();
        if ($this->_user->get_user_id() == RestUser::GUESTUSERID) {
        	$this->_current_user = ORM::factory('User', RestUser::GUESTUSERID);
        	// Allow Guest Access
        	// TODO: Restrict only read
        	if($this->request->method() != HTTP_Request::GET)
        	{
                return;
        		$code = 401;
	            $message = 'User is not logged in';
	            $field = $this->_current_user->firstname;
	            throw HTTP_Exception::factory($code, array('error' => $message, 'field' => $field), array(), NULL)->authenticate('');
        	}
        } else {

	        $this->_current_user = ORM::factory('User', $this->_user->get_user_id());
	        if (!$this->_current_user->loaded()) {
	            // This can't happen, throw an exception
	            $code = 401;
	            $message = 'User is not logged in';
	            $field = $this->_current_user->firstname;
	            throw HTTP_Exception::factory($code, array('error' => $message, 'field' => $field), array(), NULL)->authenticate('');
	        }
    	}
    }

    /**
     * Handle GET requests.
     */
    public function action_index()
    {
        try
        {
            if($this->_params['method'] == 'getProductsByDealer'){
                $this->rest_output( $this->getProductsByDealer( $this->_params ) );
            }
            elseif($this->_params['method'] == 'getModel'){
                $this->rest_output( $this->getModel( $this->_params ) );
            }
            elseif($this->_params['method'] == 'getPendingProductsByDealer'){
                $this->rest_output( $this->getPendingProductsByDealer( $this->_params ) );
            }
            elseif($this->_params['method'] == 'getCollections'){
                $this->rest_output( $this->getCollections( $this->_params ) );
            }
            elseif($this->_params['method'] == 'getProductsByModel'){
                $this->rest_output( $this->getProductsBYModel( $this->_params ) );
            }
            elseif($this->_params['method'] == 'getProductsByID'){
                $this->rest_output( $this->getProductsByID( $this->_params ) );
            }
            elseif($this->_params['method'] == 'getProductsByQuality'){
                $this->rest_output( $this->getProductsByQuality( $this->_params ) );
            }
            elseif($this->_params['method'] == 'getAllProducts'){
                $this->rest_output( $this->getAllProducts( $this->_params ) );
            }
            elseif($this->_params['method'] == 'getParentCategories'){
                $this->rest_output( $this->getParentCategories( $this->_params ) );
            }
            elseif($this->_params['method'] == 'getProductCategories'){
                $this->rest_output( $this->getProductCategories( $this->_params ) );
            }elseif($this->_params['method'] == 'getProducttCategoriesByParentID'){
                $this->rest_output( $this->getProducttCategoriesByParentID( $this->_params ) );
            }
            elseif($this->_params['method'] == 'getProductsByCategory'){
                $this->rest_output( $this->getProductsByCategory( $this->_params ) );
            }
            elseif($this->_params['method'] == 'getLatestProducts'){
                $this->rest_output($this->getLatestProducts($this->_params));
            }
            elseif($this->_params['method'] == 'getFiltersByCategory'){
                $this->rest_output($this->getFiltersByCategory($this->_params));
            }
            elseif($this->_params['method'] == 'getWishlistedItems'){
                $this->rest_output($this->getWishlistedItems($this->_params));
            }
            elseif($this->_params['method'] == 'getQualityIndexValues'){
                $this->rest_output($this->getQualityIndexValues($this->_params));
            }
            elseif($this->_params['method'] == 'getUserDetail'){
                $this->rest_output($this->getUserDetail($this->_params));
            } elseif($this->_params['method'] == 'getSavedAddresses'){
                $this->rest_output($this->getSavedAddresses($this->_params));
            } elseif($this->_params['method'] == 'getSavedAddressById'){
                $this->rest_output($this->getSavedAddressById($this->_params));
            } elseif($this->_params['method'] == 'getCartItems') {
                $this->rest_output($this->getCartItems($this->_params));
            } elseif($this->_params['method'] == 'getUserOrders') {
                $this->rest_output($this->getUserOrders($this->_params));
            } elseif($this->_params['method'] == 'getOrderDetails') {
                $this->rest_output($this->getOrderDetails($this->_params));
            } elseif($this->_params['method'] == 'getFinishedOrders') {
                $this->rest_output($this->getFinishedOrders($this->_params));
            } elseif($this->_params['method'] == 'getUserNotifications') {
                $this->rest_output($this->getUserNotifications($this->_params));
            } elseif($this->_params['method'] == 'seenNotification') {
                $this->rest_output($this->seenNotification($this->_params));
            }
            elseif($this->_params['method'] == 'search') {
                $this->rest_output($this->search($this->_params));
            }elseif($this->_params['method'] == 'getSpecialProducts') {
                $this->rest_output($this->getSpecialProducts($this->_params));
            }elseif($this->_params['method'] == 'getQualityIndexKeyValueForProduct') {
                $this->rest_output($this->getQualityIndexKeyValueForProduct($this->_params));
            }elseif($this->_params['method'] == 'getUserCart') {
                $this->rest_output($this->getUserCart($this->_params));
            }elseif($this->_params['method'] == 'getUserWishlist') {
                $this->rest_output($this->getUserWishlist($this->_params));
            } elseif($this->_params['method'] == 'loadUserSeen') {
                $this->rest_output($this->loadUserSeen($this->_params));
            } elseif($this->_params['method'] == 'loadProductHotness') {
                $this->rest_output($this->loadProductHotness($this->_params));
            }
            elseif($this->_params['method'] == 'getComplaints') {
                $this->rest_output($this->getComplaints($this->_params));
            }elseif($this->_params['method'] == 'getWarehouseProductStages') {
                $this->rest_output($this->getWarehouseProductStages($this->_params));
            }elseif($this->_params['method'] == 'getWarehouseProducts') {
                $this->rest_output($this->getWarehouseProducts($this->_params));
            }elseif($this->_params['method'] == 'getWarehouseStats') {
                $this->rest_output($this->getWarehouseStats($this->_params));
            }
            elseif($this->_params['method'] == 'getAllTickets') {
                $this->rest_output($this->getAllTickets($this->_params));
            }elseif($this->_params['method'] == 'getPendingTickets') {
                $this->rest_output($this->getPendingTickets($this->_params));
            }elseif($this->_params['method'] == 'getAllComments') {
                $this->rest_output($this->getAllComments($this->_params));
            }elseif($this->_params['method'] == 'searchOrders') {
                $this->rest_output($this->searchOrders($this->_params));
            }elseif($this->_params['method'] == 'getCallDetails') {
                $this->rest_output($this->getCallDetails($this->_params));
            } elseif($this->_params['method'] == 'searchProducts') {
                $this->rest_output($this->searchProducts($this->_params));
            }elseif($this->_params['method'] == 'markRegisteredUsers') {
                $this->rest_output($this->markRegisteredUsers($this->_params));
            }elseif($this->_params['method'] == 'getCallStats') {
                $this->rest_output($this->getCallStats($this->_params));
            }elseif($this->_params['method'] == 'get_all_patients') {
                $this->rest_output($this->get_all_patients($this->_params));
            }elseif($this->_params['method'] == 'warehouseModifyProduct') {
                $this->rest_output($this->warehouseModifyProduct($this->_params));
            }elseif($this->_params['method'] == 'getStatistics') {
                $this->rest_output($this->getStatistics($this->_params));
            }elseif($this->_params['method'] == 'sellProduct') {
                $this->rest_output($this->sellProduct($this->_params));
            }elseif($this->_params['method'] == 'getPendingSell') {
                $this->rest_output($this->getPendingSell($this->_params));
            }elseif($this->_params['method'] == 'getCustomerSellRequests') {
                $this->rest_output($this->getCustomerSellRequests($this->_params));
            }elseif($this->_params['method'] == 'getCustomerReferrals') {
                $this->rest_output($this->getCustomerReferrals($this->_params));
            }elseif($this->_params['method'] == 'addCustomerReferral') {
                $this->rest_output($this->addCustomerReferral($this->_params));
            }elseif($this->_params['method'] == 'checkReferral') {
                $this->rest_output($this->checkReferral($this->_params));
            }elseif($this->_params['method'] == 'getProcurementDetails') {
                $this->rest_output($this->getProcurementDetails($this->_params));
            }elseif($this->_params['method'] == 'getServiceRequests') {
                $this->rest_output($this->getServiceRequests($this->_params));
            }elseif($this->_params['method'] == 'getOrderDetailsByUserId') {
                $this->rest_output($this->getOrderDetailsByUserId($this->_params));
            }elseif($this->_params['method'] == 'getExpenseDetails') {
                $this->rest_output($this->getExpenseDetails($this->_params));
            }elseif($this->_params['method'] == 'getEmployeeDetails') {
                $this->rest_output($this->getEmployeeDetails($this->_params));
            }elseif($this->_params['method'] == 'getLeaveDetails') {
                $this->rest_output($this->getLeaveDetails($this->_params));
            }elseif($this->_params['method'] == 'getBuyBackDetails') {
                $this->rest_output($this->getBuyBackDetails($this->_params));
           }elseif($this->_params['method'] == 'getBuyBackStages') {
                $this->rest_output($this->getBuyBackStages($this->_params));
           }elseif($this->_params['method'] == 'reopenBb') {
                $this->rest_output($this->reopenBb($this->_params));
           }elseif($this->_params['method'] == 'is_under_warranty') {
                $this->rest_output($this->is_under_warranty($this->_params));
           }elseif($this->_params['method'] == 'addRepairExpense') {
                $this->rest_output($this->addRepairExpense($this->_params));
           }elseif($this->_params['method'] == 'getCouponDetails') {
                $this->rest_output($this->getCouponDetails($this->_params));
           }elseif($this->_params['method'] == 'getCouponDiscount') {
                $this->rest_output($this->getCouponDiscount($this->_params));
           }elseif($this->_params['method'] == 'getFilteredProducts') {
                $this->rest_output($this->getFilteredProducts($this->_params));
           }elseif($this->_params['method'] == 'getVendorDetails') {
                $this->rest_output($this->getVendorDetails($this->_params));
           } elseif($this->_params['method'] == 'saveCartComment') {
                $this->rest_output($this->saveCartComment($this->_params));
           } elseif($this->_params['method'] == 'wishListSave') {
                $this->rest_output($this->wishListSave($this->_params));
           }
           elseif($this->_params['method'] == 'addTicketComment') {
                $this->rest_output($this->addTicketComment($this->_params));
           } elseif($this->_params['method'] == 'getDeliveryDetails') {
                $this->rest_output($this->getDeliveryDetails($this->_params));

           } elseif($this->_params['method'] == 'getDelSlotInfo') {
                $this->rest_output($this->getDelSlotInfo($this->_params));
           } elseif($this->_params['method'] == 'getZestObject') {
                $this->rest_output($this->getZestObject($this->_params));
           } elseif($this->_params['method'] == 'processZestPayment') {
                $this->rest_output($this->processZestPayment($this->_params));
           } elseif($this->_params['method'] == 'deliverZestEmi') {
                $this->rest_output($this->deliverZestEmi($this->_params));
           } elseif($this->_params['method'] == 'refundZestEmi') {
                $this->rest_output($this->refundZestEmi($this->_params));
           } elseif($this->_params['method'] == 'cancelZestEmi') {
                $this->rest_output($this->cancelZestEmi($this->_params));
           }elseif($this->_params['method'] == 'markReturned') {
                $this->rest_output($this->markReturned($this->_params));
           }elseif($this->_params['method'] == 'getConversion') {
                $this->rest_output($this->getConversion($this->_params));
           }elseif($this->_params['method'] == 'addToCartNoLogin') {
                $this->rest_output($this->addToCartNoLogin($this->_params));
           } elseif($this->_params['method'] == 'reqCallback') {
                $this->rest_output($this->reqCallback($this->_params));
           } elseif($this->_params['method'] == 'getCallBack') {
                $this->rest_output($this->getCallBack($this->_params));
           }elseif($this->_params['method'] == 'updateCallBack') {
                $this->rest_output($this->updateCallBack($this->_params));
           }elseif($this->_params['method'] == 'getWarehouseInfo') {
                $this->rest_output($this->getWarehouseInfo($this->_params));
           }elseif($this->_params['method'] == 'getZestEMIStatus') {
                $this->rest_output($this->getZestEMIStatus($this->_params));
           }elseif($this->_params['method'] == 'uploadPhotoStatus') {
                $this->rest_output($this->uploadPhotoStatus($this->_params));
           }elseif($this->_params['method'] == 'getPendingUploads') {
                $this->rest_output($this->getPendingUploads($this->_params));
           }
        }
        catch (Kohana_HTTP_Exception $khe)
        {
            $this->_error($khe);
            throw $khe;
        }
        catch (Kohana_Exception $e)
        {
            $this->_error('An internal error has occurred', 500);
            throw $e;
        }
    }

    /**
     * Handle POST requests.
     */
    public function action_create()
    {
        try
        {
            if($this->_params['method'] == 'addProduct'){
                $this->rest_output( $this->addProduct( $this->_params ) );
            } elseif($this->_params['method'] == 'addProductByDealer'){
                $this->rest_output( $this->addProductByDealer( $this->_params ) );
            } elseif($this->_params['method'] == 'CreateOrModifyModel'){
                $this->rest_output( $this->CreateOrModifyModel( $this->_params ) );
            }
            elseif($this->_params['method'] == 'addParentCategory'){
                $this->rest_output( $this->addParentCategory($this->_params));
            }
            elseif($this->_params['method'] == 'addProductCategory'){
                $this->rest_output( $this->addProductCategory($this->_params));
            }
            elseif($this->_params['method'] == 'addFilterTypes'){
                $this->rest_output( $this->addFilterTypes($this->_params));
            }
            elseif($this->_params['method'] == 'addFilter'){
                $this->rest_output( $this->addFilter($this->_params));
            }
            elseif($this->_params['method'] == 'addTechnicalSpec'){
                $this->rest_output( $this->addTechnicalSpec($this->_params));
            }
            elseif($this->_params['method'] == 'markAsApproved'){
                $this->rest_output( $this->markAsApproved( $this->_params ) );
            } elseif($this->_params['method'] == 'markSold'){
                $this->rest_output( $this->markSold( $this->_params ) );
            }elseif($this->_params['method'] == 'markUnSold'){
                $this->rest_output( $this->markUnSold( $this->_params ) );
            }
            elseif($this->_params['method'] == 'orderProducts'){
                $this->rest_output( $this->orderProducts( $this->_params ) );
            }
            elseif($this->_params['method'] == 'CapturePaymentAndOrderProducts'){
                $this->rest_output( $this->CapturePaymentAndOrderProducts( $this->_params ) );
            }
            elseif($this->_params['method'] == 'initiateDelivery'){
                $this->rest_output( $this->initiateDelivery( $this->_params ) );
            }
            elseif($this->_params['method'] == 'markDelivered'){
                $this->rest_output( $this->markDelivered( $this->_params ) );
            }
            elseif($this->_params['method'] == 'markShipped'){
                $this->rest_output( $this->markShipped( $this->_params ) );
            }
            elseif($this->_params['method'] == 'addQualityIndex') {
                $this->rest_output( $this->addQualityIndex( $this->_params ) );
            }
            elseif($this->_params['method'] == 'addNewAddress') {
                $this->rest_output( $this->addNewAddress( $this->_params ) );
            }
            elseif($this->_params['method'] == 'addToWishlist'){
                $this->rest_output( $this->addToWishlist( $this->_params ) );
            }
            elseif($this->_params['method'] == 'removeFromWishlist'){
                $this->rest_output( $this->removeFromWishlist( $this->_params ) );
            }
            elseif($this->_params['method'] == 'updateUserName'){
                $this->rest_output( $this->updateUserName( $this->_params ) );
            }elseif($this->_params['method'] == 'updateUserNameLast'){
                $this->rest_output( $this->updateUserNameLast( $this->_params ) );
            }
            elseif($this->_params['method'] == 'updateEmail'){
                $this->rest_output( $this->updateEmail( $this->_params ) );
            }
            elseif($this->_params['method'] == 'updatePassword'){
                $this->rest_output( $this->updatePassword( $this->_params ) );
            }
            elseif($this->_params['method'] == 'updateSavedAddress'){
                $this->rest_output( $this->updateSavedAddress( $this->_params ) );
            }
            elseif($this->_params['method'] == 'addToCart') {
                $this->rest_output($this->addToCart($this->_params));
            }
            elseif($this->_params['method'] == 'removeFromCart') {
                $this->rest_output($this->removeFromCart($this->_params));
            }
            elseif($this->_params['method'] == 'removesavedaddress') {
                $this->rest_output($this->removesavedaddress($this->_params));
            }elseif($this->_params['method'] == 'cancelOrder') {
                $this->rest_output($this->cancelOrder($this->_params));
            }  elseif($this->_params['method'] == 'performAction') {
                $this->rest_output($this->performAction($this->_params));
            } elseif($this->_params['method'] == 'updateProductItem') {
                $this->rest_output($this->updateProductItem($this->_params));
            } elseif($this->_params['method'] == 'resizeImages') {
                $this->rest_output($this->resizeImages($this->_params));
            }elseif($this->_params['method'] == 'HideProduct') {
                $this->rest_output($this->HideProduct($this->_params));
            }elseif($this->_params['method'] == 'updateOrderComment') {
                $this->rest_output($this->updateOrderComment($this->_params));
            }elseif($this->_params['method'] == 'warehouseAddProduct') {
                $this->rest_output($this->warehouseAddProduct($this->_params));
            }elseif($this->_params['method'] == 'warehouseModifyProduct') {
                $this->rest_output($this->warehouseModifyProduct($this->_params));
            }elseif($this->_params['method'] == 'addTicket') {
                $this->rest_output($this->addTicket($this->_params));
            }elseif($this->_params['method'] == 'addTicketComment') {
                $this->rest_output($this->addTicketComment($this->_params));
            }elseif($this->_params['method'] == 'modifyComment') {
                $this->rest_output($this->modifyComment($this->_params));
            }elseif($this->_params['method'] == 'setTicketStatus') {
                $this->rest_output($this->setTicketStatus($this->_params));
           }elseif($this->_params['method'] == 'addCustomerCall') {
                $this->rest_output($this->addCustomerCall($this->_params));
            }elseif($this->_params['method'] == 'saveCallComment') {
                $this->rest_output($this->saveCallComment($this->_params));
            }elseif($this->_params['method'] == 'approveSellPrice') {
                $this->rest_output($this->approveSellPrice($this->_params));
            }
            elseif($this->_params['method'] == 'add_patient') {
                $this->rest_output($this->add_patient($this->_params));
            }elseif($this->_params['method'] == 'modify_patient') {
                $this->rest_output($this->modify_patient($this->_params));
            }elseif($this->_params['method'] == 'search_patient') {
                $this->rest_output($this->search_patient($this->_params));
            }elseif($this->_params['method'] == 'markProcured') {
                $this->rest_output($this->markProcured($this->_params));
            }elseif($this->_params['method'] == 'markRejected') {
                $this->rest_output($this->markRejected($this->_params));
            }elseif($this->_params['method'] == 'markContacted') {
                $this->rest_output($this->markContacted($this->_params));
            }
            elseif($this->_params['method'] == 'saveSaleComment') {
                $this->rest_output($this->saveSaleComment($this->_params));
            }elseif($this->_params['method'] == 'searchSaleProducts') {
                $this->rest_output($this->searchSaleProducts($this->_params));
            }elseif($this->_params['method'] == 'UnHideProduct') {
                $this->rest_output($this->UnHideProduct($this->_params));
            }elseif($this->_params['method'] == 'addProcurementDetails') {
                $this->rest_output($this->addProcurementDetails($this->_params));
            }elseif($this->_params['method'] == 'addExpenseDetails') {
                $this->rest_output($this->addExpenseDetails($this->_params));
            }elseif($this->_params['method'] == 'addEmployeeDetails') {
                $this->rest_output($this->addEmployeeDetails($this->_params));
            }elseif($this->_params['method'] == 'addLeave') {
                $this->rest_output($this->addLeave($this->_params));
            }elseif($this->_params['method'] == 'addBuyBack') {
                $this->rest_output($this->addBuyBack($this->_params));
            }elseif($this->_params['method'] == 'modifyBbPrice') {
                $this->rest_output($this->modifyBbPrice($this->_params));
            }elseif($this->_params['method'] == 'markBbPickup') {
                $this->rest_output($this->markBbPickup($this->_params));
            }elseif($this->_params['method'] == 'markBbRejected') {
                $this->rest_output($this->markBbRejected($this->_params));
            }elseif($this->_params['method'] == 'addBbTech') {
                $this->rest_output($this->addBbTech($this->_params));
           }elseif($this->_params['method'] == 'reopenBb') {
                $this->rest_output($this->reopenBb($this->_params));
           }elseif($this->_params['method'] == 'addRepairExpense') {
                $this->rest_output($this->addRepairExpense($this->_params));
           }elseif($this->_params['method'] == 'createCoupon') {
                $this->rest_output($this->createCoupon($this->_params));
           }elseif($this->_params['method'] == 'markCouponExpired') {
                $this->rest_output($this->markCouponExpired($this->_params));
           }elseif($this->_params['method'] == 'addVendorDetails') {
                $this->rest_output($this->addVendorDetails($this->_params));
           } elseif($this->_params['method'] == 'saveCartComment') {
                $this->rest_output($this->saveCartComment($this->_params));
           } elseif($this->_params['method'] == 'bookDeliverySlot') {
                $this->rest_output($this->bookDeliverySlot($this->_params));
           } elseif($this->_params['method'] == 'processZestPayment') {
                $this->rest_output($this->processZestPayment($this->_params));
           } elseif($this->_params['method'] == 'markReturned') {
                $this->rest_output($this->markReturned($this->_params));
           }elseif($this->_params['method'] == 'updateCallBack') {
                $this->rest_output($this->updateCallBack($this->_params));
           }elseif($this->_params['method'] == 'wishListSave') {
                $this->rest_output($this->wishListSave($this->_params));
           }
        }
        catch (Kohana_HTTP_Exception $khe)
        {
            $this->_error($khe);
            return;
        }
        catch (Kohana_Exception $e)
        {
            $this->_error('An internal error has occurred', 500);
            throw $e;
        }
    }

    private function getWarehouseInfo($data)
    {
        $wid = ORM::factory('Warehouseproduct')->where('warehouse_id', '=', $data['wid'])->find_all();
        if (count($wid) != 1) {
            return array('list' => array('status' => 0));
        }
        $winfo = $wid[0]->as_array();
        if ($winfo['product_id']) {
            return array('list' => array('status' => 9, 'pid' => $winfo['product_id']));
        }
        $winfo['maincat'] = ORM::factory('Productcategory', $wid[0]->product_cat)->parentcategory_id;
        return array('status' => 1, 'list' => $winfo);
    }

    private function search($data) {
        $query = $data['query'];
        $cat = -1;//$data['cat'];

        //$hits = Search::instance()->multi_find($query, $cat);
        $hits = Search::instance()->find($query);
        $prdcts = array();
        foreach ($hits as $hit) {
            // Get other attributes of the product
            $doc = $hit->getDocument();
            $pr = ORM::factory('Productitem')->getProductByID($doc->getFieldValue('id'))[0];
            $qualityindex = ORM::factory('Qualityrating')->getQualityRatingByProduct($doc->getFieldValue('id'));
            $pr['qualityindex'] = $qualityindex['qualityindex'];
            array_push($prdcts, $pr);
        }

        return array('list' => array('result' => $prdcts));
    }


    private function getCartItems($data) {
        // Get the cart items
        $cart = ORM::factory('Cart')
            ->where('user_id', '=', $this->_current_user->id)
            ->where('isexpired', '=', 0)
            ->where('ispurchased', '=', 0)
            ->find();

        if (!$cart->loaded()) {
            // No Active cart exists
            return array('list' => array('status' => 0, 'msg' => 'No Active Cart exists'));
        }

        // Get the cart items
        $cartitems = ORM::factory('Cartitem')
            ->where('cart_id', '=', $cart->id)
            ->find_all();

        $prodids = array();
        foreach($cartitems as $item) {
            $it['product_id'] = $item->item_id;
            array_push($prodids, $it);
        }

        //$items = $this->getProductsByID(array('ids' => json_encode($prodids)));
        return array('list' => array('status' => 1, 'cart_id' => $cart->id, 'items' => $prodids));
    }

    private function uploadPhotoStatus($data)
    {
        $wlist = explode("\n", $data['list']);
        foreach($wlist as $w){
            $ps = ORM::factory('Photostatus');
            $ps->shootdate = $data['date'];
            $ps->wid = $w;
            $ps->save();
        }
        return array('list' => array('status' => 1));
    }

    private function getPendingUploads($data)
    {
        if ($data['all'] == '0') {
           $ps = ORM::factory('Photostatus')->where('uploaded','=', '0')->find_all();;
        } else {
            $ps = ORM::factory('Photostatus')->find_all();
        }
        $r = array();
        foreach ($ps as $p) {
            array_push($r, $p->as_array());
        }
        return array('status'=>1, 'list' => $r);
    }

    private function createTicket($data) {
        // Make sure that the order ID is valid
        if ($this->_current_user->id != ORM::factory('Orderdetail', $data['order_id'])->user_id) {
            return array('list' => array('status' => 0));
        }
        $ticket = ORM::factory('Ticket');
        $data['user_id'] = $this->_current_user->id;
        $data['date'] = date('Y-m-d H:i:s');
        $status = $ticket->values($data, array('subject', 'message', 'date', 'user_id', 'order_id'))
                ->create();

        return array('list' => array('status' => 1));

    }
    private function addToCartNoLogin($data)
    {
        $c = ORM::factory('Checkout');
        $c->ip =$_SERVER['REMOTE_ADDR'];
        $c->prodid = $data['prodid'];
        $c->save();
        return array('list' => array('status' => 1));
    }
    private function addToCart($data) {
        // Check if user has cart, if not create a cart
        if (!isset($data['cart_id'])) {
            // Make sure that there is no active cart for the User, if so, use it
            $cart = ORM::factory('Cart')
                ->where('user_id', '=', $this->_current_user->id)
                ->where('isexpired', '=', 0)
                ->where('ispurchased', '=', 0)
                ->find();
            if ($cart->loaded()) {
                $data['cart_id'] = $cart->id;
            } else {
                // Create a cart for the User
                $cart = ORM::factory('Cart');
                $cart->user_id = $this->_current_user->id;
                $cart->save();
                $data['cart_id'] = $cart->id;
            }
        }

        // Add item to Cart if the Item is not there in cart
        foreach (json_decode($data['item_ids']) as $id) {
            $cartitem = ORM::factory('Cartitem')
                ->where('cart_id', '=', $data['cart_id'])
                ->where('item_id', '=', $id)
                ->find();

            if ($cartitem->loaded()) {
                //return array('list' => array('status' => 1, 'msg' => 'Item already in cart', 'cart_id' => $data['cart_id']));
                continue;
            }

            $cartitem = ORM::factory('Cartitem');
            $cartitem->cart_id = $data['cart_id'];
            $cartitem->item_id = $id;
            $cartitem->save();
        }

        return array('list' => array('status' => 1, 'msg' => 'Item added to cart', 'cart_id' => $data['cart_id']));
    }

    private function getUserCart($data)
    {
        $cis = ORM::factory('Cartitem')->order_by('cart_id', 'DESC')->limit(300)->find_all();
        $ops = array();
        foreach ($cis as $ci) {
            $op = array();
            $op['user'] = ORM::factory('User', ORM::factory('Cart', $ci->cart_id)->user_id)->firstname;
            $op['phonenumber'] = ORM::factory('User', ORM::factory('Cart', $ci->cart_id)->user_id)->phonenumber;
            $op['email'] = ORM::factory('User', ORM::factory('Cart', $ci->cart_id)->user_id)->email;
            $op['status'] = (ORM::factory('Cart', $ci->cart_id)->ispurchased == 1) ? "Purchased" : "Yet to Buy";
            $p = ORM::factory('Productitem')->getProductByID($ci->item_id)[0];
	    if ($p['issold'] != 1) {
		    $op['id'] = $p['id'];
		    $op['image'] = $p['images'][0]['image_full'];
		    $op['name'] = $p['product_title'];
		    $op['soldout'] = $p['issold'];
		    $op['timestamp'] = $ci->timestamp;
		    $op['cartid'] = $ci->id;
		    $op['comment'] = $ci->comment;
		    array_push($ops, $op);
	    }
        }

        return array('list' => array('status' => 1, 'cart' => $ops));

    }


    private function saveCartComment($data)
    {
        $ci = ORM::factory('Cartitem', $data['id']);
        $ci->comment = $data['comment'];
        $ci->save();
        return array('list' => array('status' => 1));
    }
    private function wishListSave($data)
    {
        // Kohana::$log->add(Log::DEBUG, "vkt");
        // echo "test";
        // echo strip_tags(Debug::vars($data));
        $wl = ORM::factory('Wishlist', $data['id']);
        $wl->comment = $data['comment'];
        $wl->save();
        // Kohana::$log->add(Log::DEBUG, $data['id']);
        // Kohana::$log->add(Log::DEBUG, $data['comment']);
        return array('list' => array('status' => 1));
    }

    private function getUserWishlist($data)
    {
        $cis = ORM::factory('Wishlist')->order_by('id', 'DESC')->limit(15)->find_all();
        $ops = array();
        foreach ($cis as $ci) {
            $op = array();
            $op['id'] = $ci->id;
            $op['user'] = ORM::factory('User', $ci->user_id)->firstname;
            $op['phonenumber'] = ORM::factory('User', $ci->user_id)->phonenumber;
            $op['email'] = ORM::factory('User', $ci->user_id)->email;
            //$op['status'] = (ORM::factory('Cart', $ci->cart_id)->ispurchased == 1) ? "Purchased" : "Yet to Buy";
            $p = ORM::factory('Productitem')->getProductByID($ci->productitem_id)[0];
            $op['image'] = $p['images'][0]['image_full'];
            $op['name'] = $p['product_title'];
            $op['soldout'] = $p['issold'];
            $op['timestamp'] = $ci->timestamp;
            $op['comment'] = $ci->comment;
            array_push($ops, $op);
        }

        return array('list' => array('status' => 1, 'wish' => $ops));

    }

    private function loadUserSeen($data)
    {
        $cis = ORM::factory('Productseen')->order_by('id', 'DESC')->limit(15)->find_all();
        $ops = array();
        foreach ($cis as $ci) {
            $op = array();
            $u = ORM::factory('User', $ci->user_id);
            if ($u->loaded()) {
                $op['user'] = $u->firstname;
            } else {
                $op['user']  = $ci->user_id;
            }
            $op['phonenumber'] = ORM::factory('User', $ci->user_id)->phonenumber;
            $op['email'] = ORM::factory('User', $ci->user_id)->email;
            //$op['status'] = (ORM::factory('Cart', $ci->cart_id)->ispurchased == 1) ? "Purchased" : "Yet to Buy";
            $p = ORM::factory('Productitem')->getProductByID($ci->product_id)[0];
            $op['id'] = $p['id'];
            $op['image'] = $p['images'][0]['image_full'];
            $op['name'] = $p['product_title'];
            $op['soldout'] = $p['issold'];
            $op['count'] = $ci->count;
            $op['time'] = $ci->time;

            array_push($ops, $op);
        }

        return array('list' => array('status' => 1, 'seen' => $ops));

    }

    private function loadProductHotness($data)
    {
        $cis = ORM::factory('Producthotness')->order_by('count', 'DESC')->limit(15)->find_all();
        $ops = array();
        foreach ($cis as $ci) {
            $op = array();
            $p = ORM::factory('Productitem')->getProductByID($ci->product_id)[0];
            $op['id'] = $p['id'];
            $op['image'] = $p['images'][0]['image_full'];
            $op['name'] = $p['product_title'];
            $op['soldout'] = $p['issold'];
            $op['count'] = $ci->count;
            array_push($ops, $op);
        }

        return array('list' => array('status' => 1, 'hotness' => $ops));

    }

    private function reqCallback($data)
    {
        $cb = ORM::factory('Callback');
        $cb->ip = $_SERVER['REMOTE_ADDR'];
        $cb->number = $data['number'];
        $cb->productid = $data['product'];
        $cb->save();
        return array('list' => array('status' => 1));
    }

    private function removeFromCart($data)
    {
        if (!isset($data['cart_id'])) {
            $cart = ORM::factory('Cart')
                ->where('user_id', '=', $this->_current_user->id)
                ->where('isexpired', '=', 0)
                ->where('ispurchased', '=', 0)
                ->find();
            if ($cart->loaded()) {
                $data['cart_id'] = $cart->id;
            }
        }

        foreach (json_decode($data['item_ids']) as $id) {
            $cartitem = ORM::factory('Cartitem')
                ->where('cart_id', '=', $data['cart_id'])
                ->where('item_id', '=', $id)
                ->find();

            if ($cartitem->loaded()) {
                $cartitem->delete();
            }
        }

        return array('list' => array('status' => 1, 'msg' => 'Items removed from cart'));
    }

    private function addNewAddress($data) {
    	//$data['user_id'] = $this->_current_user->id;
        Kohana::$log->add(Log::DEBUG, "vkt");
        Kohana::$log->add(Log::DEBUG, $data['user_id']);
        $address = ORM::factory('Address')->add_address($data, array('name', 'phonenumber', 'email', 'user_id', 'address_line_one', 'address_line_two', 'city', 'state', 'country', 'pincode', 'landmark'));
        return array('list' => array('status' => 1, 'address_id' => $address->id, 'message' => 'Address added'));
    }

    private function getUserOrders($order)
    {
        $orders = ORM::factory('Orderdetail')->where('user_id', '=', $this->_current_user->id)->order_by('id', 'desc')->find_all();
        $userorders = array();
        foreach($orders as $order) {
            // Get the Order Items
            $items = ORM::factory('Order')->where('orderdetail_id', '=', $order->id)->find_all();
            $orderitems = array();
            foreach ($items as $i) {
                $i = $i->as_array();
                $p = ORM::factory('Productitem')->getProductByID($i['productitems_id'])[0];
                $currentorder = array();
                $currentorder['id'] = $i['id'];
                $currentorder['amount'] = $i['price'];
                if ($i['iscancelled'] == '1') {
                    $currentorder['status'] = "Cancelled";
                    $currentorder['iscancelled'] = $i['iscancelled'];
                } else {
                $currentorder['status'] = $i['isdelivered'] == '1' ?
                "Delivered" : (($i['isshipped'] == '1') ?
                    "Shipped" : (($i['isqcdone'] == '1') ?
                    "Order Confirmed" : "Order Placed"));
                }

                if ($i['iscancelled'] == '1') {
                    $currentorder['date'] = $i['canceldate'];
                } elseif ($i['isdelivered'] == '1') {
                    $currentorder['date'] = $i['deliverydate'];
                } else if ($i['isshipped'] == '1') {
                    $currentorder['date'] = $i['shippeddate'];
                } else if ($i['isqcdone'] == '1') {
                    $currentorder['date'] = $i['qcdonedate'];
                } else {
                    $currentorder['date'] = $order->orderdate;
                }
                $currentorder['ispaid'] = $i['ispaid'];
                $currentorder['productname'] = $p['product_title'];
                $currentorder['productdescription'] = $p['product_description'];
                $currentorder['productimage'] = $p['images'][0]['image_full'];

                array_push($orderitems, $currentorder);
            }

            $items = array('order_id' => $order->id, 'amount' => $order->amount, 'orderdate' => $order->orderdate, 'items' => $orderitems);
            array_push($userorders, $items);
        }

        return array('list' => array('status' => 1, 'orders' => $userorders));
    }

    private function getFinishedOrders($order)
    {
        $type = 'all';
        if (isset($order['type'])) {
            $type = $order['type'];
        }
        $orders = ORM::factory('Orderdetail')->where('isdormant', '!=', 1)->order_by('id', 'desc')->find_all();
        $userorders = array();
        foreach($orders as $order) {
            // Get the Order Items
            $items = ORM::factory('Order')->where('orderdetail_id', '=', $order->id)->order_by('id', 'desc')->find_all();
            $orderitems = array();
            foreach ($items as $i) {
                $i = $i->as_array();
//                $p = ORM::factory('Productitem')->getProductByID($i['productitems_id'])[0];
		$p = ORM::factory('Productitem')->getProductByID($i['productitems_id']);
		if (count($p) ==  0) {
			// This can't happen in normal case but can happen when product is deleted and cookie still has it.
			// Ignore this error for now.
			continue;
		}

		$p = $p[0];

                $currentorder = array();
                $currentorder['id'] = $i['id'];
                $currentorder['amount'] = $i['price'];
                if ($i['iscancelled'] == '1') {
                    $currentorder['status'] = "Cancelled";
                    $currentorder['iscancelled'] = $i['iscancelled'];
                } else {
                $currentorder['status'] = $i['isdelivered'] == '1' ?
                "Delivered" : (($i['isshipped'] == '1') ?
                    "Shipped" : (($i['isqcdone'] == '1') ?
                    "Order Confirmed" : "Order Placed"));
                }
                $currentorder['active'] = 1;
                if ($i['iscancelled'] == '1') {
                    $currentorder['active'] = 0;
                    $currentorder['date'] = $i['canceldate'];
                    $currentorder['action'] = "Cancellation Processed";
                } elseif ($i['isdelivered'] == '1') {
                    $currentorder['active'] = 0;
                    $currentorder['date'] = $i['deliverydate'];
                    $currentorder['action'] = "Order Executed";
                } else if ($i['isshipped'] == '1') {
                    $currentorder['action'] = "Mark Delivered";
                    $currentorder['date'] = $i['shippeddate'];
                } else if ($i['isqcdone'] == '1') {
                    $currentorder['action'] = "Mark Shipped";
                    $currentorder['date'] = $i['qcdonedate'];

                } else {
                    $currentorder['action'] = "Mark Confirmed";
                    $currentorder['date'] = $order->orderdate;
                }
                $currentorder['ispaid'] = $i['ispaid'];
                $currentorder['productname'] = $p['product_title'];
                $currentorder['productdescription'] = $p['product_description'];
                $currentorder['productid'] = $p['productitem_id'];
                $currentorder['productimage'] = $p['images'][0]['image_full'];
                $currentorder['invoice'] = 'invoices/invoice' . '_pocketin_' . $order->id . '_' . $i['id'] . '.pdf';
                $orderactive = 0;
                if ($type == 'active' && $currentorder['active'] == 1) {
                    $orderactive = 1;
                    array_push($orderitems, $currentorder);
                }
                if ($type == 'delivered' && $i['isdelivered'] == '1') {
                    $orderactive = 1;
                    array_push($orderitems, $currentorder);
                }
                 if ($type == 'cancelled' && $i['iscancelled'] == '1') {
                    $orderactive = 1;
                    array_push($orderitems, $currentorder);
                }
            }

            /*$deliveryaddress = ORM::factory('Address', $order->address_id);
            $addr = array();
            if ($deliveryaddress->loaded()) {
                $addr['name'] = $deliveryaddress->name;
                $addr['phonenumber'] = $deliveryaddress->phonenumber;
                $addr['landmark'] = $deliveryaddress->landmark;
                $addr['address'] = $deliveryaddress->address_line_one . ", " . $deliveryaddress->address_line_two;
                $addr['city'] = $deliveryaddress->city;
                $addr['state'] = $deliveryaddress->state;
            }*/

            $addr = json_decode($order->deliveryaddress, true);
            $s = ORM::factory('Deliveryslot')->where('orderid', '=', $order->id)->find();
            $slotbooked = 0;
            $slot = array();
            if ($s->loaded()) {
                $slotbooked = 1;
                $slot = $s->as_array();
            }

            $items = array('order_id' => $order->id, 'coupon' => ORM::factory('Coupon',$order->coupon)->code, 'amount' => $order->amount, 'orderdate' => $order->orderdate, 'comment' => $order->comment, 'items' => $orderitems, 'deliveryaddress' => $addr, 'slot' => $slot, 'slotbooked' => $slotbooked, 'pendingemi' => $order->pendingemi);
            if ($orderactive == 1) {
               array_push($userorders, $items);
            }
        }
        // Show only 10 delivered items
        if ($type != "active") {
           //$userorders = array_slice($userorders, 0, 10);
        }
        return array('list' => array('status' => 1, 'orders' => $userorders));
    }

    private function getZestEMIStatus($data)
    {
        $emistatus = $this->_getZestEMIStatus($data['orderid']);
        return array('list' => array('status' => 1, 'emi' => $emistatus));
    }

    private function updateOrderComment($data)
    {
        $order = ORM::factory('Orderdetail', $data['orderid']);
        $order->comment = $data['comment'];
        $order->save();
        return array('list' => array('status' => 1));
    }

    private function _sendSMS($message, $user_id, $mobile)
    {
        $sender    = "POKTIN";
        $request = Request::factory('http://trans.kapsystem.com/api/v3/index.php')->method('POST')->post(array('method' => 'sms', 'api_key' => KAPSYSTEMSMSAPI, 'to' => $mobile, 'sender' => $sender, 'message' => $message, "custom" => $user_id));
        $response = $request->execute();
        $response = json_decode($response);
        return $response;
    }

    private function _invoice($order, $item, $user, $deliveryaddress) {
        $view = View_Pdf::factory('email/invoice');
        $view->user = $user;
        $view->order = $order;
        $view->deliveryaddress = $deliveryaddress;
        $view->homepage = "https://www.refabd.com/";
        $view->item = $item;
        $pdf= $view->render_as_pdf()->output();
        $invoice = DOCROOT . '/invoices/invoice' . '_pocketin_' . $order->orderdetail_id . '_' . $order->id . '.pdf';
        file_put_contents($invoice, $pdf);
        return $invoice;
    }

    private function email_order($status, $orderid, $userid) {
        $order = ORM::factory('Order', $orderid);
        $orderdetails = ORM::factory('Orderdetail', $order->orderdetail_id);
        $item = ORM::factory('Productitem')->getProductByID($order->productitems_id)[0];
        $user = ORM::factory('User', $userid);
        $deliveryaddress = json_decode($orderdetails->deliveryaddress);
        $view = View::factory('email/order');
        $view->user = $user;
        $view->order = $order;
        $view->deliveryaddress = $deliveryaddress;
        $view->homepage = "https://www.refabd.com/";
        $view->status = $status;
        $view->item = $item;
        $subject = "Your Order has been " . $status . ": OD:" . $order->orderdetail_id ;

        $email = Email::factory()
                    ->subject($subject)
                    ->to($user->email)
                    ->from('sale@refabd.com', "Refabd Team")
                    ->message($view, 'text/html');

        // Generate and Get the invoice if the status is delivered
        if ($status == "Delivered") {
           $invoice = $this->_invoice($order, $item, $user, $deliveryaddress);
           $email->attach_file($invoice);
        }

        try {
             $email->send();
        } catch(Exception $e){
            return 0;
        }

        return 1;
    }

    private function performAction($data)
    {
        $action = $data['action'];
        $nxtaction = "";
        $typeid;
        $date = date('Y-m-d H:i:s');
        $order = ORM::factory('Order', $data['id']);
        $item = ORM::factory('Productitem', $order->productitems_id);
        $wh = ORM::factory('Warehouseproduct')->where('product_id', '=' , $order->productitems_id)->find();
        if (count($wh) != 1) {
            //return array('list' => array('status' => 0));
        }
        //$wh = $wh[0];
        $item_title = substr($item->title, 0, 25) . '...';
        $status = "None";
        if ($action == "Mark Confirmed") {
            $order->isqcdone = 1;
            $typeid = 1;
            $order->qcdonedate = $date;
            $nxtaction = 'Mark Shipped';
            $status = "Order Confirmed";
            $smstext = "Your order for $item_title with ID:$order->orderdetail_id amounting to $order->price has been successfully confirmed. Expect call for delivery slot within 48hrs. Refabd";
        } else if ($action == "Mark Shipped") {
            $order->isshipped = 1;
            $typeid = 2;
            $order->shippeddate = $date;
            $nxtaction = 'Mark Delivered';
            $status = "Shipped";
            $smstext = "Your order for $item_title with ID:$order->orderdetail_id amounting to $order->price has been dispatched. Refabd";
        } else if ($action == "Mark Delivered") {
            // Mark the product as Sold in warehouse page.
            $order->isdelivered = 1;
            $typeid = 3;
            $order->deliverydate = $date;
            //$wh->stage = 7;
            //$wh->save();
            $nxtaction = 'Order Executed';
            $status = "Delivered";
            $smstext = "Your order ID:$order->orderdetail_id delivered successfully. Contact 7022630270 or care@refabd.com for any issues. Thanks for shopping at Refabd.";
        } else if ($action == "Mark Rejected") {
            $order->iscancelled = 1;
            $order->canceldate = $date;
            $wh->stage = 1;
            $wh->save();
            $typeid = 4;
            $nxtaction = "Done";
            $smstext = "We are sorry that we were unable to process your order for $item_title with ID: $order->orderdetail_id due to technical issues. Sorry for the inconvenience. Refabd";
        }
        else if ($action == "Delivery Verified") {
            $deliveryslot = ORM::factory('Deliveryslot')->where('orderid', '=', $data['id'])->find_all();
            foreach ($deliveryslot as $d) {
                $ds = ORM::factory('Deliveryslot', $d->id);
                $ds->active = 0;
                $ds->save();
            }
            return array('list' => array('status' => 1));
        }
        else {
            return array('list' => array('status' => 0));
        }

        $order->save();
        $ud = ORM::factory('Orderdetail', $order->orderdetail_id)->user_id;
        $this->addUserNotification(array('type_id' => $typeid, 'item_id' => $order->productitems_id, 'date' => $date, 'order_id' => $order->orderdetail_id, 'user_id' => $ud));


        $user = ORM::factory('User', $ud);

        $response = $this->_sendSMS($smstext, $ud, $user->phonenumber);
        $status = 0;
        if ($response->status == 'OK') {
           $status = 1;
        }

        // Send the Email
        if ($action != "Mark Rejected") {
            $status = explode(" ", $action)[1];
            $status = $this->email_order($status, $data['id'], $ud);
        } else {
            $status = 1;
        }

        return array('list' => array('status' => $status, 'action' => $nxtaction, 'status' => $status, 'message' => json_encode($response)));
    }

    private function getUserNotifications($data) {
        if ($data['unseen'] == '1') {
            $nots = ORM::factory('Usernotification')
            ->where('user_id', '=', $this->_current_user->id)
            ->where('seen', '=', '0')
            ->order_by('date', 'desc')
            ->find_all();
        } else {
            $nots = ORM::factory('Usernotification')
            ->where('user_id', '=', $this->_current_user->id)
            ->order_by('date', 'desc')
            ->find_all();
        }

        $currentnot = array();
        $notificatons = array();
        foreach($nots as $not) {
            $type = ORM::factory('Usernotificationtype', $not->type_id);
            $pr = ORM::factory('Productitem', $not->item_id);
            $currentnot['type'] = $type->type;
            $currentnot['date'] = $not->date;
            $currentnot['product'] = $pr->title;
            $currentnot['order'] = $not->order_id;

            // Mark as notification is delivered to user
            $not->delivered = 1;
            $not->save();

            array_push($notificatons, $currentnot);
        }

        return array('list' => array('status' => 1, 'nots' => $notificatons));
    }

    private function seenNotification($data) {
        $nots = ORM::factory('Usernotification')
                ->where('user_id', '=', $this->_current_user->id)
                ->where('seen', '=', '0')
                ->where('delivered', '=', '1')
                ->find_all();

        foreach($nots as $not) {
            $not->seen = 1;
            $not->save();
        }

        return array('list' => array('status' =>1));
    }

    private function addUserNotification($data) {
        $not = ORM::factory('Usernotification');
        $not->type_id = $data['type_id'];
        $not->item_id = $data['item_id'];
        $not->user_id = $data['user_id'];
        $not->date = $data['date'];
        $not->order_id = $data['order_id'];

        $not->save();
    }

    private function searchOrders($data) {
        if (isset($data['scat'])) {
            $ret = array();
            if ($data['scat'] == 'Order ID') {
                $data['order_id'] = $data['stext'];
                array_push($ret, $this->getOrderDetails($data)['list']['order']);
            }
            elseif($data['scat'] == 'Mobile') {
                $uid = ORM::factory('User')->where('phonenumber', '=', $data['stext'])->find()->id;
                $orders = ORM::factory('Orderdetail')->where('user_id', '=', $uid)->find_all();
                forEach($orders as $o) {
                    $data['order_id'] = $o->id;
                    array_push($ret, $this->getOrderDetails($data)['list']['order']);
                }
            }
        }

        return array('list' => array('status' => 1, 'orders' => $ret));
    }


    private function searchProducts($data) {
        $ids = array();
        array_push($ids, array('product_id' => $data['product_id']));
        $data['ids'] = json_encode($ids);

        $x = $this->getProductsByID($data);
        $r = $x['list'];

        return array('status' => 1, 'list' =>  $r);
    }

    private function getOrderDetails($data)
    {
        $order = ORM::factory('Orderdetail', $data['order_id']);
        
        // Get the Order Items
        $items = ORM::factory('Order')->where('orderdetail_id', '=', $order->id)->find_all();
        $orderitems = array();
        foreach ($items as $i) {
            $i = $i->as_array();
            $p = ORM::factory('Productitem')->getProductByID($i['productitems_id'])[0];
            $currentorder = array();
            $currentorder['amount'] = $i['price'];
            if ($i['iscancelled'] == '1') {
                    $currentorder['status'] = "Cancelled";
                    $currentorder['iscancelled'] = $i['iscancelled'];
            } else {
            $currentorder['status'] = $i['isdelivered'] == '1' ? "Delivered" : (($i['isshipped'] == '1') ? "Shipped" : (($i['isqcdone'] == '1') ? "Order Confirmed" : "Order Placed"));
            }
            if ($i['iscancelled'] == '1') {
                $currentorder['date'] = $i['canceldate'];
                $currentorder['action'] = "Cancellation Processed";
            } elseif ($i['isdelivered'] == '1') {
                $currentorder['date'] = $i['deliverydate'];
                $currentorder['action'] = "Order Executed";
            } elseif ($i['isshipped'] == '1') {
                 $currentorder['action'] = "Mark Delivered";
                $currentorder['date'] = $i['shippeddate'];
            } elseif($i['isqcdone'] == '1') {
                $currentorder['action'] = "Mark Shipped";
                $currentorder['date'] = $i['qcdonedate'];
            } else {
                $currentorder['date'] = $order->orderdate;
                $currentorder['action'] = "Mark Confirmed";
            }
            if ($i['iscancelled'] == '1') {
                $currentorder['canceldate'] = $i['canceldate'];
            }
            if ($i['isshipped'] == '1') {
                $currentorder['shippeddate'] = $i['shippeddate'];
            }
            if ($i['isdelivered'] == '1') {
                $currentorder['deliverydate'] = $i['deliverydate'];
            }

            $currentorder['ispaid'] = $i['ispaid'];
            $currentorder['productname'] = $p['product_title'];
            $currentorder['productdescription'] = $p['product_description'];
            $currentorder['productimage'] = $p['images'][0]['image_full'];

            array_push($orderitems, $currentorder);
        }

     /*   $deliveryaddress = ORM::factory('Address', $order->address_id);
        $addr = array();
        if ($deliveryaddress->loaded()) {
            $addr['name'] = $deliveryaddress->name;
            $addr['phonenumber'] = $deliveryaddress->phonenumber;
            $addr['landmark'] = $deliveryaddress->landmark;
            $addr['address'] = $deliveryaddress->address_line_one . ", " . $deliveryaddress->address_line_two;
            $addr['city'] = $deliveryaddress->city;
            $addr['state'] = $deliveryaddress->state;
        }*/

     //   $addr = json_decode($order->deliveryaddress, true);
        $addr = json_decode($order->deliveryaddress);
        $username = ORM::factory('User', $order->user_id)->firstname;

        $s = ORM::factory('Deliveryslot')->where('orderid', '=', $order->id)->find();
            $slotbooked = 0;
            $slot = array();
            if ($s->loaded()) {
                $slotbooked = 1;
                $slot = $s->as_array();
            }



        $items = array('id' => $order->id, 'order_id' => $order->id, 'amount' => $order->amount, 'orderdate' => $order->orderdate, 'items' => $orderitems, 'itemcount' => count($orderitems), 'deliveryaddress' => $addr, 'user_id' => $order->user_id, 'username' => $username, 'slot' => $slot, 'comment' => $order->comment, 'slotbooked' => $slotbooked);

        return array('list' => array('status' => 1, 'order' => $items));
    }




    private function addParentCategory($data){
        $parentcategory = ORM::factory('Parentcategory')->add_category($data, array('name'));
        return array('status' => 1, 'id' => $parentcategory->id, 'message' => 'Category created Successfully');
    }

    // This gets the collection to be displayed in the home page
    private function getCollections($data)
    {
        //$count = intval($data['count']);
        // fridge, a/c, purifier, washing machine
        //json_encode(ids => array({'product_id' => 3}, {'product_id' => 3}))
        //return array('status' => 1, 'list' => $rows->as_array());

    }
    private function getParentCategories($data){
        $parentcategories = ORM::factory('Parentcategory')->find_all();
        $cats = array();
        foreach($parentcategories as $cat) {
            array_push($cats, $cat->as_array());
        }
        return array('status' => 1, 'list' => $cats);
        return array('status' => 1, 'list' => array(array('name' => 'Refrigerator', 'shortdescription' => "Very good Refrigerators", 'image' => "assets/images/image1.png", "href" => "item"),
            array('name' => 'Washing Machine', 'shortdescription' => "Very good Refrigerators", 'image' => "assets/images/image2.png", "href" => "item"),
            array('name' => 'Air Conditioner', 'shortdescription' => "Very good Air Cond", 'image' => "assets/images/image3.png", "href" => "item"),
            array('name' => 'Air Purifier', 'shortdescription' => "Very good purifier", 'image' => "assets/images/image4.png", "href" => "item")
        ));
    }

    private function addProductCategory($data){
        $productcategory = ORM::factory('Productcategory')->add_category($data, array('name', 'parentcategory_id'));
        return array('status' => 1, 'id' => $productcategory->id, 'message' => 'Category created');
    }

    private function getProductCategories($data){
        $productcategories = ORM::factory('Productcategory')->find_all();
        $items = array();
        foreach ($productcategories as $item) {
            array_push($items, $item->as_array());

        }
        return array('status' => 1, 'list' => $items);
    }


    private function getProducttCategoriesByParentID($data){
        $productcategories = ORM::factory('Productcategory')->where('parentcategory_id', '=',  $data['parentcat_id'])->find_all();
        $items = array();
        foreach ($productcategories as $item) {
            array_push($items, $item->as_array());

        }
        return array('status' => 1, 'list' => $items);
    }

    private function addToWishlist($data){
        $data['user_id'] = $this->_current_user->id;
        if ($data['addorremove'] == 0) {
            $w = ORM::factory('Wishlist')->where('user_id', '=', $this->_current_user->id)
            ->where('productitem_id', '=', $data['productitem_id'])->find();
            if ($w->loaded()) {
                $w->delete();
            }
        } else {
            $wishlist = ORM::factory('Wishlist')->add_wishlist($data, array('user_id', 'productitem_id'));
        }
        return array('list' => array('status' => 1));
    }
    private function removeFromWishlist($data)
    {
        $wish = ORM::factory('Wishlist', $data['id']);
        if ($wish->loaded()) {
            $wish->delete();
        }

        return array('list' => array('status'=> 1));
    }

    private function HideProduct($data) {
        $p = ORM::factory('Productitem', $data['id']);
        $p->hidden = 1;
        $p->save();
        return array('list' => array('status'=> 1));
    }

    private function UnHideProduct($data) {
        $p = ORM::factory('Productitem', $data['id']);
        $p->hidden = 0;
        $p->save();
        return array('list' => array('status'=> 1));
    }



    private function getProductsByID($data) {
        $log = 0;
        /*if (isset($data['log'])) {
            if ($data['log'] == '1') {
                $log = 1;
            }
        }*/
        $ids = json_decode($data['ids'], 1);
        $opArray = array();
        // Kohana::$log->add(Log::DEBUG, 'VKT: IDs for debugging' + json_encode($ids));
        foreach($ids as $id) {
            $product = ORM::factory('Productitem')->getProductByID($id['product_id']);
            if (count($product) ==  0) {
                // This can't happen in normal case but can happen when product is deleted and cookie still has it.
                // Ignore this error for now.
                continue;
            }

            $product = $product[0];
            //echo strip_tags(Debug::vars($product));
            $qualityindex = ORM::factory('Qualityrating')->getQualityRatingByProduct($product['productitem_id']);
            $product['qualityindex'] = $qualityindex['qualityindex'];
            $wish = ORM::factory('Wishlist')->where('user_id', '=', $this->_current_user->id)->where('productitem_id', '=', $product['productitem_id'])->find();
            if ($wish->loaded()) {
               $product['inwishlist'] = 1;
            } else {
                $product['inwishlist'] = 0;
            }

            $cat = ORM::factory('Productcategory', $product['catid']);
            $product['catid'] = $cat->id;
            $product['catname'] = $cat->name;
            $product['image_full'] = $product['images'][0]['image_full'];
            if ($log) {
                // Add product seen entry in database
                Kohana::$log->add(Log::DEBUG, 'productseen ' .  $product['product_title'] . ' by ' . $this->_current_user->firstname);
            }

            //$product['zest'] = $this->_getZestEMIValue($product);

            array_push($opArray, $product);
        }

        return array('status' => 1, 'list' => $opArray);
    }

    private function getModel($data)
    {
        $model = ORM::factory('Product')->where('modelnumber', '=', $data['modelnumber'])->find();
        if ($model->loaded()) {
            return array('list' => array('status' => 1, 'id'=>$model->id, 'number'=> $data['modelnumber'],'name' => $model->name, 'shortdescription' => $model->shortdescription, 'description' => $model->description, 'mrp' => $model->mrp));
        }

        return array('list' => array('status' => 0));
    }

    private function CreateOrModifyModel($data)
    {
        $model = ORM::factory('Product')->where('modelnumber', '=', $data['modelnumber'])->find();
        if (!$model->loaded()) {
            $model = ORM::factory('Product')->add_product($data, array('name', 'description', 'shortdescription', 'mrp', 'modelnumber'));
        } else {
            $model = $model->update_product($data, array('name', 'description', 'shortdescription', 'mrp', 'modelnumber'));
        }

        return array('list' => array('status' => 1, 'modelid' => $model->id));
    }

    private function addFilterTypes($data){
        $filtertype = ORM::factory('Filtertype')->add_filtertype($data, array('productcategory_id', 'name'));

        return array('status' => 1, 'id' => $filtertype->id, 'message' => 'Filtertype added');
    }

    private function addFilter($data){
        $filter = ORM::factory('Filter')->add_filter($data, array('filtertype_id', 'name'));

        return array('status' => 1, 'id' => $filter->id, 'message' => 'Filter added');
    }

    private function addTechnicalSpec($data){
        $technicalspec = ORM::factory('Technicalspec')->add_technicalspec($data, array('name', 'productcategory_id'));
        return array('status' => 1, 'id' => $technicalspec->id, 'message' => 'The technical specification is added');
    }

    private function addQualityIndex($data){
        $qualityIndex = ORM::factory('Qualityindex')->add_qualityindex($data, array('name', 'productcategory_id'));

        return array('status' => 1, 'id' => $qualityIndex->id, 'message' => 'Quality Index added');
    }

    private function updateProductItem($data) {
        $productitem = ORM::factory('Productitem')->where('id', '=', $data['productitem_id'])->find();
        $productitem->update_productitem($data, array('title','description', 'product_id', 'mrp', 'isapproved', 'saleprice', 'longdescription', 'technicalspec', 'conditionspec', 'isfactory'));

        return array('status' => 1, 'message' => 'The product has been Modified');

    }

    private function markSold($data) {
        $p = ORM::factory('Productitem', $data['id']);
        $p->issold = 1;
        $p->save();
        return array('list' => array('status' => 1));
    }

    private function markUnSold($data) {
        $p = ORM::factory('Productitem', $data['id']);
        $p->issold = 0;
        $p->save();
        return array('list' => array('status' => 1));
    }

    private function markAsApproved($data) {
        $data['product_id'] = 1;
        $productitem = ORM::factory('Productitem')->where('id', '=', $data['productitem_id'])->find();
        $wp = ORM::factory('Warehouseproduct')->where('warehouse_id', '=', $data['warehouseid'])->find();
        if (!$wp->loaded()){
            return array( 'list' => array('status' => 9));
        } else {
            $wp->product_id = $productitem->id;
            $wp->saleprice = $productitem->saleprice;
            $wp->stage = 6; // RTS
            $wp->save();

            $photo = ORM::factory('Photostatus')->where('wid', '=', $data['warehouseid'])->find_all();
	    if (count($photo) != 0) {
		    $photo = $photo[0];
		    $photo->uploaded = 1;
		    $photo->save();
	    }
        }
        $data['isapproved'] = 1;
        $productitem->update_productitem($data, array('title','description', 'product_id', 'mrp', 'isapproved', 'saleprice', 'longdescription', 'technicalspec', 'conditionspec', 'isfactory', 'warehouseid'));


        // Index the product
        $p = ORM::factory('Productitem', $data['productitem_id']);
        Search::instance()->add($p);

        // TODO: check the images and crop/resize properly.
        $images = ORM::factory('Image')->where('productitem_id', '=', $data['productitem_id'])->find_all();
        foreach($images as $im) {
            // Move the image to products/normal
            $piwater = DOCROOT. "assets/images/watermark.png";
            $pending = DOCROOT."assets/images/dealerpending/";
            $zoom = DOCROOT."assets/images/products/zoom/";
            $normal = DOCROOT."assets/images/products/normal/";
            $thumbnail = DOCROOT."assets/images/products/thumbnail/";
            $image  = $im->name;

            /*$om = Image::factory($pending.$image);
            $watermark = Image::factory($piwater);
            $om->watermark($watermark, 0, $om->height - $watermark->height, 30);
            $om->save();*/
            copy($pending.$image, $zoom.$image);
            copy($pending.$image, $normal.$image);
            copy($pending.$image, $thumbnail.$image);
            Image::factory($thumbnail.$image)
                ->resize(NULL, 173)->save($thumbnail.$image);
            Image::factory($normal.$image)
                ->resize(NULL, 450)->save($normal.$image);
            Image::factory($zoom.$image)
                ->resize(NULL, 1100)->save($zoom.$image);

            unlink($pending.$image);
        }
        $data['user_id'] = $this->_current_user->id;
        $activitylog = ORM::factory('Activitylog')->add_log(array('object_id' => $productitem->id, 'type' => 'APPROVED_PRODUCT', 'user_id' => $data['user_id']), array('object_id', 'type', 'user_id'));

        return array('status' => 1, 'message' => 'The product has been approved');
    }

    private function cancelOrder($data)
    {
        $order_id = $data['order_id'];
        $item_id = $data['item_id'];

        // Cancel the item
        $item = ORM::factory('Order', $item_id);
        $item->iscancelled = 1;
        $item->canceldate = date('Y-m-d H:i:s');
        $item->save();

        $p = ORM::factory('Productitem', $item->productitems_id);
        $p->issold = 0;
        $p->save();

        // Check if all items are cancelled
        $items = ORM::factory('Order')->where('orderdetail_id', '=', $order_id)->where('iscancelled', '=', '0')->find_all();

        $order = ORM::factory('Orderdetail', $order_id);
        $order->amount = $order->amount - $item->price;
        $order->save();
        if (count($items) == 0) {
            $order = ORM::factory('Orderdetail', $order_id);
            $order->iscancelled = 1;
            $order->canceldate = date('Y-m-d H:i:s');
            $order->save();
        }

        // Send SMS
        $ud = ORM::factory('Orderdetail', $order_id)->user_id;
        $user = ORM::factory('User', $ud);
        $smstext = "Hi, Your order for $p->title with ID: $order_id amounting to $item->price has been cancelled. Check email for more details. Thanks for shopping at Pocketin";
        $response = $this->_sendSMS($smstext, $ud, $user->phonenumber);
        $status = 0;
        if ($response->status == 'OK') {
           $status = 1;
        }

        return array('list' => array('status' => $status
            , 'date' => $item->canceldate));
    }




    private function addProductByDealer($data) {
        $pid = $data['pid'];
        $data['title'] = $data['name'];
        $data['user_id'] = $this->_current_user->id;
        $qualityindexes = json_decode($data['qualityindexes'], true);
        $directory = DOCROOT."assets/images/dealerpending/prod_$pid";
        $pardir = DOCROOT."assets/images/dealerpending";
        $images = scandir($directory);
        $stockcount = intval($data['stockcount']);

        for($count = 1; $count <= $data['stockcount']; $count++) {
            $productitem = ORM::factory('Productitem')->add_productitem($data, array('title', 'description', 'saleprice', 'user_id', 'manufacturingyear', 'productcategory_id', 'isfactory', 'brandnew', 'warehouseid', 'compressorwarranty', 'mrp', 'longdescription', 'technicalspec', 'conditionspec'));

            foreach ($qualityindexes as $qualityindex) {
                $qualityrating = ORM::factory('Qualityrating')->add_qualityrating(array('value' => $qualityindex['value'], 'qualityindex_id' => $qualityindex['index'], 'productitem_id' => $productitem->id), array('value', 'qualityindex_id', 'productitem_id'));
            }

            // Get the uploaded images, rename them and add images in database
            $i = 1;
            $def = 1;
            foreach ($images as $image) {
                // Rename the file
                if (substr($image,0,1) == '.') {
                    continue;
                }
                $ext = explode(".", $image);
                $extension = strtolower(array_pop($ext));
                $name = $productitem->id . "_" . $i . "." . $extension;
                $imagename = $directory . "/" . $image;
                $newname = 	$pardir . "/" . $productitem->id . "_" . $i . "." . $extension;
                if ($count < $stockcount) {
                   copy($imagename, $newname);
                } else {
                    rename($imagename, $newname);
                }
                // Add to database
                //TODO: create thumbnail and add to db
                ORM::factory('Image')->add_image(array("name" => $name, "description" => "Image $i", "productitem_id" => $productitem->id, "isdefault" => $def), array('name', 'description', 'productitem_id', 'isdefault'));
                $i++;
                $def = 0;
            }
        }

        // Remove the directory
        system('rm -rf ' . escapeshellarg($directory), $retval);

        // Make as product added
        $activitylog = ORM::factory('Activitylog', $pid)
        ->update_log(array('object_id' => $productitem->id, 'type' => 'DEALER_ADD_PRODUCT_COMPLETE', 'user_id' => $data['user_id']), array('type', 'user_id', 'object_id'));

        return array('list' => array('status' => 1, 'productitem_id' => $productitem->id, 'message' => 'The product has been created successfully'));
    }

    private function addProduct($data){
        $product = ORM::factory('Product')->where('modelnumber', '=', $data['modelnumber'])->find();
        if(!$product->loaded()) {
            $product = ORM::factory('Product')->add_product($data, array('name', 'description', 'origimage', 'mrp', 'modelnumber', 'productcategory_id'));
        }
        $data['description'] = $data['vendordescription'];
        $data['product_id'] = $product->id;
        $data['user_id'] = $this->_current_user->id;
        $productitem = ORM::factory('Productitem')->add_productitem($data, array('title', 'description', 'saleprice', 'vendorprice', 'user_id', 'product_id', 'manufacturingyear', 'warranty'));

        $filters = json_decode($data['filters'], true);
        foreach ($filters as $filter) {
            $productitemfilter = ORM::factory('productitemfilter')->add_productitemfilter(array('filter_id' => $filter['id'], 'productitem_id' => $productitem->id), array('filter_id', 'productitem_id'));
        }
        $techspecs = json_decode($data['techspecs'], true);
        foreach ($techspecs  as $techspec) {
            $technicalspecvalue = ORM::factory('technicalspecvalue')->add_technicalspecvalue(array('value' => $techspec['value'], 'technicalspec_id' => $techspec['technicalspec_id'], 'productitem_id' => $productitem->id), array('value', 'technicalspec_id', 'productitem_id'));
        }
        $qualityindexes = json_decode($data['qualityindexes'], true);
        foreach ($qualityindexes as $qualityindex) {
            $qualityrating = ORM::factory('Qualityrating')->add_qualityrating(array('value' => $qualityindex['value'], 'qualityindex_id' => $qualityindex['qualityindex_id'], 'productitem_id' => $productitem->id), array('value', 'qualityindex_id', 'productitem_id'));
        }
        return array('status' => 1, 'product_id' => $product->id, 'productitem_id' => $productitem->id, 'message' => 'The product has been created successfully');
    }

    private function orderProducts($data) {
        $userloggedin = Auth::instance()->get_user();
        if (!isset($data['ispaid'])) {
            $data['ispaid'] = 0;
        }

        $o1 = $data['offer']/100;
        $o2 = 0;
        $offer = $data['offer'];
        $basket = array();
        if (isset($data['coupon']) && $data['coupon'] != 0) {
            $c = ORM::factory('Coupon', $data['coupon']);
            if ($c->ispercentage) {
                $offer = $offer +  $c->value;
            }
             // Expire the coupon
            if ($c->userid == $userloggedin->id){
                $c->isexpired = 1;
            }
            $o2= $c->value/100;
            $c->ntimes = $c->ntimes + 1;
            $c->save();
        }
        //$offer = (100 - intval($data['offer']))/100;
        $offer = (100 - intval($offer))/100;

        $data['user_id'] = $this->_current_user->id;
        if ($data['buyfromcart'] == "true") {
            // Get the Active Cart
            $cart = ORM::factory('Cart')
                ->where('user_id', '=', $data['user_id'])
                ->where('isexpired', '=', 0)
                ->where('ispurchased', '=', 0)
                ->find();
            if ($cart->loaded()) {
                $data['cart_id'] = $cart->id;
            } else {
                return array('list' => array('status' => 0, 'msg' => 'No Cart found for the User'));
            }
        } else {
            // Create a Cart for the User and add the item in the cart
            $cart = ORM::factory('Cart');
            $cart->user_id = $data['user_id'];
            $cart->save();
            $data['cart_id'] = $cart->id;

            $cartitem = ORM::factory('Cartitem');
            $cartitem->cart_id = $data['cart_id'];
            $productitems = json_decode($data['productitems']);
            foreach ($productitems as $productitem) {
                $cartitem->item_id = $productitem->id;
                $cartitem->cart_id = $cart->id;
                $cartitem->save();
            }
        }
        $data['user_id'] = $this->_current_user->id;
        $deliveryadd = ORM::factory('Address', $data['address_id'])->as_array();
        $data['deliveryaddress'] = json_encode($deliveryadd);
        $data['orderdate'] = date('Y-m-d H:i:s');
        $data['coupon'] = ORM::factory('Coupon', $data['coupon'])->id;
        $orderdetail = ORM::factory('Orderdetail')->add_orderdetail($data, array('orderdate', 'ispaid', 'user_id', 'deliveryaddress', 'cart_id','coupon', 'pendingemi'));
        $data['orderdetail_id'] = $orderdetail->id;
        $productitems = json_decode($data['productitems'], true);
        $orderIDs = array();
        $price = 0;
        foreach ($productitems as $productitem) {
            $productitem = ORM::factory('Productitem')->where('id', '=', $productitem['id'])->find();
            $productitem->update_productitem(array('issold' => 1), array('issold'));
            $data['productitems_id'] = $productitem->id;
            $saleprice = intval($productitem->saleprice) * $offer;
            $saleprice = $saleprice  +  $productitem->saleprice *$o1*$o2;
            $price = $price + $saleprice;
            $data['price'] = $saleprice;
            $order = ORM::factory('Order')->add_order($data, array('productitems_id', 'orderdetail_id', 'price', 'ispaid'));
            array_push($orderIDs, array('id' => $order->id));

            // Frame basket for zestmoney
            if (isset($data['pendingemi']) && $data['pendingemi']  == 1) {
                $b = array();
                $b['Id'] = $productitem->id;
                $b['Description'] = $productitem->title;
                $b['Quantity'] = 1;
                $b['TotalPrice'] = floor($data['price']);
                array_push($basket, $b);
            }

        }

        $data['amount'] = $price;
        $orderdetail->update_orderdetail($data, array('amount'));

        // Deactivate the Cart
        $cart = ORM::factory('Cart', $data['cart_id']);
        $cart->ispurchased = 1;
        $cart->save();

        return array('list' => array('status' => 1, 'order_id' =>  $orderdetail->id,'message' => 'The order is successfully placed', 'basket' => $basket));
    }

    private function CapturePaymentAndOrderProducts($data) {
        $data['ispaid'] = 1;
        $payid = $data['paymentid'];
        $amount = intval($data['amount']);
        $api_key = "rzp_live_koMmDyJJsb3l71";
        $api_secret = "RcC9Da2kdDGccWNm06GcBkLo";

        //$api_key = "rzp_test_OIocG7cUvwrV2z";
        //$api_secret = "c2Ays0aAOL6lCrrPMpXuYqJw";

        //$razurl = "https://" . $api_key . ":" . $api_secret . "@api.razorpay.com/" . $payid . "/" . 'capture';
        //$request = Request::factory($razurl)->method('POST')->post(array('amount' => $amount));
        //$response = $request->execute();
        //$ret = json_decode($response);

        $api = new Api($api_key, $api_secret);
        $ret = $api->payment->fetch($payid)->capture(array('amount'=>$amount)); // Capture the payment
        //TODO: check the return type

        return $this->orderProducts($data);

        //$status['list']['payment'] = $ret;

        //return $status;
    }

    private function initiateDelivery($data){
        $today = new DateTime();
        $today->add(new DateInterval('P05D'));
        $orders = ORM::factory('Order')->where('orderdetail_id', '=', $data['orderdetail_id'])->find_all();
        foreach ($orders as $order) {
            $order->update_order(array('deliverydate' => $today->format('Y-m-d')), array('deliverydate'));
        }
        array('status' => 1, 'message' => 'Delivery initiated');
    }

    private function markDelivered($data){
        $today = new DateTime();
        $today->add(new DateInterval('P05D'));
        $orders = ORM::factory('Order')->where('orderdetail_id', '=', $data['orderdetail_id'])->find_all();
        foreach ($orders as $order) {
            $order->update_order(array('isdelivered' => 1, 'ispaid' => 1, 'deliverycost' => $data['deliverycost']), array('isdelivered', 'ispaid', 'deliverycost'));
        }
        array('status' => 1, 'message' => 'Delivery initiated');
    }

    private function markShipped($data){
        $today = new DateTime();
        $orders = ORM::factory('Order')->where('orderdetail_id', '=', $data['orderdetail_id'])->find_all();
        foreach ($orders as $order) {
            $order->update_order(array('shippeddate' => $today->format('Y-m-d'), 'isshipped' => 1), array('shippeddate', 'isshipped'));
        }
        array('status' => 1, 'message' => 'Order shipped');
    }

    private function getProductsByDealer($data){
        $offset = 0;
        $limit = 100;
        $filters = null;
        if(isset($data['filters'])){
            $filters = json_decode($data['filters'], true);
        }
        $products = ORM::factory('Product')->getProductsByDealer($data['dealer_id'], $offset, $limit, $filters);

        $i = 0;
        foreach ($products as $product) {
            $qualityindex = ORM::factory('Qualityrating')->getQualityRatingByProduct($product['productitem_id']);
            $products[$i]['qualityindex'] = $qualityindex['qualityindex'];
        }

        return array('status' => 1, 'list' => $products);
    }

    private function getProductsByQuality($data){
        $offset = ($data['page_no'] - 1)*$data['limit'];
        $filters = null;
        if(isset($data['filters'])){
            $filters = json_decode($data['filters'], true);
        }
        $products = ORM::factory('Productitem')->getProductItemByQuality($data['productcategory_id'], $offset, $data['limit'], $data['sort'], $filters);
        $i = 0;
        foreach ($products as $product) {
            $qualityindex = ORM::factory('Qualityrating')->getQualityRatingByProduct($product['productitem_id']);
            $products[$i]['qualityindex'] = $qualityindex['qualityindex'];
        }

        $totalpages = 1;
        if(count($products) > $data['limit']){
            $totalpages = count($products)/$data['limit'];
        }
        return array('status' => 1, 'products' => $products, 'paging' => array('totalpages' => $totalpages, 'page_no' => $data['page_no'], 'limit' => $data['limit'], 'next_page' => $data['page_no']+ 1, 'previous_page' => $data['page_no'] - 1));
    }

    private function getPendingProductsByDealer($data)
    {
        $products = ORM::factory('Product')->getPAProductsByDealer($data['dealer_id']);
        $i = 0;
        foreach ($products as $product) {
            $qualityindex = ORM::factory('Qualityrating')->getQualityRatingByProduct($product['productitem_id']);
            $products[$i]['qualityindex'] = $qualityindex['qualityindex'];
            $i++;
        }
        return array('status' => 1, 'list' => $products);
    }

    private function resizeImages($data) {
        $images = ORM::factory('Image')->where('productitem_id', '=', $data['id'])->find_all();
        if (count($images) == 0) {
            return array('list'=>(array('status' => 0)));
        }
        foreach($images as $im) {
            // Move the image to products/normal
            $zoom = DOCROOT."assets/images/products/zoom/";
            $normal = DOCROOT."assets/images/products/normal/";
            $thumbnail = DOCROOT."assets/images/products/thumbnail/";
            $image  = $im->name;
            Image::factory($thumbnail.$image)
                ->resize(NULL, 173)->save($thumbnail.$image);
            Image::factory($normal.$image)
                ->resize(NULL, 450)->save($normal.$image);
            Image::factory($zoom.$image)
                ->resize(NULL, 1100)->save($zoom.$image);
        }
        return array('list'=>(array('status' => 1)));

    }

    private function getAllProducts($data){
        if (isset($data['custom'])) {
            $custom = $data['custom'];
        } else {
            $custom = "none";
        }
        $offset = ($data['page_no'] - 1)*$data['limit'];
        $filters = null;
        if(isset($data['filters'])){
            $filters = json_decode($data['filters'], true);
        }
        $products = null;

        if ($custom == "pendingapproval") {
            $products = ORM::factory('Product')->getPAProducts($offset, $data['limit'], $filters);
        } elseif ($custom == "pendingdelivery") {
            $products = ORM::factory('Product')->getPDProducts($offset, $data['limit'], $filters);
        } elseif ($custom == "pendingpayment") {
            $products = ORM::factory('Product')->getPPProducts($offset, $data['limit'], $filters);
        } elseif ($custom == "soldandpaid") {
            $products = ORM::factory('Product')->getSPProducts($offset, $data['limit'], $filters);
        } else {
            $products = ORM::factory('Product')->getAllProducts($offset, $data['limit'], $filters);
        }

        $i = 0;
        foreach ($products as $product) {
            $qualityindex = ORM::factory('Qualityrating')->getQualityRatingByProduct($product['productitem_id']);
            $products[$i]['qualityindex'] = $qualityindex['qualityindex'];
            $i++;
        }
        $totalpages = 1;
        if(count($products) > $data['limit']){
            $totalpages = count($products)/$data['limit'];
        }
        return array('status' => 1, 'list' => $products, 'paging' => array('totalpages' => $totalpages, 'page_no' => $data['page_no'], 'limit' => $data['limit'], 'next_page' => $data['page_no']+ 1, 'previous_page' => $data['page_no'] - 1));
    }


    private function getSpecialProducts($data) {
        $limit = 5;
        $offset = 0;
        $type = $data['type'];
        if ($type == "Quality") {
            $products = array();
            $qualityindex = DB::query(Database::SELECT, "SELECT  qualityratings.productitem_id, AVG(value) as qualityindex FROM qualityratings,productitems where productitems.id = qualityratings.productitem_id and productitems.issold = 0 and  productitems.isapproved = 1 group by qualityratings.productitem_id order by qualityindex DESC limit 10 offset $offset")->execute()->as_array();
            $products = array();

            foreach($qualityindex as $p) {
                $id = $p['productitem_id'];
                $product = DB::select('productitems.mrp', array('productitems.title', 'product_title'), array('productitems.description', 'product_description'), array('productitems.id', 'productitem_id'), 'productitems.saleprice', array('images.name', 'image_full'), 'productitems.isfactory')
                ->from('productitems')
                ->where('productitems.id', '=', $id)
                ->where('productitems.isapproved', '=', 1)
                ->where('productitems.issold', '=', 0)
                ->where('productitems.hidden', '=', 0)
                ->join('images', 'left')->on('images.productitem_id', '=', 'productitems.id')->on('images.isdefault', '=', DB::expr('1'))
                ->execute()->as_array();
                if (count($product) > 0) {
                   $product[0]['qualityindex'] = $p['qualityindex'];
                   $wish = ORM::factory('Wishlist')->where('user_id', '=', $this->_current_user->id)->where('productitem_id', '=', $product[0]['productitem_id'])->find();
                    if ($wish->loaded()) {
                        $product[0]['inwishlist'] = 1;
                    } else {
                        $product[0]['inwishlist'] = 0;
                    }
                   array_push($products, $product[0]);
                }
            }
        } else if ($type == "New") {
            $ps = DB::select('productitems.mrp', array('productitems.title', 'product_title'), array('productitems.description', 'product_description'), array('productitems.id', 'productitem_id'), 'productitems.saleprice', 'productitems.isfactory', array('images.name', 'image_full'))
            ->from('productitems')
            ->where('productitems.isapproved', '=', 1)
            ->where('productitems.issold', '=', 0)
            ->where('productitems.hidden', '=', 0)
            ->join('images', 'left')->on('images.productitem_id', '=', 'productitems.id')->on('images.isdefault', '=', DB::expr('1'))
            ->order_by('productitems.id', 'DESC')->limit($limit)->offset($offset)->execute()->as_array();
            $products = array();
            foreach($ps as $p) {
                $qualityindex = ORM::factory('Qualityrating')->getQualityRatingByProduct($p['productitem_id']);
                $p['qualityindex'] = $qualityindex['qualityindex'];
                                   $wish = ORM::factory('Wishlist')->where('user_id', '=', $this->_current_user->id)->where('productitem_id', '=', $p['productitem_id'])->find();
                    if ($wish->loaded()) {
                        $p['inwishlist'] = 1;
                    } else {
                        $p['inwishlist'] = 0;
                    }
                array_push($products, $p);
            }
        } else if ($type == "Sold") {
            $ps = DB::select('productitems.mrp', array('productitems.title', 'product_title'), array('productitems.description', 'product_description'), array('productitems.id', 'productitem_id'), 'productitems.saleprice', 'productitems.isfactory', array('images.name', 'image_full'))
            ->from('productitems')
            ->where('productitems.isapproved', '=', 1)
            ->where('productitems.issold', '=', 1)
            ->where('productitems.hidden', '=', 0)
            ->join('images', 'left')->on('images.productitem_id', '=', 'productitems.id')->on('images.isdefault', '=', DB::expr('1'))
            ->order_by('productitems.timestamp', 'DESC')->limit($limit)->offset($offset)->execute()->as_array();
            $products = array();
            foreach($ps as $p) {
                $qualityindex = ORM::factory('Qualityrating')->getQualityRatingByProduct($p['productitem_id']);
                $p['qualityindex'] = $qualityindex['qualityindex'];
                                   $wish = ORM::factory('Wishlist')->where('user_id', '=', $this->_current_user->id)->where('productitem_id', '=', $p['productitem_id'])->find();
                    if ($wish->loaded()) {
                        $p['inwishlist'] = 1;
                    } else {
                        $p['inwishlist'] = 0;
                    }
                array_push($products, $p);
            }
        } else if ($type == "Recommended") {
           // Get Limit/2 products from same product category
            $products = array();
            $category_id = $data['catid'];
            $currentproduct = $data['currentproduct'];
            $orig_cat = $category_id;
            $qualityproducts = DB::query(Database::SELECT, "SELECT  qualityratings.productitem_id, AVG(value) as qualityindex FROM qualityratings, productitems where productitems.`productcategory_id` = $category_id and productitems.id = qualityratings.productitem_id group by qualityratings.productitem_id order by qualityindex DESC limit $limit offset $offset")->execute()->as_array();

            // Get products from sibbling categories
            $parent_cat_id = ORM::factory('Parentcategory', ORM::factory('Productcategory', $category_id)->parentcategory_id);
            $sibbling_cats = ORM::factory('Productcategory')->where('parentcategory_id', '=', $parent_cat_id)->find_all();
            foreach ($sibbling_cats as $cat) {
                $category_id = $cat->id;
                if ($category_id == $orig_cat) {
                    continue;
                }
                $tp = DB::query(Database::SELECT, "SELECT  qualityratings.productitem_id, AVG(value) as qualityindex FROM qualityratings, productitems where productitems.`productcategory_id` = $category_id and productitems.id = qualityratings.productitem_id group by qualityratings.productitem_id order by qualityindex DESC limit $limit offset $offset")
                ->execute()->as_array();
                if (count($tp) > 0) {
                    foreach ($tp as $p) {
                        array_push($qualityproducts, $p);
                    }
                }
            }

            shuffle($qualityproducts);
            $count = 0;

            foreach($qualityproducts as $p) {
                $id = $p['productitem_id'];

                if ($id == $currentproduct) {
                    continue;
                }

                $product = DB::select('productitems.mrp', array('productitems.title', 'product_title'), array('productitems.description', 'product_description'), array('productitems.id', 'productitem_id'), array('productitems.productcategory_id', 'productcategory_id'),'productitems.saleprice', 'productitems.isfactory', array('images.name', 'image_full'))
                ->from('productitems')
                ->where('productitems.id', '=', $id)
                ->where('productitems.isapproved', '=', 1)
                ->where('productitems.issold', '=', 0)
                ->where('productitems.hidden', '=', 0)
                ->join('images', 'left')->on('images.productitem_id', '=', 'productitems.id')->on('images.isdefault', '=', DB::expr('1'))
                ->execute()->as_array();

                if (count($product) > 0) {
                   $product[0]['qualityindex'] = $p['qualityindex'];
                   $wish = ORM::factory('Wishlist')->where('user_id', '=', $this->_current_user->id)->where('productitem_id', '=', $product[0]['productitem_id'])->find();
                    if ($wish->loaded()) {
                        $product[0]['inwishlist'] = 1;
                    } else {
                        $product[0]['inwishlist'] = 0;
                    }
                    if ($count == $limit) {
                    break;
                    }
                    array_push($products, $product[0]);
                   $count = $count+1;
                }
            }
        }
        return array('list' => array('status' =>1, 'products' => $products));
    }

    private function getLatestProducts($data) {
        $offset = ($data['page_no'] - 1)*$data['limit'];
        $filters = null;
        if(isset($data['filters'])){
            $filters = json_decode($data['filters'], true);
        }
        $products = ORM::factory('Product')->getLatestProducts($offset, $data['limit'], $filters);

        $i = 0;
        foreach ($products as $product) {
            $qualityindex = ORM::factory('Qualityrating')->getQualityRatingByProduct($product['productitem_id']);
            $products[$i]['qualityindex'] = $qualityindex['qualityindex'];
            $i++;
        }

        $totalpages = 1;
        if(count($products) > $data['limit']){
            $totalpages = count($products)/$data['limit'];
        }
        return array('list' => array('status' => 1, 'products' => $products, 'paging' => array('totalpages' => $totalpages, 'page_no' => $data['page_no'], 'limit' => $data['limit'], 'next_page' => $data['page_no']+ 1, 'previous_page' => $data['page_no'] - 1)));
    }

    private function getProductsByModel($data){
        $offset = ($data['page_no'] - 1)*$data['limit'];
        $filters = null;
        if(isset($data['filters'])){
            $filters = json_decode($data['filters'], true);
        }
        $products = ORM::factory('Product')->getProductsByModel($data['modelnumber'], $offset, $data['limit'], $filters);

        $i = 0;
        foreach ($products as $product) {
            $qualityindex = ORM::factory('Qualityrating')->getQualityRatingByProduct($product['productitem_id']);
            $products[$i]['qualityindex'] = $qualityindex['qualityindex'];
            $i++;
        }

        $totalpages = 1;
        if(count($products) > $data['limit']){
            $totalpages = count($products)/$data['limit'];
        }
        return array('status' => 1, 'products' => $products, 'paging' => array('totalpages' => $totalpages, 'page_no' => $data['page_no'], 'limit' => $data['limit'], 'next_page' => $data['page_no']+ 1, 'previous_page' => $data['page_no'] - 1));
    }

    private function getQualityIndexValues($data)
    {
        $indices = array();
        $qindices = ORM::factory('Qualityindex')->where('productcategory_id', '=', $data['cat_id'])->find_all();
        foreach ($qindices as $qi) {
            $vals = ORM::factory('Qualityindexvalue')->where('qualityindexes_id', '=', $qi->id)->find_all();
            $value = array();
            foreach ($vals as $val) {
                array_push($value, array("value" => $val->value, "value_text" => $val->value_text));
            }
            $index = array("qid" => $qi->id, "qname" => $qi->name, "values" => $value);
            array_push($indices, $index);
        }

        return array('list' => $indices);
    }

    private function getQualityIndexKeyValueForProduct($data)
    {
        $product = $data['product_id'];

        $qvs = array();
        $qis = ORM::factory('Qualityrating')->where('productitem_id', '=', $product)->find_all();
        foreach($qis as $qi) {
            $qv = array();
            $qi_attr = ORM::factory('Qualityindex', $qi->qualityindex_id);
             $qi_val = ORM::factory('Qualityindexvalue')->where('value', '=', $qi->value)->where('qualityindexes_id', '=', $qi->qualityindex_id)->find();
            $qv['attribute'] = $qi_attr->name;
            $qv['value'] = $qi_val->value_text;
            $qv['score'] = $qi->value;
            array_push($qvs, $qv);
        }

        return array('list' => array('status' => 1, 'items' => $qvs));
    }

    private function updateUserName($data)
    {
        //$userloggedin = Session::instance(Session::$default)->get('auth_user');
        $userloggedin = Auth::instance()->get_user();
        $user = ORM::factory('User')->where('id', '=', $userloggedin->id)->find();
        if($user->loaded())
        {
            $user->update_user(array('firstname' => $data['firstname']),array('firstname'));
        }
        return array('list' => array('status' => 1));
    }

    private function updateUserNameLast($data)
    {
        //$userloggedin = Session::instance(Session::$default)->get('auth_user');
        $userloggedin = Auth::instance()->get_user();
        $user = ORM::factory('User')->where('id', '=', $userloggedin->id)->find();
        if($user->loaded())
        {
            $user->update_user(array('lastname' => $data['lastname']),array('lastname'));
        }
        return array('list' => array('status' => 1));
    }

    private function updateEmail($data)
    {
        //$userloggedin = Session::instance(Session::$default)->get('auth_user');
        // Check if the new email corresponds to another valid user, if so reject the modify
        $userloggedin = Auth::instance()->get_user();
        $another = ORM::factory('User')->where('email', '=', $data['email'])->find();
        if ($another->loaded()) {
            if ($another->id == $userloggedin->id) {
                return array('list' => array('status' => 0, 'message' => 'Failed: Email not changed!!'));
            }
            return array('list' => array('status' => 0, 'message' => 'Failed: Another user exist with this email!!'));
        }

        $user = ORM::factory('User')->where('id', '=', $userloggedin->id)->find();
        if($user->loaded())
        {
            $user->update_user(array('email' => $data['email']),array('email'));
        }
        return array('list' => array('status' => 1,));
    }

    private function updatePassword($data)
    {
        $user = Auth::instance()->get_user();
        if($user->loaded())
        {
            if (!isset($data['password'])) {
                return array('list' => array('status' => 0, 'message' => 'Failed: Empty Password!!'));
            }
            $user->update_user(array('password' => $data['password']),array('password'));
        }
        return array('list' => array('status' => 1));
    }

    private function getUserDetail()
    {
        $user = Auth::instance()->get_user();
        return array('list' => array('status' => 1, 'firstname' => $user->firstname ,  'lastname' => $user->lastname, 'phonenumber' => $user->phonenumber, 'email' => $user->email));
    }

    private function getWishlistedItems($data){
    	$data['user_id'] = $this->_current_user->id;
        $wishlists = ORM::factory('Wishlist')->where('user_id', '=', $data['user_id'])
        ->order_by('id', 'DESC')
        ->find_all();
        $wishlistArray = array();
        //$i=0;
        foreach ($wishlists as $wishlist) {
            $product = ORM::factory('Productitem')->getProductByID($wishlist->productitem_id)[0];
            //$wishlistArray[$i] = $product;
            $qualityindex = ORM::factory('Qualityrating')->getQualityRatingByProduct($wishlist->productitem_id);
            $product['qualityindex'] = $qualityindex['qualityindex'];
            $product['wishid'] = $wishlist->id;
            array_push($wishlistArray, $product);
            //echo strip_tags(Debug::vars($qualityindex['qualityindex']));
            //$i++;
        }
        return array('status' => 1, 'list' => $wishlistArray);
    }

    private function getSavedAddresses($data)
    {
        $useraddress = ORM::factory('Address')->where('user_id', '=', $this->_current_user->id)->find_all();
        $addrarr = array();
        $i = 0;
        foreach ($useraddress as $useraddr)
        {
            $addrarr[$i] = array('addressid' => $useraddr->id, 'address_line_one' => $useraddr->address_line_one ,'address_line_two' => $useraddr->address_line_two,'city' => $useraddr->city ,'state' => $useraddr->state, 'country' => $useraddr->country, 'pincode' => $useraddr->pincode, 'name' => $useraddr->name, 'phonenumber' => $useraddr->phonenumber, 'email' => $useraddr->email, 'landmark' => $useraddr->landmark);
            $i++;
        }

        // Load the current user details
        $user = Auth::instance()->get_user();
        return array('list' => array('address' => $addrarr, 'user' => array('name' => $user->firstname . ' ' . $user->lastname, 'email' => $user->email, 'phonenumber' => $user->phonenumber)));
    }

    private function getSavedAddressById($data)
    {
        $useraddr = ORM::factory('Address')->where('user_id', '=', $this->_current_user->id)->and_where('id', '=', $data['addrid'])->find();
        //$user = ORM::factory('User')->where('id', '=', $userloggedin->id)->find();

        return array('list' => array('addrid' => $useraddr->id, 'addresslineone' => $useraddr->address_line_one ,'addresslinetwo' => $useraddr->address_line_two,'city' => $useraddr->city ,'state' => $useraddr->state, 'country' => $useraddr->country, 'pincode' => $useraddr->pincode, 'name' => $useraddr->name, 'phonenumber' => $useraddr->phonenumber, 'landmark' => $useraddr->landmark, 'email' => $useraddr->email, 'landmark' => $useraddr->landmark));
    }

    private function updateSavedAddress($data)
    {
        $useraddr = ORM::factory('Address')->where('id', '=', $data['id'])->find();
        if($useraddr->loaded())
        {
            $useraddr->update_address($data, array('name', 'email', 'phonenumber', 'address_line_one', 'address_line_two', 'pincode', 'landmark'));
            return array('list' => array('status' => 1));
        }
        return array('list' => array('status' => 0));
    }

    private function removesavedaddress($data)
    {
        $addr = ORM::factory('Address', $data['id']);
        if ($addr->loaded()) {
            $addr->delete();
            return array('list' => array('status' => 1));
        }
        return array( 'list' => array('status' => 0));
    }

    private function getProductsByCategory($data) {
        $showoos = 1;
        if (isset($data['hideoos']) && $data['hideoos'] == 'true') {
           $showoos = 0;
        }

        $offset = ($data['page_no'] - 1) * $data['limit'];

        $filters = null;
        if(isset($data['filters'])){
            $filters = json_decode($data['filters'], true);
        }

        if(isset($data['sorttype']) && $data['sorttype'] == 'Price: Low to High'){
            $data['sort'] = 'ASC';
            $products = ORM::factory('Product')->getProductsByCategorySortPrice($data['category_id'], $offset, $data['limit'], $data['sort'], $filters, $showoos);
        }
        elseif(isset($data['sorttype']) && $data['sorttype'] == 'Price: High to Low'){
            $data['sort'] = 'DESC';
            $products = ORM::factory('Product')->getProductsByCategorySortPrice($data['category_id'], $offset, $data['limit'], $data['sort'], $filters, $showoos);
        }
        elseif(isset($data['sorttype']) && $data['sorttype'] == 'Top Quality'){
            $data['sort'] = 'DESC';
            $products = ORM::factory('Product')->getProductsByCategorySortQuality($data['category_id'], $offset, $data['limit'], $data['sort'], $filters, $showoos);
        }
        elseif(isset($data['sorttype']) && $data['sorttype'] == 'Latest Arrivals'){
            $data['sort'] = 'DESC';
            $products = ORM::factory('Product')->getProductsByCategorySortDate($data['category_id'], $offset, $data['limit'], $data['sort'], $filters, $showoos);
        }
        else {
            $products = ORM::factory('Product')->getProductsByCategory($data['category_id'], $offset, $data['limit'], $filters, $showoos);
        }

        $ps = array();
        $i = 0;
        foreach ($products as $product) {
            $wish = ORM::factory('Wishlist')->where('user_id', '=', $this->_current_user->id)->where('productitem_id', '=', $product['productitem_id'])->find();
            if ($wish->loaded()) {
                $product['inwishlist'] = 1;
            } else {
                $product['inwishlist'] = 0;
            }
            $product['compressorwarranty'] = ORM::factory('Productitem', $product['productitem_id'])->compressorwarranty;
            $product['closingoffer'] = ORM::factory('Productitem', $product['productitem_id'])->closingoffer;
            array_push($ps, $product);

            $i++;
        }
        $products = $ps;

        if (!isset($data['sorttype']) || $data['sorttype'] != 'Top Quality') {
            $i = 0;
            foreach ($products as $product) {
                $qualityindex = ORM::factory('Qualityrating')->getQualityRatingByProduct($product['productitem_id']);
                $products[$i]['qualityindex'] = $qualityindex['qualityindex'];
                $i++;
            }
        }

        //$query = DB::select(array('COUNT("id")', 'count'))->from('productitems');
        //$query = DB::select(array('COUNT("username")', 'total_users'))->from('users');
        if ($showoos) {
            $query = DB::select(array(DB::expr('COUNT(`id`)'), 'count'))->from('productitems')->where('productcategory_id','=',$data['category_id'])->where('hidden', '=', 0);
        } else {
            $query = DB::select(array(DB::expr('COUNT(`id`)'), 'count'))->from('productitems')->where('productcategory_id','=',$data['category_id'])->where('hidden', '=', 0)->where('issold', '=', 0);
        }
        $results = $query->execute();
        $totalproducts = $results[0]['count'];

        $totalpages = 1;
        if($totalproducts > $data['limit']) {
            $totalpages = ceil($totalproducts/$data['limit']);
        }

        $cat = ORM::factory('Productcategory', $data['category_id']);
        return array('list' => array('catname'=>$cat->name, 'status' => 1, 'products' => $products, 'paging' => array('totalpages' => $totalpages, 'totalitems'=>$totalproducts, 'page_no' => $data['page_no'], 'limit' => $data['limit'], 'next_page' => $data['page_no']+ 1, 'previous_page' => $data['page_no'] - 1)));
    }

    private function getFiltersByCategory($data){
        $filtertypes = ORM::factory('Filtertype')->where('productcategory_id', '=', $data['productcategory_id'])->find_all();
        $allFiltersArr = array();
        $i = 0;
        foreach ($filtertypes as $filtertype) {
            $filters = ORM::factory('Filter')->where('filtertype_id', '=', $filtertype->id)->find_all();
            $subFiltersArr = array();
            foreach ($filters as $filter) {
                array_push($subFiltersArr, array('id' => $filter->id, 'name' => $filter->name));
            }
            $allFiltersArr[$i] = array('filtertype' => $filtertype->name, 'filters' => $subFiltersArr);

            $i++;
        }

        return array('status' => 1, 'filters' => $allFiltersArr);
    }


    private function runCurl($url, $json = 0){
        if(!$json){
            $ch = curl_init();
            curl_setopt($ch,CURLOPT_URL,$url);
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,3);
            curl_setopt($ch, CURLOPT_TIMEOUT, 6); //timeout in seconds
            $response = curl_exec($ch);
            curl_close($ch);
        }
        else{
            $ch = curl_init();
            curl_setopt($ch,CURLOPT_URL, $url);
            curl_setopt($ch,CURLOPT_POST, 1);
            curl_setopt($ch,CURLOPT_POSTFIELDS, $json);
            curl_setopt($ch,CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
            curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,3);
            curl_setopt($ch, CURLOPT_TIMEOUT, 6); //timeout in seconds
            $response = curl_exec($ch);
            $errmsg = curl_error($ch);
            $cInfo = curl_getinfo($ch);
            curl_close($ch);
        }
        return $response;
    }



    private function base(){
        return URL::base(Request::$initial, TRUE);
    }

    //Warehouse
    private function warehouseAddProduct($data)
    {

        $w = ORM::factory('Warehouseproduct')->values($data)->save();
        $w->timestamp = date('Y-m-d H:i:s');
        $w->save();
        return array('status' => 1);
    }

    private function warehouseModifyProduct($data)
    {
        // Convert status string to int
        $data['stage'] = ORM::factory('Warehouseproductstage')->where('stage', '=', $data['stage'])->find_all()[0]->id;
        $w = ORM::factory('Warehouseproduct', $data['id']);
        $w->values($data)->save();
        $w->timestamp = date('Y-m-d H:i:s');
        $w->save();
        return array('status' => 1);
    }

    private function getWarehouseProducts($data)
    {
        $limit = 1000;
        $page = $data['page'] * $limit;
        $wps = ORM::factory('Warehouseproduct')->where('stage', '!=', '7')->where('stage', '!=', '8')->order_by('id', 'DESC')->limit($limit)->offset($page)->find_all();
        $rwps = array();
        foreach ($wps as $wp) {
            $wp = $wp->as_array();
            $wp['stage'] = ORM::factory('Warehouseproductstage', $wp['stage'])->stage;
            // convert vendor id to vendor name
            $vid = intval($wp['arrived_from']);
            if ($vid) {
                $wp['arrived_from'] = ORM::factory('Vendor', $vid)->name;
            }
            // convert category id to category name
            $cid = intval($wp['product_cat']);
            if ($vid) {
                $wp['product_cat'] = ORM::factory('Productcategory', $cid)->name;
            }


           // $wp['arrived_from'] = ORM::factory('Vendor', $wp['arrived_from'])->name;
            if (($wp['unboxed'] == 1) &&  (strpos($wp['product_title'], '[Unboxed]') == false))  {
                $wp['product_title'] = $wp['product_title'] . " [Unboxed]";
            }
            array_push($rwps, $wp);
        }
        return array('status' => 1, 'list' => $rwps);
    }
    private function getWarehouseStats($data)
    {
        $stat['arrived']  = count(ORM::factory('Warehouseproduct')->where('stage', '=', 1)->find_all());
        $stat['qc']  = count(ORM::factory('Warehouseproduct')->where('stage', '=', 2)->find_all());
        $stat['fct'] = count(ORM::factory('Warehouseproduct')->where('stage', '=', 3)->find_all());
        $stat['service']  = count(ORM::factory('Warehouseproduct')->where('stage', '=', 4)->find_all());
        $stat['rtp']        = count(ORM::factory('Warehouseproduct')->where('stage', '=', 5)->find_all());
        $stat['rts']        = count(ORM::factory('Warehouseproduct')->where('stage', '=', 6)->find_all());
        $stat['sold']       = count(ORM::factory('Warehouseproduct')->where('stage', '=', 7)->find_all());
        $stat['instock']    = count(ORM::factory('Warehouseproduct')->where('stage', '!=', 7)->where('stage', '!=', 8)->find_all());
        $stat['Fridge'] = count(ORM::factory('Warehouseproduct')->where('product_cat', '=', 'Refrigerator')->where('stage', '!=', 7)->find_all());
        $stat['wm'] = count(ORM::factory('Warehouseproduct')->where('product_cat', '=', 'Washing Machine')->where('stage', '!=', 7)->find_all());
        $stat['bed'] = count(ORM::factory('Warehouseproduct')->where('product_cat', '=', 'Bed')->where('stage', '!=', 7)->find_all());
        $stat['ac'] = count(ORM::factory('Warehouseproduct')->where('product_cat', '=', 'Air Conditioner')->where('stage', '!=', 7)->find_all());
        $stat['chair'] = count(ORM::factory('Warehouseproduct')->where('product_cat', '=', 'Office Chair')->where('stage', '!=', 7)->find_all());
        $stat['laptop'] = count(ORM::factory('Warehouseproduct')->where('product_cat', '=', 'Laptop')->where('stage', '!=', 7)->find_all());

        return array('status' => 1, 'list' => $stat);
    }

    private function getWarehouseProductStages($data) {
        $stages = ORM::factory('Warehouseproductstage')->find_all();
        $status = array();
        foreach ($stages as $stage) {
            array_push($status, $stage->as_array());
        }
        return array('status' => 1, 'list' => $status);
    }

    private function searchWarehouseProducts($data)
    {
        $field = $data['field'];
        $value = $data['value'];
        $value = "%" . $value . "%";
        $products = ORM::factory('Warehouseproduct')->where($field, 'LIKE', $value)->find_all();
        $rp = array();
        foreach ($products as $product) {
            array_push($rp, $product->as_array());
        }

        return array('status' => 1, 'list' => $rp);
    }


    // Tickets
    private function getAllTickets($data){
            $limit = 100;
            $page = $data['page'] * $limit;
            $tickets = ORM::factory('Ticket')->order_by('id', 'DESC')->limit($limit)->offset($page)->find_all();
            $returntickets = array();
            foreach ($tickets as $ticket) {
                $rticket = $ticket->as_array();
                $orderdetail = ORM::factory('Orderdetail', $rticket['order_id']);
                $today = DateTime::createFromFormat('Y-m-d H:i:s', $ticket->createdtime);
                $odate = DateTime::createFromFormat('Y-m-d H:i:s', $orderdetail->orderdate);
                $diff=date_diff($today,$odate);
                $days = $diff->days;

                $rticket['underwarranty']  = $this->is_under_warranty($days);
                $rticket['uname'] = ORM::factory('User', $ticket->user_id)->firstname;
                $rticket['mobile'] = ORM::factory('User', $ticket->user_id)->phonenumber;
                $rticket['status'] = ORM::factory('Ticketstatus', $ticket->status)->name;
                $rticket['nextstatus'] = ORM::factory('Ticketstatus', intval($ticket->status)%4+1)->name;
                array_push($returntickets, $rticket);
            }
            return array('status' => 1, 'list' => $returntickets);

    }

    private function getPendingTickets($data){
        $pendingtickets = ORM::factory('Ticket')->where('status', '!=', "4")->order_by('id', 'DESC')->find_all();
        $returnpendingtickets = array();
        foreach ($pendingtickets as $ticket) {
            $rticket = $ticket->as_array();
            $orderdetail = ORM::factory('Orderdetail', $rticket['order_id']);
            $today = DateTime::createFromFormat('Y-m-d H:i:s', $ticket->createdtime);
            $odate = DateTime::createFromFormat('Y-m-d H:i:s', $orderdetail->orderdate);
            $diff=date_diff($today,$odate);
            $days = $diff->days;

            $rticket['underwarranty']  = $this->is_under_warranty($days);
            $rticket['uname'] = ORM::factory('User', $ticket->user_id)->firstname;
            $rticket['mobile'] = ORM::factory('User', $ticket->user_id)->phonenumber;
            $rticket['status'] = ORM::factory('Ticketstatus', $ticket->status)->name;
            $rticket['nextstatus'] = ORM::factory('Ticketstatus', intval($ticket->status)%4+1)->name;
            $rticket['days'] = $days;
            array_push($returnpendingtickets, $rticket);

        }
        return array('status' => 1, 'list' => $returnpendingtickets);
    }

    private function is_under_warranty($days) {
        if ($days < 180) {
            return 1;
        }
        return 0;
    }

    private function getServiceRequests($data){
        $pendingtickets = ORM::factory('Ticket')->where('user_id', '=', $this->_user->get_user_id())->order_by('id', 'DESC')->find_all();
        $returnpendingtickets = array();
        foreach ($pendingtickets as $ticket) {
            $rticket = $ticket->as_array();
            $orderdetail = ORM::factory('Orderdetail', $rticket['order_id']);
            //$today = new DateTime('now');
            $today = DateTime::createFromFormat('Y-m-d H:i:s', $ticket->createdtime);
            $odate = DateTime::createFromFormat('Y-m-d H:i:s', $orderdetail->orderdate);
            $diff=date_diff($today,$odate);
            $days = $diff->days;

            $rticket['underwarranty']  = $this->is_under_warranty($days);
            $rticket['uname'] = ORM::factory('User', $ticket->user_id)->firstname;
            $rticket['mobile'] = ORM::factory('User', $ticket->user_id)->phonenumber;
            $rticket['status'] = ORM::factory('Ticketstatus', $ticket->status)->name;
            $rticket['nextstatus'] = ORM::factory('Ticketstatus', intval($ticket->status)%4+1)->name;
            $rticket['days'] = $days;


            array_push($returnpendingtickets, $rticket);

        }
        return array('status' => 1, 'list' => $returnpendingtickets);
    }


    private function addTicket($data) {
        $ticket = ORM::factory('Ticket')->values($data)->save();
        $data['ticket_id'] = $ticket->id;
        $data['first'] = 1;
        return $this->addTicketComment($data);
    }

    private function addRepairExpense($data) {
        $ticket = ORM::factory('Ticket', $data['ticket_id']);
        $ticket->rexpense = $data['rexpense'];
        $ticket->save();
        return array('list' => array('status' => 1,));

    }

    private function addTicketComment($data) {
        if (!isset($data['first'])) {
            $ticket = ORM::factory('Ticket', $data['ticket_id']);
            $ticket->status = 2;
            $ticket->save();
        }
        $comment = ORM::factory('Ticketcomment')->values($data)->save();
        return array('status' => 1, 'list' => $comment->as_array());
    }

    private function getAllComments($data) {
        $comments = ORM::factory('Ticketcomment')->where('ticket_id', '=', $data['ticket_id'])->find_all();
        $returnedcomments = array();
        foreach ($comments as $comment){
            $rcomment = $comment->as_array();
            array_push($returnedcomments, $rcomment);
        }
        return array('status' => 1, 'list' => $returnedcomments);
    }

    private function modifyComment($data){
        $comment = ORM::factory('Ticketcomment', $data['comment_id']);
        $comment->comment = $data['comment'];
        $comment->save();
        return array('list' => array('status' => 1,));
    }

    private function setTicketStatus($data) {
        if ($data['status'] == 'Open') {
            $data['status'] = 1;
            $data['nextstatus'] = 'VOC Done';
        }elseif ($data['status'] == 'VOC Done') {
            $data['status'] = 2;
            $data['nextstatus'] = 'Tech Assigned';
        } elseif ($data['status'] == 'Tech Assigned') {
            $data['status'] = 3;
            $data['nextstatus'] = 'Resolved';
        } elseif ($data['status'] == 'Resolved') {
            $data['status'] = 4;
            $data['nextstatus'] = 'Re-Open';
        }
        $ticket = ORM::factory('Ticket', $data['ticketid']);
        $ticket->status = $data['status'];
        if ($data['status'] == 3) {
           $ticket->tech = $data['tech'];
        }
        $ticket->save();

        return array('status'=> 1, 'list' => array('nextstatus' => $data['nextstatus']));
    }

    private function bookDeliverySlot($data)
    {
        $dt= date('Y-m-d H:i:s', strtotime($data['d2']));
        $d = ORM::factory('Deliveryslot')->where('orderid', '=', $data['orderid'])->find();
        if (!$d->loaded()) {
            $d = ORM::factory('Deliveryslot');
        }
        $d->orderid = $data['orderid'];
        $d->date = $dt;
        $d->time = $data['t2'];
        $d->save();
        return array('status' =>1);
    }

    //Customer call
     private function getCallStats($data)
    {
        //$date = $data['date'];
        //$today = date('Y-m-d');
        $stats['total']  = count(ORM::factory('Customercall')->find_all());
        $stats['registered']  = count(ORM::factory('Customercall')->where('registered', '=', 1)->find_all());
        //$stats['date']  = count(ORM::factory('Customercall')->where('date', '=', current_date())->find_all());
       // $stats['dates']  = count(ORM::factory('Customercall')->where('date', '=', $today)->find_all());

        return array('status' => 1, 'list' => $stats);
    }

    private function addCustomerCall($data){
        $call = ORM::factory('Customercall');
        $call->name = $data['name'];
        $call->mobile = $data['phone'];
        $call->source = $data['source'];
        $call->pricerange = $data['range'];
        $call->comment = $data['comment'];
        $call->purpose = $data['purpose'];
        $call->product = $data['product'];
        $call->link = $data['link'];
        $call->addedby = Auth::instance()->get_user()->firstname;
        $call->save();

        return array('status' =>1);
    }

    private function saveCallComment($data)
    {
       $call = ORM::factory('Customercall', $data['id']);
       $call->comment = $data['comment'];
       $call->save();
       return array('status' =>1);
    }

    private function getCallDetails($data) {
        $limit = 100;
        $page = $data['page'] * $limit;

        $calls = ORM::factory('Customercall')->order_by('id', 'DESC')->limit($limit)->offset($page)->find_all();
        $details = array();
        foreach ($calls as $call) {
            array_push($details, $call->as_array());
        }
        return array('status' =>1, 'list' => $details);

    }


    private function markRegisteredUsers($data)
    {
        $calls = ORM::factory('Customercall')->find_all();
        foreach($calls as $call) {
            $u = ORM::factory('User')->where('phonenumber', '=', $call->mobile)->find();
            if ($u->loaded()) {
                $call->registered = 1;
                $call->save();
            }
        }
        return array('status' => 1);
    }

    //End of customer call
    // User/Order statistics
    private function getStatistics($data)
    {
        $searching = $data['searching'];
        $statistics = array();
        $days = array();
        $weeks = array();
        $months = array();
        $years  = array();

        $visitor = ORM::factory('Visitor');
        $user = ORM::factory('User');
        $order = ORM::factory('Order');
        $seen = ORM::factory('Productseen');
        $sale = ORM::factory('Sell');
        $call =  ORM::factory('Customercall');

        // Daily details
        $count = 1;
        if ($searching == '1') {
            $count = 7;
            $currentday = $data['search_date'];
            $currentdayobj = new DateTime();
        } else if ($searching == '0') {
            $count = 1;
            $currentdayobj = new DateTime();
            $currentday = $currentdayobj->format('Y-m-d');
        }

        if ($searching == '0' || $searching == '1') {
            while ($count <= 7) {
                $visitor_count = count($visitor->where('timestamp', 'LIKE', '%'.$currentday.'%')->find_all());
                $registered_count = count($user->where('timestamp', 'LIKE', '%'.$currentday.'%')->find_all());
                $prdseen_count = count($seen->where('time', 'LIKE', '%'.$currentday.'%')->find_all());
                $count_orders = count($order->where('qcdonedate', 'LIKE', '%'.$currentday.'%')->find_all());
                $count_sale = count($sale->where('timestamp', 'LIKE', '%'.$currentday.'%')->find_all());
                $count_call_s = count($call->where('timestamp', 'LIKE', '%'.$currentday.'%')->where('purpose', '=', 'sale')->find_all());
                $count_call_p = count($call->where('timestamp', 'LIKE', '%'.$currentday.'%')->where('purpose', '=', 'procure')->find_all());
                $count_cancelled = count($order->where('canceldate', 'LIKE', '%'.$currentday.'%')->where('iscancelled', '=', '1')->find_all());
                $rows = $order->where('qcdonedate', 'LIKE', '%'.$currentday.'%')->where('iscancelled', '!=', '1')->find_all();
                $revenue = 0;
                foreach($rows as $row) {
                    $revenue = $revenue + intval($row->price);
                }
                //$revenue = $result['revenue'];

                $stati_cday = array('revenue' => $revenue, 'date' => $currentday, 'visitors' => $visitor_count, 'users' => $registered_count, 'seen' => $prdseen_count, 'orders' => $count_orders, 'cancelledorders' => $count_cancelled, 'revenue' => $revenue, 'saleposts' => $count_sale, 'scalls' => $count_call_s, 'pcalls' => $count_call_p);
                array_push($days, $stati_cday);
                $currentdayobj->modify('-1 day');
                $currentday = $currentdayobj->format('Y-m-d');
                $count += 1;
            }
        }

        if ($searching == '1') {
             $statistics['days'] =  $days;
             return array('status' => 1, 'list' => $statistics);
        }

        if ($searching == '2') {
            $count = 7;
            $currentmonth = $data['search_month'];
            $currentmonthobj = new DateTime();
        } else if ($searching == '0'){
            $count = 1;
            $currentmonthobj = new DateTime();
            $currentmonth = $currentmonthobj->format('Y-m');
        }

        // Monthly details
        if ($searching == '0' || $searching == '2') {
            while ($count <= 7) {
                $visitor_count = count($visitor->where('timestamp', 'LIKE', '%'.$currentmonth.'%')->find_all());
                $registered_count = count($user->where('timestamp', 'LIKE', '%'.$currentmonth.'%')->find_all());
                $prdseen_count = count($seen->where('time', 'LIKE', '%'.$currentmonth.'%')->find_all());
                $count_orders = count($order->where('qcdonedate', 'LIKE', '%'.$currentmonth.'%')->find_all());
                $count_sale = count($sale->where('timestamp', 'LIKE', '%'.$currentmonth.'%')->find_all());
                $count_call_s = count($call->where('timestamp', 'LIKE', '%'.$currentmonth.'%')->where('purpose', '=', 'sale')->find_all());
                $count_call_p = count($call->where('timestamp', 'LIKE', '%'.$currentmonth.'%')->where('purpose', '=', 'procure')->find_all());
                $count_cancelled = count($order->where('canceldate', 'LIKE', '%'.$currentmonth.'%')->where('iscancelled', '=', '1')->find_all());

                $rows = $order->where('qcdonedate', 'LIKE', '%'.$currentmonth.'%')->find_all();
                $revenue = 0;
                foreach($rows as $row) {
                    $revenue = $revenue + intval($row->price);
                }

                //$revenue = count($order->where('timestamp', 'LIKE', '%'.$currentday.'%')->where('iscancelled', '!=', 1)->find_all());
                $stati_cmonth = array('revenue' => $revenue, 'date' => $currentmonth,'visitors' => $visitor_count, 'users' => $registered_count, 'seen' => $prdseen_count, 'orders' => $count_orders, 'cancelledorders' => $count_cancelled, 'saleposts' => $count_sale, 'scalls' => $count_call_s, 'pcalls' => $count_call_p);
                array_push($months, $stati_cmonth);
                $currentmonthobj->modify('-1 month');
                $currentmonth = $currentmonthobj->format('Y-m');
                $count += 1;
            }
        }

        if ($searching == '2') {
             $statistics['months'] =  $months;
             return array('status' => 1, 'list' => $statistics);
        }

        if ($searching == '3') {
            $count = 7;
            $currentyear = $data['search_year'];
            $currentyearobj = new DateTime();
        } else if ($searching == '0'){
            $count = 1;
            $currentyearobj = new DateTime();
            $currentyear = $currentyearobj->format('Y');
        }
        // Yearly details
        if ($searching == '0' || $searching == '3') {
            while ($count <= 7) {
                $visitor_count = count($visitor->where('timestamp', 'LIKE', '%'.$currentyear.'%')->find_all());
                $registered_count = count($user->where('timestamp', 'LIKE', '%'.$currentyear.'%')->find_all());
                $prdseen_count = count($seen->where('time', 'LIKE', '%'.$currentyear.'%')->find_all());
                $count_orders = count($order->where('qcdonedate', 'LIKE', '%'.$currentyear.'%')->find_all());
                $count_sale = count($sale->where('timestamp', 'LIKE', '%'.$currentyear.'%')->find_all());
                $count_call_s = count($call->where('timestamp', 'LIKE', '%'.$currentyear.'%')->where('purpose', '=', 'sale')->find_all());
                $count_call_p = count($call->where('timestamp', 'LIKE', '%'.$currentyear.'%')->where('purpose', '=', 'procure')->find_all());
                $count_cancelled = count($order->where('canceldate', 'LIKE', '%'.$currentyear.'%')->where('iscancelled', '=', '1')->find_all());

                $rows = $order->where('qcdonedate', 'LIKE', '%'.$currentyear.'%')->where('iscancelled', '!=', '1')->find_all();
                $revenue = 0;
                foreach($rows as $row) {
                    $revenue = $revenue + intval($row->price);
                }

                //$revenue = count($order->where('timestamp', 'LIKE', '%'.$currentday.'%')->where('iscancelled', '!=', 1)->find_all());
                $stati_cmonth = array('revenue' => $revenue, 'date' => $currentyear,'visitors' => $visitor_count, 'users' => $registered_count, 'seen' => $prdseen_count, 'orders' => $count_orders, 'cancelledorders' => $count_cancelled, 'saleposts' => $count_sale,'scalls' => $count_call_s, 'pcalls' => $count_call_p);
                array_push($years, $stati_cmonth);
                $currentyearobj->modify('-1 year');
                $currentyear = $currentyearobj->format('Y-m');
                $count += 1;
            }
            if ($searching == '3') {
                 $statistics['years'] =  $years;
                 return array('status' => 1, 'list' => $statistics);
            }
        }

        $recent_users = array();
        $ruu = $user->order_by('id', 'DESC')->limit(30)->find_all();
        foreach($ruu as $u) {
            $ru['id'] = $u->id;
            $ru['phonenumber'] = $u->phonenumber;
            $ru['name'] = $u->firstname;
            $ru['verified'] = $u->isverified;
            $ru['timestamp'] = $u->timestamp;
            array_push($recent_users, $ru);
        }

        $statistics['days'] =  $days;
        $statistics['months'] =  $months;
        $statistics['years'] =  $years;
        $statistics['recentusers'] = $recent_users;

        return array('status' => 1, 'list' => $statistics);
    }

    // Sell product
    private function sellProduct($data)
    {
        $data = $data['product'];
        $data = json_decode($data);
        $data = (array)$data;
        $s = ORM::factory('Sell')->values($data)->save();
        return array('status' => 1, 'list' => array('sellid' => $s->sellid));
    }

    private function approveSellPrice($data)
    {
        $s = ORM::factory('Sell', $data['id']);
        $s->isapproved = 1;
        $s->isrejected = 0;
        $s->isprocured = 0;
        $s->approvedprice = $data['approvedprice'];
        $s->admincomment = isset($data['admincomment']) ? $data['admincomment'] : '';
        $s->save();
        return array('status' => 1);
    }

    private function saveSaleComment($data)
    {
        $s = ORM::factory('Sell', $data['id']);
        $s->admincomment = $data['admincomment'];
        $s->save();
        return array('status' => 1);
    }

    private function markProcured($data)
    {
        $s = ORM::factory('Sell', $data['id']);
        $s->isprocured = 1;
        $s->approvedprice = isset($data['approvedprice']) ? $data['approvedprice'] : '';
        $s->save();
        return array('status' => 1);
    }
    private function markContacted($data)
    {
        $s = ORM::factory('Sell', $data['id']);
        $s->customercalled = 1;
        $s->save();
        return array('status' => 1);
    }
    private function markRejected($data)
    {
        $s = ORM::factory('Sell', $data['id']);
        $s->isrejected = 1;
        $s->rejectreason = $data['rejectreason'];
        $s->isapproved = 0;
        $s->isprocured = 0;
        $s->save();
        return array('status' => 3);
    }

    private function getCustomerSellRequests($data)
    {
        $ros = array();
        $sells = ORM::factory('Sell')->order_by('id', 'desc')->where('user_id', '=', $this->_user->get_user_id())->find_all();
        foreach($sells as $s) {
            $rs = array();
            $rs['sellid'] = $s->as_array()['sellid'];
            $rs['productname'] = $s->as_array()['productname'];
            $rs['maincat'] = $s->as_array()['maincat'];
            $rs['subcat'] = $s->as_array()['subcat'];
            $rs['productbrand'] = $s->as_array()['productbrand'];
            $rs['timestamp'] = $s->as_array()['timestamp'];
            if ($s->isprocured) {
                $rs['status'] = 'Procured';
                $rs['statusclass'] = 'label-success';
            } elseif ($s->isrejected) {
                $rs['status'] = 'Rejected';
                $rs['statusclass'] = 'label-default';
                $rs['rejectreason'] = $s->rejectreason;
            } elseif ($s->isapproved) {
                $rs['status'] = 'Deal Fixed';
                $rs['statusclass'] = 'label-success';
                $rs['approvedprice'] = $s->approvedprice;
            } elseif ($s->customercalled) {
                $rs['status'] = 'Fixing a Deal';
                $rs['statusclass'] = 'label-info';
            } elseif ($s->admincomment) {
                $rs['status'] = 'Valuating Fair Price';
                $rs['statusclass'] = 'label-primary';
            } else {
                $rs['status'] = "Pending Process";
                $rs['statusclass'] = 'label-primary';
            }
            array_push($ros, $rs);
        }
        return array('status' => 1, 'list' => $ros);
    }
    private function getPendingSell($data)
    {
        $showoption = $data['showoption'];
        $limit = 50;
        $page = $data['page'] * $limit;

        if (isset($data['value']))
            $sps = ORM::factory('Sell')->order_by('id', 'desc')->limit($limit)->offset($page)->where($data['field'], '=', $data['value'])->find_all();
        /*else {
            $sps = ORM::factory('Sell')->order_by('id', 'desc')->limit($limit)->offset($page)->
            where('isrejected', '=', 0)->
            where('isprocured', '=', 0)->find_all();
        }*/


       else if ($showoption == 'pending') {
             $sps = ORM::factory('Sell')->order_by('id', 'desc')->limit($limit)->offset($page)->where('isapproved', '!=', 1)->where('isprocured', '!=', 1)->where('isrejected', '!=', 1)->find_all();
        } else if ($showoption == 'approved') {
             $sps = ORM::factory('Sell')->order_by('id', 'desc')->limit($limit)->offset($page)->where('isapproved', '=', 1)->where('isprocured', '!=', 1)->find_all();
        } else if ($showoption == 'procured') {
             $sps = ORM::factory('Sell')->order_by('id', 'desc')->limit($limit)->offset($page)->where('isprocured', '=', 1)->find_all();
        } else if ($showoption == 'all') {
            $sps = ORM::factory('Sell')->order_by('id', 'desc')->limit($limit)->offset($page)->find_all();
        }else if ($showoption == 'rejected') {
             $sps = ORM::factory('Sell')->order_by('id', 'desc')->limit($limit)->offset($page)->where('isrejected', '=', 1)->find_all();
        }else {
            $sps = ORM::factory('Sell')->order_by('id', 'desc')->limit($limit)->offset($page)->
            where('isrejected', '=', 0)->
            where('isprocured', '=', 0)->find_all();
        }


        $rp = array();
        foreach ($sps as $sp) {
            $sp = $sp->as_array();
            $sp['sellername'] = ORM::factory('User', $sp['user_id'])->firstname;
            $sp['sellermobile'] = ORM::factory('User', $sp['user_id'])->phonenumber;
            $sp['selleremail'] = ORM::factory('User', $sp['user_id'])->email;
            $sellid = $sp['sellid'];
            // Get image names
            $oimages = array();
            $directory = DOCROOT."assets/images/sell_requests/sell_$sellid";
            if (is_dir($directory)) {
               $sp['imageuploadfailed'] = 0;
               $images = scandir($directory);
            } else {
                $images = array();
                $sp['imageuploadfailed'] = 1;
            }
            foreach ($images as $image) {
                if (substr($image,0,1) == '.') {
                    continue;
                }
                if (substr($image,0,2) == '..') {
                    continue;
                }
                $image = "assets/images/sell_requests/sell_$sellid/".$image;
                array_push($oimages, $image);
            }
            $sp['images'] = $oimages;
            array_push($rp, $sp);
        }
        return array('status' => 1, 'list' => $rp);
    }

    private function searchSaleProducts($data)
    {
        $field = $data['field'];
        $value = $data['value'];
        $value = "%" . $value . "%";
        $sellreqs = ORM::factory('Sell')->where($field, 'LIKE', $value)->find_all();
        $rp = array();
        foreach ($sellreqs as $sellreq) {
            array_push($rp, $sellreq->as_array());

        }

        return array('status' => 1, 'list' => $rp);
    }

    private function addCustomerReferral($data)
    {
        // Already registered
        $u = ORM::factory('User')->where('phonenumber', '=', $data['mob'])->find_all();
        if (count($u) == 1) {
            return array('status' => 1, 'list' => array('status' => 3));
        }

        $r = ORM::factory('Referral');
        // Mobile number has to be unique
        $r = $r->where('rmobile', '=', $data['mob'])->find_all();
        if (count($r) == 1) {
            return array('status' => 1, 'list' => array('status' => 3));
        }
        $r = ORM::factory('Referral');
        $r->user_id = $this->_user->get_user_id();
        $r->rname = $data['name'];
        $r->rmobile = $data['mob'];
        $r->save();
/*
        $rname = substr($this->_current_user->firstname, 0, 7);
        $smstext = "Hi, $rname referred you to join www.pocketin.in Buy Quality Tested Used Appliances & Furniture. Free-Delivery|Warranty|Buy-Back|Exchange"
        $response = $this->_sendSMS($smstext, $r->user_id, $r->rmobile);
        $status = 0;
        if ($response->status == 'OK') {
           $status = 1;
        }
*/
        return array('status' => 1);
    }

    private function getCustomerReferrals($data)
    {
        $rs = ORM::factory('Referral')->where('user_id', '=', $this->_user->get_user_id())->order_by('id', 'DESC')->find_all();
        $rp = array();
        foreach($rs as $r) {
            array_push($rp, $r->as_array());
        }

        return array('status' => 1, 'list' => $rp);
    }

    private function checkReferral($data)
    {
        $order_id = $data['order_id'];

        $usr = ORM::factory('Orderdetail', $order_id)->user_id;
        $refer = ORM::factory('Referral')->where('user_id', '=', $usr)->find_all();
        $rfs = array();
        foreach($refer as $rf){
            array_push($rfs, $rf->as_array());
        }
        return array('status' => 1, 'list' => $rfs);
    }


    // Patient for Dr Deepak. Remove this once done
    private function get_all_patients($data)
    {
        $patients = ORM::factory('Patient')->find_all();
        $rp = array();
        foreach ($patients as $patient) {
            array_push($rp, $patient->as_array());
        }
        return array('status' => 1, 'list' => $rp);
    }



    private function get_patient_with_id($data)
    {
        $patient = ORM::factory('patient', $data['id']);
        return array('status' => 1, 'list' => $patient);
    }

    private function search_patient($data)
    {
        $field = $data['field'];
        $value = $data['value'];
        $value = "%" . $value . "%";
        $patients = ORM::factory('patient')->where($field, 'LIKE', $value)->find_all();
        $rp = array();
        foreach ($patients as $patient) {
            array_push($rp, $patient->as_array());
        }

        return array('status' => 1, 'list' => $rp);
    }


    private function add_patient($data)
    {
        $data = $data['patient'];
        $data = json_decode($data);
        $data = (array)$data;
        ORM::factory('patient')->values($data)->save();
        return array('status' => 1, 'list' => $data);
    }

    private function modify_patient($data)
    {
        $data = $data['patient'];
        $data = json_decode($data);
        $data = (array)$data;

        $p = ORM::factory('patient', $data['id']);
        $p->values($data)->save();
        return array('status' => 1, 'list' => $data);
    }

 private function addProcurementDetails($data)
    {
        $data = json_decode($data['input'], 1);
        $w = ORM::factory('Procurement')->values($data)->save();
        return array('status' => 1);
    }

private function getProcurementDetails($data) {

        $procurements = ORM::factory('Procurement')->order_by('date', 'DESC')->find_all();
        $pdetails = array();
        foreach ($procurements as $procurement) {
            // convert vendor id to vendor name
            $vid = intval($procurement->from);
            if ($vid) {
                $procurement->from = ORM::factory('Vendor', $vid)->name;
            }
            array_push($pdetails, $procurement->as_array());
        }
        return array('status' =>1, 'list' => $pdetails);

    }
private function addExpenseDetails($data)
    {
        $data = json_decode($data['input'], 1);
        $w = ORM::factory('Expense')->values($data)->save();
        return array('status' => 1);
    }

 private function getExpenseDetails($data) {

        $expenses = ORM::factory('Expense')->order_by('date', 'DESC')->find_all();
        $expdetails = array();
        foreach ($expenses as $expense){
            array_push($expdetails, $expense->as_array());
        }
        return array('status' =>1, 'list' => $expdetails);

    }

private function addEmployeeDetails($data)
    {
        $data = json_decode($data['input'], 1);
        $w = ORM::factory('Employee')->values($data)->save();
        return array('status' => 1);
    }


private function addVendorDetails($data)
    {
        $data = json_decode($data['input'], 1);
        $w = ORM::factory('Vendor')->values($data)->save();
        return array('status' => 1);
    }


private function getVendorDetails($data) {

        $vendors = ORM::factory('Vendor')->order_by('id', 'ASC')->find_all();
        $vendordetails = array();
        foreach ($vendors as $vendor){
            array_push($vendordetails, $vendor->as_array());
        }
        return array('status' =>1, 'list' => $vendordetails);

    }

// private function getProductCategoriess($data) {

//         $categories = ORM::factory('Productcategory')->order_by('id', 'ASC')->limit(40)->find_all();
//         $categoryedetails = array();
//         foreach ($categories as $category){
//             array_push($categoryedetails, $category->as_array());
//         }
//         return array('status' =>1, 'list' => $categoryedetails);

//     }

 private function getEmployeeDetails($data) {

        $employees = ORM::factory('Employee')->order_by('id', 'ASC')->find_all();
        $empdetails = array();
        foreach ($employees as $employee){
            array_push($empdetails, $employee->as_array());
        }
        return array('status' =>1, 'list' => $empdetails);

    }


private function addLeave($data)
    {
        $data = json_decode($data['input'], 1);
        $w = ORM::factory('EmployeeLeave')->values($data)->save();
        return array('status' => 1);
    }



private function getLeaveDetails($data)
    {
        $leaves = ORM::factory('EmployeeLeave')->order_by('date', 'DESC')->find_all();
        $leavedetails = array();
        foreach($leaves as $leave){
            $leave = $leave->as_array();
            $leave['empname'] =  ORM::factory('Employee', $leave['emp_id'])->name;
            array_push($leavedetails, $leave);
        }
        return array('status' => 1, 'list' => $leavedetails );
    }


//Buyback

private function addBuyBack($data)
    {
        $data = json_decode($data['input'], 1);
        $w = ORM::factory('Buyback')->values($data)->save();
        return array('status' => 1);
    }

private function getBuyBackDetails($data) {

    $buyback = ORM::factory('Buyback')->order_by('id', 'DESC')->find_all();
    $bbdetails = array();
    foreach ($buyback as $bb){
        $bb = $bb ->as_array();
        $bb['status'] = ORM::factory('BuybackStage', $bb['status'])->status;
        array_push($bbdetails, $bb);
    }
    return array('status' =>1, 'list' => $bbdetails);

    }

private function getBuyBackStages($data) {
        $stages = ORM::factory('BuybackStage')->find_all();
        $status = array();
        foreach ($stages as $stage) {
            array_push($status, $stage->as_array());
        }
        return array('status' => 1, 'list' => $status);
    }

private function modifyBbPrice($data){
        $id = $data['bb_id'];
        $bb = ORM::factory('Buyback', $id);
        $bb->bb_price = $data['bb_price'];
        $bb->status = 3;
        $bb->save();
        return array('list' => array('status' => 1,));
    }

private function markBbPickup($data){
        $id = $data['bb_id'];
        $pd = ORM::factory('Buyback', $id);
        $pd->pickupdone = 1;
        $pd->status = 4;
        $pd->save();
        return array('list' => array('status' => 1,));
    }

private function markBbRejected($data){
        $id = $data['bb_id'];
        $rb = ORM::factory('Buyback', $id);
        $rb->isrejected = 1;
        $rb->status = 5;
        $rb->rejectreason = $data['reason'];
        $rb->save();
        return array('list' => array('status' => 1,));
    }

private function addBbTech($data){
        $id = $data['bb_id'];
        $ta = ORM::factory('Buyback', $id);
        $ta->tech = $data['tech'];
        $ta->status = 2;
        $ta->save();
        return array('list' => array('status' => 1,));
    }

private function reopenBb($data){
        $id = $data['bb_id'];
        $rbb = ORM::factory('Buyback', $id);
        $rbb->status = 1;
        $rbb->isrejected = 0;
        $rbb->pickupdone = 0;
        $rbb->save();
        return array('list' => array('status' => 1,));
    }


private function getOrderDetailsByUserId($data)
    {
        $order = ORM::factory('Orderdetail')->where('id','=',$data['order_id'])->where('user_id','=',$this->_user->get_user_id())->where('iscancelled', '!=', 1)->find();
        if (!$order->loaded()) {
          return array('list' => array('status' => 9));

        }


        // $today = new DateTime('now');
        // $odate = DateTime::createFromFormat('Y-m-d H:i:s', $order->orderdate);//->format('Y-m-d');
        // $diff=date_diff($today,$odate);
        // // Kohana::$log->add(Log::DEBUG, "vkt");
        // // Kohana::$log->add(Log::DEBUG, $order->orderdate);
        // // Kohana::$log->add(Log::DEBUG, Debug::vars($odate));
        // // Kohana::$log->add(Log::DEBUG, $odate);
        // // Kohana::$log->add(Log::DEBUG, $diff->days);


        // if ($diff->days > 180){
        //     //return array('list' => array('status' => 10));
        //     $status = 10;
        // }else{
        //     $status = 0;
        // }



        // Get the Order Items
            $items = ORM::factory('Order')->where('orderdetail_id', '=', $order->id)->find_all();
            $orderitems = array();
            foreach ($items as $i) {
                $i = $i->as_array();
                $p = ORM::factory('Productitem')->getProductByID($i['productitems_id'])[0];
                $currentorder = array();
                $currentorder['amount'] = $i['price'];
                if ($i['iscancelled'] == '1') {
                        $currentorder['status'] = "Cancelled";
                        $currentorder['iscancelled'] = $i['iscancelled'];
                } else {
                $currentorder['status'] = $i['isdelivered'] == '1' ? "Delivered" : (($i['isshipped'] == '1') ? "Shipped" : (($i['isqcdone'] == '1') ? "Order Confirmed" : "Order Placed"));
                }
                if ($i['iscancelled'] == '1') {
                    $currentorder['date'] = $i['canceldate'];
                    $currentorder['action'] = "Cancellation Processed";
                } elseif ($i['isdelivered'] == '1') {
                    $currentorder['date'] = $i['deliverydate'];
                    $currentorder['action'] = "Order Executed";
                } elseif ($i['isshipped'] == '1') {
                     $currentorder['action'] = "Mark Delivered";
                    $currentorder['date'] = $i['shippeddate'];
                } elseif($i['isqcdone'] == '1') {
                    $currentorder['action'] = "Mark Shipped";
                    $currentorder['date'] = $i['qcdonedate'];
                } else {
                    $currentorder['date'] = $order->orderdate;
                    $currentorder['action'] = "Mark Confirmed";
                }
                if ($i['iscancelled'] == '1') {
                    $currentorder['canceldate'] = $i['canceldate'];
                }
                if ($i['isshipped'] == '1') {
                    $currentorder['shippeddate'] = $i['shippeddate'];
                }
                if ($i['isdelivered'] == '1') {
                    $currentorder['deliverydate'] = $i['deliverydate'];
                }

                $currentorder['ispaid'] = $i['ispaid'];
                $currentorder['productname'] = $p['product_title'];
                $currentorder['productdescription'] = $p['product_description'];
                $currentorder['productimage'] = $p['images'][0]['image_full'];

                array_push($orderitems, $currentorder);
            }


            $addr = json_decode($order->deliveryaddress);
            $username = ORM::factory('User', $order->user_id)->firstname;

            $items = array('id' => $order->id, 'order_id' => $order->id, 'amount' => $order->amount, 'orderdate' => $order->orderdate, 'items' => $orderitems, 'itemcount' => count($orderitems), 'deliveryaddress' => $addr, 'user_id' => $order->user_id, 'username' => $username);

             return array('list' => array('status' => 1, 'order' => $items));

    }



    //Coupon

   //Coupon

    private function createCoupon($data){
        $data = json_decode($data['input'], 1);
        $c = ORM::factory('Coupon')->values($data)->save();
        $c->date = date('Y-m-d H:i:s');
        $c->save();
        return array('status' => 1);
    }

    private function getCouponDetails($data) {
        $coupons = ORM::factory('Coupon')->order_by('id', 'DESC')->find_all();
        $cpdetails = array();
        foreach ($coupons as $cp){
            array_push($cpdetails, $cp->as_array());
        }
        return array('status' =>1, 'list' => $cpdetails);
    }


    private function getCouponDiscount($data) {
        $coupon = ORM::factory('Coupon')->where('code', '=', $data['coupon'])->find_all();
        $coupon = $coupon[0];
        if(empty($coupon)) {
            return array( 'list' => array('status' => 9));
        }
        $rc = array();
        if ($coupon->isexpired == 1) {
            return array( 'list' => array('status' => 9));
        }

        // Check if coupon is applicable for specific users
        if ($coupon->userid != NULL && $coupon->userid != $this->_user->get_user_id()) {
            return array( 'list' => array('status' => 9));
        }
        $rc['id'] = $coupon->id;
        $rc['discount'] = $coupon->value;
        $rc['ispercentage'] = $coupon->ispercentage;
        return array( 'list' => array('status' => 1, 'coupon' => $rc));
    }

     private function markCouponExpired($data){
        $ec = ORM::factory('Coupon', $data['id']);
        $ec->isexpired = 1;
        $ec->save();
        return array('status' => 1);
    }

    //Delivery Status
    private function getDeliveryDetails($data) { // NOT USED
        $slts = ORM::factory('Deliveryslot')->where('active', '=', 1 )->order_by('date1', 'DESC')->find_all();
        $slotdetails = array();
        foreach ($slts as $st){
            array_push($slotdetails, $st->as_array());
        }
        return array('status' =>1, 'list' => $slotdetails);
    }

    private function getDelSlotInfo($data)
    {
        $slots = DB::select()->from("view_deliveryinfo")->execute()->as_array();
        $rslots = array();
        foreach ($slots as $s) {
            $order = ORM::factory('Order', $s['suborder']);
            $s['shipped'] = $order->isshipped;
            $s['delivered'] = $order->isdelivered;
            $s['onthespotreturn'] = $order->onthespotreturn;
            if (!isset($rslots[$s['date1']])) {
                $rslots[$s['date1']] = array();
            }
            array_push($rslots[$s['date1']], $s);
        }
        return array('status' =>1, 'list' => $rslots);
    }

    private function markReturned($data){
        $order = ORM::factory('Order', $data['id']);
        $order->onthespotreturn = 1;
        $order->save();
        return array('status' => 1);
    }

    private function getConversion($data)
    {
        $conv = DB::select()->from("view_customerorders")->limit(50)->execute()->as_array();
        $rconv = array();
        foreach ($conv as $c) {
            $rc = array();
            $rc['orderid'] = $c['Mainorder'];
            $user = ORM::factory('User', $c['UserId']);
            $rc['mobile'] = $user->phonenumber;
            $rc['name'] = $user->firstname;
            $rc['cat'] = $c['Category'];
            $rc['saleprice'] = $c['Price'];
            $rc['date'] = $c['Date'];
            // Check if the mobile number is there in customer calls table, if so get the owner
            $call = ORM::factory('Customercall')->where('mobile', '=', $user->phonenumber)->find();
            if ($call->loaded()) {
                $rc['conversionby'] = $call->addedby;
                $rc['source'] = $call->source;
            }
            $vis = ORM::factory('Visitor')->where('ip','=', $user->regip)->find();
            if ($vis->loaded()) {
                $rc['referrer'] = $vis->referrer;
            }
            array_push($rconv,$rc);
        }
        return array('status' =>1, 'list' => $rconv);    
    }


    private function _placeNewOrder($data){
        $user = ORM::factory('User', $data['userid']);
        $ord = ORM::factory('Order');
        $ordetail = ORM::factroy('Orderdetail');
        $prod = ORM::factory('Productitem',$data['proudctid']);
        $ordetail->amount = $prod['saleprice'];
        $ordetail->orderdate = date();  //today's date;
        $ordetail->userid = $data['userid'];
        $ordetail->deliveryaddress =   // Delivery address
        $ordetail->save();
        $ord->price = $prod->salerpreice;
        $ord->shippeddate =  // $ordetail->orderdate + 1
        $ord->isdelivered = 1;
        $ord->productitem_id = $data['productid'];
        $ord->orderdetail_id =   //current orderid
        $ord->save();

     }

    //Callback
     private function getCallBack($data) {
        $limit = 100;
        $cb= ORM::factory('Callback')->order_by('id', 'DESC')->limit($limit)->find_all();
        $cbdetails = array();
        foreach ($cb as $call) {
            $call->productid = ORM::factory('Productitem', $call->productid)->title . '(' . $call->productid . ')';
            array_push($cbdetails, $call->as_array());
        }
        return array('status' =>1, 'list' => $cbdetails);

    }


       private function updateCallBack($data)
    {
        
        $cb = ORM::factory('Callback', $data['id']);
        $cb->comments = $data['comments'];
        $cb->called = $data['called'];
        $cb->name = $data['name'];
        $cb->updatetime = date('Y-m-d H:i:s');
        $cb->save();
        if ($cb->called == 1){
            $cc = ORM::factory('Customercall');
            $cc->mobile =  $cb->number;
            $cc->name =  $cb->name;
            $cc->comment =  $cb->comments;
            $cc->source = "Callback";
            $cc->addedby = Auth::instance()->get_user()->firstname;
            $cc->save();

        }
        return array('status' => 1);
    }




    //Filter

    private function getFilteredProducts($data){
        $products = array();
        $tps = ORM::factory('Producttag')->where('tagid', '=', $data['id'])->find_all();
        foreach($tps as $tp){
            $p = ORM::factory('Productitem')->where('id', '=', $tp->productid)->find();
            array_push($products, $p->as_array());
        }
        return array('status' =>1, 'list' => $products);
    }

    // Zest EMI functions
    private function getZestObject($data)
    {
        $zestemi = array();
        $zest = new ZestProcessor();
        $zestemi['clientid'] = $zest->getclientid();
        $zestemi['auth_token'] = $zest->gettoken('public');
        $zestemi['minamount'] = $zest->getminordertotal();
        $zestemi['maxamount'] = $zest->getmaxordertotal();
        $zestemi['logourl'] = 'assets/zestphpsdk/web/images/';
        $zestemi['emiUrl'] = $zest->emiurl();
        return array('status' =>1, 'list' => $zestemi);
    }

    private function processZestPayment($data)
    {
        $clientzest = json_decode($data['zest'], true);
        $zest = new ZestProcessor();
        $deliveryadd = ORM::factory('Address', $data['address_id'])->as_array();

        // Place a emipending order. orderProducts returns the basket of items back
        $data['pendingemi'] = 1;
        $zestorder = $this->orderProducts($data);
        Kohana::$log->add(Log::DEBUG, 'order return ' . Debug::vars($zestorder));
        $orderid = $zestorder['list']['order_id'];

        $orderdata = array(
            'merchantid' => $zest->getclientid(),
            'OrderId' => $orderid,
            'EmailAddress' => $clientzest['email'],
            'MerchantCustomerId' => $clientzest['user_id'],
            'BasketAmount' => $clientzest['total'],
            'Basket' => $zestorder['list']['basket'],
            'FullName' => $clientzest['name'],
            'AddressLine1' => $deliveryadd['address_line_one'],
            'DeliveryPostCode' => $deliveryadd['pincode'],
            'City' => $deliveryadd['city'],
            'MobileNumber' => $clientzest['mobile'],
            //'website' => 'https://www.refabd.com',
            'website' => 'https://www.refabd.com',
            'ApprovedUrl' => "https://www.refabd.com",
            'ReturnUrl' => 'https://www.refabd.com'
            );

        $result = $zest->applyloan($orderdata);
        //echo Debug::vars($result);
        //Kohana::$log->add(Log::DEBUG, 'zest return ' . Debug::vars($result));
        if ($result) {
            //$this->redirect($result
            $zr = array();
            $zr['url']  = $result;
            return array('status' => 1, 'list' => $zr);
        } else {
            return array('status' => 0);
        }
    }

    private function _getZestEMIStatus($order_id)
    {
        $zest = new ZestProcessor();
        $result =  $zest->getorderstatus($order_id);
        return $result;
    }

    private function cancelZestEmi($data)
    {
        $order_id = $data['order'];
        $zest = new ZestProcessor();
        $result = $zest->sendCancellation($order_id,'Damaged');
        return array('status' => 1, 'list' => array('result' => $result));
    }

    private function refundZestEmi($data)
    {
        $zest = new ZestProcessor();
        $result = $zest->sendRefund($data['order'],$data['amount'], 'Customer Returned');
        return array('status' => 1, 'list' => array('result' => $result));
    }

    private function deliverZestEmi($data)
    {
        $zest = new ZestProcessor();
        $result = $zest->sendDeliverNotify($data['order'],'Delivered');
        return array('status' => 1, 'list' => array('result' => $result));
    }
}

