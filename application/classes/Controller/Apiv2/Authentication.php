<?php defined('SYSPATH') or die('No direct script access.');
define("KAPSYSTEMSMSAPI", 'A962153f1df80217cfe1b5309d40f8d86');


class Controller_Apiv2_Authentication extends Controller_Rest {
	/**
	 * A Restexample model instance for all the business logic.
	 *
	 * @var Model_Restexample
	 */
	protected $_rest;


	/**
	 * Initialize the example model.
	 */
	public function before()
	{
		parent::before();
		//$this->_rest = Model_RestAPI::factory('Restexample', $this->_user);
	}



	/**
	 * Handle GET requests.
	 */
	public function action_index()
	{
		try
		{
			if($this->_params['method'] == 'whoami'){
				$this->rest_output( $this->whoami( $this->_params ) );
			}
		}
		catch (Kohana_HTTP_Exception $khe)
		{
			$this->_error($khe);
			return;
		}
		catch (Kohana_Exception $e)
		{
			$this->_error('An internal error has occurred', 500);
			throw $e;
		}
	}

	/**
	 * Handle POST requests.
	 */
	public function action_create()
	{
		try
		{
			if($this->_params['method'] == 'login'){
				$this->rest_output( $this->login( $this->_params ) );
			} elseif($this->_params['method'] == 'sendotp'){
				$this->rest_output( $this->sendotp( $this->_params ) );
			}
			elseif($this->_params['method'] == 'signupCustomer'){
				$this->rest_output( $this->signupCustomer( $this->_params ) );
			}
			elseif($this->_params['method'] == 'signupDealer'){
				$this->rest_output( $this->signupDealer( $this->_params ) );
			}
			elseif($this->_params['method'] == 'forgotPassword'){
				$this->rest_output( $this->forgotPassword( $this->_params ) );
			}elseif($this->_params['method'] == 'verifyOTP'){
				$this->rest_output( $this->verifyOTP( $this->_params ) );
			}elseif($this->_params['method'] == 'verifyAndUpdatePhoneNumber'){
				$this->rest_output( $this->verifyAndUpdatePhoneNumber( $this->_params ) );
			}elseif($this->_params['method'] == 'getOTPForForgotpwd'){
				$this->rest_output( $this->getOTPForForgotpwd( $this->_params ) );
			}elseif($this->_params['method'] == 'verifyOTPForForgotpwd'){
				$this->rest_output( $this->verifyOTPForForgotpwd( $this->_params ) );
			}elseif($this->_params['method'] == 'changePassword'){
				$this->rest_output( $this->changePassword( $this->_params ) );
			}elseif($this->_params['method'] == 'LoginOrRegister'){
				$this->rest_output( $this->LoginOrRegister( $this->_params ) );
			}

                }
                catch (ORM_Validation_Exception $e)
                {
                    $errors = $e->errors('models');
                    echo Debug::vars($errors);
                }

		catch (Kohana_HTTP_Exception $khe)
		{
			$this->_error($khe);
			return;
		}
		catch (Kohana_Exception $e)
		{
			$this->_error('An internal error has occurred', 500);
			throw $e;
                }
                	}

	private function whoami($data) {
		$role = 'user';
		if (isset($data['role'])) {
			$role = $data['role'];
		}
		if (Auth::instance()->logged_in()) {
			$user = Auth::instance()->get_user();
			if ($role == 'admin' && $user->isadmin == '0') {
				return array('list' => array('status' => 0));
			} else if ($role == 'dealer' && ($user->isdealer == '0' && $user->isadmin == '0')) {
				return array('list' => array('status' => 0));
			} else if ($role == 'user' && $user->isdealer == '1') {
				return array('list' => array('status' => 0));
			}
			$token = ORM::factory('User')->getUserToken($user->id, sha1(Request::$user_agent));
			if (count($token) == 0) {
				// User Token not found, Logout user,
				Auth::instance()->logout(TRUE, FALSE);
				return array('list' => array('status' => 0));
			}
			$token = $token[0]['token'];
			//echo Debug::Vars(sha1(Request::$user_agent));
			return array('list' => array('status' => 1, 'firstname' =>$user->firstname, 'lastname' => $user->lastname, 'token' => $token , 'mobile' => $user->phonenumber, 'user_id' => $user->id));
		} else {
			return array('list' => array('status' => 0));
		}
	}

	private function sendotp($data) {
		if (isset($data['to_non_user']) and $data['to_non_user'] == '1') {
			// Make sure that the new Phone number given is not of a registered user
			$anotheruser = ORM::factory('User')->where('phonenumber', '=', $data['phonenumber'])->find();
			if ($anotheruser->loaded()) {
				if ($anotheruser->id == $data['user_id']) {
					return array('list' => array('status' => 0, 'message' => 'Failed: Phone number not changed!!'));
				}

				return array('list' => array('status' => 0, 'message' => 'Failed: Another Account exists with this phone number!!'));
			}
		}

		$user_id = $data['user_id'];
		$mobile = $data['phonenumber'];
		// Generate a 4 character OTP
		$otp = rand(1000, 9999);

		$otpt = ORM::factory('Onetimepassword')->where('user_id', '=', $user_id)->find();
		if ($otpt->loaded()) {
			$otp = $otpt->otp;
		} else {
			ORM::factory('Onetimepassword')->save_otp(array('phonenumber' => $mobile, 'otp' => $otp, 'user_id' => $user_id), array('phonenumber', 'otp', 'user_id'));
		}

        $sms       =  "$otp is your OTP to complete the verification. Refabd";
        $sender    = "POKTIN";

        $request = Request::factory('http://trans.kapsystem.com/api/v3/index.php')->method('POST')->post(array('method' => 'sms', 'api_key' => KAPSYSTEMSMSAPI, 'to' => $mobile, 'sender' => $sender, 'message' => $sms, "custom" => $user_id));
        $response = $request->execute();
        $response = json_decode($response);
        if ($response->status == 'OK') {
           return array('list' => array('status' => 1));
        }

        return array('list' => array('status' => 0));
	}

	private function login($data) {
		$u = ORM::factory('User')->where('username', '=', $data['username'])->or_where('email', '=', $data['username'])->find();
		if ($u->loaded()) {
			if ($u->isverified == 0) {
				return array('list' => array('status' => 2, 'message' => 'User not verified'));
			}
			if (isset($data['isadmin']) && $data['isadmin'] == '1' && $u->isadmin != '1') {
				return array('list' => array('status' => 0, 'message' => 'Not authorized'));
			} else if (isset($data['isdealer']) && $data['isdealer'] == '1' && $u->isdealer != '1') {
				return array('list' => array('status' => 0, 'message' => 'Not authorized'));
			}/* else if (!isset($data['isadmin']) && !isset($data['isdealer'])) {
				if ($u->isadmin == '1' || $u->isdealer == '1') {
					return array('list' => array('status' => 0, 'message' => 'Not authorized'));
				}
			}*/
		} else {
			return array('list' => array('status' => 0, 'message' => 'Credentials dont match..'));
		}

		$success = 0;
		if (Auth::instance()->logged_in()) {
			if (Auth::instance()->get_user()->id != $u->id) {
				throw HTTP_Exception::factory(403, "Another User, :user is Already loggedin", array(':user' => Auth::instance()->get_user()->firstname));
			}
			$success = 1;
		} else {
			$success = Auth::instance()->login($data['username'], $data['password'], true);
		}

		if($success) {
			$user = Auth::instance()->get_user();
			$response = ORM::factory('User')->getUserToken($user->id, sha1(Request::$user_agent))[0];
			$user_id = $response['user_id'];
			$token = $response['token'];

			//so this was successfull, we will be adding the GCM id too now.
			$picture = '';
			if($user->picture != null){
				$s3config = Kohana::$config->load('s3')->get('default');
				$picture = $s3config['s3_url'].$user->picture;
			}
			//try {
			//	Kohana::$log->add(Log::DEBUG, 'vkt ' . $data['username'] . ' ' . $data['password'] . '. IP ' . $_SERVER['REMOTE_ADDR']);
			//} catch(Kohana_Exception $e) { }

			return array('list' => array('status' => 1, 'role' => 'customer',  'token' => $token, 'profile' => array('user_id' => $user->id, 'firstname' => $user->firstname, 'lastname' => $user->lastname, 'email' => $user->email, 'phonenumber' => $user->phonenumber, 'user_image' => $picture, 'user_country' => '')));

		} else {
			return array('list' => array('status' => 0, 'message' => 'Credentials dont match..'));
		}
	}

	private function verifyOTP($data)
	{
		$user = ORM::factory('Onetimepassword')->where('phonenumber', '=', $data['phonenumber'])->where('otp', '=', $data['otp'])->find();
		if ($user && $user->loaded()) {
			// Activate the user
			$u = ORM::factory('User', $user->user_id);
			$u->activate_user(array('isverified' =>1), array('isverified'));

			// Logs in the User
			$success = Auth::instance()->login($u->username, $data['password'], true);
			if (!$success) {
				return array('list' => array('status' => 0));
			}


			$token = ORM::factory('User')->getUserToken($user->user_id, sha1(Request::$user_agent))[0]['token'];

			// Delete OTP entry
			$user->delete();
			return array('list' => array('status' => 1, 'user_id' => $u->id, 'token' => $token));
		}

		return array('list' => array('status' => 0));
	}

	private function verifyAndUpdatePhoneNumber($data)
	{
		$user = ORM::factory('Onetimepassword')->where('phonenumber', '=', $data['phonenumber'])->where('otp', '=', $data['otp'])->find();
		if ($user && $user->loaded()) {
			// Activate the user
			$u = ORM::factory('User', $user->user_id);
			$u->activate_user(array('isverified' =>1), array('isverified'));

			// Delete OTP entry
			$user->delete();

			// Update the Phone number
			$user = Auth::instance()->get_user();
			$user->update_user(array('phonenumber' => $data['phonenumber'], 'username' => $data['phonenumber']),array('phonenumber', 'username'));

			return array('list' => array('status' => 1));
		}

		return array('list' => array('status' => 0));
	}

	private function getOTPForForgotpwd($data)
	{
		$user = ORM::factory('User')->where('username', '=', $data['username'])->or_where('email', '=', $data['username'])->find();
		if ($user->loaded()) {
			// Generate OTP
			$this->sendotp(array('user_id' => $user->id, 'phonenumber' => $user->phonenumber));

			return array('list' => array('status' => 1, 'phonenumber' => $user->phonenumber));
		}
		return array('list' => array('status' => 0));
	}

	private function verifyOTPForForgotpwd($data)
	{
		$user = ORM::factory('User')->where('username', '=', $data['username'])->or_where('email', '=', $data['username'])->find();
		if ($user->loaded()) {
			// Generate OTP
			$data['phonenumber'] = $user->phonenumber;
			$otp = ORM::factory('Onetimepassword')->where('phonenumber', '=', $data['phonenumber'])->where('otp', '=', $data['otp'])->find();
			if ($otp->loaded()) {
				// Activate the user
				$u = ORM::factory('User', $user->id);
				$u->activate_user(array('isverified' =>1), array('isverified'));

				// Delete OTP entry
				$otp->delete();
				return array('list' => array('status' => 1));
			}
		}

		return array('list' => array('status' => 0));
	}

	private function changePassword($data)
	{
		$user = ORM::factory('User')->where('username', '=', $data['username'])->or_where('email', '=', $data['username'])->find();
		if ($user->loaded()) {
			$data['password'] = $data['pwd'];
			$user->update_user($data, array('password'));
			return array('list' => array('status' => 1));
		}

		return array('list' => array('status' => 0));
	}

        private function signupCustomer($data) {
            $user = ORM::factory('User')->where('email', '=', $data['email'])->or_where('phonenumber' , '=', $data['phonenumber'])->find();
            if ($user->loaded()) {
                return array('list' => array('status' => 9, 'message' => 'User already exists, Please login!'));
            }

            $data['username'] = $data['phonenumber'];
            /*
            $isPasswordSet = 1;
            if(!isset($data['password'])){
                $data['password'] = $this->randomPassword();
                $isPasswordSet = 0;
            }
             */
            $data['confirm_password'] = $data['password'];
	    $data['regip'] = $_SERVER['REMOTE_ADDR'];

            $user = $user->create_user($data, array(
                'username',
                'password',
                'email',
                'firstname',
                'lastname',
                'phonenumber',
                'picture','regip')
            );

            $role = ORM::factory('Role')->add_user_role($user, 'login');

            // Send OTP
            $this->sendotp(array('user_id' => $user->id, 'phonenumber' => $data['phonenumber']));

            // Mark that user registered in calls table
            /*
            $call = ORM::factory('Customercall')->where('mobile', '=', $data['phonenumber'])->find();
            if ($call->loaded()) {
            	$call->registered = 1;
            	$call->save();
            }
            $ref = ORM::factory('Referral')->where('rmobile', '=', $data['phonenumber'])->find();
            if ($ref->loaded()) {
            	$ref->ruser_id = $user->id;
            	$ref->save();
            }*/

            return array('list' => array('status' => 1, 'message' => 'User registered successfully'));
        }

     private function LoginOrRegister($data)
     {
     	// Check if the user is registered.
     	$user = ORM::factory('User')->where('email', '=', $data['email'])->or_where('phonenumber' , '=', $data['phonenumber'])->find();
     	if ($user->loaded()) {
     		return array('list' => array('status' => 9));
     	} else {
	     	$data['username'] = $data['phonenumber'];
	        $data['confirm_password'] = $data['password'];
		    $data['regip'] = $_SERVER['REMOTE_ADDR'];
	        $user = $user->create_user($data, array(
	            'username',
	            'password',
	            'email',
	            'firstname',
	            'lastname',
	            'phonenumber',
	            'picture','regip')
	        );
	        $role = ORM::factory('Role')->add_user_role($user, 'login');
	        $u = ORM::factory('User')->where('username', '=', $data['username'])->find();
			$u->activate_user(array('isverified' =>1), array('isverified'));
			$success = Auth::instance()->login($u->username, $data['password'], true);
			if (!$success) {
				return array('list' => array('status' => 0));
			}
		}

		$user = ORM::factory('User')->where('username', '=', $data['phonenumber'])->find();
		$token = ORM::factory('User')->getUserToken($user->id, sha1(Request::$user_agent))[0]['token'];
		return array('list' => array('status' => 1, 'user_id' => $user->id, 'token' => $token));
     }

	private function signupDealer($data){
		$user = ORM::factory('User')->where('email', '=', $data['email'])->find();
    	if(!$user->loaded()){
    		$data['username'] = $data['phonenumber'];
    		$isPasswordSet = 1;
    		if(!isset($data['password'])){
				$data['password'] = $this->randomPassword();
				$isPasswordSet = 0;
    		}
    		$data['confirm_password'] = $data['password'];

    		$user = $user->create_user($data, array(
			 	'username',
			 	'password',
			 	'email',
			 	'firstname',
			 	'lastname',
			 	'phonenumber',
			 	'picture',
			 	'isdealer',
			 	'location',
			 	'locationlat',
			 	'locationlng',
			 	'place_id',
			 	'deliveryradius'
		 	)
		 	);


			$role = ORM::factory('Role')->add_user_role($user, 'login');

			return array('user_id' => $user->id, 'status' => 1, 'message' => 'User registered successfully');
    	}
    	else{
    		return array('user_id' => $user->id, 'status' => 2, 'message' => 'User already exists.');
    	}
    	return array('user_id' => $user->id, 'status' => 0);
	}

	private function send_email_newuser($userid)
	{
		try {
			$user = ORM::factory('User', $userid);
			if ($user->loaded()) {
				$email = View::factory('email/newuser');
				$email->user = $user->fname;
				$email->homepage = URL::base(Request::$initial, TRUE);
				//$emailbody = $email->render();

				$email = Email::factory()
				   ->subject("Welcome to Refabd")
				   ->to($user->email)
				   ->from('hello@refabd.com', "Refabd")
				   ->message($view, 'text/html')
				   ->send();
				   /*
				// Send the mail
				$mail = Email::mailer();
	            $message = Swift_Message::newInstance();
	            $message->setSubject('LinkMysport - Password Reset')
	                  ->setFrom(array('bookings@linkmysport.com' => 'LinkMySport'))
	                  ->setTo(array($user->email))
	                  ->setBody($html, 'text/html') ;

	            $isSent = $mail->send($message);*/
			}

		} catch(Exception $e){
	    	return array('status'=> 0);
	    }
	}


	private function send_email_resetpassword($userid)
	{
		# code...
	}


	private function send_email_orderplaced($orderid)
	{
		# code...
	}


	private function send_email_orderoutfordelivery($orderid)
	{
		# code...
	}


	private function send_email_orderdelivered($orderid)
	{
		# code...
	}

	private function forgotPassword($data){
		try {

			$user = ORM::factory('User')->where('email', '=', $data['username'])->or_where('username', '=', $data['username'])->find();
			if($user->loaded()) {
				$emailcontent = new View_Email_Reset;
				$emailcontent->userName = $user->username;
				$hash = md5(rand(0,1000));
				$user->update_user(array('hash' => $hash), array('hash'));
				$emailcontent->resetLink = URL::base(Request::$initial, TRUE).'Welcome/forgot02?hash='.$hash;
				$renderer = Kostache_Layout::factory();
	            $html = $renderer->render($emailcontent);

	            $email = Email::mailer();

	            $message = Swift_Message::newInstance();

	            $message->setSubject('LinkMysport - Password Reset')
	                  ->setFrom(array('bookings@linkmysport.com' => 'LinkMySport'))
	                  ->setTo(array($user->email))
	                  ->setBody($html, 'text/html') ;

	            $isSent = $email->send($message);

   	    	return array('status'=> 1, 'message' => 'Link for resetting password is sent to '.$user->email);

	        }
	    }catch(Exception $e){
	    	return array('status'=> 0);
	    }
	}

	

	private function runCurl($url, $json = 0){
		if(!$json){
			$ch = curl_init();
		    curl_setopt($ch,CURLOPT_URL,$url);
		    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
		    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,3);
			curl_setopt($ch, CURLOPT_TIMEOUT, 6); //timeout in seconds
		    $response = curl_exec($ch);
		    echo $response;
		    curl_close($ch);
		}
		else{
			$ch = curl_init();
		  	curl_setopt($ch,CURLOPT_URL, $url);
		  	curl_setopt($ch,CURLOPT_POST, 1);
		  	curl_setopt($ch,CURLOPT_POSTFIELDS, $json);
		  	curl_setopt($ch,CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
		  	curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
		  	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,3);
			curl_setopt($ch, CURLOPT_TIMEOUT, 6); //timeout in seconds
		  	$response = curl_exec($ch);
		  	$errmsg = curl_error($ch);
		  	$cInfo = curl_getinfo($ch);
		  	curl_close($ch);
		}
	  	return $response;
	}



    private function base()
    {
            return URL::base(Request::$initial, TRUE);
    }


} // End Welcome
