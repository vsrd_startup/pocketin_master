<?php defined('SYSPATH') or die('No direct script access.');


class Controller_Warehouse extends Controller
{
    public function action_index()
    {
        $main = View::factory('warehouse/main');
        $this->response->body($main->render());
    }
};
