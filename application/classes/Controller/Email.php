<?php defined('SYSPATH') or die('No direct script access.');


class Controller_Email extends Controller
{
    private function _invoice($order, $item, $user, $deliveryaddress) {
        $view = View_Pdf::factory('email/invoice');
        $view->user = $user;
        $view->order = $order;
        $view->deliveryaddress = $deliveryaddress;
        $view->homepage = "http://refabd.com/";
        $view->item = $item;
        $pdf= $view->render_as_pdf()->output();
        $invoice = DOCROOT . 'invoices/invoice_' . $user->firstname . '_OD' . $order->orderdetail_id . '.pdf';
        file_put_contents($invoice, $pdf);
        return $invoice;
        //var_dump( Kohana::$base_url);
        //$this->request->send_file(TRUE, 'invoice.pdf', array('inline' => TRUE));
    }

    public function action_order() {
        $status = 'Delivered';//$data['action'];
        $data['id'] = 1;
        $order = ORM::factory('Order', $data['id']);
        $orderdetails = ORM::factory('Orderdetail', $order->orderdetail_id);
        $item = ORM::factory('Productitem')->getProductByID($order->productitems_id)[0];

        $user = ORM::factory('User', 3);
        $deliveryaddress = json_decode($orderdetails->deliveryaddress);
        $view = View::factory('email/order');
        $view->user = $user;
        $view->order = $order;
        $view->deliveryaddress = $deliveryaddress;
        $view->homepage = "http://refabd.com/";
        $view->status = $status;
        $view->item = $item;
        $subject = "Your Order has been " . $status . ": OD:" . $data['id'] ;

        // Generate and Get the invoice
        $invoice = $this->_invoice($order, $item, $user, $deliveryaddress);

        try {
            $email = Email::factory()
                    ->subject($subject)
                    ->to($user->email)
                    ->from('sale@refabd.com', "Refabd Sale")
                    ->message($view, 'text/html')
                    ->attach_file($invoice)
                    ->send();
        } catch(Exception $e){
            echo Debug::vars($e);
        }

        // Remove the invoice
        unlink($invoice);
    }

    public function action_welcome()
    {
        try {
            $user = ORM::factory('User', 51);
            if ($user->loaded()) {
                $view = View::factory('email/welcome');
                $view->user = $user->firstname . ' ' . $user->lastname;
                $view->homepage = "http://pocketin.in/";//URL::base(Request::$initial, TRUE);
                //$emailbody = $email->render();

                $email = Email::factory()
                    ->subject("Welcome to Pocketin")
                    ->to($user->email)
                    ->from('vsrd.pockein@gmail.com', "Pocketin")
                    ->message($view, 'text/html')
                    ->send();
                                   /*
                                // Send the mail
                                $mail = Email::mailer();
                    $message = Swift_Message::newInstance();
                    $message->setSubject('LinkMysport - Password Reset')
                          ->setFrom(array('bookings@linkmysport.com' => 'LinkMySport'))
                          ->setTo(array($user->email))
                          ->setBody($html, 'text/html') ;

                                   $isSent = $mail->send($message);*/
            }

        } catch(Exception $e){
            echo Debug::vars($e);
        }
        echo "done";
    }
}
