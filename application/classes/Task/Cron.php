<?php defined('SYSPATH') or die('No direct script access.');

class Task_Cron extends Minion_Task
{
    protected $_options = array(
        'action' => NULL
    );

    /**
     * This is a demo task
     *
     * @return null
     */
    protected function _execute(array $params)
    {   
        if($params['action'] == 'pickupPush'){
            $response = $this->pickupPush();
            echo $response;
        }
        elseif($params['action'] == 'deliverPush'){
            $response = $this->deliverPush();
            echo $response;
        }
        
    }

    private function pickupPush(){
      $books = ORM::facctory('Sharedbook')->getSharedBooks();
      foreach ($books as $book) {
        $user = ORM::facctory('Notification')->where('users_id', '=', $book['users_id'])->where(DB::expr('DATE(timestamp)'), '=', DB::expr('DATE(NOW())'))->find();
        if(!$user->loaded()){
          $notification = ORM::facctory('Notification')->add_notification(array('users_id' => $book['users_id'], 'type' => 'PICKUP'), array('users_id', 'type'));
          ORM::facctory('Notificationobject')->add_notificationobject(array('notifications_id' => $notification->id, 'object_id' => $book['sharedbook_id']), array('notifications_id', 'object_id'));
          ORM::facctory('Sharedbook')->where('id', '=', $book['sharedbook_id'])->find()->update_share(array('ispickuppending' => 1), array('ispickuppending'));
        }
        else{
          ORM::facctory('Notificationobject')->add_notificationobject(array('notifications_id' => $notification->id, 'object_id' => $book['sharedbook_id']), array('notifications_id', 'object_id'));
          ORM::facctory('Sharedbook')->where('id', '=', $book['sharedbook_id'])->find()->update_share(array('ispickuppending' => 1), array('ispickuppending'));
        }
      }
    }

    private function deliverPush(){
      $books = ORM::facctory('Readlist')->where('ispending', '=', 1)->where(DB::expr('Date(deliverydate)'), '=', DB::expr('CURDATE() + INTERVAL 1 DAY'))->find_all();
      foreach ($books as $book) {
        $user = ORM::facctory('Notification')->where('users_id', '=', $book['users_id'])->where(DB::expr('DATE(timestamp)'), '=', DB::expr('DATE(NOW())'))->find();
        if(!$user->loaded()){
          $notification = ORM::facctory('Notification')->add_notification(array('users_id' => $book['users_id'], 'type' => 'PICKUP'), array('users_id', 'type'));
          ORM::facctory('Notificationobject')->add_notificationobject(array('notifications_id' => $notification->id, 'object_id' => $book['sharedbook_id']), array('notifications_id', 'object_id'));
          
        }
        else{
          ORM::facctory('Notificationobject')->add_notificationobject(array('notifications_id' => $notification->id, 'object_id' => $book['sharedbook_id']), array('notifications_id', 'object_id'));
          
        }
      }
    }

   

  private function runCurl($url, $json = 0){
    if(!$json){
      $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,3);
      curl_setopt($ch, CURLOPT_TIMEOUT, 6); //timeout in seconds
        $response = curl_exec($ch);
        curl_close($ch);
    }
    else{
      $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST, 1);
        curl_setopt($ch,CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch,CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,3);
      curl_setopt($ch, CURLOPT_TIMEOUT, 6); //timeout in seconds
        $response = curl_exec($ch);
        $errmsg = curl_error($ch);
        $cInfo = curl_getinfo($ch);
        curl_close($ch);
    }
      return $response;
  }

}
