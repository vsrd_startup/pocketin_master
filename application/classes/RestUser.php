<?php defined('SYSPATH') or die('No direct script access.');

class RestUser extends Kohana_RestUser {
    // Load the User based on the apikey passed.
    // For now apikey = token
    const GUESTAPIKEY = "pocketin_guest_api_key";
    const GUESTUSERID = -1;
    const GUESTROLE = "guest";

    protected function _find()
    {
        $token = $this->_api_key;
        $this->_id = NULL;
        $this->_roles = NULL;
        if ($token == RestUser::GUESTAPIKEY) {
            $this->_id = RestUser::GUESTUSERID;
            $this->_role = array(RestUser::GUESTROLE);
            return;
        }

        $token_model = ORM::factory('User_Token', array('token' => $token));
        if ($token_model->loaded() AND $token_model->user->loaded()) {
            // We Authenticate a User to use the API if:
            //  1. The user is already loggedin
            //  2. Making the call from the Same User Agent
            if ($token_model->user_agent === sha1(Request::$user_agent)) {
                if (Auth::instance()->logged_in() && Auth::instance()->get_user()->id == $token_model->user->id) {
                    $this->_id = $token_model->user->id;
                    $this->_roles = array('login');
                }
            }
        }
    }

    public function get_user_id()
    {
        return $this->_id;
    }
};
