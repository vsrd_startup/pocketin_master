<!DOCTYPE html>
<html ng-app="piwap" ng-cloak>
<head>
	<title>Pocketin - Warehouse</title>                   
	<link rel="stylesheet" href="assets/css/bootstrap.min.css"/>
	<header>
		<!--div class="navbar navbar-static-top" style="background-color: #f5f5f5"><a class="navbar-brand" href="" style="color:darkorange">POCKET<strong>IN</strong>- Warehouse</a>
		<span class="pull-right" style="margin:15px; color: blue" ng-show="loggedin">Hello {{name}}</span>
		</div-->
	</header>
	<style>
		form.ng-submitted .ng-invalid {
			border-color: #FA787E;
		}

	</style>
</head>

<body ng-controller="WarehouseCtrl">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
	  <form role="form" ng-show="!loggedin" ng-submit="Login()">
	    <legend>Warehouse Login</legend>
	    <div class="form-group">
	      <label for="">Mobile Number</label>
	      <input type="text" class="form-control" id="" placeholder="Enter Mobile Number" ng-model="username"></div>
	    <div class="form-group">
	      <label for="">Password</label>
	      <input type="password" class="form-control" id="" placeholder="Enter Password" ng-model="password"></div>
	    <button type="submit" class="btn btn-primary">Submit</button>
	    <p style="color: darkorange">{{error}}</p>
	  </form>
	</div>


	<div class="container-fluid" ng-show="loggedin">
	<!--Navbar -->

	<nav class = "navbar navbar-default" role = "navigation">
   
   <div class = "navbar-header">
      <a class = "navbar-brand" href = "#" style="background-color: #f5f5f5;">POCKETIN-WAREHOUSE</a>
   </div>
   
   <div>
      <ul class = "nav navbar-nav">
         <li><a href = "#addProduct" ng-click="myvalue = myvalue == true ? false : true;">Add New Product</a></li>
         <li><a href = "#Wactivities" ng-click="wvalue = wvalue == true ? false : true;">Warehouse Activities</a></li>
		 <li><a href = "#">Delivery Details</a></li>
			
      </ul>
   </div>
   
</nav>

<div class="tab-pane fade in" id="addProduct" ng-show="myvalue && !wvalue"  class="hideByDefault">
          <h4>Add Product</h4>
          <form role="form" name="wp" ng-submit="wp.$valid && addProduct();" novalidate>
		<div class="row">
			<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<label for="">Arrival Date</label>
				<input type="date" class="form-control" required ng-model="product.arrival_date">
			</div>

			<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12" >
				<label>Warehouse ID</label>
				<input type="text" class="form-control" ng-model="product.warehouse_id" required>
			</div>

			<!--div class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12" >
			<label >Website ID</label>
			<input type="text" class="form-control" ng-model="product.websiteid" required></div-->
			<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12" >
				<label >Arrived From</label>
				<select class="form-control" ng-model="product.arrived_from" required>
					<option value="Shabaz">Shabaz</option>
					<option value="Ayub">Ayub</option>
					<option value="Customer">Customer</option>
				</select>
			</div>
		</div><!-- 1st row ends here -->

		<div class="row">
			<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<label for="">Product Category</label>
				<select class="form-control" ng-model="product.product_cat" required>
					<option value="Refrigerator">Refrigerator</option>
					<option value="Washing Machine">Washing Machine</option>
					<option value="Bed">Bed</option>
				</select>
			</div>

			<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<label for="">Product Name</label>
				<input type="text" class="form-control" id="" ng-model="product.product_title" required>
			</div>

			<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<label for="">Procure price</label>
				<input type="text" class="form-control" id="" ng-model="product.procure_cost">
			</div>
		</div><!-- 2ndrow ends here -->

		<button  type="submit" class="btn btn-success">Add Product</button>
	</form>

</div>   <br>       
	<!--div class="navbar navbar-default">
    <div class="navbar-header" ><a class="navbar-brand" href="#" ><p style="background-color: #f5f5f5">Pocketin Warehouse</p></a><a class="navbar-toggle"
        data-toggle="collapse" data-target=".navbar-collapse">
        <span class="glyphicon glyphicon-bar"></span>
        <span class="glyphicon glyphicon-bar"></span>
        <span class="glyphicon glyphicon-bar"></span>
      </a>
    </div><div class="container">
        <div class="navbar-collapse collapse">
        <span class="pull-right" style="margin:15px; color: blue" ng-show="loggedin">Hello {{name}}</span>
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Add New Product</a>
                </li>
                <li><a href="#">Warehouse Activities</a>
                </li>
                <li><a href="#">Delivery Details</a>
                </li>
                
                
            </ul>
        
       </div>
    </div-->
     

     <div class="tab-pane fade in" id="Wactivities" ng-show="wvalue && !myvalue" class="hideByDefault">
          <h4>Add Warehouse Activities</h4>
          <form role="form" name="wa" ng-submit="wp.$valid && addWarehouseActivities();" novalidate>
		<div class="row">
			<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<label for="">Date</label>
				<input type="date" class="form-control" required ng-model="warehouse.date">
			</div>

        
			<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<label for="">Product Category</label>
				<select class="form-control" ng-model="warehouse.product_cat" required>
					<option value="Refrigerator">Refrigerator</option>
					<option value="Washing Machine">Washing Machine</option>
					<option value="Bed">Bed</option>
				</select>
			</div>

			<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12" >
				<label>Cleaned</label>
				<input type="text" class="form-control" ng-model="warehouse.cleaned" required>
			</div>

			<!--div class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12" >
			<label >Website ID</label>
			<input type="text" class="form-control" ng-model="product.websiteid" required></div-->
			

		</div><!-- 1st row ends here -->

		
		<div class="row">
			<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<label for="">Serviced</label>
				<input type="text" class="form-control" id="" ng-model="warehouse.serviced" required>
			</div>

			<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<label for="">Photographed</label>
				<input type="text" class="form-control" id="" ng-model="warehouse.photographed">
			</div>
		</div><!-- 2ndrow ends here -->

		<button  type="submit" class="btn btn-success">Add</button>
	</form>

</div> <br>


	<!-- statistics starts here -->
	<div class="panel-group">
		<div class="panel panel-info">
			<div class="panel-heading">Warehouse Statistics
			<span class="pull-right" style="color:blue"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="">Total Sold: </a><span class="badge">{{stat.sold}}</span></span>
			<span class="pull-right" style="color:blue">Total Stock: <span class="badge">{{stat.instock}}</span></span>
			</div>
		  <div class="panel-body">
			<div class="col-lg-2"><a href="" ng-click="searchText='Arrived'">Arrived: <span class="badge">{{stat.arrived}}</span></a></div>
			<div class="col-lg-2"><a href="" ng-click="searchText='QC Done'">QC Done: <span class="badge">{{stat.qc}}</span></a></div>
			<div class="col-lg-2"><a href="" ng-click="searchText='FCT Done'">FCT: <span class="badge">{{stat.fct}}</span></a></div>
			<div class="col-lg-2"><a href="" ng-click="searchText='Service'">Service: <span class="badge">{{stat.service}}</span></a></div>
			<div class="col-lg-2"><a href="" ng-click="searchText='RTP'">RTP: <span class="badge">{{stat.rtp}}</span></a></div>
			<div class="col-lg-2"><a href="" ng-click="searchText='RTS'">RTS: <span class="badge">{{stat.rts}}</span></a></div>
		  </div>
		</div>
	</div><!-- statistics ends here -->

	<!-- New Product Add starts with the  panel and then form -->
	<div class="panel-group" ng-show="showdetails==0"><!--div class="panel panel-success" ng-init="collapse=0"-->
	<!--div class="panel-heading" ng-click="collapse=(collapse == 1 ? 0: 1)"><a href="">Add New Product</a></div-->
	<!--div class="panel-heading" ng-click="collapse=(collapse == 1 ? 0: 1)" ><a href="">Warehouse Activities</a></div-->
	<div class="panel-body" ng-show="collapse==1">
	<form role="form" name="wp" ng-submit="wp.$valid && addProduct();" novalidate>
		<div class="row">
			<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<label for="">Arrival Date</label>
				<input type="date" class="form-control" required ng-model="product.arrival_date">
			</div>

			<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12" >
				<label>Warehouse ID</label>
				<input type="text" class="form-control" ng-model="product.warehouse_id" required>
			</div>

			<!--div class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12" >
			<label >Website ID</label>
			<input type="text" class="form-control" ng-model="product.websiteid" required></div-->
			<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12" >
				<label >Arrived From</label>
				<select class="form-control" ng-model="product.arrived_from" required>
					<option value="Shabaz">Shabaz</option>
					<option value="Ayub">Ayub</option>
					<option value="Customer">Customer</option>
				</select>
			</div>
		</div><!-- 1st row ends here -->

		<div class="row">
			<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<label for="">Product Category</label>
				<select class="form-control" ng-model="product.product_cat" required>
					<option value="Refrigerator">Refrigerator</option>
					<option value="Washing Machine">Washing Machine</option>
					<option value="Bed">Bed</option>
				</select>
			</div>

			<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<label for="">Product Name</label>
				<input type="text" class="form-control" id="" ng-model="product.product_title" required>
			</div>

			<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<label for="">Procure price</label>
				<input type="text" class="form-control" id="" ng-model="product.procure_cost">
			</div>
		</div><!-- 2ndrow ends here -->

		<button  type="submit" class="btn btn-success">Add Product</button>
	</form>
	</div><!--/div></div--><!-- form and 3 panel divs end --> <!-- add new product ends -->


	<div class="panel-group" ng-show="showdetails==0"><div class="panel panel-default">
	<div class="panel-heading">Products</div>
	<div style="padding:10px"><label>Search: <input ng-model="searchText"></label></div>
	<div class="panel-body">
		<table class="table table-hover">
		    <thead>
		      <tr>
		        <!--th>Image</th-->
		        <th><a href="" ng-click="orderbypred='warehouse_id'">Warehouse ID</a></th>
		        <th><a href="" ng-click="orderbypred='stage'">Status</a></th>
		        <th><a href="" ng-click="orderbypred='product_id'">Website ID</a></th>
		        <th><a href="" ng-click="orderbypred='arrival_date'">Arrival Date</a></th>
		        <th><a href="" ng-click="orderbypred='product_cat'">Product Cat</a></th>
		        <th><a href="" ng-click="orderbypred='product_title'">Product Name</a></th>
		        <th><a href="" ng-click="orderbypred='arrived_from'">From</a></th>
		        <th><a href="" ng-click="orderbypred='procure_cost'">Procure Cost</a></th>
		        <th><a href="" ng-click="orderbypred='saleprice'">Sell Price</a></th>
		        <!--th>Action</th-->
		        <!--th>QC Report</th-->
		      </tr>
		    </thead>
		    <tbody>
		      <tr ng-repeat="p in products|orderBy:orderbypred|filter:searchText">
		      	<!--td><img height=50px ng-src="assets/images/products/thumbnail/17_1.jpg"></td-->
		      	<td><a href="" ng-click="showProductDetails(p)">{{p.warehouse_id}}</a></td>
		        <td>{{p.stage}}</td>
		        <td>{{p.product_id}}</td>
		        <td>{{p.arrival_date | date:'MMM dd'}}</td>
		        <td>{{p.product_cat}}</td>
		        <td>{{p.product_title}}</td>
		        <td>{{p.arrived_from}}</td>
		        <td>{{p.procure_cost}}</td>
		        <td>{{p.saleprice}}</td>
		        <!--td><a href="">Mark QC Done</a></td>
		        <td><img height=50px ng-src="assets/images/products/thumbnail/17_1.jpg"></td-->
		      </tr>
		    </tbody>
		  </table>
	</div></div></div>

	<!-- Product detail panel -->
	<div class="panel-group" ng-show="showdetails==1"><div class="panel panel-default">
		<div class="panel-heading">Product Details<button type="button" class="close" ng-click="showdetails=0"><span class="pull-right">X</span></button></div>
	<div class="panel-body">
			<form role="form" name="mp" ng-submit="mp.$valid && modifyProduct();" novalidate>
				<div class="row">
					<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
						<label for="">Arrival Date</label>
						<input type="text" class="form-control" required ng-model="dp.arrival_date">
					</div>

					<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12" >
						<label>Warehouse ID</label>
						<input type="text" class="form-control" ng-model="dp.warehouse_id" required>
					</div>

					<!--div class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12" >
					<label >Website ID</label>
					<input type="text" class="form-control" ng-model="product.websiteid" required></div-->
					<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12" >
						<label >Arrived From</label>
						<select class="form-control" ng-model="dp.arrived_from" required>
							<option value="Shabaz">Shabaz</option>
							<option value="Ayub">Ayub</option>
							<option value="Customer">Customer</option>
						</select>
					</div>
				</div><!-- 1st row ends here -->

				<div class="row">
					<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
						<label for="">Product Category</label>
						<select class="form-control" ng-model="dp.product_cat" required>
							<option value="Refrigerator">Refrigerator</option>
							<option value="Washing Machine">Washing Machine</option>
							<option value="Bed">Bed</option>
						</select>
					</div>

					<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
						<label for="">Product Name</label>
						<input type="text" class="form-control" id="" ng-model="dp.product_title" required>
					</div>

					<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
						<label for="">Procure price</label>
						<input type="text" class="form-control" id="" ng-model="dp.procure_cost">
					</div>
				</div><!-- 2ndrow ends here -->

				<div class="row">
					<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
						<label for="">Stage</label>
						<select class="form-control" ng-model="dp.stage" required>
							<option ng-value="stage.stage" ng-repeat="stage in stages">{{stage.stage}}</option>
						</select>
					</div>

					<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
						<label for="">Sale price</label>
						<input type="text" class="form-control" id="" ng-model="dp.saleprice">
					</div>

					<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
						<label for="">Product Condition</label>
						<textarea class="form-control" rows="3" ng-model="dp.product_condition"></textarea>
					</div>
				</div><!-- 3rd row ends here -->

				<!--button type="button" class="btn btn-default">Upload Image</button>
				<button type="button" class="btn btn-default">Upload QC Report</button>
				<br><br><br-->

				<button  type="submit" class="btn btn-success" ng-confirm-click="Are you sure to change the state?">Modify Product</button>
				<p style="color:darkorange" ng-show="showmstat">Product Modified Successfully</p>
			</form>
	</div></div></div> <!-- panel divs 3 close here -->

	</div><!-- container ends here -->
	<script src="assets/bower_components/angular/angular.min.js"></script>
	<script src="assets/bower_components/angular-cookies/angular-cookies.min.js"></script>
	<script src="assets/js/warehouse/wapp.js"></script>
</body>
</html>




