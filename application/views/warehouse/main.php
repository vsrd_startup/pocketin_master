<!DOCTYPE html>
<html ng-app="piwap" ng-cloak>
<head>
	<title>Warehouse - Refabd</title>
	<link rel="stylesheet" href="assets/css/bootstrap.min.css"/>
	<header>
		<div class="navbar navbar-static-top" style="background-color: #f5f5f5"><a class="navbar-brand" href="" style="color:darkorange">REFABD- Warehouse</a>
		<span class="pull-right" style="margin:15px; color: blue" ng-show="loggedin">Hello {{name}}</span>
		</div>
	</header>
	<style>
		form.ng-submitted .ng-invalid {
			border-color: #FA787E;
		}
		.well {
			background-color: #ddd;
			border-radius: 25px;
		}
	</style>
</head>

<body ng-controller="WarehouseCtrl">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
	  <form role="form" ng-show="!loggedin" ng-submit="Login()">
	    <legend>Warehouse Login</legend>
	    <div class="form-group">
	      <label for="">Mobile Number</label>
	      <input type="text" class="form-control" id="" placeholder="Enter Mobile Number" ng-model="username"></div>
	    <div class="form-group">
	      <label for="">Password</label>
	      <input type="password" class="form-control" id="" placeholder="Enter Password" ng-model="password"></div>
	    <button type="submit" class="btn btn-primary">Submit</button>
	    <p style="color: darkorange">{{error}}</p>
	  </form>
	</div>


	<div class="container-fluid" ng-show="loggedin">

	<!-- New Product Add starts with the  panel and then form -->
	<div class="panel-group" ng-show="showdetails==0"><div class="panel panel-default" ng-init="collapse_product=collapse_proc=0">
	<div class="panel-heading text-center panel-relative">
		<button type="button" class="btn btn-link " ng-click="collapse_product=(collapse_product == 1 ? 0: 1);collapse_proc=0;collapse_proc_view=0;collapse_exp=0;collapse_exp_view=0;"> Add New Product</button>
		<button type="button" class="btn btn-link" ng-click="collapse_proc=(collapse_proc == 1 ? 0: 1);collapse_product=0;collapse_proc_view=0;collapse_exp=0;collapse_exp_view=0;"> Add Procurement</button>
		<!--button type="button" ng-click="collapse_proc_view=(collapse_proc_view == 1 ? 0: 1);collapse_product=0;collapse_proc=0"> View Procurement</button-->
		<button type="button" class="btn btn-link" ng-click="collapse_exp=(collapse_exp == 1 ? 0: 1);collapse_product=0;collapse_proc_view=0;collapse_proc=0;collapse_exp_view=0;";> Add Expense</button>
		<button type="button" class="btn btn-link" ng-click="getProcurementDetails();collapse_proc_view=(collapse_proc_view == 1 ? 0: 1);collapse_proc=0;collapse_product=0;collapse_exp=0;collapse_exp_view=0;"> View Procurements</button>
		<button type="button" class="btn btn-link" ng-click="getExpenseDetails();collapse_exp_view=(collapse_exp_view == 1 ? 0: 1);collapse_proc=0;collapse_product=0;collapse_exp=0;collapse_proc_view=0;"> View Expenses</button>

	</div>
	<div class="panel-body">
	<form role="form" name="wp" ng-submit="wp.$valid && addProduct();" novalidate ng-show="collapse_product==1">
		<div class="row">
			<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<label for="">Arrival Date</label>
				<input type="date" class="form-control" required ng-model="product.arrival_date">
			</div>

			<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12" >
				<label>Warehouse ID</label>
				<input type="text" class="form-control" name="warehouseid" ng-model="product.warehouse_id" ng-pattern="/^[a-zA-Z0-9]*$/" ng-message = " Input error" required>
				<span style="color:red" ng-show="wp.warehouseid.$error.pattern">Avoid Space and Special characters!</span>				
			</div>

			<!--div class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12" >
			<label >Website ID</label>
			<input type="text" class="form-control" ng-model="product.websiteid" required></div-->
			<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12" >
				<label >Arrived From</label>
				<select class="form-control"  required ng-model="product.arrived_from">
					<option value="{{vendor.id}}" ng-repeat="vendor in vendors">{{vendor.name}}</option>
				</select>
				<!--<select class="form-control" ng-model="product.arrived_from" required>
					<option value="Shabaz">Shabaz</option>
					<option value="Ayub">Ayub</option>
					<option value="Royal Oak">Royal Oak</option>
					<option value="Urban Ladder">Urban Ladder</option>
					<option value="Flipkart">Flipkart</option>
					<option value="Customer">Customer</option>
					<option value="Other">Other</option>
				</select>-->
			</div>
		</div><!-- 1st row ends here -->

		<div class="row">
			<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<label for="">Product Category</label>
				<select class="form-control"   ng-model="product.product_cat" required>
					<option value="{{cat.id}}" ng-repeat="cat in category"  >{{cat.name}}</option>
				</select>
				<!-- <select class="form-control" ng-model="product.product_cat" required>
					<option value="Refrigerator">Refrigerator</option>
					<option value="Washing Machine">Washing Machine</option>
					<option value="Tv">Tv</option>
					<option value="Laptop">Laptop</option>
					<option value="Microwave Oven">Microwave Oven</option>
					<option value="Bed">Bed</option>
					<option value="Sofa">Sofa</option>
					<option value="Tv Stand">Tv Unit</option>
					<option value="Dining Table">Dining Table</option>
					<option value="Coffee Table">Coffee Table</option>
					<option value="Dining Chair">Dining Chair</option>
					<option value="Office Chair">Office Chair</option>
					<option value="Recliner">Recliner</option>
					<option value="Shoe Rack">Shoe Rack</option>
					<option value="Air Conditioner">Air Conditioner</option>
					<option value="Miscellaneous">Miscellaneous</option>
				</select> -->
			</div>
			<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<label for="">Product Name</label>
				<input type="text" class="form-control" id="" ng-model="product.product_title" required>
			</div>

			<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<label for="">Model Number</label>
				<input type="text" class="form-control" id="" ng-model="product.modelno">
			</div>
		</div><!-- 2ndrow ends here -->
		<div class="row">
			<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<label for="">Procure price</label>
				<input type="text" class="form-control" id="" ng-model="product.procure_cost">
			</div>
          <div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12" style="margin-top: 30px;">

              <label>
                <input type="checkbox" name="" id="input" value="" ng-model="product.unboxed">&nbsp Unboxed</label>
          </div>
        </div>
		<button  type="submit" class="btn btn-success">Add Product</button>
	</form>
	</div>

<!-- procurement details added by user in this session -->
  <div ng-show="prs.length != 0" class="well" style="max-height: 500px;overflow: auto;">
    <br>
        <table class="table" >
          <thead>
            <tr>
              <th>Date</th>
              <th>From</th>
              <th>Description</th>
              <th>Quantity</th>
              <th>Cost</th>
        </tr>
      </thead>
      <tbody>
        <tr ng-repeat="p in prs">
          <td>{{p.date | todate | date:"MMM dd, yyyy" }}</td>
          <td>{{p.from}}</td>
          <td><pre>{{p.description}}</pre></td>
          <td>{{p.quantity}}</td>
          <td>{{p.cost}}</td>
        </tr>
  		</tbody>
		</table>
	</div>


	<!--Procurement Form begins here  -->
	<form  class= "form" role="form" name="p" ng-submit="p.$valid && addProcurement();" novalidate ng-show="collapse_proc==1">
		<div class="row container-fluid">
			<div class="form-group col-lg-2 ">
				<label for="">Procure Date</label>
				<input type="date" class="form-control" id="" ng-model="pr.date" required>
			</div>
			<div class="form-group col-lg-2" >
				<label>Procured From</label>
				<!-- <input type="text" class="form-control" ng-model="pr.from" required> -->
				<select class="form-control"  required ng-model="pr.from">
							<option value="{{vendor.id}}" ng-repeat="vendor in vendors">{{vendor.name}}</option>
						</select>
			</div>

			<div class="form-group col-lg-4" >
				<label>Item Description(Enter individual item with price)</label>
				<textarea class="form-control" rows="4" ng-model="pr.description" required></textarea>
			</div>
			<div class="form-group col-lg-1">
				<label for="">Total Quantity</label>
				<input type="number" class="form-control" id="" ng-model="pr.quantity" required>
			</div>
			<div class="form-group col-lg-2">
				<label for="">Total Cost</label>
				<input type="number" class="form-control" id="" ng-model="pr.cost" required>
			</div>
		<button  type="submit" class="btn btn-success col-lg-1" style="margin-top:25px" ng-disabled="pr.disablebutton">Add</button>
		</div>
	</form>

	<!--Expense Form  -->
	<form  class= "form" role="form" name="expense" ng-submit="expense.$valid && addExpense();" novalidate ng-show="collapse_exp==1">
		<div class="row container-fluid">
			<div class="form-group col-lg-2 ">
				<label for="">Date</label>
				<input type="date" class="form-control" id="" ng-model="ex.date" required>
			</div>
			<div class="form-group col-lg-2" >
				<label>Expense Type</label>
				<input type="text" class="form-control" ng-model="ex.expense_type" required>
			</div>

			<div class="form-group col-lg-3" >
				<label>Description</label>
				<textarea class="form-control" rows="3" ng-model="ex.description" required></textarea>
			</div>
			<div class="form-group col-lg-2">
				<label for="">Amount</label>
				<input type="number" class="form-control" id="" ng-model="ex.amount" required>
			</div>
			<div class="form-group col-lg-2">
				<label for="">By</label>
				<input type="text" class="form-control" id="" ng-model="ex.by" required>
			</div>
		<button  type="submit" class="btn btn-success col-lg-1" style="margin-top:25px" ng-disabled="pr.disablebutton">Add</button>
		</div>
	</form>


	<div ng-show = "collapse_proc_view == 1">

    <br>
        <table class="table" >
          <thead>
            <tr>
              <th>Date</th>
              <th>From</th>
              <th>Description</th>
              <th>Quantity</th>
              <th>Cost</th>
        </tr>
      </thead>
      <tbody>
        <tr ng-repeat="procurement in procurements">
          <td>{{procurement.date  | todate | date:"MMM dd, yyyy"}}</td>
          <td>{{procurement.from}}</td>
          <td><pre>{{procurement.description}}</pre></td>
          <td>{{procurement.quantity}}</td>
          <td>{{procurement.cost}}</td>
        </tr>
  </tbody>
</table>
</div>

<div ng-show = "collapse_exp_view == 1">

    <br>
        <table class="table" >
          <thead>
            <tr>
              <th>Date</th>
              <th>Type</th>
              <th>Description</th>
              <th>By</th>
              <th>Amount</th>
        </tr>
      </thead>
      <tbody>
        <tr ng-repeat="expense in expenses">
          <td>{{expense.date  | todate | date:"MMM dd, yyyy"}}</td>
          <td>{{expense.expense_type}}</td>
          <td>{{expense.description}}</td>
          <td>{{expense.by}}</td>
          <td>{{expense.amount}}</td>
        </tr>
  </tbody>
</table>
</div>

	</div></div><!--  panel divs end --> <!-- add new product ends -->


	<!-- statistics starts here -->
	<div class="well well-lg">
	  <!-- <div style="padding-bottom: 18px;"> -->
	  <div>
		<span class="pull-left" >Warehouse Statistics</span><br><br>
			<div class="col-lg-2"><a href="" ng-click="searchText='Arrived'">Arrived: <span class="badge">{{stat.arrived}}</span></a></div>
			<div class="col-lg-2"><a href="" ng-click="searchText='QC Done'">QC Done: <span class="badge">{{stat.qc}}</span></a></div>
			<div class="col-lg-2"><a href="" ng-click="searchText='FCT Done'">FCT: <span class="badge">{{stat.fct}}</span></a></div>
			<div class="col-lg-2"><a href="" ng-click="searchText='Service'">Service: <span class="badge">{{stat.service}}</span></a></div>
			<div class="col-lg-2"><a href="" ng-click="searchText='RTP'">RTP: <span class="badge">{{stat.rtp}}</span></a></div>
			<div class="col-lg-2"><a href="" ng-click="searchText='RTS'">RTS: <span class="badge">{{stat.rts}}</span></a></div>
		</div>
		<div style="padding-bottom: 31px;">
		 <br><br><span class="pull-left" >Categorywise Statistics [Unsold]</span><hr> <br>
			<div class="col-lg-2"><a href="" ng-click="searchText='Refrigerator'">Fridge: <span class="badge">{{stat.Fridge}}</span></a></div>
			<div class="col-lg-2"><a href="" ng-click="searchText='Washing Machine'">WM: <span class="badge">{{stat.wm}}</span></a></div>
			<div class="col-lg-2"><a href="" ng-click="searchText='Bed'">Bed: <span class="badge">{{stat.bed}}</span></a></div>
			<div class="col-lg-2"><a href="" ng-click="searchText='Air Conditioner'">Ac: <span class="badge">{{stat.ac}}</span></a></div>
			<div class="col-lg-2"><a href="" ng-click="searchText='Office Chair'">Office Chair: <span class="badge">{{stat.chair}}</span></a></div>
			<div class="col-lg-2"><a href="" ng-click="searchText='Laptop'">Laptop: <span class="badge">{{stat.laptop}}</span></a></div>
		</div>
	  <!-- </div> -->
	</div>
	<!-- Statistics ends -->


	<!-- <div class="panel-group">
		<div class="panel panel-default">
			<div class="panel-heading"><b>Warehouse Statistics</b> -->
			<!--span class="pull-right" style="color:blue"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="">Total Sold: </a><span class="badge">{{stat.sold}}</span></span-->
			<!-- <span class="pull-right" style="color:blue">Total Stock: <span class="badge">{{stat.instock}}</span></span>
			</div>
		  <div class="panel-body">
			<div class="col-lg-2"><a href="" ng-click="searchText='Arrived'">Arrived: <span class="badge">{{stat.arrived}}</span></a></div>
			<div class="col-lg-2"><a href="" ng-click="searchText='QC Done'">QC Done: <span class="badge">{{stat.qc}}</span></a></div>
			<div class="col-lg-2"><a href="" ng-click="searchText='FCT Done'">FCT: <span class="badge">{{stat.fct}}</span></a></div>
			<div class="col-lg-2"><a href="" ng-click="searchText='Service'">Service: <span class="badge">{{stat.service}}</span></a></div>
			<div class="col-lg-2"><a href="" ng-click="searchText='RTP'">RTP: <span class="badge">{{stat.rtp}}</span></a></div>
			<div class="col-lg-2"><a href="" ng-click="searchText='RTS'">RTS: <span class="badge">{{stat.rts}}</span></a></div>
		  </div>
		</div>
	</div> --><!-- statistics ends here -->

	<!-- <div class="panel-group">
		<div class="panel panel-default">
			<div class="panel-heading"><b>Categorywise Statistics [Unsold]</b> -->
			<!--span class="pull-right" style="color:blue"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="">Total Sold: </a><span class="badge">{{stat.sold}}</span></span-->
			<!-- <span class="pull-right" style="color:blue">Total Stock: <span class="badge">{{stat.instock}}</span></span>
			</div>
		  <div class="panel-body">
			
			<div class="col-lg-2"><a href="" ng-click="searchText='Refrigerator'">Fridge: <span class="badge">{{stat.Fridge}}</span></a></div>
			<div class="col-lg-2"><a href="" ng-click="searchText='Washing Machine'">WM: <span class="badge">{{stat.wm}}</span></a></div>
			<div class="col-lg-2"><a href="" ng-click="searchText='Bed'">Bed: <span class="badge">{{stat.bed}}</span></a></div>
			<div class="col-lg-2"><a href="" ng-click="searchText='Air Conditioner'">Ac: <span class="badge">{{stat.ac}}</span></a></div>
			<div class="col-lg-2"><a href="" ng-click="searchText='Office Chair'">Office Chair: <span class="badge">{{stat.chair}}</span></a></div>
			<div class="col-lg-2"><a href="" ng-click="searchText='Laptop'">Laptop: <span class="badge">{{stat.laptop}}</span></a></div>
		  </div>
		</div>
	</div> --><!-- statistics ends here -->





	<div class="panel-group" ng-show="showdetails==0"><div class="panel panel-default">
	<div class="panel-heading"><b>Products</b></div>
	<!--div style="padding:10px"><label>Search: <input ng-model="searchText"></label></div-->
	<div>
		<form style="margin-top:10px; margin-left:10px" class="form-inline" name="sp" ng-submit = "sp.$valid && searchProduct()">
		<label for="">Filter Result:</label>
			<!--select ng-model="srchfield" class="form-control">
				<option value="warehouse_id">Warehouse ID</option>
				<option value="product_id">Website ID</option>
				<option value="product_title">Name</option>
				<option value="product_cat">Category</option>
				<option value="saleprice">Sale Price</option>
				<option value="stage">Status</option>

			</select-->
		<input type="text" class="form-control" ng-model="searchText" required>
		<!--button type="submit"  class="btn btn-default">Searchhhh</button-->
		</form>
	</div>
	<div class="panel-body">
	<button type="button" class="btn btn-success btn-sm pull-right" style="margin-top: -40px;"export-to-csv>Download</button>
		<table class="table table-hover">
		    <thead>
		      <tr>
		        <!--th>Image</th-->
		        <!--th><a href="" ng-click="orderbypred='id'">ID</a></th-->
		        <th><a href="" ng-click="orderbypred='warehouse_id'">Warehouse ID</a></th>
		        <th><a href="" ng-click="orderbypred='stage'">Status</a></th>
		        <th><a href="" ng-click="orderbypred='product_id'">Website ID</a></th>
		        <th><a href="" ng-click="orderbypred='arrival_date'">Arrival Date</a></th>
		        <th><a href="" ng-click="orderbypred='product_cat'">Product Cat</a></th>
		        <th><a href="" ng-click="orderbypred='product_title'">Product Name</a></th>
		        <th><a href="" ng-click="orderbypred='modelno'">Model</a></th>
		        <th><a href="" ng-click="orderbypred='arrived_from'">From</a></th>
		        <th><a href="" ng-click="orderbypred='procure_cost'">Procure Cost</a></th>
		        <th><a href="" ng-click="orderbypred='saleprice'">Sale Price</a></th>
		        <th><a href="" ng-click="orderbypred='timestamp'">Last Updated on</a></th>
		        <!--th>Action</th-->
		        <!--th>QC Report</th-->
		      </tr>
		    </thead>
		    <tbody>
		      <tr ng-repeat="p in products|orderBy:orderbypred|filter:searchText" ng-click="showProductDetails(p)" style="cursor: pointer;">
		      	<!--td><img height=50px ng-src="assets/images/products/thumbnail/17_1.jpg"></td-->
		      	<!--td>{{p.id}}</td-->
		      	<td>{{p.warehouse_id}}</td>
		        <td>{{p.stage}}</td>
		        <td>{{p.product_id}}</td>
		        <td>{{p.arrival_date | date:'MMM dd, y  '}}</td>
		         <td>{{p.product_cat}}</td>
		        <td>{{p.product_title}}</td>
		        <td>{{p.modelno}}</td>
		        <td>{{p.arrived_from}}</td>
		        <td>{{p.procure_cost}}</td>
		        <td>{{p.saleprice}}</td>
		        <td>{{p.timestamp | todate | date:"MMM d, y, h:mm a" }}</td>
		        


		        <!--td><a href="">Mark QC Done</a></td>
		        <td><img height=50px ng-src="assets/images/products/thumbnail/17_1.jpg"></td-->
		      </tr>
		    </tbody>
		  </table>
		  <!--div class="row pull-right" style="margin-top: 10px; margin-bottom: 10px;" ng-show="1">
      <button type="button" class="btn btn-primary" ng-click="currentpage = (currentpage == 0 ? 0 : currentpage - 1);getAllProducts();">Previous</button>
      <button type="button" class="btn btn-primary" style="padding-left: 10px;padding-right: 10px;margin-right: 10px;" ng-click="currentpage = currentpage + 1; getAllProducts();">Next</button>
  </div-->
	</div></div></div>

	<!-- Product detail panel -->
	<div class="panel-group" ng-show="showdetails==1"><div class="panel panel-default">
		<div class="panel-heading">Product Details<button type="button" class="close" ng-click="showdetails=0"><span class="pull-right">X</span></button></div>
	<div class="panel-body">
			<form role="form" name="mp" ng-submit="mp.$valid && modifyProduct();" novalidate>
				<div class="row">
					<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
						<label for="">Arrival Date</label>
						<input type="text" class="form-control" required ng-model="dp.arrival_date">
					</div>

					<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12" >
						<label>Warehouse ID</label>
						<input type="text" class="form-control" ng-model="dp.warehouse_id" required>
					</div>

					<!--div class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12" >
					<label >Website ID</label>
					<input type="text" class="form-control" ng-model="product.websiteid" required></div-->
					<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12" >
						<label >Arrived From</label>
						<select class="form-control"   ng-model="dp.arrived_from" ng-selected="dp.arrived_from">
							<option value="{{vendor.id}}" ng-repeat="vendor in vendors">{{vendor.name}}</option>
						</select>
						<!-- <select class="form-control" ng-model="dp.arrived_from" required>
							<option value="Shabaz">Shabaz</option>
							<option value="Ayub">Ayub</option>
							<option value="Customer">Customer</option>
						</select> -->
					</div>
				</div><!-- 1st row ends here -->

				<div class="row">
					<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
						<label for="">Product Category</label>
						<!-- <select class="form-control" ng-model="dp.product_cat" required>
							<option value="Refrigerator">Refrigerator</option>
							<option value="Washing Machine">Washing Machine</option>
							<option value="Bed">Bed</option>
						</select> -->
						<select class="form-control"   ng-model="dp.product_cat">
							<option value="{{cat.id}}" ng-repeat="cat in category">{{cat.name}}</option>
						</select>
					</div>

					<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
						<label for="">Product Name</label>
						<input type="text" class="form-control" id="" ng-model="dp.product_title" required>
					</div>

					<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
						<label for="">Procure price</label>
						<input type="text" class="form-control" id="" ng-model="dp.procure_cost">
					</div>
				</div><!-- 2ndrow ends here -->

				<div class="row">
					<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
						<label for="">Stage</label>
						<select class="form-control" ng-model="dp.stage" required>
							<option ng-value="stage.stage" ng-repeat="stage in stages">{{stage.stage}}</option>
						</select>
					</div>

					<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
						<label for="">Sale price</label>
						<input type="text" class="form-control" id="" ng-model="dp.saleprice">
					</div>

					<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
						<label for="">Product Condition</label>
						<textarea class="form-control" rows="3" ng-model="dp.product_condition"></textarea>
					</div>
				</div><!-- 3rd row ends here -->
				<div class="row">
					<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
						<label for="">Website ID</label>
						<input type="text" class="form-control" id="" ng-model="dp.product_id">
					</div>
					<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
						<label for="">Model Number</label>
						<input type="text" class="form-control" id="" ng-model="dp.modelno">
					</div>
				</div>

				<!--button type="button" class="btn btn-default">Upload Image</button>
				<button type="button" class="btn btn-default">Upload QC Report</button>
				<br><br><br-->

				<button  type="submit" class="btn btn-success" ng-confirm-click="Are you sure to change the state?">Modify Product</button>
				<p style="color:darkorange" ng-show="showmstat">Product Modified Successfully</p>
			</form>
	</div></div></div> <!-- panel divs 3 close here -->

	</div><!-- container ends here -->
	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/bower_components/angular/angular.min.js"></script>
	<script src="assets/bower_components/angular-cookies/angular-cookies.min.js"></script>
	<script src="assets/js/warehouse/wapp.js"></script>
</body>
</html>
