<style>
	.photomenu {
		margin-top:20px;
		padding: 10px;
	}
	.photomenu a {
		margin-right: 15px;
		font-weight: 400;
		font-size: 15px;
		text-decoration: underline;
	}
	.pending {
		border: 1px solid #d0d0d0;
    	background: #dedede;
	}
</style>
<html ng-app="pidealer">
 <title>Dealer Page for Refabd</title>
 <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimal-ui" />
    <meta name="apple-mobile-web-app-status-bar-style" content="yes" />
	<link rel="stylesheet" href="../assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="../assets/css/dealer/dealer_main_style.min.css" />
</head>
	<div ng-show="::false" class='loader'>
		<img src='../assets/images/loader.gif'>
	</div>
<body ng-controller="DealerController as ctrl" class="container">
	<div ng-cloak>
		<header>
			<div class="container">
				<!-- Login Div -->
				<div class="row" ng-show="dealerLogged">
					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
						<a class="navbar-brand" href="">
							REFABD - Dealer
						</a>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right dealername">
						<div class="dropdown">
							<a class="dropdown-toggle" type="button" data-toggle="dropdown"  href="#">Hello, {{dealername}}
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<!--a href="">Account Details</a-->
							<li><a href='/' ng-click="logout()">Logout</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" ng-show="!dealerLogged">
			<a class="navbar-brand" href="">
			POCKET <strong>IN</strong>- Dealer
			</a>
			</div>
		</div>
	</header>

	<div class="main-container">
		<div class="container">
			<div>
				<div class="row">
					<form name="loginform" ng-show="!dealerLogged" ng-submit="loginform.$valid && Login()" novalidate>
						<legend>Dealer Login</legend>
						<div class="form-group" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<label for="">Mobile Number/Email</label>
							<input type="text" class="form-control" id="" placeholder="" ng-model="username" required></div>
						<div class="form-group" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<label for="">Password</label>
							<input type="password" class="form-control" id="" placeholder="" ng-model="password" required></div>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<button type="submit" class="btn btn-primary btn-lg" ng-disabled="loginform.$submitted">Submit</button>
						</div>
						<hr/>
						<p style="color: darkorange">{{error}}</p>
					</form>
				</div>
			</div>

			<!-- Content Tab -->
			<ul class="nav nav-tabs clear dealer" ng-show="dealerLogged">
				<li class="active">
					<a data-toggle="tab" href="#addprod">Add New Product</a>
				</li>
				<li>
					<a data-toggle="tab" href="#photoshoot">PhotoShoot Status</a>
				</li>
				<li>
					<a data-toggle="tab" href="#showprods" ng-click="ctrl.loadItems();">Show Listed Products</a>
				</li>
			</ul>
			<div ng-show="dealerLogged" class="tab-content">
				<div class="tab-pane fade in" id="showprods"">
					<h3>Listed Products</h3>
					<hr/>
					<div class="row item" ng-repeat="item in items">
						<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
							<div class="imagediv">
								<img width="150px" ng-src="../assets/images/dealerpending/{{item.images[0].image_full}}" alt="" ng-if="item.isapproved == 0"/>
								<img width="150px" ng-src="../assets/images/products/thumbnail/{{item.images[0].image_full}}" ng-if="item.isapproved == 1"alt=""/>
							</div>
						</div>
						<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
							<h4 class="title">{{item.product_title}}</h4>
						</div>
						<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 status">
							<h5 ng-style="item.issold == 1 ? {color: 'red'} : {color: 'green'}">
								{{item.isapproved == 0 ? "Approval Pending" : (item.issold == 0 ? "not sold" : (item.ispaid == 0 ? "SOLD" : "Sold and Amount Paid"))}}
							</h5>
						</div>
						<div class="col-lg-2 col-md-6 col-sm-6 col-xs-12 action">
							<!--label style="color: orange">ID: {{item.productitem_id}}</label-->
							<button ng-confirm-click type="button" class="btn btn-info" ng-click="markSold(item)" ng-disabled="item.issold == 1">Mark as Sold</button>
						</div>
					</div>
				</div>

				<div class="tab-pane fade in" id="photoshoot" ng-init="getUploadStatus(0)">
					<h3>Photoshoot Status</h3>
					<div class="photomenu" ng-init="pendingupload=1;photostatus=0">
						<button type="button" class="btn btn-info" ng-click="pendingupload=1;photostatus=0;getUploadStatus(0)">Pending Upload</button>
						<button type="button" class="btn btn-info" ng-click="photostatus=1;pendingupload=0;sshow =0">Add Photo Status</button>
					</div>
					<div ng-show="pendingupload" class="well">
						<h4>Pending Uploads</h4>
						<table class="table table-hover" ng-show="upending">
							<thead>
								<tr>
									<th>Warehouse ID</th>
									<th>Shoot Date</th>
									<th>Image</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="up in upending">
									<td>{{up.wid}}</td>
									<td>{{up.shootdate}}</td>
									<td><a ng-href="https://drive.google.com/drive/u/0/search?q={{up.wid}}" style="color:#945202;text-decoration: underline" target="_blank">Download Images</a></td>
								</tr>
							</tbody>
						</table>
					</div>
					<div ng-show="photostatus">
						<form name="pst" ng-submit="pst.$valid && uploadPhotoStatus(shootdate, shootlist)" novalidate>
						<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
							<label for="">Shoot Date</label>
							<input type="date" class="form-control" required ng-model="shootdate">
						</div>
						<div class="form-group col-lg-8 col-md-8 col-sm-6 col-xs-12" >
							<label>ID list</label>
							<textarea rows="5" class="form-control" ng-model="shootlist"></textarea>
						</div>
						<button ng-inint="photodone=0" type="submit" class="btn btn-success" style="width: 200px;display: block;margin: 0 auto;">Submit</button>
<p ng-show="photodone">Submitted</p>
						</form>
						<a href="" ng-click="getUploadStatus(1); sshow = (sshow == 0 ? 1:0) " style="text-decoration: underline; color: #0508f3">View Daily Status</a>
						<div>
							<table class="table table-hover" ng-show="upending && sshow">
							<thead>
								<tr>
									<th>Warehouse ID</th>
									<th>Shoot Date</th>
									<th>Image</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="up in upending">
									<td>{{up.wid}}</td>
									<td>{{up.shootdate}}</td>
									<td><a ng-href="https://drive.google.com/drive/u/0/search?q={{up.wid}}" style="color:#945202;text-decoration: underline" target="_blank">Download Images</a></td>
									<td><span style="color:green" ng-show="up.uploaded == 1">Uploaded</span><span style="color:darkorange" ng-show="up.uploaded == 0">Yet to upload</span></td>
								</tr>
							</tbody>
						</table>
						</div>
					</div>
					<hr/>
				</div>
				<div class="tab-pane fade in active" id="addprod">
					<h3>Add a Product</h3>
					<hr/>
					<div style="display: inline-block; border: 1px solid #d4d4d4; padding: 20px;padding-top: 30px;margin-bottom: 5px;">
					<form role="form" class="form-inline" novalidate ng-submit="fetchWarehouseInfo()">
						<div>
						<div class="form-group">
							<label for="">Enter Warehouse ID:</label>
							<input type="text" class="form-control" ng-model="wid" required>
						</div>
						<button type="submit" class="btn btn-success" id="wfetch">Fetch</button></div>
					</form>
					<label for="" style="color:#ef5608;font-weight: 400;" ng-show="winfofailed==1">Error: Failed to fetch warehouse Info. Either the ID is missing or duplicate entries exist. Contact Support</label>
					<label for="" style="color:#ecb200;font-weight: 400;" ng-show="alreadyuploaded==1">Warning: The product is already uploaded with id {{alreadyuploadedid}}</label>
					</div>
					<form role="form" name="newprod" novalidate ng-submit="completedupload == true && newprod.$valid && imageadded == 1 && addNewProduct()" class="container addform" ng-show="warehouseinfofetched">
						<div class="row">
							<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12">
								<label for="">Main Category</label>
								<select class="form-control"  required ng-model="maincat" ng-change="getSubCat()">
									<option value="{{mc.id}}" ng-repeat="mc in maincats">{{mc.name}}</option>
								</select>
							</div>
							<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12">
								<label for="">Sub Category</label>
								<select class="form-control" required ng-model="subcat" ng-change="getQualityValues()">
									<option value="{{sc.id}}" ng-repeat="sc in subcats">{{sc.name}}</option>
								</select>
							</div>
						</div>
						<hr/>
						<div class="row">
							<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<label for="">Product Name</label>
								<p style="font-size: 13px;font-weight: 300;">Format: Brand Name, Type, Category <span style="color: #ff7d7d">Eg: Zuari King Size Bed</span></p>
								<input type="text" class="form-control" id="" ng-model="productname" required></div>
							<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12" >
								<label >Product description</label>
								<p style="font-size: 13px;font-weight: 300;">Format: Usage, Color/Finish, Material. <span style="color: #ff7d7d">Eg: Unboxed Teak Finish from Urban Ladder</span></p>
								<input type="text" class="form-control" ng-model="productdesc" required>
							</div>
						</div>
						<div ng-hide="subcat == -1" class="row">
						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 quality" ng-repeat="q in quality">
							<label for="{{q.qname}}">{{q.qname}}:</label>
							<select name = "{{q.qname}}" class="form-control" required="required" ng-model="qv[q.qid]" required>
								<option value="{{val.value}}" ng-repeat="val in q.values">{{val.value_text}}</option>
							</select>
						</div>
						</div>
					<br>

					<!-- Flow starts here ="{target: 'admin/uploadprodimage', testChunks:false}"-->
					<div flow-init
						flow-file-added="ctrl.initupload($file);"
						flow-files-submitted="ctrl.uploadimage($flow)"
						flow-error="ctrl.uploadfailed($file)"
						flow-complete="ctrl.uploadcomplete()"
						>
						<label >Product Images</label>
						<p style="font-size: 13px;font-weight: 300;">Format: Download the images from google drive. Rename the images as 0,1, 2 ... 9, 91, 92 ...</p>
						<div class="drop" flow-drop ng-class="dropClass" flow-drag-enter="style={border:'1px solid grey'}" flow-drag-leave="style={}" ng-style="style">
							<span class="btn btn-default" flow-btn>Upload Image</span>
							<span class="btn btn-default" flow-btn flow-directory ng-show="$flow.supportDirectory">Upload Folder of Images</span> <b>OR</b>
							Drag And Drop images/folder here
						</div>
						<br/>

						<div>
							<div ng-repeat="file in $flow.files" class="gallery-box" ng-class="(file.error == true || file.removefailed == 1) ? 'error': '' " ng-init="file.success = false">
								<span class="title">{{file.name}}</span>
								<div class="thumbnail" ng-show="$flow.files.length">
									<img flow-img="file" />
								</div>
								<div class="progress progress-striped" ng-class="{active: file.isUploading()}">
									<div class="progress">
										<div class="progress-bar"  ng-class="file.error == true ? 'progresserror': '' " role="progressbar"
aria-valuenow="{{file.progress() * 100}}"
aria-valuemin="0"
aria-valuemax="100"
ng-style="{width: (file.progress() * 100) + '%'}">
											<span class="sr-only">{{file.progress()}}% Complete</span>
											<p ng-show="file.error == true" style="color:white">Failed to upload file</p>
										</div>
									</div>
								</div>
								<div class="btn-group">
									<a class="btn btn-xs btn-danger" ng-click="ctrl.removeimage(file)">Remove</a>
									<label ng-show="file.removefailed == 1" style="color: red">Failed to remove</label>
								</div>
							</div>
							<div class="clear"></div>
						</div>
						<!--button type="button" class="btn btn-primary" ng-click="ctrl.saveproduct($flow)">Save Product</button-->
				</div>
				<div class="row">
					<div class="form-group col-lg-8 col-xs-12">
					  <label>MRP:</label>
					  <input type="text" class="form-control" id="" placeholder="" ng-model="mrp" required/>
					</div>
					<div class="form-group col-lg-8 col-xs-12">
					  <label>Model Number</label>
					  <input type="text" class="form-control" id="" placeholder="" ng-model="winfo.modelno" readonly/>
					</div>
					<div class="form-group col-lg-8 col-xs-12">
					  <label>Long Description:</label>
					  <p style="font-size: 13px;font-weight: 300;">Format: Add a catchy description about the product. Should be around 3 to 4 lines. Note: You can search with the Model Number and get the details online and frame description based on that.</p>
					  <textarea rows="5" class="form-control" id="" placeholder="" ng-model="longdescription" required></textarea>
					</div>
					<div class="form-group col-lg-8 col-xs-12">
					  <label>Technical Spec:</label>
					  <p style="font-size: 13px;font-weight: 300;">Format: Add capacity, Material, Wood, Dimension, Type etc.,</p>
					  <textarea rows="5" class="form-control" id="" placeholder="" ng-model="technicalspec" required></textarea>
					</div>
					<div class="form-group col-lg-8 col-xs-12">
					  <label>Condition Spec:</label>
					  <p style="font-size: 13px;font-weight: 300;">Format: Inspect each image and mention the defects. Refer QC report as well</p>
					  <textarea rows="5" class="form-control" id="" placeholder="" ng-model="conditionspec" required></textarea>
					</div>
					<!--div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-4">
						<label for="">Sale Price</label>
						<input type="text" class="form-control" id="" ng-model="saleprice" required></div-->
					<!--div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-4">
						<label for="">Manufacturing Year</label>
						<input type="text" class="form-control" id="" ng-model="year" required>
					</div-->
					<!--div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-4">
						<label for="">Compressor Warranty</label>
						<input type="text" class="form-control" id="" ng-model="warranty" >
					</div-->
				</div>
				<div class="row">
						<div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-4">
							<div class="radio">
								<label>
									<input type="checkbox" name="" id="input" value="" ng-model="isfactory">
									Unboxed
								</label>
							</div>
						</div>
				</div>
				<div class="row">
					<div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-4">
						<div class="radio">
							<label>
								<input type="checkbox" name="" id="input" value="" ng-model="brandnew">
								Brand New
							</label>
						</div>
					</div>
				</div>
				<div class="row">
				<div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-4">
					<label for="">Stock Count:</label>
					<input type="number" name="" id="input" class="form-control" value="1" required="required" ng-model="stockcount">
					</div>
				</div>

				<p style="color:red; font-size: 14px;" ng-show="newprod.$submitted && imageadded == 0">Add atleast one image</p>
				<p style="color:red; font-size: 14px;" ng-show="newprod.$submitted && completedupload == false">Image upload not complete</p>
				<button type="submit" class="btn btn-primary" ng-hide="adding == 1 || success == 1">Add Product</button>
				<p style="color:grey; font-size: 14px;" ng-show="adding == 1">Adding...</p>
				<p style="color:darkorange; font-size: 14px;" ng-show="adding == 0 && success == 1">Product Added Successfully</p>

			</form>

		</div>
	</div>
</div>
</div>
</div>
<script  src="../assets/js/jquery.min.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>

<script src="../assets/bower_components/angular/angular.min.js"></script>
<script src="../assets/bower_components/angular-cookies/angular-cookies.min.js"></script>
<script src="../assets/js/dealer/dealerapp.js"></script>
<script src="../assets/js/dealer/ng-flow-standalone.js"></script>

</body>
</html>
