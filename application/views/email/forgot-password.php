<table border="0" align="center" cellpadding="20" cellspacing="0" width="80%" style="margin: 0 auto; padding: 0; max-width:640px;">
<tbody>
<tr>
<td style=" margin: 0; padding: 0;  border: 1px solid #eee; border-top: 5px solid #47C9AF;">
<table border="0" cellpadding="20" cellspacing="0" width="100%" style="padding: 30px 15px; color:#000000; line-height:150%; text-align:center; font:300 16px 'Helvetica Neue', Helvetica, Arial, sans-serif;">
	<tbody>
		<tr>
			<td valign="top" align="left" width="100" style="padding:0; vertical-align: bottom;">
				<img alt="Refabd" style="max-width:120px; " src="https://www.refabd.com/assets/images/logo.png">
			</td>
	</tbody>
</table>
<table border="0" cellpadding="20" cellspacing="0" width="100%" style="padding: 0 15px; background:#ffffff; color:#000000; line-height:150%; text-align:left; font:300 16px 'Helvetica Neue', Helvetica, Arial, sans-serif;">
	<tbody>
		<tr>
			<td valign="top" width="100" style="padding:30px 0;">
				<p style="display:block;margin:15px 0; ">Hi Sarangh Somaraj,</p>
				<p style="display:block;margin:0; font-size:14px; line-height:150%;">We received a request to reset the password associated with this e-mail address. If you made this request, please follow the instructions below.</p>
			</td>
		</tr>
	</tbody>
</table>
<table cellspacing="0" cellpadding="6" width="100%" style="padding: 0 15px;  color:#000000; line-height:150%; font:300 16px 'Helvetica Neue', Helvetica, Arial, sans-serif;" border="0">
	<tbody>
		<tr>
			<td valign="top" width="100" style="padding:30px 0; text-align:left; line-height:150%; font-size: 14px;">
				<p>Click on the link below to reset your password using our secure server:</p>
			</td>
		</tr>
		<tr>
			<td valign="top" width="100" style="padding:30px 0; text-align:center;">
				<a style="padding: 10px 15px; background: #fff; border: 1px solid #5DC6B1; color: #5DC6B1; font-size:16px; text-decoration:none;" href="">Reset Password</a>
			</td>
		</tr>
		<tr>
			<td valign="top" width="100" style="padding:30px 0; text-align:left; line-height:150%; font-size: 14px;">
				<p>If clicking the link does not seem to work, you can copy and paste the link into your browser's address window, or retype it there. Once you have returned to Amazon, we will give instructions for resetting your password.</p>
			</td>
		</tr>
 	</tbody>
</table>
<table cellspacing="0" cellpadding="6" width="100%" style=" margin-bottom:15px; color:#000000; line-height:150%; font:300 16px 'Helvetica Neue', Helvetica, Arial, sans-serif; text-align:center;" border="0">
	<tbody>
		<tr>
			<td style="font-size:12px; font-weight:300;">
					<span style="display: block; margin-bottom:15px;">
	  					<img style="text-align:left; max-height: 40px;" src="services-1.png">
					</span>
					<b style="color: #5DC6B1;">Hassle Free delivery</b>
					<p style="display: block; margin: 5px; 0">Vestibulum rutrum quam vitae fringilla tincidunt. Suspendis.</p>
				</div>
			</td>
			<td style="font-size:12px;font-weight:300;">
					<span style="display: block; margin-bottom:15px;">
	  					<img style="text-align:left; max-height:40px;" src="services-2.png">
					</span>
					<b style="color: #5DC6B1;">6 Month Warranty</b>
					<p style="display: block; margin: 5px; 0">Vestibulum rutrum quam vitae fringilla tincidunt. Suspendis.</p>
				</div>
			</td>
			<td style="font-size:12px;font-weight:300;">
				<span style="display: block; margin-bottom:15px;">
  					<img style="text-align:left; max-height:40px;" src="services-3.png">
				</span>
				<b style="color: #5DC6B1;">Quality check</b>
				<p style="display: block; margin: 5px; 0">Vestibulum rutrum quam vitae fringilla tincidunt. Suspendis.</p>
			</td>
			<td style="font-size:12px;font-weight:300;">
				<span style="display: block; margin-bottom:15px;">
  					<img style="text-align:left; max-height:40px;" src="services-4.png">
				</span>
				<b style="color: #5DC6B1;">Post sale care/service</b>
				<p style="display: block; margin: 5px; 0">Vestibulum rutrum quam vitae fringilla tincidunt. Suspendis.</p>
			</td>
		</tr>
	</tbody>
</table>
<table  class="social" width="100%" cellspacing="0" cellpadding="0" style="background: #5DC6B1; color:#ffffff; padding: 15px; font:300 12px 'Helvetica Neue', Helvetica, Arial, sans-serif;">
	<tr>
		<td>

			<!--- column 1 -->
			<table align="left" class="column">
				<tr>
					<td>				
						<table border="0" cellspacing="0" cellpadding="0" style="color:#ffffff; font:300 12px 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size:12px;">
                            <tbody>
                                <tr>
                                    <td style="font-weight: 200;">Get the App: </td>
                                    
                                    <td>
                                        <a style="display: inline-block; margin:0 10px;" href="#" target="_blank">
                        					<img style="max-height:15px;width:auto;" src="android-link.png">
                        				</a>
                                   
                                        <a style="display: inline-block; margin:0 10px 0 5px;" href="#" target="_blank">
                        					<img style="max-height:15px;width:auto;" src="ios-link.png"></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

						
					</td>
				</tr>
			</table><!-- /column 1 -->	
			
	
			<!-- /column 3 -->
			<table align="right" class="column">
				<tr>
					<td align="center" style="color:#ffffff; font-size:12px; font-weight: 200;">
                        &copy; 2015&#45;Refabd.
                    </td>
				</tr>
			</table><!-- /column 2 -->
		</td>
	</tr>
</table>
</td>
</tr>
</tbody>
</table>

<table cellspacing="0" cellpadding="6" width="100%" style="margin: 15px 0; color:#000000; line-height:150%; font:300 16px 'Helvetica Neue', Helvetica, Arial, sans-serif; text-align:center;" border="0">
	<tbody>
		<tr>
			<td style="font-size:12px;font-weight:300;">
				<a href="#" style="margin: 0 15px; display:inline-block;">Terms and Conditions</a>
				<a href="#" style="margin: 0 15px; display:inline-block;">Privacy Policy</a>
			</td>
		</tr>

</table>


