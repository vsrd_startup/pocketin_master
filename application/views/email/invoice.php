<div>
	<div>
		<p style="font-size: 26px;font-weight: 300;letter-spacing: 1px;line-height: 20px!important;color: #47C9AF;"">Refabd</p>
	</div>
	<div style="padding: 20px; ">
		<p>Hello <?php echo $user->firstname . ' ' . $user->lastname ?>, </p>
		<h3 style="color: darkorange">Order Invoice of Order, OD:<?php echo $order->orderdetail_id ?></h3>
			<h5>Delivery Date: <?php echo $order->deliverydate; ?></h5>
		<hr>
	</div>
	<div>
		<table cellspacing="0" cellpadding="6" width="40%" style="padding: 0 15px; color:#000000; line-height:150%; text-align:left; font:300 16px 'Helvetica Neue', Helvetica, Arial, sans-serif;">
			<thead>
				<tr>
					<th>
						<h3 style="color:#000000;display:block;font-family:Arial;font-size:14px;font-weight:300;margin-top:5px;margin-right:0;margin-bottom:5px;margin-left:0;text-align:left;line-height:100%">Delivery Address:</h3>
					</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td valign="top" width="50%" style="padding:15px; padding-top:0; text-transform:capitalize; font-size:14px; line-height:150%; "> <b style="font-weight:400;"><?php echo $deliveryaddress->name ?></b>
						<p style="margin:5px 0;">
							<?php echo $deliveryaddress->
							address_line_one; echo ", ";echo $deliveryaddress->address_line_two ?>
							<br/>
							<?php echo $deliveryaddress->
							city; echo ", "; echo $deliveryaddress->state; ?>
							<br/>
							<?php echo $deliveryaddress->
							pincode; ?>
							<p style="margin:0;">
								Landmark:
								<span style="font-weight:500;">
									<?php echo $deliveryaddress->landmark;?></span>
							</p>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<hr>
		<div style="margin-top: 20px;">
		<h3 style="padding-left: 20px">Order Items</h3>
			<table width="90%" align="center" cellspacing="0" cellpadding="6" style="padding: 0 15px;  border:0; color:#000000; line-height:150%; text-align:left; font:300 14px/30px 'Helvetica Neue', Helvetica, Arial, sans-serif;">
				<thead>
					<tr style="background:#fff;">
						<th scope="col" width="60%" style="border-bottom: 2px solid #eee; text-align:left;font-weight:400;">Product</th>
						<th scope="col" width="20%" style="border-bottom: 2px solid #eee; text-align:right;font-weight:400;">Price</th>
					</tr>
				</thead>
				<tbody>
					<tr width="100%" >
						<td  width="60%" style="text-align:left; vertical-align:middle; border-bottom: 2px solid #eee;  word-wrap:break-word;">
							<div style="display:inline-block; vertical-align:top; max-width: 316px;">
								<h1 style="margin:0; font-family:Arial;font-size:16px;font-weight:300;text-align:left;">
									<?php echo $item['product_title']; ?></h1>
								<span style="line-height:24px;">
									<?php echo $item['product_description']; ?></span>
							</div>
						</td>
						<td width="20%" style="text-align:right;vertical-align:middle; border-bottom: 2px solid #eee;">
							<span>
								Rs.&nbsp;
								<?php echo $order->price; ?></span>
						</td>
					</tr>
				</tbody>
				<tfoot>
					<tr>
						<td scope="row" colspan="1" style="text-align:right;vertical-align:middle;">Packing and Delivery Charges :</td>
						<td style="text-align:right; vertical-align:middle;">
							<span>Rs.&nbsp;0</span>
						</td>
					</tr>
					<tr>
						<th scope="row" colspan="1" style="text-align:right; background:#fff; text-align:right; font-weight:400;">Order Total :</th>
						<th style="text-align:right; vertical-align:middle; font-weight:400;">
							<span>
								Rs.&nbsp;
								<?php echo $order->price; ?></span>
						</th>
					</tr>
				</tfoot>
			</table>
		</div>
		<hr>

	</div>
	<div style="margin-top:10px;">
		<p style="font-size: 14px; color: grey; text-align: right;">This is a computer generated invoice and does not require any signature</p>
		<br><br><br>
		<p style="clear:both; font-size: 18px;">For any Service related issues, please call us at <b>70 226 30 270 (10AM to 4PM on all days)</b> or</p>
		<p style="font-size: 18px;">Email us at care@refabd.com</p>
	</div>