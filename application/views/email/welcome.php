<table border="0" align="center" cellpadding="20" cellspacing="0" width="80%" style="margin: 0 auto; padding: 0; max-width:640px;">
<tbody>
<tr>
<td style=" margin: 0; padding: 0;  border: 1px solid #eee; border-top: 5px solid #47C9AF;">
</tr>
<table border="0" cellpadding="20" cellspacing="0" width="100%" style="padding: 30px 15px; color:#000000; line-height:150%; text-align:center; font:300 16px 'Helvetica Neue', Helvetica, Arial, sans-serif;">
	<tbody>
		<tr>
			<td valign="top" align="left" width="100" style="padding:0; vertical-align: bottom;">
				<a href="http:://refabd.com/"><img alt="Refabd" style="max-width:120px; " src="https://www.refabd.com/assets/images/logo.png"></a>
			</td>
	</tbody>
</table>
<table border="0" cellpadding="20" cellspacing="0" width="100%" style="padding: 0 15px; background:#ffffff; color:#000000; line-height:150%; text-align:left; font:300 16px 'Helvetica Neue', Helvetica, Arial, sans-serif;">
	<tbody>
		<tr>
			<td valign="top" width="100" style="padding:30px 0;">
				<p style="display:block;margin:15px 0; ">Hi <?php echo $user ?>,</p>
				<p style="display:block;margin:0; font-size:14px; line-height:150%;">Welcome to Refabd!</p>
				<p>SOW, add few lines of text here. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
			</td>
		</tr>
	</tbody>
</table>
<table cellspacing="0" cellpadding="6" width="100%" style="padding: 0 15px;  color:#000000; line-height:150%; font:300 16px 'Helvetica Neue', Helvetica, Arial, sans-serif;" border="0">
	<tbody>
		<tr>
			<td valign="top" width="100" style="padding:30px 0; text-align:center;">
				<a style="padding: 10px 15px; background: #fff; border: 1px solid #5DC6B1; color: #5DC6B1; font-size:16px; text-decoration:none;" href="http://www.refabd.com/" target="_blank">Start Shopping</a>
			</td>
		</tr>
		<tr>
			<td valign="top" width="100" style="padding:30px 0; text-align:left; line-height:150%; font-size: 14px;">
				<p>Please note that this will be your registered email id linked to your account. You will receive information regarding order confirmation, order dispatch, order cancellation, shipping details, order invoice, cashback information & refunds on this email id.</p>
			</td>
		</tr>
	</tbody>
</table>
<table cellspacing="0" cellpadding="6" width="100%" style=" margin-bottom:15px; color:#000000; line-height:150%; font:300 16px 'Helvetica Neue', Helvetica, Arial, sans-serif; text-align:center;" border="0">
	<tbody>
		<tr>
			<td style="font-size:12px; font-weight:300;">
					<span style="display: block; margin-bottom:15px;">
	  					<img style="text-align:left; max-height: 40px;" src="http://refabd.com/assets/images/email/services-1.png">
					</span>
					<b style="color: #5DC6B1;">Free delivery</b>
					<p style="display: block; margin: 5px; 0">100% free delivery for any item purchased.</p>
				</div>
			</td>
			<td style="font-size:12px;font-weight:300;">
					<span style="display: block; margin-bottom:15px;">
	  					<img style="text-align:left; max-height:40px;" src="http://refabd.com/assets/images/email/services-2.png">
					</span>
					<b style="color: #5DC6B1;">6 Month Warranty</b>
					<p style="display: block; margin: 5px; 0">All items purchased comes with Free 6 month warranty.</p>
				</div>
			</td>
			<td style="font-size:12px;font-weight:300;">
				<span style="display: block; margin-bottom:15px;">
  					<img style="text-align:left; max-height:40px;" src="http://refabd.com/assets/images/email/services-3.png">
				</span>
				<b style="color: #5DC6B1;">Quality check</b>
				<p style="display: block; margin: 5px; 0">All items undergo thorough inspection and gets genuine quality rating</p>
			</td>
			<td style="font-size:12px;font-weight:300;">
				<span style="display: block; margin-bottom:15px;">
  					<img style="text-align:left; max-height:40px;" src="http://refabd.com/assets/images/email/services-4.png">
				</span>
				<b style="color: #5DC6B1;">Post sale care/service</b>
				<p style="display: block; margin: 5px; 0">We offer 24/7 after-sales services for faster resolution of queries</p>
			</td>
		</tr>
	</tbody>
</table>
<table  class="social" width="100%" cellspacing="0" cellpadding="0" style="background: #5DC6B1; color:#ffffff; padding: 15px; font:300 12px 'Helvetica Neue', Helvetica, Arial, sans-serif;">
	<tr>
		<td>

			<!--- column 1 -->
			<table align="left" class="column">
				<tr>
					<td>
						<table border="0" cellspacing="0" cellpadding="0" style="color:#ffffff; font:300 12px 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size:12px;">
                            <tbody>
                                <tr>
                                    <td style="font-weight: 200;">Get the App: </td>
                                    
                                    <td>
                                        <a style="display: inline-block; margin:0 10px;" href="#" target="_blank">
                        					<img style="max-height:15px;width:auto;" src="http://pocketin.in/assets/images/email/android-link.png">
                        				</a>
                                   
                                        <a style="display: inline-block; margin:0 10px 0 5px;" href="#" target="_blank">
                        					<img style="max-height:15px;width:auto;" src="http://pocketin.in/assets/images/email/ios-link.png"></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

						
					</td>
				</tr>
			</table><!-- /column 1 -->	
			
	
			<!-- /column 3 -->
			<table align="right" class="column">
				<tr>
					<td align="center" style="color:#ffffff; font-size:12px; font-weight: 200;">
                        &copy; 2017&#45;Refabd.
                    </td>
				</tr>
			</table><!-- /column 2 -->
		</td>
	</tr>
</table>
</td>
</tr>
</tbody>
</table>

<table cellspacing="0" cellpadding="6" width="100%" style="margin: 15px 0; color:#000000; line-height:150%; font:300 16px 'Helvetica Neue', Helvetica, Arial, sans-serif; text-align:center;" border="0">
	<tbody>
		<tr>
			<td style="font-size:12px;font-weight:300;">
				<a href="http://wwww.pocketin.in/terms" style="margin: 0 15px; display:inline-block;">Terms and Conditions</a>
				<a href="http://wwww.pocketin.in/privacy" style="margin: 0 15px; display:inline-block;">Privacy Policy</a>
			</td>
		</tr>
	</tbody>

</table>


