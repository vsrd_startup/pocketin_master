<table border="0" align="center" cellpadding="20" cellspacing="0" width="80%" style="margin: 0 auto; padding: 0; max-width:640px;">
<tbody>
<tr>
<td style=" margin: 0; padding: 0;  border: 1px solid #eee; border-top: 5px solid #47C9AF;">
<table border="0" cellpadding="20" cellspacing="0" width="100%" style="padding: 30px 15px; color:#000000; line-height:150%; text-align:center; font:300 16px 'Helvetica Neue', Helvetica, Arial, sans-serif;">
	<tbody>
		<tr>
			<td valign="top" align="left" width="100" style="padding:0; vertical-align: bottom;">
				<img alt="Refabd" style="max-width:120px; " src="https://www.refabd.com/assets/images/logo.png">
			</td>
			<td valign="top" align="right" style="padding:0; vertical-align: bottom;">
				Order Id: <b><?php echo $order->orderdetail_id ?></b>
			</td>
	</tbody>
</table>
<table border="0" cellpadding="20" cellspacing="0" width="100%" style="padding: 0 15px; background:#ffffff; color:#000000; line-height:150%; text-align:left; font:300 16px 'Helvetica Neue', Helvetica, Arial, sans-serif;">
	<tbody>
		<tr>
			<td valign="top" width="100" style="padding:30px 0;">
				<p style="display:block;margin:15px 0; ">Hi <?php echo "$user->firstname $user->lastname"?>,</p>
				<?php if ($status == "Cancelled") { ?>
					<p style="display:block;margin:0; font-size:14px;  line-height:150%;">We are sorry to hear that you raised a request to <b>cancel</b> the following item from your order OD: <b><?php echo $order->orderdetail_id; ?></b>. We will process the request and get back to you with the status on your registered mobile number. </p>
				 <?php } else { ?>
				<p style="display:block;margin:0; font-size:14px;  line-height:150%;">We are pleased to inform that the following item in your order OD: <b><?php echo $order->orderdetail_id; ?></b> has been <b><?php echo $status; ?></b>.
				<?php if ($status == "Placed") echo "We will verify the purchase and you will get email confirmation once it is verified.";?>
				<?php if ($status == "Confirmed") echo " Sit back and relax while we test, pack and deliver your product. You will get a call from our delivery department to book slot in 1 day.";?>
				<?php if ($status == "Shipped") echo " You can expect delivery in the chosen slot, Today."; ?>
				<?php if ($status == "Delivered") { echo " This completes your order. Thank you for shopping at Refabd!. Invoice of the order is attached with the email."; }?></p>
				<?php } ?>
			</td>
		</tr>
	</tbody>
</table>
<table align="center" cellspacing="0" cellpadding="6" style="padding: 0 15px;  border:0; color:#000000; line-height:150%; text-align:left; font:300 14px/30px 'Helvetica Neue', Helvetica, Arial, sans-serif;">
	<thead>
		<tr style="background:#fff;">
			<th scope="col" width="60%" style="border-bottom: 2px solid #eee; text-align:left;font-weight:400;">Product</th>
			<th scope="col" width="20%" style="border-bottom: 2px solid #eee; text-align:right;font-weight:400;">Price</th>
		</tr>
	</thead>
	<tbody>
		<tr width="100%" >
			<td  width="60%" style="text-align:left; vertical-align:middle; border-bottom: 2px solid #eee;  word-wrap:break-word;">
  				<div style="display:inline-block;">
  					<img style="text-align:left; max-width:100px;" src=<?php echo "http://www.refabd.com/assets/images/products/thumbnail/". $item['images'][0]['image_full']; ?> >
  				</div>
  				<div style="display:inline-block; vertical-align:top; max-width: 316px;">
  					<h1 style="margin:0; font-family:Arial;font-size:16px;font-weight:300;text-align:left;"><?php echo $item['product_title']; ?></h1>
  					<span style="line-height:24px;"><?php echo $item['product_description']; ?></span>
  				</div>
  			</td>
			<td width="20%" style="text-align:right;vertical-align:middle; border-bottom: 2px solid #eee;">
				<span>Rs.&nbsp;<?php echo $order->price; ?> </span>
			</td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<td scope="row" colspan="1" style="text-align:right;vertical-align:middle;">Packing and Delivery Charges :</td>
			<td style="text-align:right; vertical-align:middle;"><span>Rs.&nbsp;0</span></td>
		</tr>
		<tr>
			<th scope="row" colspan="1" style="text-align:right; background:#fff; text-align:right; font-weight:400;">Order Total :</th>
			<th style="text-align:right; vertical-align:middle; font-weight:400;"><span>Rs.&nbsp;<?php echo $order->price; ?></span></th>
		</tr>
	</tfoot>
</table>
<table cellspacing="0" cellpadding="6" width="100%" style="padding: 0 15px; color:#000000; line-height:150%; text-align:left; font:300 16px 'Helvetica Neue', Helvetica, Arial, sans-serif; " border="0">
	<thead>
		<tr>
			<th>
				<h3 style="color:#000000;display:block;font-family:Arial;font-size:14px;font-weight:300;margin-top:5px;margin-right:0;margin-bottom:5px;margin-left:0;text-align:left;line-height:100%">Delivery Address:</h3>
			</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td valign="top" width="50%" style="padding:15px; text-transform:capitalize; border: 1px solid #e3e3e3; font-size:14px; line-height:150%; ">
				<b style="font-weight:400;"><?php echo $deliveryaddress->name ?></b>
				<p style="margin:5px 0;"><?php echo $deliveryaddress->address_line_one; echo ", ";echo $deliveryaddress->address_line_two ?> <br/><?php echo $deliveryaddress->city; echo ", "; echo $deliveryaddress->state; ?>
				<br/> <?php echo $deliveryaddress->pincode; ?>
				<p style="margin:0;">Landmark: <span style="font-weight:500;"><?php echo $deliveryaddress->landmark;?></span> </p>
			</td>
		</tr>
	</tbody>
</table>
<table cellspacing="0" cellpadding="6" width="100%" style="padding: 0 15px;  margin:60px 0; color:#000000; line-height:150%; text-align:left; font:300 16px 'Helvetica Neue', Helvetica, Arial, sans-serif; " border="0">
	<thead>
		<tr>
			<td valign="top" align="center" width="100" style="padding:0;">
				<a  target="_blank" style="padding: 10px 15px; background: #fff; border: 1px solid #5DC6B1; color: #5DC6B1; font-size:16px; text-decoration:none;" href=<?php echo "http://www.refabd.com/#/user/order/details/" . $order->orderdetail_id ?> >View Order Details</a>
			</td>
		</tr>
	</tbody>
</table>
<table cellspacing="0" cellpadding="6" width="100%" style=" margin-bottom:15px; color:#000000; line-height:150%; font:300 16px 'Helvetica Neue', Helvetica, Arial, sans-serif; text-align:center;" border="0">
	<tbody>
		<tr>
			<td style="font-size:12px; font-weight:300;">
					<span style="display: block; margin-bottom:15px;">
	  					<img style="text-align:left; max-height: 40px;" src="http://refabd.com/assets/images/email/services-1.png">
					</span>
					<b style="color: #5DC6B1;">Free delivery</b>
					<p style="display: block; margin: 5px; 0">100% free delivery for any item purchased.</p>
				</div>
			</td>
			<td style="font-size:12px;font-weight:300;">
					<span style="display: block; margin-bottom:15px;">
	  					<img style="text-align:left; max-height:40px;" src="http://refabd.com/assets/images/email/services-2.png">
					</span>
					<b style="color: #5DC6B1;">6 Month Warranty</b>
					<p style="display: block; margin: 5px; 0">All items purchased comes with Free 6 month warranty.</p>
				</div>
			</td>
			<td style="font-size:12px;font-weight:300;">
				<span style="display: block; margin-bottom:15px;">
  					<img style="text-align:left; max-height:40px;" src="http://refabd.com/assets/images/email/services-3.png">
				</span>
				<b style="color: #5DC6B1;">Quality check</b>
				<p style="display: block; margin: 5px; 0">All items undergo thorough inspection and gets genuine quality rating</p>
			</td>
			<td style="font-size:12px;font-weight:300;">
				<span style="display: block; margin-bottom:15px;">
  					<img style="text-align:left; max-height:40px;" src="http://refabd.com/assets/images/email/services-4.png">
				</span>
				<b style="color: #5DC6B1;">Post sale care/service</b>
				<p style="display: block; margin: 5px; 0">We offer 24/7 after-sales services for faster resolution of queries</p>
			</td>
		</tr>
	</tbody>
</table>
<table  class="social" width="100%" cellspacing="0" cellpadding="0" style="background: #5DC6B1; color:#ffffff; padding: 15px; font:300 12px 'Helvetica Neue', Helvetica, Arial, sans-serif;">
	<tr>
		<td>

			<!--- column 1 -->
			<table align="left" class="column">
				<tr>
					<td>
						<table border="0" cellspacing="0" cellpadding="0" style="color:#ffffff; font:300 12px 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size:12px;">
                            <tbody>
                                <tr>
                                    <td style="font-weight: 200;">Get the App: </td>
                                    
                                    <td>
                                        <a style="display: inline-block; margin:0 10px;" href="#" target="_blank">
                        					<img style="max-height:15px;width:auto;" src="http://refabd.com/assets/images/email/android-link.png">
                        				</a>
                                   
                                        <a style="display: inline-block; margin:0 10px 0 5px;" href="#" target="_blank">
                        					<img style="max-height:15px;width:auto;" src="http://refabd.com/assets/images/email/ios-link.png"></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

						
					</td>
				</tr>
			</table><!-- /column 1 -->	
			
	
			<!-- /column 3 -->
			<table align="right" class="column">
				<tr>
					<td align="center" style="color:#ffffff; font-size:12px; font-weight: 200;">
                        &copy; 2015&#45;Refabd.
                    </td>
				</tr>
			</table><!-- /column 2 -->
		</td>
	</tr>
</table>
</td>
</tr>
</tbody>
</table>

<table cellspacing="0" cellpadding="6" width="100%" style="margin: 15px 0; color:#000000; line-height:150%; font:300 16px 'Helvetica Neue', Helvetica, Arial, sans-serif; text-align:center;" border="0">
	<tbody>
		<tr>
			<td style="font-size:12px;font-weight:300;">
				<a href="http://wwww.refabd.com/terms" style="margin: 0 15px; display:inline-block;" target="_blank">Terms and Conditions</a>
				<a href="http://wwww.refabd.com/privacy" style="margin: 0 15px; display:inline-block;" target="_blank">Privacy Policy</a>
			</td>
		</tr>
	</tbody>

</table>


