<table border="0" align="center" cellpadding="20" cellspacing="0" width="80%" style="margin: 0 auto; padding: 0; max-width:640px;">
<tbody>
<tr>
<td style=" margin: 0; padding: 0;  border: 1px solid #eee; border-top: 5px solid #47C9AF;">
<table border="0" cellpadding="20" cellspacing="0" width="100%" style="padding: 30px 15px; color:#000000; line-height:150%; text-align:center; font:300 16px 'Helvetica Neue', Helvetica, Arial, sans-serif;">
	<tbody>
		<tr>
			<td valign="top" align="left" width="100" style="padding:0; vertical-align: bottom;">
				<img alt="Refabd" style="max-width:120px; " src="https://www.refabd.com/assets/images/logo.png">
			</td>	
	</tbody>
</table>
<table border="0" cellpadding="20" cellspacing="0" width="100%" style="padding: 0 15px; background:#ffffff; color:#000000; line-height:150%; text-align:left; font:300 16px 'Helvetica Neue', Helvetica, Arial, sans-serif;">
	<tbody>
		<tr>
			<td valign="top" width="100" style="padding:30px 0;">
				<p style="display:block;margin:15px 0; ">Hi Sarangh Somaraj,</p>
				<p style="display:block;margin:0; font-size:14px;  line-height:150%;">will you please take a minute to share your experience?</p>
			</td>
		</tr>
	</tbody>
</table>
<table align="center" cellspacing="0" cellpadding="6" style="padding: 0 15px;  border:0; color:#000000; line-height:150%; text-align:left; font:300 14px/30px 'Helvetica Neue', Helvetica, Arial, sans-serif;">
	<thead>
		<tr style="background:#fff;">
			<th scope="col" width="60%" style="border-bottom: 2px solid #eee; text-align:left;font-weight:500;">Your Purchase</th>
		</tr>
	</thead>
	<tbody>
		<tr width="100%" >
			<td  width="60%" style="text-align:left; vertical-align:middle; border-bottom: 2px solid #eee;  word-wrap:break-word;">
  				<div style="display:inline-block;">
  					<img style="text-align:left; max-width:100px;" src="image-4.png">
  				</div>
  				<div style="display:inline-block; vertical-align:top;">
  					<h1 style="margin:0; font-family:Arial;font-size:16px;font-weight:300;text-align:left;">LG F8091NDL2</h1>
  					<span style="line-height:24px;">Fully-automatic Front-loading Washing Machine (6 Kg,White)</span>
  					<span style="display:block;">Colors: White</span>
  				</div>
  			</td>
			
		</tr>
	</tbody>
</table>
<table cellspacing="0" cellpadding="6" width="100%" style="padding: 15px 0; color:#000000; line-height:150%;  font:300 16px 'Helvetica Neue', Helvetica, Arial, sans-serif; " border="0">
	<tbody>
		<tr>
			<td align="center" valign="top" style="padding: 30px 0; text-transform:capitalize; font-size:14px; line-height:150%; ">
				<b>Please select a rating for our service:</b>
			</td>
		</tr>
		<tr>
			<td align="center" valign="top" style="padding: 0; text-transform:capitalize; font-size:14px; line-height:150%; ">
				<ul style="list-style:none; padding:0; text-align:center;">
					<li style="display: inline-block; padding: 0 5px;">
						<a style="text-decoration: none; color:#000;" href="">
							<img style="width:100%;display:block; min-width:30px;max-width:45px;margin: 0 auto;" src="star.png">
							<p style="margin:10px 0; font-weight:400;">Excellent</p>
						</a>
					</li>
					<li style="display: inline-block; padding: 0 5px;">
						<a style="text-decoration: none; color:#000;" href="">
							<img style="width:100%;display:block;min-width:30px;max-width:45px;margin: 0 auto;" src="star.png">
							<p style="margin:10px 0; font-weight:400;">Good</p>
						</a>
					</li>
					<li style="display: inline-block; padding: 0 5px;">
						<a style="text-decoration: none; color:#000;" href="">
							<img style="width:100%;display:block;min-width:30px;max-width:45px;margin: 0 auto;" src="star.png">
							<p style="margin:10px 0; font-weight:400;">Fair</p>
						</a>
					</li>
					<li style="display: inline-block; padding: 0 5px;">
						<a style="text-decoration: none; color:#000;" href="">
							<img style="width:100%;display:block;min-width:30px;max-width:45px;margin: 0 auto;" src="star.png">
							<p style="margin:10px 0; font-weight:400;">Poor</p>
						</a>
					</li>
					<li style="display: inline-block; padding: 0 5px;">
						<a style="text-decoration: none; color:#000;" href="">
							<img style="width:100%;display:block;min-width:30px;max-width:45px;margin: 0 auto;" src="star.png">
							<p style="margin:10px 0; font-weight:400;">Awful</p>
						</a>
					</li>
				</ul>
			</td>
		</tr>
	</tbody>
</table>
<table cellspacing="0" cellpadding="6" width="100%" style="margin-bottom:15px;  color:#000000; line-height:150%; font:300 16px 'Helvetica Neue', Helvetica, Arial, sans-serif; text-align:center;" border="0">
	<tbody>
		<tr>
			<td style="font-size:12px; font-weight:300;">
					<span style="display: block; margin-bottom:15px;">
	  					<img style="text-align:left; max-height: 40px;" src="services-1.png">
					</span>
					<b style="color: #5DC6B1;">Hassle Free delivery</b>
					<p style="display: block; margin: 5px; 0">Vestibulum rutrum quam vitae fringilla tincidunt. Suspendis.</p>
				</div>
			</td>
			<td style="font-size:12px;font-weight:300;">
					<span style="display: block; margin-bottom:15px;">
	  					<img style="text-align:left; max-height:40px;" src="services-2.png">
					</span>
					<b style="color: #5DC6B1;">6 Month Warranty</b>
					<p style="display: block; margin: 5px; 0">Vestibulum rutrum quam vitae fringilla tincidunt. Suspendis.</p>
				</div>
			</td>
			<td style="font-size:12px;font-weight:300;">
				<span style="display: block; margin-bottom:15px;">
  					<img style="text-align:left; max-height:40px;" src="services-3.png">
				</span>
				<b style="color: #5DC6B1;">Quality check</b>
				<p style="display: block; margin: 5px; 0">Vestibulum rutrum quam vitae fringilla tincidunt. Suspendis.</p>
			</td>
			<td style="font-size:12px;font-weight:300;">
				<span style="display: block; margin-bottom:15px;">
  					<img style="text-align:left; max-height:40px;" src="services-4.png">
				</span>
				<b style="color: #5DC6B1;">Post sale care/service</b>
				<p style="display: block; margin: 5px; 0">Vestibulum rutrum quam vitae fringilla tincidunt. Suspendis.</p>
			</td>
		</tr>
	</tbody>
</table>
<table  class="social" width="100%" cellspacing="0" cellpadding="0" style="background: #5DC6B1; color:#ffffff; padding: 15px; font:300 12px 'Helvetica Neue', Helvetica, Arial, sans-serif;">
	<tr>
		<td>

			<!--- column 1 -->
			<table align="left" class="column">
				<tr>
					<td>				
						<table border="0" cellspacing="0" cellpadding="0" style="color:#ffffff; font:300 12px 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size:12px;">
                            <tbody>
                                <tr>
                                    <td style="font-weight: 200;">Get the App: </td>
                                    
                                    <td>
                                        <a style="display: inline-block; margin:0 10px;" href="#" target="_blank">
                        					<img style="max-height:15px;width:auto;" src="android-link.png">
                        				</a>
                                   
                                        <a style="display: inline-block; margin:0 10px 0 5px;" href="" target="_blank">
                        					<img style="max-height:15px;width:auto;" src="ios-link.png"></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

						
					</td>
				</tr>
			</table><!-- /column 1 -->	
			
	
			<!-- /column 3 -->
			<table align="right" class="column">
				<tr>
					<td align="center" style="color:#ffffff; font-size:12px; font-weight: 200;">
                        &copy; 2017&#45;Refabd.
                    </td>
				</tr>
			</table><!-- /column 2 -->
		</td>
	</tr>
</table>
</td>
</tr>
</tbody>
</table>

<table cellspacing="0" cellpadding="6" width="100%" style="margin: 15px 0; color:#000000; line-height:150%; font:300 16px 'Helvetica Neue', Helvetica, Arial, sans-serif; text-align:center;" border="0">
	<tbody>
		<tr>
			<td style="font-size:12px;font-weight:300;">
				<a href="#" style="margin: 0 15px; display:inline-block;">Terms and Conditions</a>
				<a href="#" style="margin: 0 15px; display:inline-block;">Privacy Policy</a>
			</td>
		</tr>

</table>


