<!DOCTYPE html>
<html lang="en" ng-app="pocketin">
    <!-- Include the Head section. This includes the css and js files -->
    <?php include 'blocks/head.php'; ?>

<body class="Pocketin-skin">
    <!-- Google Tag Manager -->
    <noscript>
        <iframe src="//www.googletagmanager.com/ns.html?id=GTM-NHSD7W" height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <script>
        (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-NHSD7W');
    </script>
    <!-- End Google Tag Manager -->
    <!-- Show the loader before angular is loaded -->
    <!--div ng-show="::false" class='loader' ng-if='isRouteLoading'>
        <img src='assets/images/loader.gif'>
    </div-->
        <!-- wrap everything inside ng-cloak to show it only when angular is loaded -->
    <div ng-cloak>
        <?php include 'blocks/header_refabd.php'; ?>
        <!-- Main content and views -->
