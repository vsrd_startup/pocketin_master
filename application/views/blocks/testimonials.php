<style>
.testi-page blockquote {
	border: 1px solid rgba(71, 201, 175, 0.25);
	border-radius: 25px;
}

.refunderline:before {
    content: "";
    position: absolute;
    width: 50%;
    height: 2px;
    bottom: -10px;
    left: 25%;
    border-bottom: 1px solid darkorange;
}
</style>
<div class="main-container">
	<div class="page-content">
		<div class="page-content-area">
			<div class="container testi-page">
				<!--div class="sub-header col-xs-12 text-center">
					<div class="subtitle fancy">
						<span>
							<h4>Customer Testimonials</h4>
						</span>
					</div>
				</div-->
				<!--div class="text-left">
					<div class="testi-content">
					<p>Reason for purchasing this item:</p>
					<p>Recently moved in and furnishing afresh</p>
					<p>How did you hear about us:</p>
					<p>What other options you have evaluated before purchasing form Pocketin:</p>
					<p>Motivation for choosing Pocketin:</p>
					<p>Overall, how satisfied were you with your product:</p>
					<p>Testimonial:</p>
					<p>Additional Feedback:</p>
					<p>How likely are you to recommend Pocketin to others:</p>
					</div>
				</div-->

		<div class="hor-gird user-testimonials text-center" style="padding-left: 80px;">
		<div class="sub-header col-xs-12">
            <div class="subtitle fancy"><span class="refunderline"><h4>What our customers are saying?</h4></span></div>
		</div>
		<div class="row relative">
			<div class="col-lg-11 col-centered">
				<!--ul class="grid-control">
					<li class="control-left"><span class="icon pocketin-icon-circle-left"></span></li>
					<li class="control-right"> <span class="icon pocketin-icon-circle-right"></span></li>
				</ul-->
				<div class="row relative" style="margin-bottom: 10px;">
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 item-col">
						<div class="user-testimonial">
							<div class="text-left">
								<div class="col-sm-12 no-pad">
									<blockquote>
									    <p>It is an awesome experience with Refabd they helped me in initial set up in Bangalore as I m completely new to these purchases. We shall be happy if u start furniture dinning and furnishing a complete house</p>
									    	<div class="testimonial-username">
									    		<strong>Very Satisfied</strong>
									    		<span>Hassle free Delivery and installation</span>
									    	</div>
									</blockquote>
					            </div>
							</div>
						</div>
					</div>

					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 item-col">
						<div class="user-testimonial">
							<div class="text-left">
								<div class="col-sm-12 no-pad">
									<blockquote>
									    <p>Refabd contacted me while I was browsing items on quikr and I bought a washing machine which was serviced with a six month guarantee and also purchase back option. I was fine with the delivery time, but I feel that It is one of the pain areas of your other competitors like olx/quikr, because their delivery is very bad and there is no reliability on the seller.</p>
									    	<div class="testimonial-username">
									    		<strong>Delighted</strong>
									    		<span>Hassle free Post sale handlings</span>
									    	</div>
									</blockquote>
					            </div>
							</div>
						</div>
					</div>
				</div>

				<div class="row relative" style="margin-bottom: 10px;">
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 item-col">
						<div class="user-testimonial">
							<div class="text-left">
								<div class="col-sm-12 no-pad">
									<blockquote>
									    <p>Great experience.. Produxt got delivered within 16hrs..Great work.. come up with an app and improve upon the website</p>
									    	<div class="testimonial-username">
									    		<strong>Very Satisfied</strong>
									    		<span>Recently moved in and furnishing afresh</span>
									    	</div>
									</blockquote>
					            </div>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 item-col">
						<div class="user-testimonial">
							<!-- <div class="user-poduct-thumb">
								<img src="assets/images/image-4.png" alt="Jackets" width="360" height="466">
							</div> -->
							<div class="text-left">
								<div class="col-sm-12 no-pad">
									<blockquote>
									    <p>Received the product in a good condition , price was satisfactory and post sales customer support is quite good. Would definitely recommend.</p>
									    	<div class="testimonial-username">
									    		<strong>Value for money products</strong>
									    		<span>Somewhat satisfied</span>
									    	</div>
									</blockquote>
					            </div>
							</div>
						</div>
					</div>
				</div>

				<div class="row relative" style="margin-bottom: 10px;">
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 item-col">
						<div class="user-testimonial">
							<div class="text-left">
								<div class="col-sm-12 no-pad">
									<blockquote>
									    <p>It's a great start with an assured service and customer friendly support. All the best.It was wonderful shopping at Refabd. Very satisfied with the products and support.</p>
									    	<div class="testimonial-username">
									    		<strong>Very Satisfied</strong>
									    		<span>Recently moved in and furnishing afresh</span>
									    	</div>
									</blockquote>
					            </div>
							</div>
						</div>
					</div>

					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 item-col">
						<div class="user-testimonial">
							<div class="text-left">
								<div class="col-sm-12 no-pad">
									<blockquote>
									    <p>It was a great response from Refabd team. Quality products. Especially liked the buy back option. Hope the team won't step back on it.</p>
									    	<div class="testimonial-username">
									    		<strong>Very Satisfied</strong>
									    		<span>Value for money products</span>
									    	</div>
									</blockquote>
					            </div>
							</div>
						</div>
					</div>
				</div>


				<div class="row relative" style="margin-bottom: 10px;">
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 item-col">
						<div class="user-testimonial">
							<div class="text-left">
								<div class="col-sm-12 no-pad">
									<blockquote>
									    <p>I'm very pleased with the entire experience! Keep up the good work.I recently moved to Bangalore and was looking for a washing machine. I was looking up on Facebook and olx. I came across refabd. The response through the customer care was welcoming, detailed and the staff took me through the entire process. The best part I liked was that refabd did all the due diligence on their part. That was a big relief and money saviour.I would definitely recommend your services to anyone in need. In fact I talk about it with my friends and family often. Keep up the good work! :)</p>
									    	<div class="testimonial-username">
									    		<strong>Delighted</strong>
									    		<span>Hassle free Post sale handlings</span>
									    	</div>
									</blockquote>
					            </div>
							</div>
						</div>
					</div>

					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 item-col">
						<div class="user-testimonial">
							<div class="text-left">
								<div class="col-sm-12 no-pad">
									<blockquote>
									    <p>Overall I had a decent experience, but still there certain open issues which still to be addressedAs this is a startup, surely there is a lot to improve, installation, post sales service and customer service areas are the key one's which needs improvement.</p>
									    	<div class="testimonial-username">
									    		<strong>Satisfied</strong>
									    		<span>Recently moved in and furnishing afresh</span>
									    	</div>
									</blockquote>
					            </div>
							</div>
						</div>
					</div>
				</div>

				<div class="row relative" style="margin-bottom: 10px;">
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 item-col">
						<div class="user-testimonial">
							<div class="text-left">
								<div class="col-sm-12 no-pad">
									<blockquote>
									    <p>PocketIn provided a hassle free and seamless delivery of product.</p>
									    	<div class="testimonial-username">
									    		<strong>Satisfied</strong>
									    		<span>Recently moved in and furnishing afresh</span>
									    	</div>
									</blockquote>
					            </div>
							</div>
						</div>
					</div>

					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 item-col">
						<div class="user-testimonial">
							<div class="text-left">
								<div class="col-sm-12 no-pad">
									<blockquote>
									    <p>Great work.. Produxt got delivered within 16hrs. Come up with an app and improve upon the website</p>
									    	<div class="testimonial-username">
									    		<strong>Great experience</strong>
									    		<span>	Value for money products</span>
									    	</div>
									</blockquote>
					            </div>
							</div>
						</div>
					</div>
				</div>

				<div class="row relative" style="margin-bottom: 10px;">
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 item-col">
						<div class="user-testimonial">
							<div class="text-left">
								<div class="col-sm-12 no-pad">
									<blockquote>
									    <p>Okay delivery support. Fridge working as expected. Minor issues.</p>
									    	<div class="testimonial-username">
									    		<strong>Somewhat satisfied</strong>
									    		<span>Recently moved in and furnishing afresh</span>
									    	</div>
									</blockquote>
					            </div>
							</div>
						</div>
					</div>

					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 item-col">
						<div class="user-testimonial">
							<div class="text-left">
								<div class="col-sm-12 no-pad">
									<blockquote>
									    <p>Great service. Ordered a washing machine and got it delivered within two days. It's working fine and your service is awsome.</p>
									    	<div class="testimonial-username">
									    		<strong>Very satisfied</strong>
									    		<span>	Value for money products</span>
									    	</div>
									</blockquote>
					            </div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>

	</div><!-- testimonial ends here-->

			</div>
		</div>
	</div>
</div>
