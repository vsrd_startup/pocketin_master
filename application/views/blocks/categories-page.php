<style>
.clicknochange:hover:focus:visited  {
	border:0px;
	text-decoration: none;
	color:inherit;
}
.clearout {
	/*background-color: #2dbcab !important;*/
	background-color: #f1990f !important;
    position: absolute;
    left: 0;
    top: 0;
    width: 40px;
    height: 40px;
    border-radius: 50%;
    /* padding: 10px; */
    border: 1px solid #d48407;
    /* text-align: center; */
    padding-left: 4px;
    padding-top: 11px;
}
.clearout > span {
	color: white;
	font-size: 14px;
	font-weight: 300;
}
#catdiv .stock-availability.catview-available {
	top: 0;
    left: 0;
}
.stock-availability.catview-available.brandnew {
	background: #337ab7!important;
	padding: 5px;
}

.products-pagination {
    width: 125px!important;
}

.products-pagination .pagination>li>a {
    color: #47C9AF!important;
}
.products-pagination .pagination>li:first-child a {
    border-right: 1px solid #DEDEDE!important;
}
.pagination>li>a:focus, .pagination>li>a:hover, .pagination>li>span:focus, .pagination>li>span:hover {
	background-color: #fff!important;
}
</style>
<div id="category" ng-init=<?php echo '"initializeCatId('.$category_id.')"' ?>>
	<div class="home page-content">
		<div class="page-content-area">
			<div class="container">

				<div class="products-breadcrumb">
					<ol class="breadcrumb" style="margin-bottom: 5px;">
						<li><a href=".">Home</a></li>
						<li><a href="collection">Collections</a></li>
						<li class="active"><a href="">{{catname}}</a></li>
					</ol>
				</div>
					<div ng-show="catloading" class='catloader'>
				        <img src='assets/images/catloading.gif'>
				    </div>
				<div class="row" ng-show="!catloading">
					<!--div class="filters col-xs-12 col-sm-3" ng-controller="FilterController as ctrl">
					<div id="accordion" class="panel-group">
					    <div class="panel panel-default" ng-repeat="filter in filters">
					        <div class="panel-heading">
					            <span class="panel-title">
					                <a data-toggle="collapse" data-parent="#accordion" data-target="#{{filter.filtertype|removeSpaces}}">
					                	{{filter.filtertype}}
										<span class="icon pull-right pocketin-icon-circle-down"></span>
					                </a>
					            </span>
					        </div>
					        <div id="{{filter.filtertype|removeSpaces}}" class="panel-collapse collapse">
					            <div class="panel-body">
					                <form role="form">
									    <div class="checkbox" ng-repeat="value in filter.filters">
									      <label><input type="checkbox" value="{{value.id}}">{{value.name}}</label>
									    </div>
									</form>
					            </div>
					        </div>
					    </div>
					</div>
					</div-->
					<div class="product-results-container container">
						<div class="product-results-header">
							<label class="results-info inline">Showing {{itembegin}}–{{itemend}} of {{totalitems}} results</label>
							<div class="inline sort-dropdown dropdown pull-right">
							<div class="checkbox" style="display:inline-block; color: #47C9AF; padding: 5px; margin-right: 10px;">
								<label >
									<input type="checkbox" value="" ng-model="hideoos" ng-click="changeOos()">
									Exclude Sold Out
								</label>
							</div>
							  <button class="btn dropdown-toggle btn-sort" type="button" data-toggle="dropdown">
								  <div class="inline">
								    {{currentsort}}
								  	<span class="caret"></span>
								  </div>
							  </button>
							  <ul class="dropdown-menu" role="menu">
							    <li ng-repeat="sorttype in sorttypes"><a ng-click="sortProducts(sorttype)">{{sorttype}}</a></li>
							  </ul>
							</div>
						</div>

						<!-- Item results -->
						<div class="hor-gird product-results text-center" ig="catdiv">
							<div ng-show="totalitems == 0" class="coming-soon">
								<p style="font-size: 20px; color: #47C9AF;">Coming Soon..</p>
							</div>
							<div class="row">
								<div class="col-xs-12 col-sm-6 col-md-4 col-lg-15 item-col" ng-class="item.issold == 1 ? 'item-unavailable' : ''"  ng-repeat="item in items">
									<div class="item-col-wrap">
										<!--div class="offers-tag" ng-show="item.compressorwarranty != 0">
											<label><span class="glyphicon glyphicon-tag"></span>{{item.compressorwarranty}} year warranty</label>
										</div-->
										<!--div class="badge" style="background-color: #2dbcab !important; position:absolute; left: 0; top:0"
										ng-show="item.compressorwarranty != 0">
											<span style="color: white; font-size: 20px; position: absolute; top: 1px;">{{item.compressorwarranty}}</span> <span style="padding-left:14px; color:light-grey">year warranty</span>
										</div-->
										<!--div class="clearout"><span>{{item.closingoffer}}%</span></div-->

										<!--button class="btn btn-md btn-add-to-wishlist" ng-class="item.inwishlist == 1 ? 'active block' : ''" data-toggle="tooltip" data-placement="top" data-original-title="{{item.inwishlist == 0 ? 'Add to Wishlist': 'Remove from Wishlist'}}" type="button" ng-click="item.inwishlist=item.inwishlist==1?0:1; addtowishlist(item, item.inwishlist)">
				                			<span class="glyphicon glyphicon-heart-empty"></span>
				                		</button-->
				                		<a href="" style="position: absolute;right: 4px;top: 4px;border: 0px;background: transparent;padding: 5px;" class="clicknochange">
				                		<span class="glyphicon glyphicon-heart-empty" style="font-size:18px;color: #ff7b7b;" ng-show="item.inwishlist == 0" ng-click="item.inwishlist=item.inwishlist==1?0:1; addtowishlist(item, item.inwishlist)"></span>
				                		<i class="fa fa-heart-o" aria-hidden="true"></i>
				                		<span class="glyphicon glyphicon-heart" style="font-size:18px;color: #ff7b7b;" ng-show="item.inwishlist == 1" ng-click="item.inwishlist=item.inwishlist==1?0:1; addtowishlist(item, item.inwishlist)"></span>
				                		</a>
				                		<div class="label stock-availability catview-unavailable" ng-show="item.issold == 1">Sold Out</div>
										<div class="label stock-availability catview-available" ng-show="item.isfactory == 1 && item.issold != 1">Unboxed</div>
										<div class="label stock-availability catview-available brandnew" ng-show="item.brandnew == 1 && item.issold != 1">Brand New</div>
										<a class="" href="product/{{catname|toproducturl}}/{{item.productitem_id}}/{{item.product_title|toproducturl}}">
											<div class="poduct-grid-thumb">
												<img ng-src="assets/images/products/normal/{{item.image_full}}" alt="{{item.product_title}}" width="360" height="466">
											</div>
											<div class="poduct-grid-detail text-left">
												<div class="col-sm-12">
													<div class="item-text-info">
														<h1 class="product-name">{{item.product_title}}
															<span class="product-description">{{item.product_description}}</span>
									                	</h1>
								                	</div>
									                <div class="row">
										                <div class="col-xs-12 inline">
										                	<div style="color:#676767;text-decoration: line-through; font-size:14px; font-weight: 200"><span>&#x20B9;</span>{{item.saleprice*1.1 | number:0}}</div>
										                	<div class="product-price">
										                	<!--span style="text-decoration: line-through; color: grey; font-size: 12px; opacity: .5; padding-bottom: 5px;"><span>&#x20B9;</span>{{item.saleprice * (1+item.closingoffer/100) | number:0}}</span--><span style="color: #20a797; font-size: 18px;font-weight:400">
										                	<span style="font-size: 12px; color: black;    font-weight: 100;">Offer Price</span><span>&#x20B9;</span>{{item.saleprice | number:0}}</span>
										                	<span class="prodoffer inline text-right">({{100-((item.saleprice/item.mrp)*100) | number:0}}% off)</span>
										                	</div>
										                </div>
										                <div class="col-xs-6 inline text-right" ng-show="item.brandnew == 0">
										                	<div class="quality-index" ng-class="item.qualityindex >= 4 ? 'green-dark' : (item.qualityindex >=3 ? 'green-bright' : (item.qualityindex >=2 ? 'yellow' : (item.qualityindex< 2 ? 'red' : 'grey')))">
									                			<span class="icon pocketin-icon-quality-index"></span>{{item.qualityindex|number:1}}
									               			</div>
										                </div>
									                </div>
									                <div class="market-price"><span>&#x20B9;</span>{{item.mrp | number:0}}<small>Appr. Market Price</small></div>
									            </div>
											</div>
										</a>
									</div>
								</div>
							</div>
						</div>

						<nav class="products-pagination" ng-init="page = 1">
						  <ul class="pagination text-center">
						    <li class="pull-left" ng-class="{active:0}">
						      <a href="" aria-label="Previous" ng-click="goToPage(currentpage-1)" scroll-on-click>
						        <span aria-hidden="true">Prev</span>
						      </a>
						    </li>
						    <!--li ng-repeat="page in pages">
						    	<a href="" ng-class="page == currentpage ? 'active': ''" ng-click="goToPage(page)" scroll-on-click>{{page}}</a>
						    </li-->
						    <li class="pull-right">
						      <a href="" aria-label="Next" ng-click="goToPage(currentpage+1)" scroll-on-click>
						        <span aria-hidden="true">Next</span>
						      </a>
						    </li>
						  </ul>
						<!--div class="movetotop">
					      <a href="" scroll-on-click>
					      <span class="glyphicon glyphicon-chevron-up"></span>
					      </a>
					  	</div-->
						</nav>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
