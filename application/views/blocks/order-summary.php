<!-- Google Code for Conversion tracking for successful sale Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 845562577;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "Pq0dCMKY9nIQ0YWZkwM";
var google_conversion_value = 2000.00;
var google_conversion_currency = "INR";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/845562577/?value=2000.00&amp;currency_code=INR&amp;label=Pq0dCMKY9nIQ0YWZkwM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<div class="home page-content">
	<div class="page-content-area">
		<div class="container">
			<div class="products-breadcrumb">
				<ol class="breadcrumb" style="margin-bottom: 5px;">
					<li>
						<a href=".">Home</a>
					</li>
					<li>
						<a href="myorders">Order History</a>
					</li>
					<li class="active">
						<a href="">Order: {{orderid}}</a>
					</li>
				</ol>
			</div>

			<div class="per-order-details">
				<div class="order-summary">
					<div class="sub-header col-xs-12 text-center">
						<div class="subtitle fancy">
							<span>
								<h4>Order Summary</h4>
							</span>
						</div>
					</div>

					<div class="row">
						<div class="col-sm-12 col-xs-12 text-center1">
							<div class="">
								<div class="row">
									<div class="col-sm-12 col-xs-12 order-summary-block">
										<ul>
											<li>
												<h4>Congratulations, Your order has been placed successfully!</h4>
											</li>
										</ul>
									</div>

									<div class="col-sm-6 col-xs-12 order-summary-block">
										<ul class="">
											<li>
												<p>
													Your order will be delivered in two to three business days. You will get a call from our representative to fix a delivery slot. Order status will be emailed to
													<cite>{{address.email}}.</cite>
												</p>
											</li>
											<li>
												<label>Order ID:</label>
												<span class="order-id">PI:{{orderid}}</span>
												{{itemcount}} (Items)
												<!--span class="order-date">{{orderdate}}</span-->
											</li>
											<li ng-show="paymentmethod == 'cod'">
												<label>Amount to Pay:</label> <strong class="amount">{{amount}}</strong>
												<span class="payment-method">Cash on Delivery</span>
											</li>
											<li ng-show="paymentmethod == 'online'">
												<label>Amount Paid:</label> <strong class="amount">{{amount}}</strong>
												<span class="payment-method">Online Payment</span>
											</li>

											<li>
												You can track or cancel order from my orders,
												<a class="btn btn-xs btn-my-orders" style="padding: 5px;" href="myorders">My Orders</a>
											</li>
											<li>
												Avail extra discount before delivery by referring your friends.
												<a class="btn btn-xs" style="padding: 5px;border: 1px solid #d61b5e; background: #d61b5e; color: white" href="myreferral">My Referrals</a>
											</li>
										</ul>
									</div>

									<div class="col-sm-6 col-xs-12">
										<div class="order-address">
											<div class="row">
												<div class="col-sm-12 col-xs-12"> <strong>Shipping address</strong>
													<label class="name">
														{{address.name}}
														<span class="phone">
															<span class="glyphicon glyphicon-phone"></span>
															{{address.phonenumber}}
														</span>
													</label>
													<p class="">
														{{address.address_line_one}}
														<br/>
														{{address.address_line_two}}, {{address.pincode}}
													</p>
													<p class="">{{address.city}}, {{address.state}}</p> <b class="landmark">{{address.landmark}}</b>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-sm-12 col-xs-12">
							<div class="order-info-sec no-pad">
								<ul class="row table-head">
									<li class="col-xs-6">
										<strong>Item(s)</strong>
									</li>
									<li class="hidden-xs col-xs-6 text-right">
										<strong>Price</strong>
									</li>
								</ul>
								<ul>
									<li class="row single-item" ng-repeat="item in items">
										<div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
											<div class="row">
												<div class="col-xs-4 col-sm-4 col-md-4 col-lg-3 cart-thumb">
													<img ng-src="assets/images/products/normal/{{item.images[0].image_full}}">
												</div>
												<div class="col-xs-8 col-sm-7 col-md-8 col-lg-9 cart-item-details">
													<h1 class="product-name">
														{{item.product_title}}
														<div class="quality-index" ng-class="item.qualityindex >= 4 ? 'green-dark' : (item.qualityindex >=3 ? 'green-bright' : (item.qualityindex >=2 ? 'yellow' : (item.qualityindex< 2 ? 'red' : 'grey')))">
											<span class="icon pocketin-icon-star-empty"></span>
											{{item.qualityindex| number:1}}
											</div>
														<span class="product-description">{{item.product_description}}</span>
													</h1>
													<div class="text-left total hidden-sm hidden-md hidden-lg" style="display:block;">
														<span class="text-left value item-total product-price">
															<span>&#x20B9;</span>
															New{{item.saleprice}}
														</span>
													</div>
													<!--div class="colors-available">
														<label class="inline">Colors:</label>
														<ul class="colors-list inline">
															<li>
																<span class="item-color"></span>
															</li>
														</ul>

													</div-->
												</div>
											</div>
										</div>
										<div class="hidden-xs col-xs-offset-4 col-xs-5 col-sm-offset-0 col-sm-2 col-md-offset-0 col-md-2 col-lg-offset-0 col-lg-2 text-right">
											<div class="total" style="display:block;">
												<span class="text-left value item-total product-price">
													<span>&#x20B9;</span>
													{{item.saleprice * (100-offer)/100 * (100-cdisc)/100 | number:2}}

												</span>
											</div>

										</div>
									</li>
									<li class="row single-item">
										<div class="col-xs-12 col-sm-10 col-md-10 col-lg-9"></div>

										<div class="col-xs-offset-4 col-xs-8 col-sm-offset-0 col-sm-2 col-md-offset-0 col-md-2 col-lg-offset-0 col-lg-3 text-right">
											<div class="grand-total" style="display:block;">
												<label class="inline">Total:</label>
												<strong class="inline"><span>&#x20B9;</span>
													{{amount | number:2}}</strong>
											</div>

										</div>
									</li>
								</ul>
							</div>
						</div>

						<div class="col-sm-12 col-xs-12 text-center continue-shopping">
							<a class="btn btn-send btn-shopping" style="padding: 8px;" href="collection">Continue Shopping</a>
							<p>
								Used appliances shopping has never been this easy. Enjoy free home delivery with warranty and service. Keep Shopping
							</p>
						</div>

					</div>

				</div>

			</div>

		</div>
	</div>
</div>
