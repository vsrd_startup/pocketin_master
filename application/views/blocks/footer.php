<div ng-if='!isRouteLoading' class="footer">
	<div class="container">
		<!-- Services -->
		<div class="services text-center">
			<div class="row">
				<div class="col-xs-12 col-sm-3">
					<span class="icon pocketin-icon-Box-1 huge"></span>
					<label>Free delivery</label>
					<p>Delivery absolutely free for customers. Just pay for the product not a penny extra</p>
				</div>
				<div class="col-xs-12 col-sm-3">
					<span class="icon pocketin-icon-Riddon huge"></span>
					<label>6 Months Warranty &amp; 7 days replacement</label>
					<p>A written guarantee and a promise from us for all the products</p>
				</div>
				<div class="col-xs-12 col-sm-3">
					<span class="icon pocketin-icon-Thumps-up huge"></span>
					<label>Quality check</label>
					<p>All products undergo thorough inspection and gets genuine quality rating</p>
				</div>
				<div class="col-xs-12 col-sm-3">
					<span class="icon pocketin-icon-Settings-2 huge"></span>
					<label>Post sale care/service</label>
					<p>You have any query? We are available 24/7 for faster resolution</p>
				</div>
			</div>
		</div>
		<ul class="nav">
			<li style="color: #9fa6ad; padding: 5px;"><span class="glyphicon glyphicon-phone-alt"></span> For Sale Related Queries: 
			<b>70 226 30 100</b></li> |
			<li style="color: #9fa6ad;padding: 5px;"><span class="glyphicon glyphicon-phone-alt"></span> For Complaints &amp; Service: 
			<b>70 226 30 270</b></li> |
			<li style="color: #9fa6ad;padding: 5px;"><span class="glyphicon glyphicon glyphicon-envelope"></span> Feedback write to us: 
			<b>hello@refabd.com</b></li>
		</ul>

		<!-- <hr> -->
		<div class="inline">
			<ul class="nav">
				<li><a href="aboutus">About us</a></li>
				<li><a href="contactus">Contact Us</a></li>
				<li><a href="terms">Terms of use</a></li>
				<li><a href="privacy">Privacy</a></li>
				<!--<li><a href="">Download App</a></li>-->
				<li><a href="myorders">Track Order</a></li>
				<li><a href="myaccount">Raise Service Request</a></li>
				<!-- <li><a href="servicerequest">Raise Service Request</a></li> -->
			</ul>
		</div>

		<div class="copyright pull-right">
			<span>© 2016 Refabd</span>
			<ul class="inline nav footer-nav footer-link social text-right">
				<li><a href="https://www.facebook.com/pocketin.in/" target="_blank"><img src="assets/images/social/fb-min.png"></a></li>
				<li><a href="https://twitter.com/vsrdpocketin" target="_blank"><img src="assets/images/social/tw-min.png"></a></li>
			</ul>
		</div>
	</div>
</div>
