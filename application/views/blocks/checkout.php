<?php
include_once(dirname(__FILE__) . '/zestphpsdk/source/ZestProcessor.php');
$zest = new ZestProcessor();
?>
<script>var logourl = 'assets/zestphpsdk/web/images/'; </script>
<script>var emiUrl = '<?php echo $zest->emiurl(); ?>'; </script>
<style>
.zest-emi-table .zest-emi {
	width:100%!important;
}
.zest-emi .zest-cell-1 {
	overflow: hidden!important;
}
.emisel {
	display: none;
}
.summary-grid.paymentoptions {
	margin-top: 10px;
	border: 1px solid #e3e3e3;
	padding: 10px;
}
.summary-grid.paymentoptions ul {
	margin-top: 10px;
	margin-bottom: 20px;
}
	.summary-grid.paymentoptions li {
		width: 22%;
	}
	.summary-grid.paymentoptions li a {
		border-radius: 0px!important;
		padding: 0px!important;
		background-color: transparent;
		text-transform: uppercase;
    	line-height: 16px!important;
    	text-align: center;
    	font-size: 12px;
    	color: black;
	}
	.summary-grid.paymentoptions li.active>a {
		border-bottom: 1px solid #d61b5e;
		color: black;
	}
	.summary-grid.paymentoptions .tab-content li {
		width: 100%;
		margin-bottom:5px;
		list-style-type: disc;
	}

	.summary-grid.paymentoptions li.active>a:hover {
		background-color:transparent!important;
		color: black;

	}
	.summary-grid.paymentoptions li.active>a:focus {
		background-color:transparent!important;
		color:black;
	}
	.summary-grid .paymentoptions .tab-content label{
	    font-size: 16px;
    	/* margin-bottom: 8px; */
    	margin-top: 5px;
    	text-decoration: underline;
	}
	.summary-grid .paymentoptions .tab-content  p {
		    line-height: 19px!important;
		    font-weight: 100;
	}
	.summary-grid .paymentoptions .tab-pane{
		 padding: 0 5px;
	}
	th, td {
    text-align: center!important;
    vertical-align: middle!important;
	}
	th {
		border-bottom: 0px!important;
	}
</style>
<div class="home page-content">
	<div class="page-content-area">
		<div class="container">
			<div class="products-breadcrumb">
				<ol class="breadcrumb" style="margin-bottom: 5px;">
					<li>
						<a href="">Home</a>
					</li>
					<li>
						<a href="cart">Shopping Cart</a>
					</li>
					<li class="active">
						<a href="">Checkout</a>
					</li>
				</ol>
			</div>

			<div class="row shopping-cart checkout">
				<div class="col-xs-12 col-sm-12 col-md-8" ng-init="addnewaddress = 0">
					<!--div class="row delivery text-left sign-up-offers">
						<span class="glyphicon glyphicon-money-1"></span>
						<label style="font-size: 18px;">Cash on Delivery is the supported payment now.</label>
						<br/>
						<label style="font-size: 14px;">
							You can pay the amount after the product is delivered!! Keep Shopping..
						</label>
					</div-->
					<div class="delivery-addresses" ng-show="userloggedin()">
						<div class="row table-head">
							<div class="col-xs-8 col-sm-4 col-md-4 col-lg-8 no-pad">
								<label>Delivery Address</label>
							</div>
							<div class="col-xs-1 col-sm-4 col-md-4 col-lg-2 text-center">&nbsp;</div>
							<div class="col-xs-3 col-sm-4 col-md-4 col-lg-2 text-center">Step 1 of 2</div>
						</div>
					</div>
					<div class="row address">
						<div class="col-xs-12">
							<ul class="row saved-addresses" ng-show="userloggedin() == 1">
								<li class="col-xs-12 col-md-4 col-sm-4 active" ng-repeat="address in addresses">
									<a href="" ng-click="selectAddress(address)">
										<div>
											<span class="glyphicon glyphicon-ok defaultaddress" ng-show="address.selected == 1"></span>
											<label>{{address.name}}</label>
											<p class="">{{address.address_line_one}}</p>
											<p class="">{{address.address_line_two}}</p>
											<p class="">{{address.landmark}}</p>
											<p class="">{{address.pincode}}</p>
											<p class="">{{address.city}}, {{address.state}}</p>
											<span class="phone">{{address.phonenumber}}</span>
											<button type="submit" ng-class="address.class ? address.class : 'btn btn-address-default'" ng-click="selectAddress(address)">Select Adderess</button>
										</div>
									</a>
								</li>
							</ul>
							<br/>
							<br/>

							<hr/>
							<div class="col-xs-12" style="margin: 10px;" ng-show="userloggedin()">
							<a href="myaddress" class="btn btn-xs btn-my-orders" style="padding: 5px;">Add/Edit Address</a>
							<br></div>
						<div class="col-xs-12 text-center" ng-show="noaddress == 1 && userloggedin() == 0">
							<button class="btn btn-add-new-address" ng-click="addnewaddress = 1">
								<span class="icon pocketin-icon-plus"></span>
								Add New Address
							</button>
						</div>
						<div class="col-xs-12 new-address-form" ng-show="addnewaddress == 1 || userloggedin() == 0">
							<div class="row">
								<div class="col-xs-12 text-center">
									<label class="title">
										<!-- Enter a new shipping address
											<br/>
										-->
										<p>Enter Shipping Address</p>
									</label>
									<form class="row text-left" role="form" name="newaddrform" ng-submit="newaddrform.$valid && saveNewAddress()" novalidate>

										<div class="form-group col-xs-12 col-lg-6 col-md-6">
											<label for="name">Name</label>
											<input type="text" class="form-control" id="name" ng-model="newaddress.name" required ng-pattern="/^[a-zA-Z\s]*$/" name="name">
											<p class="validationerror" ng-show="newaddrform.name.$dirty && newaddrform.name.$invalid">Only Letters!!</p>
										</div>
										<div class="form-group col-xs-12 col-lg-6 col-md-6">
											<label>Email</label>
											<input type="email" name="email" class="form-control" ng-model="newaddress.email" ng-pattern="/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/" required>
											<p class="validationerror" ng-show="newaddrform.email.$dirty && newaddrform.email.$invalid">Not a Valid Email!!</p>
										</div>
										<div class="form-group col-xs-12 col-lg-6 col-md-6">
											<label for="phone">Phone</label>
											<input type="number" name="phone" class="form-control" id="phone" ng-model="newaddress.phonenumber" required ng-minlength = "10" ng-maxlength = "10" ng-pattern="/^[1-9]+[0-9]*$/">
											<p class="validationerror" ng-show="newaddrform.phone.$dirty && newaddrform.phone.$invalid">Exactly 10 digits, first digit non zero</p>
										</div>
										<div class="form-group col-xs-12 col-lg-6 col-md-6">
											<label>City</label>
											<input type="text" value="Bangalore" class="form-control" disabled></div>
										<div class="form-group col-xs-12 col-lg-6 col-md-6">
											<label>Address Line 1(Apartment, Floor, Block)</label>
											<input type="text" class="form-control" ng-model="newaddress.address_line_one" required></div>
										<div class="form-group col-xs-12 col-lg-6 col-md-6">
											<label>Address Line 2 (Street, Location)</label>
											<input type="text" class="form-control" ng-model="newaddress.address_line_two"></div>
										<div class="form-group col-xs-12 col-lg-6 col-md-6">
											<label>Landmark</label>
											<input type="text" class="form-control" ng-model="newaddress.landmark" required></div>
										<div class="form-group col-xs-12 col-lg-6 col-md-6">
											<label>Zip/Postal Code</label>
											<input type="number" name="zip" class="form-control" ng-model="newaddress.pincode" required ng-pattern="/^[1-9]+[0-9]*$/" ng-minlength = "6" ng-maxlength = "6">
											<p class="validationerror" ng-show="newaddrform.zip.$dirty && newaddrform.zip.$invalid">Exactly 6 digits, first digit non zero</p>
										</div>
										<!--div class="checkbox col-xs-12 text-left">
										<label>
											<input type="checkbox" ng-model="newaddress.isdefault">Set Default Address</label>
									</div-->
									<div class="col-xs-6 text-left">
										<button type="submit" class="btn  btn-save-address" style="letter-spacing: 2px;">SAVE ADDRESS</button>
										<button type="button" class="btn btn-md btn-link btn-cancel-new-address" ng-click="addnewaddress = 0" ng-hide="userloggedin() == 0">Cancel</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-xs-12 col-sm-12 col-md-4 summary-grid">
			<label>Order Summary</label>
			<div class="summary">
				<ul class="item-list">
					<li ng-repeat="item in cart">
						<div class="row">
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-3 cart-thumb inline">
								<img ng-src="assets/images/products/thumbnail/{{item.images[0].image_full}}"></div>
							<div class="col-xs-8 col-sm-8 col-md-8 col-lg-9">
								<h1 class="product-name inline">
									{{item.product_title}}
									<span class="product-description">{{item.product_description}}</span>
								</h1>
							</div>
						</div>
					</li>
				</ul>
				<div class="delivery-text">
					<label>Free Delivery</label>
					<p>Delivered in 2-3 business days.</p>
				</div>

				<div class="product-price">
					<label>Total:</label>
					<span>&#x20B9;</span>
					{{total | number:0}}
				</div>
				<div>  <!-- Coupon -->
					<form ng-init="couponapplied=0" class="form-inline" name="apply" ng-submit="apply.$valid && getCouponDiscount();couponapplied=1" novalidate ng-hide="cdisc != 0">
					  <div class="input-group">
					    <input type="text" class="form-control" ng-model="coupon" placeholder="Coupon Code " required ng-focus="invalidcoupon=0">
					    <span class="input-group-btn">
					      <button type="submit" class="btn btn-primary" style="background: #47C9AF;border: 1px solid #47C9AF;" ng-disabled="couponapplied == 1">Apply</button>
					    </span>
					  </div>
					</form>
					<span ng-show="invalidcoupon == 1" style="color:darkorange">Invalid Coupon!</span>
					<span ng-show="cdisc != 0" style="font-weight:500; font-size: 18px; color:#d61b5e">{{cdisc}}% discount applied!</span>
				</div><!-- Coupon ends -->
			</div><!-- Summary ends -->
		<!--div class="summary-grid">
			<form>
				<div class="payment-choice">
					<h5>Choice of Payment</h5>
					<div class="radio btn btn-default" onclick="$('#optionsRadios1').prop('checked', true)">
						<label>
							<input type="radio" name="paymentmethod" id="optionsRadios1" value="online" checked ng-model="paymentmethod">Online</label>
					</div>
					<div class="radio btn btn-default" onclick="$('#optionsRadios2').prop('checked', true)">
						<label>
							<input type="radio" name="paymentmethod" id="optionsRadios2" value="cod" ng-model="paymentmethod">Cash on Delivery</label>
					</div>
				</div>
				<button type="button" class="btn btn-save-address ng-binding theme-logo-color" ng-click="confirmOrder()" ng-disabled="orderplaced == true || loading == 1">{{action}}</button>
			</form>
		</div-->
 		<div class="summary-grid paymentoptions">
			<h5 style="font-size:16px;color: #d61b5e;padding-bottom:4px">Payment Choices</h5>
			<hr>
			<ul class="nav nav-pills menu">
			   <li class="active">
			     <a data-toggle="tab" href="#cod">Pay on Delivery</a>
			   </li>
			   <li>
			     <a data-toggle="tab" href="#online">Online Payment</a>
			   </li>
			   <li>
			     <a data-toggle="tab" href="#emi">EMI Payment</a>
			   </li>
			   <li>
			     <a data-toggle="tab" href="#nocardemi" ng-click="initZest()">Cardless EMI</a>
			   </li>
			</ul>
			<div class="tab-content">
			   <div class="tab-pane fade in active" id="cod">
			       <p>You can Pay at the time of delivery. You can either pay by cash or using your credit/debit card. Our delivery exeucutives carry the swiping machine for hazle free payment experience.</p>
			       <button type="button" class="btn btn-save-address ng-binding theme-logo-color" ng-click="confirmOrder(0)" ng-disabled="orderplaced == true || loading == 1" style="letter-spacing: 2px;text-transform: uppercase;">{{action}}</button>
			   </div>
			   <div class="tab-pane fade in" id="online">
			       <p>You can pay online using online banking, card or major payment wallets.</p>
			       <button type="button" class="btn btn-save-address ng-binding theme-logo-color" ng-click="confirmOrder(1)" ng-disabled="orderplaced == true || loading == 1">{{action}}</button>
			   </div>
			   <div class="tab-pane fade in" id="emi">
			       <div class="form-group">
				       <select class="form-control" ng-init='bankcode="0"' ng-model="bankcode" ng-change="getEmiDetails(bankcode)";>
				       		<option style="display:none" value="0">Select Bank</option>
				       		<!--option value="{{b.code}}" ng-repeat="b in banks">{{b.bank}}</option-->
				       		<option value="HDFC">HDFC Bank</option>
				       		<option value="KKBK">Kotak Mahindra Bank</option>
				       		<option value="UTIB">Axis Bank</option>
				       		<option value="INDB">Indusind Bank</option>
				       		<option value="RATN">RBL Bank</option>
				       		<option value="ICIC">ICICI Bank</option>
				       		<option value="AMEX">American Express</option>
				       </select>
			   		</div>
					<table class="table table-hover" style="margin-top: 5px; border: 1px solid lightgrey; padding: 5px" ng-hide='bankcode=="0"'>
						<thead>
							<tr>
								<th>Tenure</th>
								<th>Interest</th>
								<th>Monthly Installment</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="p in emi">
								<td>{{p.month}}</td>
								<td>{{p.interest}}</td>
								<td>{{p.installment}}</td>
							</tr>
						</tbody>
					</table>


			       <button type="button" class="btn btn-save-address ng-binding theme-logo-color" ng-click="confirmOrder(2)" ng-disabled="orderplaced == true || loading == 1">{{action}}</button>
			   </div>
			   <div class="tab-pane fade in" id="nocardemi">
			       <div ng-show="total > 3000 && total < 500000">
			       <p>Refabd brings a convenient way to help you pay in easy monthly installment without the need of a credit card, with our loan partner Zest.</p>
				       <label for="">Document Required</label>
				       <ul style="margin-left: 23px;">
				       	<li><p>Proof Of Identity</p></li>
				       	<li><p>Proof Of Address</p></li>
				       	<li><p>Income Proof</p></li>
				       	<li><p>Repayment Setup - Signed scanned copy of NACH(provided by Zest)</p></li>
				       </ul>
				       <!--div id="zest-emi-widget" class="emi-cnt" style="display:block;"></div-->
				       <div class="zest-payment-widget">
					       	<p class="zest-p">
						       	<div class="zest-emi-table">
						       		<table class="zest-emi" cellspacing="0">
						       			<tbody>
						       			</tbody>
						       		</table>
						       	</div>
					       	</p>
				       </div>
				     	<img style="width: 100%;" src="assets/zestphpsdk/web/images/mobile-2.png" alt="Cardless EMI">
			       </div>
			       <p style="color: red;">You will be redirected to zest payment page when you click on 'Confirm Order'</p>
			       <div ng-hide="total > 3000 && total < 500000">
			       		Not eligible for EMI as total amount is not in the EMI limit!
			       </div>
			       <button type="button" class="btn btn-save-address ng-binding theme-logo-color" ng-click="confirmOrder(3)" ng-disabled="orderplaced == true || loading == 1">{{action}}</button>
			   </div>
			</div>
		</div>

	</div>
</div>

<a id="orderplacedtrigger" data-toggle="modal" href='' data-target="#orderplacedmodal" ng-hide="1">Success</a>
<div class="modal fade" id="orderplacedmodal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">{{order.title}}</h4>
			</div>
			<div class="modal-body">
				<h6>{{order.msg1}}</h6>
				<br/>
				<h6>{{order.msg2}}</h6>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<a id="processingpayment" data-toggle="modal" href='' data-target="#processingpaymentmodal" ng-hide="1">Payment</a>
<div class="modal fade" id="processingpaymentmodal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="color:#d61b5e">
				<h4 class="modal-title">Processing Payment..</h4>
			</div>
			<div class="modal-body">
				<h6>Please wait. Your payment is being processed.</h6>
			</div>
		</div>
	</div>
</div>
</div>
</div>
</div>
