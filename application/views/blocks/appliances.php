<style>
.collection-text {
	color: #2da3b5;
	padding: 15px;
    position: absolute;
    top: -4px;
    /* left: 25%; */
    left: 0;
    z-index: 1;
    margin-left: 10px;
}
</style>
<div class="home page-content">
		<div class="page-content-area">
			<div class="container">
			    <div class= "row"><br><br><br>
			    	<div class="col-lg-3 col-sm-6 col-xs-12">
			    		<a href="category/3/refrigerator">
							 <img src="assets/images/refbanner/collection/ref.jpg" alt="Fridge">
							 <div class="collection-text"><h5>Refrigerator</h5></div>
						</a>
						
					</div>
					<div class="col-lg-3 col-sm-6 col-xs-12">
						<a href="category/4/Washing_Machine">
							<img src="assets/images/refbanner/collection/Wash.jpg" alt="Washing_Machine">
							<div class="collection-text"><h5>Washing Machine</h5></div>
						</a>
			    	</div>
			    	<div class="col-lg-3 col-sm-6 col-xs-12">
						<a href="category/5/air_conditioner">
							<img src="assets/images/refbanner/collection/Ac.jpg" alt="Ac">
							<div class="collection-text"><h5>Air Conditioner</h5></div>
						</a>
			    	</div>
			    	<div class="col-lg-3 col-sm-6 col-xs-12">
						<a href="category/11/televison">
							<img src="assets/images/refbanner/collection/Tv1.jpg" alt="Tv">
							<div class="collection-text"><h5>Television</h5></div>
						</a><br><br><br><br>
			    	</div>
			    	<div class="col-lg-3 col-sm-6 col-xs-12">
						<a href="category/14/microwave oven">
							<img src="assets/images/refbanner/collection/oven.jpg" alt="Microwave Oven">
							<div class="collection-text"><h5>Microwave Oven</h5></div>
						</a><br><br><br><br>
			    	</div>

			    </div>
			</div>
		</div>
</div>