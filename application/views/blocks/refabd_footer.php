<style>
.services {
    padding-top: 0px!important;
    padding-bottom: 10px!important;
}
.footer {
	padding-top:0!important;
	padding-bottom:0!important;
}
.footer_top {
	font-family: 'Open Sans', sans-serif!important;
	background-color:#404043!important;
	border-top:1px solid #3b3b3d;
	border-bottom:1px solid #3b3b3d;
}
.footer_top span {
	color: #c6c6c7!important;
	font-size: 60px;
};
.footer_top span:hover {
	color: #fff;
}
.footer_top label {
	color: rgba(188,192,196,.77);
	font-size: 14px;
    font-weight: 500;
    margin-top: -4px;
}
.footer_top a {
	border-right: 1px solid #3b3b3d;
	padding-left:10px;
	padding-right:10px;

}
.footer_top a:hover span {
	color:#fff!important;
}

.footer_top .services .textonly {
	font-size: 10px;
	line-height: 18px;
	font-weight: 100;
	padding-top: 9px;
	color: rgba(188,192,196,.77)!important;
	margin-left: -14px;
}

.footer_middle {
    width: 100%;
    display: inline-block;
    float: left;
    margin-bottom: 0;
    background: #313035;
}
.footer_middle {
	padding-top:10px;
	font-size: 12px;
	font-weight: 200;
}
.footer_middle li {
	padding-bottom:7px;
}

.footer_middle ul >:first-child > a {
	font-size: 14px;
	color:#a5a5a5;
	padding-bottom:10px!important;
}

.footer_middle li a:hover {
	color: #fff;
}

.footer_last {
	background-color:#333338;
	border-top: 1px solid green;
	color:white;
	padding-bottom:10px;
}

.footer_last .menu ul>li {
	padding-bottom: 10px;
}

.footer_last .menu ul>li>a {
	color: #d4d4d4!important;
}

.footer_last .contact {
	color: #8594c8!important;
}

.footer_last .menu label{
	color: white;
    font-size: 14px;
    font-weight: 400;
    padding-bottom: 5px;
    border-bottom: 1px solid #69b4df;
    margin-bottom: 10px;
    text-transform: uppercase;
}
.footer_last .social {
	margin-top:10px;
	margin-left: -22px;
}
.footer_last .social span{
	font-size: 25px;
    padding: 5px;
    color:#a5a5a5;
}
.footer_last .social a:hover span{
	color: #c7d12c;
}
</style>
<div ng-if='!isRouteLoading' class="footer">
	<div class="footer_top">
		<div class="container">
			<!-- Services -->
			<div class="services">
				<div class="row">
					<a href="#" class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
						<div class="row">
							<div class="text-center icontext col-lg-5 col-md-5 col-sm-12 col-xs-12">
								<span class="icon-del1" style="font-size: 72px!important;margin-top: 8px;m;margin-bottom: -1px;"></span>
								<label>DELIVERY</label>
							</div>
							<div>
							<span class="textonly col-lg-7 col-md-7 hidden-xs hidden-sm">
								Free delivery on all your orders. Sit back and relax until we set things up for You!
							</span>
							</div>
						</div>
					</a>
					<a href="#" class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
						<div class="row">
							<div class="text-center icontext col-lg-5 col-md-5 col-sm-12 col-xs-12">
								<span class="icon-Checklist" class="col-lg-7" style="font-size: 58px!important;margin-top: 16px;margin-bottom: 6px;"></span>
								<label>QUALITY</label>
							</div>
							<div >
								<span class="textonly col-lg-7 col-md-7 hidden-xs hidden-sm">
									Quality is guaranteed with our rigorous QC check and genuine QC Score!
								</span>
							</div>
						</div>
					</a>
					<a href="#" class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
						<div class="row">
							<div class="text-center icontext col-lg-5 col-md-5 col-sm-12 col-xs-12">
								<span class="icon-hand1" style="font-size: 60px!important;margin-top: 9px;m;margin-bottom: 9px;"></span>
								<label>WARRANTY</label>
							</div>
							<div>
								<span class="textonly col-lg-7 col-md-7 hidden-xs hidden-sm">
									We offer a minimum of six month warranty on all our products!
								</span>
							</div>
						</div>
					</a>
					<a href="#" class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
						<div class="row">
							<div class="text-center icontext col-lg-5 col-md-5 col-sm-12 col-xs-12">
								<span class="icon-Online-Deals" style="font-size: 58px!important;margin-top: 13px;m;margin-bottom: 8px;"></span>
								<label>EXCHANGE</label>
							</div>
							<div>
								<span class="textonly col-lg-7 col-md-7 hidden-xs hidden-sm">
									Don't like the appearance? return to us or exchange with any other product!
								</span>
							</div>
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>

	<div class="footer_middle">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<ul>
						<li>
							<a href="collection">Appliances</a>
						</li>
						<li>
							<a href="category/3/refriegerators">Refrigerator</a>
						</li>
						<li>
							<a href="category/3/washingmachines">Washing Machine</a>
						</li>
						<li>
							<a href="category/11/Television">Television</a>
						</li>
						<li>
							<a href="category/14/microwave oven">Microwave Oven</a>
						</li>
						<li>
							<a href="category/5/air_conditioner">Airconditioner</a>
						</li>
						
					</ul>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<ul>
						<li>
							<a href="collection">Furniture</a>
						</li>
						<li>
							<a href="category/10/bed">Single bed</a>
						</li>
						<li>
							<a href="category/10/bed">Double Bed</a>
						</li>
						<li>
							<a href="category/10/bed">Unused Bed</a>
						</li>
						<li>
							<a href="collection">Sofa set</a>
						</li>
						<li>
							<a href="collection">Recliner</a>
						</li>
						<li>
							<a href="collection">Dining Table</a>
						</li>

					</ul>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<ul>
						<li>
							<a href="category/12/laptop">Laptop</a>
						</li>
						<li>
							<a href="category/12/laptop">Lenovo Laptop</a>
						</li>
						<li>
							<a href="category/12/laptop">Dell Laptop</a>
						</li>
						<li>
							<a href="category/12/laptop">Core i5 Laptop</a>
						</li>

					</ul>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<ul>
						<li>
							<a href="collection">Unused Products</a>
						</li>
						<li>
							<a href="category/3/refriegerators">Refrigerator</a>
						</li>
						<li>
							<a href="category/12/laptop">Laptop</a>
						</li>
						<li>
							<a href="category/4/washingmachines">Washing Machine</a>
						</li>


					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="footer_last">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" style="padding-top:10px!important">
					<a href="https://www.refabd.com"><img src="assets/images/logo.png" style="width:92px"></a>
					<br>
					<label for="" style="color:#474748;font-weight: 100; font-size:12px;margin-left: 2px;margin-top: 8px;">© 2017 Refabd</label>
				<ul class="social">
					<li>
						<a href="#">
							<span class="icon-phone2"></span>
						</a>
					</a>
					</li>
					<li>
						<a href="mailto:hello@refabd.com">
							<span class="icon-mail5"></span>
						</a>
					</a>
					</li>
					<li>
						<a href="https://www.facebook.com/refabdINDIA/?ref=br_rs">
							<span class="icon-facebook"></span>
						</a>
					</a>
					</li>
				<li>
					<a href="https://twitter.com/refabdINDIA">
						<span class="icon-twitter"></span>
					</a>
				</a>
			</li>
			<li>
				<a href="#">
					<span class="icon-linkedin2"></span>
				</a>
			</a>
		</li>
	</ul>
</div>
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 menu" style="padding-top:10px!important">
	<label>The Company</label>
	<br>
	<ul>
		<li>
			<a href="aboutus">About Us</a>
		</li>
		<li>
			<a href="contactus">Contact Us</a>
		</li>
		<li>
			<a href="terms">Terms of use</a>
		</li>
		<li>
			<a href="privacy">Privacy</a>
		</li>
		<li">
			<a href="mailto:career@refabd.com?subject=Join Refabd" style="color: #66bfff!important">We are Hiring</a>
		</li>
	</ul>
</div>
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 menu" style="padding-top:10px!important">
	<label>Need Help</label>
	<br>
	<ul>
		<li>
			<a href="myorders">Track Your Order</a>
		</li>
		<li>
			<a href="myaccount">Raise Service Request</a>
		</li>
		<li>
			<a href="myorders">How we work</a>
		</li>
	</ul>
</div>

<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 menu" style="padding-top:10px!important">
	<label>Contact Us</label>
	<br>
	<ul>
		<li>
			<a href="tel:+917022630100">Sale:
				<span class="glyphicon glyphicon-phone-alt"></span>
				+91-70226-30-100
			</a>
		</li>
		<li>
			<a href="tel:+917022630100">Service:
				<span class="glyphicon glyphicon-phone-alt"></span>
				+91-70226-30-270<br> <span style="padding-left: 75px; padding-top = 2px; line-height: 20px;font-size:10px">(10AM to 4PM All days)</span>
			</a>
		</li>
		<li>
			<a href="mailto:hello@refabd.com">Sell Product:
				<span class="glyphicon glyphicon-envelope"></span>
				care@refabd.com
			</a>
		</li>
		<li>
			<a href="mailto:hello@refabd.com">Feedback:
				<span class="glyphicon glyphicon-envelope"></span>
				hello@refabd.com
			</a>
		</li>
	</ul>
</div>
</div>
</div>
</div>
</div>

