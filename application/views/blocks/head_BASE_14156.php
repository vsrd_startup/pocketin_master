<head>
	<-base href="/">
	<!--base href="/pocketin_master/pub/"-->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta charset="utf-8" />
	<title>Quality Tested Used Products | Refabd</title>
	<meta name="description" content="Shop online for quality tested used home appliances and furniture with free shipping, cash on delivery, 6 months warranty and post care sales service only at Refabd.">
	<meta name="keywords" content="Bangalore used products, Bangalore second hand products, used home appliances in bangalore, used furniture in bangalore, second hand furniture in bangalore, used refrigerator in bangalore, second hand refrigerator in bangalore, used washing machine in bangalore, second hand washing machine in Bangalore, second hand air conditioner in bangalore, used air conditioner in bangalore, second hand water purifier in bangalore, used water purifier in bangalore, used products with warrenty, used products with free service, used products with free delivery in bangalore, sell furniture, sell refrigerator, sell washing machine, sell appliances, exchange products, upgrade products, sell"/>

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<link rel="canonical" href="http://www.refabd.com" />
	<meta property="og:title" content="Quality tested used products for sale in bangalore"/>
	<meta property="og:description" content="Shop online for quality tested used home appliances and furniture with free shipping, cash on delivery, 6 months warranty and post care sales service only at Refabd."/>
	<meta property="og:type" content="website"/>
	<meta property="og:url" content="http://www.refabd.com"/>
	<meta property="og:site_name" content="refabd.com"/>

	<link rel="stylesheet" href="assets/css/bootstrap.min.css" />
	<link rel="stylesheet" href="assets/css/custom.min.css" />
	<link rel="stylesheet" href="assets/js/ngtoast/ngToast.min.css">

	<script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/bower_components/angular/angular.min.js"></script>
    <script src="assets/bower_components/angular-route/angular-route.min.js"></script>
    <script src="assets/bower_components/angular-cookies/angular-cookies.min.js"></script>
    <script src="assets/js/ngtoast/angular-sanitize.min.js"></script>
    <script src="assets/js/ngtoast/ngToast.min.js"></script>
    <script src="assets/js/angular/refabd_controllers.min.js"></script>
    <script src="assets/js/angular/pocketin_main_app.js"></script>
    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
    <script src="assets/js/imagezoom.min.js"></script>
    <script  src="assets/js/custom.min.js"></script>
    <script src="assets/js/dealer/ng-flow-standalone.min.js"></script>
</head>
