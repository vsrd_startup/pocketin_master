<style>
@media (max-width: 768px) {

  [class*="col-"] {
      margin-bottom: 20px;
  }
}
#icons_and_banner {
	font-family: "Open Sans", serif;
	margin-top:60px;
}

#icons_and_banner .homeicons {
	background-color: #f9f9f9;
	color: #3b4644;
	border: 1px solid #ddd;
	padding:10px;
	border-radius: 4px;
}
#icons_and_banner .homeicons h4 {
	color: #303030;
	text-align: center;
	padding-bottom:5px;
	font-size: 21px;
	font-weight: 400;
}
#icons_and_banner .homeicons h6 {
	color: #202020;
	font-size:12px;
	font-weight: 200;
	text-align: center;
}

#icons_and_banner .homeicons h6 span {
	border-bottom: 1px solid darkorange;
	padding-bottom: 10px;
}

#icons_and_banner .row {
	margin-bottom: 25px;
}


#icons_and_banner .oneicondiv {
	text-align: centre;
	color: #000;
	display: block;
	margin-left:25%;
	margin-right: 25%;
}

.iconimage {
	max-width: 50px;
    width: 50px;
}
#icons_and_banner a:hover p{
	color: #8d4397;
}
#icons_and_banner .oneicondiv span {
	margin-top:10px;
	font-size: 37px;
	font-weight: 500;
}

#icons_and_banner .oneicondiv p {
   	font-size: 12px;
   	font-weight: 400;
   	color: #000;
   	padding-top:5px;
    line-height: 120%;
}
.allcat {
	color: #010101;
    font-size: 14px;
    text-decoration: underline;
    margin-right: 15px;
    margin-bottom: -25px;
}
.allcat:hover {
	color: #7da32f;
	text-decoration: underline;
}

#main_slider .slider_bottom {
    background: #fff;
    opacity: 0.7;
    margin-bottom: 0px;
    bottom: 0px;
    position: absolute;
    width: 100%;
    z-index: 10;
}
#main_slider .slider_bottom li {
	text-align: center;
	padding-top: 13px;
    padding-bottom: 13px;
    font-size: 16px;
    font-weight: 500;
    color: #000;
    cursor: pointer;
    border-right: 2px solid #ded9d5;
}

@media all and (max-width: 500px) { /* screen size until 500px */
    #main_slider .slider_bottom li {
        font-size: 0.8em; /* 0.8x default size */
        }
    }
#main_slider .carousel-control {
    position: absolute;
    top: 50%!important;
    bottom: 50%!important;
    font-size: 20px;
    color: #fff;
    text-align: center;
    text-shadow: 0 1px 2px rgba(0,0,0,.6);
    background-color: rgba(0,0,0,0);
    filter: alpha(opacity=50);
    opacity: .5;
    width:5%!important;
}
#new_tv_launch {
	padding-bottom: 5px;
}
</style>
<script>
$(document).ready(function(){
	 $('.bannerslick').slick({
	 	// key : value
	 	dots: true,
        infinite: true,
        fade:true,
        speed: 500,
        arrows:true,
        autoplay: true,
  		autoplaySpeed: 3500,
	lazyLoad: 'ondemand',
	 });

});
</script>
<div class="bannerdiv">
	<section class="bannerslick">
    	<!--a href="collection">
	    	<div>
	      		<img src="assets/images/refbanner/ganesha.jpg">
	      	</div>
      	</a-->
    	<a href="category/12/laptops">
	    	<div>
	      		<img src="assets/images/refbanner/lap.jpg">
	      	</div>
      	</a>
      	<a href="category/7/sofa">
	    	<div>
	      		<img src="assets/images/refbanner/22.jpg">
	      	</div>
      	</a>
      	<a href="appliances">
	    	<div>
	      		<img src="assets/images/refbanner/33.jpg">
	      	</div>
      	</a>
	</section>
</div>

<div class="container" id="icons_and_banner">
	<div class="homeicons text-center">
		<h4>Setup Your Home with Refabd</h4>
		<h6 style="margin-bottom:30px">
			With
			<span>just 40% of your</span>
			budget!
		</h6>
		<div class="row">
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-4">
				<a href="category/3/refrigerators" class="oneicondiv">
					<img src="assets/images/reficons/refrigerator.svg" alt="Refrigerator" class="iconimage">
					<p>Refrigerator</p>
				</a>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-4">
				<a href="category/4/washing-machine" class="oneicondiv">
					<img src="assets/images/reficons/washing.svg" alt="Refrigerator" class="iconimage">
					<p>Washing Machine</p>
				</a>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-4">
				<a href="category/5/air_conditioner" class="oneicondiv">
					<img src="assets/images/reficons/ac.svg" alt="Refrigerator" class="iconimage">
					<p>Split AC</p>
				</a>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-4">
				<a href="category/12/laptop" class="oneicondiv">
					<img src="assets/images/reficons/laptop.svg" alt="Refrigerator" class="iconimage">
					<p>Laptop</p>
				</a>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-4">
				<a href="category/11/Television" class="oneicondiv">
					<img src="assets/images/reficons/tv.svg" alt="Refrigerator" class="iconimage">
					<p>Television</p>
				</a>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-4">
				<a href="category/10/bed" class="oneicondiv">
					<img src="assets/images/reficons/bed.svg" alt="Refrigerator" class="iconimage">
					<p>Bed</p>
				</a>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-4">
				<a href="category/17/dining" class="oneicondiv">
					<img src="assets/images/reficons/dining.svg" alt="Refrigerator" class="iconimage">
					<p>Dining Set</p>
				</a>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-4">
				<a href="category/8/office-chair" class="oneicondiv">
					<img src="assets/images/reficons/chair.svg" alt="Refrigerator" class="iconimage">
					<p>Office</p>
				</a>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-4">
				<a href="category/16/miscellaneous" class="oneicondiv">
					<img src="assets/images/reficons/bookshelf.svg" alt="Refrigerator" class="iconimage">
					<p>Book Shelf</p>
				</a>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-4">
				<a href="category/7/sofa" class="oneicondiv">
					<img src="assets/images/reficons/sofa.svg" alt="Refrigerator" class="iconimage">
					<p>Sofa Set</p>
				</a>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-4">
				<a href="category/15/tv unit" class="oneicondiv">
					<img src="assets/images/reficons/tvunit.svg" alt="Refrigerator" class="iconimage">
					<p>TV Unit</p>
				</a>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-3 col-xs-4">
				<a href="category/13/shoe rack" class="oneicondiv">
					<img src="assets/images/reficons/shoerack.svg" alt="Refrigerator" class="iconimage">
					<p>Shoe Rack</p>
				</a>
			</div>
		</div>
		<div class="row">
			<a href="collection" class="allcat pull-right">Explore Full Collection</a>
		</div>
	</div>
</div>
<!-- <div class="row" id="new_tv_launch">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<a href="category/11/Television">
	<img src="assets/images/refbanner/tv_launch_new.jpg" class="img-responsive" alt="Appliances at 50% Off"></a>
</div>
</div> -->

<!--div class="row" id="icons_and_banner">
	<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
	<div id="main_slider" class="carousel slide" data-ride="carousel">
		<div class="slider_bottom hidden-sm hidden-xs">
			<ul>
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-3 active" data-target="#main_slider" data-slide-to="0">Beat the Summer!</li>
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-3" data-target="#main_slider" data-slide-to="1">Unboxed Furniture</li>
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-3" data-target="#main_slider" data-slide-to="2">Laptops from 9,999</li>
				<li class="col-lg-3 col-md-3 col-sm-3 col-xs-3" data-target="#main_slider" data-slide-to="3">Sell and Upgrade</li>
			</ul>
		</div>

		<div class="carousel-inner" role="listbox">
			<div class="item active">
				<a href="appliances">
				<img src="assets/images/refbanner/app1.jpg" alt="Appliances at 50% Off"></a></div>
			<div class="item">
				<a href="furniture">
				<img src="assets/images/refbanner/furniture_banner1.jpg" alt="Unboxed Furniture at affrodable price"></a></div>
			<div class="item">
				<a href="category/12/laptop">
				<img src="assets/images/refbanner/laptop_banner1.jpg" alt="Laptops Starting at 9,999"></a></div>
			<div class="item">
				<a href="sell">
				<img src="assets/images/refbanner/exchange1.jpg" alt="Sell and Upgrade"></a></div>
		</div>
		<a class="left carousel-control" data-target="#main_slider" role="button" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="right carousel-control" data-target="#main_slider" role="button" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>
</div>
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
	<div class="homeicons text-center">
		<h4>Setup Your Home with Refabd</h4>
		<h6 style="margin-bottom:30px">
			With
			<span>just 40% of your</span>
			budget!
		</h6>
		<div class="row">
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
				<a href="category/3/refrigerators" class="oneicondiv">
					<span class="icon-09"></span>
					<p>Refrige..</p>
				</a>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
				<a href="category/4/washing-machine" class="oneicondiv">
					<span class="icon-05"></span>
					<p>Washing..</p>
				</a>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
				<a href="category/5/air_conditioner" class="oneicondiv">
					<span class="icon-38"></span>
					<p>Split AC</p>
				</a>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
				<a href="category/12/laptop" class="oneicondiv">
					<span class="icon-laptop_mac"></span>
					<p>Laptop</p>
				</a>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
				<a href="category/11/Television" class="oneicondiv">
					<span class="icon-display"></span>
					<p>Television</p>
				</a>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
				<a href="category/10/bed" class="oneicondiv">
					<span class="icon-64"></span>
					<p>Bed</p>
				</a>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
				<a href="category/17/dining" class="oneicondiv">
					<span class="icon-27"></span>
					<p>Dining</p>
				</a>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
				<a href="category/8/office-chair" class="oneicondiv">
					<span class="icon-04"></span>
					<p>Office</p>
				</a>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
				<a href="category/16/miscellaneous" class="oneicondiv">
					<span class="icon-29"></span>
					<p>Book Shelf</p>
				</a>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
				<a href="category/7/sofa" class="oneicondiv">
					<span class="icon-69"></span>
					<p>Sofa Set</p>
				</a>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
				<a href="category/15/tv unit" class="oneicondiv">
					<span class="icon-92"></span>
					<p>TV Unit</p>
				</a>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
				<a href="category/13/shoe rack" class="oneicondiv">
					<span class="icon-48"></span>
					<p>Shoe Rack</p>
				</a>
			</div>
		</div>
		<div class="row">
			<a href="collection" class="allcat pull-right">Explore Full Collection</a>
		</div>
	</div>
</div>
</div!-->

<!--div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
<ol class="carousel-indicators">
<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
</ol>
<div class="carousel-inner" role="listbox">
<div class="carousel-item active">
	<img class="d-block img-fluid" src="https://zefo-website.s3.amazonaws.com/img/website/ul_dining1.jpg" alt="First slide"></div>
<div class="carousel-item">
	<img class="d-block img-fluid" src="https://img3.gozefo.com/website/ofb_d1.jpg" alt="Second slide"></div>
<div class="carousel-item">
	<img class="d-block img-fluid" src="https://img1.gozefo.com/website/hiws_d.jpg" alt="Third slide"></div>
</div>
<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
<span class="carousel-control-prev-icon" aria-hidden="true"></span>
<span class="sr-only">Previous</span>
</a>
<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
<span class="carousel-control-next-icon" aria-hidden="true"></span>
<span class="sr-only">Next</span>
</a>
</div-->

<!--a href="collection" class="href">
<div class="banner">
<ul>
<li>
	<div class="banner-text hide1">
		<h2>
			<span>Quality</span>
			tested
			<span>used</span>
			products
			<br/>
			at best price
		</h2>
		<div class="services text-center hidden-xs">
			<div class="row">
				<div class="col-xs-6 col-sm-3">
					<span class="icon pocketin-icon-Box-1 huge"></span>
					<label>Free delivery</label>
					<p>
						Delivery absolutely free for customers. Just pay for the product not a penny extra
					</p>
				</div>
				<div class="col-xs-12 col-sm-3">
					<span class="icon pocketin-icon-Riddon huge"></span>
					<label>6 Months Warranty &amp; 7 days replacement</label>
					<p>
						A written guarantee and a promise from us for all the products
					</p>
				</div>
				<div class="col-xs-12 col-sm-3">
					<span class="icon pocketin-icon-Thumps-up huge"></span>
					<label>Quality check</label>
					<p>
						All products undergo thorough inspection and gets genuine quality rating
					</p>
				</div>
				<div class="col-xs-12 col-sm-3">
					<span class="icon pocketin-icon-Settings-2 huge"></span>
					<label>Post sale care/service</label>
					<p>
						You have any query? We are available 24/7 for faster resolution
					</p>
				</div>
			</div>
		</div>
	</div>
</ul>
</div>
</a-->
