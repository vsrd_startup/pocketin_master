<style>
	.refabd {
	background: #f1990f;
    font-size: 16px;
    font-weight: 200;
    color: white;
    padding: 5px;
	}
</style>
<header>
	<div class="top-bar h">
	<!--div class="container-fluid refabd" ng-show="homepage">
		Year End Clearance Sale <span style="font-weight: 400">Discount upto 40%</span>
	</div-->
		<div class="container">

			<div class="nav navbar-nav navbar-left edit-location" ng-hide="ismobile">
				<!--label>Change location:</label-->
				<label>Open at </label>
				<!-- user dropdown -->
				<div class="dropdown inline">
				  <a class="user-log-name dropdown-toggle" type="button" data-toggle="dropdown"  href="">Bangalore</a>
				  <!--ul class="dropdown-menu">
				  <li><a href="#">Bangalore</a></li>
				  <li><a href="#">Chennai</a></li>
				  </ul-->
				</div>
			</div>
			<ul class="nav navbar-nav navbar-right">
				<!--<li><a href="#/contactus">Customer care:  <span class="glyphicon glyphicon-phone-alt"></span> <b>70 22 63 0100</b></a></li>->
				<li><a href="#/contactus"><span class="glyphicon glyphicon-phone-alt"></span> <b>080 4214 1359</b></a></li-->

				<!--li><a href="">Download App</a></li-->
				<li><a href="" ng-click="hidewidget = 0">Offers</a></li>
				<li><a href="mywishlist">Wishlist</a></li>
				<li><a href="myorders">Track Order</a></li>
				<li><a class="sell-product" href="sell">Sell Product</a></li>
			</ul>
		</div>
	</div>
	<section class="header-menu">
		<div class="container">
			<div class="row">
				<div class="col-xs-6 col-sm-1 col-md-1 col-lg-1 no-pad" onclick="$('#search_box').val('');">
				<!-- <a class="navbar-brand" href="#/" scroll-on-click>Pocket<strong>in</strong>
                <span cla
                ss="inline beta">Beta</span>
                </a> -->
					<a class="navbar-brand" href="." style="font-size:26px!important;padding-top: 17px!important" scroll-on-click>
						<!--img src="assets/images/logo1.png"-->
	                    <strong>Refabd</strong>
	                </a>
                </div>
		        <div class="col-xs-12 col-sm-offset-1 col-sm-7 col-md-7 col-lg-7" ng-controller="SearchController as ctrl" ng-hide="ismobile">
				    <form class="input-group" ng-submit="displaySearchResult()">
		                <div class="input-group-btn search-panel">
		                    <button type="button" class="btn btn-category  dropdown-toggle" data-toggle="dropdown">
		                    	<span id="search_concept">{{search_focus}}</span> <span class="caret"></span>
		                    </button>
		                    <ul class="dropdown-menu" role="menu">
		                      <li ng-repeat="cat in cats">
		                      	<a href="" ng-click="setSearchFocus(cat)">{{cat.name}}</a>
		                      </li>
		                      <li class="divider"></li>
		                      <li><a href="" ng-click="setSearchFocus({name: 'All Items', id:'0'})">All Items</a></li>
		                    </ul>
		                </div>
		                <input type="text" class="form-control" name="x" id="search_box" placeholder="Search an Item" ng-model="query">
		                <span class="input-group-btn">
		                    <a class="btn bt-search" ng-href="search/{{selected_cat_id}}/{{query}}">
		                    	<span class="glyphicon glyphicon-search"></span>
		                    </a>
		                </span>
		                <button type="submit" class="hide"></button>
		            </form>
		        </div>
		        <div class="col-xs-7 col-sm-3 col-md-3 col-lg-3 mob-nav">
					<ul class="nav nav-main navbar-nav navbar-right">
						<li class="notification" ng-controller="NotificationController as ctrl">
							<div class="dropdown">
							  <a class="dropdown-toggle" type="button" data-toggle="dropdown" ng-click="ctrl.seenNotification()">
							  	<span class="circle" ng-class="notclass" ng-show="ctrl.getCount() > 0">{{ctrl.getCount()}}</span>
							  	<span class="pocketin-icon-Notification"></span>
							  </a>
							  <div class="dropdown-menu">
								  <ul class="notification-list">
								    <li ng-repeat="not in ctrl.getNots()">
										 <a href="notifications">
											 <div class="notification-read">
											 	<div class="row">
													 <div class="col-xs-3">
													 	<div class="notification-icon">
													 		<span class="pocketin-icon-Info"></span>
													 	</div>
													 </div>
													 <div class="col-xs-9 notification-text">
														 <div class="">
															<p>Your Item, <b>{{not.product}}</b> from Order: <b>PI:{{not.order}}</b> has been {{not.type}}. </p>
														 </div>
														 <div class="notification-timestamp">
														 	{{not.date}}
														 </div>
													 </div>
												 </div>
											 </div>
										 </a>
									</li>
									<li ng-show="ctrl.getCount() == 0">
										<div class="notification-read">
										 	<div class="row">
										 		<div class="col-xs-9 notification-text">
										 			<p style="padding-top: 10px; padding-bottom:10px; padding-left: 50%">No Notifications</p>
										 		</div>
										 	</div>
										 </div>
									</li>
									<hr>
									<li>
										<div class="notification-read">
										<div class="notification-text">
										<a href="notifications" style="padding-left: 40%; padding-top: 10px; padding-bottom: 10px;">
										<p>View All</p></a>
										</div>
										</div>
									</li>
							  </ul>
							</div>
							</div>
						</li>
						<!-- https://www.pocketin.in/#/cart-->
						<li ng-controller="CartCountController as ctrl" class="cart"><a href="https://www.refabd.com/cart">
							<span class="circle" ng-show="ctrl.getCartCount() > 0">{{ctrl.getCartCount()}}</span>
							<span class="pocketin-icon-Cart-1"></span>&nbsp;&nbsp;&nbsp;Cart</a>
						</li>


					<li class="login" ng-controller="LoginSignupController as ctrl" ng-init="inited = 0">
						<!-- login dropdown -->
						<div class="dropdown" ng-hide="ctrl.userLogged()">
						  <a class="dropdown-toggle" type="button" data-toggle="dropdown"  href="">Login</a>
						  <section class="dropdown-menu">
						    <div class="login-from">
						      <label>Login</label>
						      <form method="post" name="loginform" ng-submit="loginform.$valid && ctrl.login()" novalidate>
							      <div class="row">
							      	<div class="col-xs-12">
							        	<input class="form-control" type="text" name="login" value="" placeholder="Phone Number or Email" ng-model="user.username" ng-disabled="!canedit" required>
							        </div>
							      	<div class="col-xs-12">
							        	<input class="form-control" type="password" name="password" value="" placeholder="Password" ng-model="user.password" required ng-disabled="!canedit">
							        </div>
							      	<div class="col-xs-4 submit">
							       		<input class="btn btn-login" type="submit" name="commit" value="Login" ng-disabled="!canedit">
							        </div>
							        <div class="col-xs-8 login-help">
							            <a href="" data-toggle="modal" data-target="#forgotpwdmodal" data-backdrop="static">Forgot password?</a>
							        </div>
							        <div class="col-xs-12">
							        <p class="validationerror">{{message}}</p>
							        </div>
							    </div>
							    <a href="" style="padding: 10px;" ng-click="getotp(user.username)" ng-show="requestotp">Resend OTP?</a>
						      </form>
						      <div ng-show="requestotp">
								  	  <form ng-submit="verifyotpandlogin()">
									  	  <div class="row" ng-show="showotp">
											  <div class="form-group col-xs-8" style="padding: 0 7px 0 15px;">
											  	<input type="number" class="form-control" placeholder="Enter OTP"  ng-model="otp">
											   </div>
											   <div class="form-group col-xs-4">
											    <button type="submit" class="btn btn-default">Verify</button>
											  </div>
										  </div>
									  </form>
								</div>
						    </div>
							<hr/>
						    <div class="col-xs-12 newuserdiv">
						    	<a class="btn btn-sign-up" data-toggle="modal" data-target="#signupform" href="" data-backdrop="static" data-keyboard="false"><span>New User? Sign Up here.</span></a>
						    </div>
						    <div class="col-xs-12 text-center sign-up-offers" style="padding: 10px;">
								<label>Register now and get upto 8% OFF on your purchase!!</label>
							</div>

						  </section>
						</div>
						<!-- user dropdown -->
						<div class="dropdown" ng-show="ctrl.userLogged()">
						<!--p style="font-size: 10px; position:absolute; width: 80px;">Click to Manage</p-->
						  <a class="user-log-name dropdown-toggle" type="button" data-toggle="dropdown"  href="" id="useraction">
						  	{{ctrl.getLoggedUser()}} <span class="glyphicon glyphicon-triangle-bottom"></span></a>

						  <ul class="dropdown-menu" id="useractiondiv">
						  <li><a href="myaccount">My Account</a></li>
						  <li><a href="myorders">My Orders</a></li>
						  <li><a href="mysellrequests">Sell Requests</a></li>
						  <li><a href="." ng-click="ctrl.logout()">Logout</a></li>
						  </ul>
						</div>
					</li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<div class="category-list-subhead" style="display:none">
		<div class="container">

			<label class="inline">Go quickly to <span class="glyphicon glyphicon-chevron-right"></span></label>
			<ul class="inline category-lists">
				<li>
					<a class="btn btn-link tag-new" href="category/3/refrigerators">Refrigerators</a>
				</li>
				<li>
					<div class="sub-head-tag tag-new"><span></span>New</div>
					<a class="btn btn-link" href="category/4/washing-machines">Washing Machines</a>
				</li>
				<li ng-hide="ismobile">
					<!-- <div class="sub-head-tag tag-soon"><span></span>Coming soon</div> -->
					<div class="sub-head-tag tag-soon"><span></span>Coming soon</div>
					<a class="btn btn-link disabled" href="category/10/bed">Home Furniture</a>
				</li>
			</ul>
		</div>
	</div>
	<toast></toast>
</header>

<!--div ng-init="hidewidget = 1" class="offers-widget" ng-hide="hidewidget == 1">
	<button type="button" class="close" aria-label="Close" ng-click="hidewidget = 1"><span aria-hidden="true">&times;</span></button>
	<h4>Offers!</h4>
	<p><span>Flat <span>&#x20B9;</span>500 discount</span><br/>on all products</p>
	<hr/>
	<p><span>Get Extra 5% off</span><br/>on every purchase above 8k.</p>
	<hr/>
	<p><span>Get Extra 8% off</span><br/>on every purchase above 10k.</p>
</div-->

<div ng-init="hidewidget = 1" class="offer" ng-hide="hidewidget == 1">
	<button type="button" class="close" data-dismiss="modal" ng-click="hidewidget = 1">×</button>
		<h3 class="offer-header">Offers</h3>
		<p class="offer-line">Year end Sale - Heavy discount on all products</p><br>
		<p class="offer-line">Refer and get <b><span>&#x20B9;</span>100</b> on each <a style="color:#0d1f38; text-decoration: underline" href="myreferral">referral</a></p><br>
		<p class="offer-line">Get <b>5% </b>off on entire cart value on purchase of more than one product</p>
		<!--p class="offer-line">Extra 5% offer on purchase above <span>&#x20B9;</span>8,000</p-->
		<!--p class="offer-line">Extra 5% offer on purchase above <span>&#x20B9;</span>10,000</p-->
		<p class="offer-condition"><span>*</span>Limited period offer</p>
</div>
<!-- Modal -->
<div id="signupform" class="modal fade" role="dialog" ng-controller="LoginSignupController as ctrl" ng-init="showlogin=0">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" id="signupmodal">
	    <div class="text-left sign-up-offers" style="padding: 10px; padding-top: 0;">
	    <button ng-show="1" id="regmodalclose" type="button" class="close" data-dismiss="modal">&times;</button>
			<label>Register now and refer your friends to avail discount of <span style="font-weight: 500"><span>&#x20B9;</span>100</span> on each referral</label>
		</div>
      <div class="modal-header">
        <!-- <h5 class="modal-title" style="color: darkorange">Register now and get 10% OFF on your purchases!!</h5> -->
        <p ng-show="showlogin == 0">Already Registered? - <a href="" style="color:#d61b5e; text-decoration: underline; font-weight: 500" ng-click="showlogin=1">Login</a></p>
        <p ng-show="showlogin == 1">New User? - <a href="" style="color:#d61b5e; text-decoration: underline; font-weight: 500" ng-click="showlogin=0">Signup</a></p>
      </div>
          <div class="modal-body">
			  <section ng-show="showlogin == 1">
			    <div class="login-from">
			      <form method="post" name="loginform" ng-submit="loginform.$valid && ctrl.login()" novalidate>
				      <div class="row">
				      	<div class="col-xs-12">
				        	<input class="form-control" type="text" name="login" value="" placeholder="Phone Number or Email" ng-model="user.username" ng-disabled="!canedit" required>
				        </div>
				      	<div class="col-xs-12">
				        	<input class="form-control" type="password" name="password" value="" placeholder="Password" ng-model="user.password" required ng-disabled="!canedit">
				        </div>
				      	<div class="col-xs-4 submit">
				       		<input class="btn btn-login" type="submit" name="commit" value="Login" ng-disabled="!canedit">
				        </div>
				        <div class="col-xs-8 login-help">
				            <a href="" data-toggle="modal" data-target="#forgotpwdmodal" data-backdrop="static">Forgot password?</a>
				        </div>
				        <div class="col-xs-12">
				        <p class="validationerror">{{message}}</p>
				        </div>
				    </div>
				    <a href="" style="padding: 10px;" ng-click="getotp(user.username)" ng-show="requestotp">Resend OTP?</a>
			      </form>
			      <div ng-show="requestotp">
					  	  <form ng-submit="verifyotpandlogin()">
						  	  <div class="row" ng-show="showotp">
								  <div class="form-group col-xs-8" style="padding: 0 7px 0 15px;">
								  	<input type="number" class="form-control" placeholder="Enter OTP"  ng-model="otp">
								   </div>
								   <div class="form-group col-xs-4">
								    <button type="submit" class="btn btn-default">Verify</button>
								  </div>
							  </div>
						  </form>
					</div>
			    </div>
			  </section>

	      	<div class="row" ng-show="showlogin == 0">
	      	<form name="signupform" class="text-left" role="form" ng-submit="signupform.$valid && ctrl.signup()" novalidate>
			  <div class="form-group col-xs-6" style="padding: 0 7px 0 15px;">
			    <input type="text" name="fname" class="form-control" placeholder="First Name" ng-model="user.firstname" required ng-disabled="!canedit" ng-pattern="/^[a-zA-Z]*$/">
			    <p class="validationerror" ng-show="signupform.$submitted && signupform.fname.$invalid">Only Letters and no space</p>
			  </div>
			  <div class="form-group col-xs-6" style="padding: 0 15px 0 7px;">
			    <input name="lname" type="text" class="form-control" placeholder="Last Name"  ng-model="user.lastname" ng-disabled="!canedit" ng-pattern="/^[a-zA-Z]*$/">
			    <p class="validationerror" ng-show="signupform.$submitted && signupform.lname.$invalid">Only Letters and no space</p>
			  </div>
			  <div class="form-group col-xs-12">
			    <input name="email" type="email" placeholder="Email" class="form-control" ng-model="user.email" id="email" required ng-disabled="!canedit" ng-pattern="/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/">
			    <p class="validationerror" ng-show="signupform.$submitted && signupform.email.$invalid">Enter a valid email</p>
			  </div>
			  <div class="form-group col-xs-12">
			    <input type="number" name="phoneNumber" placeholder="Phone Number" class="form-control"  ng-minlength = "10" ng-maxlength = "10" ng-model="user.phonenumber" required ng-disabled="!canedit" ng-pattern="/^[1-9]+[0-9]*$/">
			    <p class="validationerror" ng-show="signupform.$submitted && signupform.phoneNumber.$invalid">Exactly 10 digits, first digit non zero</p>
			  </div>
			  <div class="form-group col-xs-12">
			    <input name="pwd" type="password" placeholder="Password" class="form-control" ng-model="user.password" required ng-disabled="!canedit">
			    <p class="validationerror" ng-show="signupform.$submitted && signupform.pwd.$invalid">Password Required</p>
			  </div>
			  <div class="form-group col-xs-12">
			  <p class="validationerror">{{message}}</p>
			  </div>
			  <button type="submit" class="btn btn-sign-up-form" ng-disabled="!canedit" ng-hide="1"></button>
			</form>
			<form name="signupotpform" ng-submit="signupotpform.$valid && ctrl.verifyotp(otp)" novalidate ng-show="otpshow">
			  <div class="form-group col-xs-6">
			  <input type="number" class="form-control" placeholder="Enter OTP"  ng-model="otp" required>
			  </div>
			  <button type="submit" class="btn btn-default">Verify</button>
			  </form>
			 </div>
	      </div>
			<div class="modal-footer" ng-show="showlogin == 0">
				<div class="row">
					<div class="col-xs-6 text-left">
						<button type="button" class="btn btn-sign-up-form" ng-disabled="!canedit" ng-click="signupform.$submitted = 1; signupform.$valid && ctrl.signup()">Sign Up</button>
					</div>
					<div class="col-xs-6 text-right">
						<button type="button" id="closesignup" class="btn btn-cancel" data-dismiss="modal" ng-hide="1">Cancel</button>
						<button type="reset" class="btn btn-cancel" ng-click="resetSignupModal()">Reset</button>
					</div>
				</div>
			</div>
    </div>

  </div>
</div>

<div id="forgotpwdmodal" class="modal fade" role="dialog" ng-controller="LoginSignupController as ctrl">
  <div class="modal-dialog" style="width: 380px;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" ng-click="clearForgotPwdForm()">&times;</button>
        <h5 class="modal-title">Forgot Password</h5>
      </div>
	      <div class="modal-body">
	    <form name="forgot1" class="text-left" role="form" novalidate ng-hide="done || showotp" ng-submit="forgot1.$valid && getotp(username)">
	      	<div class="row">
			  <div class="form-group col-xs-8" style="padding: 0 7px 0 15px;">
			    <input type="text" class="form-control" placeholder="Phone Number of Email" ng-model="username" required>
			  </div>
			  <div class="form-group col-xs-4">
			    <button type="submit" class="btn btn-default">Continue</button>
			  </div>
			</div>
		</form>
		<form name="forgot2" ng-show="showotp && !showpwd" ng-submit="forgot2.$valid && verifyotp()" novalidate>
		  <div class="row">
			  <div class="form-group col-xs-8" style="padding: 0 7px 0 15px;">
			  	<input type="number" class="form-control" placeholder="Enter OTP"  ng-model="otp" required>
			   </div>
			   <div class="form-group col-xs-4">
			    <button type="submit" class="btn btn-default">Verify</button>
			  </div>
		   </div>
		   </form>
		   <form ng-show="showpwd" name="forgot3" ng-submit="forgot3.$valid && changepwd()" novalidate>
			<div class="row" >
			  <div class="form-group col-xs-8" style="padding: 0 7px 0 15px;">
			    <input type="password" placeholder="Enter New Password" class="form-control"  ng-model="pwd" required>
			  </div>
			  <div class="form-group col-xs-4">
			    <button type="submit" class="btn btn-default">Submit</button>
			  </div>
			</div>
			</form>
			  <div class="row">
			  <div class="form-group col-xs-12">
			  <p class="validationerror">{{message}}</p> <br/>
			  <p class="validationerror">{{message1}}</p>
			  </div>
			  </div>
	      </div>

			<!--div class="modal-footer">
				<div class="row">
					<div class="col-xs-6 text-left">
						<button type="submit" class="btn btn-sign-up-form" ng-disabled="!canedit">Change Password</button>
					</div>
					<div class="col-xs-6 text-right">
						<button type="button" class="btn btn-cancel" data-dismiss="modal">Cancel</button>
					</div>
				</div>
			</div-->
    </div>

  </div>
</div>
