<?php
include ('ZestAbstract.php');
class ZestProcessor extends ZestAbstract {

	public function applyloan($orderdata){
		$header = array();
	    $tokenresponse = $this->gettoken();
	    $tokendata = json_decode($tokenresponse);
        $header[] = 'Authorization: '.$tokendata->token_type.' '.$tokendata->access_token; 
        $loanurl = $this->loanurl();
        $postdata = http_build_query($orderdata);
        if(DEBUG){
        	WriteLog('order data - '.print_r($orderdata,1));
        }
        $response = $this->request($loanurl, $postdata, $header);
        if(!empty($response->LogonUrl)){
        	if(DEBUG){
                WriteLog('url:  '. $response->LogonUrl);
            }
            return $response->LogonUrl;

        }else{
        	$ordererror = (!empty($orderdata['OrderId'])) ? $orderdata['OrderId'].' Error-' : 'No order id -';
        	WriteLog($ordererror.' '.print_r($response,1));
        	return false;
        }
	}

	public function sendCancellation($orderid,$reason='') {
		if(!empty($orderid)){
			$cancelurl = $this->cancelurl($orderid);
        	$header = $this->_getheader();  
	        $cancelreason = $this->_getCancellingreason();
	        $reason = (in_array($reason,$cancelreason)) ? $reason : 'Other';
	        $fields = array('Reason' => $reason);
        	$postdata = http_build_query($fields); 
        	$result = $this->request($cancelurl, $postdata, $header, 1);
        	if($result['http_code']==200){
                return true;
             }elseif($result['http_code']==404){
                WriteLog($orderid.' Error - order not found');
                return false;
             }else{
             	WriteLog($orderid.' Error -'.print_r($result,1));
                return false;
             }
		}else{
			return false;
		}
	}

    public function sendDeliverNotify($orderid,$status)
    {
        $deliverurl = $this->deliverurl($orderid);
        $header = $this->_getheader();
        $fields = array('DeliveryStatus'=> $status);
        $postdata = http_build_query($fields);
        $data = $this->request($deliverurl, $postdata, $header, 1, 'POST');
        return $data;
    }

	public function getorderstatus($orderid){
		if($orderid){
			$statusurl = $this->orderstatusurl($orderid);
        	$header = $this->_getheader();
        	$postdata='';
        	$result = $this->request($statusurl, $postdata, $header, 1, 'GET');
        	if($result['http_code']==200){
                return $result['response']->OrderStatus;
            }elseif($result['http_code']==404){
                WriteLog($orderid.' Error - order not found');
                return false;
            }else{
                WriteLog($orderid.' Error -'.print_r($result,1));
                return false;
            }
		}else{
			return false;
		}
	}

	public function sendRefund($orderid,$amount,$reason){
		if(!empty($orderid) && !empty($amount) && !empty($reason)){
			$refundurl = $this->refundurl();
        	$header = $this->_getheader();
        	$refundreason = $this->_getRefundReasonCode();
	        $reason = (in_array($reason,$refundreason)) ? $reason : 'Other';
        	$fields = array('OrderId'=> $orderid, 'RefundValue'=>$amount ,'ReasonCode' => $reason, 'RefundDate' => date('Y-m-d',time()));
        	$postdata = http_build_query($fields); 
        	$result = $this->request($refundurl, $postdata, $header, 1);
        	if($result['http_code']==201){
        		return true;
        	}elseif($result['http_code']==404){
                WriteLog($orderid.' refund Error - order not found');
                return false;
            }else{
        		WriteLog($orderid.' refund Error -'.print_r($result,1));
                return false;
        	}
    	}else{
    		WriteLog('Refund error - improper details');
    		return false;
    	}
	}

}