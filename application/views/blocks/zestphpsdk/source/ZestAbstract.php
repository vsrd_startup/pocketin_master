<?php
include ('config.php');
abstract class ZestAbstract {

	protected $apiurl;
	
	public function __construct(){
	    if(SANDBOX_MODE){
	    	$this->apiurl = SANDBOX_URL;
	    }else{
    		$this->apiurl = LIVE_URL;
    	}
   }

	public function tokenurl(){
	    $url = $this->apiurl."Authentication/connect/token";
		return $url;
	}

	public function loanurl(){
		$url = $this->apiurl."ApplicationFlow/LoanApplications";
		return $url;
	}

	public function refundurl(){
		$url = $this->apiurl."Loan/Refunds";
		return $url;
	}

	public function cancelurl($orderid){
		$url = $this->apiurl."ApplicationFlow/LoanApplications/orders/".$orderid."/cancellation";
		return $url;
	}

	public function deliverurl($orderid){
		$url = $this->apiurl."Loan/DeliveryReport/".$orderid;
		return $url;
	}

	public function orderstatusurl($orderid){
		$url = $this->apiurl."ApplicationFlow/LoanApplications/orders/".$orderid;
		return $url;
	}

	public function emiurl(){
		$url = $this->apiurl."Pricing/quote";
		return $url;
	}

	public function loanagreementurl(){
		$url = $this->apiurl."loanagreement";
		return $url;
	}

	public function getclientid(){
		return CLIENT_ID;
	}

	public function getzesttomerchant(){
		return ZEST_TO_MERCHANT_SECRET;
	}

	public function getminordertotal(){
		return MIN_ORDER_TOTAL;
	}

	public function getmaxordertotal(){
		return MAX_ORDER_TOTAL;
	}

	public function getfaqurl(){
		if(SANDBOX_MODE){
	    	$url = 'http://stagingsite.zestmoney.in/faq/';
	    }else{
    		$url = 'http://zestmoney.in/faq/';
    	}
    	return $url;
	}

	public function request($url, $postvars=null, $header=null, $getcode = null, $method = 'POST'){
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL,$url);
		if($method == 'POST') {
		curl_setopt($ch,CURLOPT_POST, 1);
		curl_setopt($ch,CURLOPT_POSTFIELDS,$postvars);
		}
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		if (count($header)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        }
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		if ($response === false)
		{
			WriteLog(curl_error($ch));
		}
		$info = curl_getinfo($ch);
		curl_close ($ch);
		if($getcode){
			$result = array('response' => json_decode($response), 'http_code' => $info['http_code']);
			return $result;
		}else{
		 	return json_decode($response);
		}
	}

	public function gettoken($type = 'sensitive'){
		$gettoken = ($type == 'sensitive') ? $this->getsecrettoken() : $this->getpublictoken();
		$gettokendata = unserialize($gettoken);
		if(time() > $gettokendata['expirytime'] || empty($gettoken) || empty($gettokendata['token'])){
			$tokendata =  $this->getaccesstoken($type);
			$expirytime = time()+$tokendata->expires_in-60;
			$encodetoken = json_encode($tokendata);
			$tokenarray =  array( 'token'=> $encodetoken, 'expirytime'=>$expirytime);
			$storetoken = serialize($tokenarray);
			if($type != 'sensitive')
				$this->writepublictoken($storetoken);
			else
				$this->writesecrettoken($storetoken);
			return $encodetoken;
		}else{
			return $gettokendata['token'];
		}

	}

	public function getaccesstoken($type = 'sensitive'){
        $tokenurl = $this->tokenurl();
        $merchantid = CLIENT_ID;
        $password = MERCHANT_TO_ZEST_SECRET; 
        $scope = ($type == 'sensitive') ? 'merchant_api_sensitive' : 'merchant_api';
        $fields = array( 'grant_type'=>'client_credentials', 'scope'=>$scope, 'client_id' => $merchantid, 'client_secret' => $password);
        $postdata = http_build_query($fields);
        return $this->request($tokenurl, $postdata);
    }

    public function writepublictoken($storetoken){
		  $handle = fopen(PUBLIC_TOKEN,"w");
		  if (is_writable(PUBLIC_TOKEN) && $handle) {
		    if (fwrite($handle, $storetoken) === FALSE) {
		      exit;
		    }
		    fclose($handle);
		  }else{  	
		  	exit("Please check if the auth file exists and it has write permissions.");
		  }
    }

    public function writesecrettoken($storetoken){
    	$handle = fopen(SECRET_TOKEN,"w");
		  if (is_writable(SECRET_TOKEN) && $handle) {
		    if (fwrite($handle, $storetoken) === FALSE) {
		      exit;
		    }
		    fclose($handle);
		  }else{  	
		  	exit("Please check if the auth file exists and it has write permissions.");
		  }
    }

    public function getsecrettoken(){
    	$tokenfile = fopen(SECRET_TOKEN, "r") or die("Unable to open file!");
    	$filesize = (filesize(SECRET_TOKEN) > 0) ? filesize(SECRET_TOKEN) : 1;
		$token = fread($tokenfile,$filesize);
		fclose($tokenfile);
		return $token;
    }

    public function getpublictoken(){
    	$tokenfile = fopen(PUBLIC_TOKEN, "r") or die("Unable to open file!");
    	$filesize = (filesize(PUBLIC_TOKEN) > 0) ? filesize(PUBLIC_TOKEN) : 1;
		$token = fread($tokenfile,$filesize);
		fclose($tokenfile);
		return $token;
    }

    protected function _getheader(){
    	$tokendata = $this->gettoken();
        $response = json_decode($tokendata);
    	$header = array();
         $header[] = 'Authorization: '.$response->token_type.' '.$response->access_token; 
         return $header;
    }

    protected function _getRefundReasonCode() {
        return array('DamagedGoods', 'GoodsNotReceived', 'NotAsDescribed','GOGW', 'PricingChanged', 'BuyerRemorse');
    }

    public function _getCancellingreason() {
        return array('Damaged', 'NotReceived', 'NotAsDescribed','GestureOfGoodWill', 'PricingChanged', 'BuyerRemorse');
    }

    public function _getZestCancelStatus() {
        return array('Declined', 'CancelledTimeout', 'Cancelled');
    }



}
