
<style>
.collection-text {
	color: #2da3b5;
	padding: 15px;
    position: absolute;
    top: -4px;
    /* left: 25%; */
    left: 0;
    z-index: 1;
    margin-left: 10px;
}

</style>
<div class="home page-content">
		<div class="page-content-area">
			<div class="container">
			<!-- <div align="center"><h2>Furniture</h2></div> -->
			    <div class= "row"><br><br><br>
			    	<div class="col-lg-3 col-sm-6 col-xs-12">
			    		<a href="category/10/bed">
							 <img src="assets/images/refbanner/collection/Bed.jpg" alt="Bed"> 
							 <div class="collection-text"><h5>Bed</h5></div>
						</a>
					</div>
					<div class="col-lg-3 col-sm-6 col-xs-12">	
						<a href="category/8/Office_chair">
							<img src="assets/images/refbanner/collection/Office_chair.jpg" alt="Office_Chair"> 
							<div class="collection-text"><h5>Office Chair</h5></div>
						</a>
			    	</div>
			    	<div class="col-lg-3 col-sm-6 col-xs-12">	
						<a href="category/15/TV-Unit">
							<img src="assets/images/refbanner/collection/tv-stand.jpg" alt="TV-Unit"> 
							<div class="collection-text"><h5>TV Unit</h5></div>
						</a>
			    	</div>
			    	<div class="col-lg-3 col-sm-6 col-xs-12">	
						<a href="category/7/sofa">
							<img src="assets/images/refbanner/collection/sofa.jpg" alt="sofa"> 
							<div class="collection-text"><h5>Sofa</h5></div>
						</a>
			    	</div>
			    	<div class="col-lg-3 col-sm-6 col-xs-12">	
						<a href="category/16/Miscellaneous">
							<img src="assets/images/refbanner/collection/Bookshelf.jpg" alt="Bookshelf">
							<div class="collection-text"><h5>Bookshelf</h5></div> 
						</a><br><br><br><br>
			    	</div>
			    	<div class="col-lg-3 col-sm-6 col-xs-12">	
						<a href="category/17/Dining">
							<img src="assets/images/refbanner/collection/dining.jpg" alt="Dining Table">
							<div class="collection-text"><h5>Dining Table</h5></div> 
						</a><br><br><br><br>
			    	</div>
			    	<div class="col-lg-3 col-sm-6 col-xs-12">	
						<a href="category/13/Shoerack">
							<img src="assets/images/refbanner/collection/shoe.jpg" alt="Shoe Rack">
							<div class="collection-text"><h5>Shoe Rack</h5></div> 
						</a><br><br><br><br>
			    	</div>

			    </div>
			</div>
		</div>
</div>