<style>
	.ref-item {
		clear: both;
	    padding: 20px 0;
	}
	.yellow {
	color: #f8bc3b;
	}
	.green {
	color: #43a047;
	}
	.red {
	color: red;
	}

</style>
<div class="home page-content">
<div class="page-content-area">
<div class="container">
	<div class="products-breadcrumb">
		<ol class="breadcrumb" style="margin-bottom: 5px;">
			<li>
				<a href="#/">Home</a>
			</li>
			<li class="active">
				<a href="">User Account</a>
			</li>
		</ol>
	</div>

	<div class="row">
		<div class="col-xs-12">
			<div class="user-account">
				<div class="row" style="padding:0 15px;">
					<div class="hidden-xs col-sm-3 col-md-3 col-lg-3 no-pad">
						<div id="rootwizard1" class="user-account-tabs tabbable tabs-left">
							<ul>
								<li class="active">
									<a data-target="#account-settings" data-toggle="tab" href="" ng-click="resetuserdetails()">
										<span class="icon pocketin-icon-User"></span>
										Account Settings
										<div class="active-tab-white"></div>
									</a>

								</li>
								<li>
									<a data-target="#addresses" href="" data-toggle="tab" id="address">
										<span class="icon pocketin-icon-home"></span>
										Saved Addresses
										<div class="active-tab-white"></div>
									</a>
								</li>
								<li>
									<a data-target="#notifications" href="" data-toggle="tab" id="nots">
										<span class="pocketin-icon-Notification"></span>
										Notifications
										<div class="active-tab-white"></div>
									</a>
								</li>
								<li>
									<a class="myorderanchor" data-target="#oders" href="" data-toggle="tab" id="order">
										<span class="icon pocketin-icon-File-1"></span>
										Order History
										<div class="active-tab-white"></div>
									</a>
								</li>
								<!--li>
									<a class="myorderanchor" data-target="#complaint" href="" data-toggle="tab" id="order">
										<span class="icon pocketin-icon-Phone"></span>
										Raise a Complaint
										<div class="active-tab-white"></div>
									</a>
								</li-->
								<!--li>
									<a data-target="#payment" href="" data-toggle="tab">
										<span class="icon pocketin-icon-Wallet"></span>
										Payment Methods
										<div class="active-tab-white"></div>
									</a>
								</li-->
								<li>
									<a data-target="#wishlist" href="" data-toggle="tab" id="wishlst">
										<span class="icon pocketin-icon-heart2"></span>
										Wishlist
										<div class="active-tab-white"></div>
									</a>
								</li>
								<li>
									<a data-target="#mysell" href="" data-toggle="tab" id="myselltrigger">
										<span class="icon pocketin-icon-Calender-1"></span>
										Sell Requests
										<div class="active-tab-white"></div>
									</a>
								</li>
								<!-- <li>
									<a data-target="#myreferral" href="" data-toggle="tab" id="myreferraltrigger">
										<span class="icon pocketin-icon-Cart-2"></span>
										Referrals and Points
										<div class="active-tab-white"></div>
									</a>
								</li> -->
								<li>
									<a data-target="#service"  href="" data-toggle="tab" id="servicerequest">
										<span class="pocketin-icon-Settings-2"></span>
										Service Requests
										<div class="active-tab-white"></div>
									</a>
								</li>
							</ul>
						</div>
					</div>

					<div class="visible-xs">
						<div class="nav-pills">
							<div class="dropdown">
								<a class="btn btn-md dropdown-toggle" type="button" data-toggle="dropdown">
									Account Setting Menu
									<span class="caret"></span>
								</a>
								<ul class="dropdown-menu">
									<li class="active">
										<a data-target="#account-settings" data-toggle="tab">Account Settings</a>
									</li>
									<li>
										<a data-target="#addresses" data-toggle="tab">Saved Addresses</a>
									</li>
									<li>
										<a data-target="#notifications" data-toggle="tab">Notifications</a>
									</li>
									<li>
										<a data-target="#oders" data-toggle="tab">Order History</a>
									</li>
									<!--li>
										<a data-target="#complaint" data-toggle="tab">Raise a Complaint</a>
									</li-->
									<!--li>
										<a data-target="#payment" data-toggle="tab">Payment Methods</a>
									</li-->
									<li>
										<a data-target="#wishlist" data-toggle="tab">Wishlist</a>
									</li>
									<li>
										<a data-target="#mysell" data-toggle="tab">Sell Requests</a>
									</li>
									<li>
										<a data-target="#myreferral" data-toggle="tab">Referrals</a>
									</li>
								</ul>
							</div>
						</div>
					</div>

					<div class="tab-content col-xs-12 col-sm-9 col-md-9 col-lg-9 no-pad" style="min-height: 441px;">
						<div class="tab-pane active" id="account-settings">
							<h3>Account Settings</h3>
							<ul class="row user-details" style="padding: 0 15px;">
								<li class="col-sm-6">
									<div class="user-details-wrap" ng-class="activeorpassive1">
										<label>First Name:</label>
										<form name="userForm" novalidate>
											<div class="user-name">
												<input type="text" id="fname" class="form-control" ng-model="updatedUserName" ng-readonly="userReadOnlyText" name="name" required ng-pattern="/^[a-zA-Z]*$/">
												<button class="btn btn-xs ng-binding" ng-class="EditSaveUserClass" ng-click="editSaveUserName(EditSaveUser)" ng-disabled="userForm.name.$dirty && userForm.name.$invalid">
													<span class="glyphicon glyphicon-pencil"></span>
													{{EditSaveUser}}
												</button>
											</div>
										</form>
										<p class="validationerror" ng-show="userForm.name.$dirty && userForm.name.$invalid">Only Letters and no space</p>
									</div>
								</li>
								<li class="col-sm-6">
									<div class="user-details-wrap" ng-class="activeorpassive2">
										<label>Last Name:</label>
										<form name="lnameForm" novalidate>
											<div>
												<input type="text" id="lname" class="form-control" ng-model="updatedUserNameLast" ng-readonly="userReadOnlyTextLastName" name="name" required ng-pattern="/^[a-zA-Z]*$/">
												<button class="btn btn-xs ng-binding"  ng-class="EditSaveUserLastNameClass" ng-click="editSaveUserLastName()" ng-disabled="lnameForm.name.$dirty && lnameForm.name.$invalid">
													<span class="glyphicon glyphicon-pencil"></span>
													{{EditSaveUserLastName}}
												</button>
											</div>
										</form>
										<p class="validationerror" ng-show="lnameForm.name.$dirty && lnameForm.name.$invalid">Only Letters and no space</p>
									</div>
								</li>
								<li class="col-sm-6">
									<div class="user-details-wrap" ng-class="activeorpassive3">
										<label>Phone:</label>
										<label ng-show="EnableVerify">{{phonenumber}}</label>
										<form name="phoneForm" novalidate>
											<div>
												<div ng-hide="EnableVerify">
													<input type="number"  id="phone" class="form-control" ng-model="phonenumber" ng-readonly="phoneNumberReadOnly" name="phone" required ng-pattern="/^[1-9]+[0-9]*$/" ng-minlength = "10" ng-maxlength = "10">
													<button class="btn btn-xs ng-binding"  ng-class="EditSavePhoneClass" ng-click="editSavePhoneNumber()" ng-disabled="phoneForm.phone.$dirty && phoneForm.phone.$invalid">
														<span class="glyphicon glyphicon-pencil"></span>
														{{EditSavePhone}}
													</button>
												</div>

												<div ng-show="EnableVerify">
													<input type="number" id="phoneotp" class="form-control" placeholder="Enter OTP" ng-model="PhoneOTP" name="otp">
													<button ng-class="EditSavePhoneClass" ng-click="editSavePhoneNumber()">{{EditSavePhone}}</button>
												</div>
											</div>
										</form>
										<p class="validationerror" ng-show="phoneForm.phone.$dirty && phoneForm.phone.$invalid">Exactly 10 digits, first digit non zero</p>
										<p class="validationerror">{{msg}}</p>
									</div>
								</li>
								<li class="col-sm-6">
									<div class="user-details-wrap" ng-class="activeorpassive4">
										<label>E-mail:</label>
										<form name="emailForm" novalidate>
											<div>
												<input id="useremail" type="text" class="form-control" ng-model="email" ng-readonly="emailReadOnly" ng-pattern="/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/" name="email" required>
												<button class="btn btn-xs ng-binding" ng-class="EditSaveEmailClass" ng-click="editSaveEmailAddr()" ng-disabled="emailForm.email.$dirty && emailForm.email.$invalid">
													<span class="glyphicon glyphicon-pencil"></span>
													{{EditSaveEmail}}
												</button>
											</div>
										</form>
										<p class="validationerror" ng-show="emailForm.email.$dirty && emailForm.email.$invalid">Not a valid email address!!</p>
										<p class="validationerror">{{msgemail}}</p>
									</div>
								</li>
								<li class="col-sm-6">
									<div class="user-details-wrap" ng-class="activeorpassive5">
										<label>Password:</label>
										<form name="passwordForm" novalidate>
											<div class="edit-password">
												<input type="password" id="pswd" class="passwordfield form-control" ng-model="newpassword" ng-readonly="passwordReadOnly" name="password" placeholder="********">
												<button ng-class="EditSavePasswordClass" ng-click="editSavePassword()">
													<span class="glyphicon glyphicon-pencil"></span>
													{{EditSavePassword}}
												</button>
											</div>
										</form>
										<p class="validationerror">{{msgpwd}}</p>
									</div>
								</li>
							</ul>
						</div>
						<!--div class="tab-pane" id="complaint" ng-controller="ComplaintsController as ctrl">
							<h3>Raise Complaint</h3>
							<ul class="nav nav-tabs">
							  <li role="presentation" class="active"><a href="" data-target="#activecomplaint" data-toggle="tab">Active Complaints</a></li>
							  <li role="presentation"><a href="" data-target="#newcomplaint" data-toggle="tab">New Complaint</a></li>
							  <li role="presentation"><a href="" data-target="#pastcomplaint" data-toggle="tab">Past Complaints</a></li>
							</ul>
							<div class="tab-content">
							  <div id="activecomplaint" class="tab-pane fade in active">
							   <ul class="list-group">
							   	<li class="list-group-item">Item 1</li>
							   	<li class="list-group-item">Item 2</li>
							   	<li class="list-group-item">Item 3</li>
							   </ul>
							  </div>

							  <div id="newcomplaint" class="tab-pane fade">
								  <form role="form" name="compform" novalidate ng-submit="compform.$valid && createTicket()">
								  	<div class="form-group">
								  		<label for="">Subject</label>
								  		<input type="text" class="form-control" ng-model="newcomplaint.subject" required>
								  	</div>
								  	<div class="form-group">
								  		<label for="">Order ID</label>
								  		<input type="text" class="form-control" ng-model="newcomplaint.order_id" placeholder="Enter your Order ID" required>
								  		<p class="validationerror" ng-show="invalidorder">Invalid Order ID</p>
								  	</div>
								  	<div class="form-group">
								  	<label for="">Message</label>
									  	<textarea name="" id="input" class="form-control" rows="6" required="required" ng-model="newcomplaint.message">
									  	</textarea>
								  	</div>
								  	<button type="submit" class="btn btn-address-edit" style="margin-bottom: 10px;" ng-disabled="ticketcreated == 1">{{submitmsg}}</button>
								  </form>
							  </div>

							  <div id="pastcomplaint" class="tab-pane fade">

							  </div>
							</div>
						</div-->

						<div class="tab-pane" id="notifications" ng-controller="UserNotificationController as ctrl">
							<h3>Notifications</h3>
							<ul class="notification-list">
								    <li ng-repeat="not in nots">
										 <a href="">
											 <div class="notification-read">
											 	<div class="row">
													 <div class="col-xs-3 col-sm-1">
													 	<div class="notification-icon">
													 		<span class="pocketin-icon-Info"></span>
													 	</div>
													 </div>
													 <div class="col-xs-9 col-sm-11 notification-text">
														 <div class="">
														    <label>Order is {{not.type}}</label>
															<p>Your Item, <b>{{not.product}}</b> from Order: <b>PI:{{not.order}}</b> has been {{not.type}}. </p>
														 </div>
														 <div class="notification-timestamp">
														 	{{not.date}}
														 </div>
													 </div>
												 </div>
											 </div>
										 </a>
									</li>
							  </ul>
						</div>
						<div class="tab-pane" id="oders" ng-controller="UserOrderController as ctrl">
							<h3>My Orders</h3>
							<div class="past-order-products">
								<ul>
									<li class="cart-item past-order-item" ng-repeat="order in orders">
										<div class="order-id">
											<div class="row">
												<div class="text-left col-xs-6">
													<a ng-href="orderdetails/{{order.order_id}}">Order ID: {{order.order_id}}</a>
												</div>
												<div class="text-right col-xs-6">
													<a ng-href="orderdetails/{{order.order_id}}">
														View Order details
														<span class="icon pocketin-icon-circle-right"></span>
													</a>
												</div>
											</div>
										</div>

										<div class="past-order-item-details" ng-class="item.status == 'Cancelled' ? 'cancelled-order' : ''" ng-repeat="item in order.items" ng-class="item.iscancelled ? 'cancelleditem' : ''">
											<div class="order-currnt-status">
												<span class="delivered-date" ng-class="item.iscancelled ? 'cancelleditem' : ''">
													<h5 class="clear confirmed">{{item.status}}</h5>
													<p>on {{item.date}}</p>
												</span>
												<a class="btn btn-md btn-remove btn-cancel" ng-hide="item.iscancelled || item.status == 'Delivered'"  href="" ng-click="cancelOrderItem(order.order_id, item)" ng-confirm-click="Are you sure that you want to raise a request to cancel the order? You will get notification after the request is processed.">Cancel order</a>
											</div>
											<div class="cart-thumb" style="max-width:100px; margin-top:-15px;">
												<img ng-src="assets/images/products/thumbnail/{{item.productimage}}">
											</div>
											<div class="cart-item-details">
												<h1 class="product-title">
													{{item.productname}}
													<!--span class="quality-index">
													<span class="icon pocketin-icon-quality-index"></span>
													4.2
												</span-->
												<span class="product-description">{{item.productdescription}}</span>
											</h1>
											<div class="colors-available">
												<label class="inline">Status: {{item.status}}</label>
											</div>

											<div class="item-price">
												<span>&#x20B9;</span>
												{{item.amount}}
											</div>
										</div>
									</div>
									<div class="row past-order-footer" style="padding:0 15px;">
										<div class="col-xs-6 text-left">{{order.orderdate}}</div>
										<div class="col-xs-6 product-price text-right">
										Amount: <span>&#x20B9;</span>{{order.amount}}
										</div>
									</div>
								</li>
							</ul>
						</div>
					</div>
					<div class="tab-pane" id="addresses" ng-controller="UserAddressController as ctrl">
						<h3 ng-hide="showeditaddr || shownewaddr">Saved Addresses</h3>
						<div class="saved-addresses-list" ng-hide="showeditaddr || shownewaddr">
							<ul class="row saved-addresses">
								<li class="col-sm-6" ng-repeat="item in items">
									<div>
										<label>{{item.name}}</label>
										<p class="">{{item.address_line_one + " " + item.address_line_two }}</p>
										<p class="landmark">{{item.landmark}}</p>
										<p class="">{{item.pincode}}</p>
										<p class="">{{item.city + "," +item.state}}</p>
										<p>{{item.email}}</p>
										<span class="phone"	>{{item.phonenumber}}</span>
										<div class="action-btns">
											<button type="submit" class="btn btn-xs btn-address-edit" ng-click="editaddress(item.addressid)">
												<span class="glyphicon glyphicon-pencil"></span>
												Edit
											</button>
											<button type="submit" class="btn btn-xs btn-remove" ng-click="removeaddress(item.addressid)" ng-confirm-click="This will permanently delete the address. Do you want to continue?">
												<span class="icon pocketin-icon-trash-2"></span>
												Delete
											</button>
										</div>
									</div>
								</li>
							</ul>
						</div>
						<div class="edit-address" ng-show="showeditaddr">
							<div class="row">
								<div class="col-xs-12 text-center">
									<form class="row text-left" role="form">
										<div class="form-group col-xs-6">
											<label>Name</label>
											<input type="text" class="form-control" ng-model="selectedaddress.name"></div>
										<div class="form-group col-xs-6">
											<label>Email</label>
											<input type="email" class="form-control" ng-model="selectedaddress.email"></div>
										<div class="form-group col-xs-6">
											<label>Phone</label>
											<input type="text" class="form-control" ng-model="selectedaddress.phonenumber"></div>
										<div class="form-group col-xs-6">
											<label>Address Line 1</label>
											<input type="text" class="form-control" ng-model="selectedaddress.address_line_one"></div>
										<div class="form-group col-xs-6">
											<label>Address Line 2 (Apt, Floor, Ste, etc)</label>
											<input type="text" class="form-control" ng-model="selectedaddress.address_line_two"></div>
										<div class="form-group col-xs-6">
											<label>Landmark</label>
											<input type="text" class="form-control" ng-model="selectedaddress.landmark"></div>
										<div class="form-group col-xs-6">
											<label>City</label>
											<input type="text" value="Bangalore" class="form-control" disabled></div>
										<div class="form-group col-xs-6">
											<label>Zip/Postal Code</label>
											<input type="text" class="form-control" ng-model="selectedaddress.pincode"></div>
										<div class="col-xs-6 text-left">
											<button type="submit" class="btn btn-save-address" ng-click="updateaddress()">Save</button>
											<button type="submit" class="btn btn-md btn-link btn-cancel-new-address" ng-click="hideeditaddress()">Cancel</button>
										</div>
									</form>
								</div>
							</div>
						</div>

						<div style="margin-top: 10px;">
							<button class="btn btn-address-edit" style="" ng-click="newAddress()" ng-hide="showeditaddr || shownewaddr">Add New Address</button>

							<div class="col-xs-12 new-address-form" ng-show="shownewaddr">
								<div class="row">
									<div class="col-xs-12 text-center">
										<label class="title">
											Enter a new address
										</label>
										<form class="row text-left" role="form" name="newaddrform" ng-submit="newaddrform.$valid && saveNewAddress()" novalidate>

											<div class="form-group col-xs-6">
												<label for="name">Name</label>
												<input type="text" class="form-control" id="name" ng-model="newaddress.name" required ng-pattern="/^[a-zA-Z\s]*$/" name="name">
												<p class="validationerror" ng-show="newaddrform.name.$dirty && newaddrform.name.$invalid">Only Letters!!</p>
											</div>
											<div class="form-group col-xs-6">
												<label>Email</label>
												<input type="email" name="email" class="form-control" ng-model="newaddress.email" ng-pattern="/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/" required>
												<p class="validationerror" ng-show="newaddrform.email.$dirty && newaddrform.email.$invalid">Not a Valid Email!!</p>
											</div>
											<div class="form-group col-xs-6">
												<label for="phone">Phone</label>
												<input type="number" name="phone" class="form-control" id="phone" ng-model="newaddress.phonenumber" required ng-minlength = "10" ng-maxlength = "10" ng-pattern="/^[1-9]+[0-9]*$/">
												<p class="validationerror" ng-show="newaddrform.phone.$dirty && newaddrform.phone.$invalid">Exactly 10 digits, first digit non zero</p>
											</div>
											<div class="form-group col-xs-6">
												<label>City</label>
												<input type="text" value="Bangalore" class="form-control" disabled></div>
											<div class="form-group col-xs-6">
												<label>Address Line 1</label>
												<input type="text" class="form-control" ng-model="newaddress.address_line_one" required></div>
											<div class="form-group col-xs-6">
												<label>Address Line 2 (Apt, Floor, Ste, etc)</label>
												<input type="text" class="form-control" ng-model="newaddress.address_line_two"></div>
											<div class="form-group col-xs-6">
												<label>Landmark</label>
												<input type="text" class="form-control" ng-model="newaddress.landmark" required></div>
											<div class="form-group col-xs-6">
												<label>Zip/Postal Code</label>
												<input type="number" name="zip" class="form-control" ng-model="newaddress.pincode" required ng-pattern="/^[1-9]+[0-9]*$/" ng-minlength = "6" ng-maxlength = "6">
												<p class="validationerror" ng-show="newaddrform.zip.$dirty && newaddrform.zip.$invalid">Exactly 6 digits, first digit non zero</p>
											</div>
											<!--div class="checkbox col-xs-12 text-left">
											<label>
												<input type="checkbox" ng-model="newaddress.isdefault">Set Default Address</label>
										</div-->
										<div class="col-xs-6 text-left">
											<button type="submit" class="btn btn-save-address" ng-click="savenewaddress()">Save Address</button>
											<button type="submit" class="btn btn-md btn-link btn-cancel-new-address" ng-click="hidesaveaddress()">Cancel</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>

				</div>


				<!--div class="tab-pane" id="payment">
					<h3>Payment Method</h3>
					<br/>
					<h5>Cash on Delivery and Secure online payment</h5>
				</div-->
				<div class="tab-pane" id="mysell" ng-controller="SellRequestController as ctrl">
					<h3>Sell Requests</h3>
					<div class="past-order-products">
						<ul>
							<li class="cart-item" ng-repeat="s in sells">
								<div class="row" style="padding-top:20px; padding-bottom: 20px; padding-left: 10px; padding-right: 10px">
									<div>
										<label class="col-lg-3 col-md-6 col-sm-12 col-xs-12">Sell ID: <span class="label label-success" style="background-color: #d61b5e!important">{{s.sellid}}</span></label>
										<div class="col-lg-5 col-md-6 col-sm-12 col-xs-12">
											<label style="font-size: 15px; font-weight: 400">{{s.productname | limitTo:10}}</label><br>
											<p style="margin-top: 10px; font-size: 13px; font-weight: 200">{{s.maincat}} | {{s.subcat}} | {{s.productbrand}}</p>
										</div>
										<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
											<label><span class="label" ng-class="s.statusclass">{{s.status}}</span></label>
											<div ng-show="s.rejectreason" style="line-height: 1.2; font-size: 12px; font-weight: 200; margin-top: 10px;">
												<span style="font-weight:400">Reason:</span> {{s.rejectreason}}
											</div>
										</div>

										<!--label class="col-lg-2 col-md-4 col-sm-6 col-xs-12">{{s.timestamp}}</label-->
										<!--label class="label label-default" ng-show="s.imageuploadfailed == 1">No images found, Please email the images</label-->
									</div>
								</div>
								<div class="row past-order-footer" style="padding:0 15px;">
									<div class="col-xs-6 text-left">
										Approved Price:
										<span class="product-price" ng-show="s.approvedprice">
											<span>&#x20B9;</span>
											{{s.approvedprice}}
										</span>
										<span style="font-size: 13px; font-weight: 200" ng-show="!s.approvedprice">
											Yet to finalize the deal
										</span>
									</div>
								</div>
							</li>
						</ul>
						<label class="pull-right" style="font-size:13px; font-weight: 300; padding: 20px" ng-hide="nosells">Contact us at <span style="font-weight: 400">70-226-30-270</span> to know detailed status</label>
					</div>
				</div>

				<div class="tab-pane" id="myreferral" ng-controller="ReferralController as ctrl">
					<h3>Referrals and Points</h3>
					<div style="margin-top: 20px;">
						<p style="font-size: 14px; font-weight: 200; margin-bottom: 10px"><span>* </span>You can refer your friends and family here.</p>
						<p style="font-size: 14px; font-weight: 200"><span>* </span>For each successful <span style="text-decoration: underline">registration</span>, you will get a discount of <span style="font-weight: 500">100<span>&#x20B9;</span></span> subjected to a maximum discount of 500<span>&#x20B9;</span> per purchase</p>
					</div>
					<div style="margin-top: 15px; padding: 10px; border-radius: 3px;">
						<form class="row text-left" name="newreferform" ng-submit="newreferform.$valid && addnewReferral()" novalidate>
							<div class="form-group col-lg-5 col-md-5 col-sm-5 col-xs-5">
								<label for="name">Name:</label>
								<input style="background: 0 0!important; border: 1px solid #d9edf7; color:#31708f" type="text" class="form-control" ng-model="nr.rname" required>
							</div>
							<div class="form-group col-lg-5 col-md-5 col-sm-5 col-xs-5">
								<label for="name">Mobile:</label>
								<input style="background: 0 0!important; border: 1px solid #d9edf7; color:#31708f" type="text" class="form-control" ng-model="nr.rmobile" required>
							</div>
							<div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-2">
								<button style="margin-top: 32px;padding:7px" type="submit" class="btn btn-address-edit">{{referbutton}}</button>
							</div>
						</form>
						<div ng-show="alreadyregstrd">
						<p style="color: darkorange; font-size: 13px;font-weight: 200;">Referral failed - user already registered</p>
						</div>
					</div>
					<div class="panel panel-info" ng-hide="norefs">
						<div class="panel-heading">
						<label for="">Referral Status</label>

						<label class="pull-right">Discount Available: <span style="font-weight: 600">{{points}}<span>&#x20B9;</span></span></label>
						</div>
						<div class="panel-body">
							<ul>
								<li>
									<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
										<label for="" style="font-size: 14px; font-weight: 500;">Name</label>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
										<label for="" style="font-size: 14px; font-weight: 500">Mobile</label>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
										<label for="" style="font-size: 14px; font-weight: 500">Status</label>
									</div>
								</li>
								<li class="ref-item" ng-repeat="r in refers">
									<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
										<label for="" style="font-size: 14px; font-weight: 300">{{r.rname}}</label>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
										<label for="" style="font-size: 14px; font-weight: 300">{{r.rmobile}}</label>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
										<label for="" style="font-size: 14px; font-weight: 300" ng-style="r.availed == 1 ? {} : r.ruser_id ? {'color' : '#0582CA'} : {}">{{r.availed == 1 ? 'Redeemed' : r.ruser_id ? 'Registered' : 'Registration Pending'}}</label>
									</div>
								</li>
							</ul>
						</div>
						<label class="pull-right" style="font-size:13px; font-weight: 300; padding: 20px">Contact us at <span style="font-weight: 400">70-226-30-100</span> to avail the discount after placing the order.</label>
					</div>
				</div>

				<!-- Service tab begins-->

<div class="tab-pane" id="service" ng-controller="ServiceController as ctrl">
	<h3>Service Requests</h3>
	<div style="margin-top: 15px; padding: 10px; border-radius: 3px;">
		<!--Request form begins -->
		<div class="row">
			<label for="" >Order ID:</label>
			<form name="orderidform" ng-submit="orderidform.$valid && getOrderDetails(neworderid);">
				<div class="input-group" style="width:50%">
				<input type="text" class="form-control" ng-model="neworderid" required>
				<span class="input-group-btn">
				<input class="btn btn-success" type="submit">Fetch</span>
				</div>
				<p style="color: darkorange;margin-left: 10px" ng-show="ordernotfound == 1">Please check the Order ID</p>
				<!-- <p style="color: darkorange;margin-left: 10px" ng-show="nowarranty == 1">Sorry! Out of warranty.</p> -->
			</form>
			<form role="form"  name="service" ng-submit="service.$valid && neworder != undefined && addTicket();" novalidate style="margin: 25px">
				<div ng-show="neworder != null" style="border: 1px solid lightgrey; padding: 10px;">
					<input type="hidden" ng-model="neworderid">
					<div class="row">
						<div class="text-left col-xs-6">
						<a style="color: darkorange">Order ID: {{neworder.id}} </a>
						</div>
					</div>
					<div class="past-order-item-details" ng-repeat="item in neworder.items">
						<div class="row">
							<div class="cart-thumb col-lg-2 col-xs-12 col-md-6 col-sm-12">
								<a ng-href="../assets/images/products/normal/{{item.productimage}}">
								<img ng-src="../assets/images/products/thumbnail/{{item.productimage}}"></a>
							</div>
							<div class="cart-item-details col-lg-3 col-xs-12 col-md-6 col-sm-12">
								<label>{{item.productname}}</label>
								<br>
								<label>Amount: {{item.amount}}</label>
							</div>
							<div class="cart-item-details col-lg-3 col-xs-12 col-md-6 col-sm-12">
								<label>{{neworder.orderdate}}</label>
							</div>
							<div class="cart-item-details col-lg-3 col-xs-12 col-md-6 col-sm-12">
								<label>{{neworder.username}}</label>
								<label>{{neworder.deliveryaddress.email}}</label>
								<label>{{neworder.deliveryaddress.address_line_one}}</label>
								<label>{{neworder.deliveryaddress.address_line_two}}</label>
							</div>
						</div>
					</div>

					<!-- <div ng-show="neworder != null"> -->
					<input type="hidden" ng-model="neworder.status">
					<div class="form-group" style="width:50%">
						<label for="">Product Title</label>
						<input type="text" class="form-control" ng-model="subject" required>
					</div>
					<div class="form-group" style="width:50%">
						<label for="">Service Location</label>
						<input type="text" class="form-control" ng-model="location" required>
					</div>  
					<div class="form-group" style="width:50%">
						<label for="">Comment</label>
						<textarea class="form-control" rows="4" ng-model="comment" required></textarea>
					</div>
					<button type="submit" class="btn btn-warning" ng-disabled="newticket == 1">Create</button>
					<span ng-show="newticket == 2" class="label label-success">Service Request Created. You will get a call within 24 hours.</span><br>
					<span ng-show="neworder == undefined && newtick.$submitted" class="label label-success">Click fetch button</span>
				</div>
			</form>
		</div> <!-- Form Row div ends --> 
		<!-- Request view begins-->
		<div class="panel panel-info" ng-hide ="show == 0" >
			<div class="panel-heading">
				<label for="">Service Requests</label>
			</div>
			<div class="panel-body">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>Ticket ID</th>
							<th>Date</th>
							<th>Order ID</th>
							<th>Subject</th>
							<th>Status</th>
							<th>Technician</th>
							<!-- <th>Days</th> -->
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="ticket in tickets">
							<!-- <td>{{ticket.id}}<span ng-show="ticket.warranty == 10" class="label label-warning"  >Out of Warranty</span></td> -->
							<td>{{ticket.id}}<span ng-show="ticket.underwarranty == 0" class="label label-warning" >Out of Warranty</span></td>
							<td>{{ticket.createdtime| todate | date:"dd'-'MM'-'yyyy"}}</td>
							<td>{{ticket.order_id}}</td>
							<td>{{ticket.subject}}</td>
							<td>{{ticket.status}}</td>
							<!-- <td ng-class="(ticket.status == 'Open' ? 'red' : ticket.status !=  'Resolved' ? 'yellow' : 'green')">{{ticket.status}}</td> -->
							<td>{{ticket.tech}}</td>
							<!-- <td>{{ticket.days}}</td> -->
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<!--Request view ends --> 
	
	<p style="position: absolute;bottom: 5px;right: 5px;font-weight: 400;font-size: 14px;">Please contact 7022630270 for your service request status.</p>

	</div>

</div>


				<!-- Service tab ends-->

				<div class="tab-pane" id="wishlist" ng-controller="WishlistController as ctrl">
					<h3>Wishlist</h3>
					<div class="past-order-products">
						<ul>
							<li class="cart-item  past-order-item" ng-repeat="item in items">
								<div class="past-order-item-details">
									<div class="order-currnt-status">
										<a href="" class="btn btn-link remove-product" ng-click="removeItem(item.wishid)">Remove</a>
										<button class="btn btn-md btn-buy-now" type="button" ng-click="buyNow(item)" ng-disabled="item.issold == 1">Buy now</button>
										<div class="inline" ng-controller="CartManagementController as cartctrl">
			                			<button class="btn btn-md btn-add-to-cart" ng-click="addToCart(item)" type="button" ng-disabled="item.issold == 1">Add to cart</button>
			                			</div>
										<br/>
										<div class="badge stock-availability unavailable" ng-show="item.issold == 1">Sold Out</div>
									</div>
									<a ng-href="product/wishlist/{{item.id}}/{{item.product_title|toproducturl}}">
										<div class="cart-thumb" style="max-width:100px; margin-top:-15px;">
											<img ng-src="assets/images/products/normal/{{item.images[0].image_full}}">
										</div>
									</a>

									<div class="cart-item-details">
										<!-- <div class="badge stock-availability available">{{item.issold}}</div>
									-->
									<h1 class="product-title">
										<a ng-href="product/wishlist/{{item.id}}/{{item.product_title|toproducturl}}">{{item.product_title}}</a>
										<div class="quality-index" ng-class="item.qualityindex >= 4 ? 'green-dark' : (item.qualityindex >=3 ? 'green-bright' : (item.qualityindex >=2 ? 'yellow' : (item.qualityindex
											< 2 ? 'red' : 'grey')))">
												<span class="icon pocketin-icon-star-empty"></span>
												{{item.qualityindex| number:1}}
											</div>
											<span class="product-description">{{item.product_description}}</span>
										</h1>
									</div>
								</div>
								<div class="row past-order-footer" style="padding:0 15px;">
									<div class="col-xs-6 text-left">
										Price
										<span class="product-price">
											<span>&#x20B9;</span>
											{{item.saleprice}}
										</span>
									</div>
									<div class="col-xs-6"></div>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>


</div>
</div>
</div>