<?php return; ?>
<div ng-controller="LoginSignupController as ctrl" style="margin-top:400px;" class="container">
	<div>
		<!-- Modal content-->
		<div>
			<div class="text-left sign-up-offers" style="padding: 10px; padding-top: 0;">
				<label>Register now and get 10% OFF on your first purchase!!</label>
			</div>
			<div class="modal-header" style="border-bottom:0">
				<!-- <h5 class="modal-title" style="color: darkorange">Register now and get 10% OFF on your purchases!!</h5>
			-->
			<p>New User - Register here</p>

		</div>
		<div>
			<div class="row">
				<form name="signupform" class="text-left" role="form" ng-submit="signupform.$valid && ctrl.signup()" novalidate>
					<div class="form-group col-xs-6" style="padding: 0 7px 0 15px;">
						<input type="text" name="fname" class="form-control" placeholder="First Name" ng-model="user.firstname" required ng-disabled="!canedit" ng-pattern="/^[a-zA-Z]*$/">
						<p class="validationerror" ng-show="signupform.$submitted && signupform.fname.$invalid">Only Letters and no space</p>
					</div>
					<div class="form-group col-xs-6" style="padding: 0 15px 0 7px;">
						<input name="lname" type="text" class="form-control" placeholder="Last Name"  ng-model="user.lastname" ng-disabled="!canedit" ng-pattern="/^[a-zA-Z]*$/">
						<p class="validationerror" ng-show="signupform.$submitted && signupform.lname.$invalid">Only Letters and no space</p>
					</div>
					<div class="form-group col-xs-12">
						<input name="email" type="email" placeholder="Email" class="form-control" ng-model="user.email" id="email" required ng-disabled="!canedit" ng-pattern="/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/">
						<p class="validationerror" ng-show="signupform.$submitted && signupform.email.$invalid">Enter a valid email</p>
					</div>
					<div class="form-group col-xs-12">
						<input type="number" name="phoneNumber" placeholder="Phone Number" class="form-control"  ng-minlength = "10" ng-maxlength = "10" ng-model="user.phonenumber" required ng-disabled="!canedit" ng-pattern="/^[1-9]+[0-9]*$/">
						<p class="validationerror" ng-show="signupform.$submitted && signupform.phoneNumber.$invalid">Exactly 10 digits, first digit non zero</p>
					</div>
					<div class="form-group col-xs-12">
						<input name="pwd" type="password" placeholder="Password" class="form-control" ng-model="user.password" required ng-disabled="!canedit">
						<p class="validationerror" ng-show="signupform.$submitted && signupform.pwd.$invalid">Password Required</p>
					</div>
					<div class="form-group col-xs-12">
						<p class="validationerror">{{message}}</p>
					</div>
					<button type="submit" class="btn btn-sign-up-form" ng-disabled="!canedit" ng-hide="1"></button>
				</form>
				<form name="signupotpform" ng-submit="signupotpform.$valid && ctrl.verifyotp(otp)" novalidate ng-show="otpshow">
					<div class="form-group col-xs-6">
						<input type="number" class="form-control" placeholder="Enter OTP"  ng-model="otp" required></div>
					<button type="submit" class="btn btn-default">Verify</button>
				</form>
			</div>
		</div>

		<div class="modal-footer">
			<div class="row">
				<div class="col-xs-6 text-left">
					<button type="button" class="btn btn-sign-up-form" ng-disabled="!canedit" ng-click="signupform.$submitted = 1; signupform.$valid && ctrl.signup()">Sign Up</button>
				</div>
				<div class="col-xs-6 text-right">
					<button type="reset" class="btn btn-cancel" ng-click="Skipsignup()">Skip Signup</button>
				</div>
			</div>
		</div>
	</div>

</div>
</div>