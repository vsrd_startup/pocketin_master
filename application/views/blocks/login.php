<div class="dropdown">
	<section>
		<div class="login-from container">
			<label>Login</label>
			<form method="post" name="loginform" ng-submit="loginform.$valid && ctrl.login()" novalidate>
				<div class="row">
					<div class="col-xs-12">
						<input class="form-control" type="text" name="login" value="" placeholder="Phone Number or Email" ng-model="user.username" ng-disabled="!canedit" required></div>
					<div class="col-xs-12">
						<input class="form-control" type="password" name="password" value="" placeholder="Password" ng-model="user.password" required ng-disabled="!canedit"></div>
					<div class="col-xs-4 submit">
						<input class="btn btn-login" type="submit" name="commit" value="Login" ng-disabled="!canedit"></div>
					<div class="col-xs-8 login-help">
						<a href="" data-toggle="modal" data-target="#forgotpwdmodal">Forgot password?</a>
					</div>
					<div class="col-xs-12">
						<p class="validationerror">{{message}}</p>
					</div>
				</div>
				<a href="" style="padding: 10px;" ng-click="getotp(user.username)" ng-show="requestotp">Resend OTP?</a>
			</form>
			<div ng-show="requestotp">
								  	  <form ng-submit="verifyotpandlogin()">
									  	  <div class="row" ng-show="showotp">
											  <div class="form-group col-xs-8" style="padding: 0 7px 0 15px;">
											  	<input type="number" class="form-control" placeholder="Enter OTP"  ng-model="otp">
											   </div>
											   <div class="form-group col-xs-4">
											    <button type="submit" class="btn btn-default">Verify</button>
											  </div>
										  </div>
									  </form>
								</div>
		</div>
		<div class="col-xs-12 newuserdiv">
			<a class="btn btn-sign-up" data-toggle="modal" data-target="#signupform" href="" data-backdrop="static" data-keyboard="false"><span>New User? Sign Up here.</span></a>
		</div>
		<div class="filler"></div>
	</section>
</div>