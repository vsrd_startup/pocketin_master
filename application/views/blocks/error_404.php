<div class="container">
    <div class="error-page">
        <img src="assets/images/error.jpg" alt="Error" >
        <h3>Opps! Page not found!!</h3>
        <p>
            We are sorry that we were not able to service your request. We will resolve the issue as early as possible. Sorry for the inconvenience caused!!
        </p>
        <br>
        <br>
        <a class="btn btn-info btn-continue-shopping" style="color: white;" href="/">Continue Shopping</a>
        </div>
</div>
