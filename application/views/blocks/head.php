<head>
	<base href="/">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta charset="utf-8" />
	<title>Quality Tested Products | Refabd</title>
	<meta name="description" content="Shop online for quality tested used home appliances and furniture with free shipping, cash on delivery, 6 months warranty and post care sales service only at Refabd.">
	<meta name="keywords" content="Bangalore used products, Bangalore second hand products, used home appliances in bangalore, used furniture in bangalore, second hand furniture in bangalore, used refrigerator in bangalore, second hand refrigerator in bangalore, used washing machine in bangalore, second hand washing machine in Bangalore, second hand air conditioner in bangalore, used air conditioner in bangalore, second hand water purifier in bangalore, used water purifier in bangalore, used products with warrenty, used products with free service, used products with free delivery in bangalore, sell furniture, sell refrigerator, sell washing machine, sell appliances, exchange products, upgrade products, sell"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<link rel="canonical" href="http://www.refabd.com" />
	<meta property="og:title" content="Quality tested used products for sale in bangalore"/>
	<meta property="og:description" content="Shop online for quality tested used home appliances and furniture with free shipping, cash on delivery, 6 months warranty and post care sales service only at Refabd."/>
	<meta property="og:type" content="website"/>
	<meta property="og:url" content="https://www.refabd.com"/>
	<meta property="og:site_name" content="refabd.com"/>

	<link rel="stylesheet" href="assets/css/bootstrap.min.css" />
	<link rel="stylesheet" href="assets/css/custom.css"/>
	<link rel="stylesheet" href="assets/css/refabd_icons/style.css">
	<link rel="stylesheet" href="assets/js/ngtoast/ngToast.min.css">
	<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
	<link rel="stylesheet" type="text/css" href="assets/slick/slick/slick.css"/>
	<link rel="stylesheet" type="text/css" href="assets/slick/slick/slick-theme.css"/>
	<link rel="stylesheet" type="text/css" href="assets/zestphpsdk/web/pwid/css/zestmerchant.css"/>
	<link rel="stylesheet" type="text/css" href="assets/zestphpsdk/web/pwid/css/zest_check.css"/>
	<link rel="stylesheet" type="text/css" href="assets/sidebarjs/dist/angular-sidebarjs.min.css"/>
	<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">

	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/bower_components/angular/angular.min.js"></script>
    <script src="assets/bower_components/angular-route/angular-route.min.js"></script>
    <script src="assets/bower_components/angular-cookies/angular-cookies.min.js"></script>
    <script src="assets/js/ngtoast/angular-sanitize.min.js"></script>
    <script src="assets/js/ngtoast/ngToast.min.js"></script>
    <script src="assets/js/angular/ref_con.min1.js"></script>
    <script src="assets/js/angular/pocketin_main_app.js"></script>
    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
    <script src="assets/js/imagezoom.min.js"></script>
    <script  src="assets/js/custom.js"></script>
    <!--script type="text/javascript" src="assets/zestphpsdk/web/js/zestmerchant.js"></script-->
    <script type="text/javascript" src="assets/zestphpsdk/web/pwid/js/zestmerchant.js"></script>
    <script src="assets/js/dealer/ng-flow-standalone.min.js"></script>
    <script type="text/javascript" src="assets/slick/slick/slick.min.js"></script>
    <script type="text/javascript" src="assets/sidebarjs/dist/angular-sidebarjs.min.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-102606028-1', 'auto');
  ga('send', 'pageview');
</script>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
<style>
	.slick-slide {
  margin: 0px 20px;
}
.slick-slide img {
  width: 100%;
}
.slick-prev:before,
.slick-next:before {
  color: black;
}
.slick-slide {
  transition: all ease-in-out .3s;
  opacity: .2;
}
.slick-active {
  opacity: .5;
}
.slick-current {
  opacity: 1;
}
.slick-prev {
	position:absolute;
	left: -3px!important;
  z-index: 1;
}
.slick-next {
	position:absolute;
  right: -3px!important;
  z-index: 1;
}
.slick-prev:before, .slick-next:before {
    color: #b5b5b5!important;
}
.slick-dots li.slick-active button:before {
    opacity: 1!important;
    color: #008bff!important;
}
</style>
</head>
