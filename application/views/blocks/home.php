<style>
.refunderline:before {
    content: "";
    position: absolute;
    width: 50%;
    height: 2px;
    bottom: -10px;
    left: 25%;
    border-bottom: 1px solid darkorange;
}
#collection img {
	border: 1px solid #fff;
}
#collection img:hover {
	border: 1px solid #d1d1d1;
}
.collection-text {
	padding: 15px;
    position: absolute;
    top: -4px;
    /* left: 25%; */
    left: 0;
    z-index: 1;
    margin-left: 10px;
}
.collection-text h4 {
	color: #2da3b5;
	font-weight: 400;
  	text-transform: uppercase;
  	font-size: 14px;
}

@media (max-width: 500px) 
{
	#collection {
		display: none;
	}

}
</style>
<div class="container-fluid">
	<?php include 'banner.php'; ?>
</div>
<div class="home page-content" ng-controller="homeController"><div class="page-content-area"><div class="container">
	<!-- Our Collection -->
	<div id="collection" class="category-list-home text-center">
		<div class="sub-header col-xs-12">
			<div class="subtitle fancy">
				<span>
					<h4>
						<a href="collection" class="refunderline">Trending Collection</a>
					</h4>
				</span>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
				<a href="category/3/refrigerator">
					<div>
						<img src="assets/images/refbanner/collection/ref.jpg" alt="Refrigerators">
					</div>
					<div class="collection-text"><h4>Refrigerator</h4></div>
				</a>
			</div>
			<!--div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
				<a href="collection/appliances">
					<div>
						<img src="assets/images/refbanner/collection/wash.jpg" alt="Washing Machines">
					</div>
				</a>
			</div-->
			<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
				<a href="category/5/air_conditioner">
					<div>
						<img src="assets/images/refbanner/collection/ac.jpg" alt="AC">
					</div>
					<div class="collection-text"><h4>Split AC</h4></div>
				</a>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
				<a href="category/12/Laptop">
					<div>
						<img src="assets/images/refbanner/collection/laptop.jpg" alt="Laptop">
					</div>
					<div class="collection-text"><h4>Laptop</h4></div>
				</a>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
				<a href="category/10/bed">
					<div>
						<img src="assets/images/refbanner/collection/bed.jpg" alt="Bed">
					</div>
					<div class="collection-text"><h4>Bed with Storage</h4></div>
				</a>
			</div>
		</div>
		<div class="col-xs-12 text-right">
			<a class="btn btn-sm btn-view-category" href="collection" type="button">View all</a>
		</div>
	</div>


	<!--div class="category-list-home text-center">
		<div class="sub-header col-xs-12">
			<div class="subtitle fancy">
				<span>
					<h4>
						<a href="collection">Home Appliances and Furniture</a>
					</h4>
				</span>
			</div>
		</div>

		<div class="row">
			<div class="left-grid col-xs-12 col-sm-6 no-pad text-left">
				<div class="col-xs-12 col-sm-6 category-col less-pad">
					<a href="category/3/refrigerators">
						<div class="relative">
							<div class="category-detail">
								<a style="color:white" href="category/3/refrigerators"><h4>REFRIGERATOR</h4></a>
								<p>Wide range of top quality refrigerators</p>
								<a href="category/3/refrigerators"><span class="btn btn-white">View more</span></a>
							</div>
							<a href="category/3/refrigerators"><span class="overlay"></span></a>
							<img src="assets/images/collections/fridge.jpg" alt="Refrigerators" width="360" height="466">
						</div>
					</a>
				</div>
				<div class="col-xs-12 col-sm-6 category-col less-pad">
					<a href="category/11/televisions">
						<div class="relative" style="height:291px;">
							<div class="category-detail">
								<a style="color:white" href="category/11/televisions"><h4>TELEVISION</h4></a>
								<p>LCD and LED Televisions</p>
								<a href="category/11/televisions"><span class="btn btn-white">View more</span></a>
							</div>
							<a href="category/11/televisions"><span class="overlay"></span></a>
							<img src="assets/images/collections/tv.jpg" alt="Televisions" style="padding-top: 40px;">
						</div>
					</a>
				</div>
				<div class="col-xs-12 col-sm-12 category-col less-pad">
					<a href="category/10/bed">
						<div class="relative">
							<div class="category-detail">
								<a style="color:white" href="category/10/bed"><h4>BED</h4></a>
								<p>Single, Queen and King size beds</p>
								<a href="category/10/bed"><span class="btn btn-white">View more</span></a>
							</div>
							<a href="category/10/bed"><span class="overlay"></span></a>
							<img src="assets/images/collections/bed.jpg" alt="Bed" width="720" height="360">
						</div>
					</a>
				</div>
			</div>
			<div class="right-grid col-xs-12 col-sm-6 category-col no-pad text-left">
				<div class="col-xs-12 less-pad">
					<a href="category/4/washing-machines">
						<div class="relative" style="height:564px;">
							<div class="category-detail">
								<a style="color:white" href="category/4/washing-machines"><h4>WASHING MACHINE</h4></a>
								<p>Quality washing machines at the best price</p>
								<a href="category/4/washing-machines"><span class="btn btn-white">View more</span></a>
							</div>
							<a href="category/4/washing-machines"><span class="overlay"></span></a>
							<img src="assets/images/collections/washing.jpeg" alt="Washing Machines" style="padding-top: 30px">
						</div>
					</a>
				</div>
			</div>

			<div class="col-xs-12 text-right">
				<a class="btn btn-sm btn-view-category" href="collection" type="button">View all</a>
			</div>
		</div>
	</div-->

	<div class="hor-gird quality-products-home text-center" ng-controller="NewArrivalsController as ctrl">
		<special-products></special-products>
	</div>

<div class="sub-header col-xs-12 text-center">
    <div class="subtitle fancy" ><span><h4><div class="refunderline">From Used to Ready to Use </div></h4></span></div>
</div>
<div class="col-lg-7 col-xs-12" style="float: none;margin: 0 auto;">
<a href="collection"><img src="assets/images/refbanner/wwd.jpg" class="img-responsive" alt="About Refabd" style="width:100%;border:1px solid #e0e0e0"></a>
</div>
	<!-- Customer stories -->
	<div class="hor-gird user-testimonials text-center">
		<div class="sub-header col-xs-12">
            <div class="subtitle fancy" ><span><h4><div class="refunderline"> What our customers say?</div></h4></span></div>
            <!--div style="color: #d61b5e; margin-top: 10px; font-size: 14px;">We have 700<sup>+</sup> registered customers and we delivered 400<sup>+</sup> products</div-->
		</div>
		<div class="row relative">
			<div class="col-lg-11 col-centered">
				<!--ul class="grid-control">
					<li class="control-left"><span class="icon pocketin-icon-circle-left"></span></li>
					<li class="control-right"> <span class="icon pocketin-icon-circle-right"></span></li>
				</ul-->
				<div class="row relative">
					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-3 item-col">
						<div class="user-testimonial">
							<div class="text-left">
								<div class="col-sm-12 no-pad">
									<blockquote>
									    <p>It is an awesome experience with Refabd they helped me in initial set up in Bangalore as I m completely new to these purchases. We shall be happy if u start furniture dinning and furnishing a complete house</p>
									    	<div class="testimonial-username">
									    		<strong>Very Satisfied</strong>
									    		<span>Hassle free Delivery and installation</span>
									    	</div>
									</blockquote>
					            </div>
							</div>
						</div>
					</div>

					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-3 item-col">
						<div class="user-testimonial">
							<div class="text-left">
								<div class="col-sm-12 no-pad">
									<blockquote>
									    <p>Refabd contacted me while I was browsing items on quikr and I bought a washing machine which was serviced with a six month guarantee and also purchase back option. I was fine with the delivery time, but I feel that It is one of the pain areas of your other competitors like olx/quikr, because their delivery is very bad and there is no reliability on the seller.</p>
									    	<div class="testimonial-username">
									    		<strong>Delighted</strong>
									    		<span>Hassle free Post sale handlings</span>
									    	</div>
									</blockquote>
					            </div>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-3 item-col">
						<div class="user-testimonial">
							<div class="text-left">
								<div class="col-sm-12 no-pad">
									<blockquote>
									    <p>Great experience.. Produxt got delivered within 16hrs..Great work.. come up with an app and improve upon the website</p>
									    	<div class="testimonial-username">
									    		<strong>Very Satisfied</strong>
									    		<span>Recently moved in and furnishing afresh</span>
									    	</div>
									</blockquote>
					            </div>
							</div>
						</div>
					</div>

					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-3 item-col">
						<div class="user-testimonial">
							<!-- <div class="user-poduct-thumb">
								<img src="assets/images/image-4.png" alt="Jackets" width="360" height="466">
							</div> -->
							<div class="text-left">
								<div class="col-sm-12 no-pad">
									<blockquote>
									    <p>Received the product in a good condition , price was satisfactory and post sales customer support is quite good. Would definitely recommend.</p>
									    	<div class="testimonial-username">
									    		<strong>Value for money products</strong>
									    		<span>Somewhat satisfied</span>
									    	</div>
									</blockquote>
					            </div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<a href="testimonials" style="color: #d61b5e; font-size: 13px; text-decoration:underline; position: absolute; right: 20px; bottom: 10px;">See them all</a>
		</div>

	</div><!-- testimonial ends here-->
	<!-- New Arrival -->

	<!-- Recently Sold -->
	<!--div class="hor-gird quality-products-home text-center" ng-controller="SoldController as ctrl" ng-hide="ismobile">
		<special-products></special-products>
	</div-->
	<!-- Top quality products -->
	<div class="hor-gird quality-products-home text-center" ng-controller="TopQualityController as ctrl">
		<special-products></special-products>
	</div>

	<!-- History -->
	<div class="hor-gird quality-products-home text-center" ng-controller="ItemHistoryController as ctrl" ng-show="itemspresent" ng-hide="ismobile">
		<special-products></special-products>
	</div>

	<!-- Related to items viewed -->
	<!--div class="hor-gird quality-products-home text-center" ng-controller="RelatedItemsController as ctrl">
	<special-products></special-products>
</div-->
</div>
</div>
</div>
