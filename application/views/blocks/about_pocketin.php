<div class="main-container">
	<div class="about-us page-content">
		<div class="page-content-area">
			<div class="container about-page">
				<div class="sub-header col-xs-12 text-center">
					<div class="subtitle fancy">
						<span>
							<h4>About Us</h4>
						</span>
					</div>
				</div>

				<div class="text-center">
					<p align="left">
						<font color="#47C9AF"><b>Planning to buy a used product ? Here we are to help you!!</b><br></font>

						Refabd is an online platform to buy quality tested used home appliances and furniture.<br><br>

						<font color="#47C9AF"><b>How Refabd started ?</b><br></font>

						When we moved to Bangalore from Delhi, we chose to buy used household products as they are 50% lower 
						in price compared to new ones and we can sell them back whenever we move out of the city. 

						<br><br> 
						But we faced a lot of challenges in buying used products. The biggest challenge was to figure out the exact quality and 
						condition of the product. There was no propper way to get the data. Understatnding the fact, there was no other option, we did a 
						rough check and bought a refrigerator. Just after one month its motor failed and it stopped working!! 
						As it was out of warranty, we got no free service nor exchange, so we ended up in  buying another product.  <br><br>

						Only then we realised, for used products, we need professionals to do the quality check in order to 
						understand the life time of the product and its components. 
						 <br><br>

						So how can we buy used products which are quality checked and with right price for its quality ?

						<br><br>That is when we decided to go online and solve this problem focusing on building TRUST in used product sales. <br><br>

						<font color="#47C9AF"><b>What we do differently ?</b><br></font>

						Refabd offers customers transperancy about the quality and the price of the product. 
						To ensure quality, every product undergo thorough inspections before being sold. 
						The company has specialized QC personnel consisting of technicians and QC checkers 
						who rate the product based on the predefined quality parameters. <br><br>

						Buyers easily get to know the exact condition and history of the product from the quality ratings and 
						get the best price. <br><br>

						Used product purchase has never been this easy and reliable. 
					</p>
				</div>
				<div class="sub-header col-xs-12 text-center">
					<div class="subtitle fancy">
						<span>
							<h4>Our Services</h4>
						</span>
					</div>
				</div>

				<div class="about-services">

					<div class="row">
						<div class="col-xs-12 col-sm-2 text-center">
							<span class="icon pocketin-icon-Box-1 huge"></span>
						</div>
						<div class="col-xs-12 col-sm-10 about-Service">
							<h5>Free delivery</h5>
							<ul>
								<li>On Time Delivery and Hassle free Service</li>
								<li>
									Delivery absolutely free for customers. so, just pay for the product not a penny extra. 
								</li>
							</ul>
						</div>
					</div>

					<div class="row">
						<div class="col-xs-12 col-sm-2 text-center">
							<span class="icon pocketin-icon-Riddon huge"></span>
						</div>
						<div class="col-xs-12 col-sm-10 about-Service">
							<h5>6 Month warranty (T&C applied)</h5>
							<ul>
								<li>
									Anything bought from us within the last 6 months can be returned to us if found to be defective .We will repair or replace the unit (at our discretion), free of charge.
								</li>
							</ul>
						</div>
					</div>

					<div class="row">
						<div class="col-xs-12 col-sm-2 text-center">
							<span class="icon pocketin-icon-Thumps-up huge"></span>
						</div>
						<div class="col-xs-12 col-sm-10 about-Service">
							<h5>Quality Check</h5>
							<p>
								To ensure quality, every product undergo thorough inspections before being sold. The company has specialized QC personnel consisting of technicians and QC checkers.
							</p>
						</div>
					</div>

					<div class="row">
						<div class="col-xs-12 col-sm-2 text-center">
							<span class="icon pocketin-icon-Riddon huge"></span>
						</div>
						<div class="col-xs-12 col-sm-10 about-Service">
							<h5>Post Sale care service</h5>
							<p>
								We offer 24/7 after-sales services and support, which ensure top quality standards, 
								fast problem resolution and the ability to establish a high value image. 
							</p>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
