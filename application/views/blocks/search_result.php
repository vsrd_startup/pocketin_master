<div id="category" ng-init="initSearch('<?php echo $query ?>', '<?php echo $cat ?>', '<?php echo $items ?>')">
	<div class="home page-content">
		<div class="page-content-area">
			<div class="container">
				<div class="products-breadcrumb">
					<ol class="breadcrumb" style="margin-bottom: 5px;">
						<li><a href=".">Home</a></li>
						<li><a href="collection">Collections</a></li>
						<li class="active"><a href=""><?php echo $query ?> </a></li>
					</ol>
				</div>
				<div ng-show="catloading" class='catloader'>
			        <img src='assets/images/catloading.gif'>
			    </div>


				<div class="row" ng-show="!catloading">
					<div class="product-results-container container">
						<!--div class="product-results-header">
							<label class="results-info inline">Showing {{itembegin}}–{{itemend}} of {{totalitems}} results</label>
							<div class="inline sort-dropdown dropdown pull-right">
							  <button class="btn dropdown-toggle btn-sort" type="button" data-toggle="dropdown">
								  <div class="inline">
								    {{currentsort}}
								  	<span class="caret"></span>
								  </div>
							  </button>
							  <ul class="dropdown-menu" role="menu">
							    <li ng-repeat="sorttype in sorttypes"><a ng-click="sortProducts(sorttype)">{{sorttype}}</a></li>
							  </ul>
							</div>
						</div-->

						<!-- Item results -->
						<div class="hor-gird product-results text-center">
							<div class="row">
								<div class="col-xs-12 col-sm-6 col-md-4 col-lg-15 item-col" ng-class="item.issold == 1 ? 'item-unavailable' : ''"  ng-repeat="item in items">
									<div class="item-col-wrap">
										<!--button class="btn btn-md btn-add-to-wishlist" ng-class="item.inwishlist == 1 ? 'active block' : ''" data-toggle="tooltip" data-placement="top" data-original-title="{{item.inwishlist == 0 ? 'Add to Wishlist': 'Remove from Wishlist'}}" type="button" ng-click="item.inwishlist=item.inwishlist==1?0:1; addtowishlist(item, item.inwishlist)">
				                			<span class="icon pocketin-icon-heart"></span>
				                		</button-->
				                		<div class="badge stock-availability catview-unavailable" ng-show="item.issold == 1">Sold Out</div>
										<div class="badge stock-availability catview-available" ng-show="item.isfactory == 1 && item.issold != 1">Unused & New</div>
										<a class="" ng-href="product/searchresult/{{item.productitem_id}}/{{item.product_title_orig|toproducturl}}">
											<div class="poduct-grid-thumb">
												<img ng-src="assets/images/products/thumbnail/{{item.image}}" alt="{{item.imagealt}}" width="360" height="466">
											</div>
											<div class="poduct-grid-detail text-left">
												<div class="col-sm-12">
													<div class="item-text-info">
								                	<h1 class="product-name" ng-bind-html="item.product_title | sanitize">
									                </h1>
								                	<span class="product-description" ng-bind-html="item.product_description | sanitize">
								                	</span>
								                	</div>
									                <div class="row">
										                <div class="col-xs-6 inline">
										                	<div class="product-price"><span>&#x20B9;</span>{{item.saleprice}}</div>
										                </div>
										                <div class="col-xs-6 inline text-right">
										                	<div class="quality-index" ng-class="item.qualityindex >= 4 ? 'green-dark' : (item.qualityindex >=3 ? 'green-bright' : (item.qualityindex >=2 ? 'yellow' : (item.qualityindex< 2 ? 'red' : 'grey')))">
									                			<span class="icon pocketin-icon-quality-index"></span>{{item.qualityindex|number:1}}
									               			</div>
										                </div>
									                </div>
									                <div class="market-price"><span>&#x20B9;</span>{{item.mrp}}<small>Market Price</small></div>
									            </div>
											</div>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
