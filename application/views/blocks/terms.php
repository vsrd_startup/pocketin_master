<div class="main-container">
	<div class="about-us page-content">
		<div class="page-content-area">
			<div class="container about-page about-terms">
				<div class="sub-header col-xs-12 text-center">
					<div class="subtitle fancy">
						<span>
							<h4>Terms and Conditions</h4>
						</span>
					</div>
				</div>

				<div style="text-align: justify;">
					<p>
							Through the Refabd website www.refabd.com and via other means, VSRD online solutions Pvt Ltd.
							(doing business as "Refabd"), provides a platform for consumers to choose among the best quality
							used home appliances and furniture. The Refabd marketplace, platform, and related functionality, 
							whether provided through the site or through other means, are collectively referred to as the 
							services (the "Services"). By using the Site and Services, you agree to be bound by these 
							Terms & Conditions.
							<br>
							<font color="#47C9AF"><br><b>Free Delivery</b><br></font>
					</p>
							<ul>
								<li>We provide services only in Bangalore<br></li>
								<li>Delivery is free only for ground floor or with lift facility, in case of no lift extra Rs.100 is chargeable<br></li>
								<li>Additional charges will be levied, if the delivery is more than 20km from Refabd warehouse at H.S.R Layout.<br>
								
								</li>
							</ul>
					<p>
							<font color="#47C9AF"><br><b>Installation and Demo</b><br></font>
					</p>

							<ul>
								<!-- <li>Installation and demo will be done within 2 to 3 days after delivery<br></li> -->
								<li>Installation will be done at the time of  delivery<br></li>
								<!-- <li>Refrigerators are plug and play and it will be installed during the delivery<br></li> -->
								<li>Refrigerators need around 1 hour of settling time after transportation hence only installation will be done at time of delivery<br></li>
								<li>For Washing machine power switch and inlet water connection should be available. Plumbing and wiring work is not included as part of installation<br></li>
							</ul>

					<p>
							<font color="#47C9AF"><br><b>Payment</b><br></font>
							Full amount should be paid to the delivery person once the product is delivered<br>
							
							<font color="#47C9AF"><br><b>6 month warranty</b><br></font>
					</p>
							<ul>
								<li>6 month warranty covers only for manufacturing/material defects<br></li>
								<li>Warranty is not applicable to physical damages after delivery<br><br></li>
								Under the following circumstances, parts and labour will be chargeable even during the warranty period. The warranty DOES NOT extend to the following<br><br>
								<li>Damages due to cause beyond control like lightning, abnormal voltage, acts of God or while in transit to service centre or purchaser's residence<br></li>
								<li>Demonstration/installation of the product purchased<br></li>
								<li>Any accessories external to the system<br></li>
								<li>Cost of transportation of system from place of installation to the service centre<br></li>
								<li>Installation/repair work carried out by persons/agency other than authorised by the company<br></li>
								<li>Site (premises where the product is kept) conditions that do not confirm to the recommended operating conditions of the machine<br></li>
								<li>Appliance has undergone repairs, modifications, or dismantled by any other person<br></li>
								<li>Product not used as per the best practices<br></li>
							</ul>
					<!-- <p>
							<b>Warranty for product parts</b><br>
							Washing machine<br>
					</p>
							<ul>
								<li>Warranty included for Drum, Motor<br></li>
								<li>Warranty not included for controller board<br></li>
							</ul>
					<p>
							Refrigerator<br>
					</p>
							<ul>
								<li>Warranty included only for Compressor<br></li>
							</ul> -->
					<p> 
							<font color="#47C9AF"><br><b>7 Days Replacement Guarantee</b><br></font>
							Our goal is that you are completely satisfied with your purchase. We take pride in the quality of the products we 
							sell and offering great customer service is our top priority. Most merchandise can be returned within 7 days of 
							purchase unless noted in our Replacement condition. Please note that all the items need to be returned with all 
							components for a full replacement. If you need assistance with the return of items, or if you have any other 
							questions, please contact Online Customer Support at 70 226 30 270. Please note that, depending on the products, i
							t may take 2-10 business days for the item to get replaced from the start of the returns process.<br>	
							<b>Replacement conditions</b><br>
					</p>
							<ul>
								<li>For replacement, any damages/defects found in the product delivered should be reported within 48 hours after delivery<br></li>
								<li>If the defect/issue reported is not mentioned in the quality report, transportation charges will be free<br></li>
								<li>If the replacement is initiated in case the look of the product delivered is not as per the liking of the customer, transportation charges will be deducted from the replacement order. 
								Inorder to avoid such instances, while placing order if the product images are not sufficient, feel free to WhatsApp to the customer care number requesting for more images<br></li>
								<li>If the replacement is initiated due to the defect/issue reported already as part of quality report, transportation charges will be deducted from the replacement order.
								Inorder to avoid such instances, please check our quality rating and condition specification to understand the condition of the product better<br></li>
								<li>Replacement not acceptable in case the issue reported is due to the user's ignorance in using the product<br></li>
							</ul>
					<p>
							<font color="#47C9AF"><br><b>On the Spot Return</b><br></font>
					</p>
							<ul>
							<li>If the appearance of the product is not as per the expectation, the product can be returned back at the time of delivery at no extra cost.
							</li>
								<!-- <li>If in case you find any defect (which is not covered in our quality report) in the product you can 
								return the product immediately during the delivery<br></li>
								<li>Or you can initiate return within 24 hours of delivery<br></li>
								<li>In case the return is initiated due to the appearance of the product not as per the liking, 
								transportation charges will be levied. 	</li> -->
							</ul>

					<p>
							<font color="#47C9AF"><br><b>Buy Back and Upgrade</b><br></font>
					</p>
							<ul>
								<li>If you are upgrading your product by selling your old product back to Refabd and then buying another product from Refabd within 6 months, you will get an offer of <span style="font-weight:bold">80%</span> of buying price of the old product. Fixed charge of Rs.500 will be charged for reverse pickup.<br></li>
								<li>If you are upgrading your product by selling your old product back to Refabd and then buying another product from Refabd within 1 year, you will get an offer of <span style="font-weight:bold">70%</span> of buying price of the old product. Fixed charge of Rs.500 will be charged for reverse pickup.<br></li>
								<li>If you are selling your old product back to Refabd within 6 months, you will get an offer of <span style="font-weight:bold">70%</span> of buying price of the old product. Fixed charge of Rs.500 will be charged for reverse pickup <br></li>
								<li>If you are selling your old product back to Refabd within 1 year, you will get an offer of <span style="font-weight:bold">60%</span> of buying price of the old product. Fixed charge of Rs.500 will be charged for reverse pickup.<br></li>
								<li>If you want to sell your old product back to Refabd any time after 1 year, contact customer care for the best deal.<br></li>
								<li>Note: Buy back of the products is sole discretion of the company and  is subjected to demand and stock</li>
								<label style="text-decoration: underline"><b>Buy Back of Brand New Products</b></label>
								<li>If you are selling back within a year and purchasing another brand new product of higher price from Refabd, you will get <span style="font-weight:bold">70%</span> of the buying price</li>
								<li>If you are selling back within a year and not buying another product from Refabd, you will get <span style="font-weight:bold">60%</span> of the buying price</li>
								<li>If you are selling back within 2 year and and purchasing another brand new product of higher price from Refabd, you will get <span style="font-weight:bold">45%</span> of the buying price</li>
								<li>If you are selling back within 2 year and not buying another product from Refabd, you will get <span style="font-weight:bold">35%</span> of the buying price</li>
								<li>If you are selling back after 2 years fixed percentage will be given after product inspection</li>
								<li>Note: Buy back of the products is sole discretion of the company and  is subjected to demand and stock</li>
							</ul>
				</div>
			</div>
		</div>
	</div>
</div>
