<div class="main-container sales-page-detials">
	<div class="sales-page page-content">
		<div class="page-content-area">
			<div class="container">
				<div class="sub-header col-xs-12 text-center">
	                <div class="subtitle"><span><h1 style="text-transform: uppercase;color: grey;font-weight: 100; font-size: 18px">Why wait, when you can sell and upgrade easily with Refabd?</h1></span></div>
				</div>
					<div class="row" style="border: 1px solid rgba(158, 158, 158, 0.46);border-top:0; background: white">
					   <div class="product-details-section col-lg-9 col-xs-12 col-sm-12 col-md-12">
					   <div class="services text-center">
							<div class="row">
								<div class="col-xs-12 col-sm-4">
									<!--span class="icon pocketin-icon-bag huge"></span-->
									<img src="assets/images/sell/sell.svg" alt="Sell" style="max-width: 60px;">
									<label>Hassle free Selling</label>
									<p>Fill in the details and upload pictures of the product and stay cool until we process and call you back!!</p>
								</div>
								<div class="col-xs-12 col-sm-4">
									<!--span class="icon pocketin-icon-Timer huge"></span-->
									<img src="assets/images/sell/time.svg" alt="Sell" style="max-width: 60px;">
									<label>Sell in maximum 3 days</label>
									<p>Our representative will contact you with true value of the product which, we are sure, won't disappoint you!</p>
								</div>
								<div class="col-xs-12 col-sm-4">
									<!--span class="icon pocketin-icon-Front-load huge"></span-->
									<img src="assets/images/sell/exchange.svg" alt="Sell" style="max-width: 60px;">
									<label>Additional Offer on Exchange</label>
									<p>Selling to upgrade? Upgrade with us and get additional discount on your purchase at Refabd</p>
								</div>
							</div>
						</div>

					   <hr/>
				         <div class="sub-heading">
				         	<h4>Fill up the product deatils</h4>
				         	<p>Give us proper and prompt answers to give you the best value for your product.</p>
				         </div>
					      <div class="product-deatils">
					         <form id="sellform" name="sellform" novalidate ng-submit="sellform.$valid && completedupload == 1 && imageadded == 1 && sellProduct()">
					         	<div class="row">
									<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
										<label for="">Main Category <span>*</span></label>
										<select class="form-control" required ng-model="product.maincat" ng-init="product.maincat='appliances'" ng-change="setSubCategory()">
											<option value="appliances">Home Appliance</option>
											<option value="furniture">Furniture</option>
										</select>
									</div>
									<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
										<label for="">Sub Category <span>*</span></label>
										<select class="form-control" required ng-model="product.subcat">
											<option value="fridge" ng-show="product.maincat == 'appliances'">Refrigerator</option>
											<option value="washing" ng-show="product.maincat == 'appliances'">Washing Machine</option>
											<option value="bed" ng-show="product.maincat == 'furniture'">Bed</option>
											<option value="dining" ng-show="product.maincat == 'furniture'">Dining Table</option>
											<option value="sofa" ng-show="product.maincat == 'furniture'">Sofa Set</option>
											<option value="other">Other</option>
										</select>
									</div>
								</div>
								<div class="row">
									<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
										<label for="">Product Name <span>*</span></label>
										<input type="text" placeholder="Enter product name" class="form-control" ng-model="product.productname" required>
									</div>
									<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
										<label for="">Product Brand <span>*</span></label>
										<select class="form-control" required ng-model="product.productbrand" ng-init="product.productbrand='lg'">
											<option value="lg">LG</option>
											<option value="samsung">Samsung</option>
											<option value="whirlpool">Whirlpool</option>
											<option value="godrej">Godrej</option>
											<option value="ifb">IFB</option>
											<option value="other">Other</option>
										</select>
									</div>
								</div>
								<div class="row">
									<div class="form-group col-lg-6 desc-text-div">
									<label for="">Product Type<span></span><span>*</span></label>
									<input type="text" placeholder="singledoor/semiautomatic/queensize/4seater/..." class="form-control" ng-model="product.producttype" required>
									</div>
									<div class="form-group col-lg-6 desc-text-div">
									<label for="">Product Model Number</label>
									<input type="text" placeholder="Enter product model" class="form-control" ng-model="product.productmodel">
									</div>
								</div>
								<br><br><br>
								<h4 style="color: #363636; font-weight: 300;">Quality Rating Indices</h4>
								<hr>
								<div class="row product-type">
									<div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-3">
										<label for="">Age of the product <span>*</span></label>
										<select class="form-control" required ng-model="product.age" ng-init="product.age='lessthanone'">
											<option value="lessthanone">Less than 1 year old</option>
											<option value="one">1 year old</option>
											<option value="two">2 year old</option>
											<option value="three">3 year old</option>
											<option value="four">4 year old</option>
											<option value="abovefive">Above 5 year</option>
										</select>
									</div>
									<div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-3" ng-show="product.maincat=='appliances'">
										<label for="">Working Condition<span>*</span></label>
										<select class="form-control" required ng-model="product.workingcondition" ng-init="product.workingcondition='excellent'">
											<option value="excellent">Excellent working condition</option>
											<option value="partial">Partial working condition</option>
											<option value="notworking">Not in working condition</option>
										</select>
									</div>
									<div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-3">
										<label for="">External Appearance <span>*</span></label>
										<select class="form-control" required ng-model="product.appearance" ng-init="product.appearance='likenew'">
											<option value="likenew">Looks new without any scratches</option>
											<option value="good">Looks good with minor scratches</option>
											<option value="decent">Looks decent with some dents</option>
											<option value="average">Looks average with some rust/cracks</option>
											<option value="belowaverage">Below average looks</option>
										</select>
									</div>
									<div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-3">
										<label for="">Internal Appearance <span>*</span></label>
										<select class="form-control" required ng-model="product.internal" ng-init="product.internal='intact'">
											<option value="intact">All parts intact</option>
											<option value="minorcrack">Minor cracks on parts</option>
											<option value="majorcracks">Major cracks on parts</option>
											<option value="partsmissing">Some parts missing</option>
										</select>
									</div>
								</div>
								<br><br><br>
								<h4 style="color: #363636; font-weight: 300;">Product Description</h4>
								<hr>
								<div class="row">
									<div class="form-group col-lg-6 desc-text-div">
										<label>Description of the Product <span>*</span></label>
					                    <div class="form-group prod-desc">
					                    	<textarea name="description" placeholder="Product description" class="form-control product-desc" rows="9" ng-model="product.productdescription" required></textarea>
					                    </div>
					                </div>
					                <div class="form-group col-lg-6 desc-text-div">
						                <label>Tick whichever Applicable</label>
						                <div style="border: 1px solid #E6E6E6 ; padding-left: 30px; padding-bottom: 10px;">
							                <div class="checkbox">
							                  <label><input type="checkbox" name="warranty" value="warranty" ng-model="product.underwarranty"> Under Manufacturer Warranty</label>
							                </div>
							                <div class="checkbox">
							                  <label><input type="checkbox" name="bill" value="bill" ng-model="product.billavailable"> Original bill available</label>
							                </div>
							                <div class="checkbox">
							                  <label><input type="checkbox" name="box" value="box" ng-model="product.boxavailable"> Original box available</label>
							                </div>
						                </div>
					                </div>
								</div>
								<br><br><br>
								<h4 style="color: #363636; font-weight: 300">Product Images</h4>
								<hr>
								<ul>
								<li style="font-size:13px;font-weight:300"><span>*</span>Please upload clear and visible images of the product from all sides</li><br>
								<li style="font-size:13px;font-weight:300"><span>*</span>Please make sure that single image size is less than 2MB</li><br>
								<li style="font-size:13px;font-weight:300"><span>*</span>Problem uploading images? Contact us at 70-226-30-270</li>
								</ul><br><br>
								<div class="drop"  flow-object="sellFlowObj" flow-init
									flow-file-added="initupload($file);"
									flow-files-submitted="uploadimage(sellFlowObj)"
									flow-error="uploadfailed($file)"
									flow-complete="uploadcomplete()">
									<div flow-drop flow-drag-enter="style={border:'1px solid grey'}" flow-drag-leave="style={}" ng-style="style">
										<span class="btn btn-default" flow-btn>Select Images</span>
										<span class="btn btn-default" flow-btn flow-directory ng-show="$flow.supportDirectory">Select Folder of Images</span> <b>OR</b>
										Drag And Drop images/folder here
									</div>
									<br/>
									<div ng-repeat="file in $flow.files" class="inline gallery-box" ng-class="(file.error == true || file.removefailed == 1) ? 'error': '' " ng-init="file.success = false">
										<div class="thumbnail" ng-show="$flow.files.length">
											<img flow-img="file" />
										</div>
										<div class="progress progress-striped" ng-class="{active: file.isUploading()}">
											<div class="progress">
												<div class="progress-bar"  ng-class="file.error == true ? 'progresserror': '' " role="progressbar"
												aria-valuenow="{{file.progress() * 100}}"
												aria-valuemin="0"
												aria-valuemax="100"
												ng-style="{width: (file.progress() * 100) + '%'}">
												<span class="sr-only">{{file.progress()}}%</span>
												<p ng-show="file.error == true" style="color:white;padding:3px;">Failed</p>
												</div>
											</div>
										</div>
										<!--div class="btn-group">
											<a class="btn btn-xs btn-danger" ng-click="removeimage(file)">Remove</a>
											<label ng-show="file.removefailed == 1" style="color: red">Failed to remove</label>
										</div-->
									</div>
								</div>
								<div class="clear"></div>
								<!--div class="row data-collect">
									<div class="form-group  col-lg-12 image-section">
										<div class="images-list-main">
											<ul class="seller_prod_images_list">
												<li class="img-content" data-index="0">
													<div class="text-center"><span class="icon pocketin-icon-plus huge"></span></div>
													<div class="text-center" id="txt_addImages" style="font-size: 11px">UPLOAD</div>
													<input type="file" multiple="multiple" onchange="handleFilesModified(this)" accept="image/x-png, image/jpeg" style="display: none">
												</li>
											</ul>
										</div>
									</div>
								</div-->

								<div class="text-center">
							      	<div class="submit-product-btn">
							      		<button type="submit" class="btn btn-submit" ng-disabled="selling == 1">Sell Product</button>
							      	</div>
							      	<label style="color:#26b99a;font-size:13px; font-weight: 500;" ng-show="sold == 1">Product info uploaded successfully. Scroll up to sell another product</label>
							      	<label style="color:darkorange;font-size:13px; font-weight: 500;"
							      	ng-show="sellform.$submitted && !sellform.$valid">Please give answers to all mandatory fields. Scroll up and look for red underlines</label>
							      	<label style="color:darkorange;font-size:13px; font-weight: 500;"
							      	ng-show="sellform.$submitted && sellform.$valid && imageadded == 0">Scroll up and add atleast one product image!</label>
							      	<label style="color:darkorange;font-size:13px; font-weight: 500;"
							      	ng-show="sellform.$submitted &&  sellform.$valid && imageadded != 0 && completedupload == false">Please wait while the images are uploaded!</label>
							      	<br><br><label style="color:#666666; font-size:13px; font-weight: 300;"><span style="color:grey">*</span>We don't share your contact details, bill and other info with buyers.</label>
					      		</div>
					         </form>
					      </div>
					   </div>
					   <div class="side-panel col-lg-3 col-xs-12 col-sm-12 col-md-12">
					   	<div class="sidepanel-content">
					   		<h4>Selling is easy with Refabd</h4>
					   		<p>We come to your place, Inpect, clean and pack the product at your convenient time. Cash is paid at the time of pickup</p>
					   	</div>
					   	<div class="sidepanel-content">
					   		<h4>What happens to product</h4>
					   		<p>Product undergoes thorough cleaning, disinfection and quality check. QC report is generated and later finds new hands</p>
					   	</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<a id="selltrigger" data-toggle="modal" href='' data-target="#sellmodal" ng-hide="1">Success</a>
	<a id="sellsuccesstrigger" data-toggle="modal" href='' data-target="#sellsuccessmodal" ng-hide="1">Success</a>
	<div class="modal fade" id="sellmodal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">{{sell.title}}</h4>
				</div>
				<div class="modal-body">
					<h6>{{sell.msg1}}</h6>
					<br/>
					<h6>{{sell.msg2}}</h6>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="sellsuccessmodal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" style="color: #666666">Congratulations</h4>
				</div>
				<div class="modal-body">
					<p style="font-size: 14px; font-weight: 300; color: #666666">Your sell request <span style="font-size: 14px; font-weight: 400;">{{sellid}}</span> is submitted for review and you will get a call in some time</p>
					<p style="font-size: 14px; font-weight: 300; color: #666666">You can check the status of the request at <a class="btn btn-xs btn-my-orders" style="padding: 5px;" href="mysellrequests">Sell Requests</a></p>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
</div>