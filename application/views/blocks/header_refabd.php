<style>
@media (max-width: 500px) {
	.bt-search {
        padding: 8px!important;
    }
    .navbar-right .dropdown-menu {
        left: 10px;
        right: auto;
        top: 40px;
        margin: 0;
        padding: 0;
    }
    #rightmenuid {
    	overflow: visible!important;
    }
    .re_icon {
    	display: block!important;
    	width: 60px;
    	max-width: 60px;
    	margin-top: 6px;
    	margin-left: 4px;
    }
    .refabd_logo {
    	display: none;
    }
    .addboxinmobile {
    	 box-shadow: 0 2px 2px rgba(0,0,0,.15);
    	 padding-bottom:8px;
    	 z-index:2;
    }
    .main_menu {
    	display:none;
    }
}
.offer {
	background: #c7d12c!important;
}
.menuheader {
	margin-bottom:2px!important;
}
.menuheader .navbar-toggle {
	background: #c7d12c!important;
}
.menuheader .navbar-toggle .caret {
	color: white;
	size: 42px;
}

.notification .circle {
    top: 4px!important;
    right: -11px!important;

}
.circle {
	background:#3fa89a!important;
}

.coupon-block {
    /*background-color: #DDE96E;*/
    background-color: #e6d265;
    color: #000;
    text-align: center;
    font-weight: 500;
    font-size: 0.9em;
    padding: 7px;
}
.coupon-block a.close {
    font-weight: 800;
    font-size: 21px;
    color: #000;
    /* float: right; */
    border: 0;
    position: absolute;
    right: 10px;
    top: 5px;
}

.rightmenu a:not(.ignore) {
    color: #666666;
    padding: 7px 10px!important;
    font-size: 11px;
    display: inline-block;
    border-bottom: 1px solid transparent;
    border:0;
    border-radius: 3px;
}
.rightmenu a:hover:not(.sell):not(.ignore) {
	text-decoration: underline;
    /*background-color: #c7d12c
    background-color: #8594c8!important;
    color:#fff;
    border-bottom: 1px solid #c7d12c;*/
}

.rightmenu a.sell {
	background-color: #3c7db6!important;
	/*background-color: #144483!important;*/
    color:#fff;
    margin-right:5px;
}
.location a {
	color: #555555;
	padding-top: 5px!important;
	font-weight:600!important;
}
.location a:hover{
	/*background-color: #8594c8!important;
    color: white!important;*/
}

.refabd_logo {
	width: 115px;
	max-width: 115px;
    /* width: 24%; */
    height: auto;
    padding: 10px;
}

.sell-product{
/*background: #d70056;*/
padding: 10px 10px;
padding-top: 22px;
}

.sell-product >a {
background: #323F88;
border-radius: 4px;
border-color: #1d2c7f;
font-size: 14px;
font-weight: 200;
color: #fff;
padding: 10px;
}

.phone {
	font-weight: 400;
	font-size:14px;
}

.searchbox {
	padding-top: 5x;
}

header {
	background-color: white;
}

.main_menu {
	border-bottom: 1px solid #eae9e9;
    padding: 2px;
    border-top: 1px solid #eae9e9;
    z-index:12;
}

.main_menu a {
	border: 0;
    background-color: transparent;
    display: block;
    color: #000;
    position: relative;
    font-weight: 300;
    font-family: 'Open Sans', sans-serif;
    font-style: normal;
    font-size: 13px;
    padding: 6px 0 6px;
    margin-right: 35px;
    border-bottom: 1px solid transparent;
}
.main_menu a:hover {
	border-bottom: 1px solid #69b4df;
}
.circlemobile {
	position:absolute!important;
	top: -9px!important;
	left: 16px!important;
}
.sidemenuitem {
	font-family:"Raleway", "Open Sans";
	display: block;
    padding: 15px 18px;
    border-bottom: 1px solid #cecbcb;
	text-transform: uppercase;
	font-weight: 900;
	font-size:13px;
}
.sidemenuitem a {
	color: #7e2b6a;
}
.sidemenuitem a:hover:visited:focus {
	color: #7e2b6a;
}
[sidebarjs].sidebarjs--is-visible [sidebarjs-container] {
	background: #f2f2f2;
}
</style>
<header style="background-color: #f5f5f5;">
	<!-- side bar menu for mobile -->
	<sidebarjs sidebarjs-config="{position: 'right'}"  class="sidebarjs">
	    <nav>
	    	<div class="sidemenuitem"><a href="https://www.refabd.com">HOME</a></div>
	    	<div class="sidemenuitem"><a href="category/3/refrigerators">Refrigerator</a></div>
		    <div class="sidemenuitem"><a href="category/4/washing-machine">Washing Machine</a></div>
			<!--div class="sidemenuitem"><a href="category/11/television">Television</a></div>
			<div class="sidemenuitem"><a href="category/14/microwave-oven">Microwave Oven</a></div-->
			<div class="sidemenuitem"><a href="category/5/air_conditioner">Split AC</a></div>
			<div class="sidemenuitem"><a href="category/10/bed">Bed</a></div>
			<div class="sidemenuitem"><a href="category/7/sofa">Sofa Set</a></div>
			<div class="sidemenuitem"><a href="category/17/dining">Dining Set</a></div>
			<div class="sidemenuitem"><a href="category/8/office-chair">Office Chair</a></div>
			<div class="sidemenuitem"><a href="category/13/shoe-rack">Shoe Rack</a></div>
			<div class="sidemenuitem"><a href="category/15/tv-unit">TV Unit</a></div>
			<div class="sidemenuitem"><a href="category/16/miscellaneous">Coffee Table</a></div>
			<div class="sidemenuitem"><a href="category/16/miscellaneous">Storage/Tables</a></div>
			<!--div class="sidemenuitem"><a href="category/16/miscellaneous">Centre Table</a></div-->
			<div class="sidemenuitem"><a href="category/16/miscellaneous">Book Shelf</a></div>
			<!--div class="sidemenuitem"><a href="category/16/miscellaneous">Bar Unit</a></div-->
			<div class="sidemenuitem"><a href="collection">View Collection</a></div>
	    </nav>
	</sidebarjs>

	<section class="coupon-block" style="font-size: 14px;" ng-show="showcoupon" ng-init="showcoupon=1">
		<div class="container" style='font-family: "Trebuchet MS", "verdana;"'>
			Happy Shopping! Use code <b>RBD005</b>
			at checkout to get extra<b> 5% off</b>
			<a href="" class="close" ng-click="showcoupon=0">x</a>
		</div>
	</section>
	<section class="menuheader navbar">
		<div class="container">
			<div class="navbar-header addboxinmobile">
				<!-- caret for mobile -->
				<!--button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#rightmenuid">
					<span class="caret"></span>
				</button-->
				<div onclick="$('#search_box').val('');">
					<a href="https://www.refabd.com">
						<img class="refabd_logo" src="assets/images/logo.png" width="115px">
						<img class="re_icon" src="assets/images/re_icon.png" width="115px" style="display: none">
					</a>
				</div>
				<div ng-controller="CartCountController as ctrl" class="ignore hidden-lg hidden-md hidden-sm" style="position: absolute;right: 67px;top: 11px;">
					<a href="https://www.refabd.com/cart">
						<span class="circle circlemobile" ng-show="ctrl.getCartCount() >0">{{ctrl.getCartCount()}}</span>
						<span class="icon-cart" style="font-size:29px;font-weight:500"></span>
					</a>
				</div>
				<a sidebarjs-toggle style="position: absolute;right: 13px;top: 11px;" class="visible-xs"><span class="glyphicon glyphicon-th-list" style="font-size: 26px;color: #5f5f5f;"></span></a>
				<span class="visible-xs" style="position: absolute;right: 110px;top: 12px;font-size: 26px;color: #666666;"><a data-toggle="modal" data-target="#signupform" href="" data-backdrop="static" data-keyboard="false"><span class="glyphicon glyphicon-user"></span></a></span>
				<span class="visible-xs" style="position: absolute;right: 154px;top: 15px;font-size: 24px;"><a href="tel:+917022630100"><span class="glyphicon glyphicon-earphone"></span></a></span>
				<!-- cart ends here-->
			</div>
			<div class="rightmenu collapse navbar-collapse" id="rightmenuid">
				<ul class="nav navbar-nav navbar-right headernav">
					<li>
						<a href="sell" class="sell">SELL ON REFABD</a>
					</li>
					<li>
						<a href="" ng-click="hidewidget = 0" class="hidden-sm hidden-xs">OFFERS</a>
					</li>
					<li>
						<a href="mailto:hello@refabd.com?subject=Bulk Purchase" class="hidden-sm hidden-xs">BUY IN BULK</a>
					</li>
					<li>
						<a href="myorders" class="hidden-sm hidden-xs">TRACK YOUR ORDER</a>
					</li>
					<li>
						<a href="mywishlist" class="hidden-sm hidden-xs">WISHLIST</a>
					</li>
					<li>
						<a data-toggle="modal" data-target="#signupform" href="" data-backdrop="static" data-keyboard="false">REGISTER</a>
					</li>
					<li class="location">
						<a href="" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown" data-delay="0" role="button" aria-haspopup="true" aria-expanded="false"> <i class="glyphicon glyphicon-map-marker"></i>
							BANGALORE
						</a>
					</li>
				</ul>
				<ul class="nav navbar-nav navbar-right headernav">
					<li ng-controller="SearchController as ctrl" style="display:inline;width:380px">
						<form class="input-group" ng-submit="displaySearchResult()" style="padding-top:10px!important; padding-left:0!important">
							<div class="input-group-btn search-panel">
								<button type="button" class="btn btn-category  dropdown-toggle" data-toggle="dropdown">
									<span id="search_concept">{{search_focus}}</span>
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu" role="menu">
									<li ng-repeat="cat in cats">
										<a href="" ng-click="setSearchFocus(cat)">{{cat.name}}</a>
									</li>
									<li class="divider"></li>
									<li>
										<a href="" ng-click="setSearchFocus({name: 'All Items', id:'0'})">All Items</a>
									</li>
								</ul>
							</div>
							<input type="text" class="form-control" name="x" id="search_box" placeholder="Search an Item" ng-model="query">
							<span class="input-group-btn">
								<a class="btn bt-search ignore" ng-href="search/{{selected_cat_id}}/{{query}}">
									<span class="glyphicon glyphicon-search"></span>
								</a>
							</span>
							<button type="submit" class="hide"></button>
						</form>
					</li>
					<!-- search li ends-->

					<!-- notification li-->
					<li class="notification" ng-controller="NotificationController as ctrl">
						<div class="dropdown">
							<a class="dropdown-toggle ignore" type="button" data-toggle="dropdown" ng-click="ctrl.seenNotification()" style="padding-top:15px;paddig-left:15px">
								<span class="circle" ng-class="notclass" ng-show="ctrl.getCount() >0">{{ctrl.getCount()}}</span>
								<span class="icon-notification" style="font-size:21px;"></span>
							</a>
							<div class="dropdown-menu">
								<ul class="notification-list">
									<li ng-repeat="not in ctrl.getNots()">
										<a href="notifications">
											<div class="notification-read">
												<div class="row">
													<div class="col-xs-3">
														<div class="notification-icon">
															<span class="pocketin-icon-Info"></span>
														</div>
													</div>
													<div class="col-xs-9 notification-text">
														<div class="">
															<p>
																Your Item,
																<b>{{not.product}}</b>
																from Order:
																<b>PI:{{not.order}}</b>
																has been {{not.type}}.
															</p>
														</div>
														<div class="notification-timestamp">{{not.date}}</div>
													</div>
												</div>
											</div>
										</a>
									</li>
									<li ng-show="ctrl.getCount() == 0">
										<div class="notification-read">
											<div class="row">
												<div class="col-xs-9 notification-text">
													<p style="padding-top: 10px; padding-bottom:10px; padding-left: 50%">No Notifications</p>
												</div>
											</div>
										</div>
									</li>
									<hr>
									<li>
										<div class="notification-read">
											<div class="notification-text">
												<a href="notifications" style="padding-left: 40%; padding-top: 10px; padding-bottom: 10px;">
													<p>View All</p>
												</a>
											</div>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</li>
					<!-- notification li ends -->
					<li ng-controller="CartCountController as ctrl" class="cart" style="padding-left:2px">
						<a href="https://www.refabd.com/cart" class="ignore">
							<span class="circle" ng-show="ctrl.getCartCount() >0">{{ctrl.getCartCount()}}</span>
							<span class="icon-cart" style="font-size:20px;font-weight:500"></span>
						</a>
					</li>
					<!-- cart ends here-->
					<li class="login" ng-controller="LoginSignupController as ctrl" ng-init="inited = 0" style="padding-top:14px">
						<!-- login dropdown -->
						<div class="dropdown" ng-hide="ctrl.userLogged()">
							<a class="dropdown-toggle" type="button" data-toggle="dropdown"  href="" style="font-size:13px;">Login</a>
							<section class="dropdown-menu">
								<div class="login-from">
									<label>Login</label>
									<form method="post" name="loginform" ng-submit="loginform.$valid && ctrl.login()" novalidate>
										<div class="row">
											<div class="col-xs-12">
												<input class="form-control" type="text" name="login" value="" placeholder="Phone Number or Email" ng-model="user.username" ng-disabled="!canedit" required></div>
											<div class="col-xs-12">
												<input class="form-control" type="password" name="password" value="" placeholder="Password" ng-model="user.password" required ng-disabled="!canedit"></div>
											<div class="col-xs-4 submit">
												<input class="btn btn-login" type="submit" name="commit" value="Login" ng-disabled="!canedit"></div>
											<div class="col-xs-8 login-help">
												<a href="" data-toggle="modal" data-target="#forgotpwdmodal" data-backdrop="static">Forgot password?</a>
											</div>
											<div class="col-xs-12">
												<p class="validationerror">{{message}}</p>
											</div>
										</div>
										<a href="" style="padding: 10px;" ng-click="getotp(user.username)" ng-show="requestotp">Resend OTP?</a>
									</form>
									<div ng-show="requestotp">
										<form ng-submit="verifyotpandlogin()">
											<div class="row" ng-show="showotp">
												<div class="form-group col-xs-8" style="padding: 0 7px 0 15px;">
													<input type="number" class="form-control" placeholder="Enter OTP"  ng-model="otp"></div>
												<div class="form-group col-xs-4">
													<button type="submit" class="btn btn-default">Verify</button>
												</div>
											</div>
										</form>
									</div>
								</div>
								<hr/>
								<div class="col-xs-12 newuserdiv">
									<a class="btn btn-sign-up" data-toggle="modal" data-target="#signupform" href="" data-backdrop="static" data-keyboard="false">
										<span>New User? Sign Up here.</span>
									</a>
								</div>
								<div class="col-xs-12 text-center sign-up-offers" style="padding: 10px;">
									<label>
										Register now and get extra
										<b>6% OFF</b>
										on your purchase
									</label>
								</div>
							</section>
						</div>
						<!-- user dropdown -->
						<div class="dropdown" ng-show="ctrl.userLogged()">
							<!--p style="font-size: 10px; position:absolute; width: 80px;">Click to Manage</p-->
						<a class="user-log-name dropdown-toggle" type="button" data-toggle="dropdown"  href="" id="useraction">
							{{ctrl.getLoggedUser()}}
							<span class="glyphicon glyphicon-triangle-bottom"></span>
						</a>

						<ul class="dropdown-menu" id="useractiondiv">
							<li>
								<a href="myaccount" class="ignore">My Account</a>
							</li>
							<li>
								<a href="myorders" class="ignore">My Orders</a>
							</li>
							<li>
								<a href="mysellrequests" class="ignore">Sell Requests</a>
							</li>
							<li>
								<a href="." ng-click="ctrl.logout()" class="ignore">Logout</a>
							</li>
						</ul>
					</div>
				</li>
				<!--login ends-->
			</ul>
		</div>
	</div>
</section>
<div class="main_menu" ng-init="apphover=0;furhover=0">
	<div class="container">
		<ul class="list-inline" style="text-align: center">
			<li class="col-lg-2 col-md-2 col-sm-4 col-xs-4" ng-mouseover="apphover=1;furhover=0">
				<a href="appliances">APPLIANCES</a>
			</li>
			<li class="col-lg-2 col-md-2 col-sm-4 col-xs-4" ng-mouseover="furhover=1;apphover=0">
				<a href="furniture">FURNITURE</a>
			</li>
			<li class="col-lg-2 col-md-2 col-sm-4 col-xs-4" ng-mouseover="furhover=0;apphover=0">
				<a href="category/12/laptop">LAPTOP</a>
			</li>
			<li class="col-lg-2 col-md-2 hidden-sm hidden-xs" ng-mouseover="furhover=0;apphover=0">
				<a href="category/16/Miscellaneous">MISCELLANEOUS</a>
			</li>
			<!--li>
			<a href="">NEW ARRIVALS</a>
		</li-->
	</ul>
</div>
</div>
<style>
	.sub_menu {
		border: 1px solid #b9b8b8;
    width: 45%;
    margin-left: 12%;
    border-top: 0;
    padding: 10px;
    position: absolute;
    z-index: 10;
    background: #f1f1f1;
    border-radius: 3px;
    border-top-left-radius: 0;
    border-top-right-radius: 0;
	}
	.sub_menu li {
		font-weight: 300;
	    font-style: normal;
	    font-size: 14px;
		color:#383838;
		padding-bottom:28px;
	}
	.sub_menu li a:hover {
		color: ##934497;
	}
</style>
<div class="sub_menu" id="appliances" ng-show="apphover" ng-mouseleave="apphover=0;furhover=0">
	<ul>
		<li class="col-lg-4"><a href="category/3/refrigerators">Refrigerator</a></li>
		<li class="col-lg-4"><a href="category/4/washing-machine">Washing Machine</a></li>
		<li class="col-lg-4"><a href="category/11/television">Television</a></li>
		<li class="col-lg-4"><a href="category/14/microwave-oven">Microwave Oven</a></li>
		<li class="col-lg-4"><a href="category/5/air_conditioner">Split AC</a></li>
	</ul>
</div>
<div class="sub_menu" id="appliances" ng-show="furhover" ng-mouseleave="apphover=0;furhover=0">
<div>
	<ul>
		<li class="col-lg-4"><a href="category/10/bed">Bed</a></li>
		<li class="col-lg-4"><a href="category/7/sofa">Sofa Set</a></li>
		<li class="col-lg-4"><a href="category/17/dining">Dining Set</a></li>
		<li class="col-lg-4"><a href="category/8/office-chair">Office Chair</a></li>
		<li class="col-lg-4"><a href="category/16/miscellaneous">Study Table</a></li>
		<li class="col-lg-4"><a href="category/13/shoe-rack">Shoe Rack</a></li>
		<li class="col-lg-4"><a href="category/15/tv-unit">TV Unit</a></li>
		<li class="col-lg-4"><a href="category/16/miscellaneous">Coffee Table</a></li>
		<li class="col-lg-4"><a href="category/16/miscellaneous">Centre Table</a></li>
		<li class="col-lg-4"><a href="category/16/miscellaneous">Book Shelf</a></li>
		<li class="col-lg-4"><a href="category/16/miscellaneous">Bar Unit</a></li>
		<li class="col-lg-4"><a href="category/16/miscellaneous">Small Furniture</a></li>
	</ul>
</div>
</div>

<toast></toast>
</header>

<a id="cbtrigger" class="btn btn-primary" data-toggle="modal" data-target='#cbmodaltrigger' style="display: none">trigger</a>
<div class="modal fade" id="cbmodal" ng-controller="TimedModalctrl">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background: #4d84a5; color: white">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<span style="font-weight: 100;font-size: 16px;">Need Help in Selecting Products ?</span>
			</div>
			<div class="modal-body" style="background: #f2faff">
			<span style="font-weight: 100;font-size: 16px;">Request a Callback</span><br><br><br>
				<form name="cbf" role="form" ng-submit="cbf.$valid && reqCallback(cbnumber)" novalidate>
					<div class="form-group">
						<input type="text" class="form-control" placeholder="Enter Contact Number"  ng-model="cbnumber" style="background: transparent !important;border-color:transparent !important;border-bottom: 1px solid #70b0d6 !important;margin-top: -11px;padding-left: 0;font-size: 14px;width: 51%;display:inline" required>
						<button type="submit" style="display: inline;padding: 10px;background: transparent;border: 1px solid #70b0d6;border-radius: 5px;margin-left: 20px;position: absolute;margin-top: -12px;font-size: 14px;">Submit</button>
					</div>
					<label style="font-size: 12px;margin-top: 5px;color:#757575;line-height: 21px;">You can alternatively mail us at hello@refabd.com or call us at <a href="tel:+917022630100" style="font-weight: 600;">7022630100</a></label>
				</form>
			</div>
			<button type="button" id="closecbmodal" class="btn btn-cancel" data-dismiss="modal" ng-hide="1">Cancel</button>
		</div>
	</div>
</div>

<div ng-init="hidewidget = 1" class="offer" ng-hide="hidewidget == 1">
<button type="button" class="close" data-dismiss="modal" ng-click="hidewidget = 1">×</button>
<h3 class="offer-header">Offers</h3>
<p class="offer-line">
First Time User? Get
<b>5% off</b>
with code RBD005
</p>
<br>
<p class="offer-line">
Refer and get
<b>₹100</b>
on each
<a style="color:#0d1f38; text-decoration: underline" href="myreferral">referral</a>
</p>
<br>
<p class="offer-line">
Get
<b>5% off</b>
on entire cart value on purchase of more than one product
</p>
<!--p class="offer-line">
Extra 5% offer on purchase above
<span>&#x20B9;</span>
8,000
</p-->
<!--p class="offer-line">
Extra 5% offer on purchase above
<span>&#x20B9;</span>
10,000
</p-->
<p class="offer-condition">
<span>*</span>
Limited period offer
</p>
</div>
<!-- Modal -->
<div id="signupform" class="modal fade" role="dialog" ng-controller="LoginSignupController as ctrl" ng-init="showlogin=0">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content" id="signupmodal">
<div class="text-left sign-up-offers" style="padding: 10px; padding-top: 0;">
<button ng-show="1" id="regmodalclose" type="button" class="close" data-dismiss="modal">&times;</button>
<label>
	Register now and get extra
	<b>5% OFF</b>
	on first purchase with code
	<b>RBD005</b>
</label>
</div>
<div class="modal-header">
<!-- <h5 class="modal-title" style="color: darkorange">Register now and get 10% OFF on your purchases!!</h5>
-->
<p ng-show="showlogin == 0">
Already Registered? -
<a href="" style="color:#d61b5e; text-decoration: underline; font-weight: 500" ng-click="showlogin=1">Login</a>
</p>
<p ng-show="showlogin == 1">
New User? -
<a href="" style="color:#d61b5e; text-decoration: underline; font-weight: 500" ng-click="showlogin=0">Signup</a>
</p>
</div>
<div class="modal-body">
<section ng-show="showlogin == 1">
<div class="login-from">
	<form method="post" name="loginform" ng-submit="loginform.$valid && ctrl.login()" novalidate>
		<div class="row">
			<div class="col-xs-12">
				<input class="form-control" type="text" name="login" value="" placeholder="Phone Number or Email" ng-model="user.username" ng-disabled="!canedit" required></div>
			<div class="col-xs-12">
				<input class="form-control" type="password" name="password" value="" placeholder="Password" ng-model="user.password" required ng-disabled="!canedit"></div>
			<div class="col-xs-4 submit">
				<input class="btn btn-login" type="submit" name="commit" value="Login" ng-disabled="!canedit"></div>
			<div class="col-xs-8 login-help">
				<a href="" data-toggle="modal" data-target="#forgotpwdmodal" data-backdrop="static">Forgot password?</a>
			</div>
			<div class="col-xs-12">
				<p class="validationerror">{{message}}</p>
			</div>
		</div>
		<a href="" style="padding: 10px;" ng-click="getotp(user.username)" ng-show="requestotp">Resend OTP?</a>
	</form>
	<div ng-show="requestotp">
		<form ng-submit="verifyotpandlogin()">
			<div class="row" ng-show="showotp">
				<div class="form-group col-xs-8" style="padding: 0 7px 0 15px;">
					<input type="number" class="form-control" placeholder="Enter OTP"  ng-model="otp"></div>
				<div class="form-group col-xs-4">
					<button type="submit" class="btn btn-default">Verify</button>
				</div>
			</div>
		</form>
	</div>
</div>
</section>

<div class="row" ng-show="showlogin == 0">
<form name="signupform" class="text-left" role="form" ng-submit="signupform.$valid && ctrl.signup()" novalidate>
	<div class="form-group col-xs-6" style="padding: 0 7px 0 15px;">
		<input type="text" name="fname" class="form-control" placeholder="First Name" ng-model="user.firstname" required ng-disabled="!canedit" ng-pattern="/^[a-zA-Z]*$/">
		<p class="validationerror" ng-show="signupform.$submitted && signupform.fname.$invalid">Only Letters and no space</p>
	</div>
	<div class="form-group col-xs-6" style="padding: 0 15px 0 7px;">
		<input name="lname" type="text" class="form-control" placeholder="Last Name"  ng-model="user.lastname" ng-disabled="!canedit" ng-pattern="/^[a-zA-Z]*$/">
		<p class="validationerror" ng-show="signupform.$submitted && signupform.lname.$invalid">Only Letters and no space</p>
	</div>
	<div class="form-group col-xs-12">
		<input name="email" type="email" placeholder="Email" class="form-control" ng-model="user.email" id="email" required ng-disabled="!canedit" ng-pattern="/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/">
		<p class="validationerror" ng-show="signupform.$submitted && signupform.email.$invalid">Enter a valid email</p>
	</div>
	<div class="form-group col-xs-12">
		<input type="number" name="phoneNumber" placeholder="Phone Number" class="form-control"  ng-minlength = "10" ng-maxlength = "10" ng-model="user.phonenumber" required ng-disabled="!canedit" ng-pattern="/^[1-9]+[0-9]*$/">
		<p class="validationerror" ng-show="signupform.$submitted && signupform.phoneNumber.$invalid">Exactly 10 digits, first digit non zero</p>
	</div>
	<div class="form-group col-xs-12">
		<input name="pwd" type="password" placeholder="Password" class="form-control" ng-model="user.password" required ng-disabled="!canedit">
		<p class="validationerror" ng-show="signupform.$submitted && signupform.pwd.$invalid">Password Required</p>
	</div>
	<div class="form-group col-xs-12">
		<p class="validationerror">{{message}}</p>
	</div>
	<button type="submit" class="btn btn-sign-up-form" ng-disabled="!canedit" ng-hide="1"></button>
</form>
<form name="signupotpform" ng-submit="signupotpform.$valid && ctrl.verifyotp(otp)" novalidate ng-show="otpshow">
	<div class="form-group col-xs-6">
		<input type="number" class="form-control" placeholder="Enter OTP"  ng-model="otp" required></div>
	<button type="submit" class="btn btn-default">Verify</button>
</form>
</div>
</div>
<div class="modal-footer" ng-show="showlogin == 0">
<div class="row">
<div class="col-xs-6 text-left">
	<button type="button" class="btn btn-sign-up-form" ng-disabled="!canedit" ng-click="signupform.$submitted = 1; signupform.$valid && ctrl.signup()">Sign Up</button>
</div>
<div class="col-xs-6 text-right">
	<button type="button" id="closesignup" class="btn btn-cancel" data-dismiss="modal" ng-hide="1">Cancel</button>
	<button type="reset" class="btn btn-cancel" ng-click="resetSignupModal()">Reset</button>
</div>
</div>
</div>
</div>

</div>
</div>

<div id="forgotpwdmodal" class="modal fade" role="dialog" ng-controller="LoginSignupController as ctrl">
<div class="modal-dialog" style="width: 380px;">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" ng-click="clearForgotPwdForm()">&times;</button>
<h5 class="modal-title">Forgot Password</h5>
</div>
<div class="modal-body">
<form name="forgot1" class="text-left" role="form" novalidate ng-hide="done || showotp" ng-submit="forgot1.$valid && getotp(username)">
<div class="row">
	<div class="form-group col-xs-8" style="padding: 0 7px 0 15px;">
		<input type="text" class="form-control" placeholder="Phone Number of Email" ng-model="username" required></div>
	<div class="form-group col-xs-4">
		<button type="submit" class="btn btn-default">Continue</button>
	</div>
</div>
</form>
<form name="forgot2" ng-show="showotp && !showpwd" ng-submit="forgot2.$valid && verifyotp()" novalidate>
<div class="row">
	<div class="form-group col-xs-8" style="padding: 0 7px 0 15px;">
		<input type="number" class="form-control" placeholder="Enter OTP"  ng-model="otp" required></div>
	<div class="form-group col-xs-4">
		<button type="submit" class="btn btn-default">Verify</button>
	</div>
</div>
</form>
<form ng-show="showpwd" name="forgot3" ng-submit="forgot3.$valid && changepwd()" novalidate>
<div class="row" >
	<div class="form-group col-xs-8" style="padding: 0 7px 0 15px;">
		<input type="password" placeholder="Enter New Password" class="form-control"  ng-model="pwd" required></div>
	<div class="form-group col-xs-4">
		<button type="submit" class="btn btn-default">Submit</button>
	</div>
</div>
</form>
<div class="row">
<div class="form-group col-xs-12">
	<p class="validationerror">{{message}}</p>
	<br/>
	<p class="validationerror">{{message1}}</p>
</div>
</div>
</div>

<!--div class="modal-footer">
<div class="row">
<div class="col-xs-6 text-left">
<button type="submit" class="btn btn-sign-up-form" ng-disabled="!canedit">Change Password</button>
</div>
<div class="col-xs-6 text-right">
<button type="button" class="btn btn-cancel" data-dismiss="modal">Cancel</button>
</div>
</div>
</div-->
</div>

</div>
</div>
<script>
	$(window).load(function(){
   	if (sessionStorage.getItem("is_seen") === null) {
            timeOut = setTimeout(function(){
                $('#cbmodal').modal('show');
                sessionStorage.setItem('is_seen', 1);
            }, 14000);
        }
});
</script>
