<div class="home page-content">
	<div class="page-content-area">
		<div class="container">
			<div class="error-page">
				<img src="assets/images/error.jpg" alt="Error" >
				<h3>Opps! Something went wrong.</h3>
				<p>
					We are sorry that we are unable to service your request. We will resolve the issue as early as possible. Sorry for the inconvenience caused!!
				</p>
				<p style="color:darkorange; font-size: 1.3em; font-weight: 200; padding:10px;"><?php echo $message ?></p>
				<br>
				<br>
				<a class="btn btn-info btn-continue-shopping" style="color: white;" href=".">Continue Shopping</a>
				<div class="debugblock" ng-show="0">
					<p>Debug info Client: {{status}}</p>
					<p>{{debugtext}}</p>
					<br>
					<a href="" ng-click="showdebug = 1" class="debugblock" ng-show="1">Show More Debug Info</a>
					<div ng-bind-html="trustAsHtml(servererror)" ng-show="showdebug == 1"></div>
				</div>
			</div>
		</div>
	</div>
</div>