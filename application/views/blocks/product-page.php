<?php
include_once(dirname(__FILE__) . '/zestphpsdk/source/ZestProcessor.php');
$zest = new ZestProcessor();
?>
<script>var logourl = 'assets/zestphpsdk/web/images/'; </script>
<script>var emiUrl = '<?php echo $zest->emiurl(); ?>'; </script>

<style>
.btn-add-to-cart {
		margin-top: 10px;
    width: 100%;
    text-transform: uppercase;
    padding: 10px;
    font-weight: 500;
    font-size: 16px;
    background: #0587b3;
    color: white;
    border-color: #0587b3;
    border-radius: 5px;
    letter-spacing: 2px;
    margin-bottom: 5px;
	}
	.btn-add-to-cart:hover {
		background:#038ab7;
		color: white;
		border-color: #0587b3;
	}
	.btn-add-to-cart:active {
		background:#038ab7;
		color: white;
		border-color: #0587b3;
	}
	.btn-add-to-cart:focus {
		background:#038ab7;
		color: white;
		border-color: #0587b3;
	}
.smallimage {
    max-width: 487px;
    float: left!important;
    vertical-align: top;
    display: inline-block;
    position: relative;
}
.smallimage ul {
    display: block;
    padding: 0;
    text-align: center;
}
@media (min-width: 768px) {
	.smallimage li {
	    width: 71px;
	    height: 71px;
	    cursor: pointer;
	    display: inline-block;
	    margin: 1px 1px;
	    border: 1px solid #D5D5D5;
	    overflow: hidden;
	}
	.prodimages1 {
		display: none;
	}
}

@media (max-width: 500px) {
	.smallimage li {
	    width: 52px;
	    height: 52px;
	    cursor: pointer;
	    display: inline-block;
	    margin: 1px 1px;
	    border: 1px solid #D5D5D5;
	    overflow: hidden;
	}
	.image-grid {
		display: none;
	}
}
.smallimage li:hover {
    border: 1px solid #B3B3B3;
}
.smallimage li.active {
    border: 1px solid #47C9AF;
}
.smallimage li img {
    width: auto;
    height: 100%;
    position: relative;
    top: 50%;
    transform: translateY(-50%);
}
.slick-dots {
	bottom:-20px!important;
	width: 90%;
	margin-left: 5%;
}
.slick-dots li {
	margin: 0!important;
}

.zest-close {
    top: 15px!important;
}

.clearout {
	/*background-color: #2dbcab !important;*/
	background-color: #f1990f !important;
    position: absolute;
    left: 0;
    top: 0;
    width: 40px;
    height: 40px;
    border-radius: 50%;
    /* padding: 10px; */
    border: 1px solid #d48407;
    /* text-align: center; */
    padding-left: 4px;
    padding-top: 11px;
}
.clearout > span {
	color: white;
	font-size: 14px;
	font-weight: 300;
}
.stock-availability.brandnew {
	background: #337ab7!important;
	padding: 5px;
}
.stock-availability.unboxed {
	background: #71ba51!important;
	padding: 5px;
}
.slider {
    width: 50%;
    margin: 100px auto;
}
.zestMoney_option {
    font-size: 14px!important;
    padding-left: 3px!important;
    font-family: 'Open Sans', sans-serif;
    color: #000000!important;
    text-decoration: underline!important;
    /* font-weight: 600; */
}
.zest-list-item ol li, .zest-list-item ul li {
    font-size: 12px!important;
    line-height: 27px!important;
}
.zest-note {
        font-size: 10px!important;
    line-height: 16px!important;
    font-weight: 300!important;

}

div#imageModal .modal-dialog {
  width: 100%;
  height: 100%;
  margin:0!important;
}

div#imageModal .modal-dialog .modal-content {
  height: 100%;
  border-radius: 0;
  padding: 0!important;
}
}
.prodimagezoom {
	padding:0!important;
	height: auto;
	margin:0 auto;
}
@media (min-width: 500px) {
	.prodimagezoom img {
	  /* Preserve aspet ratio */
	    width: 47%;
	    margin-left: 25%;
	}
	#offerimage {
		width: 75%
	}
}

@media (max-width: 500px) {
	.prodimagezoom img {
	    margin-top: 25%;
	}
	#offerimage {
		width: 100%
	}
}
.modal {
    display: block;
    visibility: hidden;
    overflow-y: hidden;
}
.modal.in {
    visibility: visible;
}

</style>

<script type="text/javascript">
$(document).ready(function(){
	 $('.prodimages1').slick({
	 	// key : value
	 dots: true,
        infinite: true,
        speed: 300,
        arrows:false,
	lazyLoad: 'ondemand',
	 });
	 $('.prodimagezoom').slick({
	 	// key : value
	 dots: true,
        infinite: true,
        speed: 300,
        arrows:true,
	lazyLoad: 'progressive',
	 });
    $("#deliveryPopover").popover({
    	title: '<span style="color: #47c9af;">Free Home Delivery</span>',
        content : "<ul style='padding-left: 10px; font-size: 13px; font-weight: 300;' ><li>We provide service only in Bangalore.</li><li>Delivery is free only for ground floor or with lift facility. In case of no lift, extra Rs.100 is chargeable.</li><li>Additional charges will be levied, if the delivery is more than 20km from Refabd warehouse at H.S.R Layout.</li></ul>",
        html: true,
        placement: 'right',
        trigger: 'hover'
    });
});
</script>
	<div class="page-content-area" ng-init=<?php echo '"initializeProd('.$productid.')"' ?>>
		<div class="container">
			<div class="products-breadcrumb">
				<ol class="breadcrumb" style="margin-bottom: 5px;">
					<li><a href=".">Home</a></li>
					<li><a href="collection">Collections</a></li>
					<li><a ng-href="category/{{catid}}/{{catname|toproducturl}}">{{catname}}</a></li>
					<li class="active"><a href="">{{item.product_title}}</a></li>
				</ol>
			</div>

			<!-- Product -->
			<div class="row single-product">
				<div class="grid-left col-xs-12 col-sm-6">
					<section class="prodimages1">
					    <?php
					    	foreach($images as $im) {?>
					    	 <a data-toggle="modal" data-target="#imageModal">
					    	 <div>
					      		<img src=<?php echo "assets/images/products/normal/$im->name" ?> >
					      	</div></a>
					    	<?php } ?>
					</section>
					<div class="image-grid">
						<!--div class="big-image"-->
						<a data-toggle="modal" data-target="#imageModal">
							<div class="big-image">
							<!--p style="display:inlince-block;">Mouse over to Zoom</p-->
								<div class="big-imagediv">
									<!--img ng-attr-data-imagezoom="{{selectedimagezoom}}"  ng-src="{{selectedimage}}" alt="{{item.product_title}}" imageonload="changeBigImage()"-->
									<img ng-src="{{selectedimage}}" alt="{{item.product_title}}" imageonload="changeBigImage()">
									<div class="badge stock-availability unavailable" ng-show="item.issold == 1">Sold Out</div>
									<!--div class="badge stock-availability stock-available" style="background-color: #f1990f !important" ng-show="item.compressorwarranty != 0">
										<span style="color: white; font-size: 20px; position: absolute; left:5px; top:1px">{{item.compressorwarranty}}</span> <span style="padding-left:7px; color:light-grey">year warranty</span></div-->
								<!--div class="badge stock-availability stock-available" ng-show="item.isfactory == 1 && item.issold != 1">Unused & New</div-->
								<!--div class="clearout"><span>{{item.closingoffer}}%</span></div-->
								</div>
							</div>
						</a>
						<div class="smallimage"> <!--image-thumb-list"-->
							<ul class="">
								<li ng-class="im.activeorpassive" ng-repeat="im in item.images">
									<img ng-src="assets/images/products/thumbnail/{{im.image_full}}" ng-click="showImageLoader(im)">
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="grid-right product-detail-grid col-xs-12 col-sm-6">
                	<h1 class="product-name"><a href="product/{{catname|toproducturl}}/{{item.id}}/{{item.product_title|toproducturl}}">{{item.product_title}}</a>
						<span class="product-description">{{item.product_description}}</span>
                	</h1>
	                <div class="quality-index" ng-show="item.brandnew == 0" ng-class="item.qualityindex >= 4 ? 'green-dark' : (item.qualityindex >=3 ? 'green-bright' : (item.qualityindex >=2 ? 'yellow' : (item.qualityindex< 2 ? 'red' : 'grey')))">
	                	<span ng-show="item.brandnew == 0"class="icon pocketin-icon-quality-index"></span><span>{{item.qualityindex|number:1}}</span>
           			</div>
           			<div class="label stock-availability brandnew" ng-show="item.brandnew == 1">Brand New</div>
           			<div class="label stock-availability unboxed" ng-show="item.isfactory == 1">Unboxed</div>
           			<div class="container" ng-show="item.brandnew == 0">
					<ul class="product-features-list">
						<!--li style="color: #47c9af;" ng-show="item.compressorwarranty != 0 && catid == 3">{{item.compressorwarranty}} year compressor warranty</li>
						<li style="color: #47c9af;" ng-show="item.compressorwarranty != 0 && catid == 4">{{item.compressorwarranty}} year motor warranty</li-->
						<li>6 Months Service Partner Warranty</li>
						<li>7 day Replacement Guarantee</li>
						<li>Cash on Delivery/Online payment</li>
						<li><a href="" style="text-decoration:underline"  id="deliveryPopover" data-toggle="popover">Free Home Delivery
						</a> </li>
						<li>Free Installation and Demo.</li>
						<a href="terms">View T&amp;C</a>
					</ul>
					</div>
					<div class="container" ng-show="item.brandnew == 1">
					<ul class="product-features-list">
						<li>Brand New Product</li>
						<li>1 Year Manufacturer Warranty</li>
						<li>Cash on Delivery/Online payment</li>
						<li><a href="" style="text-decoration:underline"  id="deliveryPopover" data-toggle="popover">Free Home Delivery
						</a> </li>
						<li>Free Installation and Demo.</li>
						<a href="terms">View T&amp;C</a>
					</ul>
					</div>

					<!--div class="colors-available">
						<label class="inline">Colors:</label>
						<ul class="colors-list inline">
							<li><span class="item-color"></span></li>
							<li><span class="item-color"></span></li>
							<li><span class="item-color"></span></li>
						</ul>

					</div-->
					<!-- offer with coupon div -->
					<!--div style="margin-top: 8px;border: 1px solid #cacaca;margin-bottom:8px" id="offerimage">
						<img src="assets/images/offer/indoffer.jpg" alt="Extra 10% off" style="width:100%">
					</div-->
					<div class="offers" ng-show="item.brandnew == 0">
						<label style="color: #d61b5e"><span class="glyphicon glyphicon-tag"></span>Sell Back to us for 80%</label>
						<p style="padding-top: 5px; padding-bottom: 5px;">We Buy this back for: <span style="font-weight: bold; font-size: 14px;"><span>&#x20B9;</span>{{item.saleprice*.8 | number:0}}</span></p><a href="terms">View T&C</a></p>
					</div>
					<div class="offers" ng-show="item.brandnew == 1">
						<label style="color: #d61b5e"><span class="glyphicon glyphicon-tag"></span>Sell Back to us for 70%</label>
						<p style="padding-top: 5px; padding-bottom: 5px;">We Buy this back for: <span style="font-weight: bold; font-size: 14px;"><span>&#x20B9;</span>{{item.saleprice*.7 | number:0}}</span></p><a href="terms">View T&C</a></p>
					</div>
					<div class="product-pricings">
						<ul>
							<!--li style="text-decoration: line-through; color: grey; font-size: 14px; opacity: .5;"><span>Sale Price: &#x20B9;</span>{{item.saleprice * (1+item.closingoffer/100) | number:0}}</li--><br>
			                <li class="product-price">Sale Price: <span>&#x20B9;</span>{{item.saleprice | number:0}}</li>
			                <li class="market-price">Approx. Market price&nbsp;<span>&#x20B9;</span>{{item.mrp | number:0}}</li>
			                <!-- <li class="user-savings">5,678<small>You save</small></li> -->
		                </ul>
		                <span class="prodoffer inline text-right" style="font-size: 16px;color: #515605;">({{100-((item.saleprice/item.mrp)*100 )| number:0}}% off)</span>
	                </div>
	                <div>
	                	<script>
		                	var clientid = '<?php echo $zest->getclientid(); ?>';
		                	//var auth_token = <?php echo $zest->gettoken('public'); ?>;
		                	var minamount = <?php echo $zest->getminordertotal(); ?>;
		                	var maxamount = <?php echo $zest->getmaxordertotal(); ?>;
		                	var amount = <?php echo $saleprice ?>;
		                	var baseSecureUrl = "./";
		                	if(amount>minamount && amount<maxamount){
								//zestMerchant.getQuote(clientid,Number(amount),false,'#zest-emi-widget',auth_token);
								zestMerchant.getQuote(clientid,Number(amount),false, '#zest-emi-widget',1000);
							}
	                	</script>
	                	<div id="zest-emi-widget" class="emi-cnt" style="display:none;"></div>
		<div class="cardlessEMI" style="margin-top: 15px;">
			<span style="display: inline-block;border: 1px solid #000000;padding: 2px;border-radius: 3px;font-weight: 600;margin-right: 5px;"><span style="font-size: 11px;line-height: 1;font-weight: 400;letter-spacing: 0px;">cardless</span><br><span style="letter-spacing: 3px;font-size: 19px; font-weight: 400;margin-left:1px">EMI</span></span>
			<span class="cardless">From <span style="font-size:20px; color:#20a797"><span style="font-size:14px">&#x20B9;</span>{{item.saleprice/12|number:0}}</span></span>
			<span class="zestMoney_option" id="zest-modal-btn">
			View More
			</span>
		</div>
		<div id="zestmoney-modal" class="zest-modal">
			<div class="zest-modal-content">
				<div class="zest-right-panel">
					<div class="zest-modal-header">
						<span class="zest-close">x</span>
						<div class="zest-merchant-logos">
							<img src="assets/zestphpsdk/web/pwid/images/zest.svg" alt="ZestMoney" class="zest-logo">
							<span class="zest-with">with</span>
							<img src="assets/images/logo.png" alt="Refabd" class="zest-merchant-logo" style="width: 61px;max-width: 61px;height: auto;">
						</div>
						<h1 class="zest-modal-title"></h1>
						<!-- <h1 class="zest-modal-title"></h1> Remove the inner Html of this Element -->
					</div>
					<div class="zest-emi-table">
					  <table class="zest-emi" cellspacing="0">
					  	<tbody>
					  	</tbody>
					  </table>
					</div>
					<div class="zest-modal-body">
						<h2 class="zest-select">Select <span class="zest-bold">"Pay Using ZestMoney EMI"</span> at Checkout</h2>
						<div class="zest-features-table">
							<table cellspacing="0">
							  	<tbody>
							  		<tr>
							  			<td class="zest-feature-1">High approval rates</td>
							  			<td class="zest-feature-2">No credit card required</td>
							  		</tr>
							  		<tr>
							  			<td class="zest-feature-3">Completely online process</td>
							  			<td class="zest-feature-4">Payment protection &amp; easy refunds</td>
							  		</tr>
							  	</tbody>
							  </table>
						</div>
						<div class="zest-list-item">
					<p class="zest-list-title">
						<span class="p-title">Two steps to confirm your order :</span>
					</p>
						<ol>
							<li> <img src="assets/zestphpsdk/web/pwid/images/01PNG.png" style="max-width: 27px;" class="title_img" /> Provide your personal  details to get <span class="zest-bold">instant approval</span></li>
							<li> <img src="assets/zestphpsdk/web/pwid/images/02PNG.png" style="max-width: 27px;" class="title_img" />Make up front payment &amp; verify your documents to <span class="zest-bold">confirm.</span></li>
						</ol>
						<p class="zest-note">* Keep your ID and income documents ready for a speedy order confirmation.<br/>
						* Up front payment can be using net banking or card and is fully refundable.</p>
				</div>
					</div>
				</div>
			</div>
		</div>
	    <script>
	    	// Get the modal
			var modal = document.getElementById('zestmoney-modal');

			// Get the button that opens the modal
			var btn = document.getElementById("zest-modal-btn");

			// Get the <span> element that closes the modal
			var span = document.getElementsByClassName("zest-close")[0];

			// When the user clicks on the button, open the modal 
			btn.onclick = function() {
			    modal.style.display = "block";
			}

			// When the user clicks on <span> (x), close the modal
			span.onclick = function() {
			    modal.style.display = "none";
			}
			// When the user press esc key, close the modal
			document.onkeydown = function(e) {
			    if (e.keyCode == 27) {
			        modal.style.display = "none";
			    }
			};

			// When the user clicks anywhere outside of the modal, close it
			window.onclick = function(event) {
			    if (event.target == modal) {
			        modal.style.display = "none";
			    }
			}

	    </script>
	                </div>

<!-- Image full screen modal -->
<div class="modal fade" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <p class="modal-title" id="myModalLabel" style='font-size: 11px;color: #007cbb;font-family: "Trebuchet MS", "verdana;";padding-top: 3px;'>These are actual Images of the product taken from Refabd Warehouse</p>
      </div>
      <div class="modal-body" style="padding:0!important">
        <section class="prodimagezoom" style="display:block!important">
            <?php
            	foreach($images as $im) {?>
            	 <div style="padding:0px!important">
              		<img src=<?php echo "assets/images/products/zoom/$im->name" ?>>
            	</div>
            <?php } ?>
        </section>
      </div>
      <div class="modal-footer" ng-controller="CartManagementController as cartctrl">
        <button class="btn btn-add-to-cart" ng-click="addToCart(item)" type="button" ng-disabled="item.issold == 1" style="width:50%;margin-right:25%">Add to cart</button>
      </div>
    </div>
  </div>
</div>


					<div class="row" style="margin-top:10px">
						<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8" ng-controller="CartManagementController as cartctrl">
							<button class="btn btn-add-to-cart" ng-click="addToCart(item)" type="button" ng-disabled="item.issold == 1">Add to cart</button><br>
							<form name="cb" role="form" ng-submit="cb.$valid && reqCallback(cbnumber,item.id)" novalidate>
								<div class="form-group" ng-show="reqcbdone == 0">
									<label style="font-weight: 100;font-size: 13px;">Any queries? Request callback:</label>
									<input type="text" class="form-control" placeholder="Enter Contact Number"  ng-model="cbnumber" style="background: transparent !important;border-color:transparent !important;border-bottom: 1px solid #B1B1B1 !important;margin-top: -11px;padding-left: 0;font-size: 13px;width: 51%;display:inline" required>
									<button type="submit" style="display:inline;padding: 5px;background: transparent;border: 1px solid #636363;border-radius: 5px;margin-left: 5px;position: absolute;margin-top: -2px;">submit</button>
								</div>
								<label ng-show="reqcbdone == 1" style="font-size: 12px;margin-top: 5px;">Thank You, you will get a call from us soon.</label>
							</form>
						</div>

							<div class="col-xs-12 col-sm-12 col-md-8 col-lg-4" style="margin-top:5px;">
								<button class="btn btn-add-to-wishlist" ng-class="item.inwishlist == 1 ? 'active' : ''" data-toggle="tooltip" data-placement="top" title="Add to Wishlist" type="button" ng-click="item.inwishlist=item.inwishlist==1?0:1; addtowishlist(item, item.inwishlist)">
				            	<span class="icon pocketin-icon-heart"></span><p style="text-decoration: underline;">add to wishlist</p>
				                </button>
							</div>
					</div>
	               	<!--div class="product-action-btns">
	               		<ul>
			                <li>
			                <div ng-controller="CartManagementController as cartctrl">
			                	<button class="btn btn-md btn-add-to-cart"  ng-click="addToCart(item)" type="button" ng-disabled="item.issold == 1">Add to cart</button>
			                </div>
			               </li>
			                <li>
				    			<button class="btn btn-md btn-buy-now" type="button" ng-click="buyNow(item)" ng-disabled="item.issold == 1">Buy Now </button>
			                </li>
			                <li>
		                		<button class="btn btn-add-to-wishlist" ng-class="item.inwishlist == 1 ? 'active' : ''" data-toggle="tooltip" data-placement="top" title="Add to Wishlist" type="button" ng-click="item.inwishlist=item.inwishlist==1?0:1; addtowishlist(item, item.inwishlist)">
				            	<span class="icon pocketin-icon-heart"></span>
				                </button>
			                </li>
		                </ul>
	                </div-->

	            </div>
			</div>
            <div class="row">
            	<div class="col-xs-12 col-sm-12 product-technical-details text-left">
					<div class="clear"> <hr/> </div>

	            </div>

	            <div ng-show="item.brandnew == 0" class="col-xs-12 col-sm-6 text-left">
				<div class="product-brief">
					<p>{{item.longdescription}}</p>
				</div>
					<h4>Product Condition</h4>
				  	<div class="tab-pane fade in active product-condition" id="additional">
			            <ul>
			                <li  ng-repeat="line in (item.conditionspec | newlines) track by $index">
						    	{{line}}
							</li>
						</ul>
			        </div>
				</div>

				<div ng-show="item.brandnew == 1" class="col-xs-12 col-sm-12 col-lg-12 col-md-12">
					<div class="product-brief">
						<p>{{item.longdescription}}</p>
					</div>
				</div>
            	<div class="col-xs-12 col-sm-6" ng-show="item.brandnew == 0">
            		<div class="quality-index-block panel panel-default">
				  <!-- Default panel contents -->
				  <div class="panel-body">
				  	<h4>Quality index breakdown</h4>
				    <p style="padding-top: 5px">Higher score is the better score.</p>
				  </div>
				  <!-- Table -->
				  <table class="table">
				    <thead>
						<tr>
						<th>Quality Index</th>
						<th>Value</th>
						<th>Score</th>
						</tr>
					</thead>
					<tbody style="">
						<tr ng-repeat="key in quality">
						<td class="text-left index-label">{{key.attribute}}</td>
						<td class="text-left">{{key.value}}</td>
						<td class="text-left">{{key.score}} / 5</td>
						</tr>
					</tbody>
				  </table>
				</div>
				<!--div class="row">
					<div class="col-xs-12 col-sm-2" ng-repeat="key in quality">
						<span class="icon pocketin-icon-Thumps-up huge" style="padding-bottom: 5px;"></span>
						<p>{{key.attribute}}</p>
						<label>{{key.value}}</label>
					</div>
				</div-->
				<!-- <div class="sub-header">
	                <div class="subtitle fancy"><span><h4>Product General Description</h4></span></div>
				</div>
				<div class="product-brief">
					<p>{{item.longdescription}}</p>
				</div> -->
            </div>
				<div class="col-xs-12 col-sm-12 text-left">
				  	<h4>Technical Specification</h4>
					<!--p>Key Technical details</p-->
				  	<div class="tab-pane fade in technical-brief" id="technical">
					  	<ul>
			                <li ng-repeat="line in (item.technicalspec | newlines) track by $index">
							    {{line}}</li>
						</ul>
					</div>
				</div>


			</div>
         	<!--div class="hor-gird quality-products-home text-center" ng-controller="RelatedItemsController as ctrl" ng-init="$">
				<special-products></special-products>
			</div-->
		</div>
	</div>
</div>
