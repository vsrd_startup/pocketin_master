<div class="home page-content">
	<div class="page-content-area">
		<div class="container">
			<div class="products-breadcrumb">
				<ol class="breadcrumb" style="margin-bottom: 5px;">
					<li><a href=".">Home</a></li>
					<li><a href="collection">Collections</a></li>
					<li class="active"><a href="">Shopping Cart</a></li>
				</ol>
			</div>
			<div ng-show="cartempty && !loading" class="empty-cart">
			<h3>There are no items in cart</h3>
				<p style="padding-top: 10px"> Please add one or more items to the cart </p>
			</div>
			<div class="row shopping-cart" ng-show="!cartempty">
				<div class="col-xs-12 col-sm-12 col-md-9">
					<div class="row delivery text-left" style="background-color: #ffa300; color: #4b1b39"" ng-hide="disableoneplusoffer || oneplusitem">
						<label style="font-size: .9em; font-weight: 300;">Add one more item to the cart to avail <span style="font-size: 1.4em; font-weight: 500; ">5</span>% offer on entire cart value.</label>
					</div>
					<div class="row delivery text-left" style="background-color: #53bda8; color: #4b1b39"" ng-show="!disableoneplusoffer&&oneplusitem">
						<label style="font-size: .9em; font-weight: 300;">Flat <span style="font-size: 1.4em; font-weight: 500; ">5</span>% discount applied on entire cart value.</label>
					</div>
					<div class="cart-items">
						<div class="row table-head">
							<div class="col-xs-12 col-sm-10 col-md-10 col-lg-10 no-pad"><label>Shopping Cart</label></div>
							<div class="col-xs-4 col-sm-2 col-md-2 col-lg-2 text-center">Total</div>
						</div>
						<div class="row cart-item" ng-repeat="item in cart" ng-class="item.issold == 1 ? 'item-unavailable' : ''">
							<div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
								<div class="row">
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-3 cart-thumb">
										<a ng-href="product/in-cart/{{item.productitem_id}}/{{item.product_title|toproducturl}}"><img ng-src="assets/images/products/thumbnail/{{item.images[0].image_full}}"></a>
									</div>
									<div class="badge stock-availability unavailable" ng-show="item.issold == 1" style="left:5%;top:40%">Sold Out</div>
									<div class="col-xs-8 col-sm-7 col-md-8 col-lg-9 cart-item-details">
		                				<h1 class="product-name"><a ng-href="product/in-cart/{{item.productitem_id}}/{{item.product_title|toproducturl}}">{{item.product_title}}</a>
					                		<div class="quality-index" ng-class="item.qualityindex >= 4 ? 'green-dark' : (item.qualityindex >=3 ? 'green-bright' : (item.qualityindex >=2 ? 'yellow' : (item.qualityindex< 2 ? 'red' : 'grey')))">
											<span class="icon pocketin-icon-star-empty"></span>
											{{item.qualityindex| number:1}}
											</div>
											<span class="product-description">{{item.product_description}}</span>
					                	</h1>
										<!--div class="colors-available">
											<label class="inline">Colors:</label>
											<ul class="colors-list inline">
												<li><span class="item-color"></span></li>
											</ul>

										</div-->
										<div class="product-actions mobile-hide">
											<button class="btn btn-md btn-add-to-wishlist" ng-class="item.inwishlist == 1 ? 'active' : ''" data-toggle="tooltip" data-placement="top" title="Add to Wishlist" type="button" ng-click="item.inwishlist=item.inwishlist==1?0:1; addtowishlist(item, item.inwishlist)">
				                			<span class="icon pocketin-icon-heart"></span>
				                		</button>
                        <a href="" ng-click="removeFromCart(item.productitem_id, item.saleprice)" class="btn btn-link remove-product" ng-if="disableremove != 1"><span class="icon pocketin-icon-trash-2"></span>Remove</a>
										</div>
										<span class="value item-total product-price hidden-sm hidden-md hidden-lg"><span>&#x20B9;</span>{{item.saleprice | number:2}}</span>

									</div>
								</div>
							</div>
							<div class="hidden-xs col-xs-offset-4 col-xs-5 col-sm-offset-0 col-sm-2 col-md-offset-0 col-md-2 col-lg-offset-0 col-lg-2 ">
								<div class="total" style="display:block; color: grey; font-size: 14px; opacity: 1.5; padding-bottom: 5px;>
									<span class="value item-total product-price"><span>&#x20B9;</span>{{item.saleprice | number:2}}</span>
								</div>
								<!--div class="total" style="display:block;">
									<span class="value item-total product-price" style="font-size: 16px;"><span>&#x20B9;</span>{{(item.saleprice * (100-offer)/100 | number:2}}</span>
								</div-->
							</div>
						</div>
					</div>
				</div>

				<div class="col-xs-12 col-sm-12 col-md-3 summary-grid">
					<label>Order Summary</label>
					<div class="summary">
						<div class="delivery-text"><label>Free Delivery</label>
						<p>Delivered in 2-3 business days.</p></div>
						<div class="product-price"><label ng-hide="offer == 0">Total:</label>
							<span ng-hide="offer == 0" style="display:inline;text-decoration: line-through; color: grey; opacity: 1.5; font-weight:300; font-size:18px"><span>&#x20B9;</span>{{total}}</span>
						<span style="font-weight: 600;color: #69BB27;font-size: 18px;" ng-style="offer == 0 ? {'margin-left': '0'} : {'margin-left': '67px'}"><span>&#x20B9;</span>{{total * (100-offer)/100 | number:2}}</span></div>
						<button class="btn btn-md btn-proceed-to-checkout" ng-click="checkout()" ng-disabled="sold == 1">Proceed to Checkout</button>
						<div ng-show="sold == 1">
							<p style="color:red; margin-top: 10px; padding: 5px;">Please remove the items which are Sold Out!!</p>
						</div>
					</div>
				</div>
			</div>
			<!--div class="hor-gird quality-products-home text-center" ng-controller="RelatedItemsController as ctrl">
				<special-products></special-products>
			</div-->
<!-- Top quality products -->
<!--
			<div class="hor-gird quality-products-home text-center" ng-controller="TopQualityController as ctrl">
				<special-products></special-products>
			</div>
			<div class="hor-gird quality-products-home text-center" ng-controller="NewArrivalsController as ctrl">
				<special-products></special-products>
			</div>
			<div class="hor-gird quality-products-home text-center" ng-controller="ItemHistoryController as ctrl" ng-show="itemspresent">
				<special-products></special-products>
			</div>
			-->
		</div>
	</div>
</div>
