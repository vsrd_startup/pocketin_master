<div class="home page-content">
	<div class="page-content-area">
		<div class="container">
			<div class="products-breadcrumb">
				<ol class="breadcrumb" style="margin-bottom: 5px;">
					<li><a href=".">Home</a></li>
					<li><a href="myorders">Order History</a></li>
					<li class="active"><a href="">Order: {{order.id}}</a></li>
				</ol>
			</div>

				<div class="per-order-details">
					<div class="order-summary">
						<div class="sub-header col-xs-12 text-center">
			                <div class="subtitle fancy"><span><h4>Order Summary</h4></span></div>
						</div>

						<div class="row">
							<div class="col-sm-12 col-xs-12 text-center1">
								<div class="">
									<div class="row">
										<div class="col-sm-12 col-xs-12 order-summary-block">
											<ul>
												<li><h4>Congratulations, Your order has been placed successfully!</h4></li>
											</ul>
										</div>
										<div class="col-sm-6 col-xs-12 order-summary-block">
											<ul class="">
												<li><p>Your order will be delivered in two to three business days. When the items are ready for shipping, you will receive an email confirmation shortly on <cite>{{order.deliveryaddress.email}} </cite></p></li>
												<li><label>Order ID:</label> <span class="order-id">PN{{order.id}}</span> ({{order.itemcount}} Items)<span class="order-date"> {{order.orderdate}}</span></li>
												<li><label>Amount to Pay: </label> <strong class="amount">{{order.amount}}</strong> <span class="payment-method">Cash on Delivery</span></li>
												<li>You can track order from my orders, <a href="myorders" class="btn btn-xs btn-my-orders" style="padding: 5px;">My Orders</a></li>
											</ul>
										</div>

										<div class="col-sm-6 col-xs-12">
											<div class="order-address">
												<div class="row">
													<div class="col-sm-12 col-xs-12">
														<strong>Shipping address</strong>
														<label class="name">{{order.deliveryaddress.name}}<span class="phone"><span class="glyphicon glyphicon-phone"></span>{{order.deliveryaddress.phonenumber}}</span></label>
														<p class="">{{order.deliveryaddress.address_line_one}}, {{order.deliveryaddress.address_line_two}}</p>
														<p class="">{{order.deliveryaddress.city}}, {{order.deliveryaddress.state}}</p>
														<p>{{order.deliveryaddress.pincode}}</p>
														<b class="landmark">{{order.deliveryaddress.landmark}}</b>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12 col-xs-12">
								<div class="order-info-sec no-pad">
									<ul class="row table-head">
										<li class="col-xs-6"><strong>Item(s)</strong></li>
										<li class="col-xs-6 text-right"><strong>Price</strong></li>
									</ul>
									<ul>
										<li class="row single-item" ng-repeat="item in order.items">
	        								<div class="col-xs-12 col-sm-10 col-md-10 col-lg-6">
	        									<div class="row"> 
	            									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-3 cart-thumb">
														<img ng-src="assets/images/products/thumbnail/{{item.productimage}}">
													</div>
													<div class="col-xs-8 col-sm-7 col-md-8 col-lg-9 cart-item-details">
						                				<h1 class="product-name">{{item.productname}}
															<span class="product-description">{{item.productdescription}}</span>
									                	</h1>
														<!--div class="colors-available">
															<label class="inline">Colors:</label>
															<ul class="colors-list inline">
																<li><span class="item-color"></span></li>
															</ul>

														</div-->
													</div>
												</div>
	        								</div>
	        								<div class="col-xs-12 col-sm-10 col-md-10 col-lg-4 no-pad">
	        									<div class="product-status text-center">
													<ul class="row statusitems">
														<li class="col-sm-3 col-xs-12" ng-class="item.status == 'Order Placed' ? 'active' : ''">
															<div class="timeline">
																<span></span>
															</div>
															<label>Placed</label>
															<div class="timeline-info" ng-show="item.status == 'Order Placed'">
																<p>Your order has been placed on</p>
																<p>on {{order.orderdate}}</p>
																<p>Payment Approved</p>
															</div>
														</li>
														<li class="col-sm-3 col-xs-12" ng-class="item.status == 'Order Confirmed' ? 'active' : ''">
															<div class="timeline">
																<span></span>
															</div>
															<label>Confirmed</label>
															<div class="timeline-info" ng-show="item.status == 'Order Confirmed'">
																<p>Your order has been confirmed on</p>
																<p>{{item.qcdonedate}}</p>
															</div>
														</li>
														<li class="col-sm-3 col-xs-12" ng-class="item.status == 'Shipped' ? 'active' : ''">
															<div class="timeline">
																<span></span>
															</div>
															<label>Dispatched</label>
															<div class="timeline-info" ng-show="item.status == 'Shipped'">
																<p>Your order has been dispatched on</p>
																<p>{{item.shippeddate}}</p>
															</div>
														</li>
														<li class="col-sm-3 col-xs-12" ng-hide="item.status == 'Cancelled'" ng-class="item.status == 'Delivered' ? 'active' : ''">
															<div class="timeline">
																<span></span>
															</div>
															<label>Delivered</label>
															<div class="timeline-info" ng-show="item.status == 'Delivered'">
																<p>Your Order has been delivered on</p>
																<p>{{item.deliverydate}}</p>
															</div>
														</li>
														<li class="col-sm-3 col-xs-12 cancelled" ng-show="item.status == 'Cancelled'">
															<div class="timeline">
																<span></span>
															</div>
															<label>Cancelled</label>
															<div class="timeline-info">
																<p>Your order has been cancelled on</p>
																<p>{{item.canceldate}}</p>
															</div>
														</li>
													</ul>
												</div>
	        								</div>
	        								<div class="col-xs-offset-4 col-xs-5 col-sm-offset-0 col-sm-2 col-md-offset-0 col-md-2 col-lg-offset-0 col-lg-2 text-right" style="margin-top: 60px;">
	        									<div class="total" style="display:block;">
													<span class="value item-total product-price"><span>&#x20B9;</span>{{item.amount}}</span>
												</div>
	        								</div>
	        							</li>
	        							<li class="row">
	        								<div class="col-xs-12 col-sm-10 col-md-10 col-lg-9">
	        								</div>
	        								<div class="col-xs-offset-4 col-xs-5 col-sm-offset-0 col-sm-2 col-md-offset-0 col-md-2 col-lg-offset-0 col-lg-3 text-right">
	        									<div class="grand-total" style="display:block;">
	        										<label class="inline">Total Amount to pay:</label>
													<strong class="inline"><span>&#x20B9;</span>{{order.amount}}</strong> 
												</div>
	        								</div>
	        							</li>
									</ul>
								</div>
							</div>
						</div>
					<!--div class="ratings-reviews">
						<div class="sub-header col-xs-12 text-center">
			                <div class="subtitle fancy"><span><h4>Ratings & Reviews</h4></span></div>
						</div>
						<div class="row">
							<div class="col-xs-12 user-ratings">
								<ul class="text-center">
									<li>
										<a href="">
											<img src="assets/images/star.png">
											<label>Excellent</label>
										</a>
									</li>
									<li>
										<a href="">
											<img src="assets/images/star.png">
											<label>Good</label>
										</a>
									</li>
									<li>
										<a href="">
											<img src="assets/images/star.png">
											<label>Fair</label>
										</a>
									</li>
									<li>
										<a href="">
											<img src="assets/images/star-unrated.png">
											<label>Poor</label>
										</a>
									</li>
									<li>
										<a href="">
											<img src="assets/images/star-unrated.png">
											<label>Awful</label>
										</a>
									</li>
								</ul>
							</div>
							<div class="col-xs-5 user-reviews">
								<div class="form-group" >
									<label>Review Title:</label>
									<input type="text" class="form-control" placeholder="Review Title">
									<p>(Maximum 60 characters.)</p>
								</div>
								<div class="form-group" >
									<label>Your Review:</label>
									<textarea class="input-group" placeholder="Review"> </textarea>
									<p>(Please make sure your review contains at least 100 characters.)</p>
								</div>
									<p class="error hide">(Please make sure your review contains at least 100 characters.)</p>

								<div class="form-group text-center">
									<button class="btn btn-submit" type="submit">Submit</button>
								</div>
							</div>
						</div>
					</div-->
					</div>
				</div>
	</div>
	</div>
	</div>

