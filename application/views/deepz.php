<!DOCTYPE html>
<html ng-app="drapp" ng-cloak>
<head>
	<title>Dr Deepak Damodaran</title>
	<link rel="stylesheet" href="assets/css/bootstrap.min.css"/>
	<nav class="navbar navbar-inverse navbar-static-top">
	  <div class="container-fluid">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="#">Dr. Deepak</a>
	    </div>

	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">
	        <li class="active"><a href="" data-toggle="collapse" ng-click="showcontent = 1">Add Patient <span class="sr-only">(current)</span></a></li>
	        <li><a href="" ng-click="showcontent = 2; showdetails = 0; getAllPatients()">All Patients</a></li>
	        <!--li class="dropdown">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
	          <ul class="dropdown-menu">
	            <li><a href="#">Action</a></li>
	            <li><a href="#">Another action</a></li>
	            <li><a href="#">Something else here</a></li>
	            <li role="separator" class="divider"></li>
	            <li><a href="#">Separated link</a></li>
	            <li role="separator" class="divider"></li>
	            <li><a href="#">One more separated link</a></li>
	          </ul>
	        </li-->
	      </ul>
	      <form name="sf" class="navbar-form navbar-left" ng-submit="sf.$valid && searchPatient()" novalidate>
	        <div class="form-group">
	           <select ng-model="searchfield" class="form-control"  >
				  <option  value="name" >Name</option>
				  <option value="sex">Sex</option>
				  <option value="phone">Mobile</option>
				</select>
	          <input type="text" class="form-control" placeholder="Search" ng-model="searchvalue" required>
	        </div>
	        <button type="submit" class="btn btn-default">Search Patient</button>
	      </form>
	      <ul class="nav navbar-nav navbar-right" ng-show="loggedin">
	        <li><a href="#">Hello {{name}}</a></li>
	        <!--li class="dropdown">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
	          <ul class="dropdown-menu">
	            <li><a href="#">Action</a></li>
	            <li><a href="#">Another action</a></li>
	            <li><a href="#">Something else here</a></li>
	            <li role="separator" class="divider"></li>
	            <li><a href="#">Separated link</a></li>
	          </ul>
	        </li-->
	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>


	<!--header>
		<div class="navbar navbar-inverse navbar-static-top"><a class="navbar-brand" href="" style="color:darkorange"><strong>Patient</strong>Info</a>
		<span class="pull-right" style="margin:15px; color: blue" ng-show="loggedin">Hello {{name}}</span>
		</div>
	</header-->
	<style>
		form.ng-submitted .ng-invalid {
			border-color: #FA787E;
		}
	</style>
</head>

<body ng-controller="PatientCtrlr">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
	  <form role="form" ng-show="!loggedin" ng-submit="Login()">
	    <legend>Doctor Login</legend>
	    <div class="form-group">
	      <label for="">Mobile Number</label>
	      <input type="text" class="form-control" id="" placeholder="Enter Mobile Number" ng-model="username"></div>
	    <div class="form-group">
	      <label for="">Password</label>
	      <input type="password" class="form-control" id="" placeholder="Enter Password" ng-model="password"></div>
	    <button type="submit" class="btn btn-primary">Submit</button>
	    <p style="color: darkorange">{{error}}</p>
	  </form>
	</div>


	<div class="container-fluid" ng-show="loggedin">
	<!-- statistics stars here -->
	<!--div class="panel-group">
		<div class="panel panel-info">
			<div class="panel-heading">Patient Statistics
			<span class="pull-right" style="color:blue"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="">Number Consulted: </a><span class="badge">{{stat.sold}}</span></span>
			<span class="pull-right" style="color:blue"><a href="">Number Alive: </a><span class="badge">{{stat.instock}}</span></span>
			</div>
		  <div class="panel-body">
			<div class="col-lg-2"><a href="" ng-click="searchText='Arrived'">Operated: <span class="badge">{{stat.arrived}}</span></a></div>
			<div class="col-lg-2"><a href="" ng-click="searchText='QC Done'">Post Op: <span class="badge">{{stat.qc}}</span></a></div>
			<div class="col-lg-2"><a href="" ng-click="searchText='FCT Done'">Other: <span class="badge">{{stat.fct}}</span></a></div>
		  </div>
		</div>
	</div--><!-- statistics ends here -->

	<!-- New Product Add starts with the  panel and then form -->
	<div class="panel-group" id="add" class="collapse" ng-show="showcontent == 1"><div class="panel panel-default">
	<div class="panel-heading">Add New Patient</div>
	<div class="panel-body">
	<form role="form" name="wp" ng-submit="wp.$valid && addPatient();" novalidate>
		<div class="row">
			<!--div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<label for="">Arrival Date</label>
				<input type="date" class="form-control" required ng-model="product.arrival_date">
			</div-->

			<div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
				<label>Name</label>
				<input type="text" class="form-control" ng-model="patient.name" required>
			</div>
			<div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
				<label>Age</label>
				<input type="text" class="form-control" ng-model="patient.age" required>
			</div>
			<div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
				<label>Sex</label>
				<input type="text" class="form-control" ng-model="patient.sex" required>
			</div>
			<div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
				<label>Phone</label>
				<input type="text" class="form-control" ng-model="patient.phone" required>
			</div>

			<div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
				<label>Arrival Date</label>
				<input type="date" class="form-control" ng-model="patient.arrivaldate" required>
			</div>
			<div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
				<label>Op No:</label>
				<input type="text" class="form-control" ng-model="patient.opnumber" required>
			</div>
			<div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
				<label>Organ Code</label>
				<input type="text" class="form-control" ng-model="patient.organcode" required>
			</div>
			<div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
				<label>Clinical Stage</label>
				<input type="text" class="form-control" ng-model="patient.clinicalstage" required>
			</div>

			<div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
				<label>Com.PS</label>
				<input type="text" class="form-control" ng-model="patient.comps" required>
			</div>
			<div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
				<label>Biopsy Grade</label>
				<input type="text" class="form-control" ng-model="patient.bygrade" required>
			</div>
			<div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
				<label>Tumor Marker</label>
				<input type="text" class="form-control" ng-model="patient.tumormarker" required>
			</div>
			<div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
				<label>Imaging Pics(upload)</label>
				<input type="file" class="form-control" ng-model="patient.bygrade">
			</div>

			<div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
				<label>Ne</label>
				<input type="text" class="form-control" ng-model="patient.ne" required>
			</div>
			<div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
				<label>Surgery Date</label>
				<input type="date" class="form-control" ng-model="patient.surgerydate" required>
			</div>
			<div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
				<label>Images</label>
				<input type="file" class="form-control" ng-model="patient.sdate" >
			</div>
			<div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
				<label>Gross Images</label>
				<input type="file" class="form-control" ng-model="patient.sdate" >
			</div></div>
		<div class="row">
		<button  type="submit" class="btn btn-success" style="margin:15px">Add Patient</button>
		<span ng-show="newpatient == 1" class="label label-warning">New Patient Added</span>
		</div>
	</form>
	

	</div></div></div><!-- form and 3 panel divs end --> <!-- add new product ends -->
	</div><!-- container ends here -->


    <div class="panel-group" id="details" ng-show="showcontent == 2">
    	<div class="panel panel-default">
			<div class="panel-heading">Patients</div>
			<table class="table table-hover">
			<tr>
				<th>Name</th>
				<th>Age</th>
				<th>Sex</th>
				<th>Op No</th>
				<th>Phone</th>
				<th>Clinical Stage</th>
			</tr>
			<tr ng-repeat="p in patients" ng-click="showPatient(p)">
				        <td>{{p.name}}</td>
				        <td>{{p.age}}</td>
				        <td>{{p.sex}}</td>
				        <td>{{p.opnumber}}</td>
				        <td>{{p.phone}}</td>
				        <td>{{p.clinicalstage}}</td>
				        
			</tr>
				    
			</table>
	    </div>
	</div>		

	<!-- Patient details-->

	<div class="panel-group" ng-show="showdetails==1"><div class="panel panel-default">
	<div class="panel-heading">Patient Details<button type="button" class="close" ng-click="showdetails=0"><span class="pull-right">X</span></button></div>
	<div class="panel-body">
	<form role="form" name="mp" ng-submit="mp.$valid && modifyPatient();" novalidate>
		<div class="row">
			<!--div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<label for="">Arrival Date</label>
				<input type="date" class="form-control" required ng-model="product.arrival_date">
			</div-->

			<div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
				<label>Name</label>
				<input type="text" class="form-control" ng-model="details.name" required>
			</div>
			<div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
				<label>Age</label>
				<input type="text" class="form-control" ng-model="details.age" required>
			</div>
			<div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
				<label>Sex</label>
				<input type="text" class="form-control" ng-model="details.sex" required>
			</div>
			<div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
				<label>Phone</label>
				<input type="text" class="form-control" ng-model="details.phone" required>
			</div>

			<!--div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
				<label>Arrival Date</label>
				<input type="date" class="form-control" ng-model="details.arrivaldate" required>
			</div-->
			<div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
				<label>Op No:</label>
				<input type="text" class="form-control" ng-model="details.opnumber" required>
			</div>
			<div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
				<label>Organ Code</label>
				<input type="text" class="form-control" ng-model="details.organcode" required>
			</div>
			<div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
				<label>Clinical Stage</label>
				<input type="text" class="form-control" ng-model="details.clinicalstage" required>
			</div>

			<div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
				<label>Com.PS</label>
				<input type="text" class="form-control" ng-model="details.comps" required>
			</div>
			<div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
				<label>Biopsy Grade</label>
				<input type="text" class="form-control" ng-model="details.bygrade" required>
			</div>
			<div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
				<label>Tumor Marker</label>
				<input type="text" class="form-control" ng-model="details.tumormarker" required>
			</div>
			

			<div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
				<label>Ne</label>
				<input type="text" class="form-control" ng-model="details.ne" required>
			</div>
			<!--div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
				<label>Surgery Date</label>
				<input type="date" class="form-control" ng-model="details.surgerydate" required>
			</div-->
			

		<button  type="submit" class="btn btn-success" style="margin:15px">Modify Patient</button>
	</form>
	

	</div></div></div> <!-- modify patient ends -->
	</div>

	<script src="assets/bower_components/angular/angular.min.js"></script>
	<script src="assets/bower_components/angular-cookies/angular-cookies.min.js"></script>
	<script src="assets/js/deepz.js"></script>
</body>
</html>