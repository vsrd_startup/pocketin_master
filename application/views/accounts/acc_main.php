<!DOCTYPE html>
<html lang="en" ng-app="piacc" ng-cloak>
<head>
  <title>Accounts - Refabd </title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>

  form.ng-submitted .ng-invalid {
	border-color: #FA787E;
  }

  th {
 	color: blue;
  }
  </style>
</head>
<body ng-controller="AccountsController">
<div class="container-fluid">
		<nav class="navbar navbar-default">
		  <div class="container-fluid">
		    <div class="navbar-header">
		      <a class="navbar-brand" href="#" style="color: darkorange;"">REFABD - Accounts</a>
		    </div>
		    <div class="pull-right">
		    <div class="dropdown" style="position:relative; margin-top: 15px;">
            <a class="dropdown-toggle" type="button" data-toggle="dropdown"  href="#">Hello {{name}},</a>
            <section class="dropdown-menu">
              <li>
                <a href=""  ng-click="logout()">Logout</a>
              </li>
            </section>
          </div>
          </div>
		    </div>
		</nav><br>
</div>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
	  <form role="form" ng-show="!loggedin" ng-submit="Login()">
	    <legend>Accounts Login</legend>
	    <div class="form-group">
	      <label for="">Mobile Number</label>
	      <input type="text" class="form-control" id="" placeholder="Enter Mobile Number" ng-model="username"></div>
	    <div class="form-group">
	      <label for="">Password</label>
	      <input type="password" class="form-control" id="" placeholder="Enter Password" ng-model="password"></div>
	    <button type="submit" class="btn btn-primary">Submit</button>
	    <p style="color: darkorange">{{error}}</p>
	  </form>
</div>

	<div class="container" ng-show="loggedin">
	    <ul class="nav nav-tabs">
	    	<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Vendor
	    	<span class="caret"></span></a>
	    	<ul class="dropdown-menu">
		    	<li><a data-toggle="tab" href="#addvendor">Add Vendor</a></li>
		    	<li><a data-toggle="tab" href="#viewvendor" ng-click="getVendorDetails()">View Vendors</a></li>
		    </ul>
		    </li>
	    	<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Expense
	    	<span class="caret"></span></a>
	    	<ul class="dropdown-menu">
		    	<li><a data-toggle="tab" href="#addexpense">Add Expense</a></li>
		    	<li><a data-toggle="tab" href="#viewexpense" ng-click="getExpenseDetails()">View Expense</a></li>
		    </ul>
		    </li>
		    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Procurement
		    <span class="caret"></span></a>
		    <ul class="dropdown-menu">
		    	<li><a data-toggle="tab" href="#addprocurement">Add Procurement</a></li>
		    	<li><a data-toggle="tab" href="#viewprocurement" ng-click="getProcurementDetails()">View Procurement</a></li>
		    </ul>
		    </li>
		    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Employee
		    <span class="caret"></span></a>
		    <ul class="dropdown-menu">
		    	<li><a data-toggle="tab" href="#addemployee">Add Employee</a></li>
		    	<li><a data-toggle="tab" href="#viewemployee" ng-click="getEmployeeDetails()">Employee Register</a></li>
		    	<li><a data-toggle="tab" href="#leave" ng-click="getLeaveDetails();getEmployeeDetails()">Add Leave</a></li>
		    	<li><a data-toggle="tab" href="#viewleave" ng-click="getLeaveDetails()">View Leave</a></li>
		    	
		    </ul>
		    </li>
	  	</ul>
	  	<br><br><br><br>

	  	<!-- Add Expense begins -->
		<div class="tab-content">
			<div class="tab-pane fade " id="addexpense">
				<form  class= "form" role="form" name="expense" ng-submit="expense.$valid && addExpense();" novalidate  >
					<div class="row ">
						<div class="form-group col-lg-2 ">
						<label for="">Date</label>
						<input type="date" class="form-control" id="" ng-model="ex.date" required>
						</div>

						<div class="form-group col-lg-2" >
							<label>Expense Type</label>
							<select class="form-control" ng-model="ex.expense_type" required>
							<option value="Inhouse Repair">Inhouse Repair</option>
							<option value="Delivery">Delivery</option>
							<option value="Warranty">Warranty</option>
							<option value="Technical">Technical</option>
							<option value="Salary">Salary</option>
							<option value="Bills">Bill/Rental</option>
							<option value="Warehouse">Warehouse</option>
							<option value="Marketing">Marketing</option>
							<option value="Other">Other</option>
							</select>
						</div>

						<div class="form-group col-lg-3" >
						<label>Description</label>
						<textarea class="form-control" rows="3" ng-model="ex.description" required></textarea>
						</div>

						<div class="form-group col-lg-2">
						<label for="">Amount</label>
						<input type="number" class="form-control" id="" ng-model="ex.amount" required>
						</div>

						<div class="form-group col-lg-2">
						<label for="">Mode</label>
						<!-- <input type="text" class="form-control" id="" ng-model="ex.by" required> -->
						<select class="form-control" ng-model="ex.by" required>
							<option value="Cash">Cash</option>
							<option value="Cheque">Cheque</option>
							<option value="Online Transfer">Online Transfer</option>
						</select>
						</div>

						<button  type="submit" class="btn btn-success col-lg-1" style="margin-top:25px" ng-disabled="pr.disablebutton">Add</button><br>
						<span ng-show="newexpense == 1" class="label label-success">Expense Added</span><br>
					</div>
				</form>
			</div> <!-- Add Expense ends here -->

			<!-- Add Procurement begins -->

			<div class="tab-pane fade" id="addprocurement">
				<form  class= "form" role="form" name="p" ng-submit="p.$valid && addProcurement();" novalidate >
					<div class="row ">
						<div class="form-group col-lg-2 ">
							<label for="">Procure Date</label>
							<input type="date" class="form-control" id="" ng-model="pr.date" required>
						</div>

						<div class="form-group col-lg-2" >
							<label>Vendor</label>
							<!-- <input type="text" class="form-control" ng-model="pr.from" required> -->
							<select class="form-control"  required ng-model="pr.from">
								<option value="{{vendor.id}}" ng-repeat="vendor in vendors">{{vendor.name}}</option>
							</select>
						</div>

						<div class="form-group col-lg-4" >
							<label>Item Description(Enter individual item with price)</label>
							<textarea class="form-control" rows="4" ng-model="pr.description" required></textarea>
						</div>

						<div class="form-group col-lg-1">
							<label for="">Total Quantity</label>
							<input type="number" class="form-control" id="" ng-model="pr.quantity" required>
						</div>

						<div class="form-group col-lg-2">
							<label for="">Total Cost</label>
							<input type="number" class="form-control" id="" ng-model="pr.cost" required>
						</div>
						<div>
							<button  type="submit" class="btn btn-success col-lg-1" style="margin-top:25px" ng-disabled="pr.disablebutton">Add</button><br.
							<span ng-show="newprocurement == 1" class="label label-success">Procurement Added</span><br>
						</div>
					</div>
				</form>
			</div><!-- Procurement ends -->


			<!-- Add Vendor begins -->

			<div class="tab-pane fade in active" id="addvendor">
				<form  class= "form" role="form" name="vendor" ng-submit="vendor.$valid && addVendor();" novalidate >
					<div class="row ">
						<div class="form-group col-lg-2 ">
							<label for="">Name</label>
							<input type="text" class="form-control" id="" ng-model="vr.name" required>
						</div>
						<div class="form-group col-lg-2">
							<label for="">Phone</label>
							<input type="text" class="form-control" id="" ng-model="vr.contact" required>
						</div>
						<div class="form-group col-lg-4" >
							<label>Address</label>
							<textarea class="form-control" rows="4" ng-model="vr.address" required></textarea>
						</div>
						<div class="form-group col-lg-2 ">
							<label for="">Product</label>
							<input type="text" class="form-control" id="" ng-model="vr.product">
						</div>
						<div class="form-group col-lg-2 ">
							<label for="">Security Deoposit</label>
							<input type="text" class="form-control" id="" ng-model="vr.securitydeposit">
						</div>
					</div><br><br>
						<div>
							<button style="margin-left: 50%; width: 150px"  type="submit" class="btn btn-success col-lg-1" ng-disabled="pr.disablebutton">Add</button><br>
							<span ng-show="newvendor == 1" class="label label-success">Vendor Added</span><br>
						</div>
					<!-- </div> -->
				</form>
			</div><!-- Add Vendor ends -->

			<!-- View Expense begins -->
			<div class="tab-pane fade" id="viewexpense">
				<button class="btn btn-success btn-sm pull-right" style="margin-top: -72px;" export-to-csv>Download</button>
				<table class="table" >
					<thead>
						<tr>
						<th>Date</th>
						<th>Type</th>
						<th>Description</th>
						<th>Amount</th>
						<th>Mode</th>
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="expense in expenses">
							<td>{{expense.date  | todate | date:"MMM dd, yyyy"}}</td>
							<td>{{expense.expense_type}}</td>
							<td><pre>{{expense.description}}</pre></td>
							<td>{{expense.amount}}</td>
							<td>{{expense.by}}</td>
						</tr>
					</tbody>
				</table>
			</div>
			<!-- View Expense ends -->


			<!-- View Procurement begins -->
			<div class="tab-pane fade" id="viewprocurement">
				<button class="btn btn-success btn-sm pull-right" style="margin-top: -72px;" export-to-csv csv-file-name="procurement.csv">Download</button>
				<table class="table" >
					<thead>
						<tr>
						<th>Date</th>
						<th>Vendor</th>
						<th>Description</th>
						<th>Quantity</th>
						<th>Cost</th>
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="p in procurements">
							<td>{{p.date | todate | date:"MMM dd, yyyy" }}</td>
							<td>{{p.from}}</td>
							<td><pre>{{p.description}}</pre></td>
							<td>{{p.quantity}}</td>
							<td>{{p.cost}}</td>
						</tr>
					</tbody>
				</table>
			</div>
			<!-- View Procurement ends -->

			<!-- View vendor begins -->
			<div class="tab-pane fade" id="viewvendor">
				<table class="table" >
					<thead>
						<tr>
						<th>Name</th>
						<th>Address</th>
						<th>Phone</th>
						<th>Security Deposit</th>
						<th>Product</th>
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="vendor in vendors">
							<td>{{vendor.name}}</td>
							<td><pre>{{vendor.address}}</pre></td>
							<td>{{vendor.contact}}</td>
							<td>{{vendor.securitydeposit}}</td>
							<td>{{vendor.product}}</td>
							
							
						</tr>
					</tbody>
				</table>
			</div>
			<!-- View vendor ends -->


			<!-- Leave tab begins -->
			<div class="tab-pane fade" id="leave">

				<form  class= "form" role="form" name="leave" ng-submit="leave.$valid && addLeave();" novalidate >
					<div class="row ">
						<div class="form-group col-lg-2 ">
							<label for="">Name</label>
							<select class="form-control"  required ng-model="att.emp_id">
								<option value="{{emp.id}}" ng-repeat="emp in employees">{{emp.name}}</option>
							</select>
						</div>
						<div class="form-group col-lg-2 ">
							<label for=""> Date</label>
							<input type="date" class="form-control" id="" ng-model="att.date" required>
						</div>

						<div class="form-group col-lg-4" >
							<label>Reason</label>
							<textarea class="form-control" rows="3" ng-model="att.reason" required></textarea>
						</div>

						<div class="form-group col-lg-2" >
							<label>No. Days</label>
							<input type ="number" class="form-control"  ng-model="att.days" required></textarea>
						</div>

						<div>
							<button style="margin-top:30px;" type="submit" class="btn btn-success col-lg-1"  ng-disabled="att.disablebutton">Add</button><br>
							<span ng-show="newleave == 1" class="label label-success">Leave Added</span><br>
						</div>	
					</div>
				</form>
				<br><br>
				</div>
				<!-- Leave tab ends -->

				<!-- View Leave tab begins-->

				<div class="tab-pane fade" id="viewleave">

				<div class="panel panel-info" >
			    <div class="panel-heading">Leave Details</div>
			    <div class="panel-body">
			    <div align="center">
					<!-- <form class="form-inline" >
		                  <div class="form-group">
		                    <input type="text" ng-model="search_date" class="form-control" >
		                  </div>
		                    <button type="submit" class="btn btn-default" ng-click="getLeaveDetails()">Submit</button>

		             </form> -->
		             </div><br>
		               <table class="table table-striped" >
						<thead>
							<tr>
								<th>Date</th>
								<th>Emp Id</th>
								<th>Name</th>
								<th>Reason</th>
								<th>No. Days</th>

							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="leave in leaves">
								<td>{{leave.date | todate | date:"MMM dd, yyyy" }}</td>
								<td>{{leave.id}}</td>
								<td>{{leave.empname}}</td>
								<td>{{leave.reason}}</td>
								<td>{{leave.days}}</td>

							</tr>
						</tbody>
					</table>
				</div>
			</div>
			</div>



			<!-- View Leave tab ends -->

			<!-- Employee Register begins -->
			<div class="tab-pane fade" id="viewemployee">
					<table class="table" >
						<thead>
							<tr>
								<th>Id</th>
								<th>Name</th>
								<th>Mobile</th>
								<th>Designation</th>
								<th ng-show = "superuser == 1">CTC</th>
								<th>Type</th>
								<th>Date Joined</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="emp in employees">
								<td>{{emp.id}}</td>
								<td>{{emp.name}}</td>
								<td>{{emp.contact}}</td>
								<td>{{emp.role}}</td>
								<!--td>{{emp.ctc_month}}</td-->
								<td ng-show = "superuser == 1">{{emp.ctc_month}}</td>
								<td>{{emp.type}}</td>
								<td>{{emp.join_date | todate | date:"MMM dd, yyyy" }}</td>
							</tr>
						</tbody>
					</table>
			</div>


			<!-- Employee Register ends -->

			<!-- Add Employee begins -->
			<div class="tab-pane fade" id="addemployee">
				<form  class= "form" role="form" name="addemp" ng-submit="addemp.$valid && addEmployee();" novalidate >
					<div class="row " style="margin-left:260px" >
						<div class="form-group col-lg-3 ">
						<label for="">Name</label>
						<input type="text" class="form-control" id="" ng-model="emp.name" required>
						</div>

						<div class="form-group col-lg-3" >
						<label>Mobile</label>
						<input type="text" class="form-control" ng-model="emp.contact" required>
						</div>

						<div class="form-group col-lg-3" >
						<label>Role</label>
						<input type="text" class="form-control" ng-model="emp.role" required>
						</div>
					</div>
					<div class="row" style="margin-left:260px">
						<div class="form-group col-lg-3">
						<label for="">CTC/Month</label>
						<input type="number" class="form-control" id="" ng-model="emp.ctc_month" required>
						</div>

						<div class="form-group col-lg-3">
						<label for="">Type</label>
						<select class="form-control" ng-model="emp.type" required>
							<option value="Fulltime">Fulltime</option>
							<option value="Contract">Contract</option>
						</select>	
						</div>

						<div class="form-group col-lg-3">
						<label for="">Join Date</label>
						<input type="date" class="form-control" id="" ng-model="emp.join_date" required>
						</div>
					</div>
					<div class="row">	

						<button  type="submit" class="btn btn-success col-lg-1" style="margin-left:500px" ng-disabled="pr.disablebutton">Add</button>
						<span ng-show="newemployee == 1" class="label label-success">Employee Added</span><br>
					</div>	

					</div>
				</form>
			</div>
			<!-- Add Employee ends -->

		
		</div>







	</div>
	<script src="assets/bower_components/angular/angular.min.js"></script>
	<script src="assets/bower_components/angular-cookies/angular-cookies.min.js"></script>
	<script src="assets/js/accounts/acc.js"></script>
</body>
</html>
