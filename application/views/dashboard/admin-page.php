<html ng-app="piadmin" ng-cloak>
<head>
  <title>Admin Page for Refabd</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimal-ui" />
  <meta name="apple-mobile-web-app-status-bar-style" content="yes" />
  <link rel="stylesheet" href="../assets/css/bootstrap.min.css" />
  <link rel="stylesheet" href="../assets/css/common.min.css" />
  <style>
  .sellform {
    margin-bottom:50px;
    margin-top:50px;
}
div.sale div.form-group label:nth-child(1) {
    font-size:13px;
    font-weight: 200;
    color:grey;
  }
div.sale div.form-group label:nth-child(3) {
    font-size:16px;
    font-weight: 400;
    color:darkorange;
  }
.imagediv {
height: auto;
}
.title,.status,.action {
margin-top: 40px;
}
.navbar-brand{
color: #000;
}
a,
a:visited{
color: #1a1a1a;
}
a:hover,
a:active,
a:focus{
color: #47C9AF;
}


.yellow {
color: #f8bc3b;
}
.green {
color: #43a047;
}
.red {
color: red;
}
.blue {
color: blue;
}

header{
background: #F7F7F7;
border-bottom: 1px solid #DEDEDE;
position: fixed;
top: 0;
left: 0;
right: 0;
z-index: 3;
}
.main-container {
margin-top: 90px;
}
.dealername {
color: orange;
float: right;
}
.dealername .dropdown > a{
padding: 15px;
display: block;
}
.dealername .dropdown{
position: relative;
}
.dealername .dropdown-menu{
position: absolute;
right: 0;
width: 140px;
padding: 15px;
left: inherit;
}
.clear {
clear: both;
}
.item {
border-bottom: 1px solid lightgrey;
padding-top: 5px;
padding-bottom: 5px;
}
.quality {
padding-bottom: 20px;
}

body {
background-color: #fff;
}

form.ng-submitted .ng-invalid {
border-color: #FA787E;
}

.menu {
padding: 10px;
margin-top: 17px;
margin-right: 10px;
border-right: 1px solid lightgrey;
}

.nav-pills>li a,
.menu a:visited{
color: #1a1a1a;
background: #fff;
border: 1px solid #fff;
}
.menu a:hover,
.menu a:active,
.menu a:focus,
.nav-pills>li.active>a, .nav-pills>li.active>a:focus, .nav-pills>li.active>a:hover{
color: #47C9AF;
background: #fff;
border-radius: 3px;
border: 1px solid #47C9AF;
}

.model {
border: 1px solid lightgrey;
margin-top: 10px;
margin-bottom: 10px;
padding-left: 10px;
}

.past-orders-head ul li{
display: inline-block;
}

.past-order-products  .order-id{
font-weight: 500;
padding: 15px;
background: #F7F7F7;
}

img {
  max-width: 100px;
  height: auto;
}


.past-order-products .product-title{
font-size: 12px!important;
margin: 0;
}

.past-order-products .cart-thumb img{
width: auto!important;
height: inherit;
display: block;
margin: 0 auto;
}

.past-order-item-details{
padding: 5px 5px 15px 0;
padding-top: 15px;
border-bottom: 1px solid #DEDEDE;
}

.past-order-footer div{
/*border-top: 1px solid #DEDEDE;*/
padding: 10px 15px;
}
.border {
border: 1px solid grey;
margin-bottom: 5px;
}


.past-order-products .product-description{
margin: 0;
}

.past-order-item .cart-item-details{
padding: 0 15px;
}

.past-order-item.cart-item{
margin: 15px 0;
/*padding: 10px 15px;*/
}

div#slots {
  font-size: 15px;
  font-weight: 300;
}

div#slots .slotorder {
    display: block;
    margin-bottom: 10px;
}
div#slots .slotorderpanel {
   border-bottom: 1px solid #e2e1dd;
  }
/*
.ng-invalid.ng-dirty {
border-color:#FA787E
}
.ng-valid.ng-dirty {
border-color:#78FA89
}
*/

</style>

</head>

<body ng-controller="AdminController as ctrl">
  <header>
    <div class="container-fluid">
      <!-- Login Div -->
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
          <a class="navbar-brand" href="">
            REFABD
            - Admin
          </a>
        </div>
        <div ng-show="xhr">
          <label style="color: darkorange; font-size: 20px; position:absolute">Loading...</label>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 text-right dealername" ng-show="adminLogged">
          <div class="dropdown">
            <a class="dropdown-toggle" type="button" data-toggle="dropdown"  href="#">Hello, {{dealername}}</a>
            <section class="dropdown-menu">
              <li>
                <a href="/" ng-click="logout()">Logout</a>
              </li>
            </section>
          </div>
        </div>
      </div>
    </div>
  </header>

  <div class="main-container">
    <div class="container-fluid">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
        <form role="form" ng-show="!adminLogged" ng-submit="Login()">
          <legend>Admin Login</legend>

          <div class="form-group">
            <label for="">Mobile Number</label>
            <input type="text" class="form-control" id="" placeholder="Enter Mobile Number" ng-model="username"></div>
          <div class="form-group">
            <label for="">Password</label>
            <input type="password" class="form-control" id="" placeholder="Enter Password" ng-model="password"></div>
          <button type="submit" class="btn btn-primary">Submit</button>
          <p style="color: darkorange">{{error}}</p>
        </form>
      </div>

      <div class="row clear" ng-show="adminLogged" ng-init="LoadPending()">
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
          <ul class="nav nav-pills nav-stacked menu">
            <li class="active">
              <a data-toggle="pill" href="#pendingapproval" ng-click="LoadPending()">Pending Approval</a>
            </li>
            <li>
              <a data-toggle="pill" href="#custorder" ng-click="loadCustOrder()">Customer Orders</a>
            </li>
            <li>
              <a data-toggle="pill" href="#allproducts" ng-click="loadAll()">All Products</a>
            </li>
            <li>
              <a data-toggle="pill" href="#usercart" ng-click="loadCart()">User Cart</a>
            </li>
            <li>
              <a data-toggle="pill" href="#userwishlist" ng-click="loadWishlist()">User Wishlist</a>
            </li>
            <!--li>
            <a data-toggle="pill" href="#productimages">Product Images</a>
          </li-->
          <li>
            <a data-toggle="pill" href="#userseen" ng-click="loadUserSeen()">User Seen Products</a>
          </li>
          <li>
            <a data-toggle="pill" href="#producthotness" ng-click="loadHotness()">Product Hotness</a>
          </li>
          <li>
            <a data-toggle="pill" href="#complaints" ng-click="getPendingTickets();">Post Sale Issues</a>
          </li>
          <li>
            <a data-toggle="pill" href="#calls" ng-click="getAllCalls()">Customer calls</a>
          </li> </li>
          <li>
            <a data-toggle="pill" href="#OrderStatistics" ng-click="getStatistics(0)">User/Order Statistics</a>
          </li>
          <li>
            <a data-toggle="pill" href="#Approvesale" ng-click="getPendingSell()">Sale Approval</a>
          </li>
          <!-- <li>
            <a data-toggle="pill" href="#Procurement" ng-click="getProcurementDetails()">Procurement</a>
          </li> -->
          <li>
            <a data-toggle="pill" href="#buyback" ng-click="getBuyBackDetails()">Buyback</a>
          </li>
          <li>
            <a data-toggle="pill" href="#coupons" >Gift Coupons</a>
          </li>
          <li>
            <a data-toggle="pill" href="#slots" ng-click="getDeliveryDetails()" >Delivery Slots</a>
          </li>
          <li>
            <a data-toggle="pill" href="#docs" ng-click=""> Files </a>
          </li>
          <li>
            <a data-toggle="pill" href="#conversion" ng-click="getConversion()"> Conversion </a>
          </li>
           <li>
            <a data-toggle="pill" href="#callback" ng-click="getCallBack()"> Callback </a>
          </li>
          <!-- <li>
            <a data-toggle="pill" href="#addsale" ng-click=""> Sell </a>
          </li> -->
          <!--li>
          <a data-toggle="pill" href="#modifyprod">Modify Products</a>
        </li>
        <li>
          <a data-toggle="pill" href="#addcat">Add Product Category</a>
        </li>
        <li>
          <a data-toggle="pill" href="#addfilter">Add Filter Type</a>
        </li>
        <li>
          <a data-toggle="pill" href="#addtechspec">Add Technical Spec</a>
        </li>
        <li>
          <a data-toggle="pill" href="#addprod">Add New Product</a>
        </li-->
      </ul>
    </div>
    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
      <div class="tab-content">
        <div class="tab-pane fade in active" id="pendingapproval">
          <h3>Pending Approval</h3>
          <hr/>
          <div class="row item" ng-repeat="item in pa_list">
            <div class="col-lg-3">
              <div class="imagediv" ng-repeat="im in item.images" style="margin-right:5px;float:left">
                <a target="_blank" ng-href="../assets/images/dealerpending/{{im.image_full}}"><img width="75px" ng-src="../assets/images/dealerpending/{{im.image_full}}" alt=""/ style="max-width: 75px; height:auto"></a>
              </div>
            </div>
            <form name="appvform" ng-submit="appvform.$valid && ApproveProduct(item)" class="col-lg-9">
              <legend>
                Listed by
                <span style="color: green">{{item.dealer_name}}</span>
              </legend>
              <label for="" style="font-size: 16px;margin-bottom: 10px;color: #da7c00;">Warehouse ID: {{item.warehouseid}}</label>
              <div class="form-group">
                <label for="">Product Title:</label>
                <input type="text" class="form-control" id="" placeholder="" ng-model="item.product_title" required/>
              </div>
              <div class="form-group">
                <label for="">Product Description:</label>
                <input type="text" class="form-control" id="" placeholder="" ng-model="item.product_description" required/>
              </div>
              <div class="form-group">
                <label>Sale Price:</label>
                <input type="text" class="form-control" id="" placeholder="" ng-model="item.saleprice" required/>
              </div>
              <div class="form-group">
                <label>Warehouse Id:</label>
                <input type="text" class="form-control" id="" placeholder="" ng-model="item.warehouseid" required />
              </div>
              <div class="form-group">
                <label>Market Price:</label>
                <input type="text" class="form-control" id="" placeholder="" ng-model="item.mrp" required/>
              </div>
              <div class="form-group">
                <label>Long Description:</label>
                <textarea rows="5" class="form-control" id="" placeholder="" ng-model="item.longdescription" required></textarea>

              </div>
              <div class="form-group">
                <label>Technical Spec:</label>
                <textarea rows="5" class="form-control" id="" placeholder="" ng-model="item.technicalspec" required></textarea>
              </div>
              <div class="form-group">
                <label>Condition Spec:</label>
                <textarea rows="5" class="form-control" id="" placeholder="" ng-model="item.conditionspec" required></textarea>
              </div>
              <div class="row">
                <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-4" ng-init="item.isfactory =  item.isfactory == 1 ? true : false">
                  <div class="radio">
                    <label>
                      <input type="checkbox" name="" id="input" value="" ng-model="item.isfactory">Unused & New</label>
                  </div>
                </div>
              </div>

              <button type="submit" class="btn btn-danger" ng-hide="item.approving == 1 || item.success == 1">Approve</button>
              <p style="color:grey; font-size: 14px;" ng-show="item.approving == 1 && item.failed == 0">Approving...</p>
               <p style="color:red; font-size: 14px;" ng-show="item.failed == 1 && item.invalidid == 1">Enter valid warehouse id</p>
              <p style="color:red; font-size: 14px;" ng-show="item.failed == 1">Failed</p>
             
              <p style="color:darkorange; font-size: 14px;" ng-show="item.approving == 0 && item.success == 1">Product Added Successfully</p>
            </form>
          </div>
        </div>

        <div class="tab-pane fade in" id="custorder">
          <h3>Customer Orders</h3>

          <!-- Search order -->
          <div class="container" ng-init="scat = 'Order ID'" style="margin-top: 15px; ">
              <div class="row">
                  <div style="width: 500px;">
                  <div class="input-group">
                          <div class="input-group-btn search-panel">
                              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <span id="search_concept">{{scat}}</span> <span class="caret"></span>
                              </button>
                              <ul class="dropdown-menu" role="menu">
                                <li><a href="" ng-click="scat = 'Order ID'">Order ID</a></li>
                                <li><a href="" ng-click="scat = 'Mobile'">Mobile</a></li>
                                <!--li><a href="" ng-click="scat = 'Name'">Name</a></li-->
                                <!--li><a href="#">Less than < </a></li>
                                <li class="divider"></li>
                                <li><a href="#all">Anything</a></li-->
                              </ul>
                          </div>
                          <input type="text" class="form-control" name="x" placeholder="Search Order" ng-model="stext">
                          <span class="input-group-btn">
                              <button class="btn btn-default" type="button" style="padding:9px;" ng-click="searchOrder();"><span class="glyphicon glyphicon-search"></span></button>
                          </span>
                      </div>
                  </div>
            </div>
          </div> <!-- search ends here -->

          <div style="margin-top: 20px; margin-left: 25%; margin-right: 25%;">
            <ul class="list-inline">
              <li class="list-group-item">
                <a href="" ng-click="Filterme(0)">Active</a>
              </li>
              <!--li class="list-group-item">
              <a href="" ng-click="Filterme(1)">Placed</a>
            </li>
            <li class="list-group-item">
              <a href="" ng-click="Filterme(2)">Confirmed</a>
            </li>
            <li class="list-group-item">
              <a href="" ng-click="Filterme(3)">Shipped</a>
            </li-->
            <li class="list-group-item">
              <a href="" ng-click="Filterme(1)">Delivered</a>
            </li>
            <li class="list-group-item">
              <a href="" ng-click="Filterme(2)">Cancelled</a>
            </li>
          </ul>
        </div>
        <div class="past-order-products">
          <ul>
            <li class="cart-item  past-order-item border" ng-repeat="order in corders">
              <div class="order-id">
                <div class="row">
                  <div class="text-left col-xs-4">
                    <a style="color: darkorange">Order ID: {{order.order_id}}</a><br>
                      <div ng-show="order.pendingemi == 1">
                        <label style="color: black;font-size: 15px;background: #f78b8b;padding: 2px;margin-top: 3px;" ng-show="order.pendingemi == 1">PENDING EMI</label>
                        <a href="" style="color: #029810;text-decoration: underline" ng-click="getEmiStatus(order)">Get status</a>
                      </div>
                      <div style="margin-top: 5px" ng-show="order.showemistatus == 1">
                        <label for="">Status: {{order.emistatus}}</label><br>
                        <button class="btn btn-default" ng-click="ZestDeliver(order.order_id)">Delivered</button>
                        <button class="btn btn-default" ng-click="ZestRefund(order.order_id, order.amount)">Refund EMI</button>
                        <button class="btn btn-default" ng-click="ZestCancel(order.order_id)">Cancel EMI</button>
                      </div>
                  </div>
                  <div class="col-lg-8" ng-init="order.slotmodify=0">
                    <span style="color: #1da229; font-size: 16px" ng-show="order.slotbooked == 1">[Delivery On] {{order.slot.date | date :  "dd MMM y"}}  {{order.slot.time | toslottime}}</span>
                    <button type="button" class="btn btn-danger" ng-show="order.slotbooked != 1" ng-click="order.slotmodify=1">SET SLOT
                    </button>
                    <button type="button" style="background: transparent; border: 0;color: #025802;text-decoration: underline;" ng-click="order.slotmodify=1" ng-show="order.slotbooked == 1">MODIFY SLOT
                    </button>
                    <form name="dslot" novalidate ng-submit="dslot.$valid && bookDeliverySlot(order)" ng-show="order.slotmodify==1">
                      <div class="row" style="padding: 10px;border: 1px solid #dedede;margin: 10px;">
                      <div class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <label for="">Slot</label>
                        <input type="date" class="form-control" required ng-model="order.s2">
                      </div>
                      <div class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12" >
                        <label>Time</label>
                        <select name="" id="input" class="form-control" required ng-model="order.t2">
                          <option value="t1">AM: 7.30 - 9.30</option>
                          <option value="t2">AM: 8.30 - 10.30</option>
                          <option value="t3">AM: 9.30 - 11.30</option>
                          <option value="t4">AM: 10.30 - 12.30</option>
                          <option value="t5">PM: 4.00 - 6.00</option>
                          <option value="t6">PM: 5.00 - 7.00</option>
                          <option value="t7">PM: 6.00 - 8.00</option>
                          <option value="t8">PM: 7.00 - 9.00</option>
                        </select>
                      </div>
                      <div class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12" >
                       <button type="submit" class="btn btn-success" style="margin-top: 20px" ng-disabled="slotbooking">Book Slot</button>
                      </div>
                      <p ng-show="alreadybookedorder != 0" style="color:red; font-size: 14px; font-weight:300;line-height: 19px!important;">[FAILED:] Slot Already Booked for Order ID {{alreadybookedorder}}</p>
                    </div>
            </form>
                </div>
                  <div class="text-right col-xs-4 pull-right" ng-init="expand=1">
                    <a href="" style="color: blue" ng-click="expand = (expand == 1 ? 0 : 1)"> <span class="glyphicon glyphicon-menu-down" ng-show="expand == 0"  ></span> <span class="glyphicon glyphicon-menu-up" ng-show="expand == 1"></span></a>
                  </div>
                </div>
              </div>

              <div class="past-order-item-details" ng-repeat="item in order.items" ng-class="item.iscancelled ? 'cancelleditem' : ''" ng-show="expand == 1">
                <div class="row">
                  <div class="cart-thumb col-lg-2 col-xs-12 col-md-6 col-sm-12">
                    <a ng-href="../assets/images/products/normal/{{item.productimage}}">
                      <img ng-src="../assets/images/products/thumbnail/{{item.productimage}}"></a>
                  </div>
                  <div class="cart-item-details col-lg-3 col-xs-12 col-md-6 col-sm-12">
                    <h1 class="product-title">Product Id: {{item.productid}}<br>{{item.productname}}</h1>
                    <!--span class="quality-index">
                    <span class="icon pocketin-icon-quality-index"></span>
                    4.2
                  </span-->
                  <span class="product-description">{{item.productdescription}}</span>
                  <!--div class="colors-available">
                  <label class="inline">Status: {{item.status}}</label>
                </div-->
                <div class="delivered-date" ng-class="item.iscancelled ? 'cancelleditem' : ''"> <b>{{item.status}}</b>
                </div>
                <h4 style="color: black;">
                  <span>&#x20B9;</span>
                  {{item.amount}}
                </h4>
                <h4 ng-show="item.ispaid == 1" style="color: orange;">Amount Paid</h4>
                <h4 ng-show="item.ispaid == 0" style="color: red;">Cash On Delivery</h4><br><br>
                <h6 ng-show="order.coupon != null">Coupon Applied: <span style="color:red">{{order.coupon}}</span></h6>
              </div>
              <div class="col-lg-4 col-xs-12 col-md-12 col-sm-12">
                <h5 style="padding-bottom: 5px;">Delivery Address</h5>
                <p class="name">{{order.deliveryaddress.name}}</p>
                <p class="phone">{{order.deliveryaddress.phonenumber}}</p>
                <p class="">
                  {{order.deliveryaddress.address_line_one}}, {{order.deliveryaddress.address_line_two}}
                </p>
                <p class="">
                  {{order.deliveryaddress.city}}, {{order.deliveryaddress.state}}
                </p> <b class="landmark">{{order.deliveryaddress.landmark}} </b>
                <p class="">
                  {{order.deliveryaddress.email}}
                </p>
              </div>
              <div class="col-lg-3 col-xs-12 col-md-12 col-sm-12">
                <!--a  href="#refers" ng-click="checkReferral(order.order_id);">Check Referral Bonus</a-->
                <button type="button" class="btn btn-link " data-toggle="modal" data-target="#refModal" ng-click="checkReferral(order.order_id)">Check Referral Bonus</button>
              </div>
              <div class="col-lg-3 col-xs-12 col-md-12 col-sm-12">
                <button ng-disabled="item.disable" class="btn btn-default" ng-click="performAction(item.action, item)" ng-confirm-click ng-show="item.action != 'Order Executed' && item.action != 'Cancellation Processed'">{{item.action}}</button>
                <button ng-hide="item.action != 'Mark Confirmed'" class="btn btn-default" ng-confirm-click ng-click="performAction('Mark Rejected', item)" style="margin-top: 5px;">Mark Rejected</button>
                <p class="label" style="font-size: 10px; padding: 10px;" ng-show="item.action == 'Order Executed' || item.action == 'Cancellation Processed'" ng-class="(item.action == 'Order Executed') ? 'label-success' : 'label-warning'">{{item.action}}</p>
              </div>

              <div style="margin-top:30px;" ng-show="item.action == 'Order Executed'">
                <a ng-href="../{{item.invoice}}" style="color:  #47C9AF">View Invoice</a>
              </div>
              <button class="btn btn-default" ng-hide="item.action == 'Cancellation Processed' || item.canstatus == 'Done'" ng-confirm-click ng-click="cancelOrderItem(order.order_id, item)" style="margin-top: 5px;" ng-hide="canstatus == 'Done'">Cancel Order</button>
              <label for="">{{item.canstatus}}</label>

            </div>
          <!--Modal Referral-->
          <div id="refModal" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <!--button type="button" class="close" data-dismiss="modal">&times;</button-->
                <h4 class="modal-title">Referrals <span class="pull-right"> Total Discount:{{discount}} </span></h4>
              </div>
              <div class="modal-body">
                <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>Referred</th>
                        <th>Status</th>
                        <th>Discount</th>
                      </tr>
                    </thead>
                  <tbody>
                    <tr ng-repeat="referral in referrals">
                        <td>{{referral.rname}}</td>
                        <td>{{referral.ruser_id ? 'Joined' : 'Pending'}}</td>
                        <td>{{referral.ruser_id ? '100' : '0'}}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>

          </div>
</div>
<!--Modal ends -->

          </div>
          <div class="row past-order-footer row" style="padding:0 15px;">
            <div class="col-xs-6 text-left">Order Time: {{order.orderdate}}</div>
            <div class="col-xs-6 product-price text-right">
              Amount to Get:
              <span style="color:darkorange;">
                <span>&#x20B9;</span>
                {{order.amount}}
              </span>
            </div>
          </div>
          <div ng-show="expand == 1">
              <div ng-show="order.slotbooked" style="margin: 10px">
                <!-- <div class="col-lg-4">
                <span style="color: #1da229; font-size: 16px">[SLOT 1] {{order.slot.date1 | date :  "dd MMM y"}}  {{order.slot.time1 | toslottime}}</span>
                </div>
                 <div class="col-lg-4">
                  <span style="color: #1da229; font-size: 16px">[SLOT 2] {{order.slot.date2 | date :  "dd MMM y"}}  {{order.slot.time2 | toslottime}}</span>
                </div> -->
              </div> <br><br>
            <form ng-submit="saveComment(order.order_id, order.comment);">
              <div class="row">
                <div class="col-lg-6 col-md-6" style="margin-left: 10px;!important">
                  <textarea rows="2" class="form-control" ng-model="order.comment"></textarea>
                </div>
                <div class="col-lg-4 col-md-4" style="margin-top: 8px;">
                  <button type="submit" class="btn btn-warning">Save Comment</button>
                  <p>{{commentdone}}</p>
                </div>
              </div>
            </form>
          </div>
        </li>
      </ul>
    </div>
  </div>

  <div class="tab-pane fade in" id="allproducts">
    <h3>All Products</h3>
    <hr/>

    <!-- Search product -->
          <div class="container" style="margin-top: 15px; ">
              <div class="row">
                  <div style="width: 500px;margin: 30px";">
                  <div class="input-group">
                          <div class="input-group-btn search-panel">
                              <!--button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <span id="search_concept">{{scat}}</span> <span class="caret"></span>
                              </button>
                              <ul class="dropdown-menu" role="menu">
                                <li><a href="" ng-click="scat = 'Product ID'">Product ID</a></li>
                                <li><a href="" ng-click="scat = 'Product Name'">Name</a></li>
                                <!--li><a href="" ng-click="scat = 'Name'">Name</a></li-->
                                <!--li><a href="#">Less than < </a></li>
                                <li class="divider"></li>
                                <li><a href="#all">Anything</a></li-->
                              </ul-->
                          </div>
                          <input type="text" class="form-control" name="x" placeholder="Enter Product Id" ng-model="pid">
                          <span class="input-group-btn">
                              <button class="btn btn-default" type="button" style="padding:9px;" ng-click="searchProduct();"><span class="glyphicon glyphicon-search"></span></button>

                          </span>
                      </div>
                  </div>
            </div>
          </div> <!-- search ends here -->

    <div class="row item" ng-repeat="item in all_list">
      <div class="col-lg-3" ng-init="item.hidden == 1 ? item.hidestatus = 'Hidden' : item.hidestatus = 'Hide Product from Customer';">
        <div class="imagediv cart-thumb">
          <img width="150px" ng-src="../assets/images/products/thumbnail/{{item.images[0].image_full}}" alt=""/>
        </div>
      </div>
      <label style="margin: 10px;">{{item.product_title}}, ID: {{item.productitem_id}}</label>
      <button type="button" class="btn btn-default" ng-click="item.showdetails = 1;" ng-hide="item.showdetails == 1">Show Details</button>
      <button type="button" class="btn btn-default" ng-click="item.showdetails = 0;" ng-show="item.showdetails == 1">Hide Details</button>
      <button type="button" class="btn btn-default" ng-confirm-click ng-click="hideProductFromCustomer(item)" ng-disabled="item.hidden == 1">{{item.hidestatus}}</button>
      <button type="button" class="btn btn-default" ng-confirm-click ng-click="unHideProductFromCustomer(item)" ng-show="item.hidden == 1">Unhide</button>
      <!-- <button type="button" class="btn btn-default" ng-confirm-click ng-click="markSold(item)"  ng-disabled="item.sold == 1">Mark Sold</button> -->
      <!-- <button type="button" class="btn btn-default" ng-confirm-click ng-click="markUnSold(item)" ng-show="item.sold == 1">Mark UnSold</button> -->
      <form name="modifyform" ng-submit="modifyform.$valid && ModifyProduct(item)" class="col-lg-9" ng-show="item.showdetails">
        <legend>
          Listed by
          <span style="color: green">{{item.dealer_name}}</span>
        </legend>
        <div class="form-group">
          <label for="">Product Title:</label>
          <input type="text" class="form-control" id="" placeholder="" ng-model="item.product_title" required/>
        </div>
        <div class="form-group">
          <label for="">Product Description:</label>
          <input type="text" class="form-control" id="" placeholder="" ng-model="item.product_description" required/>
        </div>
        <div class="form-group">
          <label>Sale Price:</label>
          <input type="text" class="form-control" id="" placeholder="" ng-model="item.saleprice" required/>
        </div>
        <div class="form-group">
          <label>Market Price:</label>
          <input type="text" class="form-control" id="" placeholder="" ng-model="item.mrp" required/>
        </div>
        <div class="form-group">
          <label>Long Description:</label>
          <textarea rows="5" class="form-control" id="" placeholder="" ng-model="item.longdescription" required></textarea>

        </div>
        <div class="form-group">
          <label>Technical Spec:</label>
          <textarea rows="5" class="form-control" id="" placeholder="" ng-model="item.technicalspec" required></textarea>
        </div>
        <div class="form-group">
          <label>Condition Spec:</label>
          <textarea rows="5" class="form-control" id="" placeholder="" ng-model="item.conditionspec" required></textarea>
        </div>
        <div class="row">
          <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-4" ng-init="item.isfactory =  item.isfactory == 1 ? true : false">
            <div class="radio">
              <label>
                <input type="checkbox" name="" id="input" value="" ng-model="item.isfactory">Unboxed</label>
            </div>
          </div>
        </div>

        <button type="submit" class="btn btn-danger" ng-hide="item.approving == 1 || item.success == 1">Modify</button>
        <p style="color:grey; font-size: 14px;" ng-show="item.approving == 1 && item.failed == 0">Modifying...</p>
        <p style="color:red; font-size: 14px;" ng-show="item.failed == 1">Failed</p>
        <p style="color:darkorange; font-size: 14px;" ng-show="item.approving == 0 && item.success == 1">Product Modified Successfully</p>
      </form>
    </div>
    <div class="row pull-right" style="margin-top: 10px; margin-bottom: 10px;">
      <button type="button" class="btn btn-primary">Previous</button>
      <button type="button" class="btn btn-primary" style="padding-left: 10px;">Next</button>
    </div>
  </div>

  <div class="tab-pane fade in" id="usercart">
    <h3>User Cart</h3>
    <!--div class="checkbox">
      <label>
        <input type="checkbox" value="0" ng-model="showsold" ng-init="showsold=0">
        Show Purchsed Cart
      </label>
    </div-->
    <div ng-repeat="item in cart" style="border-bottom: 1px solid lightgrey; margin-bottom: 5px; padding: 5px;">
    <div class="row">
        <div class="col-lg-3">
          <div class="imagediv cart-thumb">
            <img width="150px" ng-src="../assets/images/products/thumbnail/{{item.image}}" alt=""/>
          </div>
        </div>
        <div class="col-lg-3">
          <label><a ng-href="../product/admin/{{item.id}}/product">{{item.name}} ({{item.id}})</a></label>
        </div>
        <div class="col-lg-3">
          <label>by {{item.user}}</label>
          <br>
          <label>{{item.phonenumber}}</label>
          <br>
          <label>{{item.email}}</label>
        </div>
        <div class="col-lg-3">
          <label for="">{{item.timestamp | todate | date:"MMM d, y 'at' h:mm a"}}</label>
          <label>User {{item.status}}</label>
          <br>
          <label>Item {{item.soldout == 1 ? 'Soldout' : 'Available'}}</label>
        </div>
    </div>
    <form role="form" ng-submit="saveCartComment(item)" style="margin-top: 10px;"">
    <label>Comment:</label>
        <div class="row">
        <div class="col-lg-8 col-md-8">
          <textarea name="" id="input" class="form-control" rows="3" required="required" ng-model="item.comment"></textarea>
        </div>
          <button type="submit" class="btn btn-primary" style="margin-top:10px;">Save</button>
        </div>
      </form>
    </div>
  </div>

  <div class="tab-pane fade in" id="userwishlist">
    <h3>User Wishlist</h3>
    <div class="row" ng-repeat="item in wish" style="border-bottom: 1px solid lightgrey; margin-bottom: 5px; padding: 5px;">
      <div class="col-lg-3">
        <div class="imagediv cart-thumb">
          <img width="150px" ng-src="../assets/images/products/thumbnail/{{item.image}}" alt=""/>
        </div>
      </div>
      <div class="col-lg-3">
        <label>{{item.name}}</label>
      </div>
      <div class="col-lg-3">
        <label>by {{item.user}}</label>
        <br>
        <label>{{item.phonenumber}}</label>
        <br>
        <label>{{item.email}}</label>
      </div>
      <div class="col-lg-3">
        <label for="">{{item.timestamp | todate | date:"MMM d, y 'at' h:mm a"}}</label>
        <label>{{item.status}}</label>
        <br>
        <label>Item {{item.soldout == 1 ? 'Soldout' : 'Available'}}</label>
      </div>
      
      <div><br><br>

<form role="form" ng-submit="saveWishlistComment(item)" style="margin-top: 30px;"">
    <label>&nbsp Comment:</label>
        <div class="row">
        <div class="col-lg-8 col-md-8">
          <textarea name="" id="input" class="form-control" rows="3" required="required" ng-model="item.comment"></textarea>
        </div>
          <button type="submit" class="btn btn-primary" style="margin-top:10px;">Save</button>
        </div>
      </form>
      </div>

    </div>


  </div>

  <div class="tab-pane fade in" id="userseen">
    <h3>User Seen Products</h3>
    <br>
    <div class="row" ng-repeat="item in seen" style="border-bottom: 1px solid lightgrey; margin-bottom: 5px; padding: 5px;">
      <div class="col-lg-3">
        <div class="imagediv cart-thumb">
          <img width="100px" ng-src="../assets/images/products/thumbnail/{{item.image}}" alt=""/>
        </div>
      </div>
      <div class="col-lg-3">
        <label><a ng-href="../product/admin/{{item.id}}/product">{{item.name}} ({{item.id}})</a></label>

      </div>
      <div class="col-lg-3">
        <label>by {{item.user}}</label>
        <br>
        <label>{{item.phonenumber}}</label>
        <br>
        <label>{{item.email}}</label>
      </div>
      <div class="col-lg-3">
        <label>{{item.status}}</label>
        <br>
        <label>Item {{item.soldout == 1 ? 'Soldout' : 'Available'}}</label>
        <br>
        <span class="label label-info">
          Viewed
          <span class="badge">{{item.count}}</span>
          times
        </span>
        <label>{{item.time| todate | date:"MMM d, y 'at' h:mm a"}}</label>
      </div>
    </div>
  </div>

  <div class="tab-pane fade in" id="producthotness">
    <h3>Product Hotness</h3>
    <br>
    <div class="row" ng-repeat="item in hotness" style="border-bottom: 1px solid lightgrey; margin-bottom: 5px; padding: 5px;">
      <div class="col-lg-3">
        <div class="imagediv cart-thumb">
          <img width="100px" ng-src="../assets/images/products/thumbnail/{{item.image}}" alt=""/>
        </div>
      </div>
      <div class="col-lg-3">
        <label>{{item.name}} ({{item.id}})</label>
      </div>
      <div class="col-lg-3">
        <label>
          <span style="color: red; font-size:18px;">{{item.count}}</span>
        </label>
      </div>
      <div class="col-lg-3">
        <label>{{item.status}}</label>
        <br>
        <label>Item {{item.soldout == 1 ? 'Soldout' : 'Available'}}</label>
      </div>
    </div>
  </div>

  <!-- Customer calls begin -->
  <div class="tab-pane fade in" id="calls">
    <h3 style="color: darkorange">Customer calls</h3>
    <br>

    <!-- Call statistics begins -->
    <div class="panel panel-info">
      <div class="panel-heading">Call Statistics
      <span class="pull-right" style="color:blue"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
      </div>
      <div class="panel-body">
      <div class="col-lg-6 col-md-6">Total Calls: <span class="badge">{{stats.total}}</span></div>
      <div class="col-lg-6 col-md-6">Registered: <span class="badge">{{stats.registered}}</span></div>
      <!--form ng-submit="getCallerStats(call_date)">
      <div class="col-lg-3"><input type="date" class="form-control" ng-model="call_date"> <span class="badge"></span></div>
      <div class="col-lg-3"><input type="submit"></div>
      </form-->
    </div>
  </div>
  <!-- statistics ends here --><br>


    <ul class="nav nav-tabs nav-justified">
      <li class="active">
        <a data-toggle="tab" href="#calldetails" ng-click="getAllCalls()">Call Details</a>
      </li>
      <li>
        <a data-toggle="tab" href="#addcall" ng-click="">Add Call </a>
      </li>
    </ul>
  <!-- Call tabs -->
<div class="tab-content" style="margin-top: 3px;">
  <div class="tab-pane fade in active" id="calldetails" ng-init="">
    <div class="panel panel-success" ng-hide="">
      <div class="panel-heading">
        Call Details
        <a href="" ng-click="markRegistered()" class="pull-right" style="color:darkblue; text-decoration:underline">Set Register Status</a>
        <!--<a href="" ng-click="" class="pull-right" style="color: #337ab7;">View All Tickets</a>-->
      </div>
      <div class="panel-body">
        <label ng-show="calls == undefined"> There is no call details!!</label>
        <table class="table" ng-show="calls != undefined">
          <thead>
            <tr>
              <th>Name</th>
              <th>Purpose</th>
              <th>Product</th>
              <th>Mobile</th>
            <th>Source</th>
            <th>Range</th>
            <th>Comment</th>
            <th>By</th>
            <th>Time</th>
        </tr>
      </thead>
      <tbody>
        <tr ng-repeat="call in calls" ng-style="call.purpose == 'sale' ? {color: 'DarkOliveGreen'} : {color: '#415178'}" ng-init="call.showedit=0">
          <td>
            <span style="color: darkorange" ng-show="call.registered == 1">[R] </span>{{call.name}}
          </td>
          <td ng-show="call.purpose == 'procure' && call.link"><a ng-href="{{call.link}}" style="color: #415178; text-decoration: underline;" target="_blank">{{call.purpose}}</a></td>
          <td ng-show="call.purpose == 'sale' || !call.link">{{call.purpose}}</td>
          <td>{{call.product}}</td>
          <td>{{call.mobile}}</td>
          <td>{{call.source}}</td>
        <td>{{call.pricerange}}</td>
        <td ng-show="call.showedit==0" ng-click="call.showedit=1">{{call.comment}}</td>
        <td ng-show="call.showedit==1">
          <form ng-submit="saveCallComment(call)">
            <input type="text" class="form-control" ng-value="call.comment" required ng-model="call.comment" ng-blur="saveCallComment(call)">
            <button type="submit" class="btn btn-sm btn-default">save</button>
          </form>
        </td>
        <td>{{call.addedby}}</td>
        <td>{{call.timestamp| todate | date:"MMM dd 'at' h:mm a"}}</td>
    </tr>
  </tbody>
</table>
    <div class="row pull-right" style="margin-top: 10px; margin-bottom: 10px;" ng-show="1">
      <button type="button" class="btn btn-primary" ng-click="currentpage = (currentpage == 0 ? 0 : currentpage - 1);getAllCalls();">Previous</button>
      <button type="button" class="btn btn-primary" style="padding-left: 10px;" ng-click="currentpage = (calls == undefined ? currentpage: currentpage + 1); getAllCalls();">Next</button>
  </div>
</div>
</div>
</div>

<div class="tab-pane fade in" id="addcall">
<div class="panel panel-success" ng-hide="">
      <div class="panel-heading">
        Add call details
        <!--<a href="" ng-click="" class="pull-right" style="color: #337ab7;">View All Tickets</a>-->
      </div>
      <div class="panel-body">
        <form role="form" name="wp" ng-submit="wp.$valid && addCustomerCall();" novalidate>
        <div class="row">
            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <label for="">Customer Name</label>
            <input type="text" class="form-control"  ng-model="name" required></div>
            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12" >
              <label for="">Category</label>
              <input type="text" class="form-control"  ng-model="product" required>
            </div>
        </div>


      <div class="row">
         <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12" ng-init="purpose='sale'">
              <label class="radio-inline"><input type="radio" value="sale" name="optrad" ng-model="purpose" required checked>Sale</label>
              <label class="radio-inline"><input type="radio"  value="procure" name="optrad" ng-model="purpose" required>Procurement</label>            </div>
          <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12" >
            <label for="">Phone</label>
            <input type="text" class="form-control"  ng-model="phone" required></div>
          </div>

        <div class="row">

          <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12" ng-init="source='OLX'" >
            <label class="radio-inline"><input type="radio" value="OLX" name="optradio" ng-model="source" required checked>OLX</label>
            <label class="radio-inline"><input type="radio"  value="Quikr" name="optradio" ng-model="source" required>Quikr</label>
            <label class="radio-inline"><input type="radio"  value="Facebook" name="optradio" ng-model="source" required>Facebook</label>
            <label class="radio-inline"><input type="radio"  value="Reference" name="optradio" ng-model="source" required>Reference</label>
            <!--label class="radio-inline"><input type="radio"  value="other" name="optradio" ng-model="source" required>Other</label-->
          </div>
          <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <label for="">Price range</label>
            <input type="text" class="form-control"  ng-model="range" required></div>
          </div>

        <div class="row">
          <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <label >Comments</label>
            <textarea name="" id="input" class="form-control" rows="3" ng-model="comment"></textarea>
          </div>
          <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <label >Procurement Link</label>
            <textarea name="" id="input" class="form-control" rows="3" ng-model="link"></textarea>
          </div>
        </div>
        <div class="row">
          <button  type="submit" class="btn btn-primary" ng-disabled="newcall == 1">Submit</button><br>
          <span ng-show="newcall == 1" class="label label-warning">Call record Created</span>
        </div>
      </form>
</div>
</div>
</div>
</div>
</div>
<!-- Customer calls ends -->


<!-- Procurement  begins  -->


  <div class="tab-pane fade in" id="Procurement">
    <h3>Procurement Details</h3><hr>
    <br>
        <table class="table table-striped" >
          <thead>
            <tr>
              <th>Date</th>
              <th>From</th>
              <th>Description</th>
              <th>Quantity</th>
              <th>Cost</th>
        </tr>
      </thead>
      <tbody>
        <tr ng-repeat="procurement in procurements">
          <td>{{procurement.date  | todate | date:"MMM dd, yyyy"}}</td>
          <td>{{procurement.from}}</td>
          <td><pre>{{procurement.description}}</pre></td>
          <td>{{procurement.quantity}}</td>
          <td>{{procurement.cost}}</td>
        </tr>
  </tbody>
</table>
</div>


<!-- Procurement  ends here  -->

<!-- Buyback tab begins -->

<div class="tab-pane fade in" id="buyback">
    <h3>Buyback Details</h3><hr>
    <br>
    <ul class="nav nav-tabs nav-justified">
      <li class="active">
      <a data-toggle="tab" href="#buybackrequests" ng-click="showtdetails=0;getBuyBackDetails()">Buyback Requests</a>
      </li>
      <li>
      <a data-toggle="tab" href="#addbuyback" ng-click="newticket = 0;">Add Request</a>
      </li>
    </ul>
    <!-- Buyback tabs -->
    <div class="tab-content" style="margin-top: 3px;">
      <div class="tab-pane fade in active" id="buybackrequests" ng-init="detailshow = 0;">
        <div class="panel panel-success" ng-hide="showtdetails">
          <div class="panel-heading">
          All Requests
          </div>
          <div class="panel-body">
            <label ng-show="buyback == null">There are no requests!!</label>
              <table class="table table-striped" ng-hide="detailshow == 1 || buyback == null" ng-init="dticket = null">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Date</th>
                    <th>Order ID</th>
                    <th>Title</th>
                    <th>Location</th>
                    <th>Comments</th>
                    <th>Price</th>
                    <th>Status</th>
                    <!-- <th>Pickup Done</th> -->
                  </tr>
                </thead>
                <tbody>
                  <tr ng-repeat="bb in buyback" ng-click="showRequestDetails(bb)" style="cursor: pointer;">
                    <td>
                    {{bb.id}}
                    </td>
                    <td>{{bb.timestamp | todate | date:"MMM dd 'at' h:mm a"}}</td>
                    <!--td>{{ticket.mobile}}</td-->
                    <td>{{bb.order_id }}</td>
                    <td>{{bb.title}}</td>
                    <td>{{bb.bb_location}}</td>
                    <td>{{bb.comments}}</td>
                    <td>{{bb.bb_price}}</td>
                    <td ng-class="(bb.status == 'Rejected' ? 'red' : bb.status ==  'Pickup Done' ? 'green' : bb.status == 'Approved' ? 'blue' : 'yellow')">{{bb.status}}</td>
                    <!-- <td>{{bb.pickupdone == 1 ? 'Yes' : 'No'}}</td> -->
                  </tr>
                </tbody>
              </table>
          </div>
        </div>

        <div class="panel panel-success" ng-show="showtdetails">
          <div class="panel-heading">
            Request Details
            <button type="button" class="close" ng-click="showtdetails=0">
            <span class="pull-right">X</span>
            </button>
          </div>
          <div class="panel-body">
              <div class="row ticketdetails" style="color: black; font-size: 14; font-weight: 600;">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                  <label>Date:</label>
                  {{mr.timestamp | todate | date:"MMM dd 'at' h:mm a"}}
                  <br>
                  <label >Request ID:</label>
                  {{mr.id}}
                  <br>

                  <label style="margin-bottom:10px;">Order ID:</label>
                  {{mr.order_id}}
                  <br>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                  <label for="">Title:</label>
                  {{mr.title}}<br>
                  <label for="">Location:</label>
                  {{mr.bb_location}}<br>
                  <label ">Status:</label>
                  <span ng-class="(mr.status == 'Rejected' ? 'red' : mr.status ==  'Pickup Done' ? 'green' : mr.status == 'Approved' ? 'blue' : 'yellow')">{{mr.status}}</span>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                  <label for="">Technician:</label>
                  {{mr.tech}}<br>
                  <label for="">Price:</label>
                  {{mr.bb_price}}<br>
                  <!-- <label for="" ng-show = "mr.pickupdone == 1" style="color:darkorange;">Pickup Done</label> -->

                  <br><br>
                </div>
              </div><hr>
              <div class="row col-lg-12"><br><br>
                <div class=" row col-lg-6" ng-hide= "mr.isrejected==1 || mr.pickupdone ==1">
                  <form class="form-inline" name="bbtech" ng-submit="bbtech.$valid && addBbTech();" novalidate>
                      <div class="form-group ">
                      <input class="form-control"  type="text" ng-model="mr.tech" placeholder="Technician Name - Mobile " style="display: inline; width: 240px" required>

                      </div>
                      <button type="submit" class="btn btn-default">Assign Tech</button>
                      <span ng-show="techassigned==1" style="color: darkorange;"> Tech Assigned</span>
                      </form>
                </div>

                <div  class="row col-lg-2">
                    <button type="button" style="width:109px;margin-left: 420px;" class="btn btn-primary" ng-show="mr.isrejected==1" ng-disabled="reopen==1"  ng-click="bbReopen();">Reopen</button>
                </div>

                <div class="row col-lg-4" ng-hide= "mr.isrejected==1 || mr.pickupdone ==1" style="margin-left: 45px;">

                    <form class="form-inline" name="bbprice" ng-submit="bbprice.$valid && addBbPrice();" novalidate>
                      <div class="form-group ">
                      <input class="form-control" style="margin-left: -145px;" type="text" ng-model="mr.bb_price" placeholder="Enter Price" required>

                      </div>
                      <button type="submit" class="btn btn-default">Approve</button>
                      <span ng-show="priceadded==1"  style="color: darkorange;"> Price Added</span>
                      </form>
                </div><br><br>

                <!-- <div>
                    <button type="button" style="width:109px" class="btn btn-primary" ng-click="bBReopen();">Reopen</button>
                </div> -->
                <div class="row col-lg-6" ng-hide="mr.isrejected == 1 || mr.pickupdone == 1">
                    <form class="form-inline" name="rejectbb" ng-submit="rejectbb.$valid && addBbRejected();" novalidate>
                        <select class="form-control" style="display: inline; width: 250px" placeholder = "Reason to reject" ng-model="mr.rejectreason">
                        <option value="" disabled selected hidden>Reason for Rejection</option>
                        <option value="Sorry, Out of servicable area.">
                          Sorry, Out of servicable area.
                        </option>
                        <option value="Other">
                        Other
                        </option>
                        </select>
                        <button type="submit" class="btn btn-danger" style="width:100px"  ng-disable = "mr.isrejected == 1">Reject</button>
                        <span ng-show ="reject == 1" style="color: darkorange;" >Rejected</span>
                    </form>
                </div>
                <div class="row col-lg-3" style="margin-left: 50px">
                <form class="form-inline" name="pckb" ng-submit="pckb.$valid && addBbPickup();" ng-hide ="mr.pickupdone == 1 || mr.isrejected == 1" novalidate>
                  <input type="hidden" ng-model="mr.bb_price" required>
                  <button type="submit" style="width:269px" class="btn btn-success"  > Mark as Pickup Done</button>
                  <span ng-show="pickupdone == 1" style="color: darkorange;" > Pickup Done</span>
                  <span ng-show="pckb.$submitted && pckb.$invalid" style="color: darkorange;" >Add price before marking pickup done!</span>
                </form>

                <!--<button type="button" style="width:269px" class="btn btn-success" ng-click="addBbPickup();" ng-hide ="mr.pickupdone == 1 || mr.isrejected == 1" > Mark as Pickup Done</button> -->
                </div>
              </div>
          </div> <!-- Panel body ends-->
        </div>  <!-- panel ends -->
      </div>      <!-- Pending ticket tab ends -->

      <div class="tab-pane fade in" id="addbuyback">
        <div class="panel panel-success">
          <div class="panel-heading">
            Create Request
          </div>
          <form role="form"  name="newbb" ng-submit="newbb.$valid && neworder != undefined && addBuyBack();" novalidate style="margin: 25px">
            <div class="row">
              <label for="" >Order ID:</label>
              <div class="input-group" style="width:48%; margin-bottom: 15px;">
                <input type="text" class="form-control" ng-model="bb.order_id" required>
                <span class="input-group-btn">
                <button class="btn btn-default" type="button" ng-click="getOrderDetails(bb.order_id);">Fetch</button>
                </span>
              </div>
              <div ng-show="neworder != null" style="border: 1px solid lightgrey; padding: 10px;" ng-hide="neworder == null" >
                <div class="row">
                  <div class="text-left col-xs-6">
                  <a style="color: darkorange">Order ID: {{neworder.id}}</a>
                  </div>
                </div>
                <div class="past-order-item-details" ng-repeat="item in neworder.items">
                  <div class="row">
                    <div class="cart-thumb col-lg-2 col-xs-12 col-md-6 col-sm-12">
                    <a ng-href="../assets/images/products/normal/{{item.productimage}}">
                    <img ng-src="../assets/images/products/thumbnail/{{item.productimage}}"></a>
                  </div>
                  <div class="cart-item-details col-lg-3 col-xs-12 col-md-6 col-sm-12">
                    <label>{{item.productname}}</label>
                    <br>
                    <label>Amount: {{item.amount}}</label>
                  </div>
                  <div class="cart-item-details col-lg-3 col-xs-12 col-md-6 col-sm-12">
                    <label>{{neworder.orderdate}}</label>
                  </div>
                  <div class="cart-item-details col-lg-3 col-xs-12 col-md-6 col-sm-12">
                    <label>{{neworder.username}}</label>
                    <label>{{neworder.deliveryaddress.email}}</label>
                    <label>{{neworder.deliveryaddress.address_line_one}}</label>
                    <label>{{neworder.deliveryaddress.address_line_two}}</label>
                  </div>
                  </div>
                </div>
              </div>
              <div class="form-group" style="width:50%">
                <label for="">Product Title</label>
                <input type="text" class="form-control" ng-model="bb.title" required>
              </div>
              <div class="form-group" style="width:50%">
                <label for="">Pickup Location</label>
                <input type="text" class="form-control" ng-model="bb.bb_location" required>
              </div>

              <div class="form-group" style="width:50%">
                <label for="">Comments</label>
                <textarea class="form-control" rows="4" ng-model="bb.comments" required></textarea>
              </div>
              <button type="submit" class="btn btn-warning" ng-disabled="newrequest == 1">Add</button>
              <span ng-show="newrequest == 1" class="label label-success">Ticket Created</span><br>
              <span ng-show="neworder == undefined && newbb.$submitted" class="label label-success">Click fetch button </span>
            </div>
          </form>
        </div> <!-- Panel ends -->
      </div>  <!-- Create tab ends -->
    </div>  <!-- Buyback tabs end -->
</div><!-- Buyback ends ends here -->




<!-- Order statistics begins -->

<div class="tab-pane fade in" id="OrderStatistics">
    <h3 style="color: darkorange">Order Statistics</h3>
    <br>
    <ul class="nav nav-tabs nav-justified">
      <li class="active">
        <a data-toggle="tab" href="#daily" ng-click="getStatistics(0)">Daily</a>
      </li>
      <!--li>
        <a data-toggle="tab" href="#weekly" ng-click="">Weekly</a>
      </li-->
      <li>
        <a data-toggle="tab" href="#monthly" ng-click="getStatistics(0)">Monthly</a>
      </li><li>
        <a data-toggle="tab" href="#yearly" ng-click="getStatistics(0)">Yearly</a>
      </li>
    </ul>

   <!-- tabs -->
    <div class="tab-content" style="margin-top: 3px;">
      <div class="tab-pane fade in active" id="daily" ng-init="">
        <div class="panel panel-success" ng-hide="">
          <div class="panel-heading">
            Daywise Order Statistics
          </div>
          <div class="panel-body">
              <div align="center">
                <form class="form-inline">
                  <div class="form-group">
                    <input type="text" ng-model="search_date" class="form-control" >
                  </div>
                    <button type="submit" class="btn btn-default" ng-click="getStatistics(1)">Submit</button>
                </form>
              </div><br>

              <table class="table table-striped" ng-hide="" ng-init="">
                <thead>
                  <tr>
                    <th>Date</th>
                    <th>Visitor</th>
                    <th>Call</th>
                    <th>Registered</th>
                    <th>Seen</th>
                    <th>Sale Posts</th>
                    <th>Order</th>
                    <!--th>Cancelled</th-->
                    <!--th>Revenue</th-->
                  </tr>
                </thead>
                <tbody>
                  <tr ng-repeat="sday in statistics.days">
                    <td>{{sday.date | todate | date:"MMM dd, yyyy" }}</td>
                    <td>{{sday.visitors}}</td>
                    <td>{{sday.scalls+sday.pcalls}}(S:{{sday.scalls}}+P:{{sday.pcalls}})</td>
                    <td>{{sday.users}}</td>
                    <td>{{sday.seen}}</td>
                    <td>{{sday.saleposts}}</td>
                    <td>{{sday.orders}}</td>
                    <!--td>{{sday.cancelledorders}}</td-->
                    <!--td>{{sday.revenue}}</td-->
                  </tr>
                 </tbody>
              </table>
          </div>
        </div>
      </div><!--Daily tab ends here-->

      <!--div class="tab-pane fade in" id="weekly" ng-init="">
        <div class="panel panel-success" ng-hide="">
          <div class="panel-heading">
            Weekly Order Statistics
          </div>
          <div class="panel-body">
            <div align="center">
              <form class="form-inline">
                <div class="form-group">
                  <select class="form-control">
                    <option>Week 4</option>
                    <option>Week 3</option>
                    <option>Week 2</option>
                    <option>Week 1</option>
                  </select>
                </div>
                  <button type="submit" class="btn btn-default">Submit</button>
              </form>
            </div>

              <table class="table table-striped" ng-hide="" ng-init="">
                <thead>
                  <tr>
                    <th>Visitors</th>
                    <th>Registered</th>
                    <th>Seen</th>
                    <th>Orders</th>
                    <th>Cancelled</th>
                    <th>Revenue</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>100</td>
                    <td>70</td>
                    <td>150</td>
                    <td>68</td>
                    <td>2</td>
                    <td>140000</td>
                  </tr>
                 </tbody>
              </table>
          </div>
        </div>
      </div--><!--Week tab ends here-->

      <div class="tab-pane fade in" id="monthly" ng-init="">
        <div class="panel panel-success" ng-hide="">
          <div class="panel-heading">
            Monthly Order Statistics
          </div>
          <div class="panel-body">
          <div align="center">
            <form class="form-inline">
              <div class="form-group">
                 <div class="form-group">
                    <input type="text" ng-model="search_month" class="form-control" >
                  </div>
              </div>
                <button type="submit" class="btn btn-default" ng-click="getStatistics(2)">Submit</button>
            </form>
            </div>

            <table class="table table-striped" ng-hide="" ng-init="">
              <thead>
                <tr>
                  <th>Date</th>
                  <th>Visitors</th>
                  <th>Registered</th>
                  <th>Seen</th>
                  <th>Sale Posts</th>
                  <th>Calls</th>
                  <th>Orders</th>
                  <!--th>Cancelled</th-->
                  <th>Revenue</th>
                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="smonth in statistics.months">
                    <td>{{smonth.date | todate | date:"MMM, yyyy" }}</td>
                    <td>{{smonth.visitors}}</td>
                    <td>{{smonth.users}}</td>
                    <td>{{smonth.seen}}</td>
                    <td>{{smonth.saleposts}}</td>
                    <td>{{smonth.scalls+smonth.pcalls}}(S:{{smonth.scalls}}+P:{{smonth.pcalls}})</td>
                    <td>{{smonth.orders}}</td>
                    <!--td>{{smonth.cancelledorders}}</td-->
                    <td>{{smonth.revenue}}</td>
                  </tr>
               </tbody>
            </table>
          </div>
        </div>
      </div><!--Month tab ends here-->

      <div class="tab-pane fade in" id="yearly" ng-init="">
        <div class="panel panel-success" ng-hide="">
          <div class="panel-heading">
            Yearly Order Statistics
          </div>
          <div class="panel-body">
          <div align="center">
            <form class="form-inline">
              <div class="form-group">
                <div class="form-group">
                    <input type="text" ng-model="search_year" class="form-control" >
                  </div>
              </div>
                <button type="submit" class="btn btn-default" ng-click="getStatistics(3)">Submit</button>
            </form>
            </div>

            <table class="table table-striped" ng-hide="" ng-init="">
              <thead>
                <tr>
                  <th>Date</th>
                  <th>Visited</th>
                  <th>Registered</th>
                  <th>Seen</th>
                  <th>Sale Posts</th>
                  <th>Calls</th>
                  <th>Orders</th>
                  <!--th>Cancelled</th-->
                  <th>Revenue</th>
                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="syear in statistics.years">
                    <td>{{syear.date | todate | date:"yyyy" }}</td>
                    <td>{{syear.visitors}}</td>
                    <td>{{syear.users}}</td>
                    <td>{{syear.seen}}</td>
                    <td>{{syear.saleposts}}</td>
                    <td>{{syear.scalls+syear.pcalls}}(S:{{syear.scalls}}+P:{{syear.pcalls}})</td>
                    <td>{{syear.orders}}</td>
                    <!--td>{{syear.cancelledorders}}</td-->
                    <td>{{syear.revenue}}</td>
                  </tr>
               </tbody>
            </table>
          </div>
        </div>
      </div><!--Year tab ends here-->
    </div><!--Tabs ends here-->

    <!-- registered Users -->
    <div class="tab-pane fade in">
        <div class="panel panel-info">
          <div class="panel-heading">
            Recently Registered Users
          </div>
          <div class="panel-body">
             <table class="table table-striped">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Mobile</th>
                  <th>Verified</th>
                  <th>Time</th>
                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="user in statistics.recentusers">
                <td>{{user.id}}</td>
                <td>{{user.name}}</td>
                <td>{{user.phonenumber}}</td>
                <td>{{user.verified}}</td>
                <td>{{user.timestamp|todate | date:"MMM dd 'at' h:mm a"}}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
    </div> <!-- reg users ends here -->
</div><!--Panel ends here-->
<!-- statistics ends -->


<!--Pending Sale Approval begins -->
<div class="tab-pane fade in" id="Approvesale" class="sale">
    <h3 style="color: darkorange">Pending Sale Approval</h3>
    <br>
  <div class="panel panel-default">
      <div class="panel-body">
          <div class="panel panel-default">
              <div class="panel-body" ng-init="showoption='pending'">
                   <div class="radio-inline pull-left">
                        <label>
                          <input type="radio" name="optradio" value="pending" ng-model="showoption" ng-click="currentsellpage=0; getPendingSell()">
                          Show Pending
                          </label>
                      </div>
                    <div class="radio-inline pull-left">
                        <label>
                          <input type="radio" name="optradio" value="all" ng-model="showoption" ng-click="currentsellpage=0;getPendingSell()">
                          Show All
                          </label>
                      </div>
                      <div class="radio-inline pull-left">
                        <label>
                          <input type="radio" name="optradio" value="approved" ng-model="showoption" ng-click="currentsellpage=0;getPendingSell()">
                          Show Approved
                        </label>
                      </div><br><br>
                      <div class="radio-inline pull-left">
                        <label ng-init="showprocured=false">
                          <input type="radio" name="optradio" value="procured" ng-model="showoption" ng-click="getPendingSell()">
                          Show Procured
                        </label>
                      </div>
                      <div class="radio-inline pull-left">

                          <input type="radio" name="optradio" value="rejected" ng-model="showoption" ng-click="getPendingSell()">
                          Show Rejected
                        </label>
                      </div>
                <div class="pull-right">
                  <form style="margin-top:10px; margin-left:10px" class="form-inline" name="ss" ng-submit = "ss.$valid && getPendingSell()">
                    <select ng-model="srchfield" class="form-control">
                        <option value="sellid">Seller ID</option>
                        <!--option value="mobile">Mobile</option-->
                    </select>
                    <input type="text" class="form-control" ng-model="srchvalue" required>
                    <button type="submit"  class="btn btn-default btn-group">Search</button>
                  </form>
                </div>
            </div>
          </div>

        <div ng-repeat="s in sells" ng-init="s.showme=0">
          <form name="sell" class="sellform" novalidate ng-submit="sell.$valid && approveSellPrice(s)">
          <div class="row"> <!-- 0th row -->
            <div class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                  <label>ID:</label>
                  <label style="color: purple">{{s.sellid}}</label><br>
                  <label>{{s.timestamp | todate | date:"MMM dd 'at' h:mm a"}}</label>
            </div>


            <div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <label class="label" ng-class="s.isprocured == 1 ? 'label-warning' : (s.isrejected == 1 ? 'label-default' : (s.isapproved == 1 ? 'label-success' : 'label-primary'))">{{s.isprocured == 1 ? 'Procured' : (s.isrejected == 1 ? 'Rejected' : (s.isapproved == 1 ? 'Approved' : 'Pending Action'))}}</label>
                <label class="label label-default" ng-show="s.imageuploadfailed == 1">No Images</label>
                <label class="label label-default" ng-show="s.admincomment">Comment Added</label>
            </div>

            <div class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12 pull-right">
              <a href="" style="text-decoration: underline; color: #6D454C;" ng-click="s.showme=(s.showme==1 ? 0 : 1)">{{s.showme ==1 ? 'Hide Detals' : 'Show Details'}}</a>
            </div>
          </div> <!-- 0th row ends -->

            <div class="row sale">  <!-- Row 1 begins  -->
                <div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <label>Seller Name</label><br>
                  <label>{{s.sellername}}</label>
                </div>
                <div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <label>Seller Phone</label><br>
                  <label>{{s.sellermobile}}</label>
                </div>
                <div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <label>Seller Email</label><br>
                  <label>{{s.selleremail}}</label>
                </div>
            </div> <!-- Row 1 ends here  -->
            <div name="detailsdiv" ng-show="s.showme">
            <div class="row sale">
                <!--div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <label>Ready For Exchange</label>
                  <label class="form-control"></label>
                </div-->
                <!--div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <label>Reason for Sale</label>
                  <label class="form-control"></label>
                </div-->
                <div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <label>Under Warranty</label><br>
                  <label>{{s.underwarranty}}</label>
                </div>
                <div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <label>Bill Available</label><br>
                  <label>{{s.billavailable}}</label>
                </div>
                <div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <label>Box Available</label><br>
                  <label>{{s.boxavailable}}</label>
                </div>
            </div> <!-- Row 2 ends here  -->
            <div class="row sale">
                <div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <label>Product Category</label><br>
                  <label>{{s.maincat}}</label>
                </div>
                <div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <label>Sub category</label><br>
                  <label>{{s.subcat}}</label>
                </div>
                <div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <label>Brand</label><br>
                  <label>{{s.productbrand}}</label>
                </div>
            </div><!-- Row 3 ends here  -->
            <div class="row sale">
                <div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <label>Title</label><br>
                 <label>{{s.productname}}</label>
                </div>
                <div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <label>Type</label><br>
                  <label>{{s.producttype}}</label>
                </div>
               <div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <label>Model</label><br>
                  <label>{{s.productmodel}}</label>
                </div>
            </div> <!-- Row 4 ends here  -->
            <div class="row sale">
                <div class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                  <label>Age</label><br>
                  <label>{{s.age}}</label>
                </div>
                <div class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                  <label>Condition</label><br>
                  <label>{{s.workingcondition}}</label>
                </div>
                <div class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                  <label>Appearence</label><br>
                  <label>{{s.appearance}}</label>
                </div>
                <div class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                  <label>Internal Parts</label><br>
                  <label>{{s.internal}}</label>
                </div>
            </div> <!-- Row 5 ends here  -->
            <div class="row sale">
              <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <label>Description</label><br>
                  <label style="word-wrap: break-word">{{s.productdescription}}</label>
                </div>
            </div>
            <div class="row">
            <div class="form-group" style="margin-left: 20px;">
               <label>Product Images</label><br>
               <label ng-show="s.imageuploadfailed == 1" style="color:red">No Images found</label>
            </div>
                <div class="form-group col-md-3" ng-repeat="im in s.images">
                    <a ng-href="" class="thumbnail" data-toggle="modal" data-target="#sellimagemodal" ng-click="setsellmodalimage(s.images, im)">
                      <img ng-src="../{{im}}" style="width:150px;height:150px">
                    </a>
                </div>
            </div> <!-- Row 7 ends here  -->
            <div class="row sale">
                <div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <label>Approved Procure Price</label>
                  <input type="text" name="" class="form-control" placeholder="Approved price" required ng-model="s.approvedprice">
                </div>
             </div> <!-- Row 8 ends here  -->
             <div class="row sale">
                <div class="form-group col-lg-9 col-md-9 col-sm-6 col-xs-12">
                    <label>Comments</label>
                    <textarea class="form-control" style="background-color: #f4faf1; border: 1px solid lightgrey" rows="4" ng-model="s.admincomment"></textarea>
                 </div>   <br><br>
                 <button type="button" class="btn btn-default" ng-click="saveSaleComment(s)">Save Comment</button>
            </div> <!-- Row 9 ends here  -->

                    <div class = "btn-group" >
                         <button type = "button" class = "btn btn-success dropdown-toggle" data-toggle = "dropdown">Choose Actions<span class = "caret"></span></button>
                         <ul class = "dropdown-menu" role = "menu">
                            <li><a  ng-disabled="s.customercalled == 1" ng-click="markContacted(s) " ng-confirm-click="Have you contacted the customer">Contacted Customer</a></li>
                            <li><a  ng-disabled="s.approved == 1 || s.isapproved == 1" ng-click="sell.$valid && approveSellPrice(s)"> Approve</a></li>
                            <li><a  ng-disabled="s.isprocured == 1" ng-click="markProcured(s) ">Procured</a></li>
                         </ul>
                    </div>

            <!--button type="button" class="btn btn-primary" style="width: 20%" ng-disabled="s.customercalled == 1" ng-click="markContacted(s) " ng-confirm-click="Have you contacted the customer">Contacted Customer</button>
            <button type="submit" class="btn btn-success" style="width: 20%" ng-disabled="s.approved == 1 || s.isapproved == 1">Approve</button>
            <button type="button" class="btn btn-warning" style="width: 20%" ng-disabled="s.isprocured == 1" ng-click="markProcured(s) ">Procured</button-->
            <div class="sale" style="margin-top:15px;">
              <form class="form-inline" name="rejectsellform" novalidate>
                <!--div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
                  <label for="">Reject Reason(Customer Visible)</label>
                  <p ng-show="s.isrejected == 1">Rejected already..</p>
                  <input type="text" class="form-control" ng-model="rejectsellreason" required>
                </div-->
                <select class="form-control" style="display: inline; width: 500px" placeholder = "Reason to reject" ng-model="s.rejectreason">
                <option value="" disabled selected hidden>Choose reason for Rejection</option>
                  <option value="Sorry, We are not procuring this category of product at this time.">
                    Sorry, We are not procuring this category of products at this time.
                  </option>
                  <option value="Sorry, We can't procure this item as the quality does not meet our expectation.">
                    Sorry, We can't procure this item as the quality does not meet our expectation.
                  </option>
                  <option value="Sorry, We're not able to get into an agreement with the price of the product.">
                    Sorry, We're not able to get into an agreement with the price of the product.
                  </option>
		  <option value="Sorry, Out of serviceable area.">
		  Sorry, Out of serviceable area.
		  </option>

                </select>
                <button type="button" class="btn btn-danger" style="width:150px" ng-click="markRejected(s)">Reject</button>
                <p ng-show="s.isrejected == 1" style="color:darkorange">Rejected already..</p>
              </form>
            </div>
          </div><hr>
          </form>
          </div>
          <div id="sellimagemodal" class="modal fade" role="dialog">
            <div class="modal-dialog">
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Sell Image</h4>
                </div>
                <div class="modal-body">
                  <img style="max-width:none!important;" ng-src="../{{sellmodalimage}}" alt="image">
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default pull-left" ng-click="prevsellmodalimage(sellmodalimage)">Prev</button>
                  <button type="button" class="btn btn-default pull-right" ng-click="nextsellmodalimage(sellmodalimage)">Next</button>
                  <!--button type="button" class="btn btn-default" data-dismiss="modal">Close</button-->
                </div>
              </div>
            </div>
          </div>
          <div class="row pull-right" style="margin-top: 10px; margin-bottom: 10px;" ng-show="1">
      <button type="button" class="btn btn-primary" ng-click="currentsellpage = currentsellpage - 1;getPendingSell();" ng-disabled="currentsellpage == 0">Previous</button>
      <button type="button" class="btn btn-primary" style="padding-left: 10px; padding-right: 20px;" ng-click="currentsellpage=currentsellpage+1; getPendingSell();" ng-disabled="sells == null">Next</button>
  </div>
      </div> <!-- Panel body ends here  -->
  </div> <!-- Panel  ends here  -->
</div> <!-- Tab pane ends here  -->
<!-- Pending Sale Approval ends -->

<!-- Delivery slot begins -->

<div class="tab-pane fade in " id="slots">
    <h3 style="color:darkorange;">Delivery Slots</h3>
    <hr/>
    <div class="row">
    <div class="panel panel-primary" ng-repeat="(day, sl) in slots">
     <div class="panel-heading">
     Order for Day: <span style="font-weight:600">{{day | date :  "dd MMM y"}}</span>
     </div>
      <div class="panel-body slotorderpanel" ng-repeat="s in sl">
      <label for="" class="slotorder">ORDER ID: <span style="font-weight:600">{{s.orderid}}</span> <span>SLOT: <span style="font-weight:600; color: #3d4241">{{s.time1|toslottime}}</span></span> <span class="pull-right" ng-show="s.verified==1">Verified</span> <button type="button" class="pull-right btn-xs btn-primary" ng-click="VerifyDelivery(s)" ng-show="s.delivered == 1 || s.onthespotreturn == 1 && superuser == 1">Verify Delivery</button>
      </label>
      <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
      <h4 style="padding-bottom: 5px;text-decoration: underline;font-size: 16px;">Product Details</h4>
      <div class="cart-thumb col-lg-2 col-xs-4 col-md-2 col-sm-4" class="img-thumbnail">
          <img ng-src="../assets/images/products/thumbnail/{{s.productid}}_1.jpg">
      </div>
      <div class="col-lg-10 col-md-10 col-xs-8 col-sm-8">
      <p style="padding-bottom: 5px">{{s.productname}}</p>
      <p style="padding-bottom: 5px" style="padding-bottom: 5px">{{s.category}}</p>
      <p style="padding-bottom: 5px;" ng-show="s.ispaid == 1">Amount: <span style="color: grey">{{s.price}}</span>(Paid Online)</p>
      <p style="padding-bottom: 5px;" ng-show="s.ispaid == 0">Amount: <span style="color: red">{{s.price}}</span>(Cash on Delivery)</p>
      </div>
      </div>
      <div class="col-lg-6" style='margin-bottom: 15px'>
      <h4 style="padding-bottom: 5px;text-decoration: underline;font-size: 16px;">Delivery Address</h4>
      <!--p>{{s.name}}</p-->
        <p>{{s.address.name}}</p>
        <a ng-href="tel:{{s.address.phonenumber}}" style="color:#47C9AF; font-weight: 600">{{s.address.phonenumber}}</a>
        <p>
          {{s.address.address_line_one}}, {{s.address.address_line_two}}
        </p>
        <p class="">
          {{s.address.city}}, {{s.address.state}}
        </p> <b>{{s.address.landmark}} </b>
        <p class="">
          {{s.address.email}}
        </p>
        </div>
        <div style="width:300px; margin:0 auto">
          <button type="button" class="btn btn-warning" ng-click="performDelAction('Mark Shipped', s)" ng-show="s.shipped == 0">Shipped</button>
          <label class="label label-warning" ng-show="s.shipped == 1">Already Shipped</label>
          <button type="button" class="btn btn-success" ng-click="performDelAction('Mark Delivered', s)" ng-show="s.delivered == 0">Delivered</button>
          <label class="label label-success" ng-show="s.delivered == 1">Already Delivered</label>
          <button type="button" class="btn btn-danger" ng-click="markReturned(s)" ng-hide = "s.onthespotreturn == 1 ">Returned</button>
          <label class="label label-danger" ng-show="s.onthespotreturn == 1">Returned</label>
         
          <!--button type="button" class="btn btn-warning">Returned</button-->
        </div>
      </div>
    </div>
    </div>
      <!--table class="table table-striped" >
                     <thead>
                       <tr>
                         <th>Order Id</th>
                         <th>Date 1</th>
                         <th>Time 1</th>
                         <th>Date 2</th>
                         <th>Time 2</th>


                   </tr>
                 </thead>
                 <tbody>
                   <tr ng-repeat="st in slots">
                     <td>{{st.orderid}}</td>
                     <td>{{st.date1}}</td>
                     <td>{{st.time1 | toslottime}}</td>
                     <td>{{st.date2}}</td>
                     <td>{{st.time2 | toslottime}}</td>
                     

                   </tr>
             </tbody>
           </table-->

</div>



<!--Delivery slot ends -->
<div class="tab-pane fade in " id="docs">
    <h3 style="color:darkorange;">Docs</h3>
    <hr/>


 

<ul class="nav nav-pills menu">
   <li class="active">
     <a data-toggle="pill" href="#warehouse">Warehouse</a>
   </li>
   <li>
     <a data-toggle="pill" href="#sale">Sale</a>
   </li>
   <li >
     <a data-toggle="pill" href="#postsale">Post-sale</a>
   </li>
   <li>
     <a data-toggle="pill" href="#development">Development</a>
   </li>
</ul>


<div class="tab-content">
   <div class="tab-pane fade in active" id="warehouse">
       <br><br>
        <button class="btn btn-link" onclick=" window.open('https://drive.google.com/drive/u/0/folders/0B9KEFVYaTC9sN1ZzT1pZSkRZVVk','_blank')">Warehouse Pics</button><br>

   </div>
   <div class="tab-pane fade in" id="sale">
       <br><br>
               <button class="btn btn-link" onclick=" window.open('https://docs.google.com/spreadsheets/d/1qnReaUwQdbg8KgYc5pCGBpWUo_RbBttjSmeREp3q9SY/edit#gid=0','_blank')">Calls</button><br>
               <button class="btn btn-link" onclick=" window.open('https://docs.google.com/document/d/1Y3rFyTqLXR_42IvomuJqEA4ExJIM58Bcw9MoZtiVBQw/edit','_blank')">Sale/Procurement Questions</button><br>

   </div>
   <div class="tab-pane fade in" id="postsale">
       <br><br>
        <button class="btn btn-link" onclick=" window.open('https://drive.google.com/drive/u/0/folders/0B7P3bZHbJEzzRFg0bGQ1SkZTODg','_blank')">Post-Sale Feedback</button><br>
         <button class="btn btn-link" onclick=" window.open('https://drive.google.com/drive/u/0/folders/0B7P3bZHbJEzzbUV2QndUcWZXR1U','_blank')">Delivery Feedback</button><br>
         <button class="btn btn-link" onclick=" window.open('https://docs.google.com/spreadsheets/d/1sb2LnLszvgO3RCnaNjHV3UuCRNLWCQhzbFtANxSsZpI/edit#gid=0','_blank')">Product Returns</button><br>
         <button class="btn btn-link" onclick=" window.open('https://docs.google.com/spreadsheets/d/12c5b-79-zMdOrsDjcGtPx2OWZ7WejLTKVExxNEKVcFs/edit#gid=0','_blank')">Repairs</button><br>

   </div>
   <div class="tab-pane fade in" id="development">
       <br>
       <button class="btn btn-link" onclick=" window.open('https://docs.google.com/spreadsheets/d/1PGxKMH8qzJw4RMHeG_wnlcyMdAQwQOH9jV6aipum_4Q/edit#gid=0','_blank')">Development</button><br>
   </div>
</div>


</div>

<div class="tab-pane fade in" id="conversion">
    <h3 style="color:darkorange;">Conversion History</h3><hr>
    <table class="table table-hover">
      <thead>
        <tr>
          <th>Order ID</th>
          <th>Order Date</th>
          <th>Mobile</th>
          <th>Name</th>
          <th>Category</th>
          <th>Sale Price</th>
          <th>Conversion by</th>
          <th>Source</th>
          <th>Referrer</th>
        </tr>
      </thead>
      <tbody>
        <tr ng-repeat="c in conversion">
          <td>{{c.orderid}}</td>
          <td>{{c.date}}</td>
          <td>{{c.mobile}}</td>
          <td>{{c.name}}</td>
          <td>{{c.cat}}</td>
          <td>{{c.saleprice}}</td>
          <td>{{c.conversionby}}</td>
          <td>{{c.source}}</td>
          <td>{{c.referrer}}</td>
        </tr>
      </tbody>
    </table>
</div>
<!-- Gift Coupons -->


<div class="tab-pane fade in " id="coupons">
    <h3 style="color:darkorange;">Gift Coupons</h3>
    <hr/>

    <ul class="nav nav-pills menu">
       <li class="active">
         <a data-toggle="pill" href="#create">Create Coupon</a>
       </li>
       <li>
         <a data-toggle="pill" href="#view" ng-click="getCouponDetails();">View Coupons</a>
       </li>
    </ul>


    <div class="tab-content">
        <!-- Tab Create begins -->
       <div class="tab-pane fade in active" id="create" ><br><br>
              <form class="form-horizontal" role="form"  name="coupon" ng-submit="coupon.$valid && createCoupon();" novalidate style="margin: 25px">
                      <div class="form-group">
                          <label for="" class="control-label col-xs-2"><b>Enter Code</b></label>
                          <div class="col-lg-3">
                              <input type="text" class="form-control" ng-model="c.code"  required>
                          </div>
                      </div>
                      <div class="form-group">
                          <label for="" class="control-label col-lg-2"><b>Enter Value</b></label>
                          <div class="col-lg-3">
                              <input type="text" class="form-control" ng-model="c.value" required placeholder="Percentage">
                          </div>

                          <div class="col-lg-2">
                                 <div class="checkbox">
                                     <label><input type="checkbox" ng-model="c.ispercentage" > <b>Percentage</b></label>
                                 </div>
                             </div>
                      </div>
                      <div class="form-group">
                          <label for="" class="control-label col-xs-2"><b>Enter Validity</b></label>
                          <div class="col-lg-3">
                              <input type="text" class="form-control" placeholder="Number of days" ng-model="c.validity" required>
                          </div>
                      </div>
                      <div class="form-group">
                             <div class="col-lg-offset-2 col-lg-2">
                                 <div class="checkbox">
                                     <label><input type="checkbox" ng-model="isuserid"><b> For specific user </b></label>
                                 </div>
                             </div>
                         </div>

                      <div class="form-group" ng-show="isuserid">
                          <label for="" class="control-label col-xs-2"><b>Enter User ID</b></label>
                          <div class="col-lg-3">
                              <input type="text" class="form-control" ng-model="c.userid" >
                          </div>
                      </div>

                      <div class="form-group">
                          <div class="col-xs-offset-2 col-xs-10">
                              <button type="submit" class="btn btn-success">Create Coupon</button>
                              <span ng-show="newcoupon == 1" style="color: darkorange">New Coupon Created!!</span>
                          </div>
                      </div>
                  </form>

       </div>
      <!-- Tab Create ends -->

      <!-- Tab View begins -->
       <div class="tab-pane fade in" id="view"><br>
                   <table class="table table-striped" >
                     <thead>
                       <tr>
                         <th>Id</th>
                         <th>Code</th>
                         <th>Value</th>
                         <th>For</th>
                         <th>Validity</th>
                         <th>Status</th>

                   </tr>
                 </thead>
                 <tbody>
                   <tr ng-repeat="cp in coupon">
                     <td>{{cp.id}}</td>
                     <td>{{cp.code}}</td>
                     <!-- <td>{{cp.value}}<span ng-show="cp.ispercentage == 1">%</span><span ng-show="cp.ispercentage == 0"></span></td> -->
                     <td>{{cp.value}}<span ng-show="cp.ispercentage == 1 ? a='%' : a='Rs'">{{a}}</span></td>
                     <td>{{cp.userid == null ? 'All' : cp.userid}}</td>
                     <td>{{cp.validity}}</td>
                     <td >{{cp.isexpired ==1 ? "Expired" : "Active"}}</td>
                     <td ><button type="button" ng-disabled="cp.isexpired == 1 || couponexpired == 1" class="btn btn-danger" ng-click="markCouponExpired(cp)" style="padding: 3px 10px;">Disable</button><!-- <span ng-show="couponexpired == 1" style="color: darkorane">Disabled</span> --> </td>

                   </tr>
             </tbody>
           </table>


       </div> <!-- Tab View ends -->
    </div> <!--  Tab content div ends -->


</div>

<!-- Gift Coupon Ends -->
<!--sell begins -->
<!-- <div class="tab-pane fade in " id="addsale">
    <h3 style="color:darkorange;">Sale</h3>
    <hr/>

<form class="form-horizontal" role="form"  name="coupon" ng-submit="coupon.$valid && createCoupon();" novalidate style="margin: 25px">
                      <div class="form-group">
                          <label for="" class="control-label col-xs-2"><b>Enter User Id</b></label>
                          <div class="col-lg-3">
                              <input type="text" class="form-control" ng-model="userid"  required>
                          </div>
                      </div>
                      <div class="form-group">
                          <label for="" class="control-label col-lg-2"><b>Enter Product Id</b></label>
                          <div class="col-lg-3">
                              <input type="text" class="form-control" ng-model="productid" required >
                          </div>
                      </div>
                      <div class="form-group">
                          <label for="" class="control-label col-lg-2"><b>Enter Date</b></label>
                          <div class="col-lg-3">
                              <input type="date" class="form-control" ng-model="date" required >
                          </div>
                      </div> -->

                         <!--  <div class="col-lg-2">
                                 <div class="checkbox">
                                     <label><input type="checkbox" ng-model="c.ispercentage" > <b>Percentage</b></label>
                                 </div>
                             </div> -->
                      
                      <!-- <div class="form-group">
                          <label for="" class="control-label col-xs-2"><b>Enter Validity</b></label>
                          <div class="col-lg-3">
                              <input type="text" class="form-control" placeholder="Number of days" ng-model="c.validity" required>
                          </div>
                      </div>
                      <div class="form-group">
                             <div class="col-lg-offset-2 col-lg-2">
                                 <div class="checkbox">
                                     <label><input type="checkbox" ng-model="isuserid"><b> For specific user </b></label>
                                 </div>
                             </div>
                         <!-- </div> -->

                      <!-- <div class="form-group" ng-show>
                          <label for="" class="control-label col-xs-2"><b>Enter User ID</b></label>
                          <div class="col-lg-3">
                              <input type="text" class="form-control" ng-model="c.userid" >
                          </div>
                      </div> 
 -->
                     <!--  <div class="form-group">
                          <div class="col-xs-offset-2 col-xs-10">
                              <button type="submit" class="btn btn-success">Sell</button>
                          </div>
                      </div>
                  </form>

                  </div>
 -->
                  <!-- Sell ends -->

    <!--Callback begins -->
    <div class="tab-pane fade in " id="callback">
    <h3 style="color:darkorange; ">Callback</h3>
    <hr/>

    <div ng-hide="showdetails==1">
    <table class="table table-striped" >
                     <thead style="font-size: 15px; text-decoration: none; ">
                       <tr>
                         <th>Number</th>
                         <th>Comments</th>
                         <th>Status</th>
                         <th>Date</th>
                         <th>Product</th>
                   </tr>
                 </thead>
                 <tbody>
                   <tr ng-repeat="cb in callback" ng-click="showCallBack(cb)" style="cursor: pointer;">
                     <td>{{cb.number}}</td>
                     <td>{{cb.comments}}</td>
                     <td ng-class="(cb.called == 1  ? 'green':'red')">{{cb.called == 1 ? "Called" : 'Yet to Call'}}</td>
                     <td>{{cb.time}}</td>
                     <td>{{cb.productid}}</td>
                   </tr>
             </tbody>
     </table>
     </div>

     <div  ng-show="showdetails==1"><div class="panel panel-default">
    <div class="panel-heading">Details<button type="button" class="close" ng-click="showdetails=0"><span class="pull-right">X</span></button></div>
  <div class="panel-body">
      <form role="form" name="cbu" ng-submit="cbu.$valid && updateCallBack();" novalidate>
        <div class="row">
          <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <label for="">Name</label>
            <input type="text" class="form-control"  ng-model="b.name" required><br>
            <label for="">Comments</label>
            <textarea class="form-control" rows="4" required ng-model="b.comments"></textarea>
          </div><br>
          </div>
          <div class="row">
          <div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12" >

              <label>
                <input type="checkbox" name="" id="input" value="" ng-model="b.called">&nbsp Call Done</label>
          </div>
          </div>

          <div class="row">
          <div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12" >
            <button  type="submit" class="btn btn-success" >Update</button>
            <p style="color:darkorange" ng-show="showcbstat">Updated Successfully</p>
          </div>
        </div>
        </form>
        </div>
        </div>
        </div>


    </div>
    <!--Callback ends -->



<!-- Post Sale begins -->

<div class="tab-pane fade in" id="complaints">
    <h3 style="color: darkorange">Post Sale Complaints</h3>
    <br>

    <ul class="nav nav-tabs nav-justified">
      <li class="active">
      <a data-toggle="tab" href="#pendingtickets" ng-click="getPendingTickets();">Pending Tickets</a>
      </li>
      <li>
      <a data-toggle="tab" href="#createticket" ng-click="newticket = 0;">Create a Ticket</a>
      </li>
    </ul>

    <!-- Ticket tabs -->

    <div class="tab-content" style="margin-top: 3px;">
      <div class="tab-pane fade in active" id="pendingtickets" ng-init="detailshow = 0;">
          <div class="panel panel-success" ng-hide="showtdetails">
            <div class="panel-heading">
              Pending Tickets
              <a href="" ng-click="currentticketpage = 0; getAllTickets()" class="pull-right" style="color: #337ab7;">View All Tickets</a>
            </div>
            <div class="panel-body">
              <label ng-show="tickets == null">There are no Pending Tickets!!</label>
              <table class="table table-striped" ng-hide="detailshow == 1 || tickets == null" ng-init="dticket = null">
                <thead>
                  <tr>
                    <th>Ticket ID</th>
                    <th>Owner</th>
                    <th>Subject</th>
                    <th>Order ID</th>
                    <th>Location</th>
                    <th>Technician</th>
                    <th>Status</th>
                    <th>Expense</th>
                    <th>Creation Time</th>
                    <!-- <th>Days</th> -->
                  </tr>
                </thead>
                <tbody>
                  <tr ng-repeat="ticket in tickets" ng-click="showTicketDetails(ticket)" style="cursor: pointer;">
                    <td>
                    <span class="badge">{{ticket.id}}</span>
                    <span ng-show="ticket.underwarranty == 0" class="label label-warning"  >Out of Warranty</span>
                    </td>
                    <td>{{ticket.uname}}</td>
                    <!--td>{{ticket.mobile}}</td-->
                    <td>{{ticket.subject}}</td>
                    <td>{{ticket.order_id}}</td>
                    <td>{{ticket.location}}</td>
                    <td>{{ticket.tech == null?"Not Assigned":ticket.tech}}</td>
                    <td ng-class="(ticket.status == 'Open' ? 'red' : ticket.status !=  'Resolved' ? 'yellow' : 'green')">{{ticket.status}}</td>
                    <td>{{ticket.rexpense}}</td>
                    <td>{{ticket.createdtime | todate | date:"MMM dd 'at' h:mm a"}}</td>
                    <!-- <td>{{ticket.days}}</td> -->
                  </tr>
                </tbody>
              </table>
              <div class="row pull-right"  ng-show="showpagination == 1" style="margin-top: 10px; margin-bottom: 10px; ">
                <button type="button" class="btn btn-primary" ng-click="currentticketpage = (currentticketpage != 0 ? currentticketpage - 1 : 0); getAllTickets();">Previous</button>
                <button type="button" class="btn btn-primary" style="padding-left: 10px;" ng-click="currentticketpage = (tickets != null ? currentticketpage + 1 : currentticketpage); getAllTickets();">Next</button>
              </div>
            </div>
        </div>  <!-- Pending tickets  end -->
        <!-- Ticket details -->

        <div class="panel panel-success" ng-show="showtdetails">
          <div class="panel-heading">
            Ticket Details
            <button type="button" class="close" ng-click="showtdetails=0">
            <span class="pull-right">X</span>
            </button>
          </div>
          <div class="panel-body">
            <div class="row ticketdetails" style="color: black; font-size: 14; font-weight: 600;">
              <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <label >Ticket ID:</label>
                {{dticket.id}}
                <br>
                <label>Status:</label>
                <span ng-class="(dticket.status == 'Open' ? 'red' : dticket.status !=  'Resolved' ? 'yellow' : 'green')">{{dticket.status}}</span>
                <br>
                <label style="margin-bottom:10px;">Order ID:</label>
                {{dticket.order_id}}
                <br>
                <label style="margin-bottom:10px;">Technician:</label>
                {{dticket.tech}}
                <br>
                <form name="techn"><label ng-show="dticket.nextstatus == 'Tech Assigned'">Assigned Technician: </label><input type="text" name="tech" placeholder="TechName + Mobile" id="input" class="form-control" ng-show="dticket.nextstatus == 'Tech Assigned'" ng-model="tech"><br>
                  <button type="button" class="btn btn-warning" ng-confirm-click
                  ng-click="setTicketStatus()" ng-disabled="dticket.nextstatus == 'Tech Assigned' && techn.tech.$pristine">Mark {{dticket.nextstatus}}</button>
                </form>

              </div>
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label for="">Subject:</label>
                {{dticket.subject}} <br>
                <label for="">Expense:</label>
                {{dticket.rexpense}} <br>

                <div style="margin-top: 78px;">
                  <form class="form-inline" name="rexp" ng-submit="rexp.$valid && addRepairExpense(dticket.id);" novalidate>
                    <div class="input-group">
                      <input type="Number" class="form-control" ng-model="rexpense" placeholder="Repair Expense " required>
                      <span class="input-group-btn">
                        <button type="submit" class="btn btn-default">Submit</button>
                        <span ng-show="expenseadded == 1" style="color:orange;" > Expense Added..</span>
                      </span>
                    </div>
                  </form>
                </div>

              </div>

              <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                {{dticket.uname}}
                <p>{{dticket.mobile}}</p>
                <p>
                <p class="name">{{order.deliveryaddress.name}}</p>
                <p class="phone">{{order.deliveryaddress.phonenumber}}</p>
                <p class="">
                {{order.deliveryaddress.address_line_one}}, {{order.deliveryaddress.address_line_two}}
                </p>
                <p class="">
                {{order.deliveryaddress.city}}, {{order.deliveryaddress.state}}
                </p>
                <label>{{order.deliveryaddress.landmark}}</label>
                </p>
              </div>
            </div>

            <!-- <div >
              <form class="form-inline" name="rexp" ng-submit="rexp.$valid && addRepairExpense(dticket.id);" novalidate>
                <div class="input-group">
                  <input type="Number" class="form-control" ng-model="rexpense" placeholder="Repair Expense " required>
                <span class="input-group-btn">
                <button type="submit" class="btn btn-default">Submit</button>
                <span ng-show="expenseadded == 1" style="color:orange;" > Expense Added..</span></span></div>
              </form>
            </div> -->




            <div class="row">
              <ul style="margin:10px;">
                <li ng-repeat="comment in comments" class="panel panel-default" style="padding:10px;">
                <p style="color:grey; margin-top: 10px; font-size: 12px;">
                <cite>by</cite>
                {{comment.commenter}}
                <span class="pull-right">{{comment.creationtime}}</span>

                </p>
                <p style="padding-top: 5px;">{{comment.comment}}</p>
                </li>
              </ul>
              <form style="margin:10px;" role="form"  name="newcomment" ng-submit="newcomment.$valid && saveTicketComment(dticket.id, comment, cmntr, comments);" novalidate>
                <div class="form-group" style="width:80%;">
                <label for="" style="color:green">Name:</label>
                <input type="text" name="" id="input" ng-readonly="1" class="form-control" ng-model="cmntr" style="background-color: #f4faf1; border: 1px solid lightgrey" required>
                <label for="" style="color:green">Comment:</label>
                <textarea class="form-control" style="background-color: #f4faf1; border: 1px solid lightgrey" rows="4" ng-model="comment" required></textarea>
                </div>
                <button type="submit" class="btn btn-success">Save</button>
                <a href="" style="color: grey; margin-left: 15px;" ng-click="showtdetails=0">Close</a>
              </form>
            </div>
            </div>  <!-- Details panel body ends -->
          </div>  <!-- Details panel ends -->
        </div>  <!-- Pending Ticket tabs end -->


      <div class="tab-pane fade in" id="createticket">
        <div class="panel panel-success">
          <div class="panel-heading">
            Create Ticket
          </div>
          <form role="form"  name="newtick" ng-submit="newtick.$valid && neworder != undefined && addTicket();" novalidate style="margin: 25px">
            <legend></legend>
            <div class="row">
              <label for="" >Order ID:</label>
              <div class="input-group" style="width:48%; margin-bottom: 15px;">
                <input type="text" class="form-control" ng-model="neworderid" required>
                <span class="input-group-btn">
                <button class="btn btn-default" type="button" ng-click="getOrderDetails(neworderid);">Fetch</button>
                </span>
              </div>
              <div ng-show="neworder != null" style="border: 1px solid lightgrey; padding: 10px;">
                <div class="row">
                  <div class="text-left col-xs-6">
                  <a style="color: darkorange">Order ID: {{neworder.id}}</a>
                  </div>
                </div>
                <div class="past-order-item-details" ng-repeat="item in neworder.items">
                  <div class="row">
                    <div class="cart-thumb col-lg-2 col-xs-12 col-md-6 col-sm-12">
                      <a ng-href="../assets/images/products/normal/{{item.productimage}}">
                      <img ng-src="../assets/images/products/thumbnail/{{item.productimage}}"></a>
                    </div>
                    <div class="cart-item-details col-lg-3 col-xs-12 col-md-6 col-sm-12">
                      <label>{{item.productname}}</label>
                      <br>
                      <label>Amount: {{item.amount}}</label>
                    </div>
                    <div class="cart-item-details col-lg-3 col-xs-12 col-md-6 col-sm-12">
                      <label>{{neworder.orderdate}}</label>
                    </div>
                    <div class="cart-item-details col-lg-3 col-xs-12 col-md-6 col-sm-12">
                      <label>{{neworder.username}}</label>
                      <label>{{neworder.deliveryaddress.email}}</label>
                      <label>{{neworder.deliveryaddress.address_line_one}}</label>
                      <label>{{neworder.deliveryaddress.address_line_two}}</label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group" style="width:50%">
                <label for="">Subject</label>
                <input type="text" class="form-control" ng-model="subject" required>
              </div>
              <div class="form-group" style="width:50%">
                <label for="">Location</label>
                <input type="text" class="form-control" ng-model="location" required>
              </div>
              <div class="form-group" style="width:50%">
                <label for="">Comment</label>
                <textarea class="form-control" rows="4" ng-model="comment" required></textarea>
              </div>
              <button type="submit" class="btn btn-warning" ng-disabled="newticket == 1">Create</button>
              <span ng-show="newticket == 2" class="label label-success">Ticket Created</span><br>
              <span ng-show="neworder == undefined && newtick.$submitted" class="label label-success">Click fetch button </span>
            </div> <!-- Row ends -->
          </form>
        </div> <!-- Create ticket panel ends -->
      </div>  <!-- Create ticket ends -->

    </div><!-- Ticket tabs end here -->
</div><!-- Complaint  tab ends here -->





</div>
</div>
</div>
</div>
</div>
<script  src="../assets/js/jquery.min.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>

<script src="../assets/bower_components/angular/angular.min.js"></script>
<script src="../assets/bower_components/angular-cookies/angular-cookies.min.js"></script>
<script src="../assets/js/dashboard/adminapp1.js"></script>

</body>
</html>
