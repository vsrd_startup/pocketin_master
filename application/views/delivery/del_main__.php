<html ng-app="piadmin" ng-cloak>
<head>
  <title>Refabd Delivery</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimal-ui" />
  <meta name="apple-mobile-web-app-status-bar-style" content="yes" />
  <link rel="stylesheet" href="../assets/css/bootstrap.min.css" />
  <link rel="stylesheet" href="../assets/css/common.min.css" />
  <style>
  .sellform {
    margin-bottom:50px;
    margin-top:50px;
}
div.sale div.form-group label:nth-child(1) {
    font-size:13px;
    font-weight: 200;
    color:grey;
  }
div.sale div.form-group label:nth-child(3) {
    font-size:16px;
    font-weight: 400;
    color:darkorange;
  }
.imagediv {
height: auto;
}
.title,.status,.action {
margin-top: 40px;
}
.navbar-brand{
color: #000;
}
a,
a:visited{
color: #1a1a1a;
}
a:hover,
a:active,
a:focus{
color: #47C9AF;
}


.yellow {
color: #f8bc3b;
}
.green {
color: #43a047;
}
.red {
color: red;
}
.blue {
color: blue;
}

header{
background: #F7F7F7;
border-bottom: 1px solid #DEDEDE;
position: fixed;
top: 0;
left: 0;
right: 0;
z-index: 3;
}
.main-container {
margin-top: 90px;
}
.dealername {
color: orange;
float: right;
}
.dealername .dropdown > a{
padding: 15px;
display: block;
}
.dealername .dropdown{
position: relative;
}
.dealername .dropdown-menu{
position: absolute;
right: 0;
width: 140px;
padding: 15px;
left: inherit;
}
.clear {
clear: both;
}
.item {
border-bottom: 1px solid lightgrey;
padding-top: 5px;
padding-bottom: 5px;
}
.quality {
padding-bottom: 20px;
}

body {
background-color: #fff;
}

form.ng-submitted .ng-invalid {
border-color: #FA787E;
}

.menu {
padding: 10px;
margin-top: 17px;
margin-right: 10px;
border-right: 1px solid lightgrey;
}

.nav-pills>li a,
.menu a:visited{
color: #1a1a1a;
background: #fff;
border: 1px solid #fff;
}
.menu a:hover,
.menu a:active,
.menu a:focus,
.nav-pills>li.active>a, .nav-pills>li.active>a:focus, .nav-pills>li.active>a:hover{
color: #47C9AF;
background: #fff;
border-radius: 3px;
border: 1px solid #47C9AF;
}

.model {
border: 1px solid lightgrey;
margin-top: 10px;
margin-bottom: 10px;
padding-left: 10px;
}

.past-orders-head ul li{
display: inline-block;
}

.past-order-products  .order-id{
font-weight: 500;
padding: 15px;
background: #F7F7F7;
}

img {
  max-width: 100px;
  height: auto;
}


.past-order-products .product-title{
font-size: 12px!important;
margin: 0;
}

.past-order-products .cart-thumb img{
width: auto!important;
height: inherit;
display: block;
margin: 0 auto;
}

.past-order-item-details{
padding: 5px 5px 15px 0;
padding-top: 15px;
border-bottom: 1px solid #DEDEDE;
}

.past-order-footer div{
/*border-top: 1px solid #DEDEDE;*/
padding: 10px 15px;
}
.border {
border: 1px solid grey;
margin-bottom: 5px;
}


.past-order-products .product-description{
margin: 0;
}

.past-order-item .cart-item-details{
padding: 0 15px;
}

.past-order-item.cart-item{
margin: 15px 0;
/*padding: 10px 15px;*/
}

/*
.ng-invalid.ng-dirty {
border-color:#FA787E
}
.ng-valid.ng-dirty {
border-color:#78FA89
}
*/

</style>

</head>

<body ng-controller="AdminController as ctrl">
  <header>
    <div class="container-fluid">
      <!-- Login Div -->
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
          <a class="navbar-brand" href="">
            REFABD
            - Delivery
          </a>
        </div>
        <div ng-show="xhr">
          <label style="color: darkorange; font-size: 20px; position:absolute">Loading...</label>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 text-right dealername" ng-show="adminLogged">
          <div class="dropdown">
            <a class="dropdown-toggle" type="button" data-toggle="dropdown"  href="#">Hello {{dealername}}</a>
            <section class="dropdown-menu">
              <li>
                <a href="/" ng-click="logout()">Logout</a>
              </li>
            </section>
          </div>
        </div>
      </div>
    </div>
  </header>

  <div class="main-container">
    <div class="container-fluid">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
        <form role="form" ng-show="!adminLogged" ng-submit="Login()">
          <legend>Dealer Login</legend>

          <div class="form-group">
            <label for="">Mobile Number</label>
            <input type="text" class="form-control" id="" placeholder="Enter Mobile Number" ng-model="username"></div>
          <div class="form-group">
            <label for="">Password</label>
            <input type="password" class="form-control" id="" placeholder="Enter Password" ng-model="password"></div>
          <button type="submit" class="btn btn-primary">Submit</button>
          <p style="color: darkorange">{{error}}</p>
        </form>
      </div>

      <div class="row clear"> <!--ng-show="adminLogged"-->
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
          <ul class="nav nav-pills nav-stacked menu">
            <li class="active">
              <a data-toggle="pill" href="#pendingapproval">Pending Approval</a>
            </li>
            <li>
              <a data-toggle="pill" href="#custorder" ng-click="loadCustOrder()">Customer Orders</a>
            </li>
            
      </ul>
    </div>
    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
      <div class="tab-content">
        <div class="tab-pane fade in active" id="pendingapproval">
          <h3>Pending Approval</h3>
          <hr/>
          <div class="row item" ng-repeat="item in pa_list">
            <div class="col-lg-3">
              <div class="imagediv">
                <img width="150px" ng-src="../assets/images/dealerpending/{{item.images[0].image_full}}" alt=""/>
              </div>
            </div>
            <form name="appvform" ng-submit="appvform.$valid && ApproveProduct(item)" class="col-lg-9">
              <legend>
                Listed by
                <span style="color: green">{{item.dealer_name}}</span>
              </legend>
              <div class="form-group">
                <label for="">Product Title:</label>
                <input type="text" class="form-control" id="" placeholder="" ng-model="item.product_title" required/>
              </div>
              <div class="form-group">
                <label for="">Product Description:</label>
                <input type="text" class="form-control" id="" placeholder="" ng-model="item.product_description" required/>
              </div>
              <div class="form-group">
                <label>Sale Price:</label>
                <input type="text" class="form-control" id="" placeholder="" ng-model="item.saleprice" required/>
              </div>
              <div class="form-group">
                <label>Market Price:</label>
                <input type="text" class="form-control" id="" placeholder="" ng-model="item.mrp" required/>
              </div>
              <div class="form-group">
                <label>Long Description:</label>
                <textarea rows="5" class="form-control" id="" placeholder="" ng-model="item.longdescription" required></textarea>

              </div>
              <div class="form-group">
                <label>Technical Spec:</label>
                <textarea rows="5" class="form-control" id="" placeholder="" ng-model="item.technicalspec" required></textarea>
              </div>
              <div class="form-group">
                <label>Condition Spec:</label>
                <textarea rows="5" class="form-control" id="" placeholder="" ng-model="item.conditionspec" required></textarea>
              </div>
              <div class="row">
                <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-4" ng-init="item.isfactory =  item.isfactory == 1 ? true : false">
                  <div class="radio">
                    <label>
                      <input type="checkbox" name="" id="input" value="" ng-model="item.isfactory">Unused & New</label>
                  </div>
                </div>
              </div>

              <button type="submit" class="btn btn-danger" ng-hide="item.approving == 1 || item.success == 1">Approve</button>
              <p style="color:grey; font-size: 14px;" ng-show="item.approving == 1 && item.failed == 0">Approving...</p>
              <p style="color:red; font-size: 14px;" ng-show="item.failed == 1">Failed</p>
              <p style="color:darkorange; font-size: 14px;" ng-show="item.approving == 0 && item.success == 1">Product Added Successfully</p>
            </form>
          </div>
        </div>

        <div class="tab-pane fade in" id="custorder">
          <h3>Customer Orders</h3>

          <!-- Search order -->
          <div class="container" ng-init="scat = 'Order ID'" style="margin-top: 15px; ">
              <div class="row">
                  <div style="width: 500px;">
                  <div class="input-group">
                          <div class="input-group-btn search-panel">
                              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <span id="search_concept">{{scat}}</span> <span class="caret"></span>
                              </button>
                              <ul class="dropdown-menu" role="menu">
                                <li><a href="" ng-click="scat = 'Order ID'">Order ID</a></li>
                                <li><a href="" ng-click="scat = 'Mobile'">Mobile</a></li>
                                <!--li><a href="" ng-click="scat = 'Name'">Name</a></li-->
                                <!--li><a href="#">Less than < </a></li>
                                <li class="divider"></li>
                                <li><a href="#all">Anything</a></li-->
                              </ul>
                          </div>
                          <input type="text" class="form-control" name="x" placeholder="Search Order" ng-model="stext">
                          <span class="input-group-btn">
                              <button class="btn btn-default" type="button" style="padding:9px;" ng-click="searchOrder();"><span class="glyphicon glyphicon-search"></span></button>
                          </span>
                      </div>
                  </div>
            </div>
          </div> <!-- search ends here -->

          <div style="margin-top: 20px; margin-left: 25%; margin-right: 25%;">
            <ul class="list-inline">
              <li class="list-group-item">
                <a href="" ng-click="Filterme(0)">Active</a>
              </li>
              <!--li class="list-group-item">
              <a href="" ng-click="Filterme(1)">Placed</a>
            </li>
            <li class="list-group-item">
              <a href="" ng-click="Filterme(2)">Confirmed</a>
            </li>
            <li class="list-group-item">
              <a href="" ng-click="Filterme(3)">Shipped</a>
            </li-->
            <li class="list-group-item">
              <a href="" ng-click="Filterme(1)">Delivered</a>
            </li>
            <li class="list-group-item">
              <a href="" ng-click="Filterme(2)">Cancelled</a>
            </li>
          </ul>
        </div>
        <div class="past-order-products">
          <ul>
            <li class="cart-item  past-order-item border" ng-repeat="order in corders">
              <div class="order-id">
                <div class="row">
                  <div class="text-left col-xs-6">
                    <a style="color: darkorange">Order ID: {{order.order_id}}</a>
                  </div>
                </div>
              </div>

              <div class="past-order-item-details" ng-repeat="item in order.items" ng-class="item.iscancelled ? 'cancelleditem' : ''">
                <div class="row">
                  <div class="cart-thumb col-lg-2 col-xs-12 col-md-6 col-sm-12">
                    <a ng-href="../assets/images/products/normal/{{item.productimage}}">
                      <img ng-src="../assets/images/products/thumbnail/{{item.productimage}}"></a>
                  </div>
                  <div class="cart-item-details col-lg-3 col-xs-12 col-md-6 col-sm-12">
                    <h1 class="product-title">Product Id: {{item.productid}}<br>{{item.productname}}</h1>
                    <!--span class="quality-index">
                    <span class="icon pocketin-icon-quality-index"></span>
                    4.2
                  </span-->
                  <span class="product-description">{{item.productdescription}}</span>
                  <!--div class="colors-available">
                  <label class="inline">Status: {{item.status}}</label>
                </div-->
                <div class="delivered-date" ng-class="item.iscancelled ? 'cancelleditem' : ''"> <b>{{item.status}}</b>
                </div>
                <h4 style="color: black;">
                  <span>&#x20B9;</span>
                  {{item.amount}}
                </h4>
                <h4 ng-show="item.ispaid == 1" style="color: orange;">Amount Paid</h4>
                <h4 ng-show="item.ispaid == 0" style="color: red;">Cash On Delivery</h4><br><br>
                <h6 ng-show="order.coupon != null">Coupon Applied: <span style="color:red">{{order.coupon}}</span></h6>
              </div>
              <div class="col-lg-4 col-xs-12 col-md-12 col-sm-12">
                <h5 style="padding-bottom: 5px;">Delivery Address</h5>
                <p class="name">{{order.deliveryaddress.name}}</p>
                <p class="phone">{{order.deliveryaddress.phonenumber}}</p>
                <p class="">
                  {{order.deliveryaddress.address_line_one}}, {{order.deliveryaddress.address_line_two}}
                </p>
                <p class="">
                  {{order.deliveryaddress.city}}, {{order.deliveryaddress.state}}
                </p> <b class="landmark">{{order.deliveryaddress.landmark}} </b>
                <p class="">
                  {{order.deliveryaddress.email}}
                </p>
              </div>
              <div class="col-lg-3 col-xs-12 col-md-12 col-sm-12">
                <!--a  href="#refers" ng-click="checkReferral(order.order_id);">Check Referral Bonus</a-->
                <button type="button" class="btn btn-link " data-toggle="modal" data-target="#refModal" ng-click="checkReferral(order.order_id)">Check Referral Bonus</button>
              </div>
              <div class="col-lg-3 col-xs-12 col-md-12 col-sm-12">
                <button ng-disabled="item.disable" class="btn btn-default" ng-click="performAction(item.action, item)" ng-confirm-click ng-show="item.action != 'Order Executed' && item.action != 'Cancellation Processed'">{{item.action}}</button>
                <button ng-hide="item.action != 'Mark Confirmed'" class="btn btn-default" ng-confirm-click ng-click="performAction('Mark Rejected', item)" style="margin-top: 5px;">Mark Rejected</button>
                <p class="label" style="font-size: 10px; padding: 10px;" ng-show="item.action == 'Order Executed' || item.action == 'Cancellation Processed'" ng-class="(item.action == 'Order Executed') ? 'label-success' : 'label-warning'">{{item.action}}</p>
              </div>

              <div style="margin-top:30px;" ng-show="item.action == 'Order Executed'">
                <a ng-href="../{{item.invoice}}" style="color:  #47C9AF">View Invoice</a>
              </div>
              <button class="btn btn-default" ng-hide="item.action == 'Cancellation Processed' || item.canstatus == 'Done'" ng-confirm-click ng-click="cancelOrderItem(order.order_id, item)" style="margin-top: 5px;" ng-hide="canstatus == 'Done'">Cancel Order</button>
              <label for="">{{item.canstatus}}</label>

            </div>
          <!--Modal Referral-->
          <div id="refModal" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <!--button type="button" class="close" data-dismiss="modal">&times;</button-->
                <h4 class="modal-title">Referrals <span class="pull-right"> Total Discount:{{discount}} </span></h4>
              </div>
              <div class="modal-body">
                <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>Referred</th>
                        <th>Status</th>
                        <th>Discount</th>
                      </tr>
                    </thead>
                  <tbody>
                    <tr ng-repeat="referral in referrals">
                        <td>{{referral.rname}}</td>
                        <td>{{referral.ruser_id ? 'Joined' : 'Pending'}}</td>
                        <td>{{referral.ruser_id ? '100' : '0'}}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>

          </div>
</div>
<!--Modal ends -->

          </div>
          <div class="row past-order-footer row" style="padding:0 15px;">
            <div class="col-xs-6 text-left">Order Time: {{order.orderdate}}</div>
            <div class="col-xs-6 product-price text-right">
              Amount to Get:
              <span style="color:darkorange;">
                <span>&#x20B9;</span>
                {{order.amount}}
              </span>
            </div>
          </div>
          <div>
            <form ng-submit="saveComment(order.order_id, order.comment);">
              <div class="row">
                <div class="col-lg-6 col-md-6" style="margin-left: 10px;!important">
                  <textarea rows="2" class="form-control" ng-model="order.comment"></textarea>
                </div>
                <div class="col-lg-4 col-md-4" style="margin-top: 8px;">
                  <button type="submit" class="btn btn-warning">Save Comment</button>
                  <p>{{commentdone}}</p>
                </div>
              </div>
            </form>
          </div>
        </li>
      </ul>
    </div>
  </div>

 









</div>
</div>
</div>
</div>
</div>
<script  src="../assets/js/jquery.min.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>

<script src="../assets/bower_components/angular/angular.min.js"></script>
<script src="../assets/bower_components/angular-cookies/angular-cookies.min.js"></script>
<script src="../assets/js/delivery/del.js"></script>

</body>
</html>
