<!DOCTYPE html>
<html lang="en" ng-app="del" ng-cloak>
<head>
  <title>Refabd Delivery</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>

  form.ng-submitted .ng-invalid {
	border-color: #FA787E;
  }

  th {
 	color: blue;
  }
/*kkkkkkkkkkkkkkkkkkkkkkkkkkk*/
  .sellform {
    margin-bottom:50px;
    margin-top:50px;
}
div.sale div.form-group label:nth-child(1) {
    font-size:13px;
    font-weight: 200;
    color:grey;
  }
div.sale div.form-group label:nth-child(3) {
    font-size:16px;
    font-weight: 400;
    color:darkorange;
  }
.imagediv {
height: auto;
}
.title,.status,.action {
margin-top: 40px;
}
.navbar-brand{
color: #000;
}
a,
a:visited{
color: #1a1a1a;
}
a:hover,
a:active,
a:focus{
color: #47C9AF;
}


.yellow {
color: #f8bc3b;
}
.green {
color: #43a047;
}
.red {
color: red;
}
.blue {
color: blue;
}

header{
background: #F7F7F7;
border-bottom: 1px solid #DEDEDE;
position: fixed;
top: 0;
left: 0;
right: 0;
z-index: 3;
}
.main-container {
margin-top: 90px;
}
.dealername {
color: orange;
float: right;
}
.dealername .dropdown > a{
padding: 15px;
display: block;
}
.dealername .dropdown{
position: relative;
}
.dealername .dropdown-menu{
position: absolute;
right: 0;
width: 140px;
padding: 15px;
left: inherit;
}
.clear {
clear: both;
}
.item {
border-bottom: 1px solid lightgrey;
padding-top: 5px;
padding-bottom: 5px;
}
.quality {
padding-bottom: 20px;
}

body {
background-color: #fff;
}

form.ng-submitted .ng-invalid {
border-color: #FA787E;
}

.menu {
padding: 10px;
margin-top: 17px;
margin-right: 10px;
border-right: 1px solid lightgrey;
}

.nav-pills>li a,
.menu a:visited{
color: #1a1a1a;
background: #fff;
border: 1px solid #fff;
}
.menu a:hover,
.menu a:active,
.menu a:focus,
.nav-pills>li.active>a, .nav-pills>li.active>a:focus, .nav-pills>li.active>a:hover{
color: #47C9AF;
background: #fff;
border-radius: 3px;
border: 1px solid #47C9AF;
}

.model {
border: 1px solid lightgrey;
margin-top: 10px;
margin-bottom: 10px;
padding-left: 10px;
}

.past-orders-head ul li{
display: inline-block;
}

.past-order-products  .order-id{
font-weight: 500;
padding: 15px;
background: #F7F7F7;
padding-left: 10px;
margin-top: -20px;
}

img {
  max-width: 100px;
  height: auto;
}


.past-order-products .product-title{
font-size: 12px!important;
margin: 0;
padding-left: 10px;
}

.past-order-products .cart-thumb img{
width: auto!important;
height: inherit;
display: block;
margin: 0 auto;
}

.past-order-item-details{
padding: 5px 5px 15px 0;
padding-top: 15px;
border-bottom: 1px solid #DEDEDE;
}

.past-order-footer div{
/*border-top: 1px solid #DEDEDE;*/
padding: 10px 15px;
}
.border {
border: 1px solid grey;
margin-bottom: 5px;
margin-left: 5px;
}


.past-order-products .product-description{
margin: 0;
}

.past-order-item .cart-item-details{
padding: 0 15px;
}

.past-order-item.cart-item{
margin: 15px 0;
margin-left: -43px;
/*padding: 10px 15px;*/
}

/*
.ng-invalid.ng-dirty {
border-color:#FA787E
}
.ng-valid.ng-dirty {
border-color:#78FA89
}
*/

/*kkkkkkkkkkkkkkkkkkkkkkkkkkk*/


  </style>
</head>
<body ng-controller="DeliveryController">
<div class="container-fluid">
		<nav class="navbar navbar-default">
		  <div class="container-fluid">
		    <div class="navbar-header">
		      <a class="navbar-brand" href="#" style="color: darkorange;"">REFABD - Delivery</a>
		    </div>
		    <div class="pull-right">
		    <div class="dropdown" style="position:relative; margin-top: 15px;">
            <a class="dropdown-toggle" type="button" data-toggle="dropdown"  href="#">Hello {{name}},</a>
            <section class="dropdown-menu">
              <li>
                <a href=""  ng-click="logout()">Logout</a>
              </li>
            </section>
          </div>
          </div>
		    </div>
		</nav><br>
</div>
<!-- <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
	  <form role="form" ng-show="!loggedin" ng-submit="Login()">
	    <legend>Delivery Login</legend>
	    <div class="form-group">
	      <label for="">Mobile Number</label>
	      <input type="text" class="form-control" id="" placeholder="Enter Mobile Number" ng-model="username"></div>
	    <div class="form-group">
	      <label for="">Password</label>
	      <input type="password" class="form-control" id="" placeholder="Enter Password" ng-model="password"></div>
	    <button type="submit" class="btn btn-primary">Submit</button>
	    <p style="color: darkorange">{{error}}</p>
	  </form>
</div> -->

	<div class="container" ><!-- ng-show="loggedin" -->



		<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
          <ul class="nav nav-pills nav-stacked menu">

            <li class="active">
              <a data-toggle="pill" href="#custorder" ng-click="loadCustOrder()">Customer Orders</a>
            </li>
            <li>
              <a data-toggle="pill" href="#slots" ng-click="getDeliveryDetails()">Delivery Slots</a>
            </li>
            
      </ul>
    </div>

    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
    <div class="tab-content">

    <div class="tab-pane fade in active" id="custorder">
          <h3>Customer Orders</h3>
                    <!-- Search order -->
                    <div class="container" ng-init="scat = 'Order ID'" style="margin-top: 15px; ">
                        <div class="row">
                            <div style="width: 500px;">
                            <div class="input-group">
                                    <div class="input-group-btn search-panel">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                          <span id="search_concept">{{scat}}</span> <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                          <li><a href="" ng-click="scat = 'Order ID'">Order ID</a></li>
                                          <li><a href="" ng-click="scat = 'Mobile'">Mobile</a></li>

                                        </ul>
                                    </div>
                                    <input type="text" class="form-control" name="x" placeholder="Search Order" ng-model="stext" style="margin-top: 3px;">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button" style="padding:9px;" ng-click="searchOrder();"><span class="glyphicon glyphicon-search"></span></button>
                                    </span>
                                </div>
                            </div>
                      </div>
                    </div> <!-- search ends here -->


                  <div class="past-order-products">
                    <ul>
                      <li class="cart-item  past-order-item border" ng-repeat="order in corders">
                        <div class="order-id">
                          <div class="row">
                            <div class="text-left col-xs-6">
                              <a style="color: darkorange">Order ID: {{order.order_id}}</a>
                            </div>
                          </div>
                        </div>

                        <div class="past-order-item-details" ng-repeat="item in order.items" ng-class="item.iscancelled ? 'cancelleditem' : ''">
                          <div class="row">
                            <!-- <div class="cart-thumb col-lg-2 col-xs-12 col-md-6 col-sm-12">
                              <a ng-href="../assets/images/products/normal/{{item.productimage}}">
                                <img ng-src="../assets/images/products/thumbnail/{{item.productimage}}"></a>
                            </div> -->
                            <div class="cart-item-details col-lg-3 col-xs-12 col-md-6 col-sm-12">
                              <h3 class="product-title">Product Id: {{item.productid}}<br>{{item.productname}}</h3>
                              <!--span class="quality-index">
                              <span class="icon pocketin-icon-quality-index"></span>
                              4.2
                            </span-->
                            <!-- <span class="product-description">{{item.productdescription}}</span> -->
                            <!--div class="colors-available">
                            <label class="inline">Status: {{item.status}}</label>
                          </div-->
                          <div class="delivered-date" ng-class="item.iscancelled ? 'cancelleditem' : ''" style=" padding-left: 10px;"> <b>{{item.status}}</b>
                          </div>
                          <h4 style="color: red; padding-left: 10px;">
                            <span>&#x20B9;</span>
                            {{item.amount}}
                          </h4>
                          <h4 ng-show="item.ispaid == 1" style="color: orange;padding-left: 10px;">Amount Paid</h4>
                          <h4 ng-show="item.ispaid == 0" style="color: red; padding-left: 10px;">Cash On Delivery</h4><br><br>
                          <h6 style="padding-left: 10px " ng-show="order.coupon != null" >Coupon Applied: <span style="color:red;">{{order.coupon}}</span></h6>
                        </div>
                        <div class="col-lg-4 col-xs-12 col-md-12 col-sm-12">
                          <h5 style="padding-bottom: 5px;">Delivery Address</h5>
                          <p class="name">{{order.deliveryaddress.name}}</p>
                          <p class="phone">{{order.deliveryaddress.phonenumber}}</p>
                          <p class="">
                            {{order.deliveryaddress.address_line_one}}, {{order.deliveryaddress.address_line_two}}
                          </p>
                          <p class="">
                            {{order.deliveryaddress.city}}, {{order.deliveryaddress.state}}
                          </p> <b class="landmark">{{order.deliveryaddress.landmark}} </b>
                          <p class="">
                            {{order.deliveryaddress.email}}
                          </p>
                        </div>
                        <div class="col-lg-3 col-xs-12 col-md-12 col-sm-12">
                          <!--a  href="#refers" ng-click="checkReferral(order.order_id);">Check Referral Bonus</a-->
                          <button type="button" class="btn btn-link " data-toggle="modal" data-target="#refModal" ng-click="checkReferral(order.order_id)">Check Referral Bonus</button>
                        </div>
                        <div class="col-lg-3 col-xs-12 col-md-12 col-sm-12">
                          <button ng-disabled="item.disable" class="btn btn-default" ng-click="performAction(item.action, item)" ng-confirm-click ng-show="item.action != 'Order Executed' && item.action != 'Cancellation Processed'">{{item.action}}</button>
                          <button ng-hide="item.action != 'Mark Confirmed'" class="btn btn-default" ng-confirm-click ng-click="performAction('Mark Rejected', item)" style="margin-top: 5px;">Mark Rejected</button>
                          <p class="label" style="font-size: 10px; padding: 10px;" ng-show="item.action == 'Order Executed' || item.action == 'Cancellation Processed'" ng-class="(item.action == 'Order Executed') ? 'label-success' : 'label-warning'">{{item.action}}</p>
                          <button class="btn btn-default" ng-hide="item.action == 'Cancellation Processed' || item.canstatus == 'Done'" ng-confirm-click ng-click="cancelOrderItem(order.order_id, item)" style="margin-top: 5px;" ng-hide="canstatus == 'Done'">Cancel Order</button>
                        <label for="">{{item.canstatus}}</label>
                        </div>

                        <div style="margin-top:30px;" ng-show="item.action == 'Order Executed'">
                          <a ng-href="../{{item.invoice}}" style="color:  #47C9AF">View Invoice</a>
                        </div>
                        

                      </div>
                    <!--Modal Referral-->
                    <div id="refModal" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <!--button type="button" class="close" data-dismiss="modal">&times;</button-->
                          <h4 class="modal-title">Referrals <span class="pull-right"> Total Discount:{{discount}} </span></h4>
                        </div>
                        <div class="modal-body">
                          <table class="table table-striped">
                              <thead>
                                <tr>
                                  <th>Referred</th>
                                  <th>Status</th>
                                  <th>Discount</th>
                                </tr>
                              </thead>
                            <tbody>
                              <tr ng-repeat="referral in referrals">
                                  <td>{{referral.rname}}</td>
                                  <td>{{referral.ruser_id ? 'Joined' : 'Pending'}}</td>
                                  <td>{{referral.ruser_id ? '100' : '0'}}</td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                      </div>

                    </div>
          </div>
          <!--Modal ends -->

                    </div>
                    <div class="row past-order-footer row" style="padding:0 15px;">
                      <div class="col-xs-6 text-left">Order Time: {{order.orderdate}}</div>
                      <div class="col-xs-6 product-price text-right">
                        Amount to Get:
                        <span style="color:darkorange;">
                          <span>&#x20B9;</span>
                          {{order.amount}}
                        </span>
                      </div>
                    </div>

                     <div ng-show="order.slotbooked" style="margin: 10px">
                <div class="col-lg-6">
                <span style="color: #1da229; font-size: 16px">[SLOT 1] {{order.slot.date1 }}  {{order.slot.time1 | toslottime }}</span>
                </div>
                 <div class="col-lg-6">
                  <span style="color: #1da229; font-size: 16px">[SLOT 2] {{order.slot.date2 }}  {{order.slot.time2 | toslottime }}</span>
                </div>
                
              </div> 

                    <div>
                      <form ng-submit="saveComment(order.order_id, order.comment);">
                        <div class="row">
                          <div class="col-lg-6 col-md-6" style="margin-bottom: 10px; margin-left: 10px;!important">
                            <textarea rows="2" class="form-control" ng-model="order.comment"></textarea>
                          </div>
                          <div class="col-lg-4 col-md-4" style="margin-top: 8px;">
                            <button type="submit" class="btn btn-warning">Save Comment</button>
                            <p>{{commentdone}}</p>
                          </div>
                        </div>
                      </form>
                    </div>
                  </li>
                </ul>
              </div>
            </div>

            <!-- </div>  --><!-- Tab content ends -->
            

          <!-- Slots -->

          <div class="tab-pane fade in " id="slots">
           <h3 style="color:darkorange;">Delivery Slots</h3>
            <hr/>


                   <table class="table table-striped" >
                     <thead>
                       <tr>
                         <th>Order Id</th>
                         <th>Date 1</th>
                         <th>Time 1</th>
                         <th>Date 2</th>
                         <th>Time 2</th>
                   </tr>
                 </thead>
                 <tbody>
                   <tr ng-repeat="st in slots">
                     <td>{{st.orderid}}</td>
                     <td>{{st.date1}}</td>
                     <td>{{st.time1 | toslottime}}</td>
                     <td>{{st.date2 | toslottime}}</td>
                     <td>{{st.time2 }}</td>

                   </tr>
             </tbody>
           </table>





</div>
</div>









<!-- ***** -->
</div>
</div>
	<script src="assets/bower_components/angular/angular.min.js"></script>
	<script src="assets/bower_components/angular-cookies/angular-cookies.min.js"></script>
	<script src="assets/js/delivery/del.js"></script>
</body>
</html>
