# ************************************************************
# Sequel Pro SQL dump
# Version 4529
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.0.17-MariaDB)
# Database: pocketin_db
# Generation Time: 2016-07-16 02:02:14 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table activitylogs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `activitylogs`;

CREATE TABLE `activitylogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `object_id` int(11) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_activitylog_users1_idx` (`user_id`),
  KEY `object_id` (`object_id`),
  CONSTRAINT `activitylogs_ibfk_1` FOREIGN KEY (`object_id`) REFERENCES `productitems` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_activitylog_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `activitylogs` WRITE;
/*!40000 ALTER TABLE `activitylogs` DISABLE KEYS */;

INSERT INTO `activitylogs` (`id`, `object_id`, `type`, `timestamp`, `user_id`)
VALUES
	(1,1,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-22 16:08:01',2),
	(2,1,'APPROVED_PRODUCT','2016-05-22 16:11:50',2),
	(3,2,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-22 16:16:26',2),
	(4,2,'APPROVED_PRODUCT','2016-05-22 16:19:51',2),
	(5,3,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-22 16:22:09',2),
	(6,3,'APPROVED_PRODUCT','2016-05-22 16:25:33',2),
	(7,4,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-22 16:26:24',2),
	(8,4,'APPROVED_PRODUCT','2016-05-22 16:29:02',2),
	(9,5,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-22 16:30:03',2),
	(10,5,'APPROVED_PRODUCT','2016-05-22 16:31:13',2),
	(11,6,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-22 16:34:14',2),
	(12,6,'APPROVED_PRODUCT','2016-05-22 16:38:59',2),
	(13,7,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-22 16:41:48',2),
	(14,7,'APPROVED_PRODUCT','2016-05-22 16:43:47',2),
	(15,8,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-22 16:48:40',2),
	(16,8,'APPROVED_PRODUCT','2016-05-22 16:55:03',2),
	(17,NULL,'DEALER_ADD_PRODUCT_INIT','2016-05-22 16:57:11',2),
	(18,NULL,'DEALER_ADD_PRODUCT_INIT','2016-05-22 16:58:44',2),
	(19,9,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-22 17:01:10',2),
	(20,9,'APPROVED_PRODUCT','2016-05-22 17:09:01',2),
	(21,10,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-22 17:11:14',2),
	(22,10,'APPROVED_PRODUCT','2016-05-22 17:12:55',2),
	(23,11,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-22 17:18:23',2),
	(24,11,'APPROVED_PRODUCT','2016-05-22 17:21:59',2),
	(25,12,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-22 17:24:14',2),
	(26,12,'APPROVED_PRODUCT','2016-05-22 17:30:30',2),
	(27,13,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-23 02:31:12',2),
	(28,13,'APPROVED_PRODUCT','2016-05-23 02:34:57',2),
	(29,14,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-23 02:36:44',2),
	(30,14,'APPROVED_PRODUCT','2016-05-23 02:44:47',2),
	(31,15,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-23 02:52:51',2),
	(32,15,'APPROVED_PRODUCT','2016-05-23 02:59:30',2),
	(33,16,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-23 03:01:17',2),
	(34,16,'APPROVED_PRODUCT','2016-05-23 03:06:28',2),
	(35,17,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-23 03:09:08',2),
	(36,17,'APPROVED_PRODUCT','2016-05-23 03:12:37',2),
	(37,18,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-23 03:15:41',2),
	(38,18,'APPROVED_PRODUCT','2016-05-23 03:17:47',2),
	(39,19,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-23 03:21:41',2),
	(40,19,'APPROVED_PRODUCT','2016-05-23 03:26:54',2),
	(41,20,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-23 03:29:38',2),
	(42,20,'APPROVED_PRODUCT','2016-05-23 03:33:39',2),
	(43,21,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-23 03:43:30',2),
	(44,21,'APPROVED_PRODUCT','2016-05-23 03:46:38',2),
	(45,22,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-23 03:48:41',2),
	(46,22,'APPROVED_PRODUCT','2016-05-23 03:50:53',2),
	(47,23,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-23 03:53:09',2),
	(48,23,'APPROVED_PRODUCT','2016-05-23 03:55:00',2),
	(49,24,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-23 03:58:49',2),
	(50,24,'APPROVED_PRODUCT','2016-05-23 04:00:28',2),
	(51,25,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-23 09:19:03',2),
	(52,25,'APPROVED_PRODUCT','2016-05-23 09:21:04',2),
	(53,26,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-23 13:47:57',2),
	(54,26,'APPROVED_PRODUCT','2016-05-23 13:51:24',2),
	(55,27,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-24 07:13:51',1),
	(56,27,'APPROVED_PRODUCT','2016-05-24 07:16:48',1),
	(57,28,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-24 12:30:55',2),
	(58,28,'APPROVED_PRODUCT','2016-05-24 12:32:22',2),
	(59,29,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-24 12:58:06',2),
	(60,29,'APPROVED_PRODUCT','2016-05-24 13:00:18',2),
	(61,30,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-24 15:31:44',2),
	(62,30,'APPROVED_PRODUCT','2016-05-24 15:40:37',2),
	(63,31,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-26 10:33:05',2),
	(64,32,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-26 10:36:16',2),
	(65,32,'APPROVED_PRODUCT','2016-05-26 10:44:17',2),
	(66,31,'APPROVED_PRODUCT','2016-05-26 10:44:18',2),
	(67,33,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-26 11:20:41',2),
	(68,33,'APPROVED_PRODUCT','2016-05-26 11:23:19',2),
	(69,34,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-26 11:51:07',2),
	(70,34,'APPROVED_PRODUCT','2016-05-26 11:55:36',2),
	(71,44,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-27 02:33:36',3),
	(72,54,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-27 02:35:19',3),
	(73,35,'APPROVED_PRODUCT','2016-05-27 02:40:24',1),
	(74,36,'APPROVED_PRODUCT','2016-05-27 02:44:17',1),
	(75,37,'APPROVED_PRODUCT','2016-05-27 02:48:49',1),
	(76,38,'APPROVED_PRODUCT','2016-05-27 02:50:34',1),
	(77,39,'APPROVED_PRODUCT','2016-05-27 02:51:21',1),
	(78,40,'APPROVED_PRODUCT','2016-05-27 02:52:36',1),
	(79,41,'APPROVED_PRODUCT','2016-05-27 02:53:48',1),
	(80,42,'APPROVED_PRODUCT','2016-05-27 02:54:40',1),
	(81,43,'APPROVED_PRODUCT','2016-05-27 02:58:58',1),
	(82,44,'APPROVED_PRODUCT','2016-05-27 03:00:07',1),
	(83,45,'APPROVED_PRODUCT','2016-05-27 03:02:21',1),
	(84,46,'APPROVED_PRODUCT','2016-05-27 03:04:21',1),
	(85,47,'APPROVED_PRODUCT','2016-05-27 03:06:25',1),
	(86,50,'APPROVED_PRODUCT','2016-05-27 03:10:05',1),
	(87,49,'APPROVED_PRODUCT','2016-05-27 03:10:22',1),
	(88,48,'APPROVED_PRODUCT','2016-05-27 03:10:27',1),
	(89,54,'APPROVED_PRODUCT','2016-05-27 03:11:35',3),
	(90,53,'APPROVED_PRODUCT','2016-05-27 03:11:39',3),
	(91,52,'APPROVED_PRODUCT','2016-05-27 03:11:40',3),
	(92,51,'APPROVED_PRODUCT','2016-05-27 03:11:41',3),
	(93,55,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-30 08:39:03',1),
	(94,56,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-30 09:05:22',1),
	(95,57,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-30 09:08:02',1),
	(96,58,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-30 09:10:51',1),
	(97,59,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-30 09:15:34',1),
	(98,60,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-30 09:17:43',1),
	(99,55,'APPROVED_PRODUCT','2016-05-30 09:31:12',1),
	(100,56,'APPROVED_PRODUCT','2016-05-30 09:38:26',1),
	(101,57,'APPROVED_PRODUCT','2016-05-30 09:39:41',1),
	(102,58,'APPROVED_PRODUCT','2016-05-30 09:40:50',1),
	(103,59,'APPROVED_PRODUCT','2016-05-30 09:42:48',1),
	(104,60,'APPROVED_PRODUCT','2016-05-30 09:43:56',1),
	(105,61,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-30 09:55:46',1),
	(106,61,'APPROVED_PRODUCT','2016-05-30 09:59:10',1),
	(107,62,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-30 10:11:54',1),
	(108,62,'APPROVED_PRODUCT','2016-05-30 10:14:05',1),
	(109,63,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-31 09:52:37',1),
	(110,63,'APPROVED_PRODUCT','2016-05-31 09:59:49',1),
	(111,64,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-31 12:23:11',2),
	(112,64,'APPROVED_PRODUCT','2016-05-31 12:30:09',2),
	(113,65,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-31 15:30:22',2),
	(114,65,'APPROVED_PRODUCT','2016-05-31 15:35:52',2),
	(115,66,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-31 18:26:56',1),
	(116,66,'APPROVED_PRODUCT','2016-05-31 18:27:56',1),
	(117,67,'DEALER_ADD_PRODUCT_COMPLETE','2016-06-01 16:51:50',2),
	(118,68,'DEALER_ADD_PRODUCT_COMPLETE','2016-06-02 04:19:23',2),
	(119,68,'APPROVED_PRODUCT','2016-06-02 04:24:06',2),
	(120,69,'DEALER_ADD_PRODUCT_COMPLETE','2016-06-02 16:32:16',1),
	(121,69,'APPROVED_PRODUCT','2016-06-02 16:34:36',1),
	(122,70,'DEALER_ADD_PRODUCT_COMPLETE','2016-06-02 16:39:21',1),
	(123,70,'APPROVED_PRODUCT','2016-06-02 16:42:25',1),
	(124,NULL,'DEALER_ADD_PRODUCT_INIT','2016-06-04 17:32:20',119),
	(125,NULL,'DEALER_ADD_PRODUCT_INIT','2016-06-04 17:32:21',119),
	(126,71,'DEALER_ADD_PRODUCT_COMPLETE','2016-06-04 17:33:59',119),
	(127,71,'APPROVED_PRODUCT','2016-06-04 17:35:13',119),
	(128,72,'DEALER_ADD_PRODUCT_COMPLETE','2016-06-05 02:34:22',2),
	(129,72,'APPROVED_PRODUCT','2016-06-05 02:37:24',2),
	(130,73,'DEALER_ADD_PRODUCT_COMPLETE','2016-06-05 02:42:34',2),
	(131,73,'APPROVED_PRODUCT','2016-06-05 02:48:41',2),
	(132,74,'DEALER_ADD_PRODUCT_COMPLETE','2016-06-05 02:55:17',2),
	(133,74,'APPROVED_PRODUCT','2016-06-05 02:57:02',2),
	(134,75,'DEALER_ADD_PRODUCT_COMPLETE','2016-06-05 03:03:30',2),
	(135,75,'APPROVED_PRODUCT','2016-06-05 03:05:53',2),
	(136,76,'DEALER_ADD_PRODUCT_COMPLETE','2016-06-05 03:09:02',2),
	(137,76,'APPROVED_PRODUCT','2016-06-05 03:10:41',2),
	(138,77,'DEALER_ADD_PRODUCT_COMPLETE','2016-06-05 03:14:38',2),
	(139,77,'APPROVED_PRODUCT','2016-06-05 03:18:26',2),
	(140,78,'DEALER_ADD_PRODUCT_COMPLETE','2016-06-05 03:20:24',2),
	(141,78,'APPROVED_PRODUCT','2016-06-05 03:27:06',2),
	(142,79,'DEALER_ADD_PRODUCT_COMPLETE','2016-06-05 03:38:09',2),
	(143,79,'APPROVED_PRODUCT','2016-06-05 03:42:25',2),
	(144,81,'DEALER_ADD_PRODUCT_COMPLETE','2016-06-05 03:46:19',2),
	(145,80,'APPROVED_PRODUCT','2016-06-05 03:50:50',2),
	(146,81,'APPROVED_PRODUCT','2016-06-05 03:51:07',2),
	(147,82,'DEALER_ADD_PRODUCT_COMPLETE','2016-06-05 03:54:59',2),
	(148,82,'APPROVED_PRODUCT','2016-06-05 03:58:25',2),
	(149,83,'DEALER_ADD_PRODUCT_COMPLETE','2016-06-05 04:25:25',2),
	(150,83,'APPROVED_PRODUCT','2016-06-05 04:28:12',2),
	(151,84,'DEALER_ADD_PRODUCT_COMPLETE','2016-06-05 04:37:18',2),
	(152,84,'APPROVED_PRODUCT','2016-06-05 04:39:46',2),
	(153,85,'DEALER_ADD_PRODUCT_COMPLETE','2016-06-05 05:13:58',2),
	(154,85,'APPROVED_PRODUCT','2016-06-05 05:17:50',2),
	(155,86,'DEALER_ADD_PRODUCT_COMPLETE','2016-06-05 05:21:15',2),
	(156,86,'APPROVED_PRODUCT','2016-06-05 05:23:59',2),
	(157,87,'DEALER_ADD_PRODUCT_COMPLETE','2016-06-05 05:28:24',2),
	(158,87,'APPROVED_PRODUCT','2016-06-05 05:31:03',2),
	(159,88,'DEALER_ADD_PRODUCT_COMPLETE','2016-06-14 01:10:23',1),
	(160,88,'APPROVED_PRODUCT','2016-06-14 01:13:23',1);

/*!40000 ALTER TABLE `activitylogs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table addresses
# ------------------------------------------------------------

DROP TABLE IF EXISTS `addresses`;

CREATE TABLE `addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `address_line_one` varchar(512) NOT NULL,
  `address_line_two` varchar(512) NOT NULL,
  `city` varchar(256) NOT NULL,
  `state` varchar(256) NOT NULL,
  `country` varchar(256) NOT NULL,
  `pincode` varchar(256) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `landmark` varchar(256) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `name` varchar(100) NOT NULL DEFAULT '',
  `phonenumber` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `addresses_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `addresses` WRITE;
/*!40000 ALTER TABLE `addresses` DISABLE KEYS */;

INSERT INTO `addresses` (`id`, `user_id`, `address_line_one`, `address_line_two`, `city`, `state`, `country`, `pincode`, `timestamp`, `landmark`, `email`, `name`, `phonenumber`)
VALUES
	(2,13,'No 09/108,channakeshava nilaya, Opp to shell petrol bunk, Seegehalli , kadugodi post, Bangalore 560067','1st floor,Flat no. 108','Bangalore','Karnataka','India','560067','2016-05-23 11:47:20','Opp to shell petrol bunk','ankuraman.mishra@gmail.com','ankur mishra','7795847342'),
	(3,18,'N V RESIDENCY,FLAT NO-5','DODDATHOGUR ROAD, ELECTRONIC CITY PHASE-1','Bangalore','Karnataka','India','560100','2016-05-23 12:55:10','NEAR LAXMI NARSHIMA TEMPLE- AUTO STAND','ruchi21091@gmail.com','ruchismita chakraborty','9886382769'),
	(4,26,'Apartment No. B-502, BREN AVALON, Doddannekundi Extension,','Chinnapanahalli Main Road,','Bangalore','Karnataka','India','560048','2016-05-23 14:45:08','Near bigbasket warehouse','sujith10.cea@gmail.com','sujith kumar','7406926385'),
	(5,27,'Brigade Metropolis, Garudacharpalya','Mahadevapura, Whitefield road','Bangalore','Karnataka','India','560048','2016-05-24 05:28:26','Phoenix Market City','reechabansal@yahoo.com','Reecha Bansal','9742600477'),
	(6,42,'E1404, Smondo 3','Neotown, Electronic City Phase 1','Bangalore','Karnataka','India','560102','2016-05-24 05:46:17','Starlit Suites','haritgkumar@gmail.com','Harit Gulati','8290539343'),
	(7,15,'#5, (Second Floor, First Portion), 1st \'B\' Main Road, Chandra Reddy Layout, Behind Maharaja Hotel, 4th Block, Koramangala, Bangalore - 560034','','Bangalore','Karnataka','India','560034','2016-05-24 08:43:26','Near Maharaja Signal','aradhanasngh5@gmail.com','Aradhana Singh','8981542972'),
	(8,48,'FT- 151, F2 Block, Ittina Neela Appartments,','Andapura Village, Electronic City','Bangalore','Karnataka','India','560100','2016-05-24 10:12:52','Near Gold Coin Club, Glass Factory Layout','vips.wipro@gmail.com','Vipin Mohan','8971777887'),
	(9,54,'narayan appartment, Dodda tagore','electronic city phase 1 , konop agarahara,','Bangalore','Karnataka','India','560100','2016-05-24 12:27:11','near mm school','avikm01@gmail.com','Avik mukherjee','9836033457'),
	(10,55,'Flat No. 38, Room No. 9 4th Floor, 20th Cross Ejipura Main Road','KMR Building','Bangalore','Karnataka','India','560047','2016-05-24 13:07:35','Near Rama Temple','subratasrk1940@gmail.com','Subrata Sarkar','7411063991'),
	(11,21,'#426, 1st CROS , 4th MAIN','Vijaya Layout','Bangalore','Karnataka','India','560076','2016-05-25 11:41:10','Opposite Diana Memorial High School','26anoop1990@gmail.com','Anoop K','9686482400'),
	(12,62,'Shri laxmi nivas , #47 , 1st mn , 2nd crs, apparao layout, rammurthy nagar -','','Bangalore','Karnataka','India','560016','2016-05-26 06:03:10','more supermarket near fresh heritage opposite lane','peravelli8@gmail.com','purna eravelli','7776933090'),
	(13,63,'Flat s-2, Block-1, Wild grass Apts ,','Nirguna Mandir Layout , S.T.Bed , Koramangala','Bangalore','Karnataka','India','560047','2016-05-26 12:53:03','Koramangala near gem regency','kritishmani@gmail.com','Manikandan Balasubramanian','9513407060'),
	(14,3,'VS Chalet','LBS Nagar','Bangalore','Karnataka','India','560017','2016-05-26 17:49:09','Near Nilgris','vineethkumart@gmail.com','Vineeth Kumar','9538092344'),
	(15,76,'Flat No: GF2, Maruthi Residency, Opposite Sobha Marvella,','Green Glen layout, Bellandur','Bangalore','Karnataka','India','560103','2016-05-30 10:16:45','Karnataka','ashishgpt24@gmail.com','Ashish Gupta','8884261148'),
	(16,78,'No 15/1 Ground floor, Shesha Shyla Apts','Vinayakanagar, Syndicate Bank Road (4th Main, 3rd Cross)','Bangalore','Karnataka','India','560017','2016-05-30 11:39:52','Left turn after SNS Arcade, at Syndicate Bank','anusha@smoodies.in','Anusha Bhushan','9920588039'),
	(17,81,'First floor, Arunachalam, No. 49,','St. Anthony Nicolas Street, Ashok Nagar','Bangalore','Karnataka','India','560088','2016-05-31 06:24:09','Ozone Hotel, Markham Road','cinthya.anand@gmail.com','Cinthya Anand','8971062056'),
	(18,84,'108/1, B Cross, Block-2 B2','BSR Splendour Park Apartment, Vijaya Bank Colony Extension, Horamavu, Banaswadi, Bangalore-500043.','Bangalore','Karnataka','India','500043','2016-05-31 09:32:21','Vijaya Bank Colony Extension','ganesh.sangale@gmail.com','Ganesh Sangale','9702162999'),
	(19,87,'# 35, 42nd Main, KAS Officers Colony,','BTM 2nd Stage, Bangalore','Bangalore','Karnataka','India','560068','2016-06-01 06:24:10','Besides Brigade Lakeview Apartment','suraagini9@gmail.com','Papuli Banerjee','9901777228'),
	(20,93,'#26, 12th Cross,','Bellandur','Bangalore','Karnataka','India','560103','2016-06-01 08:56:42','Near Siva Temple','p11anandara@iima.ac.in','Antony Anand','8884666567'),
	(21,110,'I-002, Bren palms','kudlu main road,kudlu gate, bangalore','Bangalore','Karnataka','India','560068','2016-06-03 13:39:01','opposite to sri bhagya hotel','racheetar@gmail.com','racheeta ramanujam','9900813275'),
	(22,98,'Motappanapalya','Indiranager','Bangalore','Karnataka','India','560038','2016-06-03 14:00:26','S S B school','cellplanet484@gmail.com','Latheef Perla','9886385606'),
	(23,112,'House No. 123, 2nd Cross, 5th Main, BTM 2nd Stage','MICO Layout','Bangalore','Karnataka','India','560076','2016-06-04 06:09:55','Near BTM Water Tank and Indian Oil Petrol Bunk','charusaxena6495s@gmail.com','charu saxena','8880860600'),
	(24,113,'F1, Meenakshi Residency','Ashwini layout, 1st main, 4th cross, Ejipura','Bangalore','Karnataka','India','560093','2016-06-04 08:09:29','behind MTR office','praneta.agrawal@gmail.com','Praneta Agrawal','9686528436'),
	(25,115,'#39,1st cross,new kaverappa layout','Panathurpost,varthurhobli,Kadubishanahalli','Bangalore','Karnataka','India','560103','2016-06-04 09:42:38','5star chicken','rrajesh222@gmail.com','Rajesh Daadi','7795649939'),
	(26,116,'d 202 golden nest apartments jcr layout 2nd cross','panatur kadubeesinahalli','Bangalore','Karnataka','India','560103','2016-06-04 12:15:39','jcr layout','anishmac99@gmail.com','anish chandra','9731150033'),
	(27,109,'No3, 2nd Floor , Corner House','29th A Cross , Balaji Layout , Kaggadaspura','Bangalore','Karnataka','India','560093','2016-06-04 13:53:05','1st left , 3rd building','trailokya4u@gmail.com','Trailokya B','7086001777'),
	(28,109,'No3, 2nd Floor , Corner House','29th A Cross , Balaji Layout , Kaggadaspura','Bangalore','Karnataka','India','560093','2016-06-04 13:53:05','1st left , 3rd building','trailokya4u@gmail.com','Trailokya B','7086001777'),
	(29,109,'No3, 2nd Floor , Corner House','29th A Cross , Balaji Layout , Kaggadaspura','Bangalore','Karnataka','India','560093','2016-06-04 13:53:06','1st left , 3rd building','trailokya4u@gmail.com','Trailokya B','7086001777'),
	(30,109,'No3, 2nd Floor , Corner House','29th A Cross , Balaji Layout , Kaggadaspura','Bangalore','Karnataka','India','560093','2016-06-04 13:53:06','1st left , 3rd building','trailokya4u@gmail.com','Trailokya B','7086001777'),
	(31,109,'No3, 2nd Floor , Corner House','29th A Cross , Balaji Layout , Kaggadaspura','Bangalore','Karnataka','India','560093','2016-06-04 13:53:06','1st left , 3rd building','trailokya4u@gmail.com','Trailokya B','7086001777'),
	(32,119,'Flat T02, Building 1647','18th cross, 26th main, HSR Layour, Sector 2','Bangalore','Karnataka','India','560102','2016-06-04 17:38:54','Near NIFT College','chabigupta2009@gmail.com','Chabi Gupta','8971261714'),
	(33,88,'C-412, Maple Block, Raheja Residency','Koramangala 3rd Block, Bengaluru, Karnataka 560034','Bangalore','Karnataka','India','560034','2016-06-05 03:27:03','Raheja Residency','bayyangar@gmail.com','Balaji Ayyangar','7358316785'),
	(34,121,'# 252 11TH MAIN 13TH CROSS WILLSEN GARDEN BANGLORE 27','8951056226','Bangalore','Karnataka','India','560027','2016-06-05 07:10:10','BRAND FACTORY','yogesh.0607@yahoo.in','YOGESH YOGI','9986122456'),
	(35,122,'Second floor','No.77/38 , 1st Main, 11th cross, Bharathi layout, opp.Assissi Bhavan, S.G Palya','Bangalore','Karnataka','India','560029','2016-06-05 09:49:31','Assissi Bhavan','barshababuraj@gmail.com','Barsha Baburaj','9846263515'),
	(36,117,'P1(4th Floor), Fatima Residency, 36, 1st Main Road','ST Bed layout, Cauvery Colony, Koramangala 4th block','Bangalore','Karnataka','India','560034','2016-06-05 09:51:15','Opp Juice junction','16.shiladitya@gmail.com','Shiladitya Mandal','8197065678'),
	(37,125,'Dr. No. S/203, House no. 12/13','Maruthi Nilayam, 2nd Cross, Lakshamma Layout, Dodda Banaswadi','Bangalore','Karnataka','India','560043','2016-06-05 09:58:42','Kundan Electronics, & Mukunthamma Temple','quietnd@gmail.com','Nandini Dutta','9035168837'),
	(38,127,'No.20/1,Kalahalli,Opp to Ulsoor Lake,Next to MEG Main Gate,SC Garden Post,Bangalore-560042','C1/5,BDA Flat,2nd floor,Kalahalli Main Road,SC Garden post,Bangalore','Bangalore','Karnataka','India','560042','2016-06-06 11:31:41','Opp to Ulsoor Lake,Kalahalli Main Road,dead end (Grocery shop) turn right u will get Anjineya temple further turn right','anilmn@rediffmail.com','Anil Menon','7406537405'),
	(39,129,'#409 th , a main 2nd block hrbr layout kalyan nagar Bangalore','','Bangalore','Karnataka','India','560043','2016-06-07 02:18:34','Behind banasawadi club','nikhilriphone@gmail.com','Nikhil Ravishankar','9901343482'),
	(40,5,'rs','','Bangalore','Karnataka','India','560017','2016-06-07 06:04:44','prabhu','duraigowardhan.s@gmail.com','durai gowardhan','9944951838'),
	(41,107,'f2 emrald enclave, keraguddahalli, chik wanwara road, Abhigere,jalhali west Bangalore','f-2 emrald enclave, keraguddahalli, chik wanwara road, Abhigere,jalhali west Bangalore','Bangalore','Karnataka','India','560092','2016-06-07 08:18:22','oposite to DS Max','akt656715@gmail.com','ashok tiwari','8971914984'),
	(42,133,'Sarjapur Road, beside wipro','Y5-301 Greenwood regency 3rd floor','Bangalore','Karnataka','India','560035','2016-06-07 10:12:24','wipro','ali6988f@gmail.com','Aliya N','9739115275'),
	(43,134,'Garma Garam, Arekere, MICO Layout, Bannerghatta Road','','Bangalore','Karnataka','India','560076','2016-06-07 10:13:57','Infornt of Canara Bank','jeetcx@gmail.com','Abhijeet ','8880157153'),
	(44,137,'#105 Shiva Sai Kalpa, Tech City Layout','Doddathoguru Village, Electronic city phase1','Bangalore','Karnataka','India','560100','2016-06-08 14:22:41','Durga Bakery/Thoguru Cross','hillol.mazumder@outlook.com','hillol mazumder','9632608833');

/*!40000 ALTER TABLE `addresses` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cartitems
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cartitems`;

CREATE TABLE `cartitems` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cart_id` int(11) unsigned NOT NULL,
  `item_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `cartitems` WRITE;
/*!40000 ALTER TABLE `cartitems` DISABLE KEYS */;

INSERT INTO `cartitems` (`id`, `cart_id`, `item_id`)
VALUES
	(2,6,18),
	(3,14,25),
	(5,22,17),
	(7,27,13),
	(8,29,14),
	(9,35,3),
	(10,15,20),
	(12,31,8),
	(13,45,8),
	(14,46,26),
	(15,47,6),
	(16,52,20),
	(18,54,27),
	(19,58,1),
	(20,61,23),
	(21,65,5),
	(22,66,28),
	(23,69,29),
	(24,74,26),
	(25,77,6),
	(27,79,33),
	(28,3,32),
	(29,3,31),
	(30,81,34),
	(31,88,34),
	(33,95,26),
	(34,97,26),
	(35,100,61),
	(36,103,58),
	(37,107,64),
	(38,108,60),
	(39,108,66),
	(40,111,66),
	(41,21,65),
	(42,84,68),
	(45,78,69),
	(46,132,56),
	(48,135,70),
	(49,136,62),
	(50,137,67),
	(51,138,65),
	(52,141,62),
	(53,143,55),
	(54,130,57),
	(55,144,57),
	(56,148,71),
	(58,149,72),
	(59,152,64),
	(60,153,76),
	(62,153,82),
	(64,145,68),
	(65,156,82),
	(66,157,82),
	(67,159,80),
	(68,160,87),
	(69,145,76),
	(71,84,86),
	(72,162,9),
	(73,163,11),
	(74,164,85),
	(75,165,63),
	(76,166,59),
	(78,167,75),
	(81,5,86),
	(82,170,73),
	(86,172,68),
	(87,128,86),
	(89,177,85),
	(90,178,82),
	(91,181,83),
	(95,188,12),
	(96,191,74),
	(98,194,77),
	(99,196,11),
	(101,197,88),
	(102,198,81);

/*!40000 ALTER TABLE `cartitems` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table carts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `carts`;

CREATE TABLE `carts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `ispurchased` tinyint(1) DEFAULT '0',
  `isexpired` tinyint(1) DEFAULT '0',
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `carts` WRITE;
/*!40000 ALTER TABLE `carts` DISABLE KEYS */;

INSERT INTO `carts` (`id`, `user_id`, `ispurchased`, `isexpired`, `timestamp`)
VALUES
	(1,1,0,0,'2016-05-22 14:27:54'),
	(2,2,0,0,'2016-05-22 14:29:03'),
	(3,3,1,0,'2016-05-23 04:16:15'),
	(4,4,0,0,'2016-05-23 05:17:52'),
	(5,5,0,0,'2016-05-23 05:32:11'),
	(6,5,1,0,'2016-05-23 05:32:29'),
	(7,6,0,0,'2016-05-23 06:23:59'),
	(8,8,0,0,'2016-05-23 09:59:24'),
	(9,9,0,0,'2016-05-23 10:00:28'),
	(10,10,0,0,'2016-05-23 10:29:24'),
	(11,11,0,0,'2016-05-23 11:10:24'),
	(12,12,0,0,'2016-05-23 11:22:09'),
	(13,13,0,0,'2016-05-23 11:42:36'),
	(14,13,1,0,'2016-05-23 11:47:33'),
	(15,15,1,0,'2016-05-23 12:03:10'),
	(16,16,0,0,'2016-05-23 12:09:51'),
	(17,17,0,0,'2016-05-23 12:16:28'),
	(18,18,0,0,'2016-05-23 12:24:48'),
	(19,19,0,0,'2016-05-23 12:36:06'),
	(20,20,0,0,'2016-05-23 12:43:53'),
	(21,21,1,0,'2016-05-23 12:54:05'),
	(22,18,1,0,'2016-05-23 12:55:33'),
	(24,22,0,0,'2016-05-23 13:16:15'),
	(25,23,0,0,'2016-05-23 13:18:29'),
	(26,24,0,0,'2016-05-23 13:19:58'),
	(27,5,1,0,'2016-05-23 13:20:30'),
	(28,25,0,0,'2016-05-23 13:21:06'),
	(29,5,1,0,'2016-05-23 13:28:53'),
	(30,26,0,0,'2016-05-23 14:28:04'),
	(31,27,0,0,'2016-05-23 14:34:27'),
	(32,28,0,0,'2016-05-23 14:35:57'),
	(33,29,0,0,'2016-05-23 14:41:42'),
	(34,30,0,0,'2016-05-23 14:44:53'),
	(35,26,1,0,'2016-05-23 14:45:24'),
	(36,31,0,0,'2016-05-23 14:55:41'),
	(37,32,0,0,'2016-05-23 14:59:22'),
	(38,33,0,0,'2016-05-23 15:32:50'),
	(39,34,0,0,'2016-05-23 15:34:58'),
	(40,35,0,0,'2016-05-23 16:01:26'),
	(41,37,0,0,'2016-05-23 16:32:05'),
	(42,39,0,0,'2016-05-23 17:59:57'),
	(43,41,0,0,'2016-05-24 03:47:26'),
	(44,42,0,0,'2016-05-24 05:00:22'),
	(45,27,1,0,'2016-05-24 05:28:40'),
	(46,42,1,0,'2016-05-24 05:49:37'),
	(47,42,1,0,'2016-05-24 05:50:46'),
	(48,43,0,0,'2016-05-24 06:15:58'),
	(49,44,0,0,'2016-05-24 06:19:53'),
	(50,45,0,0,'2016-05-24 06:42:43'),
	(51,46,0,0,'2016-05-24 06:56:03'),
	(52,15,1,0,'2016-05-24 08:43:35'),
	(53,47,0,0,'2016-05-24 09:19:39'),
	(54,15,1,0,'2016-05-24 09:28:54'),
	(55,48,0,0,'2016-05-24 09:41:34'),
	(56,49,0,0,'2016-05-24 10:04:53'),
	(57,15,0,0,'2016-05-24 10:10:08'),
	(58,48,1,0,'2016-05-24 10:20:45'),
	(59,50,0,0,'2016-05-24 10:23:25'),
	(60,51,0,0,'2016-05-24 10:26:04'),
	(61,5,1,0,'2016-05-24 10:39:54'),
	(62,52,0,0,'2016-05-24 11:21:28'),
	(63,53,0,0,'2016-05-24 11:51:27'),
	(64,54,0,0,'2016-05-24 12:25:31'),
	(65,54,1,0,'2016-05-24 12:27:13'),
	(66,54,1,0,'2016-05-24 12:32:40'),
	(67,55,0,0,'2016-05-24 12:42:50'),
	(68,56,0,0,'2016-05-24 12:58:40'),
	(69,55,1,0,'2016-05-24 13:08:32'),
	(70,57,0,0,'2016-05-24 14:39:24'),
	(71,58,0,0,'2016-05-24 16:19:35'),
	(72,59,0,0,'2016-05-24 17:32:36'),
	(73,60,0,0,'2016-05-24 18:39:02'),
	(74,21,1,0,'2016-05-25 11:41:13'),
	(75,61,0,0,'2016-05-25 14:24:31'),
	(76,62,0,0,'2016-05-26 03:41:20'),
	(77,62,1,0,'2016-05-26 06:03:14'),
	(78,63,1,0,'2016-05-26 12:46:51'),
	(79,63,1,0,'2016-05-26 12:53:06'),
	(80,64,0,0,'2016-05-26 14:01:05'),
	(81,65,0,0,'2016-05-26 14:51:50'),
	(82,66,0,0,'2016-05-26 15:04:17'),
	(83,67,0,0,'2016-05-26 17:45:34'),
	(84,3,1,0,'2016-05-26 17:57:01'),
	(85,68,0,0,'2016-05-27 02:01:26'),
	(86,69,0,0,'2016-05-27 11:21:19'),
	(87,70,0,0,'2016-05-27 11:28:57'),
	(88,26,1,0,'2016-05-27 12:28:08'),
	(89,71,0,0,'2016-05-27 18:16:23'),
	(90,72,0,0,'2016-05-28 13:41:28'),
	(91,73,0,0,'2016-05-29 04:41:07'),
	(92,74,0,0,'2016-05-29 06:49:01'),
	(93,75,0,0,'2016-05-29 07:18:39'),
	(94,76,0,0,'2016-05-30 10:15:34'),
	(95,76,1,0,'2016-05-30 10:16:54'),
	(96,78,0,0,'2016-05-30 11:35:56'),
	(97,78,1,0,'2016-05-30 11:40:03'),
	(98,80,0,0,'2016-05-31 04:15:33'),
	(99,81,0,0,'2016-05-31 06:08:28'),
	(100,81,1,0,'2016-05-31 06:24:22'),
	(101,82,0,0,'2016-05-31 08:45:46'),
	(102,83,0,0,'2016-05-31 08:46:13'),
	(103,84,1,0,'2016-05-31 09:28:38'),
	(104,85,0,0,'2016-05-31 09:50:20'),
	(105,84,0,0,'2016-05-31 10:29:52'),
	(106,86,0,0,'2016-05-31 10:48:10'),
	(107,63,1,0,'2016-05-31 12:39:39'),
	(108,87,1,0,'2016-06-01 05:12:57'),
	(109,89,0,0,'2016-06-01 05:30:11'),
	(110,88,0,0,'2016-06-01 05:35:21'),
	(111,87,1,0,'2016-06-01 06:22:00'),
	(112,87,0,0,'2016-06-01 06:23:50'),
	(113,90,0,0,'2016-06-01 06:51:45'),
	(114,91,0,0,'2016-06-01 07:27:41'),
	(115,92,0,0,'2016-06-01 07:52:22'),
	(116,93,0,0,'2016-06-01 08:50:44'),
	(117,94,0,0,'2016-06-01 12:25:35'),
	(118,95,0,0,'2016-06-01 15:20:25'),
	(119,96,0,0,'2016-06-02 07:06:54'),
	(120,97,0,0,'2016-06-02 13:15:36'),
	(121,98,0,0,'2016-06-02 15:39:36'),
	(122,99,0,0,'2016-06-02 16:04:35'),
	(123,101,0,0,'2016-06-02 16:50:26'),
	(124,102,0,0,'2016-06-03 02:02:18'),
	(125,103,0,0,'2016-06-03 02:30:32'),
	(126,104,0,0,'2016-06-03 07:05:00'),
	(127,105,0,0,'2016-06-03 07:23:42'),
	(128,107,1,0,'2016-06-03 09:47:54'),
	(129,108,0,0,'2016-06-03 11:16:33'),
	(130,109,0,0,'2016-06-03 12:27:39'),
	(131,110,0,0,'2016-06-03 13:36:24'),
	(132,110,1,0,'2016-06-03 13:39:05'),
	(133,111,0,0,'2016-06-04 03:01:59'),
	(134,112,0,0,'2016-06-04 06:07:20'),
	(135,112,1,0,'2016-06-04 06:10:04'),
	(136,98,1,0,'2016-06-04 06:12:17'),
	(137,98,1,0,'2016-06-04 06:49:05'),
	(138,113,1,0,'2016-06-04 08:00:20'),
	(139,114,0,0,'2016-06-04 08:19:01'),
	(140,115,0,0,'2016-06-04 09:40:23'),
	(141,115,1,0,'2016-06-04 09:43:46'),
	(142,116,0,0,'2016-06-04 12:14:05'),
	(143,116,1,0,'2016-06-04 12:18:20'),
	(144,109,1,0,'2016-06-04 13:54:12'),
	(145,117,1,0,'2016-06-04 14:58:17'),
	(146,118,0,0,'2016-06-04 17:08:51'),
	(147,119,0,0,'2016-06-04 17:28:19'),
	(148,119,1,0,'2016-06-04 17:38:57'),
	(149,88,1,0,'2016-06-05 03:27:22'),
	(150,120,0,0,'2016-06-05 05:38:53'),
	(151,121,0,0,'2016-06-05 07:02:21'),
	(152,121,1,0,'2016-06-05 07:10:17'),
	(153,122,0,0,'2016-06-05 07:13:46'),
	(154,123,0,0,'2016-06-05 08:13:16'),
	(155,124,0,0,'2016-06-05 08:19:27'),
	(156,122,1,0,'2016-06-05 09:49:50'),
	(157,122,1,0,'2016-06-05 09:51:19'),
	(158,125,0,0,'2016-06-05 09:55:18'),
	(159,125,1,0,'2016-06-05 09:59:36'),
	(160,116,1,0,'2016-06-05 11:19:58'),
	(161,3,0,0,'2016-06-05 18:39:50'),
	(162,3,1,0,'2016-06-05 19:17:26'),
	(163,3,1,0,'2016-06-05 19:22:03'),
	(164,3,1,0,'2016-06-05 19:34:40'),
	(165,98,1,0,'2016-06-06 06:01:43'),
	(166,117,1,0,'2016-06-06 06:22:17'),
	(167,127,1,0,'2016-06-06 11:00:47'),
	(168,128,0,0,'2016-06-06 11:24:38'),
	(169,129,0,0,'2016-06-06 17:40:32'),
	(170,129,1,0,'2016-06-07 02:18:36'),
	(171,117,0,0,'2016-06-07 03:39:38'),
	(172,5,1,0,'2016-06-07 06:04:47'),
	(173,130,0,0,'2016-06-07 06:20:36'),
	(174,131,0,0,'2016-06-07 06:28:01'),
	(175,132,0,0,'2016-06-07 09:49:54'),
	(176,133,0,0,'2016-06-07 10:05:12'),
	(177,134,1,0,'2016-06-07 10:12:08'),
	(178,133,1,0,'2016-06-07 10:17:46'),
	(179,135,0,0,'2016-06-08 07:28:27'),
	(180,136,0,0,'2016-06-08 08:42:43'),
	(181,137,1,0,'2016-06-08 13:48:08'),
	(182,138,0,0,'2016-06-09 05:46:20'),
	(183,139,0,0,'2016-06-09 06:55:52'),
	(184,140,0,0,'2016-06-09 09:00:07'),
	(185,141,0,0,'2016-06-09 10:59:34'),
	(186,142,0,0,'2016-06-09 11:05:15'),
	(187,143,0,0,'2016-06-09 13:06:45'),
	(188,88,1,0,'2016-06-09 13:32:47'),
	(189,137,0,0,'2016-06-09 14:28:35'),
	(190,144,0,0,'2016-06-09 15:29:27'),
	(191,5,1,0,'2016-06-10 01:19:29'),
	(192,145,0,0,'2016-06-10 10:10:25'),
	(193,146,0,0,'2016-06-11 06:36:48'),
	(194,3,1,0,'2016-06-12 09:14:53'),
	(195,147,0,0,'2016-06-13 02:37:51'),
	(196,88,1,0,'2016-06-13 13:17:49'),
	(197,110,1,0,'2016-06-14 02:06:38'),
	(198,81,1,0,'2016-06-14 06:51:44'),
	(199,150,0,0,'2016-06-18 10:55:36'),
	(200,151,0,0,'2016-06-21 05:48:31'),
	(201,152,0,0,'2016-06-21 12:27:26'),
	(202,153,0,0,'2016-06-21 14:32:44'),
	(203,154,0,0,'2016-06-24 11:54:50'),
	(204,155,0,0,'2016-06-26 04:34:29'),
	(205,156,0,0,'2016-06-27 14:25:15'),
	(206,157,0,0,'2016-06-29 05:20:34'),
	(207,158,0,0,'2016-06-29 10:49:55'),
	(208,159,0,0,'2016-06-29 11:06:57'),
	(209,160,0,0,'2016-06-29 17:25:10'),
	(210,161,0,0,'2016-07-04 07:35:21'),
	(211,162,0,0,'2016-07-14 07:52:44');

/*!40000 ALTER TABLE `carts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table filters
# ------------------------------------------------------------

DROP TABLE IF EXISTS `filters`;

CREATE TABLE `filters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filtertype_id` int(11) NOT NULL,
  `name` varchar(512) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `filters` WRITE;
/*!40000 ALTER TABLE `filters` DISABLE KEYS */;

INSERT INTO `filters` (`id`, `filtertype_id`, `name`, `timestamp`)
VALUES
	(1,1,'Double Door','2016-03-03 21:19:13'),
	(2,1,'Single Door','2016-03-03 21:19:19'),
	(3,2,'below Rs.5000','2016-03-26 15:36:23'),
	(4,2,'Rs.5000 to 10000','2016-03-26 15:36:30'),
	(5,3,'below 200 lts','2016-03-26 15:35:44'),
	(6,3,'above 200 lts','2016-03-26 15:35:55'),
	(7,4,'Red','2016-03-26 14:29:11'),
	(8,4,'Blue','2016-03-26 14:29:13'),
	(9,4,'white','2016-03-26 14:29:15'),
	(10,4,'other colours','2016-03-26 14:29:22'),
	(11,5,'less than 3 star','2016-03-26 14:38:39'),
	(12,5,'3 star and above','2016-03-26 14:54:34'),
	(13,6,'Fully Automatic','2016-03-26 15:29:46'),
	(14,6,'Semi Automatic','2016-03-26 15:29:57'),
	(15,7,'Front Load','2016-03-26 15:30:13'),
	(16,7,'Top Load','2016-03-26 15:30:21'),
	(17,8,'6Kg and below','2016-03-26 15:34:25'),
	(18,8,'Above 6Kg','2016-03-26 15:34:22'),
	(19,9,'Split','2016-03-26 16:09:17'),
	(20,9,'Window','2016-03-26 16:09:27'),
	(21,10,'Less than 2 tonnes','2016-03-26 16:11:00'),
	(22,10,'2 tonnes & above','2016-03-26 16:11:11'),
	(23,11,'Less than 3 star','2016-03-26 16:11:27'),
	(24,11,'3 star & above','2016-03-26 16:11:38'),
	(25,12,'Electrical','2016-03-26 16:12:30'),
	(26,12,'Non-Electrical','2016-03-26 16:12:50'),
	(27,13,'Gravity based','2016-03-26 16:14:08'),
	(28,13,'RO+UV based','2016-03-26 16:19:22'),
	(29,14,'7Lts and below','2016-03-26 16:19:58'),
	(30,14,'Above 7Lts','2016-03-26 16:20:17'),
	(31,15,'3 Seater & below ','2016-03-26 16:25:10'),
	(32,15,'Above 3 Seater','2016-03-26 16:25:28'),
	(33,16,'Fabric','2016-03-26 16:25:44'),
	(34,16,'Leather','2016-03-26 16:25:53'),
	(35,16,'Wood','2016-03-26 16:26:22'),
	(36,16,'Metal','2016-03-26 16:26:34'),
	(37,17,'Straight','2016-03-26 16:26:58'),
	(38,17,'L & U Shaped','2016-03-26 16:27:13'),
	(39,18,'Beige','2016-03-26 16:27:56'),
	(40,18,'Black','2016-03-26 16:28:02'),
	(41,18,'Other Colors','2016-03-26 16:28:18'),
	(42,19,'Plastic','2016-03-26 16:31:46'),
	(43,19,'Fabric','2016-03-26 16:31:52'),
	(44,19,'Leatherette','2016-03-26 16:32:01'),
	(45,20,'Black','2016-03-26 16:33:37'),
	(46,20,'Beige','2016-03-26 16:33:53'),
	(47,20,'Other colors','2016-03-26 16:33:59'),
	(48,21,'Without Wheels ','2016-03-26 16:35:36'),
	(49,21,'With Seat Lock & Height Adjustment','2016-03-26 16:35:12'),
	(50,22,'Yes','2016-03-26 16:40:15'),
	(51,22,'No','2016-03-26 16:40:20'),
	(52,23,'Yes','2016-03-26 16:40:27'),
	(53,23,'No','2016-03-26 16:40:33');

/*!40000 ALTER TABLE `filters` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table filtertypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `filtertypes`;

CREATE TABLE `filtertypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `productcategory_id` int(11) NOT NULL,
  `name` varchar(512) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `filtertypes` WRITE;
/*!40000 ALTER TABLE `filtertypes` DISABLE KEYS */;

INSERT INTO `filtertypes` (`id`, `productcategory_id`, `name`, `timestamp`)
VALUES
	(1,3,'Door Type','2016-03-03 20:48:31'),
	(2,3,'Price','2016-03-26 14:23:04'),
	(3,3,'Capacity','2016-03-26 14:23:07'),
	(4,3,'Color','2016-03-26 14:23:09'),
	(5,3,'Energy Rating','2016-03-26 14:40:20'),
	(6,4,'Function Type','2016-03-26 14:50:14'),
	(7,4,'Load Type','2016-03-26 14:50:33'),
	(8,4,'Capacity','2016-03-26 15:26:46'),
	(9,5,'Type','2016-03-26 15:37:14'),
	(10,5,'Capacity','2016-03-26 15:37:27'),
	(11,5,'Energy Efficency','2016-03-26 15:37:41'),
	(12,6,'Type','2016-03-26 15:38:24'),
	(13,6,'Purification Technology','2016-03-26 15:38:34'),
	(14,6,'Capacity','2016-03-26 15:38:52'),
	(15,7,'Seating Capacity','2016-03-26 16:22:02'),
	(16,7,'Material','2016-03-26 16:22:30'),
	(17,7,'Shape','2016-03-26 16:22:40'),
	(18,7,'Color','2016-03-26 16:22:48'),
	(19,8,'Material','2016-03-26 16:28:59'),
	(20,8,'Color','2016-03-26 16:29:05'),
	(21,8,'Type','2016-03-26 16:30:11'),
	(22,9,'Wheels Included','2016-03-26 16:39:28'),
	(23,9,'Storage Included','2016-03-26 16:40:53');

/*!40000 ALTER TABLE `filtertypes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table images
# ------------------------------------------------------------

DROP TABLE IF EXISTS `images`;

CREATE TABLE `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `description` text,
  `thumbnail` varchar(512) DEFAULT '',
  `isdefault` int(11) DEFAULT '0',
  `productitem_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_images_products_vendors1_idx` (`productitem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `images` WRITE;
/*!40000 ALTER TABLE `images` DISABLE KEYS */;

INSERT INTO `images` (`id`, `name`, `description`, `thumbnail`, `isdefault`, `productitem_id`)
VALUES
	(1,'1_1.jpg','Image 1','',1,1),
	(2,'1_2.jpg','Image 2','',0,1),
	(3,'1_3.jpg','Image 3','',0,1),
	(4,'1_4.jpg','Image 4','',0,1),
	(5,'1_5.jpg','Image 5','',0,1),
	(6,'2_1.jpg','Image 1','',1,2),
	(7,'2_2.jpg','Image 2','',0,2),
	(8,'2_3.jpg','Image 3','',0,2),
	(9,'2_4.jpg','Image 4','',0,2),
	(10,'2_5.jpg','Image 5','',0,2),
	(11,'3_1.jpg','Image 1','',1,3),
	(12,'3_2.jpg','Image 2','',0,3),
	(13,'3_3.jpg','Image 3','',0,3),
	(14,'3_4.jpg','Image 4','',0,3),
	(15,'3_5.jpg','Image 5','',0,3),
	(16,'4_1.jpg','Image 1','',1,4),
	(17,'4_2.jpg','Image 2','',0,4),
	(18,'4_3.jpg','Image 3','',0,4),
	(19,'4_4.jpg','Image 4','',0,4),
	(20,'4_5.jpg','Image 5','',0,4),
	(21,'4_6.jpg','Image 6','',0,4),
	(22,'5_1.jpg','Image 1','',1,5),
	(23,'5_2.jpg','Image 2','',0,5),
	(24,'5_3.jpg','Image 3','',0,5),
	(25,'5_4.jpg','Image 4','',0,5),
	(26,'5_5.jpg','Image 5','',0,5),
	(27,'5_6.jpg','Image 6','',0,5),
	(28,'6_1.jpg','Image 1','',1,6),
	(29,'6_2.jpg','Image 2','',0,6),
	(30,'6_3.jpg','Image 3','',0,6),
	(31,'6_4.jpg','Image 4','',0,6),
	(32,'6_5.jpg','Image 5','',0,6),
	(33,'7_1.jpg','Image 1','',1,7),
	(34,'7_2.jpg','Image 2','',0,7),
	(35,'7_3.jpg','Image 3','',0,7),
	(36,'7_4.jpg','Image 4','',0,7),
	(37,'7_5.jpg','Image 5','',0,7),
	(38,'7_6.jpg','Image 6','',0,7),
	(39,'7_7.jpg','Image 7','',0,7),
	(40,'8_1.jpg','Image 1','',1,8),
	(41,'8_2.jpg','Image 2','',0,8),
	(42,'8_3.jpg','Image 3','',0,8),
	(43,'8_4.jpg','Image 4','',0,8),
	(44,'8_5.jpg','Image 5','',0,8),
	(45,'9_1.jpg','Image 1','',1,9),
	(46,'9_2.jpg','Image 2','',0,9),
	(47,'9_3.jpg','Image 3','',0,9),
	(48,'9_4.jpg','Image 4','',0,9),
	(49,'9_5.jpg','Image 5','',0,9),
	(50,'10_1.jpg','Image 1','',1,10),
	(51,'10_2.jpg','Image 2','',0,10),
	(52,'10_3.jpg','Image 3','',0,10),
	(53,'10_4.jpg','Image 4','',0,10),
	(54,'11_1.jpg','Image 1','',1,11),
	(55,'11_2.jpg','Image 2','',0,11),
	(56,'11_3.jpg','Image 3','',0,11),
	(57,'11_4.jpg','Image 4','',0,11),
	(58,'11_5.jpg','Image 5','',0,11),
	(59,'11_6.jpg','Image 6','',0,11),
	(60,'11_7.jpg','Image 7','',0,11),
	(61,'12_1.jpg','Image 1','',1,12),
	(62,'12_2.jpg','Image 2','',0,12),
	(63,'12_3.jpg','Image 3','',0,12),
	(64,'12_4.jpg','Image 4','',0,12),
	(65,'12_5.jpg','Image 5','',0,12),
	(66,'12_6.jpg','Image 6','',0,12),
	(67,'13_1.jpg','Image 1','',1,13),
	(68,'13_2.jpg','Image 2','',0,13),
	(69,'13_3.jpg','Image 3','',0,13),
	(70,'13_4.jpg','Image 4','',0,13),
	(71,'13_5.jpg','Image 5','',0,13),
	(72,'14_1.jpg','Image 1','',1,14),
	(73,'14_2.jpg','Image 2','',0,14),
	(74,'14_3.jpg','Image 3','',0,14),
	(75,'14_4.jpg','Image 4','',0,14),
	(76,'14_5.jpg','Image 5','',0,14),
	(77,'14_6.jpg','Image 6','',0,14),
	(78,'15_1.jpg','Image 1','',1,15),
	(79,'15_2.jpg','Image 2','',0,15),
	(80,'15_3.jpg','Image 3','',0,15),
	(81,'15_4.jpg','Image 4','',0,15),
	(82,'15_5.jpg','Image 5','',0,15),
	(83,'15_6.jpg','Image 6','',0,15),
	(84,'16_1.jpg','Image 1','',1,16),
	(85,'16_2.jpg','Image 2','',0,16),
	(86,'16_3.jpg','Image 3','',0,16),
	(87,'16_4.jpg','Image 4','',0,16),
	(88,'16_5.jpg','Image 5','',0,16),
	(89,'17_1.jpg','Image 1','',1,17),
	(90,'17_2.jpg','Image 2','',0,17),
	(91,'17_3.jpg','Image 3','',0,17),
	(92,'17_4.jpg','Image 4','',0,17),
	(93,'17_5.jpg','Image 5','',0,17),
	(94,'17_6.jpg','Image 6','',0,17),
	(95,'18_1.jpg','Image 1','',1,18),
	(96,'18_2.jpg','Image 2','',0,18),
	(97,'18_3.jpg','Image 3','',0,18),
	(98,'18_4.jpg','Image 4','',0,18),
	(99,'18_5.jpg','Image 5','',0,18),
	(100,'18_6.jpg','Image 6','',0,18),
	(101,'19_1.jpg','Image 1','',1,19),
	(102,'19_2.jpg','Image 2','',0,19),
	(103,'19_3.jpg','Image 3','',0,19),
	(104,'19_4.jpg','Image 4','',0,19),
	(105,'19_5.jpg','Image 5','',0,19),
	(106,'19_6.jpg','Image 6','',0,19),
	(107,'20_1.jpg','Image 1','',1,20),
	(108,'20_2.jpg','Image 2','',0,20),
	(109,'20_3.jpg','Image 3','',0,20),
	(110,'20_4.jpg','Image 4','',0,20),
	(111,'20_5.jpg','Image 5','',0,20),
	(112,'20_6.jpg','Image 6','',0,20),
	(113,'21_1.jpg','Image 1','',1,21),
	(114,'21_2.jpg','Image 2','',0,21),
	(115,'21_3.jpg','Image 3','',0,21),
	(116,'21_4.jpg','Image 4','',0,21),
	(117,'22_1.jpg','Image 1','',1,22),
	(118,'22_2.jpg','Image 2','',0,22),
	(119,'22_3.jpg','Image 3','',0,22),
	(120,'22_4.jpg','Image 4','',0,22),
	(121,'23_1.jpg','Image 1','',1,23),
	(122,'23_2.jpg','Image 2','',0,23),
	(123,'23_3.jpg','Image 3','',0,23),
	(124,'23_4.jpg','Image 4','',0,23),
	(125,'23_5.jpg','Image 5','',0,23),
	(126,'24_1.jpg','Image 1','',1,24),
	(127,'24_2.jpg','Image 2','',0,24),
	(128,'24_3.jpg','Image 3','',0,24),
	(129,'24_4.jpg','Image 4','',0,24),
	(130,'24_5.jpg','Image 5','',0,24),
	(131,'24_6.jpg','Image 6','',0,24),
	(132,'25_1.jpg','Image 1','',1,25),
	(133,'25_2.jpg','Image 2','',0,25),
	(134,'25_3.jpg','Image 3','',0,25),
	(135,'26_1.jpg','Image 1','',1,26),
	(136,'26_2.jpg','Image 2','',0,26),
	(137,'26_3.jpg','Image 3','',0,26),
	(138,'26_4.jpg','Image 4','',0,26),
	(139,'26_5.jpg','Image 5','',0,26),
	(140,'26_6.jpg','Image 6','',0,26),
	(141,'27_1.jpg','Image 1','',1,27),
	(142,'27_2.jpg','Image 2','',0,27),
	(143,'27_3.jpg','Image 3','',0,27),
	(144,'27_4.jpg','Image 4','',0,27),
	(145,'27_5.jpg','Image 5','',0,27),
	(146,'27_6.jpg','Image 6','',0,27),
	(147,'28_1.jpg','Image 1','',1,28),
	(148,'28_2.jpg','Image 2','',0,28),
	(149,'28_3.jpg','Image 3','',0,28),
	(150,'28_4.jpg','Image 4','',0,28),
	(151,'29_1.jpeg','Image 1','',1,29),
	(152,'29_2.jpeg','Image 2','',0,29),
	(153,'29_3.jpeg','Image 3','',0,29),
	(154,'29_4.jpeg','Image 4','',0,29),
	(155,'30_1.jpeg','Image 1','',1,30),
	(156,'30_2.jpeg','Image 2','',0,30),
	(157,'30_3.jpeg','Image 3','',0,30),
	(158,'30_4.jpeg','Image 4','',0,30),
	(159,'30_5.jpeg','Image 5','',0,30),
	(160,'31_1.jpg','Image 1','',1,31),
	(161,'31_2.jpg','Image 2','',0,31),
	(162,'31_3.jpg','Image 3','',0,31),
	(163,'32_1.jpg','Image 1','',1,32),
	(164,'32_2.jpg','Image 2','',0,32),
	(165,'32_3.jpg','Image 3','',0,32),
	(166,'33_1.jpg','Image 1','',1,33),
	(167,'33_2.jpg','Image 2','',0,33),
	(168,'33_3.jpg','Image 3','',0,33),
	(169,'34_1.jpg','Image 1','',1,34),
	(170,'34_2.jpg','Image 2','',0,34),
	(171,'34_3.jpg','Image 3','',0,34),
	(172,'34_4.jpg','Image 4','',0,34),
	(173,'34_5.jpg','Image 5','',0,34),
	(174,'35_1.jpg','Image 1','',1,35),
	(175,'35_2.jpg','Image 2','',0,35),
	(176,'35_3.jpg','Image 3','',0,35),
	(177,'36_1.jpg','Image 1','',1,36),
	(178,'36_2.jpg','Image 2','',0,36),
	(179,'36_3.jpg','Image 3','',0,36),
	(180,'37_1.jpg','Image 1','',1,37),
	(181,'37_2.jpg','Image 2','',0,37),
	(182,'37_3.jpg','Image 3','',0,37),
	(183,'38_1.jpg','Image 1','',1,38),
	(184,'38_2.jpg','Image 2','',0,38),
	(185,'38_3.jpg','Image 3','',0,38),
	(186,'39_1.jpg','Image 1','',1,39),
	(187,'39_2.jpg','Image 2','',0,39),
	(188,'39_3.jpg','Image 3','',0,39),
	(189,'40_1.jpg','Image 1','',1,40),
	(190,'40_2.jpg','Image 2','',0,40),
	(191,'40_3.jpg','Image 3','',0,40),
	(192,'41_1.jpg','Image 1','',1,41),
	(193,'41_2.jpg','Image 2','',0,41),
	(194,'41_3.jpg','Image 3','',0,41),
	(195,'42_1.jpg','Image 1','',1,42),
	(196,'42_2.jpg','Image 2','',0,42),
	(197,'42_3.jpg','Image 3','',0,42),
	(198,'43_1.jpg','Image 1','',1,43),
	(199,'43_2.jpg','Image 2','',0,43),
	(200,'43_3.jpg','Image 3','',0,43),
	(201,'44_1.jpg','Image 1','',1,44),
	(202,'44_2.jpg','Image 2','',0,44),
	(203,'44_3.jpg','Image 3','',0,44),
	(204,'45_1.jpg','Image 1','',1,45),
	(205,'45_2.jpg','Image 2','',0,45),
	(206,'45_3.jpg','Image 3','',0,45),
	(207,'46_1.jpg','Image 1','',1,46),
	(208,'46_2.jpg','Image 2','',0,46),
	(209,'46_3.jpg','Image 3','',0,46),
	(210,'47_1.jpg','Image 1','',1,47),
	(211,'47_2.jpg','Image 2','',0,47),
	(212,'47_3.jpg','Image 3','',0,47),
	(213,'48_1.jpg','Image 1','',1,48),
	(214,'48_2.jpg','Image 2','',0,48),
	(215,'48_3.jpg','Image 3','',0,48),
	(216,'49_1.jpg','Image 1','',1,49),
	(217,'49_2.jpg','Image 2','',0,49),
	(218,'49_3.jpg','Image 3','',0,49),
	(219,'50_1.jpg','Image 1','',1,50),
	(220,'50_2.jpg','Image 2','',0,50),
	(221,'50_3.jpg','Image 3','',0,50),
	(222,'51_1.jpg','Image 1','',1,51),
	(223,'51_2.jpg','Image 2','',0,51),
	(224,'51_3.jpg','Image 3','',0,51),
	(225,'52_1.jpg','Image 1','',1,52),
	(226,'52_2.jpg','Image 2','',0,52),
	(227,'52_3.jpg','Image 3','',0,52),
	(228,'53_1.jpg','Image 1','',1,53),
	(229,'53_2.jpg','Image 2','',0,53),
	(230,'53_3.jpg','Image 3','',0,53),
	(231,'54_1.jpg','Image 1','',1,54),
	(232,'54_2.jpg','Image 2','',0,54),
	(233,'54_3.jpg','Image 3','',0,54),
	(234,'55_5.jpg','Image 1','',1,55),
	(235,'55_2.jpg','Image 2','',0,55),
	(236,'55_3.jpg','Image 3','',0,55),
	(237,'55_4.jpg','Image 4','',0,55),
	(238,'56_1.jpg','Image 1','',1,56),
	(239,'56_2.jpg','Image 2','',0,56),
	(240,'56_3.jpg','Image 3','',0,56),
	(241,'56_4.jpg','Image 4','',0,56),
	(242,'56_5.jpg','Image 5','',0,56),
	(243,'57_1.jpg','Image 1','',1,57),
	(244,'57_2.jpg','Image 2','',0,57),
	(245,'57_3.jpg','Image 3','',0,57),
	(246,'57_4.jpg','Image 4','',0,57),
	(247,'57_5.jpg','Image 5','',0,57),
	(248,'58_1.jpg','Image 1','',1,58),
	(249,'58_2.jpg','Image 2','',0,58),
	(250,'58_3.jpg','Image 3','',0,58),
	(251,'58_4.jpg','Image 4','',0,58),
	(252,'58_5.jpg','Image 5','',0,58),
	(253,'59_1.jpg','Image 1','',1,59),
	(254,'59_2.jpg','Image 2','',0,59),
	(255,'59_3.jpg','Image 3','',0,59),
	(256,'59_4.jpg','Image 4','',0,59),
	(257,'59_5.jpg','Image 5','',0,59),
	(258,'60_1.jpg','Image 1','',1,60),
	(259,'60_2.jpg','Image 2','',0,60),
	(260,'60_3.jpg','Image 3','',0,60),
	(261,'60_4.jpg','Image 4','',0,60),
	(262,'60_5.jpg','Image 5','',0,60),
	(263,'61_1.jpg','Image 1','',1,61),
	(264,'61_2.jpg','Image 2','',0,61),
	(265,'61_3.jpg','Image 3','',0,61),
	(266,'61_4.jpg','Image 4','',0,61),
	(267,'61_5.jpg','Image 5','',0,61),
	(268,'62_1.jpg','Image 1','',1,62),
	(269,'62_2.jpg','Image 2','',0,62),
	(270,'62_3.jpg','Image 3','',0,62),
	(271,'62_4.jpg','Image 4','',0,62),
	(272,'62_5.jpg','Image 5','',0,62),
	(273,'63_1.jpg','Image 1','',1,63),
	(274,'63_2.jpg','Image 2','',0,63),
	(275,'63_3.jpg','Image 3','',0,63),
	(276,'63_4.jpg','Image 4','',0,63),
	(277,'63_5.jpg','Image 5','',0,63),
	(278,'64_1.jpg','Image 1','',1,64),
	(279,'64_2.jpg','Image 2','',0,64),
	(280,'64_3.jpg','Image 3','',0,64),
	(281,'64_4.jpg','Image 4','',0,64),
	(282,'64_5.jpg','Image 5','',0,64),
	(283,'64_6.jpg','Image 6','',0,64),
	(284,'65_1.jpg','Image 1','',1,65),
	(285,'65_2.jpg','Image 2','',0,65),
	(286,'65_3.jpg','Image 3','',0,65),
	(287,'65_4.jpg','Image 4','',0,65),
	(288,'65_5.jpg','Image 5','',0,65),
	(289,'55_1.jpg','Image 1','',0,55),
	(290,'66_1.jpg','Image 1','',1,66),
	(291,'66_2.jpg','Image 2','',0,66),
	(292,'66_3.jpg','Image 3','',0,66),
	(293,'66_4.jpg','Image 4','',0,66),
	(294,'66_5.jpg','Image 5','',0,66),
	(295,'66_6.jpg','Image 6','',0,66),
	(296,'67_1.jpg','Image 1','',1,67),
	(297,'67_2.jpg','Image 2','',0,67),
	(298,'67_3.jpg','Image 3','',0,67),
	(299,'67_4.jpg','Image 4','',0,67),
	(300,'67_5.jpg','Image 5','',0,67),
	(301,'67_6.jpg','Image 6','',0,67),
	(302,'68_1.jpg','Image 1','',1,68),
	(303,'68_2.jpg','Image 2','',0,68),
	(304,'68_3.jpg','Image 3','',0,68),
	(305,'68_4.jpg','Image 4','',0,68),
	(306,'69_1.jpg','Image 1','',1,69),
	(307,'69_2.jpg','Image 2','',0,69),
	(308,'69_3.jpg','Image 3','',0,69),
	(309,'69_4.jpg','Image 4','',0,69),
	(310,'69_5.jpg','Image 5','',0,69),
	(311,'70_1.jpg','Image 1','',1,70),
	(312,'70_2.jpg','Image 2','',0,70),
	(313,'70_3.jpg','Image 3','',0,70),
	(314,'70_4.jpg','Image 4','',0,70),
	(315,'70_5.jpg','Image 5','',0,70),
	(316,'71_1.jpg','Image 1','',1,71),
	(317,'71_2.jpg','Image 2','',0,71),
	(318,'71_3.jpg','Image 3','',0,71),
	(319,'72_1.jpg','Image 1','',1,72),
	(320,'72_2.jpg','Image 2','',0,72),
	(321,'72_3.jpg','Image 3','',0,72),
	(322,'72_4.jpg','Image 4','',0,72),
	(323,'72_5.jpg','Image 5','',0,72),
	(324,'73_1.jpg','Image 1','',1,73),
	(325,'73_2.jpg','Image 2','',0,73),
	(326,'73_3.jpg','Image 3','',0,73),
	(327,'73_4.jpg','Image 4','',0,73),
	(328,'73_5.jpg','Image 5','',0,73),
	(329,'74_1.jpg','Image 1','',1,74),
	(330,'74_2.jpg','Image 2','',0,74),
	(331,'74_3.jpg','Image 3','',0,74),
	(332,'74_4.jpg','Image 4','',0,74),
	(333,'75_1.jpg','Image 1','',1,75),
	(334,'75_2.jpg','Image 2','',0,75),
	(335,'75_3.jpg','Image 3','',0,75),
	(336,'75_4.jpg','Image 4','',0,75),
	(337,'75_5.jpg','Image 5','',0,75),
	(338,'75_6.jpg','Image 6','',0,75),
	(339,'76_1.jpg','Image 1','',1,76),
	(340,'76_2.jpg','Image 2','',0,76),
	(341,'76_3.jpg','Image 3','',0,76),
	(342,'76_4.jpg','Image 4','',0,76),
	(343,'76_5.jpg','Image 5','',0,76),
	(344,'77_1.jpg','Image 1','',1,77),
	(345,'77_2.jpg','Image 2','',0,77),
	(346,'77_3.jpg','Image 3','',0,77),
	(347,'77_4.jpg','Image 4','',0,77),
	(348,'77_5.jpg','Image 5','',0,77),
	(349,'78_1.jpg','Image 1','',1,78),
	(350,'78_2.jpg','Image 2','',0,78),
	(351,'78_3.jpg','Image 3','',0,78),
	(352,'78_4.jpg','Image 4','',0,78),
	(353,'78_5.jpg','Image 5','',0,78),
	(354,'79_1.jpg','Image 1','',1,79),
	(355,'79_2.jpg','Image 2','',0,79),
	(356,'79_3.jpg','Image 3','',0,79),
	(357,'79_4.jpg','Image 4','',0,79),
	(358,'79_5.jpg','Image 5','',0,79),
	(359,'80_1.jpg','Image 1','',1,80),
	(360,'80_2.jpg','Image 2','',0,80),
	(361,'80_3.jpg','Image 3','',0,80),
	(362,'80_4.jpg','Image 4','',0,80),
	(363,'80_5.jpg','Image 5','',0,80),
	(364,'81_1.jpg','Image 1','',1,81),
	(365,'81_2.jpg','Image 2','',0,81),
	(366,'81_3.jpg','Image 3','',0,81),
	(367,'81_4.jpg','Image 4','',0,81),
	(369,'82_1.jpg','Image 1','',1,82),
	(370,'82_2.jpg','Image 2','',0,82),
	(371,'82_3.jpg','Image 3','',0,82),
	(372,'82_4.jpg','Image 4','',0,82),
	(373,'82_5.jpg','Image 5','',0,82),
	(374,'83_1.jpg','Image 1','',1,83),
	(375,'83_2.jpg','Image 2','',0,83),
	(376,'83_3.jpg','Image 3','',0,83),
	(377,'83_4.jpg','Image 4','',0,83),
	(378,'83_5.jpg','Image 5','',0,83),
	(379,'84_1.jpg','Image 1','',1,84),
	(380,'84_2.jpg','Image 2','',0,84),
	(381,'84_3.jpg','Image 3','',0,84),
	(382,'84_4.jpg','Image 4','',0,84),
	(383,'84_5.jpg','Image 5','',0,84),
	(384,'85_1.jpg','Image 1','',1,85),
	(385,'85_2.jpg','Image 2','',0,85),
	(386,'85_3.jpg','Image 3','',0,85),
	(387,'85_4.jpg','Image 4','',0,85),
	(388,'85_5.jpg','Image 5','',0,85),
	(389,'86_1.jpg','Image 1','',1,86),
	(390,'86_2.jpg','Image 2','',0,86),
	(391,'86_3.jpg','Image 3','',0,86),
	(392,'86_4.jpg','Image 4','',0,86),
	(393,'86_5.jpg','Image 5','',0,86),
	(394,'87_1.jpg','Image 1','',1,87),
	(395,'87_2.jpg','Image 2','',0,87),
	(396,'87_3.jpg','Image 3','',0,87),
	(397,'87_4.jpg','Image 4','',0,87),
	(398,'87_5.jpg','Image 5','',0,87),
	(399,'88_1.jpg','Image 1','',1,88),
	(400,'88_2.jpg','Image 2','',0,88),
	(401,'88_3.jpg','Image 3','',0,88),
	(402,'88_4.jpg','Image 4','',0,88),
	(403,'88_5.jpg','Image 5','',0,88);

/*!40000 ALTER TABLE `images` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table notificationobjectchanges
# ------------------------------------------------------------

DROP TABLE IF EXISTS `notificationobjectchanges`;

CREATE TABLE `notificationobjectchanges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `verb` varchar(45) DEFAULT NULL,
  `actor` varchar(45) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT NULL,
  `notificationobject_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_notificationobjectchanges_notificationobjects1_idx` (`notificationobject_id`),
  CONSTRAINT `fk_notificationobjectchanges_notificationobjects1` FOREIGN KEY (`notificationobject_id`) REFERENCES `notificationobjects` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table notificationobjects
# ------------------------------------------------------------

DROP TABLE IF EXISTS `notificationobjects`;

CREATE TABLE `notificationobjects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `object_id` int(11) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT NULL,
  `notification_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_notificationobjects_notifications1_idx` (`notification_id`),
  CONSTRAINT `fk_notificationobjects_notifications1` FOREIGN KEY (`notification_id`) REFERENCES `notifications` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table notifications
# ------------------------------------------------------------

DROP TABLE IF EXISTS `notifications`;

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(45) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_notifications_users1_idx` (`user_id`),
  CONSTRAINT `fk_notifications_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table onetimepasswords
# ------------------------------------------------------------

DROP TABLE IF EXISTS `onetimepasswords`;

CREATE TABLE `onetimepasswords` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phonenumber` varchar(256) NOT NULL DEFAULT '',
  `otp` int(4) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `onetimepasswords` WRITE;
/*!40000 ALTER TABLE `onetimepasswords` DISABLE KEYS */;

INSERT INTO `onetimepasswords` (`id`, `phonenumber`, `otp`, `user_id`)
VALUES
	(7,'9743000647',5477,7),
	(14,'9538128057',2846,14),
	(36,'9876543210',9358,36),
	(38,'4389935641',4402,38),
	(40,'9589919002',7271,40),
	(77,'7847958538',9486,77),
	(101,'8861186894',2353,100),
	(107,'8884300022',4798,106),
	(128,'8050042321',5347,126),
	(151,'7356129552',8542,148),
	(152,'8884715888',7118,149);

/*!40000 ALTER TABLE `onetimepasswords` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table orderdetails
# ------------------------------------------------------------

DROP TABLE IF EXISTS `orderdetails`;

CREATE TABLE `orderdetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` int(11) DEFAULT NULL,
  `ispaid` tinyint(1) DEFAULT '0',
  `orderdate` datetime DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `cart_id` int(11) NOT NULL DEFAULT '0',
  `isdelivered` tinyint(1) DEFAULT '0',
  `iscancelled` tinyint(1) DEFAULT '0',
  `deliverydate` datetime DEFAULT NULL,
  `canceldate` datetime DEFAULT NULL,
  `deliveryaddress` varchar(1000) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `fk_orderdetails_users1_idx` (`user_id`),
  CONSTRAINT `fk_orderdetails_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `orderdetails` WRITE;
/*!40000 ALTER TABLE `orderdetails` DISABLE KEYS */;

INSERT INTO `orderdetails` (`id`, `amount`, `ispaid`, `orderdate`, `user_id`, `cart_id`, `isdelivered`, `iscancelled`, `deliverydate`, `canceldate`, `deliveryaddress`)
VALUES
	(1,0,0,'2016-05-23 11:02:29',5,6,0,1,NULL,'2016-05-23 11:11:31','{\"id\":\"1\",\"user_id\":\"5\",\"address_line_one\":\"murugesapalaya\",\"address_line_two\":\"\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560017\",\"timestamp\":\"2016-05-23 05:32:27\",\"landmark\":\"top\",\"email\":\"duraigowardhan.s@gmail.com\",\"name\":\"durai gowardhan\",\"phonenumber\":\"9944951838\"}'),
	(2,5200,0,'2016-05-23 17:17:33',13,14,0,0,NULL,NULL,'{\"id\":\"2\",\"user_id\":\"13\",\"address_line_one\":\"No 09\\/108,channakeshava nilaya, Opp to shell petrol bunk, Seegehalli , kadugodi post, Bangalore 560067\",\"address_line_two\":\"1st floor,Flat no. 108\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560067\",\"timestamp\":\"2016-05-23 11:47:20\",\"landmark\":\"Opp to shell petrol bunk\",\"email\":\"ankuraman.mishra@gmail.com\",\"name\":\"ankur mishra\",\"phonenumber\":\"7795847342\"}'),
	(3,6500,0,'2016-05-23 18:25:33',18,22,0,0,NULL,NULL,'{\"id\":\"3\",\"user_id\":\"18\",\"address_line_one\":\"N V RESIDENCY,FLAT NO-5\",\"address_line_two\":\"DODDATHOGUR ROAD, ELECTRONIC CITY PHASE-1\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560100\",\"timestamp\":\"2016-05-23 12:55:10\",\"landmark\":\"NEAR LAXMI NARSHIMA TEMPLE- AUTO STAND\",\"email\":\"ruchi21091@gmail.com\",\"name\":\"ruchismita chakraborty\",\"phonenumber\":\"9886382769\"}'),
	(5,9500,0,'2016-05-23 18:50:30',5,27,0,0,NULL,NULL,'{\"id\":\"1\",\"user_id\":\"5\",\"address_line_one\":\"murugesapalaya\",\"address_line_two\":\"\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560017\",\"timestamp\":\"2016-05-23 05:32:27\",\"landmark\":\"top\",\"email\":\"duraigowardhan.s@gmail.com\",\"name\":\"durai gowardhan\",\"phonenumber\":\"9944951838\"}'),
	(6,7500,0,'2016-05-23 18:58:53',5,29,0,0,NULL,NULL,'{\"id\":\"1\",\"user_id\":\"5\",\"address_line_one\":\"murugesapalaya\",\"address_line_two\":\"\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560017\",\"timestamp\":\"2016-05-23 05:32:27\",\"landmark\":\"top\",\"email\":\"duraigowardhan.s@gmail.com\",\"name\":\"durai gowardhan\",\"phonenumber\":\"9944951838\"}'),
	(7,0,0,'2016-05-23 20:15:24',26,35,0,1,NULL,'2016-05-25 16:06:43','{\"id\":\"4\",\"user_id\":\"26\",\"address_line_one\":\"Apartment No. B-502, BREN AVALON, Doddannekundi Extension,\",\"address_line_two\":\"Chinnapanahalli Main Road,\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560048\",\"timestamp\":\"2016-05-23 14:45:08\",\"landmark\":\"Near bigbasket warehouse\",\"email\":\"sujith10.cea@gmail.com\",\"name\":\"sujith kumar\",\"phonenumber\":\"7406926385\"}'),
	(8,5200,0,'2016-05-24 10:58:41',27,45,0,0,NULL,NULL,'{\"id\":\"5\",\"user_id\":\"27\",\"address_line_one\":\"Brigade Metropolis, Garudacharpalya\",\"address_line_two\":\"Mahadevapura, Whitefield road\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560048\",\"timestamp\":\"2016-05-24 05:28:26\",\"landmark\":\"Phoenix Market City\",\"email\":\"reechabansal@yahoo.com\",\"name\":\"Reecha Bansal\",\"phonenumber\":\"9742600477\"}'),
	(9,0,0,'2016-05-24 11:19:37',42,46,0,1,NULL,'2016-05-25 15:59:29','{\"id\":\"6\",\"user_id\":\"42\",\"address_line_one\":\"E1404, Smondo 3\",\"address_line_two\":\"Neotown, Electronic City Phase 1\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560102\",\"timestamp\":\"2016-05-24 05:46:17\",\"landmark\":\"Starlit Suites\",\"email\":\"haritgkumar@gmail.com\",\"name\":\"Harit Gulati\",\"phonenumber\":\"8290539343\"}'),
	(10,0,0,'2016-05-24 11:20:46',42,47,0,1,NULL,'2016-05-25 15:59:19','{\"id\":\"6\",\"user_id\":\"42\",\"address_line_one\":\"E1404, Smondo 3\",\"address_line_two\":\"Neotown, Electronic City Phase 1\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560102\",\"timestamp\":\"2016-05-24 05:46:17\",\"landmark\":\"Starlit Suites\",\"email\":\"haritgkumar@gmail.com\",\"name\":\"Harit Gulati\",\"phonenumber\":\"8290539343\"}'),
	(11,0,0,'2016-05-24 14:13:35',15,52,0,1,NULL,'2016-05-24 14:43:46','{\"id\":\"7\",\"user_id\":\"15\",\"address_line_one\":\"#5, (Second Floor, First Portion), 1st \'B\' Main Road, Chandra Reddy Layout, Behind Maharaja Hotel, 4th Block, Koramangala, Bangalore - 560034\",\"address_line_two\":\"\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560034\",\"timestamp\":\"2016-05-24 08:43:26\",\"landmark\":\"Near Maharaja Signal\",\"email\":\"aradhanasngh5@gmail.com\",\"name\":\"Aradhana Singh\",\"phonenumber\":\"8981542972\"}'),
	(12,6200,0,'2016-05-24 14:58:54',15,54,0,0,NULL,NULL,'{\"id\":\"7\",\"user_id\":\"15\",\"address_line_one\":\"#5, (Second Floor, First Portion), 1st \'B\' Main Road, Chandra Reddy Layout, Behind Maharaja Hotel, 4th Block, Koramangala, Bangalore - 560034\",\"address_line_two\":\"\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560034\",\"timestamp\":\"2016-05-24 08:43:26\",\"landmark\":\"Near Maharaja Signal\",\"email\":\"aradhanasngh5@gmail.com\",\"name\":\"Aradhana Singh\",\"phonenumber\":\"8981542972\"}'),
	(14,4000,0,'2016-05-24 15:50:45',48,58,0,0,NULL,NULL,'{\"id\":\"8\",\"user_id\":\"48\",\"address_line_one\":\"FT- 151, F2 Block, Ittina Neela Appartments,\",\"address_line_two\":\"Andapura Village, Electronic City\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560100\",\"timestamp\":\"2016-05-24 10:12:52\",\"landmark\":\"Near Gold Coin Club, Glass Factory Layout\",\"email\":\"vips.wipro@gmail.com\",\"name\":\"Vipin Mohan\",\"phonenumber\":\"8971777887\"}'),
	(15,6000,0,'2016-05-24 16:09:54',5,61,0,0,NULL,NULL,'{\"id\":\"1\",\"user_id\":\"5\",\"address_line_one\":\"murugesapalaya\",\"address_line_two\":\"\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560017\",\"timestamp\":\"2016-05-23 05:32:27\",\"landmark\":\"top\",\"email\":\"duraigowardhan.s@gmail.com\",\"name\":\"durai gowardhan\",\"phonenumber\":\"9944951838\"}'),
	(16,5200,0,'2016-05-24 17:57:13',54,65,0,0,NULL,NULL,'{\"id\":\"9\",\"user_id\":\"54\",\"address_line_one\":\"narayan appartment, Dodda tagore\",\"address_line_two\":\"electronic city phase 1 , konop agarahara,\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560100\",\"timestamp\":\"2016-05-24 12:27:11\",\"landmark\":\"near mm school\",\"email\":\"avikm01@gmail.com\",\"name\":\"Avik mukherjee\",\"phonenumber\":\"9836033457\"}'),
	(17,6500,0,'2016-05-24 18:02:40',54,66,0,0,NULL,NULL,'{\"id\":\"9\",\"user_id\":\"54\",\"address_line_one\":\"narayan appartment, Dodda tagore\",\"address_line_two\":\"electronic city phase 1 , konop agarahara,\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560100\",\"timestamp\":\"2016-05-24 12:27:11\",\"landmark\":\"near mm school\",\"email\":\"avikm01@gmail.com\",\"name\":\"Avik mukherjee\",\"phonenumber\":\"9836033457\"}'),
	(18,5200,0,'2016-05-24 18:38:32',55,69,0,0,NULL,NULL,'{\"id\":\"10\",\"user_id\":\"55\",\"address_line_one\":\"Flat No. 38, Room No. 9 4th Floor, 20th Cross Ejipura Main Road\",\"address_line_two\":\"KMR Building\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560047\",\"timestamp\":\"2016-05-24 13:07:35\",\"landmark\":\"Near Rama Temple\",\"email\":\"subratasrk1940@gmail.com\",\"name\":\"Subrata Sarkar\",\"phonenumber\":\"7411063991\"}'),
	(19,0,0,'2016-05-25 17:11:13',21,74,0,1,NULL,'2016-05-25 17:11:53','{\"id\":\"11\",\"user_id\":\"21\",\"address_line_one\":\"#426, 1st CROS , 4th MAIN\",\"address_line_two\":\"Vijaya Layout\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560076\",\"timestamp\":\"2016-05-25 11:41:10\",\"landmark\":\"Opposite Diana Memorial High School\",\"email\":\"26anoop1990@gmail.com\",\"name\":\"Anoop K\",\"phonenumber\":\"9686482400\"}'),
	(20,5500,0,'2016-05-26 11:33:14',62,77,0,0,NULL,NULL,'{\"id\":\"12\",\"user_id\":\"62\",\"address_line_one\":\"Shri laxmi nivas , #47 , 1st mn , 2nd crs, apparao layout, rammurthy nagar -\",\"address_line_two\":\"\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560016\",\"timestamp\":\"2016-05-26 06:03:10\",\"landmark\":\"more supermarket near fresh heritage opposite lane\",\"email\":\"peravelli8@gmail.com\",\"name\":\"purna eravelli\",\"phonenumber\":\"7776933090\"}'),
	(21,8000,0,'2016-05-26 18:23:06',63,79,0,0,NULL,NULL,'{\"id\":\"13\",\"user_id\":\"63\",\"address_line_one\":\"Flat s-2, Block-1, Wild grass Apts ,\",\"address_line_two\":\"Nirguna Mandir Layout , S.T.Bed , Koramangala\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560047\",\"timestamp\":\"2016-05-26 12:53:03\",\"landmark\":\"Koramangala near gem regency\",\"email\":\"kritishmani@gmail.com\",\"name\":\"Manikandan Balasubramanian\",\"phonenumber\":\"9513407060\"}'),
	(22,5300,0,'2016-05-26 23:19:12',3,3,0,0,NULL,NULL,'{\"id\":\"14\",\"user_id\":\"3\",\"address_line_one\":\"VS Chalet\",\"address_line_two\":\"LBS Nagar\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560017\",\"timestamp\":\"2016-05-26 17:49:09\",\"landmark\":\"Near Nilgris\",\"email\":\"vineethkumart@gmail.com\",\"name\":\"Vineeth Kumar\",\"phonenumber\":\"9538092344\"}'),
	(23,0,0,'2016-05-27 17:58:08',26,88,0,1,NULL,'2016-05-28 11:54:01','{\"id\":\"4\",\"user_id\":\"26\",\"address_line_one\":\"Apartment No. B-502, BREN AVALON, Doddannekundi Extension,\",\"address_line_two\":\"Chinnapanahalli Main Road,\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560048\",\"timestamp\":\"2016-05-23 14:45:08\",\"landmark\":\"Near bigbasket warehouse\",\"email\":\"sujith10.cea@gmail.com\",\"name\":\"sujith kumar\",\"phonenumber\":\"7406926385\"}'),
	(24,0,0,'2016-05-30 15:46:54',76,95,0,1,NULL,'2016-05-30 15:47:12','{\"id\":\"15\",\"user_id\":\"76\",\"address_line_one\":\"Flat No: GF2, Maruthi Residency, Opposite Sobha Marvella,\",\"address_line_two\":\"Green Glen layout, Bellandur\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560103\",\"timestamp\":\"2016-05-30 10:16:45\",\"landmark\":\"Karnataka\",\"email\":\"ashishgpt24@gmail.com\",\"name\":\"Ashish Gupta\",\"phonenumber\":\"8884261148\"}'),
	(25,6500,0,'2016-05-30 17:10:03',78,97,0,0,NULL,NULL,'{\"id\":\"16\",\"user_id\":\"78\",\"address_line_one\":\"No 15\\/1 Ground floor, Shesha Shyla Apts\",\"address_line_two\":\"Vinayakanagar, Syndicate Bank Road (4th Main, 3rd Cross)\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560017\",\"timestamp\":\"2016-05-30 11:39:52\",\"landmark\":\"Left turn after SNS Arcade, at Syndicate Bank\",\"email\":\"anusha@smoodies.in\",\"name\":\"Anusha Bhushan\",\"phonenumber\":\"9920588039\"}'),
	(26,7000,0,'2016-05-31 11:54:22',81,100,0,0,NULL,NULL,'{\"id\":\"17\",\"user_id\":\"81\",\"address_line_one\":\"First floor, Arunachalam, No. 49,\",\"address_line_two\":\"St. Anthony Nicolas Street, Ashok Nagar\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560088\",\"timestamp\":\"2016-05-31 06:24:09\",\"landmark\":\"Ozone Hotel, Markham Road\",\"email\":\"cinthya.anand@gmail.com\",\"name\":\"Cinthya Anand\",\"phonenumber\":\"8971062056\"}'),
	(27,6600,0,'2016-05-31 15:02:26',84,103,0,0,NULL,NULL,'{\"id\":\"18\",\"user_id\":\"84\",\"address_line_one\":\"108\\/1, B Cross, Block-2 B2\",\"address_line_two\":\"BSR Splendour Park Apartment, Vijaya Bank Colony Extension, Horamavu, Banaswadi, Bangalore-500043.\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"500043\",\"timestamp\":\"2016-05-31 09:32:21\",\"landmark\":\"Vijaya Bank Colony Extension\",\"email\":\"ganesh.sangale@gmail.com\",\"name\":\"Ganesh Sangale\",\"phonenumber\":\"9702162999\"}'),
	(28,0,0,'2016-05-31 18:09:39',63,107,0,1,NULL,'2016-05-31 21:12:46','{\"id\":\"13\",\"user_id\":\"63\",\"address_line_one\":\"Flat s-2, Block-1, Wild grass Apts ,\",\"address_line_two\":\"Nirguna Mandir Layout , S.T.Bed , Koramangala\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560047\",\"timestamp\":\"2016-05-26 12:53:03\",\"landmark\":\"Koramangala near gem regency\",\"email\":\"kritishmani@gmail.com\",\"name\":\"Manikandan Balasubramanian\",\"phonenumber\":\"9513407060\"}'),
	(29,6800,0,'2016-06-01 11:51:24',87,108,0,0,NULL,NULL,'{\"id\":\"19\",\"user_id\":\"87\",\"address_line_one\":\"# 35, 42nd Main, KAS Officers Colony,\",\"address_line_two\":\"BTM 2nd Stage, Bangalore\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560068\",\"timestamp\":\"2016-06-01 06:20:52\",\"landmark\":\"Besides Brigade Lakeview Apartment\",\"email\":\"suraagini9@gmail.com\",\"name\":\"Papuli Banerjee\",\"phonenumber\":\"9901199110\"}'),
	(30,8500,0,'2016-06-01 11:52:00',87,111,0,0,NULL,NULL,'{\"id\":\"19\",\"user_id\":\"87\",\"address_line_one\":\"# 35, 42nd Main, KAS Officers Colony,\",\"address_line_two\":\"BTM 2nd Stage, Bangalore\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560068\",\"timestamp\":\"2016-06-01 06:20:52\",\"landmark\":\"Besides Brigade Lakeview Apartment\",\"email\":\"suraagini9@gmail.com\",\"name\":\"Papuli Banerjee\",\"phonenumber\":\"9901199110\"}'),
	(31,0,0,'2016-06-02 14:11:32',21,21,0,1,NULL,'2016-06-02 15:08:14','{\"id\":\"11\",\"user_id\":\"21\",\"address_line_one\":\"#426, 1st CROS , 4th MAIN\",\"address_line_two\":\"Vijaya Layout\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560076\",\"timestamp\":\"2016-05-25 11:41:10\",\"landmark\":\"Opposite Diana Memorial High School\",\"email\":\"26anoop1990@gmail.com\",\"name\":\"Anoop K\",\"phonenumber\":\"9686482400\"}'),
	(32,6900,0,'2016-06-03 18:57:34',63,78,0,0,NULL,NULL,'{\"id\":\"13\",\"user_id\":\"63\",\"address_line_one\":\"Flat s-2, Block-1, Wild grass Apts ,\",\"address_line_two\":\"Nirguna Mandir Layout , S.T.Bed , Koramangala\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560047\",\"timestamp\":\"2016-05-26 12:53:03\",\"landmark\":\"Koramangala near gem regency\",\"email\":\"kritishmani@gmail.com\",\"name\":\"Manikandan Balasubramanian\",\"phonenumber\":\"9513407060\"}'),
	(33,5800,0,'2016-06-03 19:09:05',110,132,0,0,NULL,NULL,'{\"id\":\"21\",\"user_id\":\"110\",\"address_line_one\":\"I-002, Bren palms\",\"address_line_two\":\"kudlu main road,kudlu gate, bangalore\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560068\",\"timestamp\":\"2016-06-03 13:39:01\",\"landmark\":\"opposite to sri bhagya hotel\",\"email\":\"racheetar@gmail.com\",\"name\":\"racheeta ramanujam\",\"phonenumber\":\"9900813275\"}'),
	(34,5200,0,'2016-06-04 11:40:04',112,135,0,0,NULL,NULL,'{\"id\":\"23\",\"user_id\":\"112\",\"address_line_one\":\"House No. 123, 2nd Cross, 5th Main, BTM 2nd Stage\",\"address_line_two\":\"MICO Layout\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560076\",\"timestamp\":\"2016-06-04 06:09:55\",\"landmark\":\"Near BTM Water Tank and Indian Oil Petrol Bunk\",\"email\":\"charusaxena6495s@gmail.com\",\"name\":\"charu saxena\",\"phonenumber\":\"8880860600\"}'),
	(35,0,0,'2016-06-04 11:42:17',98,136,0,1,NULL,'2016-06-04 11:59:34','{\"id\":\"22\",\"user_id\":\"98\",\"address_line_one\":\"Motappanapalya\",\"address_line_two\":\"Indiranager\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560038\",\"timestamp\":\"2016-06-03 14:00:26\",\"landmark\":\"S S B school\",\"email\":\"cellplanet484@gmail.com\",\"name\":\"Latheef Perla\",\"phonenumber\":\"9886385606\"}'),
	(36,0,0,'2016-06-04 12:19:05',98,137,0,1,NULL,'2016-06-05 13:17:25','{\"id\":\"22\",\"user_id\":\"98\",\"address_line_one\":\"Motappanapalya\",\"address_line_two\":\"Indiranager\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560038\",\"timestamp\":\"2016-06-03 14:00:26\",\"landmark\":\"S S B school\",\"email\":\"cellplanet484@gmail.com\",\"name\":\"Latheef Perla\",\"phonenumber\":\"9886385606\"}'),
	(37,6700,0,'2016-06-04 13:40:25',113,138,0,0,NULL,NULL,'{\"id\":\"24\",\"user_id\":\"113\",\"address_line_one\":\"F1, Meenakshi Residency\",\"address_line_two\":\"Ashwini layout, 1st main, 4th cross, Ejipura\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560093\",\"timestamp\":\"2016-06-04 08:09:29\",\"landmark\":\"behind MTR office\",\"email\":\"praneta.agrawal@gmail.com\",\"name\":\"Praneta Agrawal\",\"phonenumber\":\"9686528436\"}'),
	(38,6600,0,'2016-06-04 15:13:46',115,141,0,0,NULL,NULL,'{\"id\":\"25\",\"user_id\":\"115\",\"address_line_one\":\"#39,1st cross,new kaverappa layout\",\"address_line_two\":\"Panathurpost,varthurhobli,Kadubishanahalli\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560103\",\"timestamp\":\"2016-06-04 09:42:38\",\"landmark\":\"5star chicken\",\"email\":\"rrajesh222@gmail.com\",\"name\":\"Rajesh Daadi\",\"phonenumber\":\"7795649939\"}'),
	(39,6200,0,'2016-06-04 17:48:20',116,143,0,0,NULL,NULL,'{\"id\":\"26\",\"user_id\":\"116\",\"address_line_one\":\"d 202 golden nest apartments jcr layout 2nd cross\",\"address_line_two\":\"panatur kadubeesinahalli\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560103\",\"timestamp\":\"2016-06-04 12:15:39\",\"landmark\":\"jcr layout\",\"email\":\"anishmac99@gmail.com\",\"name\":\"anish chandra\",\"phonenumber\":\"9731150033\"}'),
	(40,6100,0,'2016-06-04 19:24:12',109,144,0,0,NULL,NULL,'{\"id\":\"27\",\"user_id\":\"109\",\"address_line_one\":\"No3, 2nd Floor , Corner House\",\"address_line_two\":\"29th A Cross , Balaji Layout , Kaggadaspura\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560093\",\"timestamp\":\"2016-06-04 13:53:05\",\"landmark\":\"1st left , 3rd building\",\"email\":\"trailokya4u@gmail.com\",\"name\":\"Trailokya B\",\"phonenumber\":\"7086001777\"}'),
	(41,8000,0,'2016-06-04 23:08:57',119,148,0,0,NULL,NULL,'{\"id\":\"32\",\"user_id\":\"119\",\"address_line_one\":\"Flat T02, Building 1647\",\"address_line_two\":\"18th cross, 26th main, HSR Layour, Sector 2\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560102\",\"timestamp\":\"2016-06-04 17:38:54\",\"landmark\":\"Near NIFT College\",\"email\":\"chabigupta2009@gmail.com\",\"name\":\"Chabi Gupta\",\"phonenumber\":\"8971261714\"}'),
	(42,8300,0,'2016-06-05 08:57:22',88,149,0,0,NULL,NULL,'{\"id\":\"33\",\"user_id\":\"88\",\"address_line_one\":\"C-412, Maple Block, Raheja Residency\",\"address_line_two\":\"Koramangala 3rd Block, Bengaluru, Karnataka 560034\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560034\",\"timestamp\":\"2016-06-05 03:27:03\",\"landmark\":\"Raheja Residency\",\"email\":\"bayyangar@gmail.com\",\"name\":\"Balaji Ayyangar\",\"phonenumber\":\"7358316785\"}'),
	(43,12300,0,'2016-06-05 12:40:17',121,152,0,0,NULL,NULL,'{\"id\":\"34\",\"user_id\":\"121\",\"address_line_one\":\"# 252 11TH MAIN 13TH CROSS WILLSEN GARDEN BANGLORE 27\",\"address_line_two\":\"8951056226\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560027\",\"timestamp\":\"2016-06-05 07:10:10\",\"landmark\":\"BRAND FACTORY\",\"email\":\"yogesh.0607@yahoo.in\",\"name\":\"YOGESH YOGI\",\"phonenumber\":\"9986122456\"}'),
	(44,0,0,'2016-06-05 15:19:50',122,156,0,1,NULL,'2016-06-06 21:56:47','{\"id\":\"35\",\"user_id\":\"122\",\"address_line_one\":\"Second floor\",\"address_line_two\":\"No.77\\/38 , 1st Main, 11th cross, Bharathi layout, opp.Assissi Bhavan, S.G Palya\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560029\",\"timestamp\":\"2016-06-05 09:49:31\",\"landmark\":\"Assissi Bhavan\",\"email\":\"barshababuraj@gmail.com\",\"name\":\"Barsha Baburaj\",\"phonenumber\":\"9846263515\"}'),
	(46,6500,0,'2016-06-05 15:29:36',125,159,0,0,NULL,NULL,'{\"id\":\"37\",\"user_id\":\"125\",\"address_line_one\":\"Dr. No. S\\/203, House no. 12\\/13\",\"address_line_two\":\"Maruthi Nilayam, 2nd Cross, Lakshamma Layout, Dodda Banaswadi\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560043\",\"timestamp\":\"2016-06-05 09:58:42\",\"landmark\":\"Kundan Electronics, & Mukunthamma Temple\",\"email\":\"quietnd@gmail.com\",\"name\":\"Nandini Dutta\",\"phonenumber\":\"9035168837\"}'),
	(47,6950,0,'2016-06-05 16:49:58',116,160,0,0,NULL,NULL,'{\"id\":\"26\",\"user_id\":\"116\",\"address_line_one\":\"d 202 golden nest apartments jcr layout 2nd cross\",\"address_line_two\":\"panatur kadubeesinahalli\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560103\",\"timestamp\":\"2016-06-04 12:15:39\",\"landmark\":\"jcr layout\",\"email\":\"anishmac99@gmail.com\",\"name\":\"anish chandra\",\"phonenumber\":\"9731150033\"}'),
	(48,5600,0,'2016-06-05 21:22:52',117,145,0,0,NULL,NULL,'{\"id\":\"36\",\"user_id\":\"117\",\"address_line_one\":\"P1(4th Floor), Fatima Residency, 36, 1st Main Road\",\"address_line_two\":\"ST Bed layout, Cauvery Colony, Koramangala 4th block\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560034\",\"timestamp\":\"2016-06-05 09:51:15\",\"landmark\":\"Opp Juice junction\",\"email\":\"16.shiladitya@gmail.com\",\"name\":\"Shiladitya Mandal\",\"phonenumber\":\"8197065678\"}'),
	(49,0,0,'2016-06-06 00:05:20',3,84,0,1,NULL,'2016-06-06 05:53:21','{\"id\":\"14\",\"user_id\":\"3\",\"address_line_one\":\"VS Chalet\",\"address_line_two\":\"LBS Nagar\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560017\",\"timestamp\":\"2016-05-26 17:49:09\",\"landmark\":\"Near Nilgris\",\"email\":\"vineethkumart@gmail.com\",\"name\":\"Vineeth Kumar\",\"phonenumber\":\"9538092344\"}'),
	(50,0,0,'2016-06-06 00:47:28',3,162,0,1,NULL,'2016-06-06 05:53:28','{\"id\":\"14\",\"user_id\":\"3\",\"address_line_one\":\"VS Chalet\",\"address_line_two\":\"LBS Nagar\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560017\",\"timestamp\":\"2016-05-26 17:49:09\",\"landmark\":\"Near Nilgris\",\"email\":\"vineethkumart@gmail.com\",\"name\":\"Vineeth Kumar\",\"phonenumber\":\"9538092344\"}'),
	(51,0,0,'2016-06-06 00:52:04',3,163,0,1,NULL,'2016-06-06 05:53:31','{\"id\":\"14\",\"user_id\":\"3\",\"address_line_one\":\"VS Chalet\",\"address_line_two\":\"LBS Nagar\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560017\",\"timestamp\":\"2016-05-26 17:49:09\",\"landmark\":\"Near Nilgris\",\"email\":\"vineethkumart@gmail.com\",\"name\":\"Vineeth Kumar\",\"phonenumber\":\"9538092344\"}'),
	(52,0,0,'2016-06-06 01:04:41',3,164,0,1,NULL,'2016-06-06 05:53:37','{\"id\":\"14\",\"user_id\":\"3\",\"address_line_one\":\"VS Chalet\",\"address_line_two\":\"LBS Nagar\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560017\",\"timestamp\":\"2016-05-26 17:49:09\",\"landmark\":\"Near Nilgris\",\"email\":\"vineethkumart@gmail.com\",\"name\":\"Vineeth Kumar\",\"phonenumber\":\"9538092344\"}'),
	(53,8200,0,'2016-06-06 11:31:43',98,165,0,0,NULL,NULL,'{\"id\":\"22\",\"user_id\":\"98\",\"address_line_one\":\"Motappanapalya\",\"address_line_two\":\"Indiranager\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560038\",\"timestamp\":\"2016-06-03 14:00:26\",\"landmark\":\"S S B school\",\"email\":\"cellplanet484@gmail.com\",\"name\":\"Latheef Perla\",\"phonenumber\":\"9886385606\"}'),
	(54,5200,0,'2016-06-06 11:58:51',117,166,0,0,NULL,NULL,'{\"id\":\"36\",\"user_id\":\"117\",\"address_line_one\":\"P1(4th Floor), Fatima Residency, 36, 1st Main Road\",\"address_line_two\":\"ST Bed layout, Cauvery Colony, Koramangala 4th block\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560034\",\"timestamp\":\"2016-06-05 09:51:15\",\"landmark\":\"Opp Juice junction\",\"email\":\"16.shiladitya@gmail.com\",\"name\":\"Shiladitya Mandal\",\"phonenumber\":\"8197065678\"}'),
	(55,5900,0,'2016-06-06 17:01:55',127,167,0,0,NULL,NULL,'{\"id\":\"38\",\"user_id\":\"127\",\"address_line_one\":\"No.20\\/1,Kalahalli,Opp to Ulsoor Lake,Next to MEG Main Gate,SC Garden Post,Bangalore-560042\",\"address_line_two\":\"C1\\/5,BDA Flat,2nd floor,Kalahalli Main Road,SC Garden post,Bangalore\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560042\",\"timestamp\":\"2016-06-06 11:31:41\",\"landmark\":\"Opp to Ulsoor Lake,Kalahalli Main Road,dead end (Grocery shop) turn right u will get Anjineya temple further turn right\",\"email\":\"anilmn@rediffmail.com\",\"name\":\"Anil Menon\",\"phonenumber\":\"7406537405\"}'),
	(56,6300,0,'2016-06-07 07:48:36',129,170,0,0,NULL,NULL,'{\"id\":\"39\",\"user_id\":\"129\",\"address_line_one\":\"#409 th , a main 2nd block hrbr layout kalyan nagar Bangalore\",\"address_line_two\":\"\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560043\",\"timestamp\":\"2016-06-07 02:18:34\",\"landmark\":\"Behind banasawadi club\",\"email\":\"nikhilriphone@gmail.com\",\"name\":\"Nikhil Ravishankar\",\"phonenumber\":\"9901343482\"}'),
	(57,5500,0,'2016-06-07 11:34:47',5,172,0,0,NULL,NULL,'{\"id\":\"40\",\"user_id\":\"5\",\"address_line_one\":\"rs\",\"address_line_two\":\"\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560017\",\"timestamp\":\"2016-06-07 06:04:44\",\"landmark\":\"prabhu\",\"email\":\"duraigowardhan.s@gmail.com\",\"name\":\"durai gowardhan\",\"phonenumber\":\"9944951838\"}'),
	(58,8900,0,'2016-06-07 13:48:29',107,128,0,0,NULL,NULL,'{\"id\":\"41\",\"user_id\":\"107\",\"address_line_one\":\"f2 emrald enclave, keraguddahalli, chik wanwara road, Abhigere,jalhali west Bangalore\",\"address_line_two\":\"f-2 emrald enclave, keraguddahalli, chik wanwara road, Abhigere,jalhali west Bangalore\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560092\",\"timestamp\":\"2016-06-07 08:18:22\",\"landmark\":\"oposite to DS Max\",\"email\":\"akt656715@gmail.com\",\"name\":\"ashok tiwari\",\"phonenumber\":\"8971914984\"}'),
	(59,8500,0,'2016-06-07 15:44:15',134,177,0,0,NULL,NULL,'{\"id\":\"43\",\"user_id\":\"134\",\"address_line_one\":\"Garma Garam, Arekere, MICO Layout, Bannerghatta Road\",\"address_line_two\":\"\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560076\",\"timestamp\":\"2016-06-07 10:13:57\",\"landmark\":\"Infornt of Canara Bank\",\"email\":\"jeetcx@gmail.com\",\"name\":\"Abhijeet \",\"phonenumber\":\"8880157153\"}'),
	(60,6200,0,'2016-06-07 15:47:46',133,178,0,0,NULL,NULL,'{\"id\":\"42\",\"user_id\":\"133\",\"address_line_one\":\"Sarjapur Road, beside wipro\",\"address_line_two\":\"Y5-301 Greenwood regency 3rd floor\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560035\",\"timestamp\":\"2016-06-07 10:12:24\",\"landmark\":\"wipro\",\"email\":\"ali6988f@gmail.com\",\"name\":\"Aliya N\",\"phonenumber\":\"9739115275\"}'),
	(61,7200,0,'2016-06-08 19:30:33',137,181,0,0,NULL,NULL,'{\"id\":\"44\",\"user_id\":\"137\",\"address_line_one\":\"#105 Shiva Sai Kalpa, Tech City Layout\",\"address_line_two\":\"Doddathoguru Village, Electronic city phase1\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560147\",\"timestamp\":\"2016-06-08 14:00:27\",\"landmark\":\"Durga Bakery\\/Thoguru Cross\",\"email\":\"hillol.mazumder@outlook.com\",\"name\":\"hillol mazumder\",\"phonenumber\":\"9632608833\"}'),
	(62,15500,0,'2016-06-09 19:02:47',88,188,0,0,NULL,NULL,'{\"id\":\"33\",\"user_id\":\"88\",\"address_line_one\":\"C-412, Maple Block, Raheja Residency\",\"address_line_two\":\"Koramangala 3rd Block, Bengaluru, Karnataka 560034\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560034\",\"timestamp\":\"2016-06-05 03:27:03\",\"landmark\":\"Raheja Residency\",\"email\":\"bayyangar@gmail.com\",\"name\":\"Balaji Ayyangar\",\"phonenumber\":\"7358316785\"}'),
	(63,6300,0,'2016-06-10 06:49:29',5,191,0,0,NULL,NULL,'{\"id\":\"40\",\"user_id\":\"5\",\"address_line_one\":\"rs\",\"address_line_two\":\"\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560017\",\"timestamp\":\"2016-06-07 06:04:44\",\"landmark\":\"prabhu\",\"email\":\"duraigowardhan.s@gmail.com\",\"name\":\"durai gowardhan\",\"phonenumber\":\"9944951838\"}'),
	(64,8300,0,'2016-06-12 14:44:53',3,194,0,0,NULL,NULL,'{\"id\":\"14\",\"user_id\":\"3\",\"address_line_one\":\"VS Chalet\",\"address_line_two\":\"LBS Nagar\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560017\",\"timestamp\":\"2016-05-26 17:49:09\",\"landmark\":\"Near Nilgris\",\"email\":\"vineethkumart@gmail.com\",\"name\":\"Vineeth Kumar\",\"phonenumber\":\"9538092344\"}'),
	(65,12000,0,'2016-06-13 18:47:49',88,196,0,0,NULL,NULL,'{\"id\":\"33\",\"user_id\":\"88\",\"address_line_one\":\"C-412, Maple Block, Raheja Residency\",\"address_line_two\":\"Koramangala 3rd Block, Bengaluru, Karnataka 560034\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560034\",\"timestamp\":\"2016-06-05 03:27:03\",\"landmark\":\"Raheja Residency\",\"email\":\"bayyangar@gmail.com\",\"name\":\"Balaji Ayyangar\",\"phonenumber\":\"7358316785\"}'),
	(66,6900,0,'2016-06-14 07:36:38',110,197,0,0,NULL,NULL,'{\"id\":\"21\",\"user_id\":\"110\",\"address_line_one\":\"I-002, Bren palms\",\"address_line_two\":\"kudlu main road,kudlu gate, bangalore\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560068\",\"timestamp\":\"2016-06-03 13:39:01\",\"landmark\":\"opposite to sri bhagya hotel\",\"email\":\"racheetar@gmail.com\",\"name\":\"racheeta ramanujam\",\"phonenumber\":\"9900813275\"}'),
	(67,0,0,'2016-06-14 12:21:44',81,198,0,1,NULL,'2016-06-15 22:24:58','{\"id\":\"17\",\"user_id\":\"81\",\"address_line_one\":\"First floor, Arunachalam, No. 49,\",\"address_line_two\":\"St. Anthony Nicolas Street, Ashok Nagar\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560088\",\"timestamp\":\"2016-05-31 06:24:09\",\"landmark\":\"Ozone Hotel, Markham Road\",\"email\":\"cinthya.anand@gmail.com\",\"name\":\"Cinthya Anand\",\"phonenumber\":\"8971062056\"}');

/*!40000 ALTER TABLE `orderdetails` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table orders
# ------------------------------------------------------------

DROP TABLE IF EXISTS `orders`;

CREATE TABLE `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `price` int(11) DEFAULT '0',
  `isshipped` tinyint(1) DEFAULT '0',
  `shippeddate` datetime DEFAULT NULL,
  `isdelivered` tinyint(1) DEFAULT '0',
  `deliverydate` datetime DEFAULT NULL,
  `ispaid` int(11) DEFAULT '0',
  `deliverycost` int(11) DEFAULT '0',
  `productitems_id` int(11) NOT NULL,
  `orderdetail_id` int(11) NOT NULL,
  `isqcdone` tinyint(1) DEFAULT '0',
  `qcdonedate` datetime DEFAULT NULL,
  `iscancelled` tinyint(1) DEFAULT '0',
  `canceldate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_orders_products_vendors1_idx` (`productitems_id`),
  KEY `fk_orders_orderdetails1_idx` (`orderdetail_id`),
  CONSTRAINT `fk_orders_orderdetails1` FOREIGN KEY (`orderdetail_id`) REFERENCES `orderdetails` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;

INSERT INTO `orders` (`id`, `price`, `isshipped`, `shippeddate`, `isdelivered`, `deliverydate`, `ispaid`, `deliverycost`, `productitems_id`, `orderdetail_id`, `isqcdone`, `qcdonedate`, `iscancelled`, `canceldate`)
VALUES
	(1,16200,1,'2016-05-23 11:08:46',0,NULL,0,0,18,1,1,'2016-05-23 11:08:16',1,'2016-05-23 11:11:31'),
	(2,5200,1,'2016-05-24 18:38:19',1,'2016-05-25 07:18:19',0,0,25,2,1,'2016-05-23 17:57:40',0,NULL),
	(3,6500,1,'2016-05-25 08:03:05',1,'2016-05-25 12:02:51',0,0,17,3,1,'2016-05-23 23:55:27',0,NULL),
	(5,9500,1,'2016-05-23 18:56:16',1,'2016-05-23 18:56:19',0,0,13,5,1,'2016-05-23 18:56:12',0,NULL),
	(6,7500,1,'2016-05-23 19:04:04',1,'2016-05-23 19:04:14',0,0,14,6,1,'2016-05-23 19:04:00',0,NULL),
	(7,5500,0,NULL,0,NULL,0,0,3,7,1,'2016-05-23 20:47:59',1,'2016-05-25 16:06:43'),
	(8,5200,1,'2016-05-24 18:38:33',1,'2016-05-25 07:24:11',0,0,8,8,1,'2016-05-24 11:34:23',0,NULL),
	(9,5800,1,'2016-05-25 08:02:50',0,NULL,0,0,26,9,1,'2016-05-24 11:34:31',1,'2016-05-25 15:59:29'),
	(10,5500,1,'2016-05-25 08:02:46',0,NULL,0,0,6,10,1,'2016-05-24 11:34:36',1,'2016-05-25 15:59:19'),
	(11,5500,0,NULL,0,NULL,0,0,20,11,0,NULL,1,'2016-05-24 14:43:46'),
	(12,6200,1,'2016-05-28 18:23:15',1,'2016-05-28 19:19:47',0,0,27,12,1,'2016-05-24 14:59:54',0,NULL),
	(14,4000,1,'2016-05-25 08:02:32',1,'2016-05-25 12:02:57',0,0,1,14,1,'2016-05-24 16:24:20',0,NULL),
	(15,6000,0,NULL,0,NULL,0,0,23,15,0,NULL,0,NULL),
	(16,5200,1,'2016-05-25 08:02:20',1,'2016-05-25 13:47:15',0,0,5,16,1,'2016-05-24 17:58:15',0,NULL),
	(17,6500,1,'2016-05-25 08:02:16',1,'2016-05-25 13:47:10',0,0,28,17,1,'2016-05-24 18:03:00',0,NULL),
	(18,5200,1,'2016-05-25 08:01:56',1,'2016-05-25 22:21:04',0,0,29,18,1,'2016-05-24 22:01:14',0,NULL),
	(19,5500,0,NULL,0,NULL,0,0,26,19,0,NULL,1,'2016-05-25 17:11:53'),
	(20,5500,1,'2016-05-26 17:03:13',1,'2016-05-27 12:33:54',0,0,6,20,1,'2016-05-26 11:36:42',0,NULL),
	(21,8000,1,'2016-05-29 15:46:27',1,'2016-05-29 15:46:31',0,0,33,21,1,'2016-05-26 19:06:32',0,NULL),
	(22,3000,0,NULL,0,NULL,0,0,32,22,1,'2016-05-26 23:24:10',0,NULL),
	(23,2300,1,'2016-05-26 23:25:40',1,'2016-05-26 23:26:03',0,0,31,22,1,'2016-05-26 23:24:06',0,NULL),
	(24,6900,1,'2016-05-27 19:53:26',1,'2016-05-28 07:00:08',0,0,34,23,1,'2016-05-27 19:53:21',1,'2016-05-28 11:54:01'),
	(25,6500,0,NULL,0,NULL,0,0,26,24,0,NULL,1,'2016-05-30 15:47:12'),
	(26,6500,1,'2016-06-01 12:14:34',1,'2016-06-01 13:02:58',0,0,26,25,1,'2016-05-30 17:53:19',0,NULL),
	(27,7000,1,'2016-06-01 16:13:55',1,'2016-06-01 16:14:05',0,0,61,26,1,'2016-05-31 11:57:08',0,NULL),
	(28,6600,1,'2016-06-02 07:32:52',1,'2016-06-02 08:35:30',0,0,58,27,1,'2016-05-31 15:08:03',0,NULL),
	(29,12300,0,NULL,0,NULL,0,0,64,28,0,NULL,1,'2016-05-31 21:12:46'),
	(30,6800,1,'2016-06-02 09:38:47',1,'2016-06-02 11:47:23',0,0,60,29,1,'2016-06-01 13:33:01',0,NULL),
	(31,8500,1,'2016-06-02 09:38:18',1,'2016-06-02 11:47:19',0,0,66,30,1,'2016-06-01 13:32:56',0,NULL),
	(32,6700,0,NULL,0,NULL,0,0,65,31,1,'2016-06-02 14:54:53',1,'2016-06-02 15:08:14'),
	(33,6900,1,'2016-06-05 07:43:14',1,'2016-06-05 12:33:00',0,0,69,32,1,'2016-06-03 21:00:21',0,NULL),
	(34,5800,1,'2016-06-05 07:43:10',1,'2016-06-07 07:00:44',0,0,56,33,1,'2016-06-03 21:01:49',0,NULL),
	(35,5200,1,'2016-06-05 07:43:06',1,'2016-06-05 14:16:35',0,0,70,34,1,'2016-06-04 11:42:09',0,NULL),
	(36,7200,0,NULL,0,NULL,0,0,62,35,1,'2016-06-04 11:51:30',1,'2016-06-04 11:59:34'),
	(37,8550,0,NULL,0,NULL,0,0,67,36,1,'2016-06-04 12:25:29',1,'2016-06-05 13:17:25'),
	(38,6700,1,'2016-06-05 07:42:57',1,'2016-06-05 11:45:50',0,0,65,37,1,'2016-06-04 16:16:51',0,NULL),
	(39,6600,1,'2016-06-05 07:42:52',1,'2016-06-05 16:56:37',0,0,62,38,1,'2016-06-04 16:16:48',0,NULL),
	(40,6200,1,'2016-06-05 07:42:45',1,'2016-06-05 16:56:30',0,0,55,39,1,'2016-06-04 18:40:41',0,NULL),
	(41,6100,1,'2016-06-05 07:42:40',1,'2016-06-05 10:00:09',0,0,57,40,1,'2016-06-04 20:04:21',0,NULL),
	(42,8000,1,'2016-06-05 07:42:31',1,'2016-06-05 13:21:39',0,0,71,41,1,'2016-06-04 23:10:26',0,NULL),
	(43,8300,1,'2016-06-07 07:08:37',1,'2016-06-07 09:11:00',0,0,72,42,1,'2016-06-05 11:57:50',0,NULL),
	(44,12300,1,'2016-06-08 08:54:53',1,'2016-06-08 10:28:12',0,0,64,43,1,'2016-06-05 12:53:42',0,NULL),
	(45,6200,0,NULL,0,NULL,0,0,82,44,1,'2016-06-05 21:54:55',1,'2016-06-06 21:56:47'),
	(47,6500,1,'2016-06-06 08:17:21',1,'2016-06-06 09:04:14',0,0,80,46,1,'2016-06-05 17:19:53',0,NULL),
	(48,6950,1,'2016-06-08 06:59:12',1,'2016-06-08 08:05:23',0,0,87,47,1,'2016-06-05 17:19:57',0,NULL),
	(49,5500,0,NULL,0,NULL,0,0,68,48,1,'2016-06-05 21:49:23',1,'2016-06-07 07:09:39'),
	(50,5600,1,'2016-06-07 07:09:12',1,'2016-06-07 09:10:34',0,0,76,48,1,'2016-06-05 21:49:20',0,NULL),
	(51,5500,0,NULL,0,NULL,0,0,68,49,0,NULL,1,'2016-06-06 05:53:19'),
	(52,8900,0,NULL,0,NULL,0,0,86,49,0,NULL,1,'2016-06-06 05:53:21'),
	(53,12700,0,NULL,0,NULL,0,0,9,50,0,NULL,1,'2016-06-06 05:53:28'),
	(54,12000,0,NULL,0,NULL,0,0,11,51,0,NULL,1,'2016-06-06 05:53:31'),
	(55,8500,0,NULL,0,NULL,0,0,85,52,0,NULL,1,'2016-06-06 05:53:37'),
	(56,8200,1,'2016-06-07 11:10:32',1,'2016-06-07 12:08:22',0,0,63,53,1,'2016-06-06 12:42:49',0,NULL),
	(57,5200,1,'2016-06-07 07:08:51',1,'2016-06-07 09:10:24',0,0,59,54,1,'2016-06-06 12:42:46',0,NULL),
	(58,5900,1,'2016-06-09 06:30:40',1,'2016-06-13 16:37:26',0,0,75,55,1,'2016-06-06 17:03:52',0,NULL),
	(59,6300,1,'2016-06-09 06:30:31',1,'2016-06-09 19:50:00',0,0,73,56,1,'2016-06-07 08:39:15',0,NULL),
	(60,5500,0,NULL,0,NULL,0,0,68,57,0,NULL,0,NULL),
	(61,8900,0,NULL,0,NULL,0,0,86,58,1,'2016-06-07 16:00:01',0,NULL),
	(62,8500,1,'2016-06-08 06:59:38',1,'2016-06-08 09:25:35',0,0,85,59,1,'2016-06-07 15:59:40',0,NULL),
	(63,6200,1,'2016-06-09 06:30:13',1,'2016-06-09 08:05:08',0,0,82,60,1,'2016-06-07 15:59:32',0,NULL),
	(64,7200,1,'2016-06-13 14:36:45',1,'2016-06-13 14:36:53',0,0,83,61,1,'2016-06-08 20:49:20',0,NULL),
	(65,15500,0,NULL,0,NULL,0,0,12,62,0,NULL,1,'2016-06-12 14:45:45'),
	(66,6300,0,NULL,0,NULL,0,0,74,63,0,NULL,0,NULL),
	(67,8300,0,NULL,0,NULL,0,0,77,64,0,NULL,0,NULL),
	(68,12000,0,NULL,0,NULL,0,0,11,65,1,'2016-06-13 20:46:37',0,NULL),
	(69,6900,1,'2016-06-15 18:20:03',1,'2016-06-16 11:52:16',0,0,88,66,1,'2016-06-14 09:20:39',0,NULL),
	(70,6800,0,NULL,0,NULL,0,0,81,67,1,'2016-06-14 14:47:12',1,'2016-06-15 22:24:58');

/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table parentcategories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `parentcategories`;

CREATE TABLE `parentcategories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(512) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(512) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `parentcategories` WRITE;
/*!40000 ALTER TABLE `parentcategories` DISABLE KEYS */;

INSERT INTO `parentcategories` (`id`, `name`, `description`, `image`)
VALUES
	(1,'Home Appliances','Home Appliances',''),
	(2,'Furniture','Furniture','');

/*!40000 ALTER TABLE `parentcategories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table productcategories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `productcategories`;

CREATE TABLE `productcategories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `description` text NOT NULL,
  `image` varchar(512) NOT NULL,
  `parentcategory_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `productcategories` WRITE;
/*!40000 ALTER TABLE `productcategories` DISABLE KEYS */;

INSERT INTO `productcategories` (`id`, `name`, `description`, `image`, `parentcategory_id`)
VALUES
	(3,'Refrigerators','Top Quality Refrigerators','fridge.jpg',1),
	(4,'Washing Machines','Top Quality Washing Machines','washingmachine.jpg',1),
	(5,'Air Conditioners','Top quality Air Conditioners','airconditioner.jpg',1),
	(6,'Water Purifiers','Top Quality Water Purifiers','waterpurifier.jpg',1),
	(7,'Sofa set','Sofa set at cheap price','sofaset.jpg',2),
	(8,'Office Chair','Best Quality Office Chairs','officechair.jpg',2),
	(9,'Computer Table','Compuer Tables at best price','computertable.jpg',2);

/*!40000 ALTER TABLE `productcategories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table productfilters
# ------------------------------------------------------------

DROP TABLE IF EXISTS `productfilters`;

CREATE TABLE `productfilters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filter_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table productitemfilters
# ------------------------------------------------------------

DROP TABLE IF EXISTS `productitemfilters`;

CREATE TABLE `productitemfilters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filter_id` int(11) NOT NULL,
  `productitem_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table productitems
# ------------------------------------------------------------

DROP TABLE IF EXISTS `productitems`;

CREATE TABLE `productitems` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qualityindex` int(11) DEFAULT '0',
  `title` varchar(512) NOT NULL,
  `description` varchar(256) DEFAULT NULL,
  `issold` int(11) DEFAULT '0',
  `isapproved` int(11) DEFAULT '0',
  `saleprice` int(11) DEFAULT '0',
  `manufacturingyear` int(11) NOT NULL,
  `warranty` varchar(256) NOT NULL,
  `vendorprice` int(11) DEFAULT '0',
  `ispaid` int(11) DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `isdelivered` int(11) DEFAULT '0',
  `productcategory_id` int(11) DEFAULT NULL,
  `color` varchar(100) DEFAULT NULL,
  `mrp` int(11) DEFAULT '99999',
  `longdescription` varchar(1500) DEFAULT '',
  `technicalspec` varchar(9999) DEFAULT NULL,
  `isfactory` tinyint(1) DEFAULT '0',
  `conditionspec` varchar(5000) DEFAULT NULL,
  `hidden` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_products_vendors_users1_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `productitems` WRITE;
/*!40000 ALTER TABLE `productitems` DISABLE KEYS */;

INSERT INTO `productitems` (`id`, `qualityindex`, `title`, `description`, `issold`, `isapproved`, `saleprice`, `manufacturingyear`, `warranty`, `vendorprice`, `ispaid`, `user_id`, `product_id`, `timestamp`, `isdelivered`, `productcategory_id`, `color`, `mrp`, `longdescription`, `technicalspec`, `isfactory`, `conditionspec`, `hidden`)
VALUES
	(1,0,'Haier Washing Machine XPB65-114D','Semi Automatic Top Load 6.5 Kg',0,1,4000,2012,'',0,0,2,1,'2016-07-16 07:31:05',0,4,NULL,8444,'Rust Free: Now placing your machine in wet area of your home is not a problem because plastic cabinet is rust free so it gives better & longer life to your washing machine. 45 Minutes Turbo Soak: Haier washing machine with active Turbo Soak Wash of 45 minutes ensures proper cleaning of badly soiled clothes. Bubble Wash: Bubbles comes out from the bottom of the tub, hits the rigid stains of the clothes & remove this dirt out from laundry to give your brighter & cleaner clothes. Overload Protection: Protection from overfilling of clothes in washing machine ensure no damage due to overload. Washine machine doesn\'t work while it is overloaded. Buzzer: You can control the volume of end of cycle buzzer. This feature help you to avoid any disturbance. Powerful Motor: Highly efficient motor ensures powerful rotation of clothes. Strong motor promises ensures high durability & efficiency to take heavy load.','Brand: Haier\nControl Type: Semi Automatic Top Load\nWashing Capacity: 6.5 kg\nColor: Grey\nDimensions: 78 cm x 92 cm x 43.5 cm\nWashing Method: Bubble Wash\nFreshers: Overload protection, Spin shower, In-built scrubber',NULL,NULL,0),
	(2,0,'Godrej Washing Machine GWF 620 FSX','Fully Automatic Top Load 6.2 kg',0,1,5000,2011,'',0,0,2,1,'2016-07-16 07:31:05',0,4,NULL,15470,'Godrej GWF 620 FSX is a perfect replacement for all your old washing system. It efficiently uses energy, water and detergent to keep your clothes clean. The intelligent micro-computer controls all operations precisely to ensure a perfect wash. The machine is equipped with fuzzy load and quality sensing to determine the water level and wash cycle appropriate for the wash load. Washing, rinsing and spin drying happens with a single selection on the control panel. It provides the flexibility of choosing the right program from Regular, Synthetics & Heavy. Also allows one to select the right water level incase of non-fuzzy operation. It has a unique Dry Tap Protection feature If the water flow stops during the water filling process, the machine starts hibernating. If the water level shows no rise in level in the next 15 minutes the machine indicates by sounding beeps and flickering LEDs. Auto restart with memory back-up ensures that your program settings stay the way you had set them in case of a power failure. Auto Balance System if there is an imbalance in the wash load, the machine automatically stops, tries to redistribute the load evenly and resumes washing.','Hot / Cold water inlet : Cold\nColor: Yellow\nInner tub :Stainless Steel\nDimensions (w x d x h) in mm :560x555x904\nNo of Wash Programs :3\nTechnology :Pulsator\nWash capacity : 6.2 kg\nWindow :Plastic\nType: Fully Automatic washing machine',NULL,NULL,0),
	(3,0,'Samsung Washing Machine WA82BSLEC','Fully Automatic Top Load 6.2Kg',0,1,5500,2013,'',0,0,2,1,'2016-07-16 07:31:05',0,4,NULL,16500,'Enjoy smart washing with the Samsung WA82BSL','Capacity: 6.2Kg\nFunction: Last setting\nDimension (WxHxD): 540mm x 940mmx 560mm\nFeature: Eco storm technology pulsator,5 step super clean, air turbo dry, magic filter, aqua preserve, sari course\nDrum: Diamond drum tub\nColor: Grey',0,'Few scratches in the front side\nFew areas rusted in the back side',0),
	(4,0,'Godrej Washing Machine GWF 620 FSX','Fully Automatic Top load 6 Kg',0,1,5000,2012,'',0,0,2,1,'2016-07-16 07:31:06',0,4,NULL,14780,'Godrej GWF 620 FSX is a perfect replacement for all your old washing system. It efficiently uses energy, water and detergent to keep your clothes clean. The intelligent micro-computer controls all operations precisely to ensure a perfect wash. The machine is equipped with fuzzy load and quality sensing to determine the water level and wash cycle appropriate for the wash load. Washing, rinsing and spin drying happens with a single selection on the control panel. It provides the flexibility of choosing the right program from Regular, Synthetics & Heavy. Also allows one to select the right water level incase of non-fuzzy operation. It has a unique Dry Tap Protection feature If the water flow stops during the water filling process, the machine starts hibernating. If the water level shows no rise in level in the next 15 minutes the machine indicates by sounding beeps and flickering LEDs. Auto restart with memory back-up ensures that your program settings stay the way you had set them in case of a power failure. Auto Balance System if there is an imbalance in the wash load, the machine automatically stops, tries to redistribute the load evenly and resumes washing.','Dimension : 560x904x555mm\nType: Fully Automatic\nCapacity: 6 Kg\nWash programs: 3\nSpecial features: Cold Temperature selection, Force 4 Technology, Cold Water Inlet,Dry tap protection, Auto shut, Auto balance system\nFuzzy logic : yes\nColor: Red',0,NULL,0),
	(5,0,'Godrej Washing Machine GWF 620 FSX','Fully Automatic Top load 6 Kg',0,1,5200,2012,'',0,0,2,1,'2016-07-16 07:31:06',0,4,NULL,14780,'Godrej GWF 620 FSX is a perfect replacement for all your old washing system. It efficiently uses energy, water and detergent to keep your clothes clean. The intelligent micro-computer controls all operations precisely to ensure a perfect wash. The machine is equipped with fuzzy load and quality sensing to determine the water level and wash cycle appropriate for the wash load. Washing, rinsing and spin drying happens with a single selection on the control panel. It provides the flexibility of choosing the right program from Regular, Synthetics & Heavy. Also allows one to select the right water level incase of non-fuzzy operation. It has a unique Dry Tap Protection feature If the water flow stops during the water filling process, the machine starts hibernating. If the water level shows no rise in level in the next 15 minutes the machine indicates by sounding beeps and flickering LEDs. Auto restart with memory back-up ensures that your program settings stay the way you had set them in case of a power failure. Auto Balance System if there is an imbalance in the wash load, the machine automatically stops, tries to redistribute the load evenly and resumes washing.','Dimension : 560x904x555mm\nType: Fully Automatic\nCapacity: 6 Kg\nWash programs: 3\nSpecial features: Cold Temperature selection, Force 4 Technology, Cold Water Inlet,Dry tap protection, Auto shut, Auto balance system\nFuzzy logic : yes\nColor: Red',0,'Rust in the back side at the bottom\nBlack stain in the top panel',0),
	(6,0,'Samsung Washing Machine WA78E4GEC/XTL','Fully Automatic Top load 5.8 Kg',0,1,5500,2013,'',0,0,2,1,'2016-07-16 07:31:06',0,4,NULL,14000,'Fully Automatic Top load washing machine','Capacity: 5.8KG\nColor: White\nForm Factor: Top load\nType: Fully Automatic',0,'Minor rust in the back side at the bottom\nFew scratches in the front side',0),
	(7,0,'SAMSUNG Washing Machine WA88VPBEH','Fully Automatic Top load 6.8KG',0,1,10500,2015,'',0,0,2,1,'2016-07-16 07:31:06',0,4,NULL,15800,'This 6.8 kg top loading fully automatic washing machine with pulsator wash has 8 preset programmes. The machine allows prewash (soak), hot water wash, heavy wash, normal wash, delicate wash and quick wash. It also has additional features like washload sensor, automatic drainage, auto restart and child lock. It has mist shower programme, silver nano technology and air turbo drying function. It has diamond drum design for cleaner and gentler wash. It has a metallic silver colour body with Carmen wine colour top cover','Capacity: 6.8Kg\nPulsator Type	:	Double Storm\nWater Saving Tub	:	YES\nColour	:	Top cover Refined Black\nColour	:	Body Silver\nAir Turbo Drying System™	:	YES\nInner Tub	:	STS (Diamond)\nWindow	:	Transparent\nWater Inlet	:	Hot Cold\nClean Tub	:	5-Step Super Clean System\nWash Programs	:	8\n(W X D X H, mm)	:	540 x 560 x 920mm\nAg+Silver Nano	:	YES\nChild Lock	:	YES',NULL,NULL,0),
	(8,0,'Whirlpool Refrigerator 165 DLX','Single door 165 Litres',0,1,5200,2011,'',0,0,2,1,'2016-07-16 07:31:06',0,3,NULL,13000,'This Refrigerator comes in an attractive shade of wine fiesta and has a glossy finish. The refrigerator is very sturdy and has a solid built. The style and finish goes with any decor of your house be it contemporary or traditional. Since it is equipped with toughened glass shelves it can hold medium to large containers in a convenient manner.','Brand: Whirlpool\nModel: 190 DLX\nColor: Wine Fiesta\nType: Single door\nCapacity: 165 litres\nDoors:1 \nDefrosting type: Direct cool\nWxHxD: 536x623x1186 mm\nStar Rating: 4 Star\nShelves: 2',NULL,NULL,0),
	(9,0,'Godrej  Refrigerator 185L','Direct Cool Single door refrigerator',0,1,12700,2016,'',0,0,2,1,'2016-07-16 07:31:07',0,3,NULL,15900,'This refrigerator comes with 2 year warranty.\n\nOther Feature: Environment Friendly, Largest Freezer Space, Largest Shelf Space, 2.25 L Aquaspace\nTechnology Used: SIF Technology, Anti-B Technology','Number of Shelves: 3\n5 Star Rating\nDirect Cool\nColor: Wine Red\nTop Freezer Refrigerator\nWxHxD: 577 mm X 1247 mm X 667 mm',1,NULL,0),
	(10,0,'Godrej Refrigerator 185L','Direct Cool Single door refrigerator',0,1,12700,2016,'',0,0,2,1,'2016-07-16 07:31:07',0,3,NULL,15900,'This refrigerator comes with 2 years warrenty.\n\nOther Features: Environment Friendly, Largest Freezer Space, Largest Shelf Space, 2.25 L Aquaspace\nTechnology Used: SIF Technology, Anti-B Technology','Number of Shelves: 3\n5 Star Rating\nDirect Cool\nColor: Carbon Leaf\nTop Freezer Refrigerator\nWxHxD: 577 mm X 1247 mm X 667 mm',1,NULL,0),
	(11,0,'LG Refrigerator GL-B252VLGY','Double-door Refrigerator (240 Ltr, 2 Star Rating, Neo Inox)',0,1,12000,2016,'',0,0,2,1,'2016-07-16 07:31:07',0,3,NULL,21000,'Welcome home an environment friendly LG GL-B252VLGY Refrigerator that blends good looks with advanced functionality. The sleek built in the Neo Inox shade and the double doors make it look impressive. The 240 Litres refrigerator offers effective and uniform cooling by means of an efficient reciprocators compressor, mechanical temperature control, and a four way cooling technology as well. Perfectly compartmentalizes and equipped with a humidity controller and deodorizes, the unit ensures that the food you store stays healthy and fresh for longer hours in an energy efficient manner. It comes along with a 9 years compressor warranty and a 1 year comprehensive warranty from the manufacturer.','Brand: LG\nModel: GL-B252VLGY(BNI)\nEnergy Efficiency: 2 Star Rating\nCapacity: 240 Liters\nInstallation Type: Free-Standing\nPart Number: GL B252VLGY\nForm Factor: Standard Double Door\nColor: Wine Red\nDefrost System: Frost Free\nNumber of Shelves:2',NULL,NULL,0),
	(12,0,'LG Intellocool Refrigerator','375 Litre double door refrigerator',0,1,15500,2015,'',0,0,2,1,'2016-07-16 07:31:07',0,3,NULL,29800,'Double door refrigerator','Brand: LG\nForm Factor: Double door\nCapacity: 375 Liters\nColor: metallic grey',0,NULL,0),
	(13,0,'Samsung BioFresh Refrigerator','Double door frost free refrigerator',0,1,9500,2010,'',0,0,2,1,'2016-07-16 07:31:07',0,3,NULL,19000,'Samsung double door refrigerator with 6 months warranty','Capacity: 230 Litres\nDefrost system: Frost free\nForm factor: Standard double door\nColor: Grey',0,NULL,0),
	(14,0,'Whirlpool Refrigerator','6th sense single door refrigerator',0,1,7500,2012,'',0,0,2,1,'2016-07-16 07:31:08',0,3,NULL,15799,'Single door whirlpool refrigerator at best price','Defrost system: frost control\nColor: grey\nCapacity: 200 litre\nForm Factor: Single door',0,NULL,0),
	(15,0,'LG Refrigerator','Intellocool Single Door Refrigerator',0,1,5400,2012,'',0,0,2,1,'2016-07-16 07:31:08',0,3,NULL,12050,'If you are looking for one of the best storage options, then this refrigerato can be none other than Single Door refrigerator. This refrigerator is an amalgam of vanguard technology, stellar, user-oriented features and a swank design. The fridge is powered with the Evercool Technology which allows it to stay cool for up to 9 hours during power cuts. This Direct Cool Refrigerator produces ice 20% faster with the help of specially designed patented ice tray. Hence, getting a chilled drink is very easy now and you get it a lot faster without pressing any button.','Color: Red\nCapacity: 175L\nDimension: 541x575x1143\nType: Direct cool\nMRP: 8050\nDoor lock\nNumber of doors: 1\nFeature: Opaque interior, A-one cooling, Adjustable Shelves ,Extra Puff, Intello Compressor',0,NULL,0),
	(16,0,'Godrej Refrigerator','Single door semi-automatic 183 litre',0,1,6500,2012,'',0,0,2,1,'2016-07-16 07:31:08',0,3,NULL,10990,'As this environment-friendly refrigerator is HCFC, CFC and HFC-free, it doesn\'t damage the ozone layer.','Capacity: 183Litre\nNo. of doors: 1\nDoor Type: Single\nDefrosting Type: Semi Automatic\nColor: Light Lavender\nMaterial: Acrylic\nDoor lock : yes',0,NULL,0),
	(17,0,'Godrej Refrigerator GDA 19 A3/2012','Single door semi-automatic 181L',0,1,6500,2013,'',0,0,2,1,'2016-07-16 07:31:08',0,3,NULL,13500,'Rib Door Design for More Space in the Door. Better Space Organization. 2.5 L AquaSpace to keep larger bottles in the door. Deep Bottom Chiller Tray. Transparent Interiors. Roll Bond Freezer and Thicker Insulation for Superior Cooling. External Thermostat Control Lets you regulate the temperature without opening the door, thus saving on power consumption. It also allows more storage space inside the refrigerator. High Efficiency Compressor Powerful compressor that cools fast & yet saves on power, so you no longer have to worry about electricity bills','Capacity: 181L\nDefrosting Type: Semi Automatic\nNo.of doors: 1\nStar rating : 4 Star\nTemperatur control mode: Dial\nColor: Red\nOther Features: Rib Door Design for More Space in the Door, Deep Bottom Chiller Tray, Transparent Interiors,External Thermostat Control',0,NULL,0),
	(18,0,'Whirlpool FP 263D Proton Refrigerator','Roy Steel Knight 240 Ltrs Frost Free Fridge Refrigerator (Alpha Steel)',0,1,16200,2015,'',0,0,2,1,'2016-07-16 07:31:08',0,3,NULL,29500,'With the revolutionary 6th Sense ActiveFresh Technology, presenting the 3 door Protton World Series Refrigerator. Its Active Fresh Zone with Moisture Retention and the incredible MicroBlock, keeps your fruits and vegetables fresher for longer.\n\nMoisture Retention Technology\nMicroblock\nFresh Keeper\nAir Boosters\n3 Door Format','Gross Capacity (Ltr) : 240\n6th Sense Active Fresh Technology\nSeparate Vegetable Drawer\nAir Booster System\nQuick Chill Bottle Zone\nColor: Alpha steel',1,NULL,1),
	(19,0,'Whirlpool Refrigerator Roy 3S Neo FR258','Royal Double Door Refrigerator Wine Exotica 245 Ltr',0,1,16200,2016,'',0,0,2,1,'2016-05-31 04:49:15',0,3,NULL,24750,'The Whirlpool Neo offers an ideal way to keep your food and beverages fresh and cool. Powered by revolutionary 6th Sense Fresh Control Technology, the refrigerator maintains freshness of fruits and vegetables for up to 7 days. Its advanced cooling system and powerful compressor makes ice in quick time. This double door refrigerator displays an attractive design and eye-catching colour scheme. It can look great in most home decor styles.','Brand: Whirlpool\nModel: NEO IC255 Royal\nColor: Wine Exotica\nType: Double Door\nCapacity: 242 Litres\nDoors:2 \nDefrosting type: Frost-Free\nDimension: 560x662x1495mm\nShelves: 2',1,NULL,1),
	(20,0,'Whirlpool Single Door Refrigerator','180 litres 195 MP Roy 4s',0,1,5500,2011,'',0,0,2,1,'2016-07-16 07:31:10',0,3,NULL,13990,'Keep the food, fruits, vegetables and drinks cool and fresh with the Whirlpool 195 MP 4DG 180 Ltr Single Door Direct Cool Blue Duet Refrigerator. Featuring a capacity of 180 litres, it will meet the needs of modern households, while its 4 star rating will help you by reducing down the energy bills. Owing to its subtle blue duet colours, this refrigerator is the perfect blend of style and sensibility.\n\nThe refrigerator has an anti-fungal door gasket. This easy-to-clean and removable airtight gasket seals-in the freshness and acts as a barrier for the bacteria and dust particles that accumulate on the door seal. It also prevents mould spores from entering and spoiling the food inside, keeping the food fresh for a longer period. The refrigerator has a unique utility drawer, which is a non-refrigerated zone ideal for storing vegetables/eatables like potatoes, onions and garlic etc.','Single door type\ndirect cool\n180 litres capacity\n4 star rating\nToughened glass shelves\n6th sense door open alarm\n130-300 V Operation\nJumbo Bottle rack\nUnique Utility drawer\nStabiliser free operation\nUnique health guard door',0,NULL,0),
	(21,0,'LG Refrigerator','Double door frost free 230 Litre',0,1,8000,2011,'',0,0,2,1,'2016-07-16 07:31:10',0,3,NULL,16500,'LG Double door refrigerator at good working condition','Form Factor: Double door\nColor: Metallic\nType: Frost free\nCapacity: 230 Litre',0,NULL,0),
	(22,0,'Samsung Refrigerator','Biofresh double door frost free',0,1,7500,2010,'',0,0,2,1,'2016-05-31 04:48:40',0,3,NULL,14000,'Double door refrigerator at good working condition','Large Freezer Storage Area- Stores large quantities of food in the freezer.\nJumbo Storage Guard- Holds large bottles securely\nAdjustable Storage Guards- Position them for your convenience and accordingto the height of your food containers\nBio-Fresh Zone- Keeps vegetables fresh for longer periods\nSliding Drawer (Premium only)- Extra storage space for vegetables and fruits\niCooling (Premium only)- Advanced air circulation for faster surround cooling\nSuper Freeze Zone- Freezes stored items quickly\nTwist Ice-Tray (Premium Only)- Spring mechanism for automatic release of ice cubes',0,NULL,1),
	(23,0,'Samsung Refrigerator','Single door 180 litres',0,1,6000,2011,'',0,0,2,1,'2016-07-16 07:31:11',0,3,NULL,12750,'Single door Samsung refrigerator at good working condition','Form Factor: Single Door\nCapacity: 180 litres\nColor: Metallic grey\nType: Frost Free',0,NULL,1),
	(24,0,'LG Refrigerator','Single door 250 litre',0,1,11000,2016,'',0,0,2,1,'2016-07-16 07:31:11',0,3,NULL,17000,'This refrigerator comes with 2 years of motor warranty','Form factor: single door\nCapcity: 250 litre\nColor: Metallic grey leaf',1,NULL,1),
	(25,0,'LG Refrigerator','LG Single door refrigerator',0,1,5200,2011,'',0,0,2,1,'2016-07-16 07:31:11',0,3,NULL,12000,'LG Single door refrigerator at good working condition','Capacity: 185 Litres\nColor: Metallic grey\nForm factor: Single door',0,NULL,0),
	(26,0,'LG Intellocool Refrigerator','LG Single door refrigerator, 230 Litre',0,1,6500,2012,'',0,0,2,1,'2016-07-16 07:31:12',0,3,NULL,14000,'LG Single door refrigerator in good working condition. Quality tested with quality score.','Capacity: 230 Litres\n\nColor: Metallic grey\n\nForm factor: Single door',0,'New Thermostat installed\nTray replaced\nVegetable drawer installed',0),
	(27,0,'Godrej 183L Single Door','Godrej GDE 19 DX4 Single Door 183 Litres Refrigerator (Silver)',0,1,6200,2012,'',0,0,1,1,'2016-07-16 07:31:12',0,3,NULL,14350,'Stay Cool Technology, SIF Technology, Revolutionary Cooling Technology, Zinc Oxide Protection Technology, Superior Air Circulation, Convection Control Technology with SIF, Aroma Lock, Honeycomb Crisper Cover, Easily Removable Gasket, Stabilizer Free Operation, Handle','No. of DoorsSingle Door\nPower Supply140 - 260 V, 50 Hz\nEnergy Rating5\nExteriorsMetallic Door\nShelvesToughened Glass Shelves(2)\nLampsMain Lamp\nRefrigeration and Cooling TechnologyHumidity Control with Superior Cool\nSpecial CompartmentsVegetable Tray, Bottle Shelves, Bottle Snugger, 2.5 L AquaSpace, 20 L Jumbo Vegetable Tray\nDoor LockAvailable\nOther Convenience FeaturesDeodourizing\nAdditional FeaturesStay Cool Technology, SIF Technology, Revolutionary Cooling Technology, Zinc Oxide Protection Technology, Superior Air Circulation, Convection Control Technology with SIF, Aroma Lock, Honeycomb Crisper Cover, Easily Removable Gasket, Stabilizer Free Operation, Handle\nSpecial FeaturesDry Storage, Moisture Control\nRefrigerator Door Pockets',0,NULL,0),
	(28,0,'Whirlpool Single Door Refrigerator','180 litres 195 MP Roy 4s',0,1,7000,2014,'',0,0,2,1,'2016-07-16 07:31:12',0,3,NULL,13990,'In excellent condition. Keep the food, fruits, vegetables and drinks cool and fresh with the Whirlpool 195 MP 4DG 180 Ltr Single Door Direct Cool Blue Duet Refrigerator. Featuring a capacity of 180 litres, it will meet the needs of modern households, while its 4 star rating will help you by reducing down the energy bills. Owing to its subtle blue duet colours, this refrigerator is the perfect blend of style and sensibility. The refrigerator has an anti-fungal door gasket. This easy-to-clean and removable airtight gasket seals-in the freshness and acts as a barrier for the bacteria and dust particles that accumulate on the door seal. It also prevents mould spores from entering and spoiling the food inside, keeping the food fresh for a longer period. The refrigerator has a unique utility drawer, which is a non-refrigerated zone ideal for storing vegetables/eatables like potatoes, onions and garlic etc.','Single door type\n\ndirect cool\n\n180 litres capacity\n\n4 star rating\n\nToughened glass shelves\n\n6th sense door open alarm\n\n130-300 V Operation\n\nJumbo Bottle rack\n\nUnique Utility drawer\n\nStabiliser free operation\n\nUnique health guard door',0,NULL,0),
	(29,0,'Godrej Fully Automatic','Top Load, 5.5Kg, Pulsator wash',0,1,5200,2013,'',0,0,2,1,'2016-07-16 07:31:12',0,4,NULL,11950,'This 5.5 kg top loading fully automatic washing machine has pulsator wash with 4 preset programmes. The machine allows prewash (soak), heavy wash, normal wash and delicate wash. It also has additional features like washload sensor, automatic drainage, quality sensing, memory back up and customized wash option for regular, synthetic, heavy and woolen clothes apart from fuzzy logic. The Force 4 Technology dislodges the toughest dirt from the most difficult to clean areas, giving you the cleanest wash ever.','Type of Washing Machine Fully Automatic \nControl Type Micro Processor \nLoading Type Top \nWashing Programmes	4\nWash Load	5.5\nType of Washing Machine	fully automatic\nControl Type	Micro processor\nBasket Material	Polypropylene\nWash Method	Pulsator',0,NULL,0),
	(30,0,'Whirlpool refrigerator 205','ICEMAGIC POWERCOOL PRM 5S 190 ltr',0,1,9500,2016,'',0,0,2,1,'2016-07-16 07:31:12',0,3,NULL,15435,'From now even power cuts will become moments of celebration. The new Ice Magic range of refrigerators, powered by 6th Sense PowerCool Technology, retains cooling for up to 12 hours during power cuts. That means, you can enjoy cold beverages even during long power cuts.','Star rating: 5 star rating\nColor: Wine exotica\nFeature: 6th sense powercool technology, 12 hours cooling retention during powercut,extra large extra cool freezer at -26c, toughened glass shield',0,NULL,0),
	(31,0,'Office Chair height adjustable','Adjustable back seat and rolling, lean back with lean lock',0,1,2300,2013,'',0,0,2,1,'2016-07-16 07:31:13',0,8,NULL,5000,'Installation and demo for this product is done free of cost as part of this purchase. Product is in excellent condition without any issues. Sparingly used. Product comes with plastic covering.\nPocketin Quality check done and quality score included.','Metal\nKnock Down\nAdjustable Height: Yes\nSeat Lock Included:	Yes\nWheels Included: Yes\nLean Back: Yes\nLean Lock: Yes\nSuitable For: Study & Home Office\nArmrest Included: Yes',0,'Sparingly used office chair in very good condition\nNo noticeable or unnoticeable defects\nHeight adjustment, lean back, and wheels working perfectly.',1),
	(32,0,'Office Chair Height adjustable','Adjustable back seat, Rolling wheels, lean back with lean lock',0,1,3000,2014,'',0,0,2,1,'2016-07-16 07:31:13',0,8,NULL,6000,'Installation and demo for this product is done free of cost as part of this purchase. Product is in excellent condition without any issues. Sparingly used. Product comes with plastic covering.\nPocketin Quality check done and quality score included.','Metal\nKnock Down\nAdjustable Height: Yes\nSeat Lock Included:	Yes\nWheels Included: Yes\nLean Back: Yes\nLean Lock: Yes\nSuitable For: Study & Home Office\nArmrest Included: Yes',0,'Sparingly used office chair in very good condition\nNo noticeable or unnoticeable defects\nHeight adjustment, lean back, and wheels working perfectly.',1),
	(33,0,'LG Front Loading 6.5 KG Fully automatic','Allergy care, Energy Saving, Hot water',0,1,8000,2013,'',0,0,2,1,'2016-07-16 07:31:13',0,4,NULL,28900,'LG FRONT LOADING 6.5 KG WD 10490TP	washing machine comes with energy savings, allergy care, fully cycle functionality and in excellent condition. Pocketin quality check done and quality score included.','General\nCapacity	:	6.5 kg\nPower\nWATTS	:	400W\nMain Features\nFault Diagnosis	:	YES\nFuzzy Logic	:	YES\nWool	:	YES\nChild Lock	:	YES\nCrease Care	:	YES\nFeatures\nTemperature Range	:	30 - 95 Deg\nSpin Speed options	:	4\nSpin Speeds (rpm)	:	No Spin/400/800/1000\nWash Programs	:	Cotton,Baby care,Synthetic,Delicate\nQuick 30	:	YES\nOnly Spin	:	YES\nSpecial Options	:	Bio Wash\nSpecial Options	:	Rinse Hold\nSpecial Options	:	Rinse Plus\nSpecial Options	:	Pre Wash\nTime Save	:	YES\nTime Delay	:	YES\nDoor Diameter (mm)	:	300mm\nProgram Selection	:	Digi Push\nDoor Opening Angle	:	180 Deg',0,'Its like new without any issues\nNo major scratches or dents visible\ninlet and outlet pipes are include',0),
	(34,0,'Godrej Eon 6.5 Kg Top Load Fully automatic','Less than a year old, Fuzzy Logic, Truly Automatic Washing Machine',0,1,6800,2015,'',0,0,2,1,'2016-07-16 07:31:13',0,4,NULL,18500,'Senses the quantity and quality of wash load to determine the water level and wash cycle appropriate for the wash load.The Dynamic Aqua Power Control Technology automatically puts the washing machine in sleep mode, at the first detection of a water/ power cut. Pocketin quality assurance with quality check value','Digital Display\nSteel Drum\nToughened Glass Lid\n8 Wash programs.\nMemory backup\nActive Soak\n\nGeneral\nBrand	Godrej\nModel	GWF 650 FC\nType	Top Load\nCapacity	6.5 litres\nTechnology	Fully-automatic\nPanel Display	LED\nWash Program	6\nSpecial Features	Save Settings feature, Cold Temprature Selection, Cold Water Inlet, Auto Shut, Water Optimizer',0,'In Excellent condition without noticeable scratches or dents.',0),
	(35,0,'Office Chair height adjustable','Adjustable back seat and rolling, lean back with lean lock',0,1,2300,2012,'',0,0,3,1,'2016-05-31 15:21:35',0,8,NULL,5000,'Installation and demo for this product is done free of cost as part of this purchase. Product is in excellent condition without any issues. Sparingly used. Product comes with plastic covering.\nPocketin Quality check done and quality score included.','Metal\nKnock Down\nAdjustable Height: Yes\nSeat Lock Included:	Yes\nWheels Included: Yes\nLean Back: Yes\nLean Lock: Yes\nSuitable For: Study & Home Office\nArmrest Included: Yes',0,'Sparingly used office chair in very good condition\nNo noticeable or unnoticeable defects\nHeight adjustment, lean back, and wheels working perfectly.',1),
	(36,0,'Office Chair height adjustable','Adjustable back seat and rolling, lean back with lean lock',0,1,2300,2012,'',0,0,3,1,'2016-05-31 09:05:58',0,8,NULL,5000,'Installation and demo for this product is done free of cost as part of this purchase. Product is in excellent condition without any issues. Sparingly used. Product comes with plastic covering.\nPocketin Quality check done and quality score included.','Metal\nKnock Down\nAdjustable Height: Yes\nSeat Lock Included:	Yes\nWheels Included: Yes\nLean Back: Yes\nLean Lock: Yes\nSuitable For: Study & Home Office\nArmrest Included: Yes',0,'Sparingly used office chair in very good condition\nNo noticeable or unnoticeable defects\nHeight adjustment, lean back, and wheels working perfectly.',1),
	(37,0,'Office Chair height adjustable','Adjustable back seat and rolling, lean back with lean lock',0,1,2300,2012,'',0,0,3,1,'2016-05-31 09:05:59',0,8,NULL,5000,'Installation and demo for this product is done free of cost as part of this purchase. Product is in excellent condition without any issues. Sparingly used. Product comes with plastic covering.\nPocketin Quality check done and quality score included.','Metal\nKnock Down\nAdjustable Height: Yes\nSeat Lock Included:	Yes\nWheels Included: Yes\nLean Back: Yes\nLean Lock: Yes\nSuitable For: Study & Home Office\nArmrest Included: Yes',0,'Sparingly used office chair in very good condition\nNo noticeable or unnoticeable defects\nHeight adjustment, lean back, and wheels working perfectly.',1),
	(38,0,'Office Chair height adjustable','Adjustable back seat and rolling, lean back with lean lock',0,1,2300,2012,'',0,0,3,1,'2016-05-31 09:06:00',0,8,NULL,5000,'Installation and demo for this product is done free of cost as part of this purchase. Product is in excellent condition without any issues. Sparingly used. Product comes with plastic covering.\nPocketin Quality check done and quality score included.','Metal\nKnock Down\nAdjustable Height: Yes\nSeat Lock Included:	Yes\nWheels Included: Yes\nLean Back: Yes\nLean Lock: Yes\nSuitable For: Study & Home Office\nArmrest Included: Yes',0,'Sparingly used office chair in very good condition\nNo noticeable or unnoticeable defects\nHeight adjustment, lean back, and wheels working perfectly.',1),
	(39,0,'Office Chair height adjustable','Adjustable back seat and rolling, lean back with lean lock',0,1,2300,2012,'',0,0,3,1,'2016-05-31 09:06:00',0,8,NULL,5000,'Installation and demo for this product is done free of cost as part of this purchase. Product is in excellent condition without any issues. Sparingly used. Product comes with plastic covering.\nPocketin Quality check done and quality score included.','Metal\nKnock Down\nAdjustable Height: Yes\nSeat Lock Included:	Yes\nWheels Included: Yes\nLean Back: Yes\nLean Lock: Yes\nSuitable For: Study & Home Office\nArmrest Included: Yes',0,'Sparingly used office chair in very good condition\nNo noticeable or unnoticeable defects\nHeight adjustment, lean back, and wheels working perfectly.',1),
	(40,0,'Office Chair height adjustable','Adjustable back seat and rolling, lean back with lean lock',0,1,2300,2012,'',0,0,3,1,'2016-05-31 09:06:01',0,8,NULL,5000,'Installation and demo for this product is done free of cost as part of this purchase. Product is in excellent condition without any issues. Sparingly used. Product comes with plastic covering.\nPocketin Quality check done and quality score included.','Metal\nKnock Down\nAdjustable Height: Yes\nSeat Lock Included:	Yes\nWheels Included: Yes\nLean Back: Yes\nLean Lock: Yes\nSuitable For: Study & Home Office\nArmrest Included: Yes',0,'Sparingly used office chair in very good condition\nNo noticeable or unnoticeable defects\nHeight adjustment, lean back, and wheels working perfectly.',1),
	(41,0,'Office Chair height adjustable','Adjustable back seat and rolling, lean back with lean lock',0,1,2300,2012,'',0,0,3,1,'2016-05-31 09:06:02',0,8,NULL,5000,'Installation and demo for this product is done free of cost as part of this purchase. Product is in excellent condition without any issues. Sparingly used. Product comes with plastic covering.\nPocketin Quality check done and quality score included.','Metal\nKnock Down\nAdjustable Height: Yes\nSeat Lock Included:	Yes\nWheels Included: Yes\nLean Back: Yes\nLean Lock: Yes\nSuitable For: Study & Home Office\nArmrest Included: Yes',0,'Sparingly used office chair in very good condition\nNo noticeable or unnoticeable defects\nHeight adjustment, lean back, and wheels working perfectly.',1),
	(42,0,'Office Chair height adjustable','Adjustable back seat and rolling, lean back with lean lock',0,1,2300,2012,'',0,0,3,1,'2016-05-31 09:06:04',0,8,NULL,5000,'Installation and demo for this product is done free of cost as part of this purchase. Product is in excellent condition without any issues. Sparingly used. Product comes with plastic covering.\nPocketin Quality check done and quality score included.','Metal\nKnock Down\nAdjustable Height: Yes\nSeat Lock Included:	Yes\nWheels Included: Yes\nLean Back: Yes\nLean Lock: Yes\nSuitable For: Study & Home Office\nArmrest Included: Yes',0,'Sparingly used office chair in very good condition\nNo noticeable or unnoticeable defects\nHeight adjustment, lean back, and wheels working perfectly.',1),
	(43,0,'Office Chair height adjustable','Adjustable back seat and rolling, lean back with lean lock',0,1,2300,2012,'',0,0,3,1,'2016-05-31 09:06:05',0,8,NULL,5000,'Installation and demo for this product is done free of cost as part of this purchase. Product is in excellent condition without any issues. Sparingly used. Product comes with plastic covering.\nPocketin Quality check done and quality score included.','Metal\nKnock Down\nAdjustable Height: Yes\nSeat Lock Included:	Yes\nWheels Included: Yes\nLean Back: Yes\nLean Lock: Yes\nSuitable For: Study & Home Office\nArmrest Included: Yes',0,'Sparingly used office chair in very good condition\nNo noticeable or unnoticeable defects\nHeight adjustment, lean back, and wheels working perfectly.',1),
	(44,0,'Office Chair height adjustable','Adjustable back seat and rolling, lean back with lean lock',0,1,2300,2012,'',0,0,3,1,'2016-05-31 09:06:06',0,8,NULL,5000,'Installation and demo for this product is done free of cost as part of this purchase. Product is in excellent condition without any issues. Sparingly used. Product comes with plastic covering.\nPocketin Quality check done and quality score included.','Metal\nKnock Down\nAdjustable Height: Yes\nSeat Lock Included:	Yes\nWheels Included: Yes\nLean Back: Yes\nLean Lock: Yes\nSuitable For: Study & Home Office\nArmrest Included: Yes',0,'Sparingly used office chair in very good condition\nNo noticeable or unnoticeable defects\nHeight adjustment, lean back, and wheels working perfectly.',1),
	(45,0,'Office Chair Height adjustable','Adjustable back seat, Rolling wheels, lean back with lean lock',0,1,3000,2013,'',0,0,3,1,'2016-05-31 09:06:07',0,8,NULL,6000,'Installation and demo for this product is done free of cost as part of this purchase. Product is in excellent condition without any issues. Sparingly used. Product comes with plastic covering.\nPocketin Quality check done and quality score included.','Metal\nKnock Down\nAdjustable Height: Yes\nSeat Lock Included:	Yes\nWheels Included: Yes\nLean Back: Yes\nLean Lock: Yes\nSuitable For: Study & Home Office\nArmrest Included: Yes',0,'Sparingly used office chair in very good condition\nNo noticeable or unnoticeable defects\nHeight adjustment, lean back, and wheels working perfectly.',1),
	(46,0,'Office Chair Height adjustable','Adjustable back seat, Rolling wheels, lean back with lean lock',0,1,3000,2013,'',0,0,3,1,'2016-05-31 09:06:08',0,8,NULL,6000,'Installation and demo for this product is done free of cost as part of this purchase. Product is in excellent condition without any issues. Sparingly used. Product comes with plastic covering.\nPocketin Quality check done and quality score included.','Metal\nKnock Down\nAdjustable Height: Yes\nSeat Lock Included:	Yes\nWheels Included: Yes\nLean Back: Yes\nLean Lock: Yes\nSuitable For: Study & Home Office\nArmrest Included: Yes',0,'Sparingly used office chair in very good condition\nNo noticeable or unnoticeable defects\nHeight adjustment, lean back, and wheels working perfectly..',1),
	(47,0,'Office Chair Height adjustable','Adjustable back seat, Rolling wheels, lean back with lean lock',0,1,3000,2013,'',0,0,3,1,'2016-05-31 09:06:09',0,8,NULL,6000,'Installation and demo for this product is done free of cost as part of this purchase. Product is in excellent condition without any issues. Sparingly used. Product comes with plastic covering.\nPocketin Quality check done and quality score included.','Metal\nKnock Down\nAdjustable Height: Yes\nSeat Lock Included:	Yes\nWheels Included: Yes\nLean Back: Yes\nLean Lock: Yes\nSuitable For: Study & Home Office\nArmrest Included: Yes',0,'Sparingly used office chair in very good condition\nNo noticeable or unnoticeable defects\nHeight adjustment, lean back, and wheels working perfectly..',1),
	(48,0,'Office Chair Height adjustable','Adjustable back seat, Rolling wheels, lean back with lean lock',0,1,3000,2013,'',0,0,3,1,'2016-05-31 09:06:10',0,8,NULL,6000,'Installation and demo for this product is done free of cost as part of this purchase. Product is in excellent condition without any issues. Sparingly used. Product comes with plastic covering. Pocketin Quality check done and quality score included.','Metal\n\nKnock Down\n\nAdjustable Height: Yes\n\nSeat Lock Included:	Yes\n\nWheels Included: Yes\n\nLean Back: Yes\n\nLean Lock: Yes\n\nSuitable For: Study & Home Office\n\nArmrest Included: Yes',0,'Sparingly used office chair in very good condition\n\nNo noticeable or unnoticeable defects\n\nHeight adjustment, lean back, and wheels working perfectly..',1),
	(49,0,'Office Chair Height adjustable','Adjustable back seat, Rolling wheels, lean back with lean lock',0,1,3000,2013,'',0,0,3,1,'2016-05-31 09:06:11',0,8,NULL,6000,'Installation and demo for this product is done free of cost as part of this purchase. Product is in excellent condition without any issues. Sparingly used. Product comes with plastic covering. Pocketin Quality check done and quality score included.','Metal\n\nKnock Down\n\nAdjustable Height: Yes\n\nSeat Lock Included:	Yes\n\nWheels Included: Yes\n\nLean Back: Yes\n\nLean Lock: Yes\n\nSuitable For: Study & Home Office\n\nArmrest Included: Yes',0,'Sparingly used office chair in very good condition\n\nNo noticeable or unnoticeable defects\n\nHeight adjustment, lean back, and wheels working perfectly..',1),
	(50,0,'Office Chair Height adjustable','Adjustable back seat, Rolling wheels, lean back with lean lock',0,1,3000,2013,'',0,0,3,1,'2016-05-31 09:06:12',0,8,NULL,6000,'Installation and demo for this product is done free of cost as part of this purchase. Product is in excellent condition without any issues. Sparingly used. Product comes with plastic covering. Pocketin Quality check done and quality score included.','Metal\n\nKnock Down\n\nAdjustable Height: Yes\n\nSeat Lock Included:	Yes\n\nWheels Included: Yes\n\nLean Back: Yes\n\nLean Lock: Yes\n\nSuitable For: Study & Home Office\n\nArmrest Included: YesMetal\n\nKnock Down\n\nAdjustable Height: Yes\n\nSeat Lock Included:	Yes\n\nWheels Included: Yes\n\nLean Back: Yes\n\nLean Lock: Yes\n\nSuitable For: Study & Home Office\n\nArmrest Included: Yes',0,'Sparingly used office chair in very good condition\n\nNo noticeable or unnoticeable defects\n\nHeight adjustment, lean back, and wheels working perfectly..',1),
	(51,0,'Office Chair Height adjustable','Adjustable back seat, Rolling wheels, lean back with lean lock',0,1,3000,2013,'',0,0,3,1,'2016-05-31 09:06:13',0,8,NULL,6000,'Installation and demo for this product is done free of cost as part of this purchase. Product is in excellent condition without any issues. Sparingly used. Product comes with plastic covering. Pocketin Quality check done and quality score included.','Metal\n\nKnock Down\n\nAdjustable Height: Yes\n\nSeat Lock Included:	Yes\n\nWheels Included: Yes\n\nLean Back: Yes\n\nLean Lock: Yes\n\nSuitable For: Study & Home Office\n\nArmrest Included: Yes',0,'Sparingly used office chair in very good condition\n\nNo noticeable or unnoticeable defects\n\nHeight adjustment, lean back, and wheels working perfectly..',1),
	(52,0,'Office Chair Height adjustable','Adjustable back seat, Rolling wheels, lean back with lean lock',0,1,3000,2013,'',0,0,3,1,'2016-05-31 09:06:13',0,8,NULL,6000,'Installation and demo for this product is done free of cost as part of this purchase. Product is in excellent condition without any issues. Sparingly used. Product comes with plastic covering. Pocketin Quality check done and quality score included.','Metal\n\nKnock Down\n\nAdjustable Height: Yes\n\nSeat Lock Included:	Yes\n\nWheels Included: Yes\n\nLean Back: Yes\n\nLean Lock: Yes\n\nSuitable For: Study & Home Office\n\nArmrest Included: Yes',0,'Sparingly used office chair in very good condition\n\nNo noticeable or unnoticeable defects\n\nHeight adjustment, lean back, and wheels working perfectly..',1),
	(53,0,'Office Chair Height adjustable','Adjustable back seat, Rolling wheels, lean back with lean lock',0,1,3000,2013,'',0,0,3,1,'2016-05-31 09:07:16',0,8,NULL,6000,'Installation and demo for this product is done free of cost as part of this purchase. Product is in excellent condition without any issues. Sparingly used. Product comes with plastic covering. Pocketin Quality check done and quality score included.','Metal\n\nKnock Down\n\nAdjustable Height: Yes\n\nSeat Lock Included:	Yes\n\nWheels Included: Yes\n\nLean Back: Yes\n\nLean Lock: Yes\n\nSuitable For: Study & Home Office\n\nArmrest Included: Yes',0,'Sparingly used office chair in very good condition\n\nNo noticeable or unnoticeable defects\n\nHeight adjustment, lean back, and wheels working perfectly..',1),
	(54,0,'Office Chair Height adjustable','Adjustable back seat, Rolling wheels, lean back with lean lock',0,1,3000,2013,'',0,0,3,1,'2016-05-31 15:22:33',0,8,NULL,6000,'Installation and demo for this product is done free of cost as part of this purchase. Product is in excellent condition without any issues. Sparingly used. Product comes with plastic covering. Pocketin Quality check done and quality score included.','Metal\n\nKnock Down\n\nAdjustable Height: Yes\n\nSeat Lock Included:	Yes\n\nWheels Included: Yes\n\nLean Back: Yes\n\nLean Lock: Yes\n\nSuitable For: Study & Home Office\n\nArmrest Included: Yes',0,'Sparingly used office chair in very good condition\n\nNo noticeable or unnoticeable defects\n\nHeight adjustment, lean back, and wheels working perfectly..',1),
	(55,0,'Whirlpool washing machine white magic FSAH651','Fully Automatic top loading 6.5 Kg',0,1,6200,2013,'',0,0,1,1,'2016-07-16 07:31:19',0,4,NULL,19475,'Now remove 6 different types of stains in one wash! Whirlpool FSAH651 is loaded with an In-built Heater that heats up water to optimum temperature level required to facilitate the best enzyme action from the detergent','Capacity: 6.5 Kg\n575 W Powerful Wash Motor\n1-2,1-2 Hand Wash (Agitator)\nHot Wash\nStain Wash\nZPF technology',0,'Minor scratches in the front side at bottom',0),
	(56,0,'Samsung washing machine WA78K1','Fully Automatic top loading 5.8 Kg',0,1,5800,2012,'',0,0,1,1,'2016-07-16 07:31:19',0,4,NULL,15500,'This 5.8 kg top loading fully automatic washing machine with pulsator wash has 2 preset programmes. The machine allows prewash (soak) and quick wash. It also has additional features like washload sensor, automatic drainage, auto restart and child lock. It has a foldable top lid cover.','Washing programs: 2\nCapacity: 5.8 Kg\nControl Type: Digital Fuzzy logic\nType: Fully Automatic\nLoading type: Top',0,'Minor scratches',0),
	(57,0,'LG washing machine fuzzynova WF-S7012DN','Fully Automatic top loading 6 Kg',0,1,6100,2012,'',0,0,1,1,'2016-07-16 07:31:20',0,4,NULL,14490,'Digital Display: Indicates the left time, error message & process. 3 Step Washing makes the best washing performance with low tangling and is done in three steps. It gives the advantage of hand wash with best use of detergent. Fuzzy Logic Control: A built in load sensor automatically detects the laundry load and a microprocessor optimizes washing conditions such as ideal water level and washing time. You can set the Wash Program type at Fuzzy and it would sense the appropriate washing method required to give you the best washing performance. Turbodrum: The unique technology that knows how to take care of your clothes while giving you the perfect wash. The dual direction rotating drum reduces tangling of clothes which ensures your clothes stay as fresh and new for much longer than ever before. The machine also is now less susceptible to wear and tear giving you a longer life. Turbodrum allows the movement of the drum in the opposite direction of the pulsator, for a doubly-efficient wash action.','Capacity: 6 Kg\nLoading type: Top\nNo.of wash programs: 8\nFeature: Child lock, Stainless steel, Automatic water control, 3 step wash, water level 10, Turbodrum',0,'Minor scratches in the top lid',0),
	(58,0,'Whirlpool washing machine whitemagic 650h','Fully Automatic top loading 6.5kg',0,1,6600,2013,'',0,0,1,1,'2016-07-16 07:31:20',0,4,NULL,14345,'Top Loading Fully Auto Frosted Grey Washing Machine features which makes it very easy for you to use, operate and control the washing machine to your convenience. The machine gives you the convenience to wash different kind of loads according to the fabric type i.e. delicate, normal, speedy or heavy. It features an Agipeller Wash system which ensures that the clothes do not get tangled even while washing.','Capacity: 6.5 Kg\nLoading type: Top\nNumber of wash programs: 9\nAutomatic water control\nWash method: Agipeller\nEasy-to-Use Control Pannel, 6th Sense-For Resource Optimisation, In-built Heater-For, Stain Wash-For Removing 6 types of Stubborn Stains',0,'Minor scratches in the top and front side\nMinor dent in the back side',0),
	(59,0,'Onida-Hydrosmart Washing Machine 70N','Fully Automatic top loading 7 Kg',0,1,5200,2011,'',0,0,1,1,'2016-07-16 07:31:20',0,4,NULL,19300,'Active Soak Timer: Active Soak function is for those stubborn stains that cannot be removed with regular washing. It allows the clothes to be immersed in the detergent infused water and sends quivers through the wash to attack the deeper dirt. Fuzzy Logic: With Fuzzy Logic control your machine will automatically decide the cycle time and water level needed based on the load of wash you put in and the temperature of water you choose. It will also indicate the required detergent level. Tub Clean: This feature removes the toxic substances and germs which are hidden in the finer places of your tub. It releases water in your tub while the spin speed is high to remove the residual slime & dirt and give you a germ free wash every time. Magic Filter: This strategically placed box type magic filter makes excess lint on your clothes history. It is positioned on the lower level of the tub which means that even in small wash loads the fluff would be captured and your clothes would be lint free. 400 Watt Motor: 400 Watts Powerful Motor to give your clothes a stronger and a cleaner wash.','Capacity 7 Kg\nLoading type: Top\nSpin speed: 800 RPM\nNumber of wash programs: 8\nControl type: Push button\nDimension (wxdxh): 545 x 560 x 912 mm\nBasket Material : Steel',0,'Scratches in the top lid and in the sides\nMinor skin peel off',0),
	(60,0,'Godrej washing machine GWF 650 FDC','Fully Automatic top loading 6.5 Kg',0,1,6800,2012,'',0,0,1,1,'2016-07-16 07:31:20',0,4,NULL,19000,'This top loading washing machine renders washing clothes easier than ever. It saves water as well as energy, saving you money in return. It also makes drying clothes fast and easy. It has complete electronic controls so you can just switch it on while the washing machine takes care of the rest. It sports a modish body that blends in easily in any modern home. It has separate wash functions as per your requirements. It is sturdy and durable.','Capacity: 6.5 Kg\nNo.of soaks: 2\nNo.of wash programs: 8\nDimension (wxdxh): 560 x 555 x 920\nSleep mode: Yes\nDAC+Technology,Fuzzy logic,Water optimizer,Spin free mode,Save settings,Stainless steel drum',0,'Minor scratches in the front and top lid',0),
	(61,0,'Godrej refrigerator GDE 19 B2','Semi automatic Single door 183 litres',0,1,7000,2014,'',0,0,1,1,'2016-07-16 07:31:21',0,3,NULL,14000,'This 183 litres, single door refrigerator which has a 37 litres freezer capacity, has a semi automatic defrost system with a door lock. It has 7 shelves and boxes, which are adjustable. It has moisture and humidity control. This refrigerator comes with ZOP Technology which uses the process of galvanization for preventing rust. This galvanization process combined with paint coating gives double protection against rust formation. It gives stabiliser free operation (160-250V). This refrigerator comes in brushed steel colour. It is form the EDGE series. It has 5 star energy rating.','Capacity: 183 litres\nDefrosting type: Semi Automatic\nMaterial used for shelf: Acrylic\nDoor lock',0,'Minor black stains in the bottom',0),
	(62,0,'LG refrigerator GR-181TPLX','Single door 175 litre',0,1,6600,2012,'',0,0,1,1,'2016-07-16 07:31:21',0,3,NULL,15500,'Z-Deodorizer\nMoist Balance™ \nCrisperMulti Air Flow','Capacity: 175 litre\nBio Shield: yes\nNo frost: yes\nDimension (WxDxH cms):54x55x110\nDisplay Design: None\nMulti Air flow: Yes',0,'Minor scratch on sides and usage signs',0),
	(63,0,'Whirlpool Direct Cool 190L','Flower graphics, Toughened Glass Shelf, Energy efficient',0,1,8200,2012,'',0,0,1,1,'2016-07-16 07:31:21',0,3,NULL,16391,'Aesthetically designed to survive any form of spills with the advantage of easy clean up, the toughened glass shelves can withstand weight up to 90kgs. Retains cooling through a specially designed cooling zone for up to 12 hours so that you can enjoy your drinks chilled even during power cuts','5 Star Rating\nHighest energy efficiency rating awarded by BEE makes this refrigerator an earning member of your family by saving electricity.\n\nToughened Glass Shelf\nAesthetically designed to survive any form of spills with the advantage of easy clean up, the toughened glass shelves can withstand weight up to 90Kgs. So that you can store heavy utensils without a worry!\n\nPowercool zone\nRetains cooling through a specially designed cooling zone, for up to 12 hours, so that you can enjoy your drinks chilled even during power cuts.\n\nHoney Comb Veg Crisper\nKeeps vegetables and fruits fresher for longer through a unique and innovative honeycomb-patterned crisper cover that retains humidity.\n\nDry Storage\nAdditional 10L of day storage space for non-refrigerated item like onions, potatoes, etc. Ensure not only hygiene but also increases the height of the refrigerator.\n\nSuper Quick Ice\nPowerful compressor and advanced cooling system makes ice super quick\n\nAdvanced Air Flow\nSpecially designed air vents over the shelves ensure superior cooling in every corner.\n\nBrand	Whirlpool\nCapacity	190 Liters\nEnergy Efficiency	5 Star Rating\nInstallation Type	Free-standing\nForm Factor	Standard Single Door',0,'Minor scratches on left side of the door(Refer pic)\nMinor scratches on sides\nMinor dents on sides(Refer pic)\nNo serious dents and scratches throughout the body\nFreezer door was changed with a new door.(Refer pic)',0),
	(64,0,'Samsung Double Door 345 Litre','Four Star Rating, Twist Ice Tray, Cool Pack, Stabilizer free operation',0,1,12300,2007,'',0,0,2,1,'2016-07-16 07:31:21',0,3,NULL,33100,'Samsung Double door in excellent condition with new Door Seal, Cool Pad for after power shutdown cooling. It comes with twistable ice tray. Dual Fan technology, Power Freeze, Cooling even at 55 degree, Easy slide shelf, Power Cool, Toughen Glass Trays,Big bottle guard','Type	Refrigerator/Freezer\nControl Features	Electromechanical\nInstallation	Freestanding\nFreezer Position	Top\nDimensions\nWidth	25.5 inch (65 cm)\nHeight	63.9 inch (163 cm)\nDepth	23.5 inch (60 cm)\nRefrigerator or Freezer Features\nNumber of Doors	2\nNumber of Chambers	2\nClimatic Class	T (tropical)\nFreezer Defrost Type	No Frost\nRefrigerator Defrost Type	Auto Cycle Defrost\nMinimal Freezing Temperature	-18 °C (-0 °F)\nFreezing Capacity	22 lbs a day (10 kg a day)\nNumber of Compressors	1\nFresh Zone Capacity	0.4 cu.ft. (10 L)\nFeatures\nemperature Indicator	No\nSuper Freeze Feature	Yes\nAntibacterial Coating	No\nShelves Material	Metal\nNoise Level	42 dB\nStailness Steel Body	No\nCapacity\nTotal Net Capacity	10.9 cu.ft. (310 L)\nFreeezer Net Capacity	2.8 cu.ft. (78 L)\nRefrigerator Net Capacity	7.8 cu.ft. (222 L)\nMiscellaneous\nReversible Door	Yes\nIce Maker	No',0,'Few Scratches on Side\nA Minor crack on freezer',0),
	(65,0,'Samsung Single Door 180L','Single door, Stabilizer free, Rust resistant body',0,1,6700,2012,'',0,0,2,1,'2016-07-16 07:31:21',0,3,NULL,14000,'This 180 litre, single door refrigerator which has a 18 litre freezer capacity with semi automatic defrost system. It has 11 shelves and boxes which are adjustable and clean back. This refrigerator offers stabilizer free operation (135-290v). It has egg guard, anti-fungal door gasket, and rust resistant body.','Capacity  (Litres)	180\nNumber of Doors 	1\nDoor Type 	Single\nDefrosting Type 	Semi Automatic\nChiller Tray Yes',0,'It is in excellent condition without any major scratches\nFreezer ice tray has been replaced with new one.',0),
	(66,0,'Whirlpool Double Door Five Star','Whirlpool Double Door in Excellent Condition',0,1,8500,2012,'',0,0,1,1,'2016-07-16 07:31:22',0,3,NULL,16000,'Whirlpool Double Door in Excellent Condition','Whirlpool Double Door in Excellent Condition',0,'Without any issues',0),
	(67,0,'Godrej 185Ltr Direct Cool Single Door','Dual Color, FM Radio, Toughened Glass, Five Star',0,1,8550,2012,'',0,0,2,1,'2016-07-16 07:31:24',0,3,NULL,16900,'This product is still in market and its one of the latest models from Godrej. This has FM with dual speakers. It has bottom tray to keep onion and potato. This comes with steel finish Silver with Maroon dual tone. Product is in excellent condition','Type	Single Door\nCapacity	185 litres\nDoors	1\nDefrosting Type	Direct Cool\nShelves	2\nShelf Material	Toughened Glass\nAdditional Features	Finish: Powder Coated / Metallic, Recessed Lamp, 2.5 L AquaSpace, Environment Friendly\nCooling Technology	Direct Cool\nOther Convenience Features	Toughened Glass Vegetable Tray Cover, Jumbo Vegetable Tray, Bottle Snugger\nStar Rating	5 star',0,'This does not have any major scratches on body\nFreezer covering is slightly broken but no major inconvenience. This can be replaced for additional 650 Rs if requested.',0),
	(68,0,'Samsung Fully Automatic Top Load (5.8Kg)','Four Fixed wash programs, Digital Control Board, Advanced wash technology',0,1,5500,2012,'',0,0,2,1,'2016-07-16 07:31:25',0,4,NULL,12500,'This Samsung top loading washing machine is a powerful washing machine that offers the smart wash feature. It incorporates the 3D dynamic wash technology and this guarantees efficient cleaning. This Samsung washing machine reduces water usage, and saves you tons of money. Once the rinsing is done, the machine stores the water and reuses it during the next wash. This comes with magic filter and advanced wash technology','General\nType of Washing Machine	Top Loading\nWash Capacity (Kg)	5.8 Kg\nColor	White\nWash Method	Pulsator\nPulsator Type	Jet Pulsator\nBody\nControls	Fuzzy Logic\nWindow	Transparent\nWash Features/Modes\nMultiple Wash Programs	Yes\nNumber of Preset Programmes/ Wash Courses	9\nPreset Wash Programs	Normal,Heavy,Delicate,Quick,Sari\nPrewash/ Soak	Yes\nOne Touch Wash	Yes\nOdour Free Clothes	Yes\nConvenience Features\nAuto Power Off	Yes\nAuto Restart	Yes\nAutomatic Drainage	Yes\nAutomatic Water Control	Yes\nTimers	Yes\nWater level Selection	4',0,'External appearance is good without any major scratches or dents\nInternal drum condition is excellent\nMotor Condition is excellent\nNo noticeable issues and its a sure buy.',0),
	(69,0,'Samsung Single Door 180L','Single door, Stabilizer free, Rust resistant body',0,1,6900,2013,'',0,0,1,1,'2016-07-16 07:31:25',0,3,NULL,14000,'This 180 litre, single door refrigerator which has a 18 litre freezer capacity with semi automatic defrost system. It has 11 shelves and boxes which are adjustable and clean back. This refrigerator offers stabilizer free operation (135-290v). It has egg guard, anti-fungal door gasket, and rust resistant body.','Capacity (Litres)	180\n\nNumber of Doors 1\n\nDoor Type Single\n\nDefrosting Type Semi Automatic\n\nChiller Tray Yes',0,'It is in excellent condition without any major scratches',0),
	(70,0,'Godrej Champion 170L','Godrej Champion 170L Single Door White color',0,1,5200,2012,'',0,0,1,1,'2016-07-16 07:31:25',0,3,NULL,13000,'Single Door White in color 170L Excellent condition. Quality checked by Pocketin and quality score included.','Door: Single\nCapacity : 170L',0,'Minor scratches of usage\nPaint is gone from bottom left side of the door which is visible in the picture',0),
	(71,0,'Samsung Double Door','Double Door, Four Star, Silver Color',0,1,8000,2012,'',0,0,119,1,'2016-07-16 07:31:25',0,3,NULL,17000,'In Good working condition. Quality tested with Pocketin in with quality score','Capacit: 230L\nColor: Silver\nType: Double Door',0,'Minor Scratches of usage\nMinor dents on sides and door',0),
	(72,0,'IFB Front Load Serena 5.5Kg','IFB 5.5 kg Fully Automatic Front Load Washing Machine',0,1,8300,2013,'',0,0,2,1,'2016-07-16 07:31:26',0,4,NULL,22000,'This is a special in-built aqua filter that treats hard water so as to improve the performance and life your washing machine. The filter breaks the bi-carbonates in water and reduces them to crystals. This makes the water soft and allows the detergent enzymes to dissolve better in the water for a more efficient wash.','Memory Backup	Yes\nDisplay Features	Program Time and Progress Indication\nOther Convenience Features	Self Diagnosis, Temperature Options (Hot, Program Time Indication, Cold, Warm)\nDigital Display	No\n\nWater Consumption	45 L\nBrand	IFB\nShade	White\nWash Cycle Duration	151 min\nType	Washing Machine\nWashing Method	Tumble Wash\nWashing Capacity	5.5 kg\nModel Name	Serena\nFunction Type	Fully Automatic Front Load\nIn-built Heater	Yes\nTechnology Used	3D Wash System\nMaximum Spin Speed	800 rpm',0,'Appearance: No major scratches or dents, Few minor marks of regular usage\nComes with new inlet and outlet pipes',0),
	(73,0,'Electrolux 235L Double Door','Electrolux-ERP 245 FF Premium Double Door',0,1,6300,2012,'',0,0,2,1,'2016-07-16 07:31:26',0,3,NULL,13200,'This 235 litres 2 door refrigerator has 70 litres is frost free has a clean back. It has 9 shelves and boxes, which are adjustable and it has a door lock. It has other facilities like moisture and humidity control and a deodourizer.The quick freezing component helps in faster freezing of food items kept in the freezer. It is available in steel, silver, white, blue, violet and red finish.','Number of Doors	2\nCapacity (Lts)	235\nFreezer Capacity (litres)	70\nDefrost system	automatic\nInsulation	Maxi 2\nCooling Technology	Multi flow',0,'The compressor condition is excellent and cooling and defrosting is excellent\nAppearance average with scratches of usage\nVegetable tray small crack is there',0),
	(74,0,'Samsung Top Load  Fully Automatic 6Kg','Fully Automatic washing machine with Pulsator Wash and 8 preset program',0,1,6300,2012,'',0,0,2,1,'2016-07-16 07:31:26',0,4,NULL,12400,'This 6 kg top loading fully automatic washing machine with pulsator wash has 8 preset programmes. The machine allows prewash (soak), heavy wash, normal wash, delicate wash and quick wash. It also has additional features like washload sensor, automatic drainage, auto restart and child lock. It has a water saving tub and air turbo drying function. It has a light grey body and lid.','Wash Method	Dispensers	\nWidth	560 mm\nWash Load	6.0 Kg\nType of Washing Machine	Fully Automatic\nControl Type	Fuzzy\nColors	Light grey\nBasket Material	Plastic\nPrice Range	Rs 10,000\nWashing Programmes	8\nHeight (mm)	885 mm\nWater Consumption	Water saving\nDisplay Type	Digital display\nSpin RPM	680 rpm\nLoading Type	Top Loading',0,'Drum is in excellent condition and motor condition is also excellent\nNormal Usage marks are there over the body\nNew inlet and outlet pipes are included',0),
	(75,0,'Kenstar 185L with water dispenser','Kenstar Single Door with water displenser',0,1,5900,2010,'',0,0,2,1,'2016-07-16 07:31:26',0,3,NULL,13000,'Water dispenser working perfectly without leakage. Condenser Condition is excellent with excellent defrosting','Capacity: 185L\nType: Single Door',0,'Minor usage marks\nFreezer door is missing and can be replaced for addition 300 rs',0),
	(76,0,'LG Single Door 180L','Single Door in good condition',0,1,5600,2012,'',0,0,2,1,'2016-07-16 07:31:27',0,3,NULL,13400,'In very good condition, compressor and cooling excellent','Capacity: 180L\nType: Single Door',0,'Minor Usage marks and scratches are there.',0),
	(77,0,'LG Single Door 185L','Single Door 185L',0,1,8300,2014,'',0,0,2,1,'2016-07-16 07:31:27',0,3,NULL,14499,'In Excellent condition, Aged around 3 years but only a year usage. Factory display piece without any major scratches. It looks like new.','General\nBrand	LG\nModel	GL-195CLGE4\nColour	Dim Grey, Ruby Luster\nType	Single Door\nCapacity	185 litres\nDoors	1\nDefrosting Type	Direct Cool\nFeatures - Refrigerator Interiors	Egg Tray, Transparent Chiller, Transparent Veg Basket\nFeatures - Refrigerator Exteriors	Solid Door Finish, Cosmo Handle\nShelves	2\nAdditional Features	Moist Balance Crisper\nSpecial Compartments	11 L Vegetable Basket\nCooling Technology	PowerCut EverCool',0,'It looks like new. Manufacturing year is 2013 but actual usage year only around 1 year.\nIt was a factory display piece.\nCooling and compressor condition: Excellent',0),
	(78,0,'Whirlpool 6Kg fully automatic top load','6Kg Fully Automatic with preset programmes',0,1,5900,2012,'',0,0,2,1,'2016-07-16 07:31:27',0,4,NULL,14000,'In excellent condition with minor usage marks','Capacity: 6Kg\nType: Fully Automatic',0,'Excellent Motor condition and drum condition\nMinor Usage Marks Over the body',0),
	(79,0,'Whirlpool Top Load Washing Machine','Fully Automatic top loading 6.5kg',0,1,6300,2012,'',0,0,2,1,'2016-07-16 07:31:27',0,4,NULL,14345,'Top Loading Fully Auto Frosted Grey Washing Machine features which makes it very easy for you to use, operate and control the washing machine to your convenience. The machine gives you the convenience to wash different kind of loads according to the fabric type i.e. delicate, normal, speedy or heavy. It features an Agipeller Wash system which ensures that the clothes do not get tangled even while washing.','Capacity: 6.5 Kg\n\nLoading type: Top\n\nNumber of wash programs: 9\n\nAutomatic water control\n\nWash method: Agipeller\n\nEasy-to-Use Control Pannel, 6th Sense-For Resource Optimisation, In-built Heater-For, Stain Wash-For Removing 6 types of Stubborn Stains',0,'Scratches of usage are there over the body\nMotor condition and drum condition excellent',0),
	(80,0,'LG Top Load Fully Automatic','6Kg Fuzzy Logic Powerful Wash',0,1,6500,2012,'',0,0,2,1,'2016-07-16 07:31:27',0,4,NULL,14390,'The unique technology that knows how to take care of your clothes while giving you the perfect wash. The dual direction rotating drum reduces tangling of clothes which ensures your clothes stay as fresh and new for much longer than ever before. The machine also is now less susceptible to wear and tear giving you a longer life. Turbodrum allows the movement of the drum in the opposite direction of the pulsator, for a doubly-efficient wash action.','Waterfall Circulation\nImprove rinsing effect, Provide circulation of water, various and evenly strong washing currents.\n\n3-Step Wash\n3 Step Washing makes the best washing performance with low tangling and is done in three steps. It gives the advantage of hand wash with best use of detergent.',0,'Excellent condition without any major scratches',0),
	(81,0,'LG Top Load Fully Automatic','6Kg Fuzzy Logic Powerful Wash Stainless Steel Tub',0,1,6800,2012,'',0,0,2,1,'2016-07-16 07:31:28',0,4,NULL,15490,'Fully Digital. Stainless Steel Tub Durable at the core the stainless steel tub in this machine is more durable than plastic, and repels stains and odors.','Waterfall Circulation\nImprove rinsing effect, Provide circulation of water, various and evenly strong washing currents.\n\n3-Step Wash\n3 Step Washing makes the best washing performance with low tangling and is done in three steps. It gives the advantage of hand wash with best use of detergent.',0,'Excellent condition without any major scratches',0),
	(82,0,'Godrej 181L Single Door','Direct Cool Single Door Refrigerator',0,1,6200,2012,'',0,0,2,1,'2016-07-16 07:31:28',0,3,NULL,14490,'This Refrigerator offers an excellent combination of functionality and stylish look, making it a must-have for your home. This Godrej refrigerator comes with numerous advanced features that help to keep the content stored in it cool and fresh for a considerable period of time. With features like bottle snugger, humidity controller, aqua space and more, this refrigerator a worthy buy. It features a utilitarian and sleek design, which makes it an excellent addition to your kitchen.','Capacity\nThe capacity of this  Refrigerator is 181 litres. This allows you to store items of a large quantity of time ranging from containers, cooked food, vegetables, beverages, fruits to water bottles. You can also store frozen food items as well as dessert like custard, pudding and ice-cream. If you have a small or a medium-sized family, this refrigerator will be just what you need.\n\n\nStar Rating\nThe Godrej  Refrigerator comes with a 4-star rating. This rating means that it is highly utilitarian and also saves energy.\n\nDesign\nThis refrigerator is a single door model and the sleek and compact size makes it suitable for compact spaces as well as kitchen areas. It comes in a subtle shade of ivy wine and is sure to complement kitchens with both classic and modern decor. Additionally, it has jumbo vegetable box, humidity controller and bottle snugger to keep bottles. Moreover, it has a sturdy built and does not take up much floor space. Its handle is ergonomically designed and comes with a high gloss finish. It measures 667 x 1267 x 550 mm in dimension.\n\n \n\nCooling Features\nThe Refrigerator is loaded with a number of cooling features that make it utilitarian. The direct cool technology that it is engineered with allows you to enjoy cool drinks instantly. It also helps to form ice significantly fast and easy and, therefore, you will never run out of ice.\n\n\nPower Usage \nThe power consumption of this Godrej 185CTM Refrigerator is rated at 100-300 V at a frequency of 50 Hz that helps to cut down on the electricity bill. It also has the frost free feature, which makes it even more convenient to use.\n\nAdditional Features\nThis Godrej refrigerator is equipped with a number of convenient features that ensure longevity as well as quality. Some of these include SIF technology that allows the dry food items to stay fresh. Moreover, it also comes with an LED light for added convenience and also humidity controller.',0,'Compressor and defrost are in excellent condition\nMinor usage marks.',0),
	(83,0,'LG Single Door 185L','Single Door in Excellent Condition',0,1,7200,2014,'',0,0,2,1,'2016-07-16 07:31:28',0,3,NULL,14000,'In excellent condition','Capacity: 185L\nType: Single Door',0,'Looks new without any major scratches.',0),
	(84,0,'Videocon 190L Pink Color Flower Graphics','Single Door in Excellent Condition with Door Graphics',0,1,8400,2014,'',0,0,2,1,'2016-07-16 07:31:28',0,3,NULL,13434,'Five Star Stabilizer Free Operation PCM Finish Transparent Interiors Anti Bacterial Door Gasket Adjustable Shelves','Capacity  (Litres)	190\nBEE Star Rating 	5\nNumber of Doors 	1\nDoor Type 	Single',0,'In Excellent condition\nFew dents on the side of the doors which happened during trannsportation',0),
	(85,0,'Whirlpool Double Door Five Star 220L','Whirlpool Double Door in Excellent Condition Five Star',0,1,8500,2013,'',0,0,2,1,'2016-07-16 07:31:28',0,3,NULL,16000,'Whirlpool Double Door in Excellent Condition','Capacity: 220L',0,'Few Marks of usage on the body.',0),
	(86,0,'Samsung Double Door 250L Frost Free','Double Door, Frost Free, Toughened Glass',0,1,8900,2012,'',0,0,2,1,'2016-07-16 07:31:29',0,3,NULL,23000,'Double Door, Fully Automatic defrosting system, Excellent compressor\nQuality checked by Pocketin with quality Score','Capacity: 250L\nType: Double Door\nFrost Free\nToughened Glas',0,'Scratches of usage and transportation are there on the body.\nInterior looks is awesome without any broken parts.\nFew scratches on the door and a clearly visible scratch on right side of the body.',0),
	(87,0,'Samsung 190L Single Door','Single door, Stabilizer free, Rust resistant body',0,1,6950,2013,'',0,0,2,1,'2016-07-16 07:31:29',0,3,NULL,14800,'his 190 litre, single door refrigerator which has a 18 litre freezer capacity with semi automatic defrost system. It has 11 shelves and boxes which are adjustable and clean back. This refrigerator offers stabilizer free operation (135-290v). It has egg guard, anti-fungal door gasket, and rust resistant body.','Capacity (Litres)	190\n\n\nNumber of Doors 1\n\n\nDoor Type Single\n\n\nDefrosting Type Semi Automatic\n\n\nChiller Tray Yes',0,'Minor Scratches of usage on the body.\nInterior in excellent condition',0),
	(88,0,'LG single door refrigerator','LG Single Door 185L good condition',0,1,6900,2012,'',0,0,1,1,'2016-07-16 07:31:36',0,3,NULL,13200,'In very good condition, direct cool','Capacity: 185L\nType: Direct Cool',0,'Few usage marks here and there.',0);

/*!40000 ALTER TABLE `productitems` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table productlogs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `productlogs`;

CREATE TABLE `productlogs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table productrequests
# ------------------------------------------------------------

DROP TABLE IF EXISTS `productrequests`;

CREATE TABLE `productrequests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phonenumber` varchar(256) DEFAULT NULL,
  `productname` varchar(256) DEFAULT NULL,
  `model` varchar(45) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_productrequests_users1_idx` (`user_id`),
  CONSTRAINT `fk_productrequests_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table products
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `modelnumber` varchar(512) NOT NULL,
  `description` text,
  `shortdescription` varchar(512) NOT NULL,
  `instock` int(11) DEFAULT '0',
  `stockcount` int(11) DEFAULT '0',
  `origimage` varchar(256) DEFAULT NULL,
  `mrp` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;

INSERT INTO `products` (`id`, `name`, `modelnumber`, `description`, `shortdescription`, `instock`, `stockcount`, `origimage`, `mrp`)
VALUES
	(1,'Pocketin Model','PI01','Pocketin','Pocketin',1,1,NULL,9999);

/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table qualityindexes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `qualityindexes`;

CREATE TABLE `qualityindexes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(512) NOT NULL,
  `productcategory_id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `qualityindexes` WRITE;
/*!40000 ALTER TABLE `qualityindexes` DISABLE KEYS */;

INSERT INTO `qualityindexes` (`id`, `name`, `productcategory_id`, `timestamp`)
VALUES
	(1,'Age',3,'2016-03-12 10:56:38'),
	(2,'Door Seal',3,'2016-03-12 11:26:00'),
	(3,'Energy Efficiency',3,'2016-03-12 10:56:51'),
	(4,'Appearance',3,'2016-03-12 10:57:22'),
	(5,'Compressor Condition',3,'2016-05-30 10:38:03'),
	(6,'Age',4,'2016-03-12 11:00:14'),
	(7,'Full Cycle Functionality',4,'2016-03-12 11:00:53'),
	(8,'Tub/Drum Condition',4,'2016-04-04 10:33:56'),
	(9,'Efficiency Grading',4,'2016-03-12 11:00:55'),
	(10,'Appearance',4,'2016-03-12 11:00:56'),
	(12,'Age',5,'2016-03-12 11:01:12'),
	(13,'Cooling Capacity',5,'2016-03-12 11:01:21'),
	(14,'Energy Efficiency',5,'2016-03-12 11:02:48'),
	(15,'Appearance',5,'2016-03-12 11:02:50'),
	(16,'Filters/Dehumidifiers',5,'2016-04-04 10:36:31'),
	(18,'Thermostat setting',5,'2016-04-04 10:37:19'),
	(19,'Age',6,'2016-03-12 11:03:46'),
	(20,'Filter Codition',6,'2016-03-12 11:03:46'),
	(21,'Appearance',6,'2016-03-12 11:03:46'),
	(22,'Interior Parts',6,'2016-03-12 11:03:47'),
	(23,'Other',6,'2016-03-12 11:03:49'),
	(24,'Age',7,'2016-03-12 11:04:45'),
	(25,'Cushion Condition',7,'2016-03-12 11:04:46'),
	(26,'Frame/Fabric Condition',7,'2016-04-04 10:38:45'),
	(27,'Spring Condition',7,'2016-03-12 11:13:12'),
	(28,'Appearance',7,'2016-03-12 11:13:36'),
	(29,'Other',7,'2016-03-12 11:13:40'),
	(30,'Defrosting Time',3,'2016-04-04 10:34:06'),
	(31,'Noise',3,'2016-04-04 10:35:18'),
	(32,'Adjustability',8,'2016-04-04 10:39:02'),
	(33,'Seat Material',8,'2016-04-04 10:39:14'),
	(34,'Mobility',8,'2016-04-04 10:39:27'),
	(35,'Desk Shape',9,'2016-04-04 10:39:43'),
	(36,'Desk Material',9,'2016-04-04 10:40:04');

/*!40000 ALTER TABLE `qualityindexes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table qualityindexvalues
# ------------------------------------------------------------

DROP TABLE IF EXISTS `qualityindexvalues`;

CREATE TABLE `qualityindexvalues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qualityindexes_id` int(11) NOT NULL,
  `value_text` varchar(512) NOT NULL DEFAULT '',
  `value` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `qualityindexvalues` WRITE;
/*!40000 ALTER TABLE `qualityindexvalues` DISABLE KEYS */;

INSERT INTO `qualityindexvalues` (`id`, `qualityindexes_id`, `value_text`, `value`)
VALUES
	(1,1,'Around 2 years old',5),
	(2,1,'Around 3 years old',4),
	(3,1,'Around 4 years old',3),
	(4,1,'Around 6 years old',2),
	(5,1,'More than 6 years old',1),
	(6,2,'Excellent',5),
	(7,2,'Good',4),
	(8,2,'Average',3),
	(9,2,'Needs replacement after few years',2),
	(10,2,'Needs replacement in a year',1),
	(11,3,'Four Star and Above',5),
	(12,3,'Three Star',4),
	(13,3,'Above Two star',3),
	(14,3,'Below Two star',2),
	(15,3,'One star',1),
	(16,4,'Like New W/o Any Major Scratches or Dents',5),
	(17,4,'Good With minor Scratches and minor Dents',4),
	(18,4,'Average With Some Dents and Scratches',3),
	(19,4,'Some Parts are Broken',2),
	(20,4,'Looks Bad',1),
	(21,5,'Excellent Working Condition',5),
	(22,5,'Good Working Condition',4),
	(23,5,'Average Working Condition',3),
	(24,5,'Bad Working Condition',2),
	(25,5,'Not in working condition',1),
	(26,6,'Around 2 years old',5),
	(27,6,'Around 3 years old',4),
	(28,6,'Around 4 years old',3),
	(30,6,'Around 6 years old',2),
	(31,6,'More than 6 years old',1),
	(32,10,'Like New W/o Any Major Scratches or Dents',5),
	(33,10,'Good With minor Scratches and minor Dents',4),
	(34,10,'Average With Some Dents and Scratches',3),
	(35,10,'Some Parts are Broken',2),
	(36,10,'Looks Bad',1),
	(37,8,'Like New',5),
	(38,8,'Good ',4),
	(39,8,'Average',3),
	(40,8,'Bad',2),
	(41,9,'Four Star and Above',5),
	(42,9,'Three Star',4),
	(43,9,'Above Two Star',3),
	(44,9,'Two star',2),
	(45,9,'Below Two star',1),
	(50,12,'Less than 1 year old',5),
	(51,12,'1 - 2 years old',4),
	(52,12,'2 - 4 years old',3),
	(53,12,'4 - 5 years old',2),
	(54,12,'5 - 7 years old',1),
	(55,13,'Insta cooling',5),
	(56,13,'Good',4),
	(57,13,'Average',3),
	(58,13,'Poor',2),
	(59,14,'5 star',5),
	(60,14,'4 star',4),
	(61,14,'3 star',3),
	(62,14,'2 star',2),
	(63,14,'1 star',1),
	(64,15,'Like New',5),
	(65,15,'Good',4),
	(66,15,'Average',3),
	(67,15,'Bad',2),
	(68,16,'Excellent',5),
	(69,16,'Good',4),
	(70,16,'Average',3),
	(71,16,'Bad',2),
	(72,18,'Excellent Working condition',5),
	(73,18,'Good working condition',4),
	(74,18,'Average Working condition',3),
	(75,19,'Less than 1 year old',2),
	(76,20,'Excellent',5),
	(77,20,'Good',4),
	(78,20,'Average',3),
	(79,20,'Bad',2),
	(80,21,'Looks New w/o scratches/bent',5),
	(81,21,'Good',4),
	(82,21,'Average',3),
	(83,21,'Bad',2),
	(84,22,'Excellent',5),
	(85,22,'Good',4),
	(86,22,'Average',3),
	(87,22,'Bad',2),
	(88,23,'Excellent',5),
	(89,23,'Good',4),
	(90,23,'Average',3),
	(91,23,'Bad',2),
	(92,24,'Less than 1 year old',5),
	(93,24,'1 - 2 years old',4),
	(94,24,'2 - 4 years old',3),
	(95,24,'4 - 5 years old',2),
	(96,25,'Excellent',5),
	(97,25,'Good',4),
	(98,25,'Average',3),
	(99,25,'Bad',2),
	(100,26,'Excellent',5),
	(101,26,'Good',4),
	(102,26,'Average',3),
	(103,26,'Bad',2),
	(104,27,'Excellent',5),
	(105,27,'Good',4),
	(106,27,'Average',3),
	(107,27,'Bad',2),
	(108,28,'Excellent',5),
	(109,28,'Good',4),
	(110,28,'Average',3),
	(111,28,'Bad',2),
	(112,29,'Excellent',5),
	(113,29,'Good',4),
	(114,29,'Average',3),
	(115,29,'Bad',2),
	(116,30,'Fast',5),
	(117,30,'Good',4),
	(118,30,'Average',3),
	(119,30,'Bad',2),
	(120,31,'Zero Noise',5),
	(121,31,'very little noise',4),
	(122,31,'high noise',3),
	(123,32,'Easily Adjustable',5),
	(124,32,'Adjustable',4),
	(125,32,'very Tight',3),
	(126,33,'Excellent',5),
	(127,33,'Good',4),
	(128,33,'Average',3),
	(129,33,'Bad',2),
	(130,34,'Easily movable',5),
	(131,34,'Movable',4),
	(132,34,'Needs Lubrication',3),
	(133,34,'Tight',2),
	(134,35,'Excellent',5),
	(135,35,'Good',4),
	(136,35,'Average',3),
	(137,35,'Bad',2),
	(138,36,'Excellent',5),
	(139,36,'Good',4),
	(140,36,'Average',3),
	(141,36,'Bad',2),
	(142,7,'High',5),
	(143,7,'Good',4),
	(144,7,'Average',3),
	(145,7,'Bad',2),
	(146,19,'1 - 2 years old',2),
	(147,19,'2 - 4 years old',5),
	(148,19,'4 - 5 years old',2),
	(149,19,'5 - 7 years old',2),
	(150,24,'5 - 7 years old',2);

/*!40000 ALTER TABLE `qualityindexvalues` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table qualityratings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `qualityratings`;

CREATE TABLE `qualityratings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` int(11) NOT NULL,
  `qualityindex_id` int(11) NOT NULL,
  `productitem_id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `description` varchar(256) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `qualityratings` WRITE;
/*!40000 ALTER TABLE `qualityratings` DISABLE KEYS */;

INSERT INTO `qualityratings` (`id`, `value`, `qualityindex_id`, `productitem_id`, `timestamp`, `description`)
VALUES
	(1,2,6,1,'2016-05-22 16:09:47',''),
	(2,4,7,1,'2016-05-22 16:09:47',''),
	(3,4,8,1,'2016-05-22 16:09:47',''),
	(4,5,9,1,'2016-05-22 16:09:47',''),
	(5,4,10,1,'2016-05-22 16:09:47',''),
	(6,3,6,2,'2016-05-22 16:18:01',''),
	(7,4,7,2,'2016-05-22 16:18:01',''),
	(8,4,8,2,'2016-05-22 16:18:01',''),
	(9,4,9,2,'2016-05-22 16:18:01',''),
	(10,4,10,2,'2016-05-22 16:18:01',''),
	(11,2,6,3,'2016-05-22 16:23:05',''),
	(12,5,7,3,'2016-05-22 16:23:05',''),
	(13,5,8,3,'2016-05-22 16:23:05',''),
	(14,5,9,3,'2016-05-22 16:23:05',''),
	(15,5,10,3,'2016-05-22 16:23:05',''),
	(16,3,6,4,'2016-05-22 16:28:28',''),
	(17,4,7,4,'2016-05-22 16:28:28',''),
	(18,5,8,4,'2016-05-22 16:28:28',''),
	(19,4,9,4,'2016-05-22 16:28:28',''),
	(20,4,10,4,'2016-05-22 16:28:28',''),
	(21,3,6,5,'2016-05-22 16:30:37',''),
	(22,5,7,5,'2016-05-22 16:30:37',''),
	(23,5,8,5,'2016-05-22 16:30:37',''),
	(24,4,9,5,'2016-05-22 16:30:37',''),
	(25,4,10,5,'2016-05-22 16:30:37',''),
	(26,2,6,6,'2016-05-22 16:35:25',''),
	(27,5,7,6,'2016-05-22 16:35:25',''),
	(28,5,8,6,'2016-05-22 16:35:25',''),
	(29,5,9,6,'2016-05-22 16:35:25',''),
	(30,4,10,6,'2016-05-22 16:35:25',''),
	(31,5,6,7,'2016-05-22 16:42:30',''),
	(32,5,7,7,'2016-05-22 16:42:30',''),
	(33,5,8,7,'2016-05-22 16:42:30',''),
	(34,5,9,7,'2016-05-22 16:42:30',''),
	(35,5,10,7,'2016-05-22 16:42:30',''),
	(36,2,1,8,'2016-05-22 16:51:12',''),
	(37,4,2,8,'2016-05-22 16:51:12',''),
	(38,4,3,8,'2016-05-22 16:51:12',''),
	(39,4,4,8,'2016-05-22 16:51:12',''),
	(40,3,5,8,'2016-05-22 16:51:12',''),
	(41,4,30,8,'2016-05-22 16:51:12',''),
	(42,5,31,8,'2016-05-22 16:51:12',''),
	(43,5,1,9,'2016-05-22 17:02:26',''),
	(44,5,2,9,'2016-05-22 17:02:26',''),
	(45,5,3,9,'2016-05-22 17:02:26',''),
	(46,5,4,9,'2016-05-22 17:02:26',''),
	(47,5,5,9,'2016-05-22 17:02:26',''),
	(48,5,30,9,'2016-05-22 17:02:26',''),
	(49,5,31,9,'2016-05-22 17:02:26',''),
	(50,5,1,10,'2016-05-22 17:11:58',''),
	(51,5,2,10,'2016-05-22 17:11:58',''),
	(52,5,3,10,'2016-05-22 17:11:58',''),
	(53,5,4,10,'2016-05-22 17:11:58',''),
	(54,5,5,10,'2016-05-22 17:11:58',''),
	(55,5,30,10,'2016-05-22 17:11:58',''),
	(56,5,31,10,'2016-05-22 17:11:58',''),
	(57,5,1,11,'2016-05-22 17:19:24',''),
	(58,5,2,11,'2016-05-22 17:19:25',''),
	(59,5,3,11,'2016-05-22 17:19:25',''),
	(60,5,4,11,'2016-05-22 17:19:25',''),
	(61,5,5,11,'2016-05-22 17:19:25',''),
	(62,5,30,11,'2016-05-22 17:19:25',''),
	(63,5,31,11,'2016-05-22 17:19:25',''),
	(64,5,1,12,'2016-05-22 17:28:27',''),
	(65,5,2,12,'2016-05-22 17:28:27',''),
	(66,5,3,12,'2016-05-22 17:28:27',''),
	(67,5,4,12,'2016-05-22 17:28:27',''),
	(68,5,5,12,'2016-05-22 17:28:27',''),
	(69,5,30,12,'2016-05-22 17:28:27',''),
	(70,5,31,12,'2016-05-22 17:28:27',''),
	(71,1,1,13,'2016-05-23 02:33:57',''),
	(72,4,2,13,'2016-05-23 02:33:57',''),
	(73,3,3,13,'2016-05-23 02:33:57',''),
	(74,4,4,13,'2016-05-23 02:33:57',''),
	(75,4,5,13,'2016-05-23 02:33:57',''),
	(76,5,30,13,'2016-05-23 02:33:57',''),
	(77,5,31,13,'2016-05-23 02:33:57',''),
	(78,2,1,14,'2016-05-23 02:40:27',''),
	(79,4,2,14,'2016-05-23 02:40:27',''),
	(80,5,3,14,'2016-05-23 02:40:27',''),
	(81,4,4,14,'2016-05-23 02:40:27',''),
	(82,4,5,14,'2016-05-23 02:40:27',''),
	(83,5,30,14,'2016-05-23 02:40:27',''),
	(84,5,31,14,'2016-05-23 02:40:27',''),
	(85,2,1,15,'2016-05-23 02:54:12',''),
	(86,4,2,15,'2016-05-23 02:54:12',''),
	(87,4,3,15,'2016-05-23 02:54:12',''),
	(88,4,4,15,'2016-05-23 02:54:12',''),
	(89,4,5,15,'2016-05-23 02:54:12',''),
	(90,5,30,15,'2016-05-23 02:54:12',''),
	(91,5,31,15,'2016-05-23 02:54:12',''),
	(92,3,1,16,'2016-05-24 13:38:19',''),
	(93,4,2,16,'2016-05-23 03:03:00',''),
	(94,4,3,16,'2016-05-23 03:03:00',''),
	(95,4,4,16,'2016-05-23 03:03:00',''),
	(96,4,5,16,'2016-05-23 03:03:00',''),
	(97,4,30,16,'2016-05-23 03:03:00',''),
	(98,5,31,16,'2016-05-23 03:03:00',''),
	(99,3,1,17,'2016-05-23 03:10:21',''),
	(100,4,2,17,'2016-05-23 03:10:21',''),
	(101,5,3,17,'2016-05-23 03:10:21',''),
	(102,5,4,17,'2016-05-23 03:10:21',''),
	(103,5,5,17,'2016-05-23 03:10:21',''),
	(104,5,30,17,'2016-05-23 03:10:21',''),
	(105,5,31,17,'2016-05-23 03:10:21',''),
	(106,5,1,18,'2016-05-23 03:16:33',''),
	(107,5,2,18,'2016-05-23 03:16:33',''),
	(108,5,3,18,'2016-05-23 03:16:33',''),
	(109,5,4,18,'2016-05-23 03:16:33',''),
	(110,5,5,18,'2016-05-23 03:16:33',''),
	(111,5,30,18,'2016-05-23 03:16:33',''),
	(112,5,31,18,'2016-05-23 03:16:33',''),
	(113,5,1,19,'2016-05-23 03:22:59',''),
	(114,5,2,19,'2016-05-23 03:22:59',''),
	(115,5,3,19,'2016-05-23 03:22:59',''),
	(116,5,4,19,'2016-05-23 03:22:59',''),
	(117,5,5,19,'2016-05-23 03:22:59',''),
	(118,5,30,19,'2016-05-23 03:22:59',''),
	(119,5,31,19,'2016-05-23 03:22:59',''),
	(120,2,1,20,'2016-05-23 03:31:44',''),
	(121,4,2,20,'2016-05-23 03:31:44',''),
	(122,4,3,20,'2016-05-23 03:31:44',''),
	(123,4,4,20,'2016-05-23 03:31:44',''),
	(124,4,5,20,'2016-05-23 03:31:44',''),
	(125,5,30,20,'2016-05-23 03:31:44',''),
	(126,5,31,20,'2016-05-23 03:31:44',''),
	(127,2,1,21,'2016-05-31 05:13:35',''),
	(128,4,2,21,'2016-05-23 03:45:05',''),
	(129,4,3,21,'2016-05-23 03:45:06',''),
	(130,4,4,21,'2016-05-23 03:45:06',''),
	(131,4,5,21,'2016-05-23 03:45:06',''),
	(132,4,30,21,'2016-05-23 03:45:06',''),
	(133,5,31,21,'2016-05-23 03:45:06',''),
	(134,1,1,22,'2016-05-23 03:49:39',''),
	(135,4,2,22,'2016-05-23 03:49:39',''),
	(136,3,3,22,'2016-05-23 03:49:39',''),
	(137,4,4,22,'2016-05-23 03:49:39',''),
	(138,4,5,22,'2016-05-23 03:49:39',''),
	(139,4,30,22,'2016-05-23 03:49:39',''),
	(140,5,31,22,'2016-05-23 03:49:39',''),
	(141,2,1,23,'2016-05-23 03:53:49',''),
	(142,4,2,23,'2016-05-23 03:53:49',''),
	(143,4,3,23,'2016-05-23 03:53:49',''),
	(144,4,4,23,'2016-05-23 03:53:49',''),
	(145,4,5,23,'2016-05-23 03:53:49',''),
	(146,5,30,23,'2016-05-23 03:53:49',''),
	(147,5,31,23,'2016-05-23 03:53:49',''),
	(148,5,1,24,'2016-05-23 03:59:28',''),
	(149,5,2,24,'2016-05-23 03:59:28',''),
	(150,5,3,24,'2016-05-23 03:59:28',''),
	(151,5,4,24,'2016-05-23 03:59:28',''),
	(152,5,5,24,'2016-05-23 03:59:28',''),
	(153,5,30,24,'2016-05-23 03:59:28',''),
	(154,5,31,24,'2016-05-23 03:59:28',''),
	(155,2,1,25,'2016-05-23 09:19:56',''),
	(156,4,2,25,'2016-05-23 09:19:56',''),
	(157,4,3,25,'2016-05-23 09:19:56',''),
	(158,4,4,25,'2016-05-23 09:19:56',''),
	(159,4,5,25,'2016-05-23 09:19:56',''),
	(160,4,30,25,'2016-05-23 09:19:56',''),
	(161,5,31,25,'2016-05-23 09:19:56',''),
	(162,2,1,26,'2016-05-30 03:14:01',''),
	(163,4,2,26,'2016-05-23 13:48:18',''),
	(164,5,3,26,'2016-05-23 13:48:18',''),
	(165,4,4,26,'2016-05-30 03:14:29',''),
	(166,4,5,26,'2016-05-23 13:48:18',''),
	(167,5,30,26,'2016-05-23 13:48:18',''),
	(168,5,31,26,'2016-05-23 13:48:18',''),
	(169,3,1,27,'2016-05-24 07:15:32',''),
	(170,5,2,27,'2016-05-24 07:15:32',''),
	(171,5,3,27,'2016-05-24 07:15:32',''),
	(172,5,4,27,'2016-05-24 07:15:32',''),
	(173,4,5,27,'2016-05-24 07:15:32',''),
	(174,5,30,27,'2016-05-24 07:15:32',''),
	(175,5,31,27,'2016-05-24 07:15:32',''),
	(176,4,1,28,'2016-05-24 12:31:32',''),
	(177,4,2,28,'2016-05-24 12:31:32',''),
	(178,5,3,28,'2016-05-24 12:31:32',''),
	(179,5,4,28,'2016-05-24 12:31:32',''),
	(180,4,5,28,'2016-05-24 12:31:32',''),
	(181,5,30,28,'2016-05-24 12:31:32',''),
	(182,5,31,28,'2016-05-24 12:31:32',''),
	(183,4,6,29,'2016-05-24 13:01:30',''),
	(184,4,7,29,'2016-05-24 12:59:15',''),
	(185,4,8,29,'2016-05-24 12:59:15',''),
	(186,5,9,29,'2016-05-24 12:59:15',''),
	(187,4,10,29,'2016-05-24 12:59:15',''),
	(188,5,1,30,'2016-05-24 15:36:34',''),
	(189,4,2,30,'2016-05-24 15:36:34',''),
	(190,5,3,30,'2016-05-24 15:36:34',''),
	(191,4,4,30,'2016-05-24 15:36:34',''),
	(192,4,5,30,'2016-05-24 15:36:34',''),
	(193,5,30,30,'2016-05-24 15:36:34',''),
	(194,5,31,30,'2016-05-24 15:36:34',''),
	(195,4,32,31,'2016-05-26 10:33:37',''),
	(196,5,33,31,'2016-05-26 10:33:37',''),
	(197,5,34,31,'2016-05-26 10:33:37',''),
	(198,5,32,32,'2016-05-26 10:36:58',''),
	(199,5,33,32,'2016-05-26 10:36:58',''),
	(200,5,34,32,'2016-05-26 10:36:58',''),
	(201,4,6,33,'2016-05-26 11:21:20',''),
	(202,5,7,33,'2016-05-26 11:21:20',''),
	(203,5,8,33,'2016-05-26 11:21:20',''),
	(204,5,9,33,'2016-05-26 11:21:20',''),
	(205,5,10,33,'2016-05-26 11:21:20',''),
	(206,5,6,34,'2016-05-26 11:52:38',''),
	(207,5,7,34,'2016-05-26 11:52:38',''),
	(208,5,8,34,'2016-05-26 11:52:38',''),
	(209,5,9,34,'2016-05-26 11:52:38',''),
	(210,5,10,34,'2016-05-26 11:52:38',''),
	(211,4,32,35,'2016-05-27 02:33:57',''),
	(212,4,33,35,'2016-05-27 02:33:57',''),
	(213,5,34,35,'2016-05-27 02:33:57',''),
	(214,4,32,36,'2016-05-27 02:33:57',''),
	(215,4,33,36,'2016-05-27 02:33:57',''),
	(216,5,34,36,'2016-05-27 02:33:57',''),
	(217,4,32,37,'2016-05-27 02:33:57',''),
	(218,4,33,37,'2016-05-27 02:33:57',''),
	(219,5,34,37,'2016-05-27 02:33:57',''),
	(220,4,32,38,'2016-05-27 02:33:57',''),
	(221,4,33,38,'2016-05-27 02:33:57',''),
	(222,5,34,38,'2016-05-27 02:33:57',''),
	(223,4,32,39,'2016-05-27 02:33:57',''),
	(224,4,33,39,'2016-05-27 02:33:57',''),
	(225,5,34,39,'2016-05-27 02:33:57',''),
	(226,4,32,40,'2016-05-27 02:33:57',''),
	(227,4,33,40,'2016-05-27 02:33:57',''),
	(228,5,34,40,'2016-05-27 02:33:57',''),
	(229,4,32,41,'2016-05-27 02:33:57',''),
	(230,4,33,41,'2016-05-27 02:33:57',''),
	(231,5,34,41,'2016-05-27 02:33:57',''),
	(232,4,32,42,'2016-05-27 02:33:57',''),
	(233,4,33,42,'2016-05-27 02:33:57',''),
	(234,5,34,42,'2016-05-27 02:33:57',''),
	(235,4,32,43,'2016-05-27 02:33:57',''),
	(236,4,33,43,'2016-05-27 02:33:57',''),
	(237,5,34,43,'2016-05-27 02:33:57',''),
	(238,4,32,44,'2016-05-27 02:33:57',''),
	(239,4,33,44,'2016-05-27 02:33:57',''),
	(240,5,34,44,'2016-05-27 02:33:57',''),
	(241,5,32,45,'2016-05-27 02:35:38',''),
	(242,5,33,45,'2016-05-27 02:35:38',''),
	(243,5,34,45,'2016-05-27 02:35:38',''),
	(244,5,32,46,'2016-05-27 02:35:38',''),
	(245,5,33,46,'2016-05-27 02:35:38',''),
	(246,5,34,46,'2016-05-27 02:35:38',''),
	(247,5,32,47,'2016-05-27 02:35:38',''),
	(248,5,33,47,'2016-05-27 02:35:38',''),
	(249,5,34,47,'2016-05-27 02:35:38',''),
	(250,5,32,48,'2016-05-27 02:35:38',''),
	(251,5,33,48,'2016-05-27 02:35:38',''),
	(252,5,34,48,'2016-05-27 02:35:38',''),
	(253,5,32,49,'2016-05-27 02:35:38',''),
	(254,5,33,49,'2016-05-27 02:35:38',''),
	(255,5,34,49,'2016-05-27 02:35:38',''),
	(256,5,32,50,'2016-05-27 02:35:38',''),
	(257,5,33,50,'2016-05-27 02:35:38',''),
	(258,5,34,50,'2016-05-27 02:35:38',''),
	(259,5,32,51,'2016-05-27 02:35:38',''),
	(260,5,33,51,'2016-05-27 02:35:38',''),
	(261,5,34,51,'2016-05-27 02:35:38',''),
	(262,5,32,52,'2016-05-27 02:35:38',''),
	(263,5,33,52,'2016-05-27 02:35:38',''),
	(264,5,34,52,'2016-05-27 02:35:38',''),
	(265,5,32,53,'2016-05-27 02:35:38',''),
	(266,5,33,53,'2016-05-27 02:35:38',''),
	(267,5,34,53,'2016-05-27 02:35:38',''),
	(268,5,32,54,'2016-05-27 02:35:38',''),
	(269,5,33,54,'2016-05-27 02:35:38',''),
	(270,5,34,54,'2016-05-27 02:35:38',''),
	(271,3,6,55,'2016-05-30 10:17:08',''),
	(272,5,7,55,'2016-05-30 08:42:42',''),
	(273,5,8,55,'2016-05-31 10:40:17',''),
	(274,5,9,55,'2016-05-30 08:42:42',''),
	(275,4,10,55,'2016-05-30 08:42:42',''),
	(276,3,6,56,'2016-05-31 10:40:46',''),
	(277,5,7,56,'2016-05-30 09:55:23',''),
	(278,5,8,56,'2016-05-30 09:55:18',''),
	(279,5,9,56,'2016-05-30 09:55:09',''),
	(280,4,10,56,'2016-05-30 09:07:21',''),
	(281,3,6,57,'2016-05-31 10:39:38',''),
	(282,5,7,57,'2016-05-30 09:54:12',''),
	(283,5,8,57,'2016-05-30 09:54:02',''),
	(284,5,9,57,'2016-05-30 09:53:59',''),
	(285,4,10,57,'2016-05-30 09:09:19',''),
	(286,3,6,58,'2016-05-30 10:16:35',''),
	(287,5,7,58,'2016-05-30 09:13:21',''),
	(288,5,8,58,'2016-05-30 09:53:16',''),
	(289,5,9,58,'2016-05-30 09:53:13',''),
	(290,4,10,58,'2016-05-30 09:13:21',''),
	(291,3,6,59,'2016-05-30 09:16:51',''),
	(292,5,7,59,'2016-05-30 09:52:25',''),
	(293,4,8,59,'2016-05-31 10:41:31',''),
	(294,5,9,59,'2016-05-30 09:51:28',''),
	(295,4,10,59,'2016-05-30 09:16:51',''),
	(296,5,6,60,'2016-05-30 09:55:55',''),
	(297,5,7,60,'2016-05-30 09:49:22',''),
	(298,5,8,60,'2016-05-30 09:49:24',''),
	(299,5,9,60,'2016-05-30 09:49:29',''),
	(300,5,10,60,'2016-05-30 09:50:03',''),
	(301,3,1,61,'2016-05-30 09:57:20',''),
	(302,4,2,61,'2016-05-30 09:57:20',''),
	(303,5,3,61,'2016-05-30 09:57:20',''),
	(304,4,4,61,'2016-05-30 09:57:20',''),
	(305,4,5,61,'2016-05-30 09:57:20',''),
	(306,5,30,61,'2016-05-30 09:57:20',''),
	(307,5,31,61,'2016-05-30 09:57:20',''),
	(308,4,1,62,'2016-06-01 05:59:55',''),
	(309,5,2,62,'2016-05-31 10:36:28',''),
	(310,4,3,62,'2016-05-31 10:36:40',''),
	(311,4,4,62,'2016-05-30 10:12:23',''),
	(312,5,5,62,'2016-05-31 10:37:44',''),
	(313,5,30,62,'2016-05-30 10:12:23',''),
	(314,5,31,62,'2016-05-30 10:12:23',''),
	(315,4,1,63,'2016-05-31 10:35:18',''),
	(316,5,2,63,'2016-05-31 10:35:30',''),
	(317,5,3,63,'2016-05-31 09:53:43',''),
	(318,4,4,63,'2016-05-31 09:53:43',''),
	(319,5,5,63,'2016-05-31 10:35:44',''),
	(320,5,30,63,'2016-05-31 09:53:43',''),
	(321,5,31,63,'2016-05-31 09:53:43',''),
	(322,4,1,64,'2016-06-01 05:09:06',''),
	(323,5,2,64,'2016-05-31 12:24:51',''),
	(324,5,3,64,'2016-05-31 12:24:51',''),
	(325,4,4,64,'2016-06-01 05:09:49',''),
	(326,5,5,64,'2016-05-31 12:24:51',''),
	(327,5,30,64,'2016-05-31 12:24:51',''),
	(328,5,31,64,'2016-05-31 12:24:51',''),
	(329,4,1,65,'2016-05-31 15:31:05',''),
	(330,5,2,65,'2016-05-31 15:31:05',''),
	(331,5,3,65,'2016-05-31 15:31:05',''),
	(332,5,4,65,'2016-05-31 15:31:05',''),
	(333,5,5,65,'2016-05-31 15:31:05',''),
	(334,5,30,65,'2016-05-31 15:31:05',''),
	(335,5,31,65,'2016-05-31 15:31:05',''),
	(336,4,1,66,'2016-05-31 18:27:24',''),
	(337,5,2,66,'2016-05-31 18:27:24',''),
	(338,5,3,66,'2016-05-31 18:27:24',''),
	(339,4,4,66,'2016-06-01 05:14:34',''),
	(340,5,5,66,'2016-06-01 05:13:58',''),
	(341,5,30,66,'2016-05-31 18:27:24',''),
	(342,5,31,66,'2016-05-31 18:27:24',''),
	(343,4,1,67,'2016-06-01 16:52:23',''),
	(344,5,2,67,'2016-06-01 16:52:23',''),
	(345,5,3,67,'2016-06-01 16:52:23',''),
	(346,4,4,67,'2016-06-01 16:52:23',''),
	(347,5,5,67,'2016-06-01 16:52:23',''),
	(348,5,30,67,'2016-06-01 16:52:23',''),
	(349,5,31,67,'2016-06-01 16:52:23',''),
	(350,3,6,68,'2016-06-02 04:20:20',''),
	(351,5,7,68,'2016-06-02 04:20:20',''),
	(352,5,8,68,'2016-06-02 04:20:20',''),
	(353,5,9,68,'2016-06-02 04:20:20',''),
	(354,4,10,68,'2016-06-05 02:27:03',''),
	(355,4,1,69,'2016-06-02 16:33:49',''),
	(356,5,2,69,'2016-06-02 16:33:49',''),
	(357,5,3,69,'2016-06-02 16:33:49',''),
	(358,5,4,69,'2016-06-02 16:33:49',''),
	(359,5,5,69,'2016-06-02 16:33:49',''),
	(360,5,30,69,'2016-06-02 16:33:49',''),
	(361,5,31,69,'2016-06-02 16:33:49',''),
	(362,3,1,70,'2016-06-02 16:40:57',''),
	(363,4,2,70,'2016-06-02 16:40:57',''),
	(364,4,3,70,'2016-06-02 16:40:57',''),
	(365,4,4,70,'2016-06-02 16:40:57',''),
	(366,5,5,70,'2016-06-02 16:40:57',''),
	(367,5,30,70,'2016-06-02 16:40:57',''),
	(368,5,31,70,'2016-06-02 16:40:57',''),
	(369,4,1,71,'2016-06-04 17:34:11',''),
	(370,5,2,71,'2016-06-04 17:34:11',''),
	(371,5,3,71,'2016-06-04 17:34:11',''),
	(372,4,4,71,'2016-06-04 17:34:11',''),
	(373,5,5,71,'2016-06-04 17:34:11',''),
	(374,5,30,71,'2016-06-04 17:34:11',''),
	(375,5,31,71,'2016-06-04 17:34:11',''),
	(376,4,6,72,'2016-06-05 02:38:28',''),
	(377,5,7,72,'2016-06-05 02:35:26',''),
	(378,5,8,72,'2016-06-05 02:35:26',''),
	(379,5,9,72,'2016-06-05 02:35:26',''),
	(380,5,10,72,'2016-06-05 02:35:26',''),
	(381,3,1,73,'2016-06-05 02:43:07',''),
	(382,4,2,73,'2016-06-05 02:43:07',''),
	(383,4,3,73,'2016-06-05 02:43:07',''),
	(384,3,4,73,'2016-06-05 02:43:07',''),
	(385,5,5,73,'2016-06-05 02:43:07',''),
	(386,5,30,73,'2016-06-05 02:43:07',''),
	(387,5,31,73,'2016-06-05 02:43:07',''),
	(388,4,6,74,'2016-06-05 02:55:57',''),
	(389,5,7,74,'2016-06-05 02:55:57',''),
	(390,5,8,74,'2016-06-05 02:55:57',''),
	(391,5,9,74,'2016-06-05 02:55:57',''),
	(392,4,10,74,'2016-06-05 02:55:57',''),
	(393,3,1,75,'2016-06-05 03:04:08',''),
	(394,4,2,75,'2016-06-05 03:04:08',''),
	(395,5,3,75,'2016-06-05 03:04:08',''),
	(396,4,4,75,'2016-06-05 03:04:08',''),
	(397,5,5,75,'2016-06-05 03:04:08',''),
	(398,5,30,75,'2016-06-05 03:04:08',''),
	(399,5,31,75,'2016-06-05 03:04:08',''),
	(400,3,1,76,'2016-06-05 03:09:38',''),
	(401,4,2,76,'2016-06-05 03:09:38',''),
	(402,5,3,76,'2016-06-05 03:09:38',''),
	(403,3,4,76,'2016-06-05 03:09:38',''),
	(404,5,5,76,'2016-06-05 03:09:38',''),
	(405,5,30,76,'2016-06-05 03:09:38',''),
	(406,5,31,76,'2016-06-05 03:09:38',''),
	(407,5,1,77,'2016-06-05 03:15:16',''),
	(408,5,2,77,'2016-06-05 03:15:16',''),
	(409,5,3,77,'2016-06-05 03:15:16',''),
	(410,5,4,77,'2016-06-05 03:15:16',''),
	(411,5,5,77,'2016-06-05 03:15:16',''),
	(412,5,30,77,'2016-06-05 03:15:16',''),
	(413,5,31,77,'2016-06-05 03:15:16',''),
	(414,4,6,78,'2016-06-05 03:21:11',''),
	(415,5,7,78,'2016-06-05 03:21:11',''),
	(416,5,8,78,'2016-06-05 03:21:11',''),
	(417,5,9,78,'2016-06-05 03:21:11',''),
	(418,4,10,78,'2016-06-05 03:21:11',''),
	(419,4,6,79,'2016-06-05 03:38:53',''),
	(420,5,7,79,'2016-06-05 03:38:53',''),
	(421,5,8,79,'2016-06-05 03:38:53',''),
	(422,5,9,79,'2016-06-05 03:38:53',''),
	(423,4,10,79,'2016-06-05 03:38:53',''),
	(424,4,6,80,'2016-06-05 03:46:47',''),
	(425,5,7,80,'2016-06-05 03:46:47',''),
	(426,5,8,80,'2016-06-05 03:46:47',''),
	(427,5,9,80,'2016-06-05 03:46:47',''),
	(428,5,10,80,'2016-06-05 03:46:47',''),
	(429,4,6,81,'2016-06-05 03:46:47',''),
	(430,5,7,81,'2016-06-05 03:46:47',''),
	(431,5,8,81,'2016-06-05 03:46:47',''),
	(432,5,9,81,'2016-06-05 03:46:47',''),
	(433,5,10,81,'2016-06-05 03:46:47',''),
	(434,3,1,82,'2016-06-05 03:55:22',''),
	(435,5,2,82,'2016-06-05 03:55:22',''),
	(436,5,3,82,'2016-06-05 03:55:22',''),
	(437,4,4,82,'2016-06-05 03:55:22',''),
	(438,5,5,82,'2016-06-05 03:55:22',''),
	(439,5,30,82,'2016-06-05 03:55:22',''),
	(440,5,31,82,'2016-06-05 03:55:22',''),
	(441,4,1,83,'2016-06-05 04:27:14',''),
	(442,5,2,83,'2016-06-05 04:27:14',''),
	(443,5,3,83,'2016-06-05 04:27:14',''),
	(444,5,4,83,'2016-06-05 04:27:14',''),
	(445,5,5,83,'2016-06-05 04:27:14',''),
	(446,5,30,83,'2016-06-05 04:27:14',''),
	(447,5,31,83,'2016-06-05 04:27:14',''),
	(448,5,1,84,'2016-06-05 04:37:58',''),
	(449,5,2,84,'2016-06-05 04:37:58',''),
	(450,5,3,84,'2016-06-05 04:37:58',''),
	(451,5,4,84,'2016-06-05 04:37:58',''),
	(452,5,5,84,'2016-06-05 04:37:58',''),
	(453,5,30,84,'2016-06-05 04:37:58',''),
	(454,5,31,84,'2016-06-05 04:37:58',''),
	(455,4,1,85,'2016-06-05 05:14:51',''),
	(456,5,2,85,'2016-06-05 05:14:51',''),
	(457,5,3,85,'2016-06-05 05:14:51',''),
	(458,4,4,85,'2016-06-05 05:14:51',''),
	(459,5,5,85,'2016-06-05 05:14:51',''),
	(460,5,30,85,'2016-06-05 05:14:51',''),
	(461,5,31,85,'2016-06-05 05:14:51',''),
	(462,3,1,86,'2016-06-05 05:21:37',''),
	(463,5,2,86,'2016-06-05 05:21:37',''),
	(464,5,3,86,'2016-06-05 05:21:37',''),
	(465,3,4,86,'2016-06-05 05:21:37',''),
	(466,5,5,86,'2016-06-05 05:21:37',''),
	(467,5,30,86,'2016-06-05 05:21:37',''),
	(468,5,31,86,'2016-06-05 05:21:37',''),
	(469,4,1,87,'2016-06-05 05:29:34',''),
	(470,5,2,87,'2016-06-05 05:29:34',''),
	(471,5,3,87,'2016-06-05 05:29:34',''),
	(472,5,4,87,'2016-06-05 05:29:34',''),
	(473,5,5,87,'2016-06-05 05:29:34',''),
	(474,5,30,87,'2016-06-05 05:29:34',''),
	(475,5,31,87,'2016-06-05 05:29:34',''),
	(476,3,1,88,'2016-06-14 01:10:58',''),
	(477,5,2,88,'2016-06-14 01:10:58',''),
	(478,5,3,88,'2016-06-14 01:10:58',''),
	(479,4,4,88,'2016-06-14 01:10:58',''),
	(480,5,5,88,'2016-06-14 01:10:58',''),
	(481,5,30,88,'2016-06-14 01:10:58',''),
	(482,5,31,88,'2016-06-14 01:10:58','');

/*!40000 ALTER TABLE `qualityratings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;

INSERT INTO `roles` (`id`, `name`, `description`)
VALUES
	(1,'login','Login privileges, granted after account confirmation'),
	(2,'admin','Administrative user, has access to everything.'),
	(3,'dealer','this is a delaer');

/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table roles_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `roles_users`;

CREATE TABLE `roles_users` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `fk_role_id` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `roles_users` WRITE;
/*!40000 ALTER TABLE `roles_users` DISABLE KEYS */;

INSERT INTO `roles_users` (`user_id`, `role_id`)
VALUES
	(1,1),
	(2,1),
	(3,1),
	(4,1),
	(5,1),
	(6,1),
	(7,1),
	(8,1),
	(9,1),
	(10,1),
	(11,1),
	(12,1),
	(13,1),
	(14,1),
	(15,1),
	(16,1),
	(17,1),
	(18,1),
	(19,1),
	(20,1),
	(21,1),
	(22,1),
	(23,1),
	(24,1),
	(25,1),
	(26,1),
	(27,1),
	(28,1),
	(29,1),
	(30,1),
	(31,1),
	(32,1),
	(33,1),
	(34,1),
	(35,1),
	(36,1),
	(37,1),
	(38,1),
	(39,1),
	(40,1),
	(41,1),
	(42,1),
	(43,1),
	(44,1),
	(45,1),
	(46,1),
	(47,1),
	(48,1),
	(49,1),
	(50,1),
	(51,1),
	(52,1),
	(53,1),
	(54,1),
	(55,1),
	(56,1),
	(57,1),
	(58,1),
	(59,1),
	(60,1),
	(61,1),
	(62,1),
	(63,1),
	(64,1),
	(65,1),
	(66,1),
	(67,1),
	(68,1),
	(69,1),
	(70,1),
	(71,1),
	(72,1),
	(73,1),
	(74,1),
	(75,1),
	(76,1),
	(77,1),
	(78,1),
	(79,1),
	(80,1),
	(81,1),
	(82,1),
	(83,1),
	(84,1),
	(85,1),
	(86,1),
	(87,1),
	(88,1),
	(89,1),
	(90,1),
	(91,1),
	(92,1),
	(93,1),
	(94,1),
	(95,1),
	(96,1),
	(97,1),
	(98,1),
	(99,1),
	(100,1),
	(101,1),
	(102,1),
	(103,1),
	(104,1),
	(105,1),
	(106,1),
	(107,1),
	(108,1),
	(109,1),
	(110,1),
	(111,1),
	(112,1),
	(113,1),
	(114,1),
	(115,1),
	(116,1),
	(117,1),
	(118,1),
	(119,1),
	(120,1),
	(121,1),
	(122,1),
	(123,1),
	(124,1),
	(125,1),
	(126,1),
	(127,1),
	(128,1),
	(129,1),
	(130,1),
	(131,1),
	(132,1),
	(133,1),
	(134,1),
	(135,1),
	(136,1),
	(137,1),
	(138,1),
	(139,1),
	(140,1),
	(141,1),
	(142,1),
	(143,1),
	(144,1),
	(145,1),
	(146,1),
	(147,1),
	(148,1),
	(149,1),
	(150,1),
	(151,1),
	(152,1),
	(153,1),
	(154,1),
	(155,1),
	(156,1),
	(157,1),
	(158,1),
	(159,1),
	(160,1),
	(161,1),
	(162,1);

/*!40000 ALTER TABLE `roles_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table technicalspecs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `technicalspecs`;

CREATE TABLE `technicalspecs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(512) NOT NULL,
  `productcategory_id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `technicalspecs` WRITE;
/*!40000 ALTER TABLE `technicalspecs` DISABLE KEYS */;

INSERT INTO `technicalspecs` (`id`, `name`, `productcategory_id`, `timestamp`)
VALUES
	(1,'capacity',3,'2016-03-03 22:36:29');

/*!40000 ALTER TABLE `technicalspecs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table technicalspecvalues
# ------------------------------------------------------------

DROP TABLE IF EXISTS `technicalspecvalues`;

CREATE TABLE `technicalspecvalues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(256) NOT NULL,
  `technicalspec_id` int(11) NOT NULL,
  `productitem_id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `technicalspecvalues` WRITE;
/*!40000 ALTER TABLE `technicalspecvalues` DISABLE KEYS */;

INSERT INTO `technicalspecvalues` (`id`, `value`, `technicalspec_id`, `productitem_id`, `timestamp`)
VALUES
	(1,'200c',1,6,'2016-03-03 23:09:15');

/*!40000 ALTER TABLE `technicalspecvalues` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ticketcomments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ticketcomments`;

CREATE TABLE `ticketcomments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comment` text NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `commentor_id` int(11) NOT NULL,
  `ticket_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_comments_users1_idx` (`commentor_id`),
  KEY `fk_comments_tickets1_idx` (`ticket_id`),
  CONSTRAINT `fk_comments_tickets1` FOREIGN KEY (`ticket_id`) REFERENCES `tickets` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_comments_users1` FOREIGN KEY (`commentor_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tickets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tickets`;

CREATE TABLE `tickets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` text NOT NULL,
  `subject` varchar(256) NOT NULL DEFAULT '',
  `resolved` tinyint(1) DEFAULT '0',
  `date` datetime NOT NULL,
  `tickettype_id` int(11) DEFAULT '1',
  `user_id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tickets_tickettypes1_idx` (`tickettype_id`),
  KEY `fk_tickets_users1_idx` (`user_id`),
  CONSTRAINT `fk_tickets_tickettypes1` FOREIGN KEY (`tickettype_id`) REFERENCES `tickettypes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tickets_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tickettypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tickettypes`;

CREATE TABLE `tickettypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tickettypes` WRITE;
/*!40000 ALTER TABLE `tickettypes` DISABLE KEYS */;

INSERT INTO `tickettypes` (`id`, `name`)
VALUES
	(1,'Service');

/*!40000 ALTER TABLE `tickettypes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_tokens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_tokens`;

CREATE TABLE `user_tokens` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `user_agent` varchar(40) NOT NULL,
  `token` varchar(40) NOT NULL,
  `created` int(10) unsigned NOT NULL,
  `expires` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_token` (`token`),
  KEY `fk_user_id` (`user_id`),
  KEY `expires` (`expires`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `user_tokens` WRITE;
/*!40000 ALTER TABLE `user_tokens` DISABLE KEYS */;

INSERT INTO `user_tokens` (`id`, `user_id`, `user_agent`, `token`, `created`, `expires`)
VALUES
	(350,60,'f9847aed8dc72b987d56d513855762727263cdba','85e95862d87ef36f62af8bb75c7c84d729b7f143',1467876335,1468740335),
	(352,1,'978011374834bdb1262d356f8bd687895c85b84c','4b345f138a9a6f57877b4a911881b106e10e257e',1468480963,1469344963),
	(353,162,'f40985f0dab88d2bb8aa231f761bd7abc0d2c7af','37c6d705c6a9e6110597779b103464eb9ce17da1',1468482763,1469346763),
	(354,1,'78e4dd2e0d98188bc6000245d700ed46d9056802','f05c4b481e8c36ee04b6a087e765a363f22e5f76',1468483175,1469347175),
	(355,82,'978011374834bdb1262d356f8bd687895c85b84c','bed61c6c42c95037b1e3847d6fa5154c34eaf38c',1468561588,1469425588);

/*!40000 ALTER TABLE `user_tokens` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table usernotifications
# ------------------------------------------------------------

DROP TABLE IF EXISTS `usernotifications`;

CREATE TABLE `usernotifications` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type_id` int(11) unsigned NOT NULL,
  `item_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `order_id` int(11) NOT NULL,
  `seen` tinyint(1) NOT NULL DEFAULT '0',
  `delivered` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `usernotifications` WRITE;
/*!40000 ALTER TABLE `usernotifications` DISABLE KEYS */;

INSERT INTO `usernotifications` (`id`, `type_id`, `item_id`, `user_id`, `date`, `order_id`, `seen`, `delivered`)
VALUES
	(1,1,18,5,'2016-05-23 11:08:16',1,1,1),
	(2,2,18,5,'2016-05-23 11:08:46',1,1,1),
	(3,1,25,13,'2016-05-23 17:57:40',2,1,1),
	(4,1,13,5,'2016-05-23 18:56:12',5,1,1),
	(5,2,13,5,'2016-05-23 18:56:16',5,1,1),
	(6,3,13,5,'2016-05-23 18:56:19',5,1,1),
	(7,1,14,5,'2016-05-23 19:04:00',6,1,1),
	(8,2,14,5,'2016-05-23 19:04:04',6,1,1),
	(9,3,14,5,'2016-05-23 19:04:14',6,1,1),
	(10,1,3,26,'2016-05-23 20:47:59',7,1,1),
	(11,1,17,18,'2016-05-23 23:55:27',3,0,0),
	(12,1,8,27,'2016-05-24 11:34:23',8,0,0),
	(13,1,26,42,'2016-05-24 11:34:31',9,0,0),
	(14,1,6,42,'2016-05-24 11:34:36',10,0,0),
	(15,1,27,15,'2016-05-24 14:59:54',12,0,1),
	(16,1,1,48,'2016-05-24 16:24:20',14,1,1),
	(17,1,5,54,'2016-05-24 17:58:15',16,1,1),
	(18,1,28,54,'2016-05-24 18:03:00',17,1,1),
	(19,2,25,13,'2016-05-24 18:38:19',2,1,1),
	(20,2,8,27,'2016-05-24 18:38:33',8,0,0),
	(21,1,29,55,'2016-05-24 22:01:14',18,0,0),
	(22,3,25,13,'2016-05-25 07:18:19',2,1,1),
	(23,3,8,27,'2016-05-25 07:24:11',8,0,0),
	(24,2,29,55,'2016-05-25 08:01:56',18,0,0),
	(25,2,28,54,'2016-05-25 08:02:16',17,0,0),
	(26,2,5,54,'2016-05-25 08:02:20',16,0,0),
	(27,2,1,48,'2016-05-25 08:02:32',14,1,1),
	(28,2,6,42,'2016-05-25 08:02:46',10,0,0),
	(29,2,26,42,'2016-05-25 08:02:50',9,0,0),
	(30,2,17,18,'2016-05-25 08:03:05',3,0,0),
	(31,3,17,18,'2016-05-25 12:02:51',3,0,0),
	(32,3,1,48,'2016-05-25 12:02:57',14,1,1),
	(33,3,28,54,'2016-05-25 13:47:10',17,0,0),
	(34,3,5,54,'2016-05-25 13:47:15',16,0,0),
	(35,3,29,55,'2016-05-25 22:21:04',18,0,0),
	(36,1,6,62,'2016-05-26 11:36:42',20,0,1),
	(37,2,6,62,'2016-05-26 17:03:13',20,0,1),
	(38,1,33,63,'2016-05-26 19:06:32',21,1,1),
	(39,1,31,3,'2016-05-26 23:24:06',22,1,1),
	(40,1,32,3,'2016-05-26 23:24:10',22,1,1),
	(41,2,31,3,'2016-05-26 23:25:40',22,1,1),
	(42,3,31,3,'2016-05-26 23:26:03',22,1,1),
	(43,3,6,62,'2016-05-27 12:33:54',20,0,1),
	(44,1,34,26,'2016-05-27 19:53:21',23,0,1),
	(45,2,34,26,'2016-05-27 19:53:26',23,0,1),
	(46,3,34,26,'2016-05-28 07:00:08',23,0,1),
	(47,2,27,15,'2016-05-28 18:23:15',12,0,0),
	(48,3,27,15,'2016-05-28 19:19:47',12,0,0),
	(49,2,33,63,'2016-05-29 15:46:27',21,1,1),
	(50,3,33,63,'2016-05-29 15:46:31',21,1,1),
	(51,1,26,78,'2016-05-30 17:53:19',25,0,0),
	(52,1,61,81,'2016-05-31 11:57:08',26,1,1),
	(53,1,58,84,'2016-05-31 15:08:03',27,0,1),
	(54,2,26,78,'2016-06-01 12:14:34',25,0,0),
	(55,3,26,78,'2016-06-01 13:02:58',25,0,0),
	(56,1,66,87,'2016-06-01 13:32:56',30,0,0),
	(57,1,60,87,'2016-06-01 13:33:01',29,0,0),
	(58,2,61,81,'2016-06-01 16:13:55',26,1,1),
	(59,3,61,81,'2016-06-01 16:14:05',26,1,1),
	(60,2,58,84,'2016-06-02 07:32:52',27,0,0),
	(61,3,58,84,'2016-06-02 08:35:30',27,0,0),
	(62,2,66,87,'2016-06-02 09:38:18',30,0,0),
	(63,2,60,87,'2016-06-02 09:38:47',29,0,0),
	(64,3,66,87,'2016-06-02 11:47:19',30,0,0),
	(65,3,60,87,'2016-06-02 11:47:23',29,0,0),
	(66,1,65,21,'2016-06-02 14:54:53',31,0,1),
	(67,1,69,63,'2016-06-03 21:00:21',32,0,0),
	(68,1,56,110,'2016-06-03 21:01:49',33,0,1),
	(69,1,70,112,'2016-06-04 11:42:09',34,0,1),
	(70,1,62,98,'2016-06-04 11:51:30',35,1,1),
	(71,1,67,98,'2016-06-04 12:25:29',36,0,1),
	(72,1,62,115,'2016-06-04 16:16:48',38,0,0),
	(73,1,65,113,'2016-06-04 16:16:51',37,0,0),
	(74,1,55,116,'2016-06-04 18:40:41',39,0,1),
	(75,1,57,109,'2016-06-04 20:04:21',40,0,0),
	(76,1,71,119,'2016-06-04 23:10:26',41,0,0),
	(77,2,71,119,'2016-06-05 07:42:31',41,0,0),
	(78,2,57,109,'2016-06-05 07:42:40',40,0,0),
	(79,2,55,116,'2016-06-05 07:42:45',39,0,1),
	(80,2,62,115,'2016-06-05 07:42:52',38,0,0),
	(81,2,65,113,'2016-06-05 07:42:57',37,0,0),
	(82,2,70,112,'2016-06-05 07:43:06',34,0,0),
	(83,2,56,110,'2016-06-05 07:43:10',33,0,1),
	(84,2,69,63,'2016-06-05 07:43:14',32,0,0),
	(85,3,57,109,'2016-06-05 10:00:09',40,0,0),
	(86,3,65,113,'2016-06-05 11:45:50',37,0,0),
	(87,1,72,88,'2016-06-05 11:57:50',42,0,1),
	(88,3,69,63,'2016-06-05 12:33:00',32,0,0),
	(89,1,64,121,'2016-06-05 12:53:42',43,1,1),
	(90,3,71,119,'2016-06-05 13:21:39',41,0,0),
	(91,3,70,112,'2016-06-05 14:16:35',34,0,0),
	(92,1,82,122,'2016-06-05 15:25:23',45,0,1),
	(93,3,55,116,'2016-06-05 16:56:30',39,0,1),
	(94,3,62,115,'2016-06-05 16:56:37',38,0,0),
	(95,1,80,125,'2016-06-05 17:19:53',46,0,0),
	(96,1,87,116,'2016-06-05 17:19:57',47,0,1),
	(97,1,76,117,'2016-06-05 21:49:20',48,1,1),
	(98,1,68,117,'2016-06-05 21:49:23',48,1,1),
	(99,1,82,122,'2016-06-05 21:54:55',44,0,1),
	(100,2,80,125,'2016-06-06 08:17:21',46,0,0),
	(101,3,80,125,'2016-06-06 09:04:14',46,0,0),
	(102,1,59,117,'2016-06-06 12:42:46',54,1,1),
	(103,1,63,98,'2016-06-06 12:42:49',53,0,0),
	(104,1,75,127,'2016-06-06 17:03:52',55,0,0),
	(105,2,59,117,'2016-06-07 06:11:29',54,1,1),
	(106,2,76,117,'2016-06-07 06:11:44',48,1,1),
	(107,2,72,88,'2016-06-07 06:11:57',42,0,1),
	(108,3,56,110,'2016-06-07 07:00:44',33,0,1),
	(109,2,72,88,'2016-06-07 07:08:37',42,0,1),
	(110,2,59,117,'2016-06-07 07:08:51',54,1,1),
	(111,2,76,117,'2016-06-07 07:09:12',48,1,1),
	(112,1,73,129,'2016-06-07 08:39:15',56,0,0),
	(113,3,59,117,'2016-06-07 09:10:24',54,0,1),
	(114,3,76,117,'2016-06-07 09:10:34',48,0,1),
	(115,3,72,88,'2016-06-07 09:11:00',42,0,1),
	(116,2,63,98,'2016-06-07 11:10:32',53,0,0),
	(117,3,63,98,'2016-06-07 12:08:22',53,0,0),
	(118,1,82,133,'2016-06-07 15:59:32',60,0,0),
	(119,1,85,134,'2016-06-07 15:59:40',59,0,1),
	(120,1,86,107,'2016-06-07 16:00:01',58,0,0),
	(121,2,87,116,'2016-06-08 06:59:12',47,0,1),
	(122,2,85,134,'2016-06-08 06:59:38',59,0,0),
	(123,3,87,116,'2016-06-08 08:05:23',47,0,1),
	(124,2,64,121,'2016-06-08 08:54:53',43,0,0),
	(125,3,85,134,'2016-06-08 09:25:35',59,0,0),
	(126,3,64,121,'2016-06-08 10:28:12',43,0,0),
	(127,1,83,137,'2016-06-08 20:49:20',61,1,1),
	(128,2,82,133,'2016-06-09 06:30:13',60,0,0),
	(129,2,73,129,'2016-06-09 06:30:31',56,0,0),
	(130,2,75,127,'2016-06-09 06:30:40',55,0,0),
	(131,3,82,133,'2016-06-09 08:05:08',60,0,0),
	(132,3,73,129,'2016-06-09 19:50:00',56,0,0),
	(133,4,12,88,'2016-06-12 14:45:45',62,0,1),
	(134,2,83,137,'2016-06-13 14:36:45',61,0,1),
	(135,3,83,137,'2016-06-13 14:36:53',61,0,1),
	(136,3,75,127,'2016-06-13 16:37:26',55,0,0),
	(137,1,11,88,'2016-06-13 20:46:37',65,0,1),
	(138,1,88,110,'2016-06-14 09:20:39',66,0,0),
	(139,1,81,81,'2016-06-14 14:47:12',67,0,0),
	(140,2,88,110,'2016-06-15 18:20:03',66,0,0),
	(141,3,88,110,'2016-06-16 11:52:16',66,0,0);

/*!40000 ALTER TABLE `usernotifications` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table usernotificationtypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `usernotificationtypes`;

CREATE TABLE `usernotificationtypes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `usernotificationtypes` WRITE;
/*!40000 ALTER TABLE `usernotificationtypes` DISABLE KEYS */;

INSERT INTO `usernotificationtypes` (`id`, `type`)
VALUES
	(1,'Confirmed'),
	(2,'Shipped'),
	(3,'Delivered'),
	(4,'Cancelled'),
	(5,'Executed'),
	(6,'Replacement'),
	(7,'Replaced');

/*!40000 ALTER TABLE `usernotificationtypes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(256) DEFAULT NULL,
  `username` varchar(256) DEFAULT NULL,
  `password` varchar(256) DEFAULT NULL,
  `phonenumber` varchar(256) DEFAULT NULL,
  `firstname` varchar(512) NOT NULL,
  `lastname` varchar(512) NOT NULL,
  `picture` varchar(256) NOT NULL,
  `isdealer` int(11) NOT NULL DEFAULT '0',
  `location` varchar(512) NOT NULL,
  `locationlat` varchar(512) NOT NULL,
  `locationlng` varchar(512) NOT NULL,
  `place_id` varchar(512) NOT NULL,
  `deliveryradius` int(11) NOT NULL,
  `hash` varchar(256) DEFAULT NULL,
  `isverified` int(11) DEFAULT '0',
  `logins` int(11) DEFAULT NULL,
  `last_login` int(11) DEFAULT NULL,
  `isadmin` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `email`, `username`, `password`, `phonenumber`, `firstname`, `lastname`, `picture`, `isdealer`, `location`, `locationlat`, `locationlng`, `place_id`, `deliveryradius`, `hash`, `isverified`, `logins`, `last_login`, `isadmin`)
VALUES
	(1,'admin@pocketin.in','1','2be4a24eedc38c6d7f117e1276d8adbb9ef4321470abc33c08cb8382ea36c453','1','Admin','Pocketin','none',0,'','','','',0,NULL,1,NULL,1468483175,1),
	(2,'pidealer@pocketin.in','2','2be4a24eedc38c6d7f117e1276d8adbb9ef4321470abc33c08cb8382ea36c453','2','Dealer','Pocketin','none',1,'','','','',0,NULL,1,NULL,1468302779,0),
	(3,'vineethkumart@gmail.com','9538092344','f933788acaf9ca7ee829fdfdcea3a96c824b9b333be9058384d43d44e6bdfa88','9538092344','Vineeth','Kumar','none',0,'','','','',0,NULL,1,NULL,1467715468,0),
	(4,'deebanya@yahoo.com','9487625714','9fab8b949b54e8fbc4949c747ad73809a0ca64639095ab7d4a8bb6288dbaaa32','9487625714','Nivrhithi','Karthikeyan','none',0,'','','','',0,NULL,1,NULL,1463980955,0),
	(5,'duraigowardhan.s@gmail.com','9944951838','ca8cead16533dea6f21bc3b8f9d937ac855c1375834e859a15956317a647e6d2','9944951838','durai','gowardhan','none',0,'','','','',0,NULL,1,NULL,1467177369,0),
	(6,'soorajkrishnan1997@gmail.com','9400035112','7864c4b5583fd63388f1c08f99e0353b1abc94cf40ee9f81ef96076881896cd6','9400035112','Sooraj','Krishnan','none',0,'','','','',0,NULL,1,NULL,1464333158,0),
	(7,'ncnarenchoudary@gmail.com','9743000647','6bd32e14ee83bdd1474837dfe8db5a4e9e9aa15e16cb44e702f4514d8939019a','9743000647','Naren','C','none',0,'','','','',0,NULL,0,NULL,NULL,0),
	(8,'anjali.bansal123@gmail.com','9148973085','0e64b766da582144e3599e1a1746dc84e0342642ffeb355406625ba8b8a8732d','9148973085','Anjali','Bansal','none',0,'','','','',0,NULL,1,NULL,1464088456,0),
	(9,'azma.anjum@gmail.com','9545895588','3cb6dcd530f3c63a1e183b2a2df58b8a92a15305d20bb125aaa12f8754934871','9545895588','Azma','Anjum','none',0,'','','','',0,NULL,1,NULL,1463997627,0),
	(10,'a.sahai12@gmail.com','7838453699','22dc150817813930fe6dd2105a55f95d6ad4a39b2272c16638e13c9adc33b3d7','7838453699','Astha','Sahai','none',0,'','','','',0,NULL,1,NULL,1463999362,0),
	(11,'jintocyriac87@gmail.com','8747057450','b4b853cefc371faf379ddbfbf172c233d1361956f706fa6a28f67df7433974ed','8747057450','JINTO','CYRIAC','none',0,'','','','',0,NULL,1,NULL,1464029209,0),
	(12,'mohana.priya10@yahoo.com','9902001183','fb95e2ed0c0bb3734ea8d8e5cd658a3f903cbc875787d5320ddeb60be1a66abf','9902001183','Priya','Mohana','none',0,'','','','',0,NULL,1,NULL,1464260583,0),
	(13,'ankuraman.mishra@gmail.com','7795847342','d35d193c8cf6f183e5e7f342c04a33439a10fcda70b8ca894655f7a0139fa8b3','7795847342','ankur','mishra','none',0,'','','','',0,NULL,1,NULL,1466833432,0),
	(14,'airs2006@gmail.com','9538128057','65a806f0206d8d8127003981dfeb9d1cda0cb00aab5e9cec97d72713ca295877','9538128057','Arijit','Singha','none',0,'','','','',0,NULL,0,NULL,NULL,0),
	(15,'aradhanasngh5@gmail.com','8981542972','4d11e82d2ebefd1d8b0d4f06be65026d232f1b8973809b11977413e89e625b58','8981542972','Aradhana','Singh','none',0,'','','','',1,NULL,1,NULL,1464087218,0),
	(16,'Silliya.ravindran@gmail.com','8050493379','1d19d8548bc9a0fe1fb549d95b71e73399826aafc3e59cd8052dd7c2d6f704b5','8050493379','Silliya','Vk','none',0,'','','','',0,NULL,1,NULL,1464005390,0),
	(17,'prakharautocad2d@gmail.com','7359539830','245a936258b02f750ab6aa489d414bb0c3f64eabd5a45e012b83de9f85ff568e','7359539830','Prakhar','V','none',0,'','','','',0,NULL,1,NULL,1464005787,0),
	(18,'ruchi21091@gmail.com','9886382769','1af9c7fe5b6d4b16d32c0992ffd90a9e1076d4c4c1a81974a4f35a6b08e05ed7','9886382769','ruchismita','chakraborty','none',0,'','','','',0,NULL,1,NULL,1464019060,0),
	(19,'hariprasath.mohankumar@gmail.com','7259121152','287099edfe01aa99fc7704020db4968234a076139e55c1a710714e94da49ef24','7259121152','Hariprasath','M','none',0,'','','','',0,NULL,1,NULL,1465204958,0),
	(20,'himanshuchoudhary247@gmail.com','9166602365','114ad6b1ecbaf6445b629589185c48c8ece4df62f6412621c1cd74edc4ffaaeb','9166602365','Himanshu','Choudhary','none',0,'','','','',0,NULL,1,NULL,1464011312,0),
	(21,'26anoop1990@gmail.com','9686482400','0d59af8fe3f11ca9f5eedfc674c9070d67e772b89b7975ee9d9331dba025ec8e','9686482400','Anoop','K','none',0,'','','','',0,NULL,1,NULL,1464856799,0),
	(22,'anandiyer18@gmail.com','7022004119','eda45b7ee536e2a29724bfe52c4e7c89ab57926d2fb6cddcc4de9f276becd42e','7022004119','Anand','Iyer','none',0,'','','','',0,NULL,1,NULL,1464009374,0),
	(23,'madhushrichoudhary86@gmail.com','8793167506','1fc3773e0b44049796c223a3399c9de71a4e472c2060186c6d5cfad52ac56758','8793167506','Madhushri','Patel','none',0,'','','','',0,NULL,1,NULL,1464009507,0),
	(24,'pradeeps1988@gmail.com','9663866544','66a17fcda0c73298195528313bec4422281c1b1e0ff48704869e92265f16defc','9663866544','Pradeep','S','none',0,'','','','',0,NULL,1,NULL,1464009597,0),
	(25,'pritvirajsaha@gmail.com','9804837863','35dacf2be498cb428360bc8e9bebdc8fe61b5bcb65ecfe11e8cd7708735cd0a9','9804837863','Pritviraj','Saha','none',0,'','','','',0,NULL,1,NULL,1464009665,0),
	(26,'sujith10.cea@gmail.com','7406926385','5aeea58ebe4244ec0256eafec1a83c48942f652ef93016112d038ac885c41481','7406926385','sujith','kumar','none',0,'','','','',0,NULL,1,NULL,1464351487,0),
	(27,'reechabansal@yahoo.com','9742600477','a7173e98943488e5a87a3544c8f3a743c548ab82ed75269af12921209da247ac','9742600477','Reecha','Bansal','none',0,'','','','',0,NULL,1,NULL,1464067304,0),
	(28,'choudharysudhanshu0871999@gmail.com','9521866830','e9666d517927c6f46c465f3f576511e0f65a9c6732dec0a4ea1975f9b91717ad','9521866830','Sudhanshu','Choudhary','none',0,'','','','',0,NULL,1,NULL,1464014154,0),
	(29,'shubham.chaurasia.261996@gmail.com','9001201842','aba01f9b5b573c96713f02313cf250ae793bdb4db21c10506f3a5d47c3aea969','9001201842','Shubham','Chaurasia','none',0,'','','','',0,NULL,1,NULL,1464014501,0),
	(30,'jinupthomas@gmail.com','9349373847','cdcd11800ff9844e760403c4abe6acbd046598891f417fec9b3779f625de7231','9349373847','Jinu','PThomas','none',0,'','','','',0,NULL,1,NULL,1464014692,0),
	(31,'ganeshvarma1212@gmail.com','9633746577','10784dd594a6f564fcac98a5c9214da3aa9e186da7b4f395c3ab4a07d00894cf','9633746577','Ganesh','Varma','none',0,'','','','',0,NULL,1,NULL,1464015339,0),
	(32,'anusree.nidimbal94@gmail.com','8951338475','4ab6691d0cdf69648a2ddf5314041f1e0f37e3e3b330b068af5e4bc64318498b','8951338475','Anusree','NP','none',0,'','','','',0,NULL,1,NULL,1464692554,0),
	(33,'cshubham261996@gmail.com','8503072144','aba01f9b5b573c96713f02313cf250ae793bdb4db21c10506f3a5d47c3aea969','8503072144','Shubham','roy','none',0,'','','','',0,NULL,1,NULL,1464017569,0),
	(34,'15ucs136@lnmiit.ac.in','9694664630','aba01f9b5b573c96713f02313cf250ae793bdb4db21c10506f3a5d47c3aea969','9694664630','shubham','patel','none',0,'','','','',0,NULL,1,NULL,1464017697,0),
	(35,'sneha.varma2014@gmail.com','9028293398','4978d5a817932590384f561f8ccc89996ea70e053069c7eeca079e58354231f2','9028293398','Sneha','Varma','none',0,'','','','',0,NULL,1,NULL,1464019285,0),
	(36,'ffggh@gjh.com','9876543210','53988d4c7746fab0ad717414cc0968fab04cc51fb8918353f6950668fa8b16ec','9876543210','Gh','Ffg','none',0,'','','','',0,NULL,0,NULL,NULL,0),
	(37,'nandinichoudhary7@gmail.com','9899973771','719c1732cdfef98566a8db9073ca2b631afe8645306b263b9144015bd0325f4f','9899973771','Nandini','Choudhary','none',0,'','','','',0,NULL,1,NULL,1464021125,0),
	(38,'utkarsh12@guerillamail.com','4389935641','924a4ad92cd20535bbd6bad66d74ec99f053f4af5af79fb72b152983c94a8106','4389935641','Utkarsh','Choudhary','none',0,'','','','',0,NULL,0,NULL,NULL,0),
	(39,'himtorq@gmail.com','8146067550','5baa3addb8ff4f755a1561110ff461937fae4c2b7e47961aa2e1f01f4edf6aaf','8146067550','Himanshu','Gaurav','none',0,'','','','',0,NULL,1,NULL,1464026395,0),
	(40,'salilkolhe1@gmail.com','9589919002','ab29b7b8dd8af5bd4ea9790e05f9779796625ddeb45197bc81ace4e486974d3b','9589919002','salil','Kolhe','none',0,'','','','',0,NULL,0,NULL,NULL,0),
	(41,'knarad91p@gmail.com','9886121745','b2adbef0988e69cc0764ef60badca1335e6177326b29098e94abb06e96fc9535','9886121745','Narad','Kumar','none',0,'','','','',0,NULL,1,NULL,1464069434,0),
	(42,'haritgkumar@gmail.com','8290539343','20547c96ba97061ed7e8b5c9a64e7b4f366e5b548a58c16aaba04652fa792dd0','8290539343','Harit','Gulati','none',0,'','','','',0,NULL,1,NULL,1464068608,0),
	(43,'pushkaranand0312@gmail.com','9611500701','f569f1206a5b2e904e1bbf99c6ae930972c825124b2ed52eb3776b2816b7664e','9611500701','Pushkar','Anand','none',0,'','','','',0,NULL,1,NULL,1464070557,0),
	(44,'caravind07@gmail.com','8105877020','5288686952c02f3761229de4ef35948bd86fe22c2cbbc89a4beb49638b90f0a5','8105877020','Aravind','C','none',0,'','','','',0,NULL,1,NULL,1464667394,0),
	(45,'santhosh.rst@gmail.com','8088312676','56e163afadc754d3940c0850b0d7f1e5277792a9bed2ab75d08225d862edd74d','8088312676','Santhosh','R','none',0,'','','','',0,NULL,1,NULL,1464072162,0),
	(46,'sandappan@gmail.com','8904924969','9fc8bb5200e971d0cda70a9cb72f18a59e194f7d82500efb901ff3f8bc0c72a7','8904924969','Sandeep','Narayanan','none',0,'','','','',0,NULL,1,NULL,1464421002,0),
	(47,'mkisore@gmail.com','9944882189','3730829c32365fa64b8dce02c266d596c7c0ffb0e4e5d1c7052ffa4c5d209f7e','9944882189','Kisore','M','none',0,'','','','',0,NULL,1,NULL,1466484625,0),
	(48,'vips.wipro@gmail.com','8971777887','fab7aa9a1772d7de335fdc1c1070602e7f10534bee9654f0bdab002589ae977c','8971777887','Vipin','Mohan','none',0,'','','','',0,NULL,1,NULL,1464887276,0),
	(49,'uk1441@gmail.com','8951634790','c0bd8e44e89ccc529887dfa677901592ddef72093b4849b4f9799acbf884cef6','8951634790','UDAYA','KUMAR','none',0,'','','','',0,NULL,1,NULL,1464084288,0),
	(50,'frankcyril@gmail.com','8086515566','e03aa2440ced6b067bf952cbd589a525d32a3dcbf072a0e9f83139ce19c525ca','8086515566','Cyril','Frank','none',0,'','','','',0,NULL,1,NULL,1464085404,0),
	(51,'Sattanathan.45@hotmail.com','9739552827','0f7064269c273048c3e054f3372b03a431fb2fdbbc56c43c9c60068d80542945','9739552827','Satty','','none',0,'','','','',0,NULL,1,NULL,1464085563,0),
	(52,'prashanthsep11@gmail.com','9036199008','e4a2fb9db2aef517bac4cb36e47762f0b7d3cf2ce7c0d09c4a0670c9c032bc0f','9036199008','Prashanth','T','none',0,'','','','',0,NULL,1,NULL,1464088884,0),
	(53,'midhuna1994@gmail.com','9567078162','b1c575c31abeb04f9fd74e92ffbb7041412e4a27124ac8a49df48fd7f3eac667','9567078162','Midhuna','varma','none',0,'','','','',0,NULL,1,NULL,1464090687,0),
	(54,'avikm01@gmail.com','9836033457','9ee4a7fca90964f94c5a1841805af992718369bb5538da19145721d11efd8824','9836033457','Avik','mukherjee','none',0,'','','','',0,NULL,1,NULL,1464092730,0),
	(55,'subratasrk1940@gmail.com','7411063991','e8614a97aa91df540ea7eace778e63b534f5acbfc764d8c0fbddf032f802a379','7411063991','Subrata','Sarkar','none',0,'','','','',0,NULL,1,NULL,1464093770,0),
	(56,'malharhunge60@gmail.com','9767173675','25e290ffa75ff8f120df0ff90dfd8cda39b646f066fd894ceecd4f4b6b38c326','9767173675','Malhar','Hunge','none',0,'','','','',0,NULL,1,NULL,1464094719,0),
	(57,'masih.naveen@rediffmail.com','9516690708','e9ad5974c2638b120836c438bb1be7536bcab6bdd7e4f44bde6352bb0b9af4be','9516690708','naveen','masih','none',0,'','','','',0,NULL,1,NULL,1464100764,0),
	(58,'meenalochinicse@gmail.com','9865382937','08be225da368e4cc17675ca59d0e8fee46236b0c41860b506e3e359283d0b6d4','9865382937','Meena','','none',0,'','','','',0,NULL,1,NULL,1464106774,0),
	(59,'firoshshaji1987@gmail.com','8089791551','6a9497dea222032c9a5049a46055fbaee78faa32ef475dc30e16ab6aaad16f6b','8089791551','Firosh','Shaji','none',0,'','','','',0,NULL,1,NULL,1464111155,0),
	(60,'thesarangh@gmail.com','9496981479','ca71222d08f04212c3a052ad4ff8db96e0b01d52c098acb23882409e51ce54d1','9496981479','Sarangh','Somaraj','none',0,'','','','',0,NULL,1,NULL,1468311735,0),
	(61,'shanmugapriya1990@gmail.com','8553147503','4d00e494ebbd617e059061f0b9f97cdac9fca13b137ed622c80d0d2fbe195f67','8553147503','Shanmugapriya','V','none',0,'','','','',0,NULL,1,NULL,1464186271,0),
	(62,'peravelli8@gmail.com','9160722621','47a80a42f3c3a8932e4bdcdca1fc5ae126e2a13375d0b5882d4789789178e539','9160722621','purna','eravelli','none',0,'','','','',0,NULL,1,NULL,1464336741,0),
	(63,'kritishmani@gmail.com','7899837738','040c853fe9d5b4b2dd1ccc16d8e1d45328eba80112dd2cad8a158b1149ea0ee9','7899837738','Manikandan','Balasubramanian','none',0,'','','','',0,NULL,1,NULL,1464960407,0),
	(64,'narain.urs@gmail.com','9620106655','8146af5beaf0e2267e8889eabc71c6f367d6aa65d2fbe8ef62362d8907753d44','9620106655','LakshmiNarayanan','','none',0,'','','','',0,NULL,1,NULL,1464781102,0),
	(65,'mickysunish@gmail.com','7406054965','829df62f2eefacc4ea5792f6fd3c9940e82f2ee5ab0910146648f4d0131a22c4','7406054965','sunish','satya','none',0,'','','','',0,NULL,1,NULL,1465036135,0),
	(66,'gurucmy55@gmail.com','9986691733','53f3f0aab87d9ebe3419483e5497cd15c1c24e749ad1e3c1471b22da9f7681c6','9986691733','Guruprasad','Guru','none',0,'','','','',0,NULL,1,NULL,1464275055,0),
	(67,'jashjena11@gmail.com','9861643280','79da91928a884258fe3b72f7ae95ba7dfe14287b05f697f5058e6f42c414be8a','9861643280','Jasmin','Jena','none',0,'','','','',0,NULL,1,NULL,1464505513,0),
	(68,'santhosh.jnv@gmail.com','9844466068','cebcc8958e06cad7182684f9136031d326f876f8d2632816c46069951ef6febd','9844466068','Santosh','','none',0,'','','','',0,NULL,1,NULL,1464314485,0),
	(69,'ningaraju4723@gmail.com','9740009017','55d0093d8825d8ee60eb2d76ece71c1da16d075a3a411dc50001f71f4787c09b','9740009017','ningaraju','kb','none',0,'','','','',0,NULL,1,NULL,1464348078,0),
	(70,'sreerajt@yahoo.co.in','9167562835','8c62795afa3ddb9482f906ba94c5a0ea8ee44e0f034c21da49f88dbbbcd27b3d','9167562835','SREERAJ','T','none',0,'','','','',0,NULL,1,NULL,1464868679,0),
	(71,'anaghabharathan@gmail.com','8086105326','8c5d9a122f9530e3ac23a96510452c936aa5ce333dfae0214d4bf501f06ac158','8086105326','Anagha','B','none',0,'','','','',0,NULL,1,NULL,1464546150,0),
	(72,'jeevi.perky@gmail.com','8971681991','287099edfe01aa99fc7704020db4968234a076139e55c1a710714e94da49ef24','8971681991','Jeevitha','Balakrishnan','none',0,'','','','',0,NULL,1,NULL,1464442887,0),
	(73,'gowri2982@gmail.com','7795013515','789a6d9e5306e783beecf039138076b36acc7b2d5a625029ae93deaefb8b7b57','7795013515','Gowri','G','none',0,'','','','',0,NULL,1,NULL,1464496866,0),
	(74,'deepu.damodaran@gmail.com','9840529898','05c526f8b22e103060263558546efb00da51fd27625817e6143ef7ea5a93d3e8','9840529898','deepak','Damodaran','none',0,'','','','',0,NULL,1,NULL,1464504540,0),
	(75,'sandeepkris2003@gmail.com','9496463697','ab70b71a9e4bfb86b34c59a00ab416d851ac5c5785c60a1d965240eaeaa52144','9496463697','AnanthaKrishnan','Thekkayil','none',0,'','','','',0,NULL,1,NULL,1464506319,0),
	(76,'ashishgpt24@gmail.com','8884261148','254d93b6ecf54bc605a187f818ff2e80c77db5d880172d9ea2ebd414b203160c','8884261148','Ashish','Gupta','none',0,'','','','',0,NULL,1,NULL,1464603333,0),
	(77,'palack.w@gmail.com','7847958538','0852692b7b5ef38175190f9ef8f0adb782d6a80a02693639dcf77862d286d63f','7847958538','Palack','Waney','none',0,'','','','',0,NULL,0,NULL,NULL,0),
	(78,'anusha@smoodies.in','9920588039','0926c1fda4d78762db5a34050f96766bb9b742745545ddca7fce55ef6516cd2c','9920588039','Anusha','Bhushan','none',0,'','','','',0,NULL,1,NULL,1464608155,0),
	(79,'raomit.a@gmail.com','8056723136','15a15b84c6e92ad245cff4d5574a6ac412e8aa43f3989a9f019a7b210e9ca99c','8056723136','Ramit','Sharma','none',0,'','','','',0,NULL,1,NULL,1464624136,0),
	(80,'ramesh.502@gmail.com','8886868502','35b82e55e614378ea970a6bf4671815036ab6c748af7dafd300742f5e89656e9','8886868502','RAMESHKUMAR','S','none',0,'','','','',0,NULL,1,NULL,1464668132,0),
	(81,'cinthya.atm@gmail.com','8971062056','7703d86bb40079c18f28c0592cc98b3da157d8f1cfcb5a247cc573e191b9a44a','8971062056','Cinthya','Anand','none',0,'','','','',0,NULL,1,NULL,1465887083,0),
	(82,'sajithsathian@gmail.com','9980802802','dce47f0dea6abee252b2b8355a9bd188125a5cf89291ae02d14758472203ed05','9980802802','Sajith','Sathiadevan','none',0,'','','','',0,NULL,1,NULL,1468561588,0),
	(83,'ratheesh@gmail.com','9980161522','578bc11a53e1b9373894b62317128e1037d31f7742129e0f6793cd00c4424a1c','9980161522','Ratheesh','Kumar','none',0,'','','','',0,NULL,1,NULL,1464684373,0),
	(84,'ganesh.sangale@gmail.com','9702162999','99793ce0905e37bbfb492a5287b1520a0e79e3c3446f932f908d112fd094f17f','9702162999','Ganesh','Sangale','none',0,'','','','',0,NULL,1,NULL,1464686917,0),
	(85,'shreecomputers@gmail.com','9448120928','31bacd0ccd1ac69e2849036e760c2098aff3b488239c935d3ffbde792fca2f4c','9448120928','shreesha','kumar','none',0,'','','','',0,NULL,1,NULL,1464693769,0),
	(86,'srinivas.aditya@gmail.com','9900613961','4ddc90de4e21ad20c35808120a0fcf9983d8212663501ac759d05e6bcc3c1957','9900613961','Srinivas','Aditya','none',0,'','','','',0,NULL,1,NULL,1464691689,0),
	(87,'suraagini9@gmail.com','9901777228','5633e77deb921b46e90145ec7756f3ba8be43d58a38f9e730d56cc8c2d91d957','9901777228','Papuli','Banerjee','none',0,'','','','',0,NULL,1,NULL,1464757977,0),
	(88,'bayyangar@gmail.com','7358316785','5b59e8e8329772227f85c56320357f9d5bcd71abb5c047d5fb83f29751ef8721','7358316785','Balaji','Ayyangar','none',0,'','','','',0,NULL,1,NULL,1465823825,0),
	(89,'hellosanthoshmathai@gmail.com','9986619201','0041cd7c7a993269a4c0f11914e8b616e7d46f200437e1f0a48258d772f6c73b','9986619201','Santhosh','Mathai','none',0,'','','','',0,NULL,1,NULL,1465376269,0),
	(90,'sanjayppandey46@gmail.com','9632401564','3a6ba83d7fc05841488fd26d6200cd71104c45081cb44189ad87df48a7456aa8','9632401564','sanjay','pandey','none',0,'','','','',0,NULL,1,NULL,1464763905,0),
	(91,'aj.gm90@gmail.com','9629351945','09b09dbab62d57bf14eda347e3b28a60d530d631983c99854fdda1bf841edc76','9629351945','Arjun','Mohan','none',0,'','','','',0,NULL,1,NULL,1464858324,0),
	(92,'shivakumarbms5@gmail.com','9901657215','7c0664c2d4e59b5db443cf8a81875c71dd363d7aa1f60f43a8cc7730640bb394','9901657215','shiva','kumar','none',0,'','','','',0,NULL,1,NULL,1464767542,0),
	(93,'p11anandara@iima.ac.in','8884666567','8da3dde87934a51085135b3047dfb27b6ba88168e2d99f588c86a0b226783a8c','8884666567','Antony','Anand','none',0,'','','','',0,NULL,1,NULL,1465317063,0),
	(94,'bharath4791@gmail.com','9551441670','d42db32a3b9da7394f0e84b3174ed2c4ddf33c3d115e4a22a579b4ac6308a96e','9551441670','Bharath','Kumar','none',0,'','','','',0,NULL,1,NULL,1464883447,0),
	(95,'suman.padhy91@gmail.com','9663979492','4dd191408fac63366db79ee5ad6efb253abcdf347d3b76810b6686382e3534a7','9663979492','Suman','Padhy','none',0,'','','','',0,NULL,1,NULL,1464794425,0),
	(96,'ankitmishra073@gmail.com','7093179222','b737fa1e4abab40836ea21b8e493cbd8b263c947f22d259e1f2cc0c412ba216a','7093179222','Ankit','Mishra','none',0,'','','','',0,NULL,1,NULL,1464851213,0),
	(97,'kavin2408@gmail.com','9442059904','f2a60c347a38ff3b32f14e33101437669c9a3d485852affed571c565a53c213c','9442059904','kavin','sundar','none',0,'','','','',0,NULL,1,NULL,1464955878,0),
	(98,'cellplanet484@gmail.com','9886385606','0d6a0223c2027e463dd840d0fba0be0435c3bc14ff3d5714e63cbebb4ae64939','9886385606','Latheef','Perla','none',0,'','','','',0,NULL,1,NULL,1465192894,0),
	(99,'karni.mohamed@gmail.com','9164760302','18bc4e0d9053770e9591873da3c4745520747b6ae0f7817def96a76546b782ad','9164760302','Mohamed','Karni','none',0,'','','','',0,NULL,1,NULL,1464883474,0),
	(100,'snehakumari157@gmail.com','8861186894','efbb5acbd065c41831fa47974b955524e09d13e1183e8f3a34ea9dba335c8732','8861186894','Sneha','Kumari','none',0,'','','','',0,NULL,0,NULL,NULL,0),
	(101,'gireesh.pv1@gmail.com','8123901816','57a456cdf60c724876ffe1aedac3bd9c659ac1e6493c99dac04cc0f9ed08ae60','8123901816','GIREESH','Pv','none',0,'','','','',0,NULL,1,NULL,1465189299,0),
	(102,'kishore.iyer3@gmail.com','7411913904','d282804b7cc147e01b0b391b4f491f6ce409409a1fdaaa0d64801865e7206230','7411913904','Kishore','K','none',0,'','','','',0,NULL,1,NULL,1464920617,0),
	(103,'Fast11.com@gmail.com','8159849862','8f5c7c8d7500cb1f6d7a3221ea0f654ece830d1ce7442090be91798b38d7ad70','8159849862','Arkadeep','Gorai','none',0,'','','','',0,NULL,1,NULL,1464954502,0),
	(104,'amit.pramanik123@gmail.com','7204333259','46447c9f90ca43282dcd4619cca06a70dc62f6632fffd74872eed68cf70def63','7204333259','Amit','Kumar','none',0,'','','','',0,NULL,1,NULL,1465403255,0),
	(105,'yashwant11@gmail.com','9972743792','530d3733bff87da6c513fa169820838b3f5a92411239baf1e54d1d382c565816','9972743792','Yashwant','Shanbhag','none',0,'','','','',0,NULL,1,NULL,1465311913,0),
	(106,'er.neharastogi@gmail.com','8884300022','45d7dca0a79c758824d3b251fcf46c35c61c6a7dc6132020dc215ddc48fc8f6b','8884300022','Neha','Rastogi','none',0,'','','','',0,NULL,0,NULL,NULL,0),
	(107,'akt656715@gmail.com','8971914984','fff9be61033b4d28aeb61fda1ba51665ec4471d9fe0d3db11dd8237d271d8851','8971914984','ashok','tiwari','none',0,'','','','',0,NULL,1,NULL,1465285266,0),
	(108,'pkn29007@yahoo.com','9446360604','a2ce1d09e6970b03f7a921a5f1921c20167808bfae7cf22e8e9e4d5cd59abfa3','9446360604','PRAMOD','KUMAR','none',0,'','','','',0,NULL,1,NULL,1464952593,0),
	(109,'trailokya4u@gmail.com','7086001777','6f0b95e89078ea665e17ce0541de1526d142339b9d45314d8d72bc6f2511ba96','7086001777','Trailokya','B','none',0,'','','','',0,NULL,1,NULL,1465048583,0),
	(110,'racheetar@gmail.com','9900813275','341655f3e819582c26becc06bd74d5259dbba222e6a842b36d48c2abfe5236ab','9900813275','racheeta','ramanujam','none',0,'','','','',0,NULL,1,NULL,1466239016,0),
	(111,'mumpim@gmail.com','9663823506','08194633c0810e994d4d058ed33947332515512972b49348d798e6bb4a3d9046','9663823506','Mumpi','Talukdar','none',0,'','','','',0,NULL,1,NULL,1465009318,0),
	(112,'charusaxena6495s@gmail.com','8880860600','b9d9f7e7f63490d20421a6db4807f8bcc0f16d96ee508e0cb73ffb4bded97a95','8880860600','charu','saxena','none',0,'','','','',0,NULL,1,NULL,1465020440,0),
	(113,'praneta.agrawal@gmail.com','9686528436','9adbdc30d127797af6d2e6a4af4515a864f85dd46fa975759d051c651ac2363c','9686528436','Praneta','Agrawal','none',0,'','','','',0,NULL,1,NULL,1465027219,0),
	(114,'deepakdeep333@gmail.com','9741623300','dd94e3a8bd1bf06543e3824b24256d64045962f3072791c6e7a27735a3d4e1e8','9741623300','Deepak','','none',0,'','','','',0,NULL,1,NULL,1465161203,0),
	(115,'rrajesh222@gmail.com','7795649939','032e0ad0d0e29f041456ff5082a3db4d83af4adc98edf51585c4382a44b89e10','7795649939','Rajesh','Daadi','none',0,'','','','',0,NULL,1,NULL,1465033221,0),
	(116,'anishmac99@gmail.com','9731150033','ed44c39033c1f467c925d480e032f2b53a4945e0224432450ed4acee95ccf70e','9731150033','anish','chandra','none',0,'','','','',0,NULL,1,NULL,1465758079,0),
	(117,'16.shiladitya@gmail.com','8197065678','4dfb65a46957d1c85aabdd7103c4eb74f26511263fefd99d60186637d088c266','8197065678','Shiladitya','Mandal','none',0,'','','','',0,NULL,1,NULL,1465283925,0),
	(118,'vaishnavi.frizzi@gmail.com','9916102171','ee0589eb5032ac80a6b81ed918e10f88eaf6506fce117ff15be1de795f883a39','9916102171','vaishnavi','balusu','none',0,'','','','',0,NULL,1,NULL,1465060130,0),
	(119,'chabigupta2009@gmail.com','8971261714','287099edfe01aa99fc7704020db4968234a076139e55c1a710714e94da49ef24','8971261714','Chabi','Gupta','none',0,'','','','',0,NULL,1,NULL,1465061299,0),
	(120,'geetha.patcharu@gmail.com','7406099976','802634a7f1f65f4572138d1f1b65c7d2c1c814e85c05234c6962e7be575628a4','7406099976','Geetha','Patcharu','none',0,'','','','',0,NULL,1,NULL,1465105132,0),
	(121,'yogesh.0607@yahoo.in','9986122456','2c311c46e22cd6e6593bc2a91787aa4c7268591d3016aee8305e5fb5f861896d','9986122456','YOGESH','YOGI','none',0,'','','','',0,NULL,1,NULL,1465298284,0),
	(122,'barshababuraj@gmail.com','9562388111','e66851b0fb7af71c7e7750a34f025f80bffa88f59b404c7e7173a81999075e75','9562388111','Barsha','Baburaj','none',0,'','','','',0,NULL,1,NULL,1465144131,0),
	(123,'t.sivasa789@gmail.com','8892236925','5b0570ca0ac42f80e18a1514bdecf8e51ce86d01b69f1c0971894e6d7b332276','8892236925','Siva','sankar','none',0,'','','','',0,NULL,1,NULL,1465114394,0),
	(124,'swapnil.marvel@gmail.com','9766576878','3b1d11925914cbd8d92bd70b4e488d6d9878f0e0571a792d8c30bc74e264923a','9766576878','Swapnil','Tanksale','none',0,'','','','',0,NULL,1,NULL,1465114766,0),
	(125,'quietnd@gmail.com','9035168837','2691756d529e7554c893353e6c193af586917c36599beab3682ad5cedda770aa','9035168837','Nandini','Dutta','none',0,'','','','',0,NULL,1,NULL,1465120518,0),
	(126,'pradnya.deshmukh@gmail.com','8050042321','2512a6b764c18bb3e0df27db35c3f83b4fb153e64193b0a92660b2de7bb5dbad','8050042321','Pradnya','Deshmukh','none',0,'','','','',0,NULL,0,NULL,NULL,0),
	(127,'anilmn@rediffmail.com','7406537405','1ad45d7da7fa75fe0f00f7240aee17d3ae78634885fb0651e8f02944b8a2043b','7406537405','Anil','Menon','none',0,'','','','',0,NULL,1,NULL,1465210846,0),
	(128,'mohammed.shahir@gmail.com','9497004400','813d89ee95eeba32f4d7a40ab06c64a77a7c01843635b5144f8379b43f882cc0','9497004400','shahir','m','none',0,'','','','',0,NULL,1,NULL,1465212277,0),
	(129,'nikhilriphone@gmail.com','9901343482','8687bd71481bd7d7ece32a45728f28c536a8b3b2f53025d16627a3d930909285','9901343482','Nikhil','Ravishankar','none',0,'','','','',0,NULL,1,NULL,1465265800,0),
	(130,'mithra.manoj50@gmail.com','9036642233','ba2abe268720efb7ceec9c389e66f07ea3aa7707472a624cc2c5f22ee5fd8b12','9036642233','MANOJ','KUMAR','none',0,'','','','',0,NULL,1,NULL,1465280436,0),
	(131,'chanduhoney10@gmail.com','9742374204','09ddb207924a33799f32446616c2c5cc540eab73c59d34772611fae588470343','9742374204','chandra','sekhar','none',0,'','','','',0,NULL,1,NULL,1465280881,0),
	(132,'kumrawat.h@gmail.com','9700104320','fae4e261a632908a4e8ee6076a5be9394f347bfb321bc4b9d171c2d0ebf90c56','9700104320','Himanshu','Kumrawat','none',0,'','','','',0,NULL,1,NULL,1465292993,0),
	(133,'ali6988f@gmail.com','9739115275','fbd68999064be707b2cf3b7a9d457a1a8b8b5e62ccb66a296f06df4ab881b396','9739115275','Aliya','N','none',0,'','','','',0,NULL,1,NULL,1465293912,0),
	(134,'jeetcx@gmail.com','8880157153','9b8bc7396f0cc6129961a124b0cffd9d3f9f8835161898750b589847069a83fe','8880157153','Abhijeet','','none',0,'','','','',0,NULL,1,NULL,1465373425,0),
	(135,'nitish.rathore25@gmail.com','9748852278','9e3d3d5229232a1d433ff3b9631e40b71549e4695e7597ba06e5fa232a60f4a0','9748852278','Nitish','Singh','none',0,'','','','',0,NULL,1,NULL,1465370906,0),
	(136,'hrudayaraj234@gmail.com','9886655711','133e0246fe2e0b4a1a95e6aae6f3c684462d43bb8ea8afe2d7b4eca143a5d004','9886655711','Deva','HrudayaRaj','none',0,'','','','',0,NULL,1,NULL,1465375362,0),
	(137,'hillol.mazumder@outlook.com','9632608833','4e9ca55f5000df82c177036976b9555c0e8f59b5cff92e6827fda29f63380b36','9632608833','hillol','mazumder','none',0,'','','','',0,NULL,1,NULL,1466113242,0),
	(138,'krshnnjj@gmail.com','9594734043','248ea134033bf62955ffe23c0cb78a9f9d25a06d985e3031b91096d88196b1b8','9594734043','krishnan','j','none',0,'','','','',0,NULL,1,NULL,1465451179,0),
	(139,'leekhangems@gmail.com','9738273395','dcf904788a59f0da8adcca1de1f09d1be82375cc753ecbfe3d89a1783d3e3af4','9738273395','Lee','Khan','none',0,'','','','',0,NULL,1,NULL,1465455351,0),
	(140,'griffingarima@gmail.com','9663856498','4a85a82d9d890f28b012c100dee1e6acabcf9820b93aaa0ae93487d98f5eb8b7','9663856498','garima','poudyal','none',0,'','','','',0,NULL,1,NULL,1465462807,0),
	(141,'anand.sonia@hotmail.com','7829114554','0c064ead62307bf542f0c4e2ddc56bc8537008cedf2076bd0909ee6b77f188cd','7829114554','Sonia','Anand','none',0,'','','','',0,NULL,1,NULL,1465469973,0),
	(142,'tiwaryrohit143@gmail.com','9620824489','493c2ad593d0429cb40f23f95e7cbe36e709f4390a63f4b92cc4ac2c0979059b','9620824489','Rohit','Tiwary','none',0,'','','','',0,NULL,1,NULL,1465470332,0),
	(143,'sandeep.melur@gmail.com','9886011687','80d79b38835867465319bb3f34d5d9f039be51c68fa6e2b6026781c6d69d006e','9886011687','Sandeep','AR','none',0,'','','','',0,NULL,1,NULL,1465477605,0),
	(144,'reddy1975@gmail.com','7702991171','6bb006ad4db029090731cf4bd3d4b96189399505b80573fadc3958576b583900','7702991171','Jagadeesh','Reddy','none',0,'','','','',0,NULL,1,NULL,1465486167,0),
	(145,'kkph1@yahoo.co.in','8553562525','478757012046adeb97af99ffd66fbf253a7ecbd8fe6d4b37c2fa8fae74d7fc28','8553562525','Mahaboob','Khan','none',0,'','','','',0,NULL,1,NULL,1465816064,0),
	(146,'deepaksmt@gmail.com','9595040782','d4ab18ae7792da6b3e78947c53969b5fbba4c97646f0fff62e73847784fc7156','9595040782','Deepak','Samant','none',0,'','','','',0,NULL,1,NULL,1465627007,0),
	(147,'b.prabhu81@gmail.com','9945106067','ddb4e155addc0987d812f09032c810e8e1d74c9b3f35e20a3be9e6a632a4eeb4','9945106067','Prabhu','Balachandran','none',0,'','','','',0,NULL,1,NULL,1466788125,0),
	(148,'susan19dileep@gmail.com','7356129552','80f7686cfef7a8b5b6a54506cf37b183e5a8d2c02c430c5690e9ffe36c44fb2d','7356129552','Susan','Dileep','none',0,'','','','',0,NULL,0,NULL,NULL,0),
	(149,'shineeshpalamthodi@gmail.com','8884715888','fa98ba02d4b5f780388a200103f51bec3227af8a66d9d3ed1516710be17e9720','8884715888','shineesh','p','none',0,'','','','',0,NULL,0,NULL,NULL,0),
	(150,'kumcse@gmail.com','9591148681','459f0a50b26e47ec7030029119eed10b8f207d8a697ce38a3cc31b67c2e53835','9591148681','Mathankumar','Devarajan','none',0,'','','','',0,NULL,1,NULL,1466247335,0),
	(151,'anjughosh921@gmail.com','9744238834','21f8b56b6331877262684f00c4ecf2cbb071d79b352e2d2b856b11aed0eebfbe','9744238834','Anju','Sujith','none',0,'','','','',0,NULL,1,NULL,1466525580,0),
	(152,'shailezz@gmail.com','9620311878','548c5b9e1ff7dfb8f77c508cc7fdc041486c6acc1cd2b4dc2dade2901138f8d8','9620311878','ladumor','shailesh','none',0,'','','','',0,NULL,1,NULL,1466512045,0),
	(153,'narayanrao.m797@gmail.com','8105979782','7193e24c600e65093d6a94c9ef4b25bc42c36b3c2ff83291ed78d749cf46d627','8105979782','Narayan','Rao','none',0,'','','','',0,NULL,1,NULL,1466531146,0),
	(154,'inbam84@gmail.com','9972733597','de1c0004a4b3579574e08fb0f7fe22d20cc3e3c44053d176687bae6230bba500','9972733597','Inbaraj','Duraipandian','none',0,'','','','',0,NULL,1,NULL,1466769289,0),
	(155,'ragnsv@gmail.com','9650468664','ff932750d54b8eaa65467bdaba5cc127e200866db58095e8d7d09224a8297b7f','9650468664','Raghavendra','Nuggu','none',0,'','','','',0,NULL,1,NULL,1466915668,0),
	(156,'singhinderveer94@gmail.com','9035483959','2fa8df1eb5eeb8451d8f2be0dd3165fc2d07409e48d3e894c705d531ea1a8a86','9035483959','Inderveer','Singh','none',0,'','','','',0,NULL,1,NULL,1467037515,0),
	(157,'advaya.worx@gmail.com','9901966888','576860c6a69dfa23b9a356564c7860b9f42d895a5d6d89a9ba1c9f4d05d3cc91','9901966888','Anand','Sankaran','none',0,'','','','',0,NULL,1,NULL,1467180106,0),
	(158,'daniel@helpr.in','9036098865','5d4a227a6f78ad44de7481200a4f3ecddfc13c7e0e0156c7526b1331086ba8ae','9036098865','Daniel','Kuttanathil','none',0,'','','','',0,NULL,1,NULL,1467197394,0),
	(159,'moses.sam.paul@gmail.com','8870164289','3934bad3e80bba58ec325b1fa1f3a405c013eac2fa1e2f45817d11c21a58dcfb','8870164289','sam','john','none',0,'','','','',0,NULL,1,NULL,1467198416,0),
	(160,'renjithpg1@gmail.com','8105652385','597792bdac3f7daf0f0b5a0c8da2ac1dfe417b7ef23a1184e5a68c8618cfc530','8105652385','renjith','george','none',0,'','','','',0,NULL,1,NULL,1467689217,0),
	(161,'Lingarajubt91@gmail.com','9206343514','cadefb281176294e8f8f75f849f3740fc9270f01cd25341b73ec6be0b2466c65','9206343514','Lingaraju','BT','none',0,'','','','',0,NULL,1,NULL,1467617717,0),
	(162,'sevalallal@yahoo.com','9742097852','49a7fc046aa162287f6535cd1b8fccb25698b803b0d9614d6e7ae8eff20156b8','9742097852','sevu','rathod','none',0,'','','','',0,NULL,1,NULL,1468482763,0);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wishlists
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wishlists`;

CREATE TABLE `wishlists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  `productitem_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `wishlists` WRITE;
/*!40000 ALTER TABLE `wishlists` DISABLE KEYS */;

INSERT INTO `wishlists` (`id`, `timestamp`, `user_id`, `productitem_id`)
VALUES
	(1,'2016-05-23 04:18:39',3,14),
	(2,'2016-05-23 12:26:53',18,20),
	(3,'2016-05-23 12:37:59',18,23),
	(5,'2016-05-26 07:19:57',47,21),
	(6,'2016-05-26 15:09:17',66,33),
	(7,'2016-05-30 10:15:54',76,26),
	(9,'2016-05-31 16:45:08',48,24),
	(10,'2016-06-02 13:46:08',5,67),
	(11,'2016-06-02 13:53:49',5,64),
	(12,'2016-06-02 17:56:20',5,11),
	(13,'2016-06-03 13:36:31',110,56),
	(18,'2016-06-05 09:47:42',117,82),
	(19,'2016-06-05 09:47:44',117,68),
	(20,'2016-06-05 15:50:37',117,76),
	(23,'2016-06-06 18:25:38',5,86),
	(24,'2016-06-14 10:12:13',1,77),
	(28,'2016-06-14 15:31:10',1,83);

/*!40000 ALTER TABLE `wishlists` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
