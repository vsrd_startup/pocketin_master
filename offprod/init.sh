if [ ! -f init.sh ]; then
    echo "--------------------------------------"
    echo "Please run this command from inside offprod directory."
    echo "--------------------------------------"
    exit
fi

echo "Setting up the development environment"
echo "--------------------------------------"

echo "1. Creating necessary files and folers";
chmod 777 ../pub/searchindex;
chmod 777 ../pub/assets/images/dealerpending;
chmod 777 ../pub/assets/images/products/normal/;
chmod 777 ../pub/assets/images/products/thumbnail/;
mkdir     ../pub/assets/images/products/zoom/ 2> /dev/null;
chmod 777 ../pub/assets/images/products/zoom/;
mkdir     ../pub/assets/images/sell_requests/ 2> /dev/null;
chmod 777 ../pub/assets/images/sell_requests/;

echo "2. Creating product Images";
cp images/normal/* ../pub/assets/images/products/normal/
cp images/thumbnail/* ../pub/assets/images/products/thumbnail/
cp images/zoom/* ../pub/assets/images/products/zoom/

echo "3. Setting up Kohana";
chmod 777 ../application/logs;
chmod 777 ../application/cache;
mkdir     ../pub/invoices 2> /dev/null;
chmod 777 ../pub/invoices;

echo "4. Setting up Database";
cp database.php ../application/config/
cp local.htaccess ../pub/.htaccess

git update-index --assume-unchanged ../pub/.htaccess
git update-index --assume-unchanged ../application/config/database.php

echo "--------------------------------------"
echo "Now set sql password for root user as 'root'. Create a database with name pocketin_db. Import offprod/pocketin_dev_pristine.sql to the database"
