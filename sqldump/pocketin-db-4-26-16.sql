# ************************************************************
# Sequel Pro SQL dump
# Version 4529
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.0.17-MariaDB)
# Database: pocketin-db
# Generation Time: 2016-04-26 02:48:57 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table activitylogs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `activitylogs`;

CREATE TABLE `activitylogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `object_id` int(11) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_activitylog_users1_idx` (`user_id`),
  CONSTRAINT `fk_activitylog_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `activitylogs` WRITE;
/*!40000 ALTER TABLE `activitylogs` DISABLE KEYS */;

INSERT INTO `activitylogs` (`id`, `object_id`, `type`, `timestamp`, `user_id`)
VALUES
	(1,8,'APPROVED_PRODUCT','2016-04-03 06:42:40',23),
	(2,11,'APPROVED_PRODUCT','2016-04-04 05:38:01',23),
	(3,12,'APPROVED_PRODUCT','2016-04-05 14:12:09',23),
	(4,13,'APPROVED_PRODUCT','2016-04-11 10:55:16',23),
	(5,14,'APPROVED_PRODUCT','2016-04-19 03:25:47',23),
	(6,15,'APPROVED_PRODUCT','2016-04-19 06:23:48',23),
	(7,16,'APPROVED_PRODUCT','2016-04-19 19:38:58',23);

/*!40000 ALTER TABLE `activitylogs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table addresses
# ------------------------------------------------------------

DROP TABLE IF EXISTS `addresses`;

CREATE TABLE `addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `address_line_one` varchar(512) NOT NULL,
  `address_line_two` varchar(512) NOT NULL,
  `city` varchar(256) NOT NULL,
  `state` varchar(256) NOT NULL,
  `country` varchar(256) NOT NULL,
  `pincode` varchar(256) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `landmark` varchar(256) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `name` varchar(100) NOT NULL DEFAULT '',
  `phonenumber` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `addresses` WRITE;
/*!40000 ALTER TABLE `addresses` DISABLE KEYS */;

INSERT INTO `addresses` (`id`, `user_id`, `address_line_one`, `address_line_two`, `city`, `state`, `country`, `pincode`, `timestamp`, `landmark`, `email`, `name`, `phonenumber`)
VALUES
	(1,51,'No 53, Balaji Nivas','Murugeshpalaya','Bangalore','Karnataka','India','560017','2016-04-19 19:04:29','Opposite Top in Town Supermarket','vineethkumart@gmail.com','Vineeth Kumar T','9538092344'),
	(8,51,'rahul','varma','Bangalore','Karntaka','India','12345','2016-04-19 19:04:16','rahul pushi','rahul@gmail.com','Rahul','123'),
	(10,51,'Durai house','Durai Street','Bangalore','Karntaka','India','560067','2016-04-19 19:31:06','opp durai mal','durai@gmail.com','Durai','8978766554'),
	(11,51,'my home','my street','Bangalore','Karnataka','India','560071','2016-04-19 19:33:48','my house is great','vkt@vkt.com','VKT','786765');

/*!40000 ALTER TABLE `addresses` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cartitems
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cartitems`;

CREATE TABLE `cartitems` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cart_id` int(11) unsigned NOT NULL,
  `item_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `cartitems` WRITE;
/*!40000 ALTER TABLE `cartitems` DISABLE KEYS */;

INSERT INTO `cartitems` (`id`, `cart_id`, `item_id`)
VALUES
	(54,2,12),
	(55,3,12),
	(56,4,12),
	(57,5,12),
	(58,6,12),
	(59,7,12),
	(60,8,12);

/*!40000 ALTER TABLE `cartitems` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table carts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `carts`;

CREATE TABLE `carts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `ispurchased` tinyint(1) DEFAULT '0',
  `isexpired` tinyint(1) DEFAULT '0',
  `timestamp` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `carts` WRITE;
/*!40000 ALTER TABLE `carts` DISABLE KEYS */;

INSERT INTO `carts` (`id`, `user_id`, `ispurchased`, `isexpired`, `timestamp`)
VALUES
	(1,51,0,0,NULL),
	(2,55,0,0,NULL),
	(3,63,0,0,NULL),
	(4,66,0,0,NULL),
	(5,67,0,0,NULL),
	(6,68,0,0,NULL),
	(7,69,0,0,NULL),
	(8,70,0,0,NULL);

/*!40000 ALTER TABLE `carts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table comments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `comments`;

CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comment` varchar(256) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `ticket_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_comments_users1_idx` (`user_id`),
  KEY `fk_comments_tickets1_idx` (`ticket_id`),
  CONSTRAINT `fk_comments_tickets1` FOREIGN KEY (`ticket_id`) REFERENCES `tickets` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_comments_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table filters
# ------------------------------------------------------------

DROP TABLE IF EXISTS `filters`;

CREATE TABLE `filters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filtertype_id` int(11) NOT NULL,
  `name` varchar(512) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `filters` WRITE;
/*!40000 ALTER TABLE `filters` DISABLE KEYS */;

INSERT INTO `filters` (`id`, `filtertype_id`, `name`, `timestamp`)
VALUES
	(1,1,'Double Door','2016-03-03 15:49:13'),
	(2,1,'Single Door','2016-03-03 15:49:19'),
	(3,2,'below Rs.5000','2016-03-26 10:06:23'),
	(4,2,'Rs.5000 to 10000','2016-03-26 10:06:30'),
	(5,3,'below 200 lts','2016-03-26 10:05:44'),
	(6,3,'above 200 lts','2016-03-26 10:05:55'),
	(7,4,'Red','2016-03-26 08:59:11'),
	(8,4,'Blue','2016-03-26 08:59:13'),
	(9,4,'white','2016-03-26 08:59:15'),
	(10,4,'other colours','2016-03-26 08:59:22'),
	(11,5,'less than 3 star','2016-03-26 09:08:39'),
	(12,5,'3 star and above','2016-03-26 09:24:34'),
	(13,6,'Fully Automatic','2016-03-26 09:59:46'),
	(14,6,'Semi Automatic','2016-03-26 09:59:57'),
	(15,7,'Front Load','2016-03-26 10:00:13'),
	(16,7,'Top Load','2016-03-26 10:00:21'),
	(17,8,'6Kg and below','2016-03-26 10:04:25'),
	(18,8,'Above 6Kg','2016-03-26 10:04:22'),
	(19,9,'Split','2016-03-26 10:39:17'),
	(20,9,'Window','2016-03-26 10:39:27'),
	(21,10,'Less than 2 tonnes','2016-03-26 10:41:00'),
	(22,10,'2 tonnes & above','2016-03-26 10:41:11'),
	(23,11,'Less than 3 star','2016-03-26 10:41:27'),
	(24,11,'3 star & above','2016-03-26 10:41:38'),
	(25,12,'Electrical','2016-03-26 10:42:30'),
	(26,12,'Non-Electrical','2016-03-26 10:42:50'),
	(27,13,'Gravity based','2016-03-26 10:44:08'),
	(28,13,'RO+UV based','2016-03-26 10:49:22'),
	(29,14,'7Lts and below','2016-03-26 10:49:58'),
	(30,14,'Above 7Lts','2016-03-26 10:50:17'),
	(31,15,'3 Seater & below ','2016-03-26 10:55:10'),
	(32,15,'Above 3 Seater','2016-03-26 10:55:28'),
	(33,16,'Fabric','2016-03-26 10:55:44'),
	(34,16,'Leather','2016-03-26 10:55:53'),
	(35,16,'Wood','2016-03-26 10:56:22'),
	(36,16,'Metal','2016-03-26 10:56:34'),
	(37,17,'Straight','2016-03-26 10:56:58'),
	(38,17,'L & U Shaped','2016-03-26 10:57:13'),
	(39,18,'Beige','2016-03-26 10:57:56'),
	(40,18,'Black','2016-03-26 10:58:02'),
	(41,18,'Other Colors','2016-03-26 10:58:18'),
	(42,19,'Plastic','2016-03-26 11:01:46'),
	(43,19,'Fabric','2016-03-26 11:01:52'),
	(44,19,'Leatherette','2016-03-26 11:02:01'),
	(45,20,'Black','2016-03-26 11:03:37'),
	(46,20,'Beige','2016-03-26 11:03:53'),
	(47,20,'Other colors','2016-03-26 11:03:59'),
	(48,21,'Without Wheels ','2016-03-26 11:05:36'),
	(49,21,'With Seat Lock & Height Adjustment','2016-03-26 11:05:12'),
	(50,22,'Yes','2016-03-26 11:10:15'),
	(51,22,'No','2016-03-26 11:10:20'),
	(52,23,'Yes','2016-03-26 11:10:27'),
	(53,23,'No','2016-03-26 11:10:33');

/*!40000 ALTER TABLE `filters` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table filtertypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `filtertypes`;

CREATE TABLE `filtertypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `productcategory_id` int(11) NOT NULL,
  `name` varchar(512) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `filtertypes` WRITE;
/*!40000 ALTER TABLE `filtertypes` DISABLE KEYS */;

INSERT INTO `filtertypes` (`id`, `productcategory_id`, `name`, `timestamp`)
VALUES
	(1,3,'Door Type','2016-03-03 15:18:31'),
	(2,3,'Price','2016-03-26 08:53:04'),
	(3,3,'Capacity','2016-03-26 08:53:07'),
	(4,3,'Color','2016-03-26 08:53:09'),
	(5,3,'Energy Rating','2016-03-26 09:10:20'),
	(6,4,'Function Type','2016-03-26 09:20:14'),
	(7,4,'Load Type','2016-03-26 09:20:33'),
	(8,4,'Capacity','2016-03-26 09:56:46'),
	(9,5,'Type','2016-03-26 10:07:14'),
	(10,5,'Capacity','2016-03-26 10:07:27'),
	(11,5,'Energy Efficency','2016-03-26 10:07:41'),
	(12,6,'Type','2016-03-26 10:08:24'),
	(13,6,'Purification Technology','2016-03-26 10:08:34'),
	(14,6,'Capacity','2016-03-26 10:08:52'),
	(15,7,'Seating Capacity','2016-03-26 10:52:02'),
	(16,7,'Material','2016-03-26 10:52:30'),
	(17,7,'Shape','2016-03-26 10:52:40'),
	(18,7,'Color','2016-03-26 10:52:48'),
	(19,8,'Material','2016-03-26 10:58:59'),
	(20,8,'Color','2016-03-26 10:59:05'),
	(21,8,'Type','2016-03-26 11:00:11'),
	(22,9,'Wheels Included','2016-03-26 11:09:28'),
	(23,9,'Storage Included','2016-03-26 11:10:53');

/*!40000 ALTER TABLE `filtertypes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table images
# ------------------------------------------------------------

DROP TABLE IF EXISTS `images`;

CREATE TABLE `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `description` text,
  `thumbnail` varchar(512) DEFAULT '',
  `isdefault` int(11) DEFAULT '0',
  `productitem_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_images_products_vendors1_idx` (`productitem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `images` WRITE;
/*!40000 ALTER TABLE `images` DISABLE KEYS */;

INSERT INTO `images` (`id`, `name`, `description`, `thumbnail`, `isdefault`, `productitem_id`)
VALUES
	(32,'11_1.jpg','Image 1','',1,11),
	(33,'11_2.jpg','Image 2','',0,11),
	(34,'11_3.jpg','Image 3','',0,11),
	(35,'12_1.jpg','Image 1','',1,12),
	(36,'12_2.jpg','Image 2','',0,12),
	(37,'12_3.jpg','Image 3','',0,12),
	(38,'13_1.jpg','Image 1','',1,13),
	(39,'13_2.jpg','Image 2','',0,13),
	(40,'13_3.jpg','Image 3','',0,13),
	(43,'16_1.jpeg','Image 1','',1,16),
	(44,'16_2.jpg','Image 2','',0,16),
	(45,'16_3.jpg','Image 3','',0,16),
	(46,'16_4.jpg','Image 4','',0,16),
	(47,'16_5.jpg','Image 5','',0,16),
	(48,'16_6.jpg','Image 6','',0,16);

/*!40000 ALTER TABLE `images` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table notificationobjectchanges
# ------------------------------------------------------------

DROP TABLE IF EXISTS `notificationobjectchanges`;

CREATE TABLE `notificationobjectchanges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `verb` varchar(45) DEFAULT NULL,
  `actor` varchar(45) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT NULL,
  `notificationobject_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_notificationobjectchanges_notificationobjects1_idx` (`notificationobject_id`),
  CONSTRAINT `fk_notificationobjectchanges_notificationobjects1` FOREIGN KEY (`notificationobject_id`) REFERENCES `notificationobjects` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table notificationobjects
# ------------------------------------------------------------

DROP TABLE IF EXISTS `notificationobjects`;

CREATE TABLE `notificationobjects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `object_id` int(11) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT NULL,
  `notification_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_notificationobjects_notifications1_idx` (`notification_id`),
  CONSTRAINT `fk_notificationobjects_notifications1` FOREIGN KEY (`notification_id`) REFERENCES `notifications` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table notifications
# ------------------------------------------------------------

DROP TABLE IF EXISTS `notifications`;

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(45) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_notifications_users1_idx` (`user_id`),
  CONSTRAINT `fk_notifications_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table onetimepasswords
# ------------------------------------------------------------

DROP TABLE IF EXISTS `onetimepasswords`;

CREATE TABLE `onetimepasswords` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phonenumber` varchar(256) NOT NULL DEFAULT '',
  `otp` int(4) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table orderdetails
# ------------------------------------------------------------

DROP TABLE IF EXISTS `orderdetails`;

CREATE TABLE `orderdetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` int(11) DEFAULT NULL,
  `name` varchar(256) DEFAULT NULL,
  `email` varchar(256) DEFAULT NULL,
  `phonenumber` varchar(256) DEFAULT NULL,
  `ispaid` int(11) DEFAULT '0',
  `address_id` int(11) NOT NULL,
  `timestamp` timestamp NULL DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `cart_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_orderdetails_users1_idx` (`user_id`),
  KEY `fk_orderdetails_carts1_idx` (`cart_id`),
  CONSTRAINT `fk_orderdetails_carts1` FOREIGN KEY (`cart_id`) REFERENCES `carts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_orderdetails_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table orders
# ------------------------------------------------------------

DROP TABLE IF EXISTS `orders`;

CREATE TABLE `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `price` int(11) DEFAULT '0',
  `isshipped` int(11) DEFAULT '0',
  `shippeddate` datetime DEFAULT NULL,
  `isdelivered` int(11) DEFAULT '0',
  `deliverydate` datetime DEFAULT NULL,
  `ispaid` int(11) DEFAULT '0',
  `deliverycost` int(11) DEFAULT '0',
  `timestamp` varchar(45) DEFAULT NULL,
  `productitems_id` int(11) NOT NULL,
  `orderdetail_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_orders_products_vendors1_idx` (`productitems_id`),
  KEY `fk_orders_orderdetails1_idx` (`orderdetail_id`),
  CONSTRAINT `fk_orders_orderdetails1` FOREIGN KEY (`orderdetail_id`) REFERENCES `orderdetails` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table parentcategories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `parentcategories`;

CREATE TABLE `parentcategories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(512) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(512) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `parentcategories` WRITE;
/*!40000 ALTER TABLE `parentcategories` DISABLE KEYS */;

INSERT INTO `parentcategories` (`id`, `name`, `description`, `image`)
VALUES
	(1,'Home Appliances','Home Appliances',''),
	(2,'Furniture','Furniture',''),
	(3,'Cat','','');

/*!40000 ALTER TABLE `parentcategories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table productcategories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `productcategories`;

CREATE TABLE `productcategories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `description` text NOT NULL,
  `image` varchar(512) NOT NULL,
  `parentcategory_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `productcategories` WRITE;
/*!40000 ALTER TABLE `productcategories` DISABLE KEYS */;

INSERT INTO `productcategories` (`id`, `name`, `description`, `image`, `parentcategory_id`)
VALUES
	(3,'Refrigerators','Top Quality Refrigerators','fridge.jpg',1),
	(4,'Washing Machines','Top Quality Washing Machines','washingmachine.jpg',1),
	(5,'Air Conditioners','Top quality Air Conditioners','airconditioner.jpg',1),
	(6,'Water Purifiers','Top Quality Water Purifiers','waterpurifier.jpg',1),
	(7,'Sofa set','Sofa set at cheap price','sofaset.jpg',2),
	(8,'Office Chair','Best Quality Office Chairs','officechair.jpg',2),
	(9,'Computer Table','Compuer Tables at best price','computertable.jpg',2);

/*!40000 ALTER TABLE `productcategories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table productfilters
# ------------------------------------------------------------

DROP TABLE IF EXISTS `productfilters`;

CREATE TABLE `productfilters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filter_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table productitemfilters
# ------------------------------------------------------------

DROP TABLE IF EXISTS `productitemfilters`;

CREATE TABLE `productitemfilters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filter_id` int(11) NOT NULL,
  `productitem_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table productitems
# ------------------------------------------------------------

DROP TABLE IF EXISTS `productitems`;

CREATE TABLE `productitems` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qualityindex` int(11) DEFAULT '0',
  `title` varchar(512) NOT NULL,
  `description` varchar(256) DEFAULT NULL,
  `issold` int(11) DEFAULT '0',
  `isapproved` int(11) DEFAULT '0',
  `saleprice` int(11) DEFAULT '0',
  `manufacturingyear` int(11) NOT NULL,
  `warranty` varchar(256) NOT NULL,
  `vendorprice` int(11) DEFAULT '0',
  `ispaid` int(11) DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `isdelivered` int(11) DEFAULT '0',
  `productcategory_id` int(11) DEFAULT NULL,
  `color` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_products_vendors_users1_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `productitems` WRITE;
/*!40000 ALTER TABLE `productitems` DISABLE KEYS */;

INSERT INTO `productitems` (`id`, `qualityindex`, `title`, `description`, `issold`, `isapproved`, `saleprice`, `manufacturingyear`, `warranty`, `vendorprice`, `ispaid`, `user_id`, `product_id`, `timestamp`, `isdelivered`, `productcategory_id`, `color`)
VALUES
	(11,0,'LG single door','good one',0,1,6000,2011,'',0,0,22,3,'2016-04-25 08:38:13',0,3,'white'),
	(12,0,'Samsung Top Load','very good',0,1,5000,2013,'',0,0,22,4,'2016-04-26 02:33:43',0,4,'red'),
	(13,0,'LG Washing Machine','good',0,1,4000,2010,'',0,0,22,2,'2016-04-26 02:33:38',0,4,'blue'),
	(16,0,'Samsung Double Door','Single Door, Best cooling, Best looks',0,1,7900,2012,'',0,0,22,2,'2016-04-26 02:33:49',0,3,'grey');

/*!40000 ALTER TABLE `productitems` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table productrequests
# ------------------------------------------------------------

DROP TABLE IF EXISTS `productrequests`;

CREATE TABLE `productrequests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phonenumber` varchar(256) DEFAULT NULL,
  `productname` varchar(256) DEFAULT NULL,
  `model` varchar(45) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_productrequests_users1_idx` (`user_id`),
  CONSTRAINT `fk_productrequests_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table products
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `modelnumber` varchar(512) NOT NULL,
  `description` text,
  `shortdescription` varchar(512) NOT NULL,
  `instock` int(11) DEFAULT '0',
  `stockcount` int(11) DEFAULT '0',
  `origimage` varchar(256) DEFAULT NULL,
  `mrp` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;

INSERT INTO `products` (`id`, `name`, `modelnumber`, `description`, `shortdescription`, `instock`, `stockcount`, `origimage`, `mrp`)
VALUES
	(2,'LG single Door','lg101','Cool technology','Intellitech',1,0,NULL,14000),
	(3,'LG single door','lg102','good','good one',1,0,NULL,1233),
	(4,'samsung','sam123','sdfkj','good',1,0,NULL,12000),
	(5,'LG Model','mod123','nice','good one',1,0,NULL,15000);

/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table qualityindexes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `qualityindexes`;

CREATE TABLE `qualityindexes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(512) NOT NULL,
  `productcategory_id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `qualityindexes` WRITE;
/*!40000 ALTER TABLE `qualityindexes` DISABLE KEYS */;

INSERT INTO `qualityindexes` (`id`, `name`, `productcategory_id`, `timestamp`)
VALUES
	(1,'Age',3,'2016-03-12 05:26:38'),
	(2,'Door Seal',3,'2016-03-12 05:56:00'),
	(3,'Energy Efficiency',3,'2016-03-12 05:26:51'),
	(4,'Appearance',3,'2016-03-12 05:27:22'),
	(5,'Motor Condition',3,'2016-03-12 05:32:03'),
	(6,'Age',4,'2016-03-12 05:30:14'),
	(7,'Full Cycle Functionality',4,'2016-03-12 05:30:53'),
	(8,'Tub/Drum Condition',4,'2016-04-04 05:03:56'),
	(9,'Efficiency Grading',4,'2016-03-12 05:30:55'),
	(10,'Appearance',4,'2016-03-12 05:30:56'),
	(12,'Age',5,'2016-03-12 05:31:12'),
	(13,'Cooling Capacity',5,'2016-03-12 05:31:21'),
	(14,'Energy Efficiency',5,'2016-03-12 05:32:48'),
	(15,'Appearance',5,'2016-03-12 05:32:50'),
	(16,'Filters/Dehumidifiers',5,'2016-04-04 05:06:31'),
	(18,'Thermostat setting',5,'2016-04-04 05:07:19'),
	(19,'Age',6,'2016-03-12 05:33:46'),
	(20,'Filter Codition',6,'2016-03-12 05:33:46'),
	(21,'Appearance',6,'2016-03-12 05:33:46'),
	(22,'Interior Parts',6,'2016-03-12 05:33:47'),
	(23,'Other',6,'2016-03-12 05:33:49'),
	(24,'Age',7,'2016-03-12 05:34:45'),
	(25,'Cushion Condition',7,'2016-03-12 05:34:46'),
	(26,'Frame/Fabric Condition',7,'2016-04-04 05:08:45'),
	(27,'Spring Condition',7,'2016-03-12 05:43:12'),
	(28,'Appearance',7,'2016-03-12 05:43:36'),
	(29,'Other',7,'2016-03-12 05:43:40'),
	(30,'Defrosting Time',3,'2016-04-04 05:04:06'),
	(31,'Noise',3,'2016-04-04 05:05:18'),
	(32,'Adjustability',8,'2016-04-04 05:09:02'),
	(33,'Seat Material',8,'2016-04-04 05:09:14'),
	(34,'Mobility',8,'2016-04-04 05:09:27'),
	(35,'Desk Shape',9,'2016-04-04 05:09:43'),
	(36,'Desk Material',9,'2016-04-04 05:10:04');

/*!40000 ALTER TABLE `qualityindexes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table qualityindexvalues
# ------------------------------------------------------------

DROP TABLE IF EXISTS `qualityindexvalues`;

CREATE TABLE `qualityindexvalues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qualityindexes_id` int(11) NOT NULL,
  `value_text` varchar(512) NOT NULL DEFAULT '',
  `value` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `qualityindexvalues` WRITE;
/*!40000 ALTER TABLE `qualityindexvalues` DISABLE KEYS */;

INSERT INTO `qualityindexvalues` (`id`, `qualityindexes_id`, `value_text`, `value`)
VALUES
	(1,1,'>= 2014',5),
	(2,1,'2012 - 2014',4),
	(3,1,'2010 - 2012',3),
	(4,1,'2005 - 2010',2),
	(5,1,'< 2005',1),
	(6,2,'New Seal Installed',5),
	(7,2,'Good Working Seal',4),
	(8,2,'Average Seal',3),
	(9,2,'Bad Seal',2),
	(10,2,'Seal Damaged',1),
	(11,3,'Five Star',5),
	(12,3,'Four Star',4),
	(13,3,'Three Star',3),
	(14,3,'Two Star',2),
	(15,3,'< Two Star',1),
	(16,4,'Like New W/o Any Major Scratches or Dents',5),
	(17,4,'Good With Some Scratches and no Dents',4),
	(18,4,'Average With Some Dents and Scratches',3),
	(19,4,'Some Parts are Broken',2),
	(20,4,'Looks Bad',1),
	(21,5,'New Motor',5),
	(22,5,'Excellent Working Condition',4),
	(23,5,'Good Working Condition',3),
	(24,5,'Average',2),
	(25,5,'Bad',1),
	(26,6,'>= 2014',5),
	(27,6,'2012 - 2014',4),
	(28,6,'2005 - 2010',2),
	(30,6,'2010 - 2012',3),
	(31,6,'< 2005',1),
	(32,10,'Like New W/o Any Major Scratches or Dents',5),
	(33,10,'Good With Some Scratches and no Dents',4),
	(34,10,'Average With Some Dents and Scratches',3),
	(35,10,'Some Parts are Broken',2),
	(36,10,'Looks Bad',1),
	(37,8,'Like New',5),
	(38,8,'Good ',4),
	(39,8,'Average',3),
	(40,8,'Bad',2),
	(41,9,'5 star',5),
	(42,9,'4 star',4),
	(43,9,'3 star',3),
	(44,9,'2 star',2),
	(45,9,'1 star',1),
	(50,12,'>2015',5),
	(51,12,'2014 to 2015',4),
	(52,12,'2012 to 2014',3),
	(53,12,'2010 to 2012',2),
	(54,12,'<2010',1),
	(55,13,'Insta cooling',5),
	(56,13,'Good',4),
	(57,13,'Average',3),
	(58,13,'Poor',2),
	(59,14,'5 star',5),
	(60,14,'4 star',4),
	(61,14,'3 star',3),
	(62,14,'2 star',2),
	(63,14,'1 star',1),
	(64,15,'Like New',5),
	(65,15,'Good',4),
	(66,15,'Average',3),
	(67,15,'Bad',2),
	(68,16,'Excellent',5),
	(69,16,'Good',4),
	(70,16,'Average',3),
	(71,16,'Bad',2),
	(72,18,'Excellent Working condition',5),
	(73,18,'Good working condition',4),
	(74,18,'Average Working condition',3),
	(75,19,'> 2014',2),
	(76,20,'Excellent',5),
	(77,20,'Good',4),
	(78,20,'Average',3),
	(79,20,'Bad',2),
	(80,21,'Looks New w/o scratches/bent',5),
	(81,21,'Good',4),
	(82,21,'Average',3),
	(83,21,'Bad',2),
	(84,22,'Excellent',5),
	(85,22,'Good',4),
	(86,22,'Average',3),
	(87,22,'Bad',2),
	(88,23,'Excellent',5),
	(89,23,'Good',4),
	(90,23,'Average',3),
	(91,23,'Bad',2),
	(92,24,'>2015',5),
	(93,24,'2013 to 2015',4),
	(94,24,'2010 to 2013',3),
	(95,24,'<2010',2),
	(96,25,'Excellent',5),
	(97,25,'Good',4),
	(98,25,'Average',3),
	(99,25,'Bad',2),
	(100,26,'Excellent',5),
	(101,26,'Good',4),
	(102,26,'Average',3),
	(103,26,'Bad',2),
	(104,27,'Excellent',5),
	(105,27,'Good',4),
	(106,27,'Average',3),
	(107,27,'Bad',2),
	(108,28,'Excellent',5),
	(109,28,'Good',4),
	(110,28,'Average',3),
	(111,28,'Bad',2),
	(112,29,'Excellent',5),
	(113,29,'Good',4),
	(114,29,'Average',3),
	(115,29,'Bad',2),
	(116,30,'Fast',5),
	(117,30,'Good',4),
	(118,30,'Average',3),
	(119,30,'Bad',2),
	(120,31,'Zero Noise',5),
	(121,31,'very little noise',4),
	(122,31,'high noise',3),
	(123,32,'Easily Adjustable',5),
	(124,32,'Adjustable',4),
	(125,32,'very Tight',3),
	(126,33,'Excellent',5),
	(127,33,'Good',4),
	(128,33,'Average',3),
	(129,33,'Bad',2),
	(130,34,'Easily movable',5),
	(131,34,'Movable',4),
	(132,34,'Needs Lubrication',3),
	(133,34,'Tight',2),
	(134,35,'Excellent',5),
	(135,35,'Good',4),
	(136,35,'Average',3),
	(137,35,'Bad',2),
	(138,36,'Excellent',5),
	(139,36,'Good',4),
	(140,36,'Average',3),
	(141,36,'Bad',2),
	(142,7,'High',5),
	(143,7,'Good',4),
	(144,7,'Average',3),
	(145,7,'Bad',2),
	(146,19,'2012 - 2014',2),
	(147,19,'2010 - 2012',5),
	(148,19,'2005 - 2010',2),
	(149,19,'<2005',2);

/*!40000 ALTER TABLE `qualityindexvalues` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table qualityratings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `qualityratings`;

CREATE TABLE `qualityratings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` int(11) NOT NULL,
  `qualityindex_id` int(11) NOT NULL,
  `productitem_id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `description` varchar(256) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `qualityratings` WRITE;
/*!40000 ALTER TABLE `qualityratings` DISABLE KEYS */;

INSERT INTO `qualityratings` (`id`, `value`, `qualityindex_id`, `productitem_id`, `timestamp`, `description`)
VALUES
	(61,3,1,11,'2016-04-03 07:06:36',''),
	(62,4,2,11,'2016-04-03 07:06:36',''),
	(63,4,3,11,'2016-04-03 07:06:36',''),
	(64,4,4,11,'2016-04-03 07:06:36',''),
	(65,4,5,11,'2016-04-03 07:06:36',''),
	(66,4,1,12,'2016-04-05 14:10:26',''),
	(67,5,2,12,'2016-04-05 14:10:26',''),
	(68,3,3,12,'2016-04-05 14:10:26',''),
	(69,4,4,12,'2016-04-05 14:10:26',''),
	(70,4,5,12,'2016-04-05 14:10:26',''),
	(71,5,1,13,'2016-04-11 10:54:50',''),
	(72,4,2,13,'2016-04-11 10:54:50',''),
	(73,3,3,13,'2016-04-11 10:54:50',''),
	(74,4,4,13,'2016-04-11 10:54:50',''),
	(75,3,5,13,'2016-04-11 10:54:50',''),
	(76,5,1,14,'2016-04-19 03:24:31',''),
	(77,5,2,14,'2016-04-19 03:24:31',''),
	(78,4,3,14,'2016-04-19 03:24:31',''),
	(79,4,4,14,'2016-04-19 03:24:31',''),
	(80,4,5,14,'2016-04-19 03:24:31',''),
	(81,2,1,15,'2016-04-19 06:23:01',''),
	(82,1,2,15,'2016-04-19 06:23:01',''),
	(83,1,3,15,'2016-04-19 06:23:01',''),
	(84,1,4,15,'2016-04-19 06:23:01',''),
	(85,1,5,15,'2016-04-19 06:23:01',''),
	(86,4,1,16,'2016-04-19 19:38:47',''),
	(87,2,2,16,'2016-04-19 19:38:47',''),
	(88,5,3,16,'2016-04-19 19:38:47',''),
	(89,4,4,16,'2016-04-19 19:38:47',''),
	(90,1,5,16,'2016-04-19 19:38:47','');

/*!40000 ALTER TABLE `qualityratings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;

INSERT INTO `roles` (`id`, `name`, `description`)
VALUES
	(1,'login','Login privileges, granted after account confirmation'),
	(2,'admin','Administrative user, has access to everything.'),
	(3,'dealer','this is a delaer');

/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table roles_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `roles_users`;

CREATE TABLE `roles_users` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `fk_role_id` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `roles_users` WRITE;
/*!40000 ALTER TABLE `roles_users` DISABLE KEYS */;

INSERT INTO `roles_users` (`user_id`, `role_id`)
VALUES
	(1,1),
	(2,1),
	(4,1),
	(5,1),
	(6,1),
	(7,1),
	(8,1),
	(9,1),
	(10,1),
	(11,1),
	(12,1),
	(13,1),
	(14,1),
	(15,1),
	(16,1),
	(17,1),
	(18,1),
	(19,1),
	(20,1),
	(21,1),
	(22,1),
	(23,1),
	(24,1),
	(25,1),
	(26,1),
	(27,1),
	(28,1),
	(29,1),
	(30,1),
	(31,1),
	(32,1),
	(33,1),
	(34,1),
	(35,1),
	(36,1),
	(37,1),
	(38,1),
	(39,1),
	(40,1),
	(41,1),
	(42,1),
	(43,1),
	(44,1),
	(45,1),
	(46,1),
	(47,1),
	(48,1),
	(49,1),
	(50,1),
	(51,1),
	(52,1),
	(53,1),
	(54,1),
	(55,1),
	(56,1),
	(57,1),
	(58,1),
	(59,1),
	(60,1),
	(61,1),
	(62,1),
	(63,1),
	(64,1),
	(65,1),
	(66,1),
	(67,1),
	(68,1),
	(69,1),
	(70,1);

/*!40000 ALTER TABLE `roles_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table technicalspecs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `technicalspecs`;

CREATE TABLE `technicalspecs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(512) NOT NULL,
  `productcategory_id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `technicalspecs` WRITE;
/*!40000 ALTER TABLE `technicalspecs` DISABLE KEYS */;

INSERT INTO `technicalspecs` (`id`, `name`, `productcategory_id`, `timestamp`)
VALUES
	(1,'capacity',3,'2016-03-03 17:06:29');

/*!40000 ALTER TABLE `technicalspecs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table technicalspecvalues
# ------------------------------------------------------------

DROP TABLE IF EXISTS `technicalspecvalues`;

CREATE TABLE `technicalspecvalues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(256) NOT NULL,
  `technicalspec_id` int(11) NOT NULL,
  `productitem_id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `technicalspecvalues` WRITE;
/*!40000 ALTER TABLE `technicalspecvalues` DISABLE KEYS */;

INSERT INTO `technicalspecvalues` (`id`, `value`, `technicalspec_id`, `productitem_id`, `timestamp`)
VALUES
	(1,'200c',1,6,'2016-03-03 17:39:15');

/*!40000 ALTER TABLE `technicalspecvalues` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tickets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tickets`;

CREATE TABLE `tickets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` varchar(256) DEFAULT NULL,
  `subject` varchar(256) DEFAULT NULL,
  `isactive` int(11) DEFAULT '0',
  `date` datetime DEFAULT NULL,
  `tickettype_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tickets_tickettypes1_idx` (`tickettype_id`),
  KEY `fk_tickets_users1_idx` (`user_id`),
  CONSTRAINT `fk_tickets_tickettypes1` FOREIGN KEY (`tickettype_id`) REFERENCES `tickettypes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tickets_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tickettypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tickettypes`;

CREATE TABLE `tickettypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table user_tokens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_tokens`;

CREATE TABLE `user_tokens` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `user_agent` varchar(40) NOT NULL,
  `token` varchar(40) NOT NULL,
  `created` int(10) unsigned NOT NULL,
  `expires` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_token` (`token`),
  KEY `fk_user_id` (`user_id`),
  KEY `expires` (`expires`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `user_tokens` WRITE;
/*!40000 ALTER TABLE `user_tokens` DISABLE KEYS */;

INSERT INTO `user_tokens` (`id`, `user_id`, `user_agent`, `token`, `created`, `expires`)
VALUES
	(51,55,'0313573c03647e313bccc59fd03b9d9fa1d50619','e7503d5c2375e1c845495548170b0c48e4fdd7cb',1461561721,1466745721),
	(54,63,'0313573c03647e313bccc59fd03b9d9fa1d50619','f92bf6b95bd01bcd4a91437d07041b564b8eb034',1461566570,1466750570),
	(55,64,'0313573c03647e313bccc59fd03b9d9fa1d50619','e95575ea8304818f06b18b72f55ce4f739f630cc',1461568088,1466752088),
	(56,65,'0313573c03647e313bccc59fd03b9d9fa1d50619','c22aab7b213a85e9f62792b4d250698b3fba9165',1461568280,1466752280),
	(57,66,'0313573c03647e313bccc59fd03b9d9fa1d50619','a7dd09046575e0b2c444fb561ea1f133a0928593',1461568413,1466752413),
	(58,67,'0313573c03647e313bccc59fd03b9d9fa1d50619','239e119c445ed746e5c6a0e5084224cdc18b2e9a',1461568559,1466752559),
	(59,68,'0313573c03647e313bccc59fd03b9d9fa1d50619','c18510349661c2625805d01da1d724f5d9c37ca4',1461568972,1466752972),
	(60,69,'0313573c03647e313bccc59fd03b9d9fa1d50619','6df8a35f61a5cfcef9a3a6a6b3e744ba329a13c3',1461570129,1466754129),
	(61,70,'0313573c03647e313bccc59fd03b9d9fa1d50619','78314aa9d244ef166b9c7b36e7a9c37490de2cb6',1461570204,1466754204),
	(81,51,'0313573c03647e313bccc59fd03b9d9fa1d50619','48106b0248ae67b3a65a9f0e761e3aa067f05085',1461589373,1466773373),
	(82,22,'0313573c03647e313bccc59fd03b9d9fa1d50619','263c00e0ae166540fce225010e9b28efac489509',1461638480,1466822480);

/*!40000 ALTER TABLE `user_tokens` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(256) DEFAULT NULL,
  `username` varchar(256) DEFAULT NULL,
  `password` varchar(256) DEFAULT NULL,
  `phonenumber` varchar(256) DEFAULT NULL,
  `firstname` varchar(512) NOT NULL,
  `lastname` varchar(512) NOT NULL,
  `picture` varchar(256) NOT NULL,
  `isdealer` int(11) NOT NULL DEFAULT '0',
  `location` varchar(512) NOT NULL,
  `locationlat` varchar(512) NOT NULL,
  `locationlng` varchar(512) NOT NULL,
  `place_id` varchar(512) NOT NULL,
  `deliveryradius` int(11) NOT NULL,
  `hash` varchar(256) DEFAULT NULL,
  `isverified` int(11) DEFAULT '0',
  `logins` int(11) DEFAULT NULL,
  `last_login` int(11) DEFAULT NULL,
  `isadmin` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `email`, `username`, `password`, `phonenumber`, `firstname`, `lastname`, `picture`, `isdealer`, `location`, `locationlat`, `locationlng`, `place_id`, `deliveryradius`, `hash`, `isverified`, `logins`, `last_login`, `isadmin`)
VALUES
	(22,'dealer@dealer.com','123','f6667c9d18d10065e9bcebcd8761c9affe7a78bf49b13544022b87aca4b47a17','123','Dealer_1','','none',1,'','','','',0,NULL,1,NULL,1461638480,0),
	(23,'admin@admin.com','1234','290988abfd30b4b800ddbc09b953da98e8361a1a775e385111795185c60f7b87','1234','Admin','Admin','none',0,'','','','',0,NULL,1,NULL,1461036303,1),
	(51,'vineethkumart@gmail.com','9538092344','e19ead0e6fd98035d3ee8082cda6f041565e0127e8058eacaec8178348b21512','9538092344','Vineeth','Kumar','none',0,'','','','',0,NULL,1,NULL,1461589373,0),
	(52,'durai@gmail.com','9944951838','0014cd8ac068b3b7ef890453cdfd69cd04b378594e435400534a59f799bed0bf','9944951838','Durai','Gowardhan','none',0,'','','','',0,NULL,1,NULL,1458228954,0),
	(53,'varma@gmail.com','12345','2b0574df8dd7cec5712fce70568744126af8e06089036f0181bfcedf50f76a8c','12345','Varma','V','none',0,'','','','',0,NULL,0,NULL,NULL,0),
	(54,'sow@gmail.com','9741039233','b677038ac39ab1e2762a0756e169a6b5a6e354f6850d655fbfa7f9cdce206382','9741039233','Sowbarani','S','none',0,'','','','',0,NULL,0,NULL,NULL,0),
	(55,'anagha@gmail.com','8086105326','e219aead4a2a12a7251dbc4d7d6f39fa6ba97daf920a1fc0f5af4c7bafbb3e8b','8086105326','Anagha','Vineeth','none',0,'','','','',0,NULL,1,NULL,1461561721,0),
	(70,'ass.hole@gmail.com','9999222234','e19ead0e6fd98035d3ee8082cda6f041565e0127e8058eacaec8178348b21512','9999222234','Ass','Hol','none',0,'','','','',0,NULL,1,NULL,1461570204,0);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wishlists
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wishlists`;

CREATE TABLE `wishlists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  `productitem_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `wishlists` WRITE;
/*!40000 ALTER TABLE `wishlists` DISABLE KEYS */;

INSERT INTO `wishlists` (`id`, `timestamp`, `user_id`, `productitem_id`)
VALUES
	(4,'2016-04-26 02:23:51',51,12),
	(5,'2016-04-26 02:24:21',51,16),
	(6,'2016-04-26 02:24:48',51,13),
	(7,'2016-04-26 02:24:50',51,11);

/*!40000 ALTER TABLE `wishlists` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
