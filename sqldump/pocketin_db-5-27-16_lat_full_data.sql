# ************************************************************
# Sequel Pro SQL dump
# Version 4529
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: pocketin.c1f5oyiblvfj.us-west-2.rds.amazonaws.com (MySQL 5.5.5-10.0.17-MariaDB-log)
# Database: pocketin_db
# Generation Time: 2016-05-27 12:34:11 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table activitylogs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `activitylogs`;

CREATE TABLE `activitylogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `object_id` int(11) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_activitylog_users1_idx` (`user_id`),
  CONSTRAINT `fk_activitylog_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `activitylogs` WRITE;
/*!40000 ALTER TABLE `activitylogs` DISABLE KEYS */;

INSERT INTO `activitylogs` (`id`, `object_id`, `type`, `timestamp`, `user_id`)
VALUES
	(1,1,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-22 16:08:01',2),
	(2,1,'APPROVED_PRODUCT','2016-05-22 16:11:50',2),
	(3,2,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-22 16:16:26',2),
	(4,2,'APPROVED_PRODUCT','2016-05-22 16:19:51',2),
	(5,3,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-22 16:22:09',2),
	(6,3,'APPROVED_PRODUCT','2016-05-22 16:25:33',2),
	(7,4,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-22 16:26:24',2),
	(8,4,'APPROVED_PRODUCT','2016-05-22 16:29:02',2),
	(9,5,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-22 16:30:03',2),
	(10,5,'APPROVED_PRODUCT','2016-05-22 16:31:13',2),
	(11,6,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-22 16:34:14',2),
	(12,6,'APPROVED_PRODUCT','2016-05-22 16:38:59',2),
	(13,7,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-22 16:41:48',2),
	(14,7,'APPROVED_PRODUCT','2016-05-22 16:43:47',2),
	(15,8,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-22 16:48:40',2),
	(16,8,'APPROVED_PRODUCT','2016-05-22 16:55:03',2),
	(17,NULL,'DEALER_ADD_PRODUCT_INIT','2016-05-22 16:57:11',2),
	(18,NULL,'DEALER_ADD_PRODUCT_INIT','2016-05-22 16:58:44',2),
	(19,9,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-22 17:01:10',2),
	(20,9,'APPROVED_PRODUCT','2016-05-22 17:09:01',2),
	(21,10,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-22 17:11:14',2),
	(22,10,'APPROVED_PRODUCT','2016-05-22 17:12:55',2),
	(23,11,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-22 17:18:23',2),
	(24,11,'APPROVED_PRODUCT','2016-05-22 17:21:59',2),
	(25,12,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-22 17:24:14',2),
	(26,12,'APPROVED_PRODUCT','2016-05-22 17:30:30',2),
	(27,13,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-23 02:31:12',2),
	(28,13,'APPROVED_PRODUCT','2016-05-23 02:34:57',2),
	(29,14,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-23 02:36:44',2),
	(30,14,'APPROVED_PRODUCT','2016-05-23 02:44:47',2),
	(31,15,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-23 02:52:51',2),
	(32,15,'APPROVED_PRODUCT','2016-05-23 02:59:30',2),
	(33,16,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-23 03:01:17',2),
	(34,16,'APPROVED_PRODUCT','2016-05-23 03:06:28',2),
	(35,17,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-23 03:09:08',2),
	(36,17,'APPROVED_PRODUCT','2016-05-23 03:12:37',2),
	(37,18,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-23 03:15:41',2),
	(38,18,'APPROVED_PRODUCT','2016-05-23 03:17:47',2),
	(39,19,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-23 03:21:41',2),
	(40,19,'APPROVED_PRODUCT','2016-05-23 03:26:54',2),
	(41,20,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-23 03:29:38',2),
	(42,20,'APPROVED_PRODUCT','2016-05-23 03:33:39',2),
	(43,21,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-23 03:43:30',2),
	(44,21,'APPROVED_PRODUCT','2016-05-23 03:46:38',2),
	(45,22,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-23 03:48:41',2),
	(46,22,'APPROVED_PRODUCT','2016-05-23 03:50:53',2),
	(47,23,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-23 03:53:09',2),
	(48,23,'APPROVED_PRODUCT','2016-05-23 03:55:00',2),
	(49,24,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-23 03:58:49',2),
	(50,24,'APPROVED_PRODUCT','2016-05-23 04:00:28',2),
	(51,25,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-23 09:19:03',2),
	(52,25,'APPROVED_PRODUCT','2016-05-23 09:21:04',2),
	(53,26,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-23 13:47:57',2),
	(54,26,'APPROVED_PRODUCT','2016-05-23 13:51:24',2),
	(55,27,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-24 07:13:51',1),
	(56,27,'APPROVED_PRODUCT','2016-05-24 07:16:48',1),
	(57,28,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-24 12:30:55',2),
	(58,28,'APPROVED_PRODUCT','2016-05-24 12:32:22',2),
	(59,29,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-24 12:58:06',2),
	(60,29,'APPROVED_PRODUCT','2016-05-24 13:00:18',2),
	(61,30,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-24 15:31:44',2),
	(62,30,'APPROVED_PRODUCT','2016-05-24 15:40:37',2),
	(63,31,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-26 10:33:05',2),
	(64,32,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-26 10:36:16',2),
	(65,32,'APPROVED_PRODUCT','2016-05-26 10:44:17',2),
	(66,31,'APPROVED_PRODUCT','2016-05-26 10:44:18',2),
	(67,33,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-26 11:20:41',2),
	(68,33,'APPROVED_PRODUCT','2016-05-26 11:23:19',2),
	(69,34,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-26 11:51:07',2),
	(70,34,'APPROVED_PRODUCT','2016-05-26 11:55:36',2),
	(71,44,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-27 02:33:36',3),
	(72,54,'DEALER_ADD_PRODUCT_COMPLETE','2016-05-27 02:35:19',3),
	(73,35,'APPROVED_PRODUCT','2016-05-27 02:40:24',1),
	(74,36,'APPROVED_PRODUCT','2016-05-27 02:44:17',1),
	(75,37,'APPROVED_PRODUCT','2016-05-27 02:48:49',1),
	(76,38,'APPROVED_PRODUCT','2016-05-27 02:50:34',1),
	(77,39,'APPROVED_PRODUCT','2016-05-27 02:51:21',1),
	(78,40,'APPROVED_PRODUCT','2016-05-27 02:52:36',1),
	(79,41,'APPROVED_PRODUCT','2016-05-27 02:53:48',1),
	(80,42,'APPROVED_PRODUCT','2016-05-27 02:54:40',1),
	(81,43,'APPROVED_PRODUCT','2016-05-27 02:58:58',1),
	(82,44,'APPROVED_PRODUCT','2016-05-27 03:00:07',1),
	(83,45,'APPROVED_PRODUCT','2016-05-27 03:02:21',1),
	(84,46,'APPROVED_PRODUCT','2016-05-27 03:04:21',1),
	(85,47,'APPROVED_PRODUCT','2016-05-27 03:06:25',1),
	(86,50,'APPROVED_PRODUCT','2016-05-27 03:10:05',1),
	(87,49,'APPROVED_PRODUCT','2016-05-27 03:10:22',1),
	(88,48,'APPROVED_PRODUCT','2016-05-27 03:10:27',1),
	(89,54,'APPROVED_PRODUCT','2016-05-27 03:11:35',3),
	(90,53,'APPROVED_PRODUCT','2016-05-27 03:11:39',3),
	(91,52,'APPROVED_PRODUCT','2016-05-27 03:11:40',3),
	(92,51,'APPROVED_PRODUCT','2016-05-27 03:11:41',3);

/*!40000 ALTER TABLE `activitylogs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table addresses
# ------------------------------------------------------------

DROP TABLE IF EXISTS `addresses`;

CREATE TABLE `addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `address_line_one` varchar(512) NOT NULL,
  `address_line_two` varchar(512) NOT NULL,
  `city` varchar(256) NOT NULL,
  `state` varchar(256) NOT NULL,
  `country` varchar(256) NOT NULL,
  `pincode` varchar(256) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `landmark` varchar(256) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `name` varchar(100) NOT NULL DEFAULT '',
  `phonenumber` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `addresses` WRITE;
/*!40000 ALTER TABLE `addresses` DISABLE KEYS */;

INSERT INTO `addresses` (`id`, `user_id`, `address_line_one`, `address_line_two`, `city`, `state`, `country`, `pincode`, `timestamp`, `landmark`, `email`, `name`, `phonenumber`)
VALUES
	(1,5,'murugesapalaya','','Bangalore','Karnataka','India','560017','2016-05-23 05:32:27','top','duraigowardhan.s@gmail.com','durai gowardhan','9944951838'),
	(2,13,'No 09/108,channakeshava nilaya, Opp to shell petrol bunk, Seegehalli , kadugodi post, Bangalore 560067','1st floor,Flat no. 108','Bangalore','Karnataka','India','560067','2016-05-23 11:47:20','Opp to shell petrol bunk','ankuraman.mishra@gmail.com','ankur mishra','7795847342'),
	(3,18,'N V RESIDENCY,FLAT NO-5','DODDATHOGUR ROAD, ELECTRONIC CITY PHASE-1','Bangalore','Karnataka','India','560100','2016-05-23 12:55:10','NEAR LAXMI NARSHIMA TEMPLE- AUTO STAND','ruchi21091@gmail.com','ruchismita chakraborty','9886382769'),
	(4,26,'Apartment No. B-502, BREN AVALON, Doddannekundi Extension,','Chinnapanahalli Main Road,','Bangalore','Karnataka','India','560048','2016-05-23 14:45:08','Near bigbasket warehouse','sujith10.cea@gmail.com','sujith kumar','7406926385'),
	(5,27,'Brigade Metropolis, Garudacharpalya','Mahadevapura, Whitefield road','Bangalore','Karnataka','India','560048','2016-05-24 05:28:26','Phoenix Market City','reechabansal@yahoo.com','Reecha Bansal','9742600477'),
	(6,42,'E1404, Smondo 3','Neotown, Electronic City Phase 1','Bangalore','Karnataka','India','560102','2016-05-24 05:46:17','Starlit Suites','haritgkumar@gmail.com','Harit Gulati','8290539343'),
	(7,15,'#5, (Second Floor, First Portion), 1st \'B\' Main Road, Chandra Reddy Layout, Behind Maharaja Hotel, 4th Block, Koramangala, Bangalore - 560034','','Bangalore','Karnataka','India','560034','2016-05-24 08:43:26','Near Maharaja Signal','aradhanasngh5@gmail.com','Aradhana Singh','8981542972'),
	(8,48,'FT- 151, F2 Block, Ittina Neela Appartments,','Andapura Village, Electronic City','Bangalore','Karnataka','India','560100','2016-05-24 10:12:52','Near Gold Coin Club, Glass Factory Layout','vips.wipro@gmail.com','Vipin Mohan','8971777887'),
	(9,54,'narayan appartment, Dodda tagore','electronic city phase 1 , konop agarahara,','Bangalore','Karnataka','India','560100','2016-05-24 12:27:11','near mm school','avikm01@gmail.com','Avik mukherjee','9836033457'),
	(10,55,'Flat No. 38, Room No. 9 4th Floor, 20th Cross Ejipura Main Road','KMR Building','Bangalore','Karnataka','India','560047','2016-05-24 13:07:35','Near Rama Temple','subratasrk1940@gmail.com','Subrata Sarkar','7411063991'),
	(11,21,'#426, 1st CROS , 4th MAIN','Vijaya Layout','Bangalore','Karnataka','India','560076','2016-05-25 11:41:10','Opposite Diana Memorial High School','26anoop1990@gmail.com','Anoop K','9686482400'),
	(12,62,'Shri laxmi nivas , #47 , 1st mn , 2nd crs, apparao layout, rammurthy nagar -','','Bangalore','Karnataka','India','560016','2016-05-26 06:03:10','more supermarket near fresh heritage opposite lane','peravelli8@gmail.com','purna eravelli','7776933090'),
	(13,63,'Flat s-2, Block-1, Wild grass Apts ,','Nirguna Mandir Layout , S.T.Bed , Koramangala','Bangalore','Karnataka','India','560047','2016-05-26 12:53:03','Koramangala near gem regency','kritishmani@gmail.com','Manikandan Balasubramanian','9513407060'),
	(14,3,'VS Chalet','LBS Nagar','Bangalore','Karnataka','India','560017','2016-05-26 17:49:09','Near Nilgris','vineethkumart@gmail.com','Vineeth Kumar','9538092344');

/*!40000 ALTER TABLE `addresses` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cartitems
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cartitems`;

CREATE TABLE `cartitems` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cart_id` int(11) unsigned NOT NULL,
  `item_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `cartitems` WRITE;
/*!40000 ALTER TABLE `cartitems` DISABLE KEYS */;

INSERT INTO `cartitems` (`id`, `cart_id`, `item_id`)
VALUES
	(2,6,18),
	(3,14,25),
	(5,22,17),
	(7,27,13),
	(8,29,14),
	(9,35,3),
	(10,15,20),
	(12,31,8),
	(13,45,8),
	(14,46,26),
	(15,47,6),
	(16,52,20),
	(18,54,27),
	(19,58,1),
	(20,61,23),
	(21,65,5),
	(22,66,28),
	(23,69,29),
	(24,74,26),
	(25,77,6),
	(26,53,21),
	(27,79,33),
	(28,3,32),
	(29,3,31),
	(30,81,34),
	(31,88,34);

/*!40000 ALTER TABLE `cartitems` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table carts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `carts`;

CREATE TABLE `carts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `ispurchased` tinyint(1) DEFAULT '0',
  `isexpired` tinyint(1) DEFAULT '0',
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `carts` WRITE;
/*!40000 ALTER TABLE `carts` DISABLE KEYS */;

INSERT INTO `carts` (`id`, `user_id`, `ispurchased`, `isexpired`, `timestamp`)
VALUES
	(1,1,0,0,'2016-05-22 14:27:54'),
	(2,2,0,0,'2016-05-22 14:29:03'),
	(3,3,1,0,'2016-05-23 04:16:15'),
	(4,4,0,0,'2016-05-23 05:17:52'),
	(5,5,0,0,'2016-05-23 05:32:11'),
	(6,5,1,0,'2016-05-23 05:32:29'),
	(7,6,0,0,'2016-05-23 06:23:59'),
	(8,8,0,0,'2016-05-23 09:59:24'),
	(9,9,0,0,'2016-05-23 10:00:28'),
	(10,10,0,0,'2016-05-23 10:29:24'),
	(11,11,0,0,'2016-05-23 11:10:24'),
	(12,12,0,0,'2016-05-23 11:22:09'),
	(13,13,0,0,'2016-05-23 11:42:36'),
	(14,13,1,0,'2016-05-23 11:47:33'),
	(15,15,1,0,'2016-05-23 12:03:10'),
	(16,16,0,0,'2016-05-23 12:09:51'),
	(17,17,0,0,'2016-05-23 12:16:28'),
	(18,18,0,0,'2016-05-23 12:24:48'),
	(19,19,0,0,'2016-05-23 12:36:06'),
	(20,20,0,0,'2016-05-23 12:43:53'),
	(21,21,0,0,'2016-05-23 12:54:05'),
	(22,18,1,0,'2016-05-23 12:55:33'),
	(24,22,0,0,'2016-05-23 13:16:15'),
	(25,23,0,0,'2016-05-23 13:18:29'),
	(26,24,0,0,'2016-05-23 13:19:58'),
	(27,5,1,0,'2016-05-23 13:20:30'),
	(28,25,0,0,'2016-05-23 13:21:06'),
	(29,5,1,0,'2016-05-23 13:28:53'),
	(30,26,0,0,'2016-05-23 14:28:04'),
	(31,27,0,0,'2016-05-23 14:34:27'),
	(32,28,0,0,'2016-05-23 14:35:57'),
	(33,29,0,0,'2016-05-23 14:41:42'),
	(34,30,0,0,'2016-05-23 14:44:53'),
	(35,26,1,0,'2016-05-23 14:45:24'),
	(36,31,0,0,'2016-05-23 14:55:41'),
	(37,32,0,0,'2016-05-23 14:59:22'),
	(38,33,0,0,'2016-05-23 15:32:50'),
	(39,34,0,0,'2016-05-23 15:34:58'),
	(40,35,0,0,'2016-05-23 16:01:26'),
	(41,37,0,0,'2016-05-23 16:32:05'),
	(42,39,0,0,'2016-05-23 17:59:57'),
	(43,41,0,0,'2016-05-24 03:47:26'),
	(44,42,0,0,'2016-05-24 05:00:22'),
	(45,27,1,0,'2016-05-24 05:28:40'),
	(46,42,1,0,'2016-05-24 05:49:37'),
	(47,42,1,0,'2016-05-24 05:50:46'),
	(48,43,0,0,'2016-05-24 06:15:58'),
	(49,44,0,0,'2016-05-24 06:19:53'),
	(50,45,0,0,'2016-05-24 06:42:43'),
	(51,46,0,0,'2016-05-24 06:56:03'),
	(52,15,1,0,'2016-05-24 08:43:35'),
	(53,47,0,0,'2016-05-24 09:19:39'),
	(54,15,1,0,'2016-05-24 09:28:54'),
	(55,48,0,0,'2016-05-24 09:41:34'),
	(56,49,0,0,'2016-05-24 10:04:53'),
	(57,15,0,0,'2016-05-24 10:10:08'),
	(58,48,1,0,'2016-05-24 10:20:45'),
	(59,50,0,0,'2016-05-24 10:23:25'),
	(60,51,0,0,'2016-05-24 10:26:04'),
	(61,5,1,0,'2016-05-24 10:39:54'),
	(62,52,0,0,'2016-05-24 11:21:28'),
	(63,53,0,0,'2016-05-24 11:51:27'),
	(64,54,0,0,'2016-05-24 12:25:31'),
	(65,54,1,0,'2016-05-24 12:27:13'),
	(66,54,1,0,'2016-05-24 12:32:40'),
	(67,55,0,0,'2016-05-24 12:42:50'),
	(68,56,0,0,'2016-05-24 12:58:40'),
	(69,55,1,0,'2016-05-24 13:08:32'),
	(70,57,0,0,'2016-05-24 14:39:24'),
	(71,58,0,0,'2016-05-24 16:19:35'),
	(72,59,0,0,'2016-05-24 17:32:36'),
	(73,60,0,0,'2016-05-24 18:39:02'),
	(74,21,1,0,'2016-05-25 11:41:13'),
	(75,61,0,0,'2016-05-25 14:24:31'),
	(76,62,0,0,'2016-05-26 03:41:20'),
	(77,62,1,0,'2016-05-26 06:03:14'),
	(78,63,0,0,'2016-05-26 12:46:51'),
	(79,63,1,0,'2016-05-26 12:53:06'),
	(80,64,0,0,'2016-05-26 14:01:05'),
	(81,65,0,0,'2016-05-26 14:51:50'),
	(82,66,0,0,'2016-05-26 15:04:17'),
	(83,67,0,0,'2016-05-26 17:45:34'),
	(84,3,0,0,'2016-05-26 17:57:01'),
	(85,68,0,0,'2016-05-27 02:01:26'),
	(86,69,0,0,'2016-05-27 11:21:19'),
	(87,70,0,0,'2016-05-27 11:28:57'),
	(88,26,1,0,'2016-05-27 12:28:08');

/*!40000 ALTER TABLE `carts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table filters
# ------------------------------------------------------------

DROP TABLE IF EXISTS `filters`;

CREATE TABLE `filters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filtertype_id` int(11) NOT NULL,
  `name` varchar(512) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `filters` WRITE;
/*!40000 ALTER TABLE `filters` DISABLE KEYS */;

INSERT INTO `filters` (`id`, `filtertype_id`, `name`, `timestamp`)
VALUES
	(1,1,'Double Door','2016-03-03 21:19:13'),
	(2,1,'Single Door','2016-03-03 21:19:19'),
	(3,2,'below Rs.5000','2016-03-26 15:36:23'),
	(4,2,'Rs.5000 to 10000','2016-03-26 15:36:30'),
	(5,3,'below 200 lts','2016-03-26 15:35:44'),
	(6,3,'above 200 lts','2016-03-26 15:35:55'),
	(7,4,'Red','2016-03-26 14:29:11'),
	(8,4,'Blue','2016-03-26 14:29:13'),
	(9,4,'white','2016-03-26 14:29:15'),
	(10,4,'other colours','2016-03-26 14:29:22'),
	(11,5,'less than 3 star','2016-03-26 14:38:39'),
	(12,5,'3 star and above','2016-03-26 14:54:34'),
	(13,6,'Fully Automatic','2016-03-26 15:29:46'),
	(14,6,'Semi Automatic','2016-03-26 15:29:57'),
	(15,7,'Front Load','2016-03-26 15:30:13'),
	(16,7,'Top Load','2016-03-26 15:30:21'),
	(17,8,'6Kg and below','2016-03-26 15:34:25'),
	(18,8,'Above 6Kg','2016-03-26 15:34:22'),
	(19,9,'Split','2016-03-26 16:09:17'),
	(20,9,'Window','2016-03-26 16:09:27'),
	(21,10,'Less than 2 tonnes','2016-03-26 16:11:00'),
	(22,10,'2 tonnes & above','2016-03-26 16:11:11'),
	(23,11,'Less than 3 star','2016-03-26 16:11:27'),
	(24,11,'3 star & above','2016-03-26 16:11:38'),
	(25,12,'Electrical','2016-03-26 16:12:30'),
	(26,12,'Non-Electrical','2016-03-26 16:12:50'),
	(27,13,'Gravity based','2016-03-26 16:14:08'),
	(28,13,'RO+UV based','2016-03-26 16:19:22'),
	(29,14,'7Lts and below','2016-03-26 16:19:58'),
	(30,14,'Above 7Lts','2016-03-26 16:20:17'),
	(31,15,'3 Seater & below ','2016-03-26 16:25:10'),
	(32,15,'Above 3 Seater','2016-03-26 16:25:28'),
	(33,16,'Fabric','2016-03-26 16:25:44'),
	(34,16,'Leather','2016-03-26 16:25:53'),
	(35,16,'Wood','2016-03-26 16:26:22'),
	(36,16,'Metal','2016-03-26 16:26:34'),
	(37,17,'Straight','2016-03-26 16:26:58'),
	(38,17,'L & U Shaped','2016-03-26 16:27:13'),
	(39,18,'Beige','2016-03-26 16:27:56'),
	(40,18,'Black','2016-03-26 16:28:02'),
	(41,18,'Other Colors','2016-03-26 16:28:18'),
	(42,19,'Plastic','2016-03-26 16:31:46'),
	(43,19,'Fabric','2016-03-26 16:31:52'),
	(44,19,'Leatherette','2016-03-26 16:32:01'),
	(45,20,'Black','2016-03-26 16:33:37'),
	(46,20,'Beige','2016-03-26 16:33:53'),
	(47,20,'Other colors','2016-03-26 16:33:59'),
	(48,21,'Without Wheels ','2016-03-26 16:35:36'),
	(49,21,'With Seat Lock & Height Adjustment','2016-03-26 16:35:12'),
	(50,22,'Yes','2016-03-26 16:40:15'),
	(51,22,'No','2016-03-26 16:40:20'),
	(52,23,'Yes','2016-03-26 16:40:27'),
	(53,23,'No','2016-03-26 16:40:33');

/*!40000 ALTER TABLE `filters` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table filtertypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `filtertypes`;

CREATE TABLE `filtertypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `productcategory_id` int(11) NOT NULL,
  `name` varchar(512) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `filtertypes` WRITE;
/*!40000 ALTER TABLE `filtertypes` DISABLE KEYS */;

INSERT INTO `filtertypes` (`id`, `productcategory_id`, `name`, `timestamp`)
VALUES
	(1,3,'Door Type','2016-03-03 20:48:31'),
	(2,3,'Price','2016-03-26 14:23:04'),
	(3,3,'Capacity','2016-03-26 14:23:07'),
	(4,3,'Color','2016-03-26 14:23:09'),
	(5,3,'Energy Rating','2016-03-26 14:40:20'),
	(6,4,'Function Type','2016-03-26 14:50:14'),
	(7,4,'Load Type','2016-03-26 14:50:33'),
	(8,4,'Capacity','2016-03-26 15:26:46'),
	(9,5,'Type','2016-03-26 15:37:14'),
	(10,5,'Capacity','2016-03-26 15:37:27'),
	(11,5,'Energy Efficency','2016-03-26 15:37:41'),
	(12,6,'Type','2016-03-26 15:38:24'),
	(13,6,'Purification Technology','2016-03-26 15:38:34'),
	(14,6,'Capacity','2016-03-26 15:38:52'),
	(15,7,'Seating Capacity','2016-03-26 16:22:02'),
	(16,7,'Material','2016-03-26 16:22:30'),
	(17,7,'Shape','2016-03-26 16:22:40'),
	(18,7,'Color','2016-03-26 16:22:48'),
	(19,8,'Material','2016-03-26 16:28:59'),
	(20,8,'Color','2016-03-26 16:29:05'),
	(21,8,'Type','2016-03-26 16:30:11'),
	(22,9,'Wheels Included','2016-03-26 16:39:28'),
	(23,9,'Storage Included','2016-03-26 16:40:53');

/*!40000 ALTER TABLE `filtertypes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table images
# ------------------------------------------------------------

DROP TABLE IF EXISTS `images`;

CREATE TABLE `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `description` text,
  `thumbnail` varchar(512) DEFAULT '',
  `isdefault` int(11) DEFAULT '0',
  `productitem_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_images_products_vendors1_idx` (`productitem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `images` WRITE;
/*!40000 ALTER TABLE `images` DISABLE KEYS */;

INSERT INTO `images` (`id`, `name`, `description`, `thumbnail`, `isdefault`, `productitem_id`)
VALUES
	(1,'1_1.jpg','Image 1','',1,1),
	(2,'1_2.jpg','Image 2','',0,1),
	(3,'1_3.jpg','Image 3','',0,1),
	(4,'1_4.jpg','Image 4','',0,1),
	(5,'1_5.jpg','Image 5','',0,1),
	(6,'2_1.jpg','Image 1','',1,2),
	(7,'2_2.jpg','Image 2','',0,2),
	(8,'2_3.jpg','Image 3','',0,2),
	(9,'2_4.jpg','Image 4','',0,2),
	(10,'2_5.jpg','Image 5','',0,2),
	(11,'3_1.jpg','Image 1','',1,3),
	(12,'3_2.jpg','Image 2','',0,3),
	(13,'3_3.jpg','Image 3','',0,3),
	(14,'3_4.jpg','Image 4','',0,3),
	(15,'3_5.jpg','Image 5','',0,3),
	(16,'4_1.jpg','Image 1','',1,4),
	(17,'4_2.jpg','Image 2','',0,4),
	(18,'4_3.jpg','Image 3','',0,4),
	(19,'4_4.jpg','Image 4','',0,4),
	(20,'4_5.jpg','Image 5','',0,4),
	(21,'4_6.jpg','Image 6','',0,4),
	(22,'5_1.jpg','Image 1','',1,5),
	(23,'5_2.jpg','Image 2','',0,5),
	(24,'5_3.jpg','Image 3','',0,5),
	(25,'5_4.jpg','Image 4','',0,5),
	(26,'5_5.jpg','Image 5','',0,5),
	(27,'5_6.jpg','Image 6','',0,5),
	(28,'6_1.jpg','Image 1','',1,6),
	(29,'6_2.jpg','Image 2','',0,6),
	(30,'6_3.jpg','Image 3','',0,6),
	(31,'6_4.jpg','Image 4','',0,6),
	(32,'6_5.jpg','Image 5','',0,6),
	(33,'7_1.jpg','Image 1','',1,7),
	(34,'7_2.jpg','Image 2','',0,7),
	(35,'7_3.jpg','Image 3','',0,7),
	(36,'7_4.jpg','Image 4','',0,7),
	(37,'7_5.jpg','Image 5','',0,7),
	(38,'7_6.jpg','Image 6','',0,7),
	(39,'7_7.jpg','Image 7','',0,7),
	(40,'8_1.jpg','Image 1','',1,8),
	(41,'8_2.jpg','Image 2','',0,8),
	(42,'8_3.jpg','Image 3','',0,8),
	(43,'8_4.jpg','Image 4','',0,8),
	(44,'8_5.jpg','Image 5','',0,8),
	(45,'9_1.jpg','Image 1','',1,9),
	(46,'9_2.jpg','Image 2','',0,9),
	(47,'9_3.jpg','Image 3','',0,9),
	(48,'9_4.jpg','Image 4','',0,9),
	(49,'9_5.jpg','Image 5','',0,9),
	(50,'10_1.jpg','Image 1','',1,10),
	(51,'10_2.jpg','Image 2','',0,10),
	(52,'10_3.jpg','Image 3','',0,10),
	(53,'10_4.jpg','Image 4','',0,10),
	(54,'11_1.jpg','Image 1','',1,11),
	(55,'11_2.jpg','Image 2','',0,11),
	(56,'11_3.jpg','Image 3','',0,11),
	(57,'11_4.jpg','Image 4','',0,11),
	(58,'11_5.jpg','Image 5','',0,11),
	(59,'11_6.jpg','Image 6','',0,11),
	(60,'11_7.jpg','Image 7','',0,11),
	(61,'12_1.jpg','Image 1','',1,12),
	(62,'12_2.jpg','Image 2','',0,12),
	(63,'12_3.jpg','Image 3','',0,12),
	(64,'12_4.jpg','Image 4','',0,12),
	(65,'12_5.jpg','Image 5','',0,12),
	(66,'12_6.jpg','Image 6','',0,12),
	(67,'13_1.jpg','Image 1','',1,13),
	(68,'13_2.jpg','Image 2','',0,13),
	(69,'13_3.jpg','Image 3','',0,13),
	(70,'13_4.jpg','Image 4','',0,13),
	(71,'13_5.jpg','Image 5','',0,13),
	(72,'14_1.jpg','Image 1','',1,14),
	(73,'14_2.jpg','Image 2','',0,14),
	(74,'14_3.jpg','Image 3','',0,14),
	(75,'14_4.jpg','Image 4','',0,14),
	(76,'14_5.jpg','Image 5','',0,14),
	(77,'14_6.jpg','Image 6','',0,14),
	(78,'15_1.jpg','Image 1','',1,15),
	(79,'15_2.jpg','Image 2','',0,15),
	(80,'15_3.jpg','Image 3','',0,15),
	(81,'15_4.jpg','Image 4','',0,15),
	(82,'15_5.jpg','Image 5','',0,15),
	(83,'15_6.jpg','Image 6','',0,15),
	(84,'16_1.jpg','Image 1','',1,16),
	(85,'16_2.jpg','Image 2','',0,16),
	(86,'16_3.jpg','Image 3','',0,16),
	(87,'16_4.jpg','Image 4','',0,16),
	(88,'16_5.jpg','Image 5','',0,16),
	(89,'17_1.jpg','Image 1','',1,17),
	(90,'17_2.jpg','Image 2','',0,17),
	(91,'17_3.jpg','Image 3','',0,17),
	(92,'17_4.jpg','Image 4','',0,17),
	(93,'17_5.jpg','Image 5','',0,17),
	(94,'17_6.jpg','Image 6','',0,17),
	(95,'18_1.jpg','Image 1','',1,18),
	(96,'18_2.jpg','Image 2','',0,18),
	(97,'18_3.jpg','Image 3','',0,18),
	(98,'18_4.jpg','Image 4','',0,18),
	(99,'18_5.jpg','Image 5','',0,18),
	(100,'18_6.jpg','Image 6','',0,18),
	(101,'19_1.jpg','Image 1','',1,19),
	(102,'19_2.jpg','Image 2','',0,19),
	(103,'19_3.jpg','Image 3','',0,19),
	(104,'19_4.jpg','Image 4','',0,19),
	(105,'19_5.jpg','Image 5','',0,19),
	(106,'19_6.jpg','Image 6','',0,19),
	(107,'20_1.jpg','Image 1','',1,20),
	(108,'20_2.jpg','Image 2','',0,20),
	(109,'20_3.jpg','Image 3','',0,20),
	(110,'20_4.jpg','Image 4','',0,20),
	(111,'20_5.jpg','Image 5','',0,20),
	(112,'20_6.jpg','Image 6','',0,20),
	(113,'21_1.jpg','Image 1','',1,21),
	(114,'21_2.jpg','Image 2','',0,21),
	(115,'21_3.jpg','Image 3','',0,21),
	(116,'21_4.jpg','Image 4','',0,21),
	(117,'22_1.jpg','Image 1','',1,22),
	(118,'22_2.jpg','Image 2','',0,22),
	(119,'22_3.jpg','Image 3','',0,22),
	(120,'22_4.jpg','Image 4','',0,22),
	(121,'23_1.jpg','Image 1','',1,23),
	(122,'23_2.jpg','Image 2','',0,23),
	(123,'23_3.jpg','Image 3','',0,23),
	(124,'23_4.jpg','Image 4','',0,23),
	(125,'23_5.jpg','Image 5','',0,23),
	(126,'24_1.jpg','Image 1','',1,24),
	(127,'24_2.jpg','Image 2','',0,24),
	(128,'24_3.jpg','Image 3','',0,24),
	(129,'24_4.jpg','Image 4','',0,24),
	(130,'24_5.jpg','Image 5','',0,24),
	(131,'24_6.jpg','Image 6','',0,24),
	(132,'25_1.jpg','Image 1','',1,25),
	(133,'25_2.jpg','Image 2','',0,25),
	(134,'25_3.jpg','Image 3','',0,25),
	(135,'26_1.jpg','Image 1','',1,26),
	(136,'26_2.jpg','Image 2','',0,26),
	(137,'26_3.jpg','Image 3','',0,26),
	(138,'26_4.jpg','Image 4','',0,26),
	(139,'26_5.jpg','Image 5','',0,26),
	(140,'26_6.jpg','Image 6','',0,26),
	(141,'27_1.jpg','Image 1','',1,27),
	(142,'27_2.jpg','Image 2','',0,27),
	(143,'27_3.jpg','Image 3','',0,27),
	(144,'27_4.jpg','Image 4','',0,27),
	(145,'27_5.jpg','Image 5','',0,27),
	(146,'27_6.jpg','Image 6','',0,27),
	(147,'28_1.jpg','Image 1','',1,28),
	(148,'28_2.jpg','Image 2','',0,28),
	(149,'28_3.jpg','Image 3','',0,28),
	(150,'28_4.jpg','Image 4','',0,28),
	(151,'29_1.jpeg','Image 1','',1,29),
	(152,'29_2.jpeg','Image 2','',0,29),
	(153,'29_3.jpeg','Image 3','',0,29),
	(154,'29_4.jpeg','Image 4','',0,29),
	(155,'30_1.jpeg','Image 1','',1,30),
	(156,'30_2.jpeg','Image 2','',0,30),
	(157,'30_3.jpeg','Image 3','',0,30),
	(158,'30_4.jpeg','Image 4','',0,30),
	(159,'30_5.jpeg','Image 5','',0,30),
	(160,'31_1.jpg','Image 1','',1,31),
	(161,'31_2.jpg','Image 2','',0,31),
	(162,'31_3.jpg','Image 3','',0,31),
	(163,'32_1.jpg','Image 1','',1,32),
	(164,'32_2.jpg','Image 2','',0,32),
	(165,'32_3.jpg','Image 3','',0,32),
	(166,'33_1.jpg','Image 1','',1,33),
	(167,'33_2.jpg','Image 2','',0,33),
	(168,'33_3.jpg','Image 3','',0,33),
	(169,'34_1.jpg','Image 1','',1,34),
	(170,'34_2.jpg','Image 2','',0,34),
	(171,'34_3.jpg','Image 3','',0,34),
	(172,'34_4.jpg','Image 4','',0,34),
	(173,'34_5.jpg','Image 5','',0,34),
	(174,'35_1.jpg','Image 1','',1,35),
	(175,'35_2.jpg','Image 2','',0,35),
	(176,'35_3.jpg','Image 3','',0,35),
	(177,'36_1.jpg','Image 1','',1,36),
	(178,'36_2.jpg','Image 2','',0,36),
	(179,'36_3.jpg','Image 3','',0,36),
	(180,'37_1.jpg','Image 1','',1,37),
	(181,'37_2.jpg','Image 2','',0,37),
	(182,'37_3.jpg','Image 3','',0,37),
	(183,'38_1.jpg','Image 1','',1,38),
	(184,'38_2.jpg','Image 2','',0,38),
	(185,'38_3.jpg','Image 3','',0,38),
	(186,'39_1.jpg','Image 1','',1,39),
	(187,'39_2.jpg','Image 2','',0,39),
	(188,'39_3.jpg','Image 3','',0,39),
	(189,'40_1.jpg','Image 1','',1,40),
	(190,'40_2.jpg','Image 2','',0,40),
	(191,'40_3.jpg','Image 3','',0,40),
	(192,'41_1.jpg','Image 1','',1,41),
	(193,'41_2.jpg','Image 2','',0,41),
	(194,'41_3.jpg','Image 3','',0,41),
	(195,'42_1.jpg','Image 1','',1,42),
	(196,'42_2.jpg','Image 2','',0,42),
	(197,'42_3.jpg','Image 3','',0,42),
	(198,'43_1.jpg','Image 1','',1,43),
	(199,'43_2.jpg','Image 2','',0,43),
	(200,'43_3.jpg','Image 3','',0,43),
	(201,'44_1.jpg','Image 1','',1,44),
	(202,'44_2.jpg','Image 2','',0,44),
	(203,'44_3.jpg','Image 3','',0,44),
	(204,'45_1.jpg','Image 1','',1,45),
	(205,'45_2.jpg','Image 2','',0,45),
	(206,'45_3.jpg','Image 3','',0,45),
	(207,'46_1.jpg','Image 1','',1,46),
	(208,'46_2.jpg','Image 2','',0,46),
	(209,'46_3.jpg','Image 3','',0,46),
	(210,'47_1.jpg','Image 1','',1,47),
	(211,'47_2.jpg','Image 2','',0,47),
	(212,'47_3.jpg','Image 3','',0,47),
	(213,'48_1.jpg','Image 1','',1,48),
	(214,'48_2.jpg','Image 2','',0,48),
	(215,'48_3.jpg','Image 3','',0,48),
	(216,'49_1.jpg','Image 1','',1,49),
	(217,'49_2.jpg','Image 2','',0,49),
	(218,'49_3.jpg','Image 3','',0,49),
	(219,'50_1.jpg','Image 1','',1,50),
	(220,'50_2.jpg','Image 2','',0,50),
	(221,'50_3.jpg','Image 3','',0,50),
	(222,'51_1.jpg','Image 1','',1,51),
	(223,'51_2.jpg','Image 2','',0,51),
	(224,'51_3.jpg','Image 3','',0,51),
	(225,'52_1.jpg','Image 1','',1,52),
	(226,'52_2.jpg','Image 2','',0,52),
	(227,'52_3.jpg','Image 3','',0,52),
	(228,'53_1.jpg','Image 1','',1,53),
	(229,'53_2.jpg','Image 2','',0,53),
	(230,'53_3.jpg','Image 3','',0,53),
	(231,'54_1.jpg','Image 1','',1,54),
	(232,'54_2.jpg','Image 2','',0,54),
	(233,'54_3.jpg','Image 3','',0,54);

/*!40000 ALTER TABLE `images` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table notificationobjectchanges
# ------------------------------------------------------------

DROP TABLE IF EXISTS `notificationobjectchanges`;

CREATE TABLE `notificationobjectchanges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `verb` varchar(45) DEFAULT NULL,
  `actor` varchar(45) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT NULL,
  `notificationobject_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_notificationobjectchanges_notificationobjects1_idx` (`notificationobject_id`),
  CONSTRAINT `fk_notificationobjectchanges_notificationobjects1` FOREIGN KEY (`notificationobject_id`) REFERENCES `notificationobjects` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table notificationobjects
# ------------------------------------------------------------

DROP TABLE IF EXISTS `notificationobjects`;

CREATE TABLE `notificationobjects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `object_id` int(11) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT NULL,
  `notification_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_notificationobjects_notifications1_idx` (`notification_id`),
  CONSTRAINT `fk_notificationobjects_notifications1` FOREIGN KEY (`notification_id`) REFERENCES `notifications` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table notifications
# ------------------------------------------------------------

DROP TABLE IF EXISTS `notifications`;

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(45) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_notifications_users1_idx` (`user_id`),
  CONSTRAINT `fk_notifications_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table onetimepasswords
# ------------------------------------------------------------

DROP TABLE IF EXISTS `onetimepasswords`;

CREATE TABLE `onetimepasswords` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phonenumber` varchar(256) NOT NULL DEFAULT '',
  `otp` int(4) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `onetimepasswords` WRITE;
/*!40000 ALTER TABLE `onetimepasswords` DISABLE KEYS */;

INSERT INTO `onetimepasswords` (`id`, `phonenumber`, `otp`, `user_id`)
VALUES
	(7,'9743000647',5477,7),
	(14,'9538128057',2846,14),
	(36,'9876543210',9358,36),
	(38,'4389935641',4402,38),
	(40,'9589919002',7271,40);

/*!40000 ALTER TABLE `onetimepasswords` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table orderdetails
# ------------------------------------------------------------

DROP TABLE IF EXISTS `orderdetails`;

CREATE TABLE `orderdetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` int(11) DEFAULT NULL,
  `ispaid` tinyint(1) DEFAULT '0',
  `orderdate` datetime DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `cart_id` int(11) NOT NULL DEFAULT '0',
  `isdelivered` tinyint(1) DEFAULT '0',
  `iscancelled` tinyint(1) DEFAULT '0',
  `deliverydate` datetime DEFAULT NULL,
  `canceldate` datetime DEFAULT NULL,
  `deliveryaddress` varchar(1000) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `fk_orderdetails_users1_idx` (`user_id`),
  CONSTRAINT `fk_orderdetails_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `orderdetails` WRITE;
/*!40000 ALTER TABLE `orderdetails` DISABLE KEYS */;

INSERT INTO `orderdetails` (`id`, `amount`, `ispaid`, `orderdate`, `user_id`, `cart_id`, `isdelivered`, `iscancelled`, `deliverydate`, `canceldate`, `deliveryaddress`)
VALUES
	(1,0,0,'2016-05-23 11:02:29',5,6,0,1,NULL,'2016-05-23 11:11:31','{\"id\":\"1\",\"user_id\":\"5\",\"address_line_one\":\"murugesapalaya\",\"address_line_two\":\"\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560017\",\"timestamp\":\"2016-05-23 05:32:27\",\"landmark\":\"top\",\"email\":\"duraigowardhan.s@gmail.com\",\"name\":\"durai gowardhan\",\"phonenumber\":\"9944951838\"}'),
	(2,5200,0,'2016-05-23 17:17:33',13,14,0,0,NULL,NULL,'{\"id\":\"2\",\"user_id\":\"13\",\"address_line_one\":\"No 09\\/108,channakeshava nilaya, Opp to shell petrol bunk, Seegehalli , kadugodi post, Bangalore 560067\",\"address_line_two\":\"1st floor,Flat no. 108\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560067\",\"timestamp\":\"2016-05-23 11:47:20\",\"landmark\":\"Opp to shell petrol bunk\",\"email\":\"ankuraman.mishra@gmail.com\",\"name\":\"ankur mishra\",\"phonenumber\":\"7795847342\"}'),
	(3,6500,0,'2016-05-23 18:25:33',18,22,0,0,NULL,NULL,'{\"id\":\"3\",\"user_id\":\"18\",\"address_line_one\":\"N V RESIDENCY,FLAT NO-5\",\"address_line_two\":\"DODDATHOGUR ROAD, ELECTRONIC CITY PHASE-1\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560100\",\"timestamp\":\"2016-05-23 12:55:10\",\"landmark\":\"NEAR LAXMI NARSHIMA TEMPLE- AUTO STAND\",\"email\":\"ruchi21091@gmail.com\",\"name\":\"ruchismita chakraborty\",\"phonenumber\":\"9886382769\"}'),
	(5,9500,0,'2016-05-23 18:50:30',5,27,0,0,NULL,NULL,'{\"id\":\"1\",\"user_id\":\"5\",\"address_line_one\":\"murugesapalaya\",\"address_line_two\":\"\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560017\",\"timestamp\":\"2016-05-23 05:32:27\",\"landmark\":\"top\",\"email\":\"duraigowardhan.s@gmail.com\",\"name\":\"durai gowardhan\",\"phonenumber\":\"9944951838\"}'),
	(6,7500,0,'2016-05-23 18:58:53',5,29,0,0,NULL,NULL,'{\"id\":\"1\",\"user_id\":\"5\",\"address_line_one\":\"murugesapalaya\",\"address_line_two\":\"\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560017\",\"timestamp\":\"2016-05-23 05:32:27\",\"landmark\":\"top\",\"email\":\"duraigowardhan.s@gmail.com\",\"name\":\"durai gowardhan\",\"phonenumber\":\"9944951838\"}'),
	(7,0,0,'2016-05-23 20:15:24',26,35,0,1,NULL,'2016-05-25 16:06:43','{\"id\":\"4\",\"user_id\":\"26\",\"address_line_one\":\"Apartment No. B-502, BREN AVALON, Doddannekundi Extension,\",\"address_line_two\":\"Chinnapanahalli Main Road,\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560048\",\"timestamp\":\"2016-05-23 14:45:08\",\"landmark\":\"Near bigbasket warehouse\",\"email\":\"sujith10.cea@gmail.com\",\"name\":\"sujith kumar\",\"phonenumber\":\"7406926385\"}'),
	(8,5200,0,'2016-05-24 10:58:41',27,45,0,0,NULL,NULL,'{\"id\":\"5\",\"user_id\":\"27\",\"address_line_one\":\"Brigade Metropolis, Garudacharpalya\",\"address_line_two\":\"Mahadevapura, Whitefield road\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560048\",\"timestamp\":\"2016-05-24 05:28:26\",\"landmark\":\"Phoenix Market City\",\"email\":\"reechabansal@yahoo.com\",\"name\":\"Reecha Bansal\",\"phonenumber\":\"9742600477\"}'),
	(9,0,0,'2016-05-24 11:19:37',42,46,0,1,NULL,'2016-05-25 15:59:29','{\"id\":\"6\",\"user_id\":\"42\",\"address_line_one\":\"E1404, Smondo 3\",\"address_line_two\":\"Neotown, Electronic City Phase 1\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560102\",\"timestamp\":\"2016-05-24 05:46:17\",\"landmark\":\"Starlit Suites\",\"email\":\"haritgkumar@gmail.com\",\"name\":\"Harit Gulati\",\"phonenumber\":\"8290539343\"}'),
	(10,0,0,'2016-05-24 11:20:46',42,47,0,1,NULL,'2016-05-25 15:59:19','{\"id\":\"6\",\"user_id\":\"42\",\"address_line_one\":\"E1404, Smondo 3\",\"address_line_two\":\"Neotown, Electronic City Phase 1\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560102\",\"timestamp\":\"2016-05-24 05:46:17\",\"landmark\":\"Starlit Suites\",\"email\":\"haritgkumar@gmail.com\",\"name\":\"Harit Gulati\",\"phonenumber\":\"8290539343\"}'),
	(11,0,0,'2016-05-24 14:13:35',15,52,0,1,NULL,'2016-05-24 14:43:46','{\"id\":\"7\",\"user_id\":\"15\",\"address_line_one\":\"#5, (Second Floor, First Portion), 1st \'B\' Main Road, Chandra Reddy Layout, Behind Maharaja Hotel, 4th Block, Koramangala, Bangalore - 560034\",\"address_line_two\":\"\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560034\",\"timestamp\":\"2016-05-24 08:43:26\",\"landmark\":\"Near Maharaja Signal\",\"email\":\"aradhanasngh5@gmail.com\",\"name\":\"Aradhana Singh\",\"phonenumber\":\"8981542972\"}'),
	(12,6200,0,'2016-05-24 14:58:54',15,54,0,0,NULL,NULL,'{\"id\":\"7\",\"user_id\":\"15\",\"address_line_one\":\"#5, (Second Floor, First Portion), 1st \'B\' Main Road, Chandra Reddy Layout, Behind Maharaja Hotel, 4th Block, Koramangala, Bangalore - 560034\",\"address_line_two\":\"\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560034\",\"timestamp\":\"2016-05-24 08:43:26\",\"landmark\":\"Near Maharaja Signal\",\"email\":\"aradhanasngh5@gmail.com\",\"name\":\"Aradhana Singh\",\"phonenumber\":\"8981542972\"}'),
	(14,4000,0,'2016-05-24 15:50:45',48,58,0,0,NULL,NULL,'{\"id\":\"8\",\"user_id\":\"48\",\"address_line_one\":\"FT- 151, F2 Block, Ittina Neela Appartments,\",\"address_line_two\":\"Andapura Village, Electronic City\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560100\",\"timestamp\":\"2016-05-24 10:12:52\",\"landmark\":\"Near Gold Coin Club, Glass Factory Layout\",\"email\":\"vips.wipro@gmail.com\",\"name\":\"Vipin Mohan\",\"phonenumber\":\"8971777887\"}'),
	(15,6000,0,'2016-05-24 16:09:54',5,61,0,0,NULL,NULL,'{\"id\":\"1\",\"user_id\":\"5\",\"address_line_one\":\"murugesapalaya\",\"address_line_two\":\"\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560017\",\"timestamp\":\"2016-05-23 05:32:27\",\"landmark\":\"top\",\"email\":\"duraigowardhan.s@gmail.com\",\"name\":\"durai gowardhan\",\"phonenumber\":\"9944951838\"}'),
	(16,5200,0,'2016-05-24 17:57:13',54,65,0,0,NULL,NULL,'{\"id\":\"9\",\"user_id\":\"54\",\"address_line_one\":\"narayan appartment, Dodda tagore\",\"address_line_two\":\"electronic city phase 1 , konop agarahara,\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560100\",\"timestamp\":\"2016-05-24 12:27:11\",\"landmark\":\"near mm school\",\"email\":\"avikm01@gmail.com\",\"name\":\"Avik mukherjee\",\"phonenumber\":\"9836033457\"}'),
	(17,6500,0,'2016-05-24 18:02:40',54,66,0,0,NULL,NULL,'{\"id\":\"9\",\"user_id\":\"54\",\"address_line_one\":\"narayan appartment, Dodda tagore\",\"address_line_two\":\"electronic city phase 1 , konop agarahara,\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560100\",\"timestamp\":\"2016-05-24 12:27:11\",\"landmark\":\"near mm school\",\"email\":\"avikm01@gmail.com\",\"name\":\"Avik mukherjee\",\"phonenumber\":\"9836033457\"}'),
	(18,5200,0,'2016-05-24 18:38:32',55,69,0,0,NULL,NULL,'{\"id\":\"10\",\"user_id\":\"55\",\"address_line_one\":\"Flat No. 38, Room No. 9 4th Floor, 20th Cross Ejipura Main Road\",\"address_line_two\":\"KMR Building\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560047\",\"timestamp\":\"2016-05-24 13:07:35\",\"landmark\":\"Near Rama Temple\",\"email\":\"subratasrk1940@gmail.com\",\"name\":\"Subrata Sarkar\",\"phonenumber\":\"7411063991\"}'),
	(19,0,0,'2016-05-25 17:11:13',21,74,0,1,NULL,'2016-05-25 17:11:53','{\"id\":\"11\",\"user_id\":\"21\",\"address_line_one\":\"#426, 1st CROS , 4th MAIN\",\"address_line_two\":\"Vijaya Layout\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560076\",\"timestamp\":\"2016-05-25 11:41:10\",\"landmark\":\"Opposite Diana Memorial High School\",\"email\":\"26anoop1990@gmail.com\",\"name\":\"Anoop K\",\"phonenumber\":\"9686482400\"}'),
	(20,5500,0,'2016-05-26 11:33:14',62,77,0,0,NULL,NULL,'{\"id\":\"12\",\"user_id\":\"62\",\"address_line_one\":\"Shri laxmi nivas , #47 , 1st mn , 2nd crs, apparao layout, rammurthy nagar -\",\"address_line_two\":\"\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560016\",\"timestamp\":\"2016-05-26 06:03:10\",\"landmark\":\"more supermarket near fresh heritage opposite lane\",\"email\":\"peravelli8@gmail.com\",\"name\":\"purna eravelli\",\"phonenumber\":\"7776933090\"}'),
	(21,8000,0,'2016-05-26 18:23:06',63,79,0,0,NULL,NULL,'{\"id\":\"13\",\"user_id\":\"63\",\"address_line_one\":\"Flat s-2, Block-1, Wild grass Apts ,\",\"address_line_two\":\"Nirguna Mandir Layout , S.T.Bed , Koramangala\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560047\",\"timestamp\":\"2016-05-26 12:53:03\",\"landmark\":\"Koramangala near gem regency\",\"email\":\"kritishmani@gmail.com\",\"name\":\"Manikandan Balasubramanian\",\"phonenumber\":\"9513407060\"}'),
	(22,5300,0,'2016-05-26 23:19:12',3,3,0,0,NULL,NULL,'{\"id\":\"14\",\"user_id\":\"3\",\"address_line_one\":\"VS Chalet\",\"address_line_two\":\"LBS Nagar\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560017\",\"timestamp\":\"2016-05-26 17:49:09\",\"landmark\":\"Near Nilgris\",\"email\":\"vineethkumart@gmail.com\",\"name\":\"Vineeth Kumar\",\"phonenumber\":\"9538092344\"}'),
	(23,6900,0,'2016-05-27 17:58:08',26,88,0,0,NULL,NULL,'{\"id\":\"4\",\"user_id\":\"26\",\"address_line_one\":\"Apartment No. B-502, BREN AVALON, Doddannekundi Extension,\",\"address_line_two\":\"Chinnapanahalli Main Road,\",\"city\":\"Bangalore\",\"state\":\"Karnataka\",\"country\":\"India\",\"pincode\":\"560048\",\"timestamp\":\"2016-05-23 14:45:08\",\"landmark\":\"Near bigbasket warehouse\",\"email\":\"sujith10.cea@gmail.com\",\"name\":\"sujith kumar\",\"phonenumber\":\"7406926385\"}');

/*!40000 ALTER TABLE `orderdetails` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table orders
# ------------------------------------------------------------

DROP TABLE IF EXISTS `orders`;

CREATE TABLE `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `price` int(11) DEFAULT '0',
  `isshipped` tinyint(1) DEFAULT '0',
  `shippeddate` datetime DEFAULT NULL,
  `isdelivered` tinyint(1) DEFAULT '0',
  `deliverydate` datetime DEFAULT NULL,
  `ispaid` int(11) DEFAULT '0',
  `deliverycost` int(11) DEFAULT '0',
  `productitems_id` int(11) NOT NULL,
  `orderdetail_id` int(11) NOT NULL,
  `isqcdone` tinyint(1) DEFAULT '0',
  `qcdonedate` datetime DEFAULT NULL,
  `iscancelled` tinyint(1) DEFAULT '0',
  `canceldate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_orders_products_vendors1_idx` (`productitems_id`),
  KEY `fk_orders_orderdetails1_idx` (`orderdetail_id`),
  CONSTRAINT `fk_orders_orderdetails1` FOREIGN KEY (`orderdetail_id`) REFERENCES `orderdetails` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;

INSERT INTO `orders` (`id`, `price`, `isshipped`, `shippeddate`, `isdelivered`, `deliverydate`, `ispaid`, `deliverycost`, `productitems_id`, `orderdetail_id`, `isqcdone`, `qcdonedate`, `iscancelled`, `canceldate`)
VALUES
	(1,16200,1,'2016-05-23 11:08:46',0,NULL,0,0,18,1,1,'2016-05-23 11:08:16',1,'2016-05-23 11:11:31'),
	(2,5200,1,'2016-05-24 18:38:19',1,'2016-05-25 07:18:19',0,0,25,2,1,'2016-05-23 17:57:40',0,NULL),
	(3,6500,1,'2016-05-25 08:03:05',1,'2016-05-25 12:02:51',0,0,17,3,1,'2016-05-23 23:55:27',0,NULL),
	(5,9500,1,'2016-05-23 18:56:16',1,'2016-05-23 18:56:19',0,0,13,5,1,'2016-05-23 18:56:12',0,NULL),
	(6,7500,1,'2016-05-23 19:04:04',1,'2016-05-23 19:04:14',0,0,14,6,1,'2016-05-23 19:04:00',0,NULL),
	(7,5500,0,NULL,0,NULL,0,0,3,7,1,'2016-05-23 20:47:59',1,'2016-05-25 16:06:43'),
	(8,5200,1,'2016-05-24 18:38:33',1,'2016-05-25 07:24:11',0,0,8,8,1,'2016-05-24 11:34:23',0,NULL),
	(9,5800,1,'2016-05-25 08:02:50',0,NULL,0,0,26,9,1,'2016-05-24 11:34:31',1,'2016-05-25 15:59:29'),
	(10,5500,1,'2016-05-25 08:02:46',0,NULL,0,0,6,10,1,'2016-05-24 11:34:36',1,'2016-05-25 15:59:19'),
	(11,5500,0,NULL,0,NULL,0,0,20,11,0,NULL,1,'2016-05-24 14:43:46'),
	(12,6200,0,NULL,0,NULL,0,0,27,12,1,'2016-05-24 14:59:54',0,NULL),
	(14,4000,1,'2016-05-25 08:02:32',1,'2016-05-25 12:02:57',0,0,1,14,1,'2016-05-24 16:24:20',0,NULL),
	(15,6000,0,NULL,0,NULL,0,0,23,15,0,NULL,0,NULL),
	(16,5200,1,'2016-05-25 08:02:20',1,'2016-05-25 13:47:15',0,0,5,16,1,'2016-05-24 17:58:15',0,NULL),
	(17,6500,1,'2016-05-25 08:02:16',1,'2016-05-25 13:47:10',0,0,28,17,1,'2016-05-24 18:03:00',0,NULL),
	(18,5200,1,'2016-05-25 08:01:56',1,'2016-05-25 22:21:04',0,0,29,18,1,'2016-05-24 22:01:14',0,NULL),
	(19,5500,0,NULL,0,NULL,0,0,26,19,0,NULL,1,'2016-05-25 17:11:53'),
	(20,5500,1,'2016-05-26 17:03:13',1,'2016-05-27 12:33:54',0,0,6,20,1,'2016-05-26 11:36:42',0,NULL),
	(21,8000,0,NULL,0,NULL,0,0,33,21,1,'2016-05-26 19:06:32',0,NULL),
	(22,3000,0,NULL,0,NULL,0,0,32,22,1,'2016-05-26 23:24:10',0,NULL),
	(23,2300,1,'2016-05-26 23:25:40',1,'2016-05-26 23:26:03',0,0,31,22,1,'2016-05-26 23:24:06',0,NULL),
	(24,6900,0,NULL,0,NULL,0,0,34,23,0,NULL,0,NULL);

/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table parentcategories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `parentcategories`;

CREATE TABLE `parentcategories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(512) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(512) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `parentcategories` WRITE;
/*!40000 ALTER TABLE `parentcategories` DISABLE KEYS */;

INSERT INTO `parentcategories` (`id`, `name`, `description`, `image`)
VALUES
	(1,'Home Appliances','Home Appliances',''),
	(2,'Furniture','Furniture','');

/*!40000 ALTER TABLE `parentcategories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table productcategories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `productcategories`;

CREATE TABLE `productcategories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `description` text NOT NULL,
  `image` varchar(512) NOT NULL,
  `parentcategory_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `productcategories` WRITE;
/*!40000 ALTER TABLE `productcategories` DISABLE KEYS */;

INSERT INTO `productcategories` (`id`, `name`, `description`, `image`, `parentcategory_id`)
VALUES
	(3,'Refrigerators','Top Quality Refrigerators','fridge.jpg',1),
	(4,'Washing Machines','Top Quality Washing Machines','washingmachine.jpg',1),
	(5,'Air Conditioners','Top quality Air Conditioners','airconditioner.jpg',1),
	(6,'Water Purifiers','Top Quality Water Purifiers','waterpurifier.jpg',1),
	(7,'Sofa set','Sofa set at cheap price','sofaset.jpg',2),
	(8,'Office Chair','Best Quality Office Chairs','officechair.jpg',2),
	(9,'Computer Table','Compuer Tables at best price','computertable.jpg',2);

/*!40000 ALTER TABLE `productcategories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table productfilters
# ------------------------------------------------------------

DROP TABLE IF EXISTS `productfilters`;

CREATE TABLE `productfilters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filter_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table productitemfilters
# ------------------------------------------------------------

DROP TABLE IF EXISTS `productitemfilters`;

CREATE TABLE `productitemfilters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filter_id` int(11) NOT NULL,
  `productitem_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table productitems
# ------------------------------------------------------------

DROP TABLE IF EXISTS `productitems`;

CREATE TABLE `productitems` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qualityindex` int(11) DEFAULT '0',
  `title` varchar(512) NOT NULL,
  `description` varchar(256) DEFAULT NULL,
  `issold` int(11) DEFAULT '0',
  `isapproved` int(11) DEFAULT '0',
  `saleprice` int(11) DEFAULT '0',
  `manufacturingyear` int(11) NOT NULL,
  `warranty` varchar(256) NOT NULL,
  `vendorprice` int(11) DEFAULT '0',
  `ispaid` int(11) DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `isdelivered` int(11) DEFAULT '0',
  `productcategory_id` int(11) DEFAULT NULL,
  `color` varchar(100) DEFAULT NULL,
  `mrp` int(11) DEFAULT '99999',
  `longdescription` varchar(1500) DEFAULT '',
  `technicalspec` varchar(9999) DEFAULT NULL,
  `isfactory` tinyint(1) DEFAULT '0',
  `conditionspec` varchar(5000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_products_vendors_users1_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `productitems` WRITE;
/*!40000 ALTER TABLE `productitems` DISABLE KEYS */;

INSERT INTO `productitems` (`id`, `qualityindex`, `title`, `description`, `issold`, `isapproved`, `saleprice`, `manufacturingyear`, `warranty`, `vendorprice`, `ispaid`, `user_id`, `product_id`, `timestamp`, `isdelivered`, `productcategory_id`, `color`, `mrp`, `longdescription`, `technicalspec`, `isfactory`, `conditionspec`)
VALUES
	(1,0,'Haier Washing Machine XPB65-114D','Semi Automatic Top Load 6.5 Kg',1,1,4000,2012,'',0,0,2,1,'2016-05-24 10:20:45',0,4,NULL,8444,'Rust Free: Now placing your machine in wet area of your home is not a problem because plastic cabinet is rust free so it gives better & longer life to your washing machine. 45 Minutes Turbo Soak: Haier washing machine with active Turbo Soak Wash of 45 minutes ensures proper cleaning of badly soiled clothes. Bubble Wash: Bubbles comes out from the bottom of the tub, hits the rigid stains of the clothes & remove this dirt out from laundry to give your brighter & cleaner clothes. Overload Protection: Protection from overfilling of clothes in washing machine ensure no damage due to overload. Washine machine doesn\'t work while it is overloaded. Buzzer: You can control the volume of end of cycle buzzer. This feature help you to avoid any disturbance. Powerful Motor: Highly efficient motor ensures powerful rotation of clothes. Strong motor promises ensures high durability & efficiency to take heavy load.','Brand: Haier\nControl Type: Semi Automatic Top Load\nWashing Capacity: 6.5 kg\nColor: Grey\nDimensions: 78 cm x 92 cm x 43.5 cm\nWashing Method: Bubble Wash\nFreshers: Overload protection, Spin shower, In-built scrubber',NULL,NULL),
	(2,0,'Godrej Washing Machine GWF 620 FSX','Fully Automatic Top Load 6.2 kg',1,1,5000,2011,'',0,0,2,1,'2016-05-24 14:04:28',0,4,NULL,15470,'Godrej GWF 620 FSX is a perfect replacement for all your old washing system. It efficiently uses energy, water and detergent to keep your clothes clean. The intelligent micro-computer controls all operations precisely to ensure a perfect wash. The machine is equipped with fuzzy load and quality sensing to determine the water level and wash cycle appropriate for the wash load. Washing, rinsing and spin drying happens with a single selection on the control panel. It provides the flexibility of choosing the right program from Regular, Synthetics & Heavy. Also allows one to select the right water level incase of non-fuzzy operation. It has a unique Dry Tap Protection feature If the water flow stops during the water filling process, the machine starts hibernating. If the water level shows no rise in level in the next 15 minutes the machine indicates by sounding beeps and flickering LEDs. Auto restart with memory back-up ensures that your program settings stay the way you had set them in case of a power failure. Auto Balance System if there is an imbalance in the wash load, the machine automatically stops, tries to redistribute the load evenly and resumes washing.','Hot / Cold water inlet : Cold\nColor: Yellow\nInner tub :Stainless Steel\nDimensions (w x d x h) in mm :560x555x904\nNo of Wash Programs :3\nTechnology :Pulsator\nWash capacity : 6.2 kg\nWindow :Plastic\nType: Fully Automatic washing machine',NULL,NULL),
	(3,0,'Samsung Washing Machine WA82BSLEC','Fully Automatic Top Load 6.2Kg',1,1,5500,2013,'',0,0,2,1,'2016-05-26 05:48:45',0,4,NULL,16500,'Enjoy smart washing with the Samsung WA82BSL','Capacity: 6.2Kg\nFunction: Last setting\nDimension (WxHxD): 540mm x 940mmx 560mm\nFeature: Eco storm technology pulsator,5 step super clean, air turbo dry, magic filter, aqua preserve, sari course\nDrum: Diamond drum tub\nColor: Grey',0,'Few scratches in the front side\nFew areas rusted in the back side'),
	(4,0,'Godrej Washing Machine GWF 620 FSX','Fully Automatic Top load 6 Kg',0,1,5000,2012,'',0,0,2,1,'2016-05-24 02:57:55',0,4,NULL,14780,'Godrej GWF 620 FSX is a perfect replacement for all your old washing system. It efficiently uses energy, water and detergent to keep your clothes clean. The intelligent micro-computer controls all operations precisely to ensure a perfect wash. The machine is equipped with fuzzy load and quality sensing to determine the water level and wash cycle appropriate for the wash load. Washing, rinsing and spin drying happens with a single selection on the control panel. It provides the flexibility of choosing the right program from Regular, Synthetics & Heavy. Also allows one to select the right water level incase of non-fuzzy operation. It has a unique Dry Tap Protection feature If the water flow stops during the water filling process, the machine starts hibernating. If the water level shows no rise in level in the next 15 minutes the machine indicates by sounding beeps and flickering LEDs. Auto restart with memory back-up ensures that your program settings stay the way you had set them in case of a power failure. Auto Balance System if there is an imbalance in the wash load, the machine automatically stops, tries to redistribute the load evenly and resumes washing.','Dimension : 560x904x555mm\nType: Fully Automatic\nCapacity: 6 Kg\nWash programs: 3\nSpecial features: Cold Temperature selection, Force 4 Technology, Cold Water Inlet,Dry tap protection, Auto shut, Auto balance system\nFuzzy logic : yes\nColor: Red',0,NULL),
	(5,0,'Godrej Washing Machine GWF 620 FSX','Fully Automatic Top load 6 Kg',1,1,5200,2012,'',0,0,2,1,'2016-05-25 18:18:40',0,4,NULL,14780,'Godrej GWF 620 FSX is a perfect replacement for all your old washing system. It efficiently uses energy, water and detergent to keep your clothes clean. The intelligent micro-computer controls all operations precisely to ensure a perfect wash. The machine is equipped with fuzzy load and quality sensing to determine the water level and wash cycle appropriate for the wash load. Washing, rinsing and spin drying happens with a single selection on the control panel. It provides the flexibility of choosing the right program from Regular, Synthetics & Heavy. Also allows one to select the right water level incase of non-fuzzy operation. It has a unique Dry Tap Protection feature If the water flow stops during the water filling process, the machine starts hibernating. If the water level shows no rise in level in the next 15 minutes the machine indicates by sounding beeps and flickering LEDs. Auto restart with memory back-up ensures that your program settings stay the way you had set them in case of a power failure. Auto Balance System if there is an imbalance in the wash load, the machine automatically stops, tries to redistribute the load evenly and resumes washing.','Dimension : 560x904x555mm\nType: Fully Automatic\nCapacity: 6 Kg\nWash programs: 3\nSpecial features: Cold Temperature selection, Force 4 Technology, Cold Water Inlet,Dry tap protection, Auto shut, Auto balance system\nFuzzy logic : yes\nColor: Red',0,'Rust in the back side at the bottom\nBlack stain in the top panel'),
	(6,0,'Samsung Washing Machine WA78E4GEC/XTL','Fully Automatic Top load 5.8 Kg',1,1,5500,2013,'',0,0,2,1,'2016-05-26 06:03:14',0,4,NULL,14000,'Fully Automatic Top load washing machine','Capacity: 5.8KG\nColor: White\nForm Factor: Top load\nType: Fully Automatic',0,'Minor rust in the back side at the bottom\nFew scratches in the front side'),
	(7,0,'SAMSUNG Washing Machine WA88VPBEH','Fully Automatic Top load 6.8KG',1,1,10500,2015,'',0,0,2,1,'2016-05-24 09:01:20',0,4,NULL,15800,'This 6.8 kg top loading fully automatic washing machine with pulsator wash has 8 preset programmes. The machine allows prewash (soak), hot water wash, heavy wash, normal wash, delicate wash and quick wash. It also has additional features like washload sensor, automatic drainage, auto restart and child lock. It has mist shower programme, silver nano technology and air turbo drying function. It has diamond drum design for cleaner and gentler wash. It has a metallic silver colour body with Carmen wine colour top cover','Capacity: 6.8Kg\nPulsator Type	:	Double Storm\nWater Saving Tub	:	YES\nColour	:	Top cover Refined Black\nColour	:	Body Silver\nAir Turbo Drying System™	:	YES\nInner Tub	:	STS (Diamond)\nWindow	:	Transparent\nWater Inlet	:	Hot Cold\nClean Tub	:	5-Step Super Clean System\nWash Programs	:	8\n(W X D X H, mm)	:	540 x 560 x 920mm\nAg+Silver Nano	:	YES\nChild Lock	:	YES',NULL,NULL),
	(8,0,'Whirlpool Refrigerator 165 DLX','Single door 165 Litres',1,1,5200,2011,'',0,0,2,1,'2016-05-24 05:28:41',0,3,NULL,13000,'This Refrigerator comes in an attractive shade of wine fiesta and has a glossy finish. The refrigerator is very sturdy and has a solid built. The style and finish goes with any decor of your house be it contemporary or traditional. Since it is equipped with toughened glass shelves it can hold medium to large containers in a convenient manner.','Brand: Whirlpool\nModel: 190 DLX\nColor: Wine Fiesta\nType: Single door\nCapacity: 165 litres\nDoors:1 \nDefrosting type: Direct cool\nWxHxD: 536x623x1186 mm\nStar Rating: 4 Star\nShelves: 2',NULL,NULL),
	(9,0,'Godrej  Refrigerator 185L','Direct Cool Single door refrigerator',0,1,12700,2016,'',0,0,2,1,'2016-05-23 02:49:47',0,3,NULL,15900,'This refrigerator comes with 2 year warranty.\n\nOther Feature: Environment Friendly, Largest Freezer Space, Largest Shelf Space, 2.25 L Aquaspace\nTechnology Used: SIF Technology, Anti-B Technology','Number of Shelves: 3\n5 Star Rating\nDirect Cool\nColor: Wine Red\nTop Freezer Refrigerator\nWxHxD: 577 mm X 1247 mm X 667 mm',1,NULL),
	(10,0,'Godrej Refrigerator 185L','Direct Cool Single door refrigerator',0,1,12700,2016,'',0,0,2,1,'2016-05-23 02:49:43',0,3,NULL,15900,'This refrigerator comes with 2 years warrenty.\n\nOther Features: Environment Friendly, Largest Freezer Space, Largest Shelf Space, 2.25 L Aquaspace\nTechnology Used: SIF Technology, Anti-B Technology','Number of Shelves: 3\n5 Star Rating\nDirect Cool\nColor: Carbon Leaf\nTop Freezer Refrigerator\nWxHxD: 577 mm X 1247 mm X 667 mm',1,NULL),
	(11,0,'LG Refrigerator GL-B252VLGY','Double-door Refrigerator (240 Ltr, 2 Star Rating, Neo Inox)',0,1,12000,2016,'',0,0,2,1,'2016-05-22 17:21:58',0,3,NULL,21000,'Welcome home an environment friendly LG GL-B252VLGY Refrigerator that blends good looks with advanced functionality. The sleek built in the Neo Inox shade and the double doors make it look impressive. The 240 Litres refrigerator offers effective and uniform cooling by means of an efficient reciprocators compressor, mechanical temperature control, and a four way cooling technology as well. Perfectly compartmentalizes and equipped with a humidity controller and deodorizes, the unit ensures that the food you store stays healthy and fresh for longer hours in an energy efficient manner. It comes along with a 9 years compressor warranty and a 1 year comprehensive warranty from the manufacturer.','Brand: LG\nModel: GL-B252VLGY(BNI)\nEnergy Efficiency: 2 Star Rating\nCapacity: 240 Liters\nInstallation Type: Free-Standing\nPart Number: GL B252VLGY\nForm Factor: Standard Double Door\nColor: Wine Red\nDefrost System: Frost Free\nNumber of Shelves:2',NULL,NULL),
	(12,0,'LG Intellocool Refrigerator','375 Litre double door refrigerator',0,1,15500,2015,'',0,0,2,1,'2016-05-23 03:55:29',0,3,NULL,29800,'Double door refrigerator','Brand: LG\nForm Factor: Double door\nCapacity: 375 Liters\nColor: metallic grey',0,NULL),
	(13,0,'Samsung BioFresh Refrigerator','Double door frost free refrigerator',1,1,9500,2010,'',0,0,2,1,'2016-05-23 13:20:30',0,3,NULL,19000,'Samsung double door refrigerator with 6 months warranty','Capacity: 230 Litres\nDefrost system: Frost free\nForm factor: Standard double door\nColor: Grey',0,NULL),
	(14,0,'Whirlpool Refrigerator','6th sense single door refrigerator',1,1,7500,2012,'',0,0,2,1,'2016-05-23 13:28:53',0,3,NULL,15799,'Single door whirlpool refrigerator at best price','Defrost system: frost control\nColor: grey\nCapacity: 200 litre\nForm Factor: Single door',0,NULL),
	(15,0,'LG Refrigerator','Intellocool Single Door Refrigerator',1,1,5400,2012,'',0,0,2,1,'2016-05-24 09:02:11',0,3,NULL,12050,'If you are looking for one of the best storage options, then this refrigerato can be none other than Single Door refrigerator. This refrigerator is an amalgam of vanguard technology, stellar, user-oriented features and a swank design. The fridge is powered with the Evercool Technology which allows it to stay cool for up to 9 hours during power cuts. This Direct Cool Refrigerator produces ice 20% faster with the help of specially designed patented ice tray. Hence, getting a chilled drink is very easy now and you get it a lot faster without pressing any button.','Color: Red\nCapacity: 175L\nDimension: 541x575x1143\nType: Direct cool\nMRP: 8050\nDoor lock\nNumber of doors: 1\nFeature: Opaque interior, A-one cooling, Adjustable Shelves ,Extra Puff, Intello Compressor',0,NULL),
	(16,0,'Godrej Refrigerator','Single door semi-automatic 183 litre',1,1,6500,2012,'',0,0,2,1,'2016-05-24 14:04:14',0,3,NULL,10990,'As this environment-friendly refrigerator is HCFC, CFC and HFC-free, it doesn\'t damage the ozone layer.','Capacity: 183Litre\nNo. of doors: 1\nDoor Type: Single\nDefrosting Type: Semi Automatic\nColor: Light Lavender\nMaterial: Acrylic\nDoor lock : yes',0,NULL),
	(17,0,'Godrej Refrigerator GDA 19 A3/2012','Single door semi-automatic 181L',1,1,6500,2013,'',0,0,2,1,'2016-05-23 13:05:04',0,3,NULL,13500,'Rib Door Design for More Space in the Door. Better Space Organization. 2.5 L AquaSpace to keep larger bottles in the door. Deep Bottom Chiller Tray. Transparent Interiors. Roll Bond Freezer and Thicker Insulation for Superior Cooling. External Thermostat Control Lets you regulate the temperature without opening the door, thus saving on power consumption. It also allows more storage space inside the refrigerator. High Efficiency Compressor Powerful compressor that cools fast & yet saves on power, so you no longer have to worry about electricity bills','Capacity: 181L\nDefrosting Type: Semi Automatic\nNo.of doors: 1\nStar rating : 4 Star\nTemperatur control mode: Dial\nColor: Red\nOther Features: Rib Door Design for More Space in the Door, Deep Bottom Chiller Tray, Transparent Interiors,External Thermostat Control',0,NULL),
	(18,0,'Whirlpool FP 263D Proton Refrigerator','Roy Steel Knight 240 Ltrs Frost Free Fridge Refrigerator (Alpha Steel)',0,1,16200,2015,'',0,0,2,1,'2016-05-23 09:27:37',0,3,NULL,29500,'With the revolutionary 6th Sense ActiveFresh Technology, presenting the 3 door Protton World Series Refrigerator. Its Active Fresh Zone with Moisture Retention and the incredible MicroBlock, keeps your fruits and vegetables fresher for longer.\n\nMoisture Retention Technology\nMicroblock\nFresh Keeper\nAir Boosters\n3 Door Format','Gross Capacity (Ltr) : 240\n6th Sense Active Fresh Technology\nSeparate Vegetable Drawer\nAir Booster System\nQuick Chill Bottle Zone\nColor: Alpha steel',1,NULL),
	(19,0,'Whirlpool Refrigerator Roy 3S Neo FR258','Royal Double Door Refrigerator Wine Exotica 245 Ltr',0,1,16200,2016,'',0,0,2,1,'2016-05-23 03:26:53',0,3,NULL,24750,'The Whirlpool Neo offers an ideal way to keep your food and beverages fresh and cool. Powered by revolutionary 6th Sense Fresh Control Technology, the refrigerator maintains freshness of fruits and vegetables for up to 7 days. Its advanced cooling system and powerful compressor makes ice in quick time. This double door refrigerator displays an attractive design and eye-catching colour scheme. It can look great in most home decor styles.','Brand: Whirlpool\nModel: NEO IC255 Royal\nColor: Wine Exotica\nType: Double Door\nCapacity: 242 Litres\nDoors:2 \nDefrosting type: Frost-Free\nDimension: 560x662x1495mm\nShelves: 2',1,NULL),
	(20,0,'Whirlpool Single Door Refrigerator','180 litres 195 MP Roy 4s',1,1,5500,2011,'',0,0,2,1,'2016-05-24 09:15:10',0,3,NULL,13990,'Keep the food, fruits, vegetables and drinks cool and fresh with the Whirlpool 195 MP 4DG 180 Ltr Single Door Direct Cool Blue Duet Refrigerator. Featuring a capacity of 180 litres, it will meet the needs of modern households, while its 4 star rating will help you by reducing down the energy bills. Owing to its subtle blue duet colours, this refrigerator is the perfect blend of style and sensibility.\n\nThe refrigerator has an anti-fungal door gasket. This easy-to-clean and removable airtight gasket seals-in the freshness and acts as a barrier for the bacteria and dust particles that accumulate on the door seal. It also prevents mould spores from entering and spoiling the food inside, keeping the food fresh for a longer period. The refrigerator has a unique utility drawer, which is a non-refrigerated zone ideal for storing vegetables/eatables like potatoes, onions and garlic etc.','Single door type\ndirect cool\n180 litres capacity\n4 star rating\nToughened glass shelves\n6th sense door open alarm\n130-300 V Operation\nJumbo Bottle rack\nUnique Utility drawer\nStabiliser free operation\nUnique health guard door',0,NULL),
	(21,0,'LG Refrigerator','Double door frost free 230 Litre',0,1,8000,2011,'',0,0,2,1,'2016-05-23 03:46:38',0,3,NULL,16500,'LG Double door refrigerator at good working condition','Form Factor: Double door\nColor: Metallic\nType: Frost free\nCapacity: 230 Litre',0,NULL),
	(22,0,'Samsung Refrigerator','Biofresh double door frost free',0,1,7500,2010,'',0,0,2,1,'2016-05-23 03:50:53',0,3,NULL,14000,'Double door refrigerator at good working condition','Large Freezer Storage Area- Stores large quantities of food in the freezer.\nJumbo Storage Guard- Holds large bottles securely\nAdjustable Storage Guards- Position them for your convenience and accordingto the height of your food containers\nBio-Fresh Zone- Keeps vegetables fresh for longer periods\nSliding Drawer (Premium only)- Extra storage space for vegetables and fruits\niCooling (Premium only)- Advanced air circulation for faster surround cooling\nSuper Freeze Zone- Freezes stored items quickly\nTwist Ice-Tray (Premium Only)- Spring mechanism for automatic release of ice cubes',0,NULL),
	(23,0,'Samsung Refrigerator','Single door 180 litres',1,1,6000,2011,'',0,0,2,1,'2016-05-24 10:39:54',0,3,NULL,12750,'Single door Samsung refrigerator at good working condition','Form Factor: Single Door\nCapacity: 180 litres\nColor: Metallic grey\nType: Frost Free',0,NULL),
	(24,0,'LG Refrigerator','Single door 250 litre',0,1,11000,2016,'',0,0,2,1,'2016-05-23 04:00:27',0,3,NULL,17000,'This refrigerator comes with 2 years of motor warranty','Form factor: single door\nCapcity: 250 litre\nColor: Metallic grey leaf',1,NULL),
	(25,0,'LG Refrigerator','LG Single door refrigerator',1,1,5200,2011,'',0,0,2,1,'2016-05-24 09:20:59',0,3,NULL,12000,'LG Single door refrigerator at good working condition','Capacity: 185 Litres\nColor: Metallic grey\nForm factor: Single door',0,NULL),
	(26,0,'LG Intellocool Refrigerator','LG Single door refrigerator, 230 Litre',0,1,6500,2012,'',0,0,2,1,'2016-05-26 06:22:28',0,3,NULL,14000,'LG Single door refrigerator in good working condition. Quality tested with quality score.','Capacity: 230 Litres\n\nColor: Metallic grey\n\nForm factor: Single door',0,'New Thermostat installed\nTray replaced\nVegetable drawer installed'),
	(27,0,'Godrej 183L Single Door','Godrej GDE 19 DX4 Single Door 183 Litres Refrigerator (Silver)',1,1,6200,2012,'',0,0,1,1,'2016-05-24 09:28:54',0,3,NULL,14350,'Stay Cool Technology, SIF Technology, Revolutionary Cooling Technology, Zinc Oxide Protection Technology, Superior Air Circulation, Convection Control Technology with SIF, Aroma Lock, Honeycomb Crisper Cover, Easily Removable Gasket, Stabilizer Free Operation, Handle','No. of DoorsSingle Door\nPower Supply140 - 260 V, 50 Hz\nEnergy Rating5\nExteriorsMetallic Door\nShelvesToughened Glass Shelves(2)\nLampsMain Lamp\nRefrigeration and Cooling TechnologyHumidity Control with Superior Cool\nSpecial CompartmentsVegetable Tray, Bottle Shelves, Bottle Snugger, 2.5 L AquaSpace, 20 L Jumbo Vegetable Tray\nDoor LockAvailable\nOther Convenience FeaturesDeodourizing\nAdditional FeaturesStay Cool Technology, SIF Technology, Revolutionary Cooling Technology, Zinc Oxide Protection Technology, Superior Air Circulation, Convection Control Technology with SIF, Aroma Lock, Honeycomb Crisper Cover, Easily Removable Gasket, Stabilizer Free Operation, Handle\nSpecial FeaturesDry Storage, Moisture Control\nRefrigerator Door Pockets',0,NULL),
	(28,0,'Whirlpool Single Door Refrigerator','180 litres 195 MP Roy 4s',1,1,7000,2014,'',0,0,2,1,'2016-05-24 12:34:57',0,3,NULL,13990,'In excellent condition. Keep the food, fruits, vegetables and drinks cool and fresh with the Whirlpool 195 MP 4DG 180 Ltr Single Door Direct Cool Blue Duet Refrigerator. Featuring a capacity of 180 litres, it will meet the needs of modern households, while its 4 star rating will help you by reducing down the energy bills. Owing to its subtle blue duet colours, this refrigerator is the perfect blend of style and sensibility. The refrigerator has an anti-fungal door gasket. This easy-to-clean and removable airtight gasket seals-in the freshness and acts as a barrier for the bacteria and dust particles that accumulate on the door seal. It also prevents mould spores from entering and spoiling the food inside, keeping the food fresh for a longer period. The refrigerator has a unique utility drawer, which is a non-refrigerated zone ideal for storing vegetables/eatables like potatoes, onions and garlic etc.','Single door type\n\ndirect cool\n\n180 litres capacity\n\n4 star rating\n\nToughened glass shelves\n\n6th sense door open alarm\n\n130-300 V Operation\n\nJumbo Bottle rack\n\nUnique Utility drawer\n\nStabiliser free operation\n\nUnique health guard door',0,NULL),
	(29,0,'Godrej Fully Automatic','Top Load, 5.5Kg, Pulsator wash',1,1,5200,2013,'',0,0,2,1,'2016-05-24 13:08:32',0,4,NULL,11950,'This 5.5 kg top loading fully automatic washing machine has pulsator wash with 4 preset programmes. The machine allows prewash (soak), heavy wash, normal wash and delicate wash. It also has additional features like washload sensor, automatic drainage, quality sensing, memory back up and customized wash option for regular, synthetic, heavy and woolen clothes apart from fuzzy logic. The Force 4 Technology dislodges the toughest dirt from the most difficult to clean areas, giving you the cleanest wash ever.','Type of Washing Machine Fully Automatic \nControl Type Micro Processor \nLoading Type Top \nWashing Programmes	4\nWash Load	5.5\nType of Washing Machine	fully automatic\nControl Type	Micro processor\nBasket Material	Polypropylene\nWash Method	Pulsator',0,NULL),
	(30,0,'Whirlpool refrigerator 205','ICEMAGIC POWERCOOL PRM 5S 190 ltr',1,1,9500,2016,'',0,0,2,1,'2016-05-25 08:46:50',0,3,NULL,15435,'From now even power cuts will become moments of celebration. The new Ice Magic range of refrigerators, powered by 6th Sense PowerCool Technology, retains cooling for up to 12 hours during power cuts. That means, you can enjoy cold beverages even during long power cuts.','Star rating: 5 star rating\nColor: Wine exotica\nFeature: 6th sense powercool technology, 12 hours cooling retention during powercut,extra large extra cool freezer at -26c, toughened glass shield',0,NULL),
	(31,0,'Office Chair height adjustable','Adjustable back seat and rolling, lean back with lean lock',1,1,2300,2013,'',0,0,2,1,'2016-05-27 03:17:11',0,8,NULL,5000,'Installation and demo for this product is done free of cost as part of this purchase. Product is in excellent condition without any issues. Sparingly used. Product comes with plastic covering.\nPocketin Quality check done and quality score included.','Metal\nKnock Down\nAdjustable Height: Yes\nSeat Lock Included:	Yes\nWheels Included: Yes\nLean Back: Yes\nLean Lock: Yes\nSuitable For: Study & Home Office\nArmrest Included: Yes',0,'Sparingly used office chair in very good condition\nNo noticeable or unnoticeable defects\nHeight adjustment, lean back, and wheels working perfectly.'),
	(32,0,'Office Chair Height adjustable','Adjustable back seat, Rolling wheels, lean back with lean lock',1,1,3000,2014,'',0,0,2,1,'2016-05-27 03:17:19',0,8,NULL,6000,'Installation and demo for this product is done free of cost as part of this purchase. Product is in excellent condition without any issues. Sparingly used. Product comes with plastic covering.\nPocketin Quality check done and quality score included.','Metal\nKnock Down\nAdjustable Height: Yes\nSeat Lock Included:	Yes\nWheels Included: Yes\nLean Back: Yes\nLean Lock: Yes\nSuitable For: Study & Home Office\nArmrest Included: Yes',0,'Sparingly used office chair in very good condition\nNo noticeable or unnoticeable defects\nHeight adjustment, lean back, and wheels working perfectly.'),
	(33,0,'LG Front Loading 6.5 KG Fully automatic','Allergy care, Energy Saving, Hot water',1,1,8000,2013,'',0,0,2,1,'2016-05-26 12:53:06',0,4,NULL,28900,'LG FRONT LOADING 6.5 KG WD 10490TP	washing machine comes with energy savings, allergy care, fully cycle functionality and in excellent condition. Pocketin quality check done and quality score included.','General\nCapacity	:	6.5 kg\nPower\nWATTS	:	400W\nMain Features\nFault Diagnosis	:	YES\nFuzzy Logic	:	YES\nWool	:	YES\nChild Lock	:	YES\nCrease Care	:	YES\nFeatures\nTemperature Range	:	30 - 95 Deg\nSpin Speed options	:	4\nSpin Speeds (rpm)	:	No Spin/400/800/1000\nWash Programs	:	Cotton,Baby care,Synthetic,Delicate\nQuick 30	:	YES\nOnly Spin	:	YES\nSpecial Options	:	Bio Wash\nSpecial Options	:	Rinse Hold\nSpecial Options	:	Rinse Plus\nSpecial Options	:	Pre Wash\nTime Save	:	YES\nTime Delay	:	YES\nDoor Diameter (mm)	:	300mm\nProgram Selection	:	Digi Push\nDoor Opening Angle	:	180 Deg',0,'Its like new without any issues\nNo major scratches or dents visible\ninlet and outlet pipes are include'),
	(34,0,'Godrej Eon 6.5 Kg Top Load Fully automatic','Less than a year old, Fuzzy Logic, Truly Automatic Washing Machine',1,1,6900,2015,'',0,0,2,1,'2016-05-27 12:28:08',0,4,NULL,18500,'Senses the quantity and quality of wash load to determine the water level and wash cycle appropriate for the wash load.The Dynamic Aqua Power Control Technology automatically puts the washing machine in sleep mode, at the first detection of a water/ power cut. Pocketin quality assurance with quality check value','Digital Display\nSteel Drum\nToughened Glass Lid\n8 Wash programs.\nMemory backup\nActive Soak\n\nGeneral\nBrand	Godrej\nModel	GWF 650 FC\nType	Top Load\nCapacity	6.5 litres\nTechnology	Fully-automatic\nPanel Display	LED\nWash Program	6\nSpecial Features	Save Settings feature, Cold Temprature Selection, Cold Water Inlet, Auto Shut, Water Optimizer',0,'In Excellent condition without noticeable scratches or dents.'),
	(35,0,'Office Chair height adjustable','Adjustable back seat and rolling, lean back with lean lock',0,1,2300,2012,'',0,0,3,1,'2016-05-27 03:17:24',0,8,NULL,5000,'Installation and demo for this product is done free of cost as part of this purchase. Product is in excellent condition without any issues. Sparingly used. Product comes with plastic covering.\nPocketin Quality check done and quality score included.','Metal\nKnock Down\nAdjustable Height: Yes\nSeat Lock Included:	Yes\nWheels Included: Yes\nLean Back: Yes\nLean Lock: Yes\nSuitable For: Study & Home Office\nArmrest Included: Yes',0,'Sparingly used office chair in very good condition\nNo noticeable or unnoticeable defects\nHeight adjustment, lean back, and wheels working perfectly.'),
	(36,0,'Office Chair height adjustable','Adjustable back seat and rolling, lean back with lean lock',0,1,2300,2012,'',0,0,3,1,'2016-05-27 03:17:29',0,8,NULL,5000,'Installation and demo for this product is done free of cost as part of this purchase. Product is in excellent condition without any issues. Sparingly used. Product comes with plastic covering.\nPocketin Quality check done and quality score included.','Metal\nKnock Down\nAdjustable Height: Yes\nSeat Lock Included:	Yes\nWheels Included: Yes\nLean Back: Yes\nLean Lock: Yes\nSuitable For: Study & Home Office\nArmrest Included: Yes',0,'Sparingly used office chair in very good condition\nNo noticeable or unnoticeable defects\nHeight adjustment, lean back, and wheels working perfectly.'),
	(37,0,'Office Chair height adjustable','Adjustable back seat and rolling, lean back with lean lock',0,1,2300,2012,'',0,0,3,1,'2016-05-27 03:17:36',0,8,NULL,5000,'Installation and demo for this product is done free of cost as part of this purchase. Product is in excellent condition without any issues. Sparingly used. Product comes with plastic covering.\nPocketin Quality check done and quality score included.','Metal\nKnock Down\nAdjustable Height: Yes\nSeat Lock Included:	Yes\nWheels Included: Yes\nLean Back: Yes\nLean Lock: Yes\nSuitable For: Study & Home Office\nArmrest Included: Yes',0,'Sparingly used office chair in very good condition\nNo noticeable or unnoticeable defects\nHeight adjustment, lean back, and wheels working perfectly.'),
	(38,0,'Office Chair height adjustable','Adjustable back seat and rolling, lean back with lean lock',0,1,2300,2012,'',0,0,3,1,'2016-05-27 03:17:40',0,8,NULL,5000,'Installation and demo for this product is done free of cost as part of this purchase. Product is in excellent condition without any issues. Sparingly used. Product comes with plastic covering.\nPocketin Quality check done and quality score included.','Metal\nKnock Down\nAdjustable Height: Yes\nSeat Lock Included:	Yes\nWheels Included: Yes\nLean Back: Yes\nLean Lock: Yes\nSuitable For: Study & Home Office\nArmrest Included: Yes',0,'Sparingly used office chair in very good condition\nNo noticeable or unnoticeable defects\nHeight adjustment, lean back, and wheels working perfectly.'),
	(39,0,'Office Chair height adjustable','Adjustable back seat and rolling, lean back with lean lock',0,1,2300,2012,'',0,0,3,1,'2016-05-27 03:17:45',0,8,NULL,5000,'Installation and demo for this product is done free of cost as part of this purchase. Product is in excellent condition without any issues. Sparingly used. Product comes with plastic covering.\nPocketin Quality check done and quality score included.','Metal\nKnock Down\nAdjustable Height: Yes\nSeat Lock Included:	Yes\nWheels Included: Yes\nLean Back: Yes\nLean Lock: Yes\nSuitable For: Study & Home Office\nArmrest Included: Yes',0,'Sparingly used office chair in very good condition\nNo noticeable or unnoticeable defects\nHeight adjustment, lean back, and wheels working perfectly.'),
	(40,0,'Office Chair height adjustable','Adjustable back seat and rolling, lean back with lean lock',0,1,2300,2012,'',0,0,3,1,'2016-05-27 03:17:50',0,8,NULL,5000,'Installation and demo for this product is done free of cost as part of this purchase. Product is in excellent condition without any issues. Sparingly used. Product comes with plastic covering.\nPocketin Quality check done and quality score included.','Metal\nKnock Down\nAdjustable Height: Yes\nSeat Lock Included:	Yes\nWheels Included: Yes\nLean Back: Yes\nLean Lock: Yes\nSuitable For: Study & Home Office\nArmrest Included: Yes',0,'Sparingly used office chair in very good condition\nNo noticeable or unnoticeable defects\nHeight adjustment, lean back, and wheels working perfectly.'),
	(41,0,'Office Chair height adjustable','Adjustable back seat and rolling, lean back with lean lock',0,1,2300,2012,'',0,0,3,1,'2016-05-27 03:17:55',0,8,NULL,5000,'Installation and demo for this product is done free of cost as part of this purchase. Product is in excellent condition without any issues. Sparingly used. Product comes with plastic covering.\nPocketin Quality check done and quality score included.','Metal\nKnock Down\nAdjustable Height: Yes\nSeat Lock Included:	Yes\nWheels Included: Yes\nLean Back: Yes\nLean Lock: Yes\nSuitable For: Study & Home Office\nArmrest Included: Yes',0,'Sparingly used office chair in very good condition\nNo noticeable or unnoticeable defects\nHeight adjustment, lean back, and wheels working perfectly.'),
	(42,0,'Office Chair height adjustable','Adjustable back seat and rolling, lean back with lean lock',0,1,2300,2012,'',0,0,3,1,'2016-05-27 03:17:59',0,8,NULL,5000,'Installation and demo for this product is done free of cost as part of this purchase. Product is in excellent condition without any issues. Sparingly used. Product comes with plastic covering.\nPocketin Quality check done and quality score included.','Metal\nKnock Down\nAdjustable Height: Yes\nSeat Lock Included:	Yes\nWheels Included: Yes\nLean Back: Yes\nLean Lock: Yes\nSuitable For: Study & Home Office\nArmrest Included: Yes',0,'Sparingly used office chair in very good condition\nNo noticeable or unnoticeable defects\nHeight adjustment, lean back, and wheels working perfectly.'),
	(43,0,'Office Chair height adjustable','Adjustable back seat and rolling, lean back with lean lock',0,1,2300,2012,'',0,0,3,1,'2016-05-27 03:18:04',0,8,NULL,5000,'Installation and demo for this product is done free of cost as part of this purchase. Product is in excellent condition without any issues. Sparingly used. Product comes with plastic covering.\nPocketin Quality check done and quality score included.','Metal\nKnock Down\nAdjustable Height: Yes\nSeat Lock Included:	Yes\nWheels Included: Yes\nLean Back: Yes\nLean Lock: Yes\nSuitable For: Study & Home Office\nArmrest Included: Yes',0,'Sparingly used office chair in very good condition\nNo noticeable or unnoticeable defects\nHeight adjustment, lean back, and wheels working perfectly.'),
	(44,0,'Office Chair height adjustable','Adjustable back seat and rolling, lean back with lean lock',0,1,2300,2012,'',0,0,3,1,'2016-05-27 03:18:08',0,8,NULL,5000,'Installation and demo for this product is done free of cost as part of this purchase. Product is in excellent condition without any issues. Sparingly used. Product comes with plastic covering.\nPocketin Quality check done and quality score included.','Metal\nKnock Down\nAdjustable Height: Yes\nSeat Lock Included:	Yes\nWheels Included: Yes\nLean Back: Yes\nLean Lock: Yes\nSuitable For: Study & Home Office\nArmrest Included: Yes',0,'Sparingly used office chair in very good condition\nNo noticeable or unnoticeable defects\nHeight adjustment, lean back, and wheels working perfectly.'),
	(45,0,'Office Chair Height adjustable','Adjustable back seat, Rolling wheels, lean back with lean lock',0,1,3000,2013,'',0,0,3,1,'2016-05-27 03:18:14',0,8,NULL,6000,'Installation and demo for this product is done free of cost as part of this purchase. Product is in excellent condition without any issues. Sparingly used. Product comes with plastic covering.\nPocketin Quality check done and quality score included.','Metal\nKnock Down\nAdjustable Height: Yes\nSeat Lock Included:	Yes\nWheels Included: Yes\nLean Back: Yes\nLean Lock: Yes\nSuitable For: Study & Home Office\nArmrest Included: Yes',0,'Sparingly used office chair in very good condition\nNo noticeable or unnoticeable defects\nHeight adjustment, lean back, and wheels working perfectly.'),
	(46,0,'Office Chair Height adjustable','Adjustable back seat, Rolling wheels, lean back with lean lock',0,1,3000,2013,'',0,0,3,1,'2016-05-27 03:18:19',0,8,NULL,6000,'Installation and demo for this product is done free of cost as part of this purchase. Product is in excellent condition without any issues. Sparingly used. Product comes with plastic covering.\nPocketin Quality check done and quality score included.','Metal\nKnock Down\nAdjustable Height: Yes\nSeat Lock Included:	Yes\nWheels Included: Yes\nLean Back: Yes\nLean Lock: Yes\nSuitable For: Study & Home Office\nArmrest Included: Yes',0,'Sparingly used office chair in very good condition\nNo noticeable or unnoticeable defects\nHeight adjustment, lean back, and wheels working perfectly..'),
	(47,0,'Office Chair Height adjustable','Adjustable back seat, Rolling wheels, lean back with lean lock',0,1,3000,2013,'',0,0,3,1,'2016-05-27 03:18:28',0,8,NULL,6000,'Installation and demo for this product is done free of cost as part of this purchase. Product is in excellent condition without any issues. Sparingly used. Product comes with plastic covering.\nPocketin Quality check done and quality score included.','Metal\nKnock Down\nAdjustable Height: Yes\nSeat Lock Included:	Yes\nWheels Included: Yes\nLean Back: Yes\nLean Lock: Yes\nSuitable For: Study & Home Office\nArmrest Included: Yes',0,'Sparingly used office chair in very good condition\nNo noticeable or unnoticeable defects\nHeight adjustment, lean back, and wheels working perfectly..'),
	(48,0,'Office Chair Height adjustable','Adjustable back seat, Rolling wheels, lean back with lean lock',0,1,3000,2013,'',0,0,3,1,'2016-05-27 03:18:33',0,8,NULL,6000,'Installation and demo for this product is done free of cost as part of this purchase. Product is in excellent condition without any issues. Sparingly used. Product comes with plastic covering. Pocketin Quality check done and quality score included.','Metal\n\nKnock Down\n\nAdjustable Height: Yes\n\nSeat Lock Included:	Yes\n\nWheels Included: Yes\n\nLean Back: Yes\n\nLean Lock: Yes\n\nSuitable For: Study & Home Office\n\nArmrest Included: Yes',0,'Sparingly used office chair in very good condition\n\nNo noticeable or unnoticeable defects\n\nHeight adjustment, lean back, and wheels working perfectly..'),
	(49,0,'Office Chair Height adjustable','Adjustable back seat, Rolling wheels, lean back with lean lock',0,1,3000,2013,'',0,0,3,1,'2016-05-27 03:18:42',0,8,NULL,6000,'Installation and demo for this product is done free of cost as part of this purchase. Product is in excellent condition without any issues. Sparingly used. Product comes with plastic covering. Pocketin Quality check done and quality score included.','Metal\n\nKnock Down\n\nAdjustable Height: Yes\n\nSeat Lock Included:	Yes\n\nWheels Included: Yes\n\nLean Back: Yes\n\nLean Lock: Yes\n\nSuitable For: Study & Home Office\n\nArmrest Included: Yes',0,'Sparingly used office chair in very good condition\n\nNo noticeable or unnoticeable defects\n\nHeight adjustment, lean back, and wheels working perfectly..'),
	(50,0,'Office Chair Height adjustable','Adjustable back seat, Rolling wheels, lean back with lean lock',0,1,3000,2013,'',0,0,3,1,'2016-05-27 03:18:47',0,8,NULL,6000,'Installation and demo for this product is done free of cost as part of this purchase. Product is in excellent condition without any issues. Sparingly used. Product comes with plastic covering. Pocketin Quality check done and quality score included.','Metal\n\nKnock Down\n\nAdjustable Height: Yes\n\nSeat Lock Included:	Yes\n\nWheels Included: Yes\n\nLean Back: Yes\n\nLean Lock: Yes\n\nSuitable For: Study & Home Office\n\nArmrest Included: YesMetal\n\nKnock Down\n\nAdjustable Height: Yes\n\nSeat Lock Included:	Yes\n\nWheels Included: Yes\n\nLean Back: Yes\n\nLean Lock: Yes\n\nSuitable For: Study & Home Office\n\nArmrest Included: Yes',0,'Sparingly used office chair in very good condition\n\nNo noticeable or unnoticeable defects\n\nHeight adjustment, lean back, and wheels working perfectly..'),
	(51,0,'Office Chair Height adjustable','Adjustable back seat, Rolling wheels, lean back with lean lock',0,1,3000,2013,'',0,0,3,1,'2016-05-27 03:18:52',0,8,NULL,6000,'Installation and demo for this product is done free of cost as part of this purchase. Product is in excellent condition without any issues. Sparingly used. Product comes with plastic covering. Pocketin Quality check done and quality score included.','Metal\n\nKnock Down\n\nAdjustable Height: Yes\n\nSeat Lock Included:	Yes\n\nWheels Included: Yes\n\nLean Back: Yes\n\nLean Lock: Yes\n\nSuitable For: Study & Home Office\n\nArmrest Included: Yes',0,'Sparingly used office chair in very good condition\n\nNo noticeable or unnoticeable defects\n\nHeight adjustment, lean back, and wheels working perfectly..'),
	(52,0,'Office Chair Height adjustable','Adjustable back seat, Rolling wheels, lean back with lean lock',0,1,3000,2013,'',0,0,3,1,'2016-05-27 03:18:57',0,8,NULL,6000,'Installation and demo for this product is done free of cost as part of this purchase. Product is in excellent condition without any issues. Sparingly used. Product comes with plastic covering. Pocketin Quality check done and quality score included.','Metal\n\nKnock Down\n\nAdjustable Height: Yes\n\nSeat Lock Included:	Yes\n\nWheels Included: Yes\n\nLean Back: Yes\n\nLean Lock: Yes\n\nSuitable For: Study & Home Office\n\nArmrest Included: Yes',0,'Sparingly used office chair in very good condition\n\nNo noticeable or unnoticeable defects\n\nHeight adjustment, lean back, and wheels working perfectly..'),
	(53,0,'Office Chair Height adjustable','Adjustable back seat, Rolling wheels, lean back with lean lock',0,1,3000,2013,'',0,0,3,1,'2016-05-27 03:19:01',0,8,NULL,6000,'Installation and demo for this product is done free of cost as part of this purchase. Product is in excellent condition without any issues. Sparingly used. Product comes with plastic covering. Pocketin Quality check done and quality score included.','Metal\n\nKnock Down\n\nAdjustable Height: Yes\n\nSeat Lock Included:	Yes\n\nWheels Included: Yes\n\nLean Back: Yes\n\nLean Lock: Yes\n\nSuitable For: Study & Home Office\n\nArmrest Included: Yes',0,'Sparingly used office chair in very good condition\n\nNo noticeable or unnoticeable defects\n\nHeight adjustment, lean back, and wheels working perfectly..'),
	(54,0,'Office Chair Height adjustable','Adjustable back seat, Rolling wheels, lean back with lean lock',0,1,3000,2013,'',0,0,3,1,'2016-05-27 03:19:11',0,8,NULL,6000,'Installation and demo for this product is done free of cost as part of this purchase. Product is in excellent condition without any issues. Sparingly used. Product comes with plastic covering. Pocketin Quality check done and quality score included.','Metal\n\nKnock Down\n\nAdjustable Height: Yes\n\nSeat Lock Included:	Yes\n\nWheels Included: Yes\n\nLean Back: Yes\n\nLean Lock: Yes\n\nSuitable For: Study & Home Office\n\nArmrest Included: Yes',0,'Sparingly used office chair in very good condition\n\nNo noticeable or unnoticeable defects\n\nHeight adjustment, lean back, and wheels working perfectly..');

/*!40000 ALTER TABLE `productitems` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table productlogs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `productlogs`;

CREATE TABLE `productlogs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table productrequests
# ------------------------------------------------------------

DROP TABLE IF EXISTS `productrequests`;

CREATE TABLE `productrequests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phonenumber` varchar(256) DEFAULT NULL,
  `productname` varchar(256) DEFAULT NULL,
  `model` varchar(45) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_productrequests_users1_idx` (`user_id`),
  CONSTRAINT `fk_productrequests_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table products
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `modelnumber` varchar(512) NOT NULL,
  `description` text,
  `shortdescription` varchar(512) NOT NULL,
  `instock` int(11) DEFAULT '0',
  `stockcount` int(11) DEFAULT '0',
  `origimage` varchar(256) DEFAULT NULL,
  `mrp` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;

INSERT INTO `products` (`id`, `name`, `modelnumber`, `description`, `shortdescription`, `instock`, `stockcount`, `origimage`, `mrp`)
VALUES
	(1,'Pocketin Model','PI01','Pocketin','Pocketin',1,1,NULL,9999);

/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table qualityindexes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `qualityindexes`;

CREATE TABLE `qualityindexes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(512) NOT NULL,
  `productcategory_id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `qualityindexes` WRITE;
/*!40000 ALTER TABLE `qualityindexes` DISABLE KEYS */;

INSERT INTO `qualityindexes` (`id`, `name`, `productcategory_id`, `timestamp`)
VALUES
	(1,'Age',3,'2016-03-12 10:56:38'),
	(2,'Door Seal',3,'2016-03-12 11:26:00'),
	(3,'Energy Efficiency',3,'2016-03-12 10:56:51'),
	(4,'Appearance',3,'2016-03-12 10:57:22'),
	(5,'Motor Condition',3,'2016-03-12 11:02:03'),
	(6,'Age',4,'2016-03-12 11:00:14'),
	(7,'Full Cycle Functionality',4,'2016-03-12 11:00:53'),
	(8,'Tub/Drum Condition',4,'2016-04-04 10:33:56'),
	(9,'Efficiency Grading',4,'2016-03-12 11:00:55'),
	(10,'Appearance',4,'2016-03-12 11:00:56'),
	(12,'Age',5,'2016-03-12 11:01:12'),
	(13,'Cooling Capacity',5,'2016-03-12 11:01:21'),
	(14,'Energy Efficiency',5,'2016-03-12 11:02:48'),
	(15,'Appearance',5,'2016-03-12 11:02:50'),
	(16,'Filters/Dehumidifiers',5,'2016-04-04 10:36:31'),
	(18,'Thermostat setting',5,'2016-04-04 10:37:19'),
	(19,'Age',6,'2016-03-12 11:03:46'),
	(20,'Filter Codition',6,'2016-03-12 11:03:46'),
	(21,'Appearance',6,'2016-03-12 11:03:46'),
	(22,'Interior Parts',6,'2016-03-12 11:03:47'),
	(23,'Other',6,'2016-03-12 11:03:49'),
	(24,'Age',7,'2016-03-12 11:04:45'),
	(25,'Cushion Condition',7,'2016-03-12 11:04:46'),
	(26,'Frame/Fabric Condition',7,'2016-04-04 10:38:45'),
	(27,'Spring Condition',7,'2016-03-12 11:13:12'),
	(28,'Appearance',7,'2016-03-12 11:13:36'),
	(29,'Other',7,'2016-03-12 11:13:40'),
	(30,'Defrosting Time',3,'2016-04-04 10:34:06'),
	(31,'Noise',3,'2016-04-04 10:35:18'),
	(32,'Adjustability',8,'2016-04-04 10:39:02'),
	(33,'Seat Material',8,'2016-04-04 10:39:14'),
	(34,'Mobility',8,'2016-04-04 10:39:27'),
	(35,'Desk Shape',9,'2016-04-04 10:39:43'),
	(36,'Desk Material',9,'2016-04-04 10:40:04');

/*!40000 ALTER TABLE `qualityindexes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table qualityindexvalues
# ------------------------------------------------------------

DROP TABLE IF EXISTS `qualityindexvalues`;

CREATE TABLE `qualityindexvalues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qualityindexes_id` int(11) NOT NULL,
  `value_text` varchar(512) NOT NULL DEFAULT '',
  `value` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `qualityindexvalues` WRITE;
/*!40000 ALTER TABLE `qualityindexvalues` DISABLE KEYS */;

INSERT INTO `qualityindexvalues` (`id`, `qualityindexes_id`, `value_text`, `value`)
VALUES
	(1,1,'Less than 1 year old',5),
	(2,1,'1 - 2 years old',4),
	(3,1,'2 - 4 years old',3),
	(4,1,'4 - 5 years old',2),
	(5,1,'5 - 7 years old',1),
	(6,2,'New Seal Installed',5),
	(7,2,'Good Working Seal',4),
	(8,2,'Average Seal',3),
	(9,2,'Bad Seal',2),
	(10,2,'Seal Damaged',1),
	(11,3,'Five Star',5),
	(12,3,'Four Star',4),
	(13,3,'Three Star',3),
	(14,3,'Two Star',2),
	(15,3,'< Two Star',1),
	(16,4,'Like New W/o Any Major Scratches or Dents',5),
	(17,4,'Good With Some Scratches and no Dents',4),
	(18,4,'Average With Some Dents and Scratches',3),
	(19,4,'Some Parts are Broken',2),
	(20,4,'Looks Bad',1),
	(21,5,'New Motor',5),
	(22,5,'Excellent Working Condition',4),
	(23,5,'Good Working Condition',3),
	(24,5,'Average',2),
	(25,5,'Bad',1),
	(26,6,'Less than 1 year old',5),
	(27,6,'1 - 2 years old',4),
	(28,6,'2 - 4 years old',2),
	(30,6,'4 - 5 years old',3),
	(31,6,'5 - 7 years old',1),
	(32,10,'Like New W/o Any Major Scratches or Dents',5),
	(33,10,'Good With Some Scratches and no Dents',4),
	(34,10,'Average With Some Dents and Scratches',3),
	(35,10,'Some Parts are Broken',2),
	(36,10,'Looks Bad',1),
	(37,8,'Like New',5),
	(38,8,'Good ',4),
	(39,8,'Average',3),
	(40,8,'Bad',2),
	(41,9,'5 star',5),
	(42,9,'4 star',4),
	(43,9,'3 star',3),
	(44,9,'2 star',2),
	(45,9,'1 star',1),
	(50,12,'Less than 1 year old',5),
	(51,12,'1 - 2 years old',4),
	(52,12,'2 - 4 years old',3),
	(53,12,'4 - 5 years old',2),
	(54,12,'5 - 7 years old',1),
	(55,13,'Insta cooling',5),
	(56,13,'Good',4),
	(57,13,'Average',3),
	(58,13,'Poor',2),
	(59,14,'5 star',5),
	(60,14,'4 star',4),
	(61,14,'3 star',3),
	(62,14,'2 star',2),
	(63,14,'1 star',1),
	(64,15,'Like New',5),
	(65,15,'Good',4),
	(66,15,'Average',3),
	(67,15,'Bad',2),
	(68,16,'Excellent',5),
	(69,16,'Good',4),
	(70,16,'Average',3),
	(71,16,'Bad',2),
	(72,18,'Excellent Working condition',5),
	(73,18,'Good working condition',4),
	(74,18,'Average Working condition',3),
	(75,19,'Less than 1 year old',2),
	(76,20,'Excellent',5),
	(77,20,'Good',4),
	(78,20,'Average',3),
	(79,20,'Bad',2),
	(80,21,'Looks New w/o scratches/bent',5),
	(81,21,'Good',4),
	(82,21,'Average',3),
	(83,21,'Bad',2),
	(84,22,'Excellent',5),
	(85,22,'Good',4),
	(86,22,'Average',3),
	(87,22,'Bad',2),
	(88,23,'Excellent',5),
	(89,23,'Good',4),
	(90,23,'Average',3),
	(91,23,'Bad',2),
	(92,24,'Less than 1 year old',5),
	(93,24,'1 - 2 years old',4),
	(94,24,'2 - 4 years old',3),
	(95,24,'4 - 5 years old',2),
	(96,25,'Excellent',5),
	(97,25,'Good',4),
	(98,25,'Average',3),
	(99,25,'Bad',2),
	(100,26,'Excellent',5),
	(101,26,'Good',4),
	(102,26,'Average',3),
	(103,26,'Bad',2),
	(104,27,'Excellent',5),
	(105,27,'Good',4),
	(106,27,'Average',3),
	(107,27,'Bad',2),
	(108,28,'Excellent',5),
	(109,28,'Good',4),
	(110,28,'Average',3),
	(111,28,'Bad',2),
	(112,29,'Excellent',5),
	(113,29,'Good',4),
	(114,29,'Average',3),
	(115,29,'Bad',2),
	(116,30,'Fast',5),
	(117,30,'Good',4),
	(118,30,'Average',3),
	(119,30,'Bad',2),
	(120,31,'Zero Noise',5),
	(121,31,'very little noise',4),
	(122,31,'high noise',3),
	(123,32,'Easily Adjustable',5),
	(124,32,'Adjustable',4),
	(125,32,'very Tight',3),
	(126,33,'Excellent',5),
	(127,33,'Good',4),
	(128,33,'Average',3),
	(129,33,'Bad',2),
	(130,34,'Easily movable',5),
	(131,34,'Movable',4),
	(132,34,'Needs Lubrication',3),
	(133,34,'Tight',2),
	(134,35,'Excellent',5),
	(135,35,'Good',4),
	(136,35,'Average',3),
	(137,35,'Bad',2),
	(138,36,'Excellent',5),
	(139,36,'Good',4),
	(140,36,'Average',3),
	(141,36,'Bad',2),
	(142,7,'High',5),
	(143,7,'Good',4),
	(144,7,'Average',3),
	(145,7,'Bad',2),
	(146,19,'1 - 2 years old',2),
	(147,19,'2 - 4 years old',5),
	(148,19,'4 - 5 years old',2),
	(149,19,'5 - 7 years old',2),
	(150,24,'5 - 7 years old',2);

/*!40000 ALTER TABLE `qualityindexvalues` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table qualityratings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `qualityratings`;

CREATE TABLE `qualityratings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` int(11) NOT NULL,
  `qualityindex_id` int(11) NOT NULL,
  `productitem_id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `description` varchar(256) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `qualityratings` WRITE;
/*!40000 ALTER TABLE `qualityratings` DISABLE KEYS */;

INSERT INTO `qualityratings` (`id`, `value`, `qualityindex_id`, `productitem_id`, `timestamp`, `description`)
VALUES
	(1,2,6,1,'2016-05-22 16:09:47',''),
	(2,4,7,1,'2016-05-22 16:09:47',''),
	(3,4,8,1,'2016-05-22 16:09:47',''),
	(4,5,9,1,'2016-05-22 16:09:47',''),
	(5,4,10,1,'2016-05-22 16:09:47',''),
	(6,3,6,2,'2016-05-22 16:18:01',''),
	(7,4,7,2,'2016-05-22 16:18:01',''),
	(8,4,8,2,'2016-05-22 16:18:01',''),
	(9,4,9,2,'2016-05-22 16:18:01',''),
	(10,4,10,2,'2016-05-22 16:18:01',''),
	(11,2,6,3,'2016-05-22 16:23:05',''),
	(12,5,7,3,'2016-05-22 16:23:05',''),
	(13,5,8,3,'2016-05-22 16:23:05',''),
	(14,5,9,3,'2016-05-22 16:23:05',''),
	(15,5,10,3,'2016-05-22 16:23:05',''),
	(16,3,6,4,'2016-05-22 16:28:28',''),
	(17,4,7,4,'2016-05-22 16:28:28',''),
	(18,5,8,4,'2016-05-22 16:28:28',''),
	(19,4,9,4,'2016-05-22 16:28:28',''),
	(20,4,10,4,'2016-05-22 16:28:28',''),
	(21,3,6,5,'2016-05-22 16:30:37',''),
	(22,5,7,5,'2016-05-22 16:30:37',''),
	(23,5,8,5,'2016-05-22 16:30:37',''),
	(24,4,9,5,'2016-05-22 16:30:37',''),
	(25,4,10,5,'2016-05-22 16:30:37',''),
	(26,2,6,6,'2016-05-22 16:35:25',''),
	(27,5,7,6,'2016-05-22 16:35:25',''),
	(28,5,8,6,'2016-05-22 16:35:25',''),
	(29,5,9,6,'2016-05-22 16:35:25',''),
	(30,4,10,6,'2016-05-22 16:35:25',''),
	(31,5,6,7,'2016-05-22 16:42:30',''),
	(32,5,7,7,'2016-05-22 16:42:30',''),
	(33,5,8,7,'2016-05-22 16:42:30',''),
	(34,5,9,7,'2016-05-22 16:42:30',''),
	(35,5,10,7,'2016-05-22 16:42:30',''),
	(36,2,1,8,'2016-05-22 16:51:12',''),
	(37,4,2,8,'2016-05-22 16:51:12',''),
	(38,4,3,8,'2016-05-22 16:51:12',''),
	(39,4,4,8,'2016-05-22 16:51:12',''),
	(40,3,5,8,'2016-05-22 16:51:12',''),
	(41,4,30,8,'2016-05-22 16:51:12',''),
	(42,5,31,8,'2016-05-22 16:51:12',''),
	(43,5,1,9,'2016-05-22 17:02:26',''),
	(44,5,2,9,'2016-05-22 17:02:26',''),
	(45,5,3,9,'2016-05-22 17:02:26',''),
	(46,5,4,9,'2016-05-22 17:02:26',''),
	(47,5,5,9,'2016-05-22 17:02:26',''),
	(48,5,30,9,'2016-05-22 17:02:26',''),
	(49,5,31,9,'2016-05-22 17:02:26',''),
	(50,5,1,10,'2016-05-22 17:11:58',''),
	(51,5,2,10,'2016-05-22 17:11:58',''),
	(52,5,3,10,'2016-05-22 17:11:58',''),
	(53,5,4,10,'2016-05-22 17:11:58',''),
	(54,5,5,10,'2016-05-22 17:11:58',''),
	(55,5,30,10,'2016-05-22 17:11:58',''),
	(56,5,31,10,'2016-05-22 17:11:58',''),
	(57,5,1,11,'2016-05-22 17:19:24',''),
	(58,5,2,11,'2016-05-22 17:19:25',''),
	(59,5,3,11,'2016-05-22 17:19:25',''),
	(60,5,4,11,'2016-05-22 17:19:25',''),
	(61,5,5,11,'2016-05-22 17:19:25',''),
	(62,5,30,11,'2016-05-22 17:19:25',''),
	(63,5,31,11,'2016-05-22 17:19:25',''),
	(64,5,1,12,'2016-05-22 17:28:27',''),
	(65,5,2,12,'2016-05-22 17:28:27',''),
	(66,5,3,12,'2016-05-22 17:28:27',''),
	(67,5,4,12,'2016-05-22 17:28:27',''),
	(68,5,5,12,'2016-05-22 17:28:27',''),
	(69,5,30,12,'2016-05-22 17:28:27',''),
	(70,5,31,12,'2016-05-22 17:28:27',''),
	(71,1,1,13,'2016-05-23 02:33:57',''),
	(72,4,2,13,'2016-05-23 02:33:57',''),
	(73,3,3,13,'2016-05-23 02:33:57',''),
	(74,4,4,13,'2016-05-23 02:33:57',''),
	(75,4,5,13,'2016-05-23 02:33:57',''),
	(76,5,30,13,'2016-05-23 02:33:57',''),
	(77,5,31,13,'2016-05-23 02:33:57',''),
	(78,2,1,14,'2016-05-23 02:40:27',''),
	(79,4,2,14,'2016-05-23 02:40:27',''),
	(80,5,3,14,'2016-05-23 02:40:27',''),
	(81,4,4,14,'2016-05-23 02:40:27',''),
	(82,4,5,14,'2016-05-23 02:40:27',''),
	(83,5,30,14,'2016-05-23 02:40:27',''),
	(84,5,31,14,'2016-05-23 02:40:27',''),
	(85,2,1,15,'2016-05-23 02:54:12',''),
	(86,4,2,15,'2016-05-23 02:54:12',''),
	(87,4,3,15,'2016-05-23 02:54:12',''),
	(88,4,4,15,'2016-05-23 02:54:12',''),
	(89,4,5,15,'2016-05-23 02:54:12',''),
	(90,5,30,15,'2016-05-23 02:54:12',''),
	(91,5,31,15,'2016-05-23 02:54:12',''),
	(92,3,1,16,'2016-05-24 13:38:19',''),
	(93,4,2,16,'2016-05-23 03:03:00',''),
	(94,4,3,16,'2016-05-23 03:03:00',''),
	(95,4,4,16,'2016-05-23 03:03:00',''),
	(96,4,5,16,'2016-05-23 03:03:00',''),
	(97,4,30,16,'2016-05-23 03:03:00',''),
	(98,5,31,16,'2016-05-23 03:03:00',''),
	(99,3,1,17,'2016-05-23 03:10:21',''),
	(100,4,2,17,'2016-05-23 03:10:21',''),
	(101,5,3,17,'2016-05-23 03:10:21',''),
	(102,5,4,17,'2016-05-23 03:10:21',''),
	(103,5,5,17,'2016-05-23 03:10:21',''),
	(104,5,30,17,'2016-05-23 03:10:21',''),
	(105,5,31,17,'2016-05-23 03:10:21',''),
	(106,5,1,18,'2016-05-23 03:16:33',''),
	(107,5,2,18,'2016-05-23 03:16:33',''),
	(108,5,3,18,'2016-05-23 03:16:33',''),
	(109,5,4,18,'2016-05-23 03:16:33',''),
	(110,5,5,18,'2016-05-23 03:16:33',''),
	(111,5,30,18,'2016-05-23 03:16:33',''),
	(112,5,31,18,'2016-05-23 03:16:33',''),
	(113,5,1,19,'2016-05-23 03:22:59',''),
	(114,5,2,19,'2016-05-23 03:22:59',''),
	(115,5,3,19,'2016-05-23 03:22:59',''),
	(116,5,4,19,'2016-05-23 03:22:59',''),
	(117,5,5,19,'2016-05-23 03:22:59',''),
	(118,5,30,19,'2016-05-23 03:22:59',''),
	(119,5,31,19,'2016-05-23 03:22:59',''),
	(120,2,1,20,'2016-05-23 03:31:44',''),
	(121,4,2,20,'2016-05-23 03:31:44',''),
	(122,4,3,20,'2016-05-23 03:31:44',''),
	(123,4,4,20,'2016-05-23 03:31:44',''),
	(124,4,5,20,'2016-05-23 03:31:44',''),
	(125,5,30,20,'2016-05-23 03:31:44',''),
	(126,5,31,20,'2016-05-23 03:31:44',''),
	(127,1,1,21,'2016-05-23 03:45:05',''),
	(128,4,2,21,'2016-05-23 03:45:05',''),
	(129,4,3,21,'2016-05-23 03:45:06',''),
	(130,4,4,21,'2016-05-23 03:45:06',''),
	(131,4,5,21,'2016-05-23 03:45:06',''),
	(132,4,30,21,'2016-05-23 03:45:06',''),
	(133,5,31,21,'2016-05-23 03:45:06',''),
	(134,1,1,22,'2016-05-23 03:49:39',''),
	(135,4,2,22,'2016-05-23 03:49:39',''),
	(136,3,3,22,'2016-05-23 03:49:39',''),
	(137,4,4,22,'2016-05-23 03:49:39',''),
	(138,4,5,22,'2016-05-23 03:49:39',''),
	(139,4,30,22,'2016-05-23 03:49:39',''),
	(140,5,31,22,'2016-05-23 03:49:39',''),
	(141,2,1,23,'2016-05-23 03:53:49',''),
	(142,4,2,23,'2016-05-23 03:53:49',''),
	(143,4,3,23,'2016-05-23 03:53:49',''),
	(144,4,4,23,'2016-05-23 03:53:49',''),
	(145,4,5,23,'2016-05-23 03:53:49',''),
	(146,5,30,23,'2016-05-23 03:53:49',''),
	(147,5,31,23,'2016-05-23 03:53:49',''),
	(148,5,1,24,'2016-05-23 03:59:28',''),
	(149,5,2,24,'2016-05-23 03:59:28',''),
	(150,5,3,24,'2016-05-23 03:59:28',''),
	(151,5,4,24,'2016-05-23 03:59:28',''),
	(152,5,5,24,'2016-05-23 03:59:28',''),
	(153,5,30,24,'2016-05-23 03:59:28',''),
	(154,5,31,24,'2016-05-23 03:59:28',''),
	(155,2,1,25,'2016-05-23 09:19:56',''),
	(156,4,2,25,'2016-05-23 09:19:56',''),
	(157,4,3,25,'2016-05-23 09:19:56',''),
	(158,4,4,25,'2016-05-23 09:19:56',''),
	(159,4,5,25,'2016-05-23 09:19:56',''),
	(160,4,30,25,'2016-05-23 09:19:56',''),
	(161,5,31,25,'2016-05-23 09:19:56',''),
	(162,1,1,26,'2016-05-25 10:32:50',''),
	(163,4,2,26,'2016-05-23 13:48:18',''),
	(164,5,3,26,'2016-05-23 13:48:18',''),
	(165,3,4,26,'2016-05-25 10:33:16',''),
	(166,4,5,26,'2016-05-23 13:48:18',''),
	(167,5,30,26,'2016-05-23 13:48:18',''),
	(168,5,31,26,'2016-05-23 13:48:18',''),
	(169,3,1,27,'2016-05-24 07:15:32',''),
	(170,5,2,27,'2016-05-24 07:15:32',''),
	(171,5,3,27,'2016-05-24 07:15:32',''),
	(172,5,4,27,'2016-05-24 07:15:32',''),
	(173,4,5,27,'2016-05-24 07:15:32',''),
	(174,5,30,27,'2016-05-24 07:15:32',''),
	(175,5,31,27,'2016-05-24 07:15:32',''),
	(176,4,1,28,'2016-05-24 12:31:32',''),
	(177,4,2,28,'2016-05-24 12:31:32',''),
	(178,5,3,28,'2016-05-24 12:31:32',''),
	(179,5,4,28,'2016-05-24 12:31:32',''),
	(180,4,5,28,'2016-05-24 12:31:32',''),
	(181,5,30,28,'2016-05-24 12:31:32',''),
	(182,5,31,28,'2016-05-24 12:31:32',''),
	(183,4,6,29,'2016-05-24 13:01:30',''),
	(184,4,7,29,'2016-05-24 12:59:15',''),
	(185,4,8,29,'2016-05-24 12:59:15',''),
	(186,5,9,29,'2016-05-24 12:59:15',''),
	(187,4,10,29,'2016-05-24 12:59:15',''),
	(188,5,1,30,'2016-05-24 15:36:34',''),
	(189,4,2,30,'2016-05-24 15:36:34',''),
	(190,5,3,30,'2016-05-24 15:36:34',''),
	(191,4,4,30,'2016-05-24 15:36:34',''),
	(192,4,5,30,'2016-05-24 15:36:34',''),
	(193,5,30,30,'2016-05-24 15:36:34',''),
	(194,5,31,30,'2016-05-24 15:36:34',''),
	(195,4,32,31,'2016-05-26 10:33:37',''),
	(196,5,33,31,'2016-05-26 10:33:37',''),
	(197,5,34,31,'2016-05-26 10:33:37',''),
	(198,5,32,32,'2016-05-26 10:36:58',''),
	(199,5,33,32,'2016-05-26 10:36:58',''),
	(200,5,34,32,'2016-05-26 10:36:58',''),
	(201,4,6,33,'2016-05-26 11:21:20',''),
	(202,5,7,33,'2016-05-26 11:21:20',''),
	(203,5,8,33,'2016-05-26 11:21:20',''),
	(204,5,9,33,'2016-05-26 11:21:20',''),
	(205,5,10,33,'2016-05-26 11:21:20',''),
	(206,5,6,34,'2016-05-26 11:52:38',''),
	(207,5,7,34,'2016-05-26 11:52:38',''),
	(208,5,8,34,'2016-05-26 11:52:38',''),
	(209,5,9,34,'2016-05-26 11:52:38',''),
	(210,5,10,34,'2016-05-26 11:52:38',''),
	(211,4,32,35,'2016-05-27 02:33:57',''),
	(212,4,33,35,'2016-05-27 02:33:57',''),
	(213,5,34,35,'2016-05-27 02:33:57',''),
	(214,4,32,36,'2016-05-27 02:33:57',''),
	(215,4,33,36,'2016-05-27 02:33:57',''),
	(216,5,34,36,'2016-05-27 02:33:57',''),
	(217,4,32,37,'2016-05-27 02:33:57',''),
	(218,4,33,37,'2016-05-27 02:33:57',''),
	(219,5,34,37,'2016-05-27 02:33:57',''),
	(220,4,32,38,'2016-05-27 02:33:57',''),
	(221,4,33,38,'2016-05-27 02:33:57',''),
	(222,5,34,38,'2016-05-27 02:33:57',''),
	(223,4,32,39,'2016-05-27 02:33:57',''),
	(224,4,33,39,'2016-05-27 02:33:57',''),
	(225,5,34,39,'2016-05-27 02:33:57',''),
	(226,4,32,40,'2016-05-27 02:33:57',''),
	(227,4,33,40,'2016-05-27 02:33:57',''),
	(228,5,34,40,'2016-05-27 02:33:57',''),
	(229,4,32,41,'2016-05-27 02:33:57',''),
	(230,4,33,41,'2016-05-27 02:33:57',''),
	(231,5,34,41,'2016-05-27 02:33:57',''),
	(232,4,32,42,'2016-05-27 02:33:57',''),
	(233,4,33,42,'2016-05-27 02:33:57',''),
	(234,5,34,42,'2016-05-27 02:33:57',''),
	(235,4,32,43,'2016-05-27 02:33:57',''),
	(236,4,33,43,'2016-05-27 02:33:57',''),
	(237,5,34,43,'2016-05-27 02:33:57',''),
	(238,4,32,44,'2016-05-27 02:33:57',''),
	(239,4,33,44,'2016-05-27 02:33:57',''),
	(240,5,34,44,'2016-05-27 02:33:57',''),
	(241,5,32,45,'2016-05-27 02:35:38',''),
	(242,5,33,45,'2016-05-27 02:35:38',''),
	(243,5,34,45,'2016-05-27 02:35:38',''),
	(244,5,32,46,'2016-05-27 02:35:38',''),
	(245,5,33,46,'2016-05-27 02:35:38',''),
	(246,5,34,46,'2016-05-27 02:35:38',''),
	(247,5,32,47,'2016-05-27 02:35:38',''),
	(248,5,33,47,'2016-05-27 02:35:38',''),
	(249,5,34,47,'2016-05-27 02:35:38',''),
	(250,5,32,48,'2016-05-27 02:35:38',''),
	(251,5,33,48,'2016-05-27 02:35:38',''),
	(252,5,34,48,'2016-05-27 02:35:38',''),
	(253,5,32,49,'2016-05-27 02:35:38',''),
	(254,5,33,49,'2016-05-27 02:35:38',''),
	(255,5,34,49,'2016-05-27 02:35:38',''),
	(256,5,32,50,'2016-05-27 02:35:38',''),
	(257,5,33,50,'2016-05-27 02:35:38',''),
	(258,5,34,50,'2016-05-27 02:35:38',''),
	(259,5,32,51,'2016-05-27 02:35:38',''),
	(260,5,33,51,'2016-05-27 02:35:38',''),
	(261,5,34,51,'2016-05-27 02:35:38',''),
	(262,5,32,52,'2016-05-27 02:35:38',''),
	(263,5,33,52,'2016-05-27 02:35:38',''),
	(264,5,34,52,'2016-05-27 02:35:38',''),
	(265,5,32,53,'2016-05-27 02:35:38',''),
	(266,5,33,53,'2016-05-27 02:35:38',''),
	(267,5,34,53,'2016-05-27 02:35:38',''),
	(268,5,32,54,'2016-05-27 02:35:38',''),
	(269,5,33,54,'2016-05-27 02:35:38',''),
	(270,5,34,54,'2016-05-27 02:35:38','');

/*!40000 ALTER TABLE `qualityratings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;

INSERT INTO `roles` (`id`, `name`, `description`)
VALUES
	(1,'login','Login privileges, granted after account confirmation'),
	(2,'admin','Administrative user, has access to everything.'),
	(3,'dealer','this is a delaer');

/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table roles_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `roles_users`;

CREATE TABLE `roles_users` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `fk_role_id` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `roles_users` WRITE;
/*!40000 ALTER TABLE `roles_users` DISABLE KEYS */;

INSERT INTO `roles_users` (`user_id`, `role_id`)
VALUES
	(1,1),
	(2,1),
	(3,1),
	(4,1),
	(5,1),
	(6,1),
	(7,1),
	(8,1),
	(9,1),
	(10,1),
	(11,1),
	(12,1),
	(13,1),
	(14,1),
	(15,1),
	(16,1),
	(17,1),
	(18,1),
	(19,1),
	(20,1),
	(21,1),
	(22,1),
	(23,1),
	(24,1),
	(25,1),
	(26,1),
	(27,1),
	(28,1),
	(29,1),
	(30,1),
	(31,1),
	(32,1),
	(33,1),
	(34,1),
	(35,1),
	(36,1),
	(37,1),
	(38,1),
	(39,1),
	(40,1),
	(41,1),
	(42,1),
	(43,1),
	(44,1),
	(45,1),
	(46,1),
	(47,1),
	(48,1),
	(49,1),
	(50,1),
	(51,1),
	(52,1),
	(53,1),
	(54,1),
	(55,1),
	(56,1),
	(57,1),
	(58,1),
	(59,1),
	(60,1),
	(61,1),
	(62,1),
	(63,1),
	(64,1),
	(65,1),
	(66,1),
	(67,1),
	(68,1),
	(69,1),
	(70,1);

/*!40000 ALTER TABLE `roles_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table technicalspecs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `technicalspecs`;

CREATE TABLE `technicalspecs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(512) NOT NULL,
  `productcategory_id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `technicalspecs` WRITE;
/*!40000 ALTER TABLE `technicalspecs` DISABLE KEYS */;

INSERT INTO `technicalspecs` (`id`, `name`, `productcategory_id`, `timestamp`)
VALUES
	(1,'capacity',3,'2016-03-03 22:36:29');

/*!40000 ALTER TABLE `technicalspecs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table technicalspecvalues
# ------------------------------------------------------------

DROP TABLE IF EXISTS `technicalspecvalues`;

CREATE TABLE `technicalspecvalues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(256) NOT NULL,
  `technicalspec_id` int(11) NOT NULL,
  `productitem_id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `technicalspecvalues` WRITE;
/*!40000 ALTER TABLE `technicalspecvalues` DISABLE KEYS */;

INSERT INTO `technicalspecvalues` (`id`, `value`, `technicalspec_id`, `productitem_id`, `timestamp`)
VALUES
	(1,'200c',1,6,'2016-03-03 23:09:15');

/*!40000 ALTER TABLE `technicalspecvalues` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ticketcomments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ticketcomments`;

CREATE TABLE `ticketcomments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comment` text NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `commentor_id` int(11) NOT NULL,
  `ticket_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_comments_users1_idx` (`commentor_id`),
  KEY `fk_comments_tickets1_idx` (`ticket_id`),
  CONSTRAINT `fk_comments_tickets1` FOREIGN KEY (`ticket_id`) REFERENCES `tickets` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_comments_users1` FOREIGN KEY (`commentor_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tickets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tickets`;

CREATE TABLE `tickets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` text NOT NULL,
  `subject` varchar(256) NOT NULL DEFAULT '',
  `resolved` tinyint(1) DEFAULT '0',
  `date` datetime NOT NULL,
  `tickettype_id` int(11) DEFAULT '1',
  `user_id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tickets_tickettypes1_idx` (`tickettype_id`),
  KEY `fk_tickets_users1_idx` (`user_id`),
  CONSTRAINT `fk_tickets_tickettypes1` FOREIGN KEY (`tickettype_id`) REFERENCES `tickettypes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tickets_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tickettypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tickettypes`;

CREATE TABLE `tickettypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tickettypes` WRITE;
/*!40000 ALTER TABLE `tickettypes` DISABLE KEYS */;

INSERT INTO `tickettypes` (`id`, `name`)
VALUES
	(1,'Service');

/*!40000 ALTER TABLE `tickettypes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_tokens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_tokens`;

CREATE TABLE `user_tokens` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `user_agent` varchar(40) NOT NULL,
  `token` varchar(40) NOT NULL,
  `created` int(10) unsigned NOT NULL,
  `expires` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_token` (`token`),
  KEY `fk_user_id` (`user_id`),
  KEY `expires` (`expires`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `user_tokens` WRITE;
/*!40000 ALTER TABLE `user_tokens` DISABLE KEYS */;

INSERT INTO `user_tokens` (`id`, `user_id`, `user_agent`, `token`, `created`, `expires`)
VALUES
	(5,2,'0d85853bdbb4a4c2d9d9525cef950ec4597174f9','09835743db5f27fe4247dbd945a9cbda8cd35823',1463927662,1464791662),
	(6,2,'5ccc9bdb116774eb957fa32e650705a819aa594e','d9f1497b5c1f62d7ec65df23a0d3d34e1dc32ff8',1463928716,1464792716),
	(8,1,'5ccc9bdb116774eb957fa32e650705a819aa594e','291e7ff8d710b922e0e891a453295501cef5c42a',1463975630,1464839630),
	(9,3,'5ccc9bdb116774eb957fa32e650705a819aa594e','7d56810322da2173c6d5ef6cc6ae79968aa25c5d',1463976974,1464840974),
	(12,4,'927058fd22cf331e469deb882a4372ea5d143461','f40ebfc8c0696f0d5e8e646dd824c4e2bbacd0c9',1463980954,1464844954),
	(13,5,'21b8892f102c2d762de2bbe50ededcdea8a6ee0e','df34f89d7622d4a1e46609a2c091dfa41726ad2e',1463981531,1464845531),
	(14,6,'b1fcf97acc8ed8f6a47eeb3c45a8c7ba58ae2f1c','cf59311b43d7f895df399c6b20d306324260b563',1463984638,1464848638),
	(16,2,'0d85853bdbb4a4c2d9d9525cef950ec4597174f9','0a40b7bf0bf97ca200d2739e6d6d6a3b1e59de08',1463995060,1464859060),
	(17,8,'7108bb0deb6a4368c467d84babe7681292579062','366a8594f7e9664e2d0e543a26eb69975586335a',1463997563,1464861563),
	(18,9,'21b8892f102c2d762de2bbe50ededcdea8a6ee0e','10c83477a2b748db8f76451a2a2ec5d9faad4b57',1463997627,1464861627),
	(19,10,'48726ebcd81bf515164c0df8f34e5fea1779f796','317e66ef61e8e77436e7c7f4cdad273c0bfd3246',1463999362,1464863362),
	(21,11,'2bd2bed44e816fe40b23dc2ecc12005081b088b4','b98f43295ae57b8fd2cad11ec099fbd9b4ccadfa',1464001822,1464865822),
	(22,12,'826ab713976af70f0dd36356f276e0672cbabd78','1feefa73620b3a0407dfd17aed357e020f08f880',1464002528,1464866528),
	(23,13,'22d14318c36ee86b76782903da3d72468db7071c','83f99e9336c992bf3326666cee2e0679a9a79c0d',1464003754,1464867754),
	(24,1,'5ccc9bdb116774eb957fa32e650705a819aa594e','875cc6dbb5279f45db04bb35b28ad676ac2820ee',1464004817,1464868817),
	(25,15,'21b8892f102c2d762de2bbe50ededcdea8a6ee0e','da6395556d08d9747d310bafd49858d3ffeaa6d1',1464004989,1464868989),
	(26,16,'28025d6b5c5c4ad697c6aa93b5b211cf7bf68b31','7a2d26891d5aff1b0bd5bfa4abbdeb73eea83736',1464005390,1464869390),
	(27,17,'19c60b42e7f76c80ffd9dce3ae53e17d30f04b5e','200a702e0827a046f5946619203c2e6eebd89260',1464005787,1464869787),
	(28,18,'fd3eb0e667d23b3ee67fc96dc36cc7ee0c6c7749','52e84e2ccc80067357d772af7dc10af2774d725e',1464006283,1464870283),
	(29,19,'21b8892f102c2d762de2bbe50ededcdea8a6ee0e','b1a296c8fd78497811811dce025b1c33eae979fb',1464006965,1464870965),
	(31,21,'40c59a933b40ae44f8b2a518002f995d0c398e84','3f5f3d83299bf8aa92270a31b4ac4d200dc1a332',1464008041,1464872041),
	(32,22,'9052e9b347a34a483e9d24bee2fca90ae3926dfd','a819c38aa6564caf572c6860ccdd4cfad95cf309',1464009374,1464873374),
	(33,23,'f62cfb43c19a5f58bca402d2f48358a691e9270a','58c5f6619782a40714e73797d51281ac4539b9ee',1464009507,1464873507),
	(34,24,'ec5b5735ea5c777a410742d1e3f02ee9d1f2e488','671c6b6a80c127f7a5b0f96ffceca69187f51c69',1464009597,1464873597),
	(36,25,'2bd2bed44e816fe40b23dc2ecc12005081b088b4','dc6bfe3530f3a7560d1a9ad998350b20ec542dbe',1464009665,1464873665),
	(38,26,'3f8a27a3d6f81c4d780fcbd0eebc03c205255f3a','3f8940aadaf9032729214049246b71cd43e848e7',1464013683,1464877683),
	(39,27,'24b264e35e8d623f5f489335878caf7986d9b9aa','2d18ce56d511b0f4d8394e8f127e59f4c14fc65e',1464014066,1464878066),
	(42,30,'48c598976e87e2b556da3f405ff01ca5e1cb4b82','03acdfe8bdfcaca1310c56bc25d5b26dc4777624',1464014692,1464878692),
	(44,32,'050eaedfb5bcf7ca629eac2cda56cba3854305d9','b637c3c8bf62abff5bcae51ba10f2a01609afc41',1464015561,1464879561),
	(46,33,'11fcdfdd74bee49876a5e4d562e21fa21cb5fa04','a35648d7323e057e33d7979c4d1a4e70ef164778',1464017569,1464881569),
	(47,34,'11fcdfdd74bee49876a5e4d562e21fa21cb5fa04','8854ed03758baeccda5fd1f43e50bf5580cfde1f',1464017697,1464881697),
	(51,39,'16f242ece164540a02003340d1abda5062d4efad','381d977b4ce8f08ceb90880f0760fc0d653d3aef',1464026395,1464890395),
	(60,41,'e3a25e5cb4d5507622daaac5d5db490c1549c9d2','219a4e0b14869e2ab219ae8fb4b5a1323c931cbc',1464061622,1464925622),
	(61,15,'4fcb8c6f9decbe0fafb3bbca02f38c84fe4054cf','76ce281610209a37b4154044278d6b613cd6ba35',1464065136,1464929136),
	(62,42,'4387072aaa04dc41c93e1cae6e2d786eaee79034','ef5fa4718997990caab3f5a030d61572a434186c',1464066021,1464930021),
	(65,43,'d2b389a24c8fe70657e09a0fcc1e70d5288574c6','82998b31425bd8335a1a7256613fd1e2ffae2465',1464070557,1464934557),
	(66,44,'927058fd22cf331e469deb882a4372ea5d143461','5ae44875e5d4f71054823c06503ff686f1458b60',1464070792,1464934792),
	(67,45,'8d47932abbaea4ad4e7db5f5c8932b7ce17dff01','46a5dc28362ae465e06228933df21d039dc472c5',1464072162,1464936162),
	(68,46,'47138e4b823f9b2ad3f45e57b66704e7a7fc9fc9','55be47a305423efe963bd5d2db75bd44477b1054',1464072963,1464936963),
	(76,48,'bc9c5bd18262a4a255a125410c36396f094728cf','fc1bae8adcb767704d569fef0a0d356441e6d023',1464082894,1464946894),
	(77,49,'8a43ac18875f117fb04b31a598c86b09254072cd','6728e7f571b5478b471fac20292309cc02d2bb73',1464084288,1464948288),
	(80,50,'b20ea6e704c8fa49b201030a761b57ed6b54b783','78cba53644422453071134c6004c097e250ff4c9',1464085404,1464949404),
	(81,51,'c5f555ec31cf709e18f608700bd56b48efc44708','8fae28a6304bb623fe60280a1f596e316576140b',1464085563,1464949563),
	(86,52,'a211986cbbea6513cc6027bc0993d001203336ab','d38e51119353eda7a335a09eec92e8bce9112151',1464088884,1464952884),
	(87,53,'21b8892f102c2d762de2bbe50ededcdea8a6ee0e','5ea15581a43cfd7b74c0f49a82880d28a8229880',1464090687,1464954687),
	(91,55,'21b8892f102c2d762de2bbe50ededcdea8a6ee0e','f444ad04b2b26bbbcdc1d8fc0376a0627646c9e1',1464093770,1464957770),
	(93,56,'d7bedb763a81be6033b1b8e51556cf44fc1e19f9','47dab8e74d5df1adbc03880daf8f761d93afeca2',1464094719,1464958719),
	(94,1,'5ccc9bdb116774eb957fa32e650705a819aa594e','980c8b21298a88ffe761f93099276bc712a1e3c7',1464095253,1464959253),
	(95,1,'12eef7c9a7cfd4d5d94e79e6912a9b0a0d7f9a1c','8ad476d1a845d13c96e850ad1e6a14efbceb136a',1464097521,1464961521),
	(97,57,'137377a3df5b24d69b6a13e79346e3ca988b3ddc','bb7f38b6405677a57033b06d85254f9d52336f32',1464100764,1464964764),
	(98,58,'ba95f9620c2bab94a0a2d8f76c6543d68aab1781','78908d26a9f38b1c4be04d84f860b03bbc0a8df9',1464106774,1464970774),
	(99,59,'cf28105a3e12a771fdb63a9dd2eeec7859405d0a','38fc99f09e99f4a0db972ab550d14749825efeef',1464111155,1464975155),
	(100,3,'27b0b33688c3c187d8415500e5f1282b23685c03','069ac4a8cc94954717999d8712d376480c40dc6b',1464115109,1464979108),
	(101,60,'27b0b33688c3c187d8415500e5f1282b23685c03','529ee76e211652c8a64ba8e9d50b5d0605260b45',1464115140,1464979140),
	(104,1,'5ccc9bdb116774eb957fa32e650705a819aa594e','5f3d80e21d3ca4f463c0c6113483dd2f90708928',1464149839,1465013839),
	(105,48,'5504023284ae9eb355e1b844dd9e2aa1ae77924f','a1d2b1c8866f5cda0eaad3ed62036d115a22b6dc',1464156736,1465020736),
	(106,2,'21b8892f102c2d762de2bbe50ededcdea8a6ee0e','0584cd1f92e19bdf9c34690788b3976d0cae4dbd',1464162481,1465026481),
	(107,2,'21b8892f102c2d762de2bbe50ededcdea8a6ee0e','c2cb98ba3a98a4f0f563fcea6a8174d8bd381ee1',1464166005,1465030005),
	(108,1,'0d85853bdbb4a4c2d9d9525cef950ec4597174f9','85168aae64b3eedc8f64cc9dbfd898352f30c28e',1464171121,1465035121),
	(109,21,'40c59a933b40ae44f8b2a518002f995d0c398e84','fe93bb7b3faeca46128fef69ac26c3c79abb686d',1464176398,1465040398),
	(112,62,'5456b95ad2e29619bec0eb5753a4ea7bfc3cba1b','ad4d2fce1a72608b75d0591690231d19e74b2e02',1464234079,1465098079),
	(117,63,'c3780be74fcfb8fc27be98d8b238c7db6ba823cb','6feea42e9cf6bd22e132291e239108a88d80ed9a',1464266811,1465130811),
	(118,64,'3a8c0bdc0c031cce402ab6fbafededa0750dd3ce','d21da8a76ed2f7cd2fb6cf6664171d371beafe5f',1464271264,1465135264),
	(119,65,'24b264e35e8d623f5f489335878caf7986d9b9aa','9fc727051c230d37a7922213edbae76e0487bc83',1464274309,1465138309),
	(120,66,'8c5646cf7c4dedeedfb39cb99f5e2b1a17ca52cd','13f32608d575b847635f461b0e05079f124e628b',1464275055,1465139055),
	(122,3,'5ccc9bdb116774eb957fa32e650705a819aa594e','823f6e9eed45e23af598db1f3123f922cb564f3d',1464283872,1465147872),
	(123,1,'e5554e39851cb271bf553d23e4c7170ed34b261f','2ab321a24d521de8002d4972043ceeb4a2e308c8',1464284220,1465148220),
	(124,67,'2778d31064a954c1acb9d455e235e2809cd60bd3','cfc0114db07c4718bf96352975eea3fe003af1ff',1464284733,1465148733),
	(125,1,'5ccc9bdb116774eb957fa32e650705a819aa594e','1f04389049b2d858209a9db64d947d5c8cdebed8',1464285220,1465149220),
	(126,3,'f4055e68e22ed5e5def8846d58158ecb27400df6','245b27a7e2124bd2e9508946568fabc17ff822b6',1464285420,1465149420),
	(128,68,'2778d31064a954c1acb9d455e235e2809cd60bd3','9fb33deb97b2dbaa72c1a51d6c769ab6f37bf014',1464314485,1465178485),
	(131,70,'5456b95ad2e29619bec0eb5753a4ea7bfc3cba1b','7bfe486fcc9a068d664b51a5a63bf28c4ae86003',1464348537,1465212537);

/*!40000 ALTER TABLE `user_tokens` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table usernotifications
# ------------------------------------------------------------

DROP TABLE IF EXISTS `usernotifications`;

CREATE TABLE `usernotifications` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type_id` int(11) unsigned NOT NULL,
  `item_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `order_id` int(11) NOT NULL,
  `seen` tinyint(1) NOT NULL DEFAULT '0',
  `delivered` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `usernotifications` WRITE;
/*!40000 ALTER TABLE `usernotifications` DISABLE KEYS */;

INSERT INTO `usernotifications` (`id`, `type_id`, `item_id`, `user_id`, `date`, `order_id`, `seen`, `delivered`)
VALUES
	(1,1,18,5,'2016-05-23 11:08:16',1,1,1),
	(2,2,18,5,'2016-05-23 11:08:46',1,1,1),
	(3,1,25,13,'2016-05-23 17:57:40',2,1,1),
	(4,1,13,5,'2016-05-23 18:56:12',5,1,1),
	(5,2,13,5,'2016-05-23 18:56:16',5,1,1),
	(6,3,13,5,'2016-05-23 18:56:19',5,1,1),
	(7,1,14,5,'2016-05-23 19:04:00',6,1,1),
	(8,2,14,5,'2016-05-23 19:04:04',6,1,1),
	(9,3,14,5,'2016-05-23 19:04:14',6,1,1),
	(10,1,3,26,'2016-05-23 20:47:59',7,1,1),
	(11,1,17,18,'2016-05-23 23:55:27',3,0,0),
	(12,1,8,27,'2016-05-24 11:34:23',8,0,0),
	(13,1,26,42,'2016-05-24 11:34:31',9,0,0),
	(14,1,6,42,'2016-05-24 11:34:36',10,0,0),
	(15,1,27,15,'2016-05-24 14:59:54',12,0,1),
	(16,1,1,48,'2016-05-24 16:24:20',14,1,1),
	(17,1,5,54,'2016-05-24 17:58:15',16,1,1),
	(18,1,28,54,'2016-05-24 18:03:00',17,1,1),
	(19,2,25,13,'2016-05-24 18:38:19',2,0,1),
	(20,2,8,27,'2016-05-24 18:38:33',8,0,0),
	(21,1,29,55,'2016-05-24 22:01:14',18,0,0),
	(22,3,25,13,'2016-05-25 07:18:19',2,0,1),
	(23,3,8,27,'2016-05-25 07:24:11',8,0,0),
	(24,2,29,55,'2016-05-25 08:01:56',18,0,0),
	(25,2,28,54,'2016-05-25 08:02:16',17,0,0),
	(26,2,5,54,'2016-05-25 08:02:20',16,0,0),
	(27,2,1,48,'2016-05-25 08:02:32',14,1,1),
	(28,2,6,42,'2016-05-25 08:02:46',10,0,0),
	(29,2,26,42,'2016-05-25 08:02:50',9,0,0),
	(30,2,17,18,'2016-05-25 08:03:05',3,0,0),
	(31,3,17,18,'2016-05-25 12:02:51',3,0,0),
	(32,3,1,48,'2016-05-25 12:02:57',14,0,0),
	(33,3,28,54,'2016-05-25 13:47:10',17,0,0),
	(34,3,5,54,'2016-05-25 13:47:15',16,0,0),
	(35,3,29,55,'2016-05-25 22:21:04',18,0,0),
	(36,1,6,62,'2016-05-26 11:36:42',20,0,1),
	(37,2,6,62,'2016-05-26 17:03:13',20,0,1),
	(38,1,33,63,'2016-05-26 19:06:32',21,0,1),
	(39,1,31,3,'2016-05-26 23:24:06',22,1,1),
	(40,1,32,3,'2016-05-26 23:24:10',22,1,1),
	(41,2,31,3,'2016-05-26 23:25:40',22,1,1),
	(42,3,31,3,'2016-05-26 23:26:03',22,1,1),
	(43,3,6,62,'2016-05-27 12:33:54',20,0,1);

/*!40000 ALTER TABLE `usernotifications` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table usernotificationtypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `usernotificationtypes`;

CREATE TABLE `usernotificationtypes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `usernotificationtypes` WRITE;
/*!40000 ALTER TABLE `usernotificationtypes` DISABLE KEYS */;

INSERT INTO `usernotificationtypes` (`id`, `type`)
VALUES
	(1,'Confirmed'),
	(2,'Shipped'),
	(3,'Delivered'),
	(4,'Cancelled'),
	(5,'Executed'),
	(6,'Replacement'),
	(7,'Replaced');

/*!40000 ALTER TABLE `usernotificationtypes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(256) DEFAULT NULL,
  `username` varchar(256) DEFAULT NULL,
  `password` varchar(256) DEFAULT NULL,
  `phonenumber` varchar(256) DEFAULT NULL,
  `firstname` varchar(512) NOT NULL,
  `lastname` varchar(512) NOT NULL,
  `picture` varchar(256) NOT NULL,
  `isdealer` int(11) NOT NULL DEFAULT '0',
  `location` varchar(512) NOT NULL,
  `locationlat` varchar(512) NOT NULL,
  `locationlng` varchar(512) NOT NULL,
  `place_id` varchar(512) NOT NULL,
  `deliveryradius` int(11) NOT NULL,
  `hash` varchar(256) DEFAULT NULL,
  `isverified` int(11) DEFAULT '0',
  `logins` int(11) DEFAULT NULL,
  `last_login` int(11) DEFAULT NULL,
  `isadmin` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `email`, `username`, `password`, `phonenumber`, `firstname`, `lastname`, `picture`, `isdealer`, `location`, `locationlat`, `locationlng`, `place_id`, `deliveryradius`, `hash`, `isverified`, `logins`, `last_login`, `isadmin`)
VALUES
	(1,'admin@pocketin.in','1','2be4a24eedc38c6d7f117e1276d8adbb9ef4321470abc33c08cb8382ea36c453','1','Admin','Pocketin','none',0,'','','','',0,NULL,1,NULL,1464316369,1),
	(2,'pidealer@pocketin.in','2','2be4a24eedc38c6d7f117e1276d8adbb9ef4321470abc33c08cb8382ea36c453','2','Dealer','Pocketin','none',1,'','','','',0,NULL,1,NULL,1464263402,0),
	(3,'vineethkumart@gmail.com','9538092344','e19ead0e6fd98035d3ee8082cda6f041565e0127e8058eacaec8178348b21512','9538092344','Vineeth','Kumar','none',0,'','','','',0,NULL,1,NULL,1464285420,0),
	(4,'deebanya@yahoo.com','9487625714','9fab8b949b54e8fbc4949c747ad73809a0ca64639095ab7d4a8bb6288dbaaa32','9487625714','Nivrhithi','Karthikeyan','none',0,'','','','',0,NULL,1,NULL,1463980955,0),
	(5,'duraigowardhan.s@gmail.com','9944951838','ca8cead16533dea6f21bc3b8f9d937ac855c1375834e859a15956317a647e6d2','9944951838','durai','gowardhan','none',0,'','','','',0,NULL,1,NULL,1464164015,0),
	(6,'soorajkrishnan1997@gmail.com','9400035112','7864c4b5583fd63388f1c08f99e0353b1abc94cf40ee9f81ef96076881896cd6','9400035112','Sooraj','Krishnan','none',0,'','','','',0,NULL,1,NULL,1464333158,0),
	(7,'ncnarenchoudary@gmail.com','9743000647','6bd32e14ee83bdd1474837dfe8db5a4e9e9aa15e16cb44e702f4514d8939019a','9743000647','Naren','C','none',0,'','','','',0,NULL,0,NULL,NULL,0),
	(8,'anjali.bansal123@gmail.com','9148973085','0e64b766da582144e3599e1a1746dc84e0342642ffeb355406625ba8b8a8732d','9148973085','Anjali','Bansal','none',0,'','','','',0,NULL,1,NULL,1464088456,0),
	(9,'azma.anjum@gmail.com','9545895588','3cb6dcd530f3c63a1e183b2a2df58b8a92a15305d20bb125aaa12f8754934871','9545895588','Azma','Anjum','none',0,'','','','',0,NULL,1,NULL,1463997627,0),
	(10,'a.sahai12@gmail.com','7838453699','22dc150817813930fe6dd2105a55f95d6ad4a39b2272c16638e13c9adc33b3d7','7838453699','Astha','Sahai','none',0,'','','','',0,NULL,1,NULL,1463999362,0),
	(11,'jintocyriac87@gmail.com','8747057450','b4b853cefc371faf379ddbfbf172c233d1361956f706fa6a28f67df7433974ed','8747057450','JINTO','CYRIAC','none',0,'','','','',0,NULL,1,NULL,1464029209,0),
	(12,'mohana.priya10@yahoo.com','9902001183','fb95e2ed0c0bb3734ea8d8e5cd658a3f903cbc875787d5320ddeb60be1a66abf','9902001183','Priya','Mohana','none',0,'','','','',0,NULL,1,NULL,1464260583,0),
	(13,'ankuraman.mishra@gmail.com','7795847342','d35d193c8cf6f183e5e7f342c04a33439a10fcda70b8ca894655f7a0139fa8b3','7795847342','ankur','mishra','none',0,'','','','',0,NULL,1,NULL,1464168700,0),
	(14,'airs2006@gmail.com','9538128057','65a806f0206d8d8127003981dfeb9d1cda0cb00aab5e9cec97d72713ca295877','9538128057','Arijit','Singha','none',0,'','','','',0,NULL,0,NULL,NULL,0),
	(15,'aradhanasngh5@gmail.com','8981542972','4d11e82d2ebefd1d8b0d4f06be65026d232f1b8973809b11977413e89e625b58','8981542972','Aradhana','Singh','none',0,'','','','',1,NULL,1,NULL,1464087218,0),
	(16,'Silliya.ravindran@gmail.com','8050493379','1d19d8548bc9a0fe1fb549d95b71e73399826aafc3e59cd8052dd7c2d6f704b5','8050493379','Silliya','Vk','none',0,'','','','',0,NULL,1,NULL,1464005390,0),
	(17,'prakharautocad2d@gmail.com','7359539830','245a936258b02f750ab6aa489d414bb0c3f64eabd5a45e012b83de9f85ff568e','7359539830','Prakhar','V','none',0,'','','','',0,NULL,1,NULL,1464005787,0),
	(18,'ruchi21091@gmail.com','9886382769','1af9c7fe5b6d4b16d32c0992ffd90a9e1076d4c4c1a81974a4f35a6b08e05ed7','9886382769','ruchismita','chakraborty','none',0,'','','','',0,NULL,1,NULL,1464019060,0),
	(19,'hariprasath.mohankumar@gmail.com','7259121152','287099edfe01aa99fc7704020db4968234a076139e55c1a710714e94da49ef24','7259121152','Hariprasath','M','none',0,'','','','',0,NULL,1,NULL,1464067292,0),
	(20,'himanshuchoudhary247@gmail.com','9166602365','114ad6b1ecbaf6445b629589185c48c8ece4df62f6412621c1cd74edc4ffaaeb','9166602365','Himanshu','Choudhary','none',0,'','','','',0,NULL,1,NULL,1464011312,0),
	(21,'26anoop1990@gmail.com','9686482400','0d59af8fe3f11ca9f5eedfc674c9070d67e772b89b7975ee9d9331dba025ec8e','9686482400','Anoop','K','none',0,'','','','',0,NULL,1,NULL,1464176398,0),
	(22,'anandiyer18@gmail.com','7022004119','eda45b7ee536e2a29724bfe52c4e7c89ab57926d2fb6cddcc4de9f276becd42e','7022004119','Anand','Iyer','none',0,'','','','',0,NULL,1,NULL,1464009374,0),
	(23,'madhushrichoudhary86@gmail.com','8793167506','1fc3773e0b44049796c223a3399c9de71a4e472c2060186c6d5cfad52ac56758','8793167506','Madhushri','Patel','none',0,'','','','',0,NULL,1,NULL,1464009507,0),
	(24,'pradeeps1988@gmail.com','9663866544','66a17fcda0c73298195528313bec4422281c1b1e0ff48704869e92265f16defc','9663866544','Pradeep','S','none',0,'','','','',0,NULL,1,NULL,1464009597,0),
	(25,'pritvirajsaha@gmail.com','9804837863','35dacf2be498cb428360bc8e9bebdc8fe61b5bcb65ecfe11e8cd7708735cd0a9','9804837863','Pritviraj','Saha','none',0,'','','','',0,NULL,1,NULL,1464009665,0),
	(26,'sujith10.cea@gmail.com','7406926385','5aeea58ebe4244ec0256eafec1a83c48942f652ef93016112d038ac885c41481','7406926385','sujith','kumar','none',0,'','','','',0,NULL,1,NULL,1464351487,0),
	(27,'reechabansal@yahoo.com','9742600477','a7173e98943488e5a87a3544c8f3a743c548ab82ed75269af12921209da247ac','9742600477','Reecha','Bansal','none',0,'','','','',0,NULL,1,NULL,1464067304,0),
	(28,'choudharysudhanshu0871999@gmail.com','9521866830','e9666d517927c6f46c465f3f576511e0f65a9c6732dec0a4ea1975f9b91717ad','9521866830','Sudhanshu','Choudhary','none',0,'','','','',0,NULL,1,NULL,1464014154,0),
	(29,'shubham.chaurasia.261996@gmail.com','9001201842','aba01f9b5b573c96713f02313cf250ae793bdb4db21c10506f3a5d47c3aea969','9001201842','Shubham','Chaurasia','none',0,'','','','',0,NULL,1,NULL,1464014501,0),
	(30,'jinupthomas@gmail.com','9349373847','cdcd11800ff9844e760403c4abe6acbd046598891f417fec9b3779f625de7231','9349373847','Jinu','PThomas','none',0,'','','','',0,NULL,1,NULL,1464014692,0),
	(31,'ganeshvarma1212@gmail.com','9633746577','10784dd594a6f564fcac98a5c9214da3aa9e186da7b4f395c3ab4a07d00894cf','9633746577','Ganesh','Varma','none',0,'','','','',0,NULL,1,NULL,1464015339,0),
	(32,'anusree.nidimbal94@gmail.com','8951338475','4ab6691d0cdf69648a2ddf5314041f1e0f37e3e3b330b068af5e4bc64318498b','8951338475','Anusree','NP','none',0,'','','','',0,NULL,1,NULL,1464077616,0),
	(33,'cshubham261996@gmail.com','8503072144','aba01f9b5b573c96713f02313cf250ae793bdb4db21c10506f3a5d47c3aea969','8503072144','Shubham','roy','none',0,'','','','',0,NULL,1,NULL,1464017569,0),
	(34,'15ucs136@lnmiit.ac.in','9694664630','aba01f9b5b573c96713f02313cf250ae793bdb4db21c10506f3a5d47c3aea969','9694664630','shubham','patel','none',0,'','','','',0,NULL,1,NULL,1464017697,0),
	(35,'sneha.varma2014@gmail.com','9028293398','4978d5a817932590384f561f8ccc89996ea70e053069c7eeca079e58354231f2','9028293398','Sneha','Varma','none',0,'','','','',0,NULL,1,NULL,1464019285,0),
	(36,'ffggh@gjh.com','9876543210','53988d4c7746fab0ad717414cc0968fab04cc51fb8918353f6950668fa8b16ec','9876543210','Gh','Ffg','none',0,'','','','',0,NULL,0,NULL,NULL,0),
	(37,'nandinichoudhary7@gmail.com','9899973771','719c1732cdfef98566a8db9073ca2b631afe8645306b263b9144015bd0325f4f','9899973771','Nandini','Choudhary','none',0,'','','','',0,NULL,1,NULL,1464021125,0),
	(38,'utkarsh12@guerillamail.com','4389935641','924a4ad92cd20535bbd6bad66d74ec99f053f4af5af79fb72b152983c94a8106','4389935641','Utkarsh','Choudhary','none',0,'','','','',0,NULL,0,NULL,NULL,0),
	(39,'himtorq@gmail.com','8146067550','5baa3addb8ff4f755a1561110ff461937fae4c2b7e47961aa2e1f01f4edf6aaf','8146067550','Himanshu','Gaurav','none',0,'','','','',0,NULL,1,NULL,1464026395,0),
	(40,'salilkolhe1@gmail.com','9589919002','ab29b7b8dd8af5bd4ea9790e05f9779796625ddeb45197bc81ace4e486974d3b','9589919002','salil','Kolhe','none',0,'','','','',0,NULL,0,NULL,NULL,0),
	(41,'knarad91p@gmail.com','9886121745','b2adbef0988e69cc0764ef60badca1335e6177326b29098e94abb06e96fc9535','9886121745','Narad','Kumar','none',0,'','','','',0,NULL,1,NULL,1464069434,0),
	(42,'haritgkumar@gmail.com','8290539343','20547c96ba97061ed7e8b5c9a64e7b4f366e5b548a58c16aaba04652fa792dd0','8290539343','Harit','Gulati','none',0,'','','','',0,NULL,1,NULL,1464068608,0),
	(43,'pushkaranand0312@gmail.com','9611500701','f569f1206a5b2e904e1bbf99c6ae930972c825124b2ed52eb3776b2816b7664e','9611500701','Pushkar','Anand','none',0,'','','','',0,NULL,1,NULL,1464070557,0),
	(44,'caravind07@gmail.com','8105877020','5288686952c02f3761229de4ef35948bd86fe22c2cbbc89a4beb49638b90f0a5','8105877020','Aravind','C','none',0,'','','','',0,NULL,1,NULL,1464070792,0),
	(45,'santhosh.rst@gmail.com','8088312676','56e163afadc754d3940c0850b0d7f1e5277792a9bed2ab75d08225d862edd74d','8088312676','Santhosh','R','none',0,'','','','',0,NULL,1,NULL,1464072162,0),
	(46,'sandappan@gmail.com','8904924969','9fc8bb5200e971d0cda70a9cb72f18a59e194f7d82500efb901ff3f8bc0c72a7','8904924969','Sandeep','Narayanan','none',0,'','','','',0,NULL,1,NULL,1464343170,0),
	(47,'mkisore@gmail.com','9944882189','3730829c32365fa64b8dce02c266d596c7c0ffb0e4e5d1c7052ffa4c5d209f7e','9944882189','Kisore','M','none',0,'','','','',0,NULL,1,NULL,1464245858,0),
	(48,'vips.wipro@gmail.com','8971777887','fab7aa9a1772d7de335fdc1c1070602e7f10534bee9654f0bdab002589ae977c','8971777887','Vipin','Mohan','none',0,'','','','',0,NULL,1,NULL,1464156736,0),
	(49,'uk1441@gmail.com','8951634790','c0bd8e44e89ccc529887dfa677901592ddef72093b4849b4f9799acbf884cef6','8951634790','UDAYA','KUMAR','none',0,'','','','',0,NULL,1,NULL,1464084288,0),
	(50,'frankcyril@gmail.com','8086515566','e03aa2440ced6b067bf952cbd589a525d32a3dcbf072a0e9f83139ce19c525ca','8086515566','Cyril','Frank','none',0,'','','','',0,NULL,1,NULL,1464085404,0),
	(51,'Sattanathan.45@hotmail.com','9739552827','0f7064269c273048c3e054f3372b03a431fb2fdbbc56c43c9c60068d80542945','9739552827','Satty','','none',0,'','','','',0,NULL,1,NULL,1464085563,0),
	(52,'prashanthsep11@gmail.com','9036199008','e4a2fb9db2aef517bac4cb36e47762f0b7d3cf2ce7c0d09c4a0670c9c032bc0f','9036199008','Prashanth','T','none',0,'','','','',0,NULL,1,NULL,1464088884,0),
	(53,'midhuna1994@gmail.com','9567078162','b1c575c31abeb04f9fd74e92ffbb7041412e4a27124ac8a49df48fd7f3eac667','9567078162','Midhuna','varma','none',0,'','','','',0,NULL,1,NULL,1464090687,0),
	(54,'avikm01@gmail.com','9836033457','9ee4a7fca90964f94c5a1841805af992718369bb5538da19145721d11efd8824','9836033457','Avik','mukherjee','none',0,'','','','',0,NULL,1,NULL,1464092730,0),
	(55,'subratasrk1940@gmail.com','7411063991','e8614a97aa91df540ea7eace778e63b534f5acbfc764d8c0fbddf032f802a379','7411063991','Subrata','Sarkar','none',0,'','','','',0,NULL,1,NULL,1464093770,0),
	(56,'malharhunge60@gmail.com','9767173675','25e290ffa75ff8f120df0ff90dfd8cda39b646f066fd894ceecd4f4b6b38c326','9767173675','Malhar','Hunge','none',0,'','','','',0,NULL,1,NULL,1464094719,0),
	(57,'masih.naveen@rediffmail.com','9516690708','e9ad5974c2638b120836c438bb1be7536bcab6bdd7e4f44bde6352bb0b9af4be','9516690708','naveen','masih','none',0,'','','','',0,NULL,1,NULL,1464100764,0),
	(58,'meenalochinicse@gmail.com','9865382937','08be225da368e4cc17675ca59d0e8fee46236b0c41860b506e3e359283d0b6d4','9865382937','Meena','','none',0,'','','','',0,NULL,1,NULL,1464106774,0),
	(59,'firoshshaji1987@gmail.com','8089791551','6a9497dea222032c9a5049a46055fbaee78faa32ef475dc30e16ab6aaad16f6b','8089791551','Firosh','Shaji','none',0,'','','','',0,NULL,1,NULL,1464111155,0),
	(60,'thesarangh@gmail.com','9496981479','9bae7805c60a7dac6270fec74b4ee4e0242e3ae8d92e79ec535d99d9da9d7153','9496981479','Sarangh','Somaraj','none',0,'','','','',0,NULL,1,NULL,1464185508,0),
	(61,'shanmugapriya1990@gmail.com','8553147503','4d00e494ebbd617e059061f0b9f97cdac9fca13b137ed622c80d0d2fbe195f67','8553147503','Shanmugapriya','V','none',0,'','','','',0,NULL,1,NULL,1464186271,0),
	(62,'peravelli8@gmail.com','9160722621','47a80a42f3c3a8932e4bdcdca1fc5ae126e2a13375d0b5882d4789789178e539','9160722621','purna','eravelli','none',0,'','','','',0,NULL,1,NULL,1464336741,0),
	(63,'kritishmani@gmail.com','7899837738','040c853fe9d5b4b2dd1ccc16d8e1d45328eba80112dd2cad8a158b1149ea0ee9','7899837738','Manikandan','Balasubramanian','none',0,'','','','',0,NULL,1,NULL,1464266811,0),
	(64,'narain.urs@gmail.com','9620106655','8146af5beaf0e2267e8889eabc71c6f367d6aa65d2fbe8ef62362d8907753d44','9620106655','LakshmiNarayanan','','none',0,'','','','',0,NULL,1,NULL,1464271264,0),
	(65,'mickysunish@gmail.com','7406054965','829df62f2eefacc4ea5792f6fd3c9940e82f2ee5ab0910146648f4d0131a22c4','7406054965','sunish','satya','none',0,'','','','',0,NULL,1,NULL,1464274309,0),
	(66,'gurucmy55@gmail.com','9986691733','53f3f0aab87d9ebe3419483e5497cd15c1c24e749ad1e3c1471b22da9f7681c6','9986691733','Guruprasad','Guru','none',0,'','','','',0,NULL,1,NULL,1464275055,0),
	(67,'jashjena11@gmail.com','9861643280','79da91928a884258fe3b72f7ae95ba7dfe14287b05f697f5058e6f42c414be8a','9861643280','Jasmin','Jena','none',0,'','','','',0,NULL,1,NULL,1464284733,0),
	(68,'santhosh.jnv@gmail.com','9844466068','cebcc8958e06cad7182684f9136031d326f876f8d2632816c46069951ef6febd','9844466068','Santosh','','none',0,'','','','',0,NULL,1,NULL,1464314485,0),
	(69,'ningaraju4723@gmail.com','9740009017','55d0093d8825d8ee60eb2d76ece71c1da16d075a3a411dc50001f71f4787c09b','9740009017','ningaraju','kb','none',0,'','','','',0,NULL,1,NULL,1464348078,0),
	(70,'sreerajt@yahoo.co.in','9167562835','8c62795afa3ddb9482f906ba94c5a0ea8ee44e0f034c21da49f88dbbbcd27b3d','9167562835','SREERAJ','T','none',0,'','','','',0,NULL,1,NULL,1464348537,0);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wishlists
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wishlists`;

CREATE TABLE `wishlists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  `productitem_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `wishlists` WRITE;
/*!40000 ALTER TABLE `wishlists` DISABLE KEYS */;

INSERT INTO `wishlists` (`id`, `timestamp`, `user_id`, `productitem_id`)
VALUES
	(1,'2016-05-23 04:18:39',3,14),
	(2,'2016-05-23 12:26:53',18,20),
	(3,'2016-05-23 12:37:59',18,23),
	(5,'2016-05-26 07:19:57',47,21),
	(6,'2016-05-26 15:09:17',66,33);

/*!40000 ALTER TABLE `wishlists` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
