# ************************************************************
# Sequel Pro SQL dump
# Version 4529
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.0.17-MariaDB)
# Database: pocketin-db
# Generation Time: 2016-04-04 04:34:33 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table activitylogs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `activitylogs`;

CREATE TABLE `activitylogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `object_id` int(11) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_activitylog_users1_idx` (`user_id`),
  CONSTRAINT `fk_activitylog_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `activitylogs` WRITE;
/*!40000 ALTER TABLE `activitylogs` DISABLE KEYS */;

INSERT INTO `activitylogs` (`id`, `object_id`, `type`, `timestamp`, `user_id`)
VALUES
	(1,8,'APPROVED_PRODUCT','2016-04-03 12:12:40',23);

/*!40000 ALTER TABLE `activitylogs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table addresses
# ------------------------------------------------------------

DROP TABLE IF EXISTS `addresses`;

CREATE TABLE `addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `address_line_one` varchar(512) NOT NULL,
  `address_line_two` varchar(512) NOT NULL,
  `city` varchar(256) NOT NULL,
  `state` varchar(256) NOT NULL,
  `country` varchar(256) NOT NULL,
  `pincode` varchar(256) NOT NULL,
  `isdefault` int(11) NOT NULL DEFAULT '0',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table carts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `carts`;

CREATE TABLE `carts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `session_id` varchar(256) DEFAULT NULL,
  `ispurchased` int(11) DEFAULT '0',
  `timestamp` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table comments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `comments`;

CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comment` varchar(256) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `ticket_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_comments_users1_idx` (`user_id`),
  KEY `fk_comments_tickets1_idx` (`ticket_id`),
  CONSTRAINT `fk_comments_tickets1` FOREIGN KEY (`ticket_id`) REFERENCES `tickets` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_comments_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table filters
# ------------------------------------------------------------

DROP TABLE IF EXISTS `filters`;

CREATE TABLE `filters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filtertype_id` int(11) NOT NULL,
  `name` varchar(512) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `filters` WRITE;
/*!40000 ALTER TABLE `filters` DISABLE KEYS */;

INSERT INTO `filters` (`id`, `filtertype_id`, `name`, `timestamp`)
VALUES
	(1,1,'Double Door','2016-03-03 21:19:13'),
	(2,1,'Single Door','2016-03-03 21:19:19'),
	(3,2,'below Rs.5000','2016-03-26 15:36:23'),
	(4,2,'Rs.5000 to 10000','2016-03-26 15:36:30'),
	(5,3,'below 200 lts','2016-03-26 15:35:44'),
	(6,3,'above 200 lts','2016-03-26 15:35:55'),
	(7,4,'Red','2016-03-26 14:29:11'),
	(8,4,'Blue','2016-03-26 14:29:13'),
	(9,4,'white','2016-03-26 14:29:15'),
	(10,4,'other colours','2016-03-26 14:29:22'),
	(11,5,'less than 3 star','2016-03-26 14:38:39'),
	(12,5,'3 star and above','2016-03-26 14:54:34'),
	(13,6,'Fully Automatic','2016-03-26 15:29:46'),
	(14,6,'Semi Automatic','2016-03-26 15:29:57'),
	(15,7,'Front Load','2016-03-26 15:30:13'),
	(16,7,'Top Load','2016-03-26 15:30:21'),
	(17,8,'6Kg and below','2016-03-26 15:34:25'),
	(18,8,'Above 6Kg','2016-03-26 15:34:22'),
	(19,9,'Split','2016-03-26 16:09:17'),
	(20,9,'Window','2016-03-26 16:09:27'),
	(21,10,'Less than 2 tonnes','2016-03-26 16:11:00'),
	(22,10,'2 tonnes & above','2016-03-26 16:11:11'),
	(23,11,'Less than 3 star','2016-03-26 16:11:27'),
	(24,11,'3 star & above','2016-03-26 16:11:38'),
	(25,12,'Electrical','2016-03-26 16:12:30'),
	(26,12,'Non-Electrical','2016-03-26 16:12:50'),
	(27,13,'Gravity based','2016-03-26 16:14:08'),
	(28,13,'RO+UV based','2016-03-26 16:19:22'),
	(29,14,'7Lts and below','2016-03-26 16:19:58'),
	(30,14,'Above 7Lts','2016-03-26 16:20:17'),
	(31,15,'3 Seater & below ','2016-03-26 16:25:10'),
	(32,15,'Above 3 Seater','2016-03-26 16:25:28'),
	(33,16,'Fabric','2016-03-26 16:25:44'),
	(34,16,'Leather','2016-03-26 16:25:53'),
	(35,16,'Wood','2016-03-26 16:26:22'),
	(36,16,'Metal','2016-03-26 16:26:34'),
	(37,17,'Straight','2016-03-26 16:26:58'),
	(38,17,'L & U Shaped','2016-03-26 16:27:13'),
	(39,18,'Beige','2016-03-26 16:27:56'),
	(40,18,'Black','2016-03-26 16:28:02'),
	(41,18,'Other Colors','2016-03-26 16:28:18'),
	(42,19,'Plastic','2016-03-26 16:31:46'),
	(43,19,'Fabric','2016-03-26 16:31:52'),
	(44,19,'Leatherette','2016-03-26 16:32:01'),
	(45,20,'Black','2016-03-26 16:33:37'),
	(46,20,'Beige','2016-03-26 16:33:53'),
	(47,20,'Other colors','2016-03-26 16:33:59'),
	(48,21,'Without Wheels ','2016-03-26 16:35:36'),
	(49,21,'With Seat Lock & Height Adjustment','2016-03-26 16:35:12'),
	(50,22,'Yes','2016-03-26 16:40:15'),
	(51,22,'No','2016-03-26 16:40:20'),
	(52,23,'Yes','2016-03-26 16:40:27'),
	(53,23,'No','2016-03-26 16:40:33');

/*!40000 ALTER TABLE `filters` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table filtertypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `filtertypes`;

CREATE TABLE `filtertypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `productcategory_id` int(11) NOT NULL,
  `name` varchar(512) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `filtertypes` WRITE;
/*!40000 ALTER TABLE `filtertypes` DISABLE KEYS */;

INSERT INTO `filtertypes` (`id`, `productcategory_id`, `name`, `timestamp`)
VALUES
	(1,3,'Door Type','2016-03-03 20:48:31'),
	(2,3,'Price','2016-03-26 14:23:04'),
	(3,3,'Capacity','2016-03-26 14:23:07'),
	(4,3,'Color','2016-03-26 14:23:09'),
	(5,3,'Energy Rating','2016-03-26 14:40:20'),
	(6,4,'Function Type','2016-03-26 14:50:14'),
	(7,4,'Load Type','2016-03-26 14:50:33'),
	(8,4,'Capacity','2016-03-26 15:26:46'),
	(9,5,'Type','2016-03-26 15:37:14'),
	(10,5,'Capacity','2016-03-26 15:37:27'),
	(11,5,'Energy Efficency','2016-03-26 15:37:41'),
	(12,6,'Type','2016-03-26 15:38:24'),
	(13,6,'Purification Technology','2016-03-26 15:38:34'),
	(14,6,'Capacity','2016-03-26 15:38:52'),
	(15,7,'Seating Capacity','2016-03-26 16:22:02'),
	(16,7,'Material','2016-03-26 16:22:30'),
	(17,7,'Shape','2016-03-26 16:22:40'),
	(18,7,'Color','2016-03-26 16:22:48'),
	(19,8,'Material','2016-03-26 16:28:59'),
	(20,8,'Color','2016-03-26 16:29:05'),
	(21,8,'Type','2016-03-26 16:30:11'),
	(22,9,'Wheels Included','2016-03-26 16:39:28'),
	(23,9,'Storage Included','2016-03-26 16:40:53');

/*!40000 ALTER TABLE `filtertypes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table images
# ------------------------------------------------------------

DROP TABLE IF EXISTS `images`;

CREATE TABLE `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `description` text,
  `thumbnail` varchar(512) DEFAULT '',
  `isdefault` int(11) DEFAULT '0',
  `productitem_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_images_products_vendors1_idx` (`productitem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `images` WRITE;
/*!40000 ALTER TABLE `images` DISABLE KEYS */;

INSERT INTO `images` (`id`, `name`, `description`, `thumbnail`, `isdefault`, `productitem_id`)
VALUES
	(32,'11_1.jpg','Image 1','',1,11),
	(33,'11_2.jpg','Image 2','',0,11),
	(34,'11_3.jpg','Image 3','',0,11);

/*!40000 ALTER TABLE `images` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table notificationobjectchanges
# ------------------------------------------------------------

DROP TABLE IF EXISTS `notificationobjectchanges`;

CREATE TABLE `notificationobjectchanges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `verb` varchar(45) DEFAULT NULL,
  `actor` varchar(45) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT NULL,
  `notificationobject_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_notificationobjectchanges_notificationobjects1_idx` (`notificationobject_id`),
  CONSTRAINT `fk_notificationobjectchanges_notificationobjects1` FOREIGN KEY (`notificationobject_id`) REFERENCES `notificationobjects` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table notificationobjects
# ------------------------------------------------------------

DROP TABLE IF EXISTS `notificationobjects`;

CREATE TABLE `notificationobjects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `object_id` int(11) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT NULL,
  `notification_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_notificationobjects_notifications1_idx` (`notification_id`),
  CONSTRAINT `fk_notificationobjects_notifications1` FOREIGN KEY (`notification_id`) REFERENCES `notifications` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table notifications
# ------------------------------------------------------------

DROP TABLE IF EXISTS `notifications`;

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(45) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_notifications_users1_idx` (`user_id`),
  CONSTRAINT `fk_notifications_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table onetimepasswords
# ------------------------------------------------------------

DROP TABLE IF EXISTS `onetimepasswords`;

CREATE TABLE `onetimepasswords` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phonenumber` varchar(256) NOT NULL DEFAULT '',
  `otp` int(4) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table orderdetails
# ------------------------------------------------------------

DROP TABLE IF EXISTS `orderdetails`;

CREATE TABLE `orderdetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` int(11) DEFAULT NULL,
  `name` varchar(256) DEFAULT NULL,
  `email` varchar(256) DEFAULT NULL,
  `phonenumber` varchar(256) DEFAULT NULL,
  `ispaid` int(11) DEFAULT '0',
  `address_id` int(11) NOT NULL,
  `timestamp` timestamp NULL DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `cart_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_orderdetails_users1_idx` (`user_id`),
  KEY `fk_orderdetails_carts1_idx` (`cart_id`),
  CONSTRAINT `fk_orderdetails_carts1` FOREIGN KEY (`cart_id`) REFERENCES `carts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_orderdetails_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table orders
# ------------------------------------------------------------

DROP TABLE IF EXISTS `orders`;

CREATE TABLE `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `price` int(11) DEFAULT '0',
  `isshipped` int(11) DEFAULT '0',
  `shippeddate` datetime DEFAULT NULL,
  `isdelivered` int(11) DEFAULT '0',
  `deliverydate` datetime DEFAULT NULL,
  `ispaid` int(11) DEFAULT '0',
  `deliverycost` int(11) DEFAULT '0',
  `timestamp` varchar(45) DEFAULT NULL,
  `productitems_id` int(11) NOT NULL,
  `orderdetail_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_orders_products_vendors1_idx` (`productitems_id`),
  KEY `fk_orders_orderdetails1_idx` (`orderdetail_id`),
  CONSTRAINT `fk_orders_orderdetails1` FOREIGN KEY (`orderdetail_id`) REFERENCES `orderdetails` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table parentcategories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `parentcategories`;

CREATE TABLE `parentcategories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(512) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(512) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `parentcategories` WRITE;
/*!40000 ALTER TABLE `parentcategories` DISABLE KEYS */;

INSERT INTO `parentcategories` (`id`, `name`, `description`, `image`)
VALUES
	(1,'Home Appliances','Home Appliances',''),
	(2,'Furniture','Furniture',''),
	(3,'Cat','','');

/*!40000 ALTER TABLE `parentcategories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table productcategories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `productcategories`;

CREATE TABLE `productcategories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `description` text NOT NULL,
  `image` varchar(512) NOT NULL,
  `parentcategory_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `productcategories` WRITE;
/*!40000 ALTER TABLE `productcategories` DISABLE KEYS */;

INSERT INTO `productcategories` (`id`, `name`, `description`, `image`, `parentcategory_id`)
VALUES
	(3,'Refrigerators','Top Quality Refrigerators','fridge.jpg',1),
	(4,'Washing Machines','Top Quality Washing Machines','washingmachine.jpg',1),
	(5,'Air Conditioners','Top quality Air Conditioners','airconditioner.jpg',1),
	(6,'Water Purifiers','Top Quality Water Purifiers','waterpurifier.jpg',1),
	(7,'Sofa set','Sofa set at cheap price','sofaset.jpg',2),
	(8,'Office Chair','Best Quality Office Chairs','officechair.jpg',2),
	(9,'Computer Table','Compuer Tables at best price','computertable.jpg',2);

/*!40000 ALTER TABLE `productcategories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table productfilters
# ------------------------------------------------------------

DROP TABLE IF EXISTS `productfilters`;

CREATE TABLE `productfilters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filter_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table productitemfilters
# ------------------------------------------------------------

DROP TABLE IF EXISTS `productitemfilters`;

CREATE TABLE `productitemfilters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filter_id` int(11) NOT NULL,
  `productitem_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table productitems
# ------------------------------------------------------------

DROP TABLE IF EXISTS `productitems`;

CREATE TABLE `productitems` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qualityindex` int(11) DEFAULT '0',
  `title` varchar(512) NOT NULL,
  `description` varchar(256) DEFAULT NULL,
  `issold` int(11) DEFAULT '0',
  `isapproved` int(11) DEFAULT '0',
  `saleprice` int(11) DEFAULT '0',
  `manufacturingyear` int(11) NOT NULL,
  `warranty` varchar(256) NOT NULL,
  `vendorprice` int(11) DEFAULT '0',
  `ispaid` int(11) DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `isdelivered` int(11) DEFAULT '0',
  `productcategory_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_products_vendors_users1_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `productitems` WRITE;
/*!40000 ALTER TABLE `productitems` DISABLE KEYS */;

INSERT INTO `productitems` (`id`, `qualityindex`, `title`, `description`, `issold`, `isapproved`, `saleprice`, `manufacturingyear`, `warranty`, `vendorprice`, `ispaid`, `user_id`, `product_id`, `timestamp`, `isdelivered`, `productcategory_id`)
VALUES
	(11,0,'LG single door',NULL,0,0,6000,2011,'',0,0,22,NULL,'2016-04-03 12:36:36',0,NULL);

/*!40000 ALTER TABLE `productitems` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table productrequests
# ------------------------------------------------------------

DROP TABLE IF EXISTS `productrequests`;

CREATE TABLE `productrequests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phonenumber` varchar(256) DEFAULT NULL,
  `productname` varchar(256) DEFAULT NULL,
  `model` varchar(45) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_productrequests_users1_idx` (`user_id`),
  CONSTRAINT `fk_productrequests_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table products
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `modelnumber` varchar(512) NOT NULL,
  `description` text,
  `shortdescription` varchar(512) NOT NULL,
  `instock` int(11) DEFAULT '0',
  `stockcount` int(11) DEFAULT '0',
  `origimage` varchar(256) DEFAULT NULL,
  `mrp` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table qualityindexes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `qualityindexes`;

CREATE TABLE `qualityindexes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(512) NOT NULL,
  `productcategory_id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `qualityindexes` WRITE;
/*!40000 ALTER TABLE `qualityindexes` DISABLE KEYS */;

INSERT INTO `qualityindexes` (`id`, `name`, `productcategory_id`, `timestamp`)
VALUES
	(1,'Age',3,'2016-03-12 10:56:38'),
	(2,'Door Seal',3,'2016-03-12 11:26:00'),
	(3,'Energy Efficiency',3,'2016-03-12 10:56:51'),
	(4,'Appearance',3,'2016-03-12 10:57:22'),
	(5,'Motor Condition',3,'2016-03-12 11:02:03'),
	(6,'Age',4,'2016-03-12 11:00:14'),
	(7,'Full Cycle Functionality',4,'2016-03-12 11:00:53'),
	(8,'Drum Condition',4,'2016-03-12 11:00:54'),
	(9,'Efficiency Grading',4,'2016-03-12 11:00:55'),
	(10,'Appearance',4,'2016-03-12 11:00:56'),
	(12,'Age',5,'2016-03-12 11:01:12'),
	(13,'Cooling Capacity',5,'2016-03-12 11:01:21'),
	(14,'Energy Efficiency',5,'2016-03-12 11:02:48'),
	(15,'Appearance',5,'2016-03-12 11:02:50'),
	(16,'Internal Parts',5,'2016-03-12 11:02:51'),
	(18,'Other',5,'2016-03-12 11:02:58'),
	(19,'Age',6,'2016-03-12 11:03:46'),
	(20,'Filter Codition',6,'2016-03-12 11:03:46'),
	(21,'Appearance',6,'2016-03-12 11:03:46'),
	(22,'Interior Parts',6,'2016-03-12 11:03:47'),
	(23,'Other',6,'2016-03-12 11:03:49'),
	(24,'Age',7,'2016-03-12 11:04:45'),
	(25,'Cushion Condition',7,'2016-03-12 11:04:46'),
	(26,'Frame Condition',7,'2016-03-12 11:13:10'),
	(27,'Spring Condition',7,'2016-03-12 11:13:12'),
	(28,'Appearance',7,'2016-03-12 11:13:36'),
	(29,'Other',7,'2016-03-12 11:13:40');

/*!40000 ALTER TABLE `qualityindexes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table qualityindexvalues
# ------------------------------------------------------------

DROP TABLE IF EXISTS `qualityindexvalues`;

CREATE TABLE `qualityindexvalues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qualityindexes_id` int(11) NOT NULL,
  `value_text` varchar(512) NOT NULL DEFAULT '',
  `value` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `qualityindexvalues` WRITE;
/*!40000 ALTER TABLE `qualityindexvalues` DISABLE KEYS */;

INSERT INTO `qualityindexvalues` (`id`, `qualityindexes_id`, `value_text`, `value`)
VALUES
	(1,1,'>= 2014',5),
	(2,1,'2012 - 2014',4),
	(3,1,'2010 - 2012',3),
	(4,1,'2005 - 2010',2),
	(5,1,'< 2005',1),
	(6,2,'New Seal Installed',5),
	(7,2,'Good Working Seal',4),
	(8,2,'Average Seal',3),
	(9,2,'Bad Seal',2),
	(10,2,'Seal Damaged',1),
	(11,3,'Five Star',5),
	(12,3,'Four Star',4),
	(13,3,'Three Star',3),
	(14,3,'Two Star',2),
	(15,3,'< Two Star',1),
	(16,4,'Like New W/o Any Major Scratches or Dents',5),
	(17,4,'Good With Some Scratches and no Dents',4),
	(18,4,'Average With Some Dents and Scratches',3),
	(19,4,'Some Parts are Broken',2),
	(20,4,'Looks Bad',1),
	(21,5,'New Motor',5),
	(22,5,'Excellent Working Condition',4),
	(23,5,'Good Working Condition',3),
	(24,5,'Average',2),
	(25,5,'Bad',1),
	(26,6,'>= 2014',5),
	(27,6,'2012 - 2014',4),
	(28,6,'2005 - 2010',2),
	(30,6,'2010 - 2012',3),
	(31,6,'< 2005',1),
	(32,10,'Like New W/o Any Major Scratches or Dents',5),
	(33,10,'Good With Some Scratches and no Dents',4),
	(34,10,'Average With Some Dents and Scratches',3),
	(35,10,'Some Parts are Broken',2),
	(36,10,'Looks Bad',1);

/*!40000 ALTER TABLE `qualityindexvalues` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table qualityratings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `qualityratings`;

CREATE TABLE `qualityratings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` int(11) NOT NULL,
  `qualityindex_id` int(11) NOT NULL,
  `productitem_id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `description` varchar(256) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `qualityratings` WRITE;
/*!40000 ALTER TABLE `qualityratings` DISABLE KEYS */;

INSERT INTO `qualityratings` (`id`, `value`, `qualityindex_id`, `productitem_id`, `timestamp`, `description`)
VALUES
	(61,3,1,11,'2016-04-03 12:36:36',''),
	(62,4,2,11,'2016-04-03 12:36:36',''),
	(63,4,3,11,'2016-04-03 12:36:36',''),
	(64,4,4,11,'2016-04-03 12:36:36',''),
	(65,4,5,11,'2016-04-03 12:36:36','');

/*!40000 ALTER TABLE `qualityratings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;

INSERT INTO `roles` (`id`, `name`, `description`)
VALUES
	(1,'login','Login privileges, granted after account confirmation'),
	(2,'admin','Administrative user, has access to everything.'),
	(3,'dealer','this is a delaer');

/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table roles_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `roles_users`;

CREATE TABLE `roles_users` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `fk_role_id` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `roles_users` WRITE;
/*!40000 ALTER TABLE `roles_users` DISABLE KEYS */;

INSERT INTO `roles_users` (`user_id`, `role_id`)
VALUES
	(1,1),
	(2,1),
	(4,1),
	(5,1),
	(6,1),
	(7,1),
	(8,1),
	(9,1),
	(10,1),
	(11,1),
	(12,1),
	(13,1),
	(14,1),
	(15,1),
	(16,1),
	(17,1),
	(18,1),
	(19,1),
	(20,1),
	(21,1),
	(22,1),
	(23,1),
	(24,1),
	(25,1),
	(26,1),
	(27,1),
	(28,1),
	(29,1),
	(30,1),
	(31,1),
	(32,1),
	(33,1),
	(34,1),
	(35,1),
	(36,1),
	(37,1),
	(38,1),
	(39,1),
	(40,1),
	(41,1),
	(42,1),
	(43,1),
	(44,1),
	(45,1),
	(46,1),
	(47,1),
	(48,1),
	(49,1),
	(50,1),
	(51,1),
	(52,1),
	(53,1),
	(54,1);

/*!40000 ALTER TABLE `roles_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table technicalspecs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `technicalspecs`;

CREATE TABLE `technicalspecs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(512) NOT NULL,
  `productcategory_id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `technicalspecs` WRITE;
/*!40000 ALTER TABLE `technicalspecs` DISABLE KEYS */;

INSERT INTO `technicalspecs` (`id`, `name`, `productcategory_id`, `timestamp`)
VALUES
	(1,'capacity',3,'2016-03-03 22:36:29');

/*!40000 ALTER TABLE `technicalspecs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table technicalspecvalues
# ------------------------------------------------------------

DROP TABLE IF EXISTS `technicalspecvalues`;

CREATE TABLE `technicalspecvalues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(256) NOT NULL,
  `technicalspec_id` int(11) NOT NULL,
  `productitem_id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `technicalspecvalues` WRITE;
/*!40000 ALTER TABLE `technicalspecvalues` DISABLE KEYS */;

INSERT INTO `technicalspecvalues` (`id`, `value`, `technicalspec_id`, `productitem_id`, `timestamp`)
VALUES
	(1,'200c',1,6,'2016-03-03 23:09:15');

/*!40000 ALTER TABLE `technicalspecvalues` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tickets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tickets`;

CREATE TABLE `tickets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` varchar(256) DEFAULT NULL,
  `subject` varchar(256) DEFAULT NULL,
  `isactive` int(11) DEFAULT '0',
  `date` datetime DEFAULT NULL,
  `tickettype_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tickets_tickettypes1_idx` (`tickettype_id`),
  KEY `fk_tickets_users1_idx` (`user_id`),
  CONSTRAINT `fk_tickets_tickettypes1` FOREIGN KEY (`tickettype_id`) REFERENCES `tickettypes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tickets_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tickettypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tickettypes`;

CREATE TABLE `tickettypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table user_tokens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_tokens`;

CREATE TABLE `user_tokens` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `user_agent` varchar(40) NOT NULL,
  `token` varchar(40) NOT NULL,
  `created` int(10) unsigned NOT NULL,
  `expires` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_token` (`token`),
  KEY `fk_user_id` (`user_id`),
  KEY `expires` (`expires`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `user_tokens` WRITE;
/*!40000 ALTER TABLE `user_tokens` DISABLE KEYS */;

INSERT INTO `user_tokens` (`id`, `user_id`, `user_agent`, `token`, `created`, `expires`)
VALUES
	(1,22,'03a5df1374233e36166ce0663ac68d14e94d78a2','6e161d2756a4e328e763f8841ff920e4c848473b',1458018088,1463202088),
	(2,23,'03a5df1374233e36166ce0663ac68d14e94d78a2','157d6c60b72e97c91d4807f25addee974f07a525',1458018093,1463202093),
	(3,22,'838008a7bdc283a25fd7ce96eccf070209e9d988','21ceda53537208ff555d1cbb8c066e57ab7774d0',1458144199,1463328199),
	(4,23,'838008a7bdc283a25fd7ce96eccf070209e9d988','cb7af9823d9044cf02f9009904f0678e3ce66df1',1458144256,1463328256),
	(5,24,'838008a7bdc283a25fd7ce96eccf070209e9d988','a00d56f90a3c39491d3da26ddda04a135c16ca6b',1458146145,1463330145),
	(6,29,'838008a7bdc283a25fd7ce96eccf070209e9d988','0910ff3fa5e7238ee8ba7979d1f91845386a7ebb',1458211202,1463395202),
	(7,32,'838008a7bdc283a25fd7ce96eccf070209e9d988','6246032c2d8040086352fb892f62330e485b0d3b',1458212437,1463396437),
	(8,33,'838008a7bdc283a25fd7ce96eccf070209e9d988','44d1898da87513a11b53dd7f495edb2db87d8f91',1458212494,1463396494),
	(9,51,'838008a7bdc283a25fd7ce96eccf070209e9d988','37837dc257c91aeffe49d07e7e236886a6f0414e',1458228854,1463412854),
	(10,52,'838008a7bdc283a25fd7ce96eccf070209e9d988','a3449036246330aee717e7dd1ac9c5bcd86cc542',1458228954,1463412954),
	(11,22,'838008a7bdc283a25fd7ce96eccf070209e9d988','9f4c4eb7daadaa1bc34cb4965f8d10dfc53dcd78',1458230434,1463414434),
	(12,22,'838008a7bdc283a25fd7ce96eccf070209e9d988','9cd80a84f5bfb397fc06b06d7d2869b51d797953',1458230450,1463414450),
	(13,22,'7a987703ba77c2fb8fb3ba87c6bd17b8e042a037','a78da2619b35869649804c6ac542ed5669d453e7',1458230464,1463414464),
	(14,23,'838008a7bdc283a25fd7ce96eccf070209e9d988','fb971eeb6838c423be050402b242784bfd2bb2db',1458230607,1463414607),
	(15,23,'838008a7bdc283a25fd7ce96eccf070209e9d988','49b649858112fcf23d8fef6361bfa7c1f72c61a9',1458230626,1463414626),
	(16,51,'838008a7bdc283a25fd7ce96eccf070209e9d988','c541f9df9258f3d263d7c91b5cd2460dc96e4640',1459266394,1464450394),
	(17,51,'e30494ebc899bb80dc81a15fbab8d3c02c15923e','d169340d9bcf20660d695f90d3f201f9eaf2012c',1459657992,1464841992),
	(18,51,'e30494ebc899bb80dc81a15fbab8d3c02c15923e','a088187174f5e4f0a6277da12767667669c91ef3',1459658015,1464842015),
	(19,22,'e30494ebc899bb80dc81a15fbab8d3c02c15923e','302c5eb721dd8b7df273e9fd795d98d4f4733d50',1459658162,1464842162),
	(20,22,'e30494ebc899bb80dc81a15fbab8d3c02c15923e','229f17b20024ea1c8bf537acde17f073e9cbb550',1459658170,1464842170),
	(21,22,'08e99c59ab2a19485d8d06669e77a4818d98ad91','801ad7efdd97d41f87523eccddbc1a14a8977bf8',1459659834,1464843834),
	(22,23,'e30494ebc899bb80dc81a15fbab8d3c02c15923e','67e824df79ae7c5f3e7cab5ab322b630e2a848d0',1459665113,1464849113);

/*!40000 ALTER TABLE `user_tokens` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(256) DEFAULT NULL,
  `username` varchar(256) DEFAULT NULL,
  `password` varchar(256) DEFAULT NULL,
  `phonenumber` varchar(256) DEFAULT NULL,
  `firstname` varchar(512) NOT NULL,
  `lastname` varchar(512) NOT NULL,
  `picture` varchar(256) NOT NULL,
  `isdealer` int(11) NOT NULL DEFAULT '0',
  `location` varchar(512) NOT NULL,
  `locationlat` varchar(512) NOT NULL,
  `locationlng` varchar(512) NOT NULL,
  `place_id` varchar(512) NOT NULL,
  `deliveryradius` int(11) NOT NULL,
  `hash` varchar(256) DEFAULT NULL,
  `isverified` int(11) DEFAULT '0',
  `logins` int(11) DEFAULT NULL,
  `last_login` int(11) DEFAULT NULL,
  `isadmin` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `email`, `username`, `password`, `phonenumber`, `firstname`, `lastname`, `picture`, `isdealer`, `location`, `locationlat`, `locationlng`, `place_id`, `deliveryradius`, `hash`, `isverified`, `logins`, `last_login`, `isadmin`)
VALUES
	(22,'dealer@dealer.com','123','f6667c9d18d10065e9bcebcd8761c9affe7a78bf49b13544022b87aca4b47a17','123','Dealer_1','','none',1,'','','','',0,NULL,1,NULL,1459659834,0),
	(23,'admin@admin.com','1234','290988abfd30b4b800ddbc09b953da98e8361a1a775e385111795185c60f7b87','1234','Admin','Admin','none',0,'','','','',0,NULL,1,NULL,1459665113,1),
	(51,'vineethkumart@gmail.com','9538092344','d3ecc5ed9bc837e2a94732ab9fd7787fe5845128001ffe82e6f1c238198bad63','9538092344','Vineeth','Kumar','none',0,'','','','',0,NULL,1,NULL,1459658015,0),
	(52,'durai@gmail.com','9944951838','0014cd8ac068b3b7ef890453cdfd69cd04b378594e435400534a59f799bed0bf','9944951838','Durai','Gowardhan','none',0,'','','','',0,NULL,1,NULL,1458228954,0),
	(53,'varma@gmail.com','12345','2b0574df8dd7cec5712fce70568744126af8e06089036f0181bfcedf50f76a8c','12345','Varma','V','none',0,'','','','',0,NULL,0,NULL,NULL,0),
	(54,'sow@gmail.com','9741039233','b677038ac39ab1e2762a0756e169a6b5a6e354f6850d655fbfa7f9cdce206382','9741039233','Sowbarani','S','none',0,'','','','',0,NULL,0,NULL,NULL,0);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wishlists
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wishlists`;

CREATE TABLE `wishlists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  `productitem_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
