# ************************************************************
# Sequel Pro SQL dump
# Version 4529
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.0.17-MariaDB)
# Database: pocketin-db
# Generation Time: 2016-05-07 03:43:21 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table activitylogs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `activitylogs`;

CREATE TABLE `activitylogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `object_id` int(11) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_activitylog_users1_idx` (`user_id`),
  CONSTRAINT `fk_activitylog_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `activitylogs` WRITE;
/*!40000 ALTER TABLE `activitylogs` DISABLE KEYS */;

INSERT INTO `activitylogs` (`id`, `object_id`, `type`, `timestamp`, `user_id`)
VALUES
	(1,8,'APPROVED_PRODUCT','2016-04-03 12:12:40',23),
	(2,11,'APPROVED_PRODUCT','2016-04-04 11:08:01',23),
	(3,12,'APPROVED_PRODUCT','2016-04-05 19:42:09',23),
	(4,13,'APPROVED_PRODUCT','2016-04-11 16:25:16',23),
	(5,14,'APPROVED_PRODUCT','2016-04-19 08:55:47',23),
	(6,15,'APPROVED_PRODUCT','2016-04-19 11:53:48',23),
	(7,16,'APPROVED_PRODUCT','2016-04-20 01:08:58',23),
	(8,17,'APPROVED_PRODUCT','2016-04-27 13:03:22',23),
	(9,18,'APPROVED_PRODUCT','2016-04-27 13:06:44',23),
	(10,19,'APPROVED_PRODUCT','2016-04-27 13:07:02',23),
	(11,19,'APPROVED_PRODUCT','2016-04-27 13:07:04',23),
	(12,20,'APPROVED_PRODUCT','2016-04-27 13:07:08',23),
	(13,21,'APPROVED_PRODUCT','2016-04-27 13:07:11',23),
	(14,22,'APPROVED_PRODUCT','2016-04-27 13:07:14',23),
	(15,23,'APPROVED_PRODUCT','2016-04-27 13:07:17',23),
	(16,24,'APPROVED_PRODUCT','2016-04-27 13:07:22',23),
	(17,25,'APPROVED_PRODUCT','2016-04-27 13:07:26',23),
	(18,26,'APPROVED_PRODUCT','2016-04-27 13:07:30',23),
	(19,27,'APPROVED_PRODUCT','2016-04-27 13:07:33',23),
	(20,28,'APPROVED_PRODUCT','2016-04-27 13:07:36',23),
	(21,29,'APPROVED_PRODUCT','2016-04-27 13:07:40',23),
	(22,30,'APPROVED_PRODUCT','2016-04-27 13:07:43',23),
	(23,31,'APPROVED_PRODUCT','2016-04-27 13:07:46',23);

/*!40000 ALTER TABLE `activitylogs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table addresses
# ------------------------------------------------------------

DROP TABLE IF EXISTS `addresses`;

CREATE TABLE `addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `address_line_one` varchar(512) NOT NULL,
  `address_line_two` varchar(512) NOT NULL,
  `city` varchar(256) NOT NULL,
  `state` varchar(256) NOT NULL,
  `country` varchar(256) NOT NULL,
  `pincode` varchar(256) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `landmark` varchar(256) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `name` varchar(100) NOT NULL DEFAULT '',
  `phonenumber` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `addresses` WRITE;
/*!40000 ALTER TABLE `addresses` DISABLE KEYS */;

INSERT INTO `addresses` (`id`, `user_id`, `address_line_one`, `address_line_two`, `city`, `state`, `country`, `pincode`, `timestamp`, `landmark`, `email`, `name`, `phonenumber`)
VALUES
	(22,51,'Sopanam','Kodakkat','Bangalore','Karnataka','India','678234','2016-05-02 22:00:00','sopanam','vineethkumart@gmail.com','Vineeth Kumar','9538092344'),
	(23,51,'sdf','','Bangalore','Karnataka','India','111111','2016-05-07 09:10:43','sdf','sdf@gmail.com','sdf','1111111111');

/*!40000 ALTER TABLE `addresses` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cartitems
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cartitems`;

CREATE TABLE `cartitems` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cart_id` int(11) unsigned NOT NULL,
  `item_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `cartitems` WRITE;
/*!40000 ALTER TABLE `cartitems` DISABLE KEYS */;

INSERT INTO `cartitems` (`id`, `cart_id`, `item_id`)
VALUES
	(133,66,16),
	(134,67,16);

/*!40000 ALTER TABLE `cartitems` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table carts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `carts`;

CREATE TABLE `carts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `ispurchased` tinyint(1) DEFAULT '0',
  `isexpired` tinyint(1) DEFAULT '0',
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `carts` WRITE;
/*!40000 ALTER TABLE `carts` DISABLE KEYS */;

INSERT INTO `carts` (`id`, `user_id`, `ispurchased`, `isexpired`, `timestamp`)
VALUES
	(66,51,0,0,'2016-05-05 14:21:29'),
	(67,83,0,0,'2016-05-05 21:25:57');

/*!40000 ALTER TABLE `carts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table comments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `comments`;

CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comment` varchar(256) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `ticket_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_comments_users1_idx` (`user_id`),
  KEY `fk_comments_tickets1_idx` (`ticket_id`),
  CONSTRAINT `fk_comments_tickets1` FOREIGN KEY (`ticket_id`) REFERENCES `tickets` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_comments_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table filters
# ------------------------------------------------------------

DROP TABLE IF EXISTS `filters`;

CREATE TABLE `filters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filtertype_id` int(11) NOT NULL,
  `name` varchar(512) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `filters` WRITE;
/*!40000 ALTER TABLE `filters` DISABLE KEYS */;

INSERT INTO `filters` (`id`, `filtertype_id`, `name`, `timestamp`)
VALUES
	(1,1,'Double Door','2016-03-03 21:19:13'),
	(2,1,'Single Door','2016-03-03 21:19:19'),
	(3,2,'below Rs.5000','2016-03-26 15:36:23'),
	(4,2,'Rs.5000 to 10000','2016-03-26 15:36:30'),
	(5,3,'below 200 lts','2016-03-26 15:35:44'),
	(6,3,'above 200 lts','2016-03-26 15:35:55'),
	(7,4,'Red','2016-03-26 14:29:11'),
	(8,4,'Blue','2016-03-26 14:29:13'),
	(9,4,'white','2016-03-26 14:29:15'),
	(10,4,'other colours','2016-03-26 14:29:22'),
	(11,5,'less than 3 star','2016-03-26 14:38:39'),
	(12,5,'3 star and above','2016-03-26 14:54:34'),
	(13,6,'Fully Automatic','2016-03-26 15:29:46'),
	(14,6,'Semi Automatic','2016-03-26 15:29:57'),
	(15,7,'Front Load','2016-03-26 15:30:13'),
	(16,7,'Top Load','2016-03-26 15:30:21'),
	(17,8,'6Kg and below','2016-03-26 15:34:25'),
	(18,8,'Above 6Kg','2016-03-26 15:34:22'),
	(19,9,'Split','2016-03-26 16:09:17'),
	(20,9,'Window','2016-03-26 16:09:27'),
	(21,10,'Less than 2 tonnes','2016-03-26 16:11:00'),
	(22,10,'2 tonnes & above','2016-03-26 16:11:11'),
	(23,11,'Less than 3 star','2016-03-26 16:11:27'),
	(24,11,'3 star & above','2016-03-26 16:11:38'),
	(25,12,'Electrical','2016-03-26 16:12:30'),
	(26,12,'Non-Electrical','2016-03-26 16:12:50'),
	(27,13,'Gravity based','2016-03-26 16:14:08'),
	(28,13,'RO+UV based','2016-03-26 16:19:22'),
	(29,14,'7Lts and below','2016-03-26 16:19:58'),
	(30,14,'Above 7Lts','2016-03-26 16:20:17'),
	(31,15,'3 Seater & below ','2016-03-26 16:25:10'),
	(32,15,'Above 3 Seater','2016-03-26 16:25:28'),
	(33,16,'Fabric','2016-03-26 16:25:44'),
	(34,16,'Leather','2016-03-26 16:25:53'),
	(35,16,'Wood','2016-03-26 16:26:22'),
	(36,16,'Metal','2016-03-26 16:26:34'),
	(37,17,'Straight','2016-03-26 16:26:58'),
	(38,17,'L & U Shaped','2016-03-26 16:27:13'),
	(39,18,'Beige','2016-03-26 16:27:56'),
	(40,18,'Black','2016-03-26 16:28:02'),
	(41,18,'Other Colors','2016-03-26 16:28:18'),
	(42,19,'Plastic','2016-03-26 16:31:46'),
	(43,19,'Fabric','2016-03-26 16:31:52'),
	(44,19,'Leatherette','2016-03-26 16:32:01'),
	(45,20,'Black','2016-03-26 16:33:37'),
	(46,20,'Beige','2016-03-26 16:33:53'),
	(47,20,'Other colors','2016-03-26 16:33:59'),
	(48,21,'Without Wheels ','2016-03-26 16:35:36'),
	(49,21,'With Seat Lock & Height Adjustment','2016-03-26 16:35:12'),
	(50,22,'Yes','2016-03-26 16:40:15'),
	(51,22,'No','2016-03-26 16:40:20'),
	(52,23,'Yes','2016-03-26 16:40:27'),
	(53,23,'No','2016-03-26 16:40:33');

/*!40000 ALTER TABLE `filters` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table filtertypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `filtertypes`;

CREATE TABLE `filtertypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `productcategory_id` int(11) NOT NULL,
  `name` varchar(512) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `filtertypes` WRITE;
/*!40000 ALTER TABLE `filtertypes` DISABLE KEYS */;

INSERT INTO `filtertypes` (`id`, `productcategory_id`, `name`, `timestamp`)
VALUES
	(1,3,'Door Type','2016-03-03 20:48:31'),
	(2,3,'Price','2016-03-26 14:23:04'),
	(3,3,'Capacity','2016-03-26 14:23:07'),
	(4,3,'Color','2016-03-26 14:23:09'),
	(5,3,'Energy Rating','2016-03-26 14:40:20'),
	(6,4,'Function Type','2016-03-26 14:50:14'),
	(7,4,'Load Type','2016-03-26 14:50:33'),
	(8,4,'Capacity','2016-03-26 15:26:46'),
	(9,5,'Type','2016-03-26 15:37:14'),
	(10,5,'Capacity','2016-03-26 15:37:27'),
	(11,5,'Energy Efficency','2016-03-26 15:37:41'),
	(12,6,'Type','2016-03-26 15:38:24'),
	(13,6,'Purification Technology','2016-03-26 15:38:34'),
	(14,6,'Capacity','2016-03-26 15:38:52'),
	(15,7,'Seating Capacity','2016-03-26 16:22:02'),
	(16,7,'Material','2016-03-26 16:22:30'),
	(17,7,'Shape','2016-03-26 16:22:40'),
	(18,7,'Color','2016-03-26 16:22:48'),
	(19,8,'Material','2016-03-26 16:28:59'),
	(20,8,'Color','2016-03-26 16:29:05'),
	(21,8,'Type','2016-03-26 16:30:11'),
	(22,9,'Wheels Included','2016-03-26 16:39:28'),
	(23,9,'Storage Included','2016-03-26 16:40:53');

/*!40000 ALTER TABLE `filtertypes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table images
# ------------------------------------------------------------

DROP TABLE IF EXISTS `images`;

CREATE TABLE `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `description` text,
  `thumbnail` varchar(512) DEFAULT '',
  `isdefault` int(11) DEFAULT '0',
  `productitem_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_images_products_vendors1_idx` (`productitem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `images` WRITE;
/*!40000 ALTER TABLE `images` DISABLE KEYS */;

INSERT INTO `images` (`id`, `name`, `description`, `thumbnail`, `isdefault`, `productitem_id`)
VALUES
	(32,'11_1.jpg','Image 1','',1,11),
	(33,'11_2.jpg','Image 2','',0,11),
	(34,'11_3.jpg','Image 3','',0,11),
	(35,'12_1.jpg','Image 1','',1,12),
	(36,'12_2.jpg','Image 2','',0,12),
	(37,'12_3.jpg','Image 3','',0,12),
	(38,'13_1.jpg','Image 1','',1,13),
	(39,'13_2.jpg','Image 2','',0,13),
	(40,'13_3.jpg','Image 3','',0,13),
	(43,'16_1.jpeg','Image 1','',1,16),
	(44,'16_2.jpg','Image 2','',0,16),
	(45,'16_3.jpg','Image 3','',0,16),
	(46,'16_4.jpg','Image 4','',0,16),
	(47,'16_5.jpg','Image 5','',0,16),
	(48,'16_6.jpg','Image 6','',0,16),
	(49,'17_1.jpg','Image 1','',1,17),
	(50,'18_1.jpg','Image 1','',1,18),
	(51,'18_1.jpg','Image 1','',1,19),
	(52,'18_1.jpg','Image 1','',1,20),
	(53,'18_1.jpg','Image 1','',1,21),
	(54,'18_1.jpg','Image 1','',1,22),
	(55,'18_1.jpg','Image 1','',1,23),
	(56,'18_1.jpg','Image 1','',1,24),
	(57,'18_1.jpg','Image 1','',1,25),
	(58,'18_1.jpg','Image 1','',1,26),
	(59,'18_1.jpg','Image 1','',1,27),
	(60,'18_1.jpg','Image 1','',1,28),
	(61,'18_1.jpg','Image 1','',1,29),
	(62,'18_1.jpg','Image 1','',1,30),
	(63,'18_1.jpg','Image 1','',1,31);

/*!40000 ALTER TABLE `images` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table notificationobjectchanges
# ------------------------------------------------------------

DROP TABLE IF EXISTS `notificationobjectchanges`;

CREATE TABLE `notificationobjectchanges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `verb` varchar(45) DEFAULT NULL,
  `actor` varchar(45) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT NULL,
  `notificationobject_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_notificationobjectchanges_notificationobjects1_idx` (`notificationobject_id`),
  CONSTRAINT `fk_notificationobjectchanges_notificationobjects1` FOREIGN KEY (`notificationobject_id`) REFERENCES `notificationobjects` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table notificationobjects
# ------------------------------------------------------------

DROP TABLE IF EXISTS `notificationobjects`;

CREATE TABLE `notificationobjects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `object_id` int(11) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT NULL,
  `notification_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_notificationobjects_notifications1_idx` (`notification_id`),
  CONSTRAINT `fk_notificationobjects_notifications1` FOREIGN KEY (`notification_id`) REFERENCES `notifications` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table notifications
# ------------------------------------------------------------

DROP TABLE IF EXISTS `notifications`;

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(45) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_notifications_users1_idx` (`user_id`),
  CONSTRAINT `fk_notifications_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table onetimepasswords
# ------------------------------------------------------------

DROP TABLE IF EXISTS `onetimepasswords`;

CREATE TABLE `onetimepasswords` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phonenumber` varchar(256) NOT NULL DEFAULT '',
  `otp` int(4) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `onetimepasswords` WRITE;
/*!40000 ALTER TABLE `onetimepasswords` DISABLE KEYS */;

INSERT INTO `onetimepasswords` (`id`, `phonenumber`, `otp`, `user_id`)
VALUES
	(72,'9538092344',4691,51);

/*!40000 ALTER TABLE `onetimepasswords` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table orderdetails
# ------------------------------------------------------------

DROP TABLE IF EXISTS `orderdetails`;

CREATE TABLE `orderdetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` int(11) DEFAULT NULL,
  `ispaid` tinyint(1) DEFAULT '0',
  `address_id` int(11) NOT NULL,
  `orderdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  `cart_id` int(11) NOT NULL DEFAULT '0',
  `isdelivered` tinyint(1) DEFAULT '0',
  `iscancelled` tinyint(1) DEFAULT '0',
  `deliverydate` datetime DEFAULT NULL,
  `canceldate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_orderdetails_users1_idx` (`user_id`),
  CONSTRAINT `fk_orderdetails_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `orderdetails` WRITE;
/*!40000 ALTER TABLE `orderdetails` DISABLE KEYS */;

INSERT INTO `orderdetails` (`id`, `amount`, `ispaid`, `address_id`, `orderdate`, `user_id`, `cart_id`, `isdelivered`, `iscancelled`, `deliverydate`, `canceldate`)
VALUES
	(40,125245,0,22,'2016-05-01 11:39:10',51,51,0,0,NULL,'2016-05-02 22:34:36'),
	(41,15900,0,22,'2016-05-01 11:39:57',51,52,0,0,NULL,'2016-05-02 22:36:26'),
	(42,86689,0,22,'2016-05-03 16:10:20',51,53,0,0,NULL,NULL),
	(43,46456,0,22,'2016-05-04 09:37:13',51,64,0,0,NULL,NULL);

/*!40000 ALTER TABLE `orderdetails` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table orders
# ------------------------------------------------------------

DROP TABLE IF EXISTS `orders`;

CREATE TABLE `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `price` int(11) DEFAULT '0',
  `isshipped` tinyint(1) DEFAULT '0',
  `shippeddate` datetime DEFAULT NULL,
  `isdelivered` tinyint(1) DEFAULT '0',
  `deliverydate` datetime DEFAULT NULL,
  `ispaid` int(11) DEFAULT '0',
  `deliverycost` int(11) DEFAULT '0',
  `productitems_id` int(11) NOT NULL,
  `orderdetail_id` int(11) NOT NULL,
  `isqcdone` tinyint(1) DEFAULT '0',
  `qcdonedate` datetime DEFAULT NULL,
  `iscancelled` tinyint(1) DEFAULT '0',
  `canceldate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_orders_products_vendors1_idx` (`productitems_id`),
  KEY `fk_orders_orderdetails1_idx` (`orderdetail_id`),
  CONSTRAINT `fk_orders_orderdetails1` FOREIGN KEY (`orderdetail_id`) REFERENCES `orderdetails` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;

INSERT INTO `orders` (`id`, `price`, `isshipped`, `shippeddate`, `isdelivered`, `deliverydate`, `ispaid`, `deliverycost`, `productitems_id`, `orderdetail_id`, `isqcdone`, `qcdonedate`, `iscancelled`, `canceldate`)
VALUES
	(54,78789,1,'2016-05-03 04:02:33',1,'2016-05-03 04:02:43',0,0,24,40,1,'2016-05-03 04:02:16',0,'2016-05-02 22:34:26'),
	(55,46456,1,'2016-05-03 04:03:15',1,'2016-05-03 04:09:58',0,0,20,40,1,'2016-05-03 04:03:09',0,'2016-05-02 22:34:36'),
	(56,7900,0,NULL,0,NULL,0,0,16,41,1,'2016-05-03 04:03:11',1,'2016-05-03 04:09:02'),
	(57,8000,1,'2016-05-03 04:09:25',1,'2016-05-03 05:36:26',0,0,17,41,1,'2016-05-03 04:03:13',0,'2016-05-02 22:36:26'),
	(58,7900,1,'2016-05-03 05:41:14',1,'2016-05-03 05:41:36',0,0,16,42,1,'2016-05-03 05:40:50',0,NULL),
	(59,78789,0,NULL,0,NULL,0,0,24,42,1,'2016-05-03 05:41:07',0,NULL),
	(60,46456,1,'2016-05-03 23:12:34',1,'2016-05-03 23:13:28',0,0,20,43,1,'2016-05-03 23:08:09',0,NULL);

/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table parentcategories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `parentcategories`;

CREATE TABLE `parentcategories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(512) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(512) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `parentcategories` WRITE;
/*!40000 ALTER TABLE `parentcategories` DISABLE KEYS */;

INSERT INTO `parentcategories` (`id`, `name`, `description`, `image`)
VALUES
	(1,'Home Appliances','Home Appliances',''),
	(2,'Furniture','Furniture',''),
	(3,'Cat','','');

/*!40000 ALTER TABLE `parentcategories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table productcategories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `productcategories`;

CREATE TABLE `productcategories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `description` text NOT NULL,
  `image` varchar(512) NOT NULL,
  `parentcategory_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `productcategories` WRITE;
/*!40000 ALTER TABLE `productcategories` DISABLE KEYS */;

INSERT INTO `productcategories` (`id`, `name`, `description`, `image`, `parentcategory_id`)
VALUES
	(3,'Refrigerators','Top Quality Refrigerators','fridge.jpg',1),
	(4,'Washing Machines','Top Quality Washing Machines','washingmachine.jpg',1),
	(5,'Air Conditioners','Top quality Air Conditioners','airconditioner.jpg',1),
	(6,'Water Purifiers','Top Quality Water Purifiers','waterpurifier.jpg',1),
	(7,'Sofa set','Sofa set at cheap price','sofaset.jpg',2),
	(8,'Office Chair','Best Quality Office Chairs','officechair.jpg',2),
	(9,'Computer Table','Compuer Tables at best price','computertable.jpg',2);

/*!40000 ALTER TABLE `productcategories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table productfilters
# ------------------------------------------------------------

DROP TABLE IF EXISTS `productfilters`;

CREATE TABLE `productfilters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filter_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table productitemfilters
# ------------------------------------------------------------

DROP TABLE IF EXISTS `productitemfilters`;

CREATE TABLE `productitemfilters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filter_id` int(11) NOT NULL,
  `productitem_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table productitems
# ------------------------------------------------------------

DROP TABLE IF EXISTS `productitems`;

CREATE TABLE `productitems` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qualityindex` int(11) DEFAULT '0',
  `title` varchar(512) NOT NULL,
  `description` varchar(256) DEFAULT NULL,
  `issold` int(11) DEFAULT '0',
  `isapproved` int(11) DEFAULT '0',
  `saleprice` int(11) DEFAULT '0',
  `manufacturingyear` int(11) NOT NULL,
  `warranty` varchar(256) NOT NULL,
  `vendorprice` int(11) DEFAULT '0',
  `ispaid` int(11) DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `isdelivered` int(11) DEFAULT '0',
  `productcategory_id` int(11) DEFAULT NULL,
  `color` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_products_vendors_users1_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `productitems` WRITE;
/*!40000 ALTER TABLE `productitems` DISABLE KEYS */;

INSERT INTO `productitems` (`id`, `qualityindex`, `title`, `description`, `issold`, `isapproved`, `saleprice`, `manufacturingyear`, `warranty`, `vendorprice`, `ispaid`, `user_id`, `product_id`, `timestamp`, `isdelivered`, `productcategory_id`, `color`)
VALUES
	(11,0,'LG single door','good one',1,1,6000,2011,'',0,0,22,3,'2016-04-27 08:24:56',0,3,'white'),
	(12,0,'Samsung Top Load','very good',1,1,5000,2013,'',0,0,22,4,'2016-04-27 07:56:30',0,4,'red'),
	(13,0,'LG Washing Machine','good',1,1,4000,2010,'',0,0,22,2,'2016-04-27 07:50:06',0,4,'blue'),
	(16,0,'Samsung Double Door','Single Door, Best cooling, Best looks',1,1,7900,2012,'',0,0,22,2,'2016-04-27 08:19:44',0,3,'grey'),
	(17,0,'p1','good one, best cooling',1,1,8000,2012,'',0,0,22,2,'2016-05-01 11:39:57',0,3,NULL),
	(18,0,'Sam Dup','good',0,1,8000,2012,'',0,0,22,2,'2016-04-27 16:48:07',0,3,NULL),
	(19,0,'Sam Dup','good',0,1,3453,2012,'',0,0,22,2,'2016-04-27 16:48:09',0,3,NULL),
	(20,0,'Sam Dup','good',1,1,46456,2012,'',0,0,22,2,'2016-05-01 11:34:52',0,3,NULL),
	(21,0,'Sam Dup','goo',0,1,4353,2012,'',0,0,22,2,'2016-04-27 16:48:13',0,3,NULL),
	(22,0,'Sam Dup','good',0,1,7878,2012,'',0,0,22,2,'2016-04-27 16:48:15',0,3,NULL),
	(23,0,'Sam Dup','good',0,1,2323,2012,'',0,0,22,2,'2016-04-27 16:48:16',0,3,NULL),
	(24,0,'Sam Dup','good',1,1,78789,2012,'',0,0,22,2,'2016-04-29 10:37:32',0,3,NULL),
	(25,0,'Sam Dup','good',0,1,4543,2012,'',0,0,22,2,'2016-04-27 16:48:19',0,3,NULL),
	(26,0,'Sam Dup','good',1,1,32434,2012,'',0,0,22,2,'2016-04-27 17:58:24',0,3,NULL),
	(27,0,'Sam Dup','good',0,1,676,2012,'',0,0,22,2,'2016-04-27 16:48:21',0,3,NULL),
	(28,0,'Sam Dup','bad',0,1,5656,2012,'',0,0,22,2,'2016-04-27 16:48:23',0,3,NULL),
	(29,0,'Sam Dup','worse',0,1,8989,2012,'',0,0,22,2,'2016-04-27 16:48:25',0,3,NULL),
	(30,0,'Sam Dup','worst',0,1,123,2012,'',0,0,22,2,'2016-04-27 16:48:28',0,3,NULL),
	(31,0,'Sam Dup','enough',0,1,12223,2012,'',0,0,22,2,'2016-04-27 16:48:31',0,3,NULL);

/*!40000 ALTER TABLE `productitems` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table productrequests
# ------------------------------------------------------------

DROP TABLE IF EXISTS `productrequests`;

CREATE TABLE `productrequests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phonenumber` varchar(256) DEFAULT NULL,
  `productname` varchar(256) DEFAULT NULL,
  `model` varchar(45) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_productrequests_users1_idx` (`user_id`),
  CONSTRAINT `fk_productrequests_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table products
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `modelnumber` varchar(512) NOT NULL,
  `description` text,
  `shortdescription` varchar(512) NOT NULL,
  `instock` int(11) DEFAULT '0',
  `stockcount` int(11) DEFAULT '0',
  `origimage` varchar(256) DEFAULT NULL,
  `mrp` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;

INSERT INTO `products` (`id`, `name`, `modelnumber`, `description`, `shortdescription`, `instock`, `stockcount`, `origimage`, `mrp`)
VALUES
	(2,'LG single Door','lg101','Cool technology','Intellitech',1,0,NULL,14000),
	(3,'LG single door','lg102','good','good one',1,0,NULL,1233),
	(4,'samsung','sam123','sdfkj','good',1,0,NULL,12000),
	(5,'LG Model','mod123','nice','good one',1,0,NULL,15000);

/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table qualityindexes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `qualityindexes`;

CREATE TABLE `qualityindexes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(512) NOT NULL,
  `productcategory_id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `qualityindexes` WRITE;
/*!40000 ALTER TABLE `qualityindexes` DISABLE KEYS */;

INSERT INTO `qualityindexes` (`id`, `name`, `productcategory_id`, `timestamp`)
VALUES
	(1,'Age',3,'2016-03-12 10:56:38'),
	(2,'Door Seal',3,'2016-03-12 11:26:00'),
	(3,'Energy Efficiency',3,'2016-03-12 10:56:51'),
	(4,'Appearance',3,'2016-03-12 10:57:22'),
	(5,'Motor Condition',3,'2016-03-12 11:02:03'),
	(6,'Age',4,'2016-03-12 11:00:14'),
	(7,'Full Cycle Functionality',4,'2016-03-12 11:00:53'),
	(8,'Tub/Drum Condition',4,'2016-04-04 10:33:56'),
	(9,'Efficiency Grading',4,'2016-03-12 11:00:55'),
	(10,'Appearance',4,'2016-03-12 11:00:56'),
	(12,'Age',5,'2016-03-12 11:01:12'),
	(13,'Cooling Capacity',5,'2016-03-12 11:01:21'),
	(14,'Energy Efficiency',5,'2016-03-12 11:02:48'),
	(15,'Appearance',5,'2016-03-12 11:02:50'),
	(16,'Filters/Dehumidifiers',5,'2016-04-04 10:36:31'),
	(18,'Thermostat setting',5,'2016-04-04 10:37:19'),
	(19,'Age',6,'2016-03-12 11:03:46'),
	(20,'Filter Codition',6,'2016-03-12 11:03:46'),
	(21,'Appearance',6,'2016-03-12 11:03:46'),
	(22,'Interior Parts',6,'2016-03-12 11:03:47'),
	(23,'Other',6,'2016-03-12 11:03:49'),
	(24,'Age',7,'2016-03-12 11:04:45'),
	(25,'Cushion Condition',7,'2016-03-12 11:04:46'),
	(26,'Frame/Fabric Condition',7,'2016-04-04 10:38:45'),
	(27,'Spring Condition',7,'2016-03-12 11:13:12'),
	(28,'Appearance',7,'2016-03-12 11:13:36'),
	(29,'Other',7,'2016-03-12 11:13:40'),
	(30,'Defrosting Time',3,'2016-04-04 10:34:06'),
	(31,'Noise',3,'2016-04-04 10:35:18'),
	(32,'Adjustability',8,'2016-04-04 10:39:02'),
	(33,'Seat Material',8,'2016-04-04 10:39:14'),
	(34,'Mobility',8,'2016-04-04 10:39:27'),
	(35,'Desk Shape',9,'2016-04-04 10:39:43'),
	(36,'Desk Material',9,'2016-04-04 10:40:04');

/*!40000 ALTER TABLE `qualityindexes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table qualityindexvalues
# ------------------------------------------------------------

DROP TABLE IF EXISTS `qualityindexvalues`;

CREATE TABLE `qualityindexvalues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qualityindexes_id` int(11) NOT NULL,
  `value_text` varchar(512) NOT NULL DEFAULT '',
  `value` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `qualityindexvalues` WRITE;
/*!40000 ALTER TABLE `qualityindexvalues` DISABLE KEYS */;

INSERT INTO `qualityindexvalues` (`id`, `qualityindexes_id`, `value_text`, `value`)
VALUES
	(1,1,'>= 2014',5),
	(2,1,'2012 - 2014',4),
	(3,1,'2010 - 2012',3),
	(4,1,'2005 - 2010',2),
	(5,1,'< 2005',1),
	(6,2,'New Seal Installed',5),
	(7,2,'Good Working Seal',4),
	(8,2,'Average Seal',3),
	(9,2,'Bad Seal',2),
	(10,2,'Seal Damaged',1),
	(11,3,'Five Star',5),
	(12,3,'Four Star',4),
	(13,3,'Three Star',3),
	(14,3,'Two Star',2),
	(15,3,'< Two Star',1),
	(16,4,'Like New W/o Any Major Scratches or Dents',5),
	(17,4,'Good With Some Scratches and no Dents',4),
	(18,4,'Average With Some Dents and Scratches',3),
	(19,4,'Some Parts are Broken',2),
	(20,4,'Looks Bad',1),
	(21,5,'New Motor',5),
	(22,5,'Excellent Working Condition',4),
	(23,5,'Good Working Condition',3),
	(24,5,'Average',2),
	(25,5,'Bad',1),
	(26,6,'>= 2014',5),
	(27,6,'2012 - 2014',4),
	(28,6,'2005 - 2010',2),
	(30,6,'2010 - 2012',3),
	(31,6,'< 2005',1),
	(32,10,'Like New W/o Any Major Scratches or Dents',5),
	(33,10,'Good With Some Scratches and no Dents',4),
	(34,10,'Average With Some Dents and Scratches',3),
	(35,10,'Some Parts are Broken',2),
	(36,10,'Looks Bad',1),
	(37,8,'Like New',5),
	(38,8,'Good ',4),
	(39,8,'Average',3),
	(40,8,'Bad',2),
	(41,9,'5 star',5),
	(42,9,'4 star',4),
	(43,9,'3 star',3),
	(44,9,'2 star',2),
	(45,9,'1 star',1),
	(50,12,'>2015',5),
	(51,12,'2014 to 2015',4),
	(52,12,'2012 to 2014',3),
	(53,12,'2010 to 2012',2),
	(54,12,'<2010',1),
	(55,13,'Insta cooling',5),
	(56,13,'Good',4),
	(57,13,'Average',3),
	(58,13,'Poor',2),
	(59,14,'5 star',5),
	(60,14,'4 star',4),
	(61,14,'3 star',3),
	(62,14,'2 star',2),
	(63,14,'1 star',1),
	(64,15,'Like New',5),
	(65,15,'Good',4),
	(66,15,'Average',3),
	(67,15,'Bad',2),
	(68,16,'Excellent',5),
	(69,16,'Good',4),
	(70,16,'Average',3),
	(71,16,'Bad',2),
	(72,18,'Excellent Working condition',5),
	(73,18,'Good working condition',4),
	(74,18,'Average Working condition',3),
	(75,19,'> 2014',2),
	(76,20,'Excellent',5),
	(77,20,'Good',4),
	(78,20,'Average',3),
	(79,20,'Bad',2),
	(80,21,'Looks New w/o scratches/bent',5),
	(81,21,'Good',4),
	(82,21,'Average',3),
	(83,21,'Bad',2),
	(84,22,'Excellent',5),
	(85,22,'Good',4),
	(86,22,'Average',3),
	(87,22,'Bad',2),
	(88,23,'Excellent',5),
	(89,23,'Good',4),
	(90,23,'Average',3),
	(91,23,'Bad',2),
	(92,24,'>2015',5),
	(93,24,'2013 to 2015',4),
	(94,24,'2010 to 2013',3),
	(95,24,'<2010',2),
	(96,25,'Excellent',5),
	(97,25,'Good',4),
	(98,25,'Average',3),
	(99,25,'Bad',2),
	(100,26,'Excellent',5),
	(101,26,'Good',4),
	(102,26,'Average',3),
	(103,26,'Bad',2),
	(104,27,'Excellent',5),
	(105,27,'Good',4),
	(106,27,'Average',3),
	(107,27,'Bad',2),
	(108,28,'Excellent',5),
	(109,28,'Good',4),
	(110,28,'Average',3),
	(111,28,'Bad',2),
	(112,29,'Excellent',5),
	(113,29,'Good',4),
	(114,29,'Average',3),
	(115,29,'Bad',2),
	(116,30,'Fast',5),
	(117,30,'Good',4),
	(118,30,'Average',3),
	(119,30,'Bad',2),
	(120,31,'Zero Noise',5),
	(121,31,'very little noise',4),
	(122,31,'high noise',3),
	(123,32,'Easily Adjustable',5),
	(124,32,'Adjustable',4),
	(125,32,'very Tight',3),
	(126,33,'Excellent',5),
	(127,33,'Good',4),
	(128,33,'Average',3),
	(129,33,'Bad',2),
	(130,34,'Easily movable',5),
	(131,34,'Movable',4),
	(132,34,'Needs Lubrication',3),
	(133,34,'Tight',2),
	(134,35,'Excellent',5),
	(135,35,'Good',4),
	(136,35,'Average',3),
	(137,35,'Bad',2),
	(138,36,'Excellent',5),
	(139,36,'Good',4),
	(140,36,'Average',3),
	(141,36,'Bad',2),
	(142,7,'High',5),
	(143,7,'Good',4),
	(144,7,'Average',3),
	(145,7,'Bad',2),
	(146,19,'2012 - 2014',2),
	(147,19,'2010 - 2012',5),
	(148,19,'2005 - 2010',2),
	(149,19,'<2005',2);

/*!40000 ALTER TABLE `qualityindexvalues` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table qualityratings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `qualityratings`;

CREATE TABLE `qualityratings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` int(11) NOT NULL,
  `qualityindex_id` int(11) NOT NULL,
  `productitem_id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `description` varchar(256) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `qualityratings` WRITE;
/*!40000 ALTER TABLE `qualityratings` DISABLE KEYS */;

INSERT INTO `qualityratings` (`id`, `value`, `qualityindex_id`, `productitem_id`, `timestamp`, `description`)
VALUES
	(61,3,1,11,'2016-04-03 12:36:36',''),
	(62,4,2,11,'2016-04-03 12:36:36',''),
	(63,4,3,11,'2016-04-03 12:36:36',''),
	(64,4,4,11,'2016-04-03 12:36:36',''),
	(65,4,5,11,'2016-04-03 12:36:36',''),
	(66,4,1,12,'2016-04-05 19:40:26',''),
	(67,5,2,12,'2016-04-05 19:40:26',''),
	(68,3,3,12,'2016-04-05 19:40:26',''),
	(69,4,4,12,'2016-04-05 19:40:26',''),
	(70,4,5,12,'2016-04-05 19:40:26',''),
	(71,5,1,13,'2016-04-11 16:24:50',''),
	(72,4,2,13,'2016-04-11 16:24:50',''),
	(73,3,3,13,'2016-04-11 16:24:50',''),
	(74,4,4,13,'2016-04-11 16:24:50',''),
	(75,3,5,13,'2016-04-11 16:24:50',''),
	(76,5,1,14,'2016-04-19 08:54:31',''),
	(77,5,2,14,'2016-04-19 08:54:31',''),
	(78,4,3,14,'2016-04-19 08:54:31',''),
	(79,4,4,14,'2016-04-19 08:54:31',''),
	(80,4,5,14,'2016-04-19 08:54:31',''),
	(81,2,1,15,'2016-04-19 11:53:01',''),
	(82,1,2,15,'2016-04-19 11:53:01',''),
	(83,1,3,15,'2016-04-19 11:53:01',''),
	(84,1,4,15,'2016-04-19 11:53:01',''),
	(85,1,5,15,'2016-04-19 11:53:01',''),
	(86,4,1,16,'2016-04-20 01:08:47',''),
	(87,2,2,16,'2016-04-20 01:08:47',''),
	(88,5,3,16,'2016-04-20 01:08:47',''),
	(89,4,4,16,'2016-04-20 01:08:47',''),
	(90,1,5,16,'2016-04-20 01:08:47',''),
	(91,5,1,17,'2016-04-27 13:02:43',''),
	(92,3,2,17,'2016-04-27 13:02:43',''),
	(93,4,3,17,'2016-04-27 13:02:43',''),
	(94,1,4,17,'2016-04-27 13:02:43',''),
	(95,2,5,17,'2016-04-27 13:02:43',''),
	(96,2,30,17,'2016-04-27 13:02:43',''),
	(97,4,31,17,'2016-04-27 13:02:43',''),
	(98,5,1,18,'2016-04-27 13:06:33',''),
	(99,4,2,18,'2016-04-27 13:06:33',''),
	(100,2,3,18,'2016-04-27 13:06:33',''),
	(101,3,4,18,'2016-04-27 13:06:33',''),
	(102,3,5,18,'2016-04-27 13:06:33',''),
	(103,3,30,18,'2016-04-27 13:06:33',''),
	(104,3,31,18,'2016-04-27 13:06:33',''),
	(105,5,1,19,'2016-04-27 13:06:34',''),
	(106,4,2,19,'2016-04-27 13:06:34',''),
	(107,2,3,19,'2016-04-27 13:06:34',''),
	(108,3,4,19,'2016-04-27 13:06:34',''),
	(109,3,5,19,'2016-04-27 13:06:34',''),
	(110,3,30,19,'2016-04-27 13:06:34',''),
	(111,3,31,19,'2016-04-27 13:06:34',''),
	(112,5,1,20,'2016-04-27 13:06:34',''),
	(113,4,2,20,'2016-04-27 13:06:34',''),
	(114,2,3,20,'2016-04-27 13:06:34',''),
	(115,3,4,20,'2016-04-27 13:06:34',''),
	(116,3,5,20,'2016-04-27 13:06:34',''),
	(117,3,30,20,'2016-04-27 13:06:34',''),
	(118,3,31,20,'2016-04-27 13:06:34',''),
	(119,5,1,21,'2016-04-27 13:06:35',''),
	(120,4,2,21,'2016-04-27 13:06:35',''),
	(121,2,3,21,'2016-04-27 13:06:35',''),
	(122,3,4,21,'2016-04-27 13:06:35',''),
	(123,3,5,21,'2016-04-27 13:06:35',''),
	(124,3,30,21,'2016-04-27 13:06:35',''),
	(125,3,31,21,'2016-04-27 13:06:35',''),
	(126,5,1,22,'2016-04-27 13:06:35',''),
	(127,4,2,22,'2016-04-27 13:06:35',''),
	(128,2,3,22,'2016-04-27 13:06:35',''),
	(129,3,4,22,'2016-04-27 13:06:35',''),
	(130,3,5,22,'2016-04-27 13:06:35',''),
	(131,3,30,22,'2016-04-27 13:06:35',''),
	(132,3,31,22,'2016-04-27 13:06:35',''),
	(133,5,1,23,'2016-04-27 13:06:35',''),
	(134,4,2,23,'2016-04-27 13:06:35',''),
	(135,2,3,23,'2016-04-27 13:06:35',''),
	(136,3,4,23,'2016-04-27 13:06:35',''),
	(137,3,5,23,'2016-04-27 13:06:35',''),
	(138,3,30,23,'2016-04-27 13:06:35',''),
	(139,3,31,23,'2016-04-27 13:06:35',''),
	(140,5,1,24,'2016-04-27 13:06:35',''),
	(141,4,2,24,'2016-04-27 13:06:35',''),
	(142,2,3,24,'2016-04-27 13:06:35',''),
	(143,3,4,24,'2016-04-27 13:06:35',''),
	(144,3,5,24,'2016-04-27 13:06:35',''),
	(145,3,30,24,'2016-04-27 13:06:35',''),
	(146,3,31,24,'2016-04-27 13:06:35',''),
	(147,5,1,25,'2016-04-27 13:06:35',''),
	(148,4,2,25,'2016-04-27 13:06:35',''),
	(149,2,3,25,'2016-04-27 13:06:35',''),
	(150,3,4,25,'2016-04-27 13:06:35',''),
	(151,3,5,25,'2016-04-27 13:06:35',''),
	(152,3,30,25,'2016-04-27 13:06:35',''),
	(153,3,31,25,'2016-04-27 13:06:35',''),
	(154,5,1,26,'2016-04-27 13:06:35',''),
	(155,4,2,26,'2016-04-27 13:06:35',''),
	(156,2,3,26,'2016-04-27 13:06:35',''),
	(157,3,4,26,'2016-04-27 13:06:35',''),
	(158,3,5,26,'2016-04-27 13:06:35',''),
	(159,3,30,26,'2016-04-27 13:06:35',''),
	(160,3,31,26,'2016-04-27 13:06:35',''),
	(161,5,1,27,'2016-04-27 13:06:35',''),
	(162,4,2,27,'2016-04-27 13:06:35',''),
	(163,2,3,27,'2016-04-27 13:06:35',''),
	(164,3,4,27,'2016-04-27 13:06:35',''),
	(165,3,5,27,'2016-04-27 13:06:35',''),
	(166,3,30,27,'2016-04-27 13:06:35',''),
	(167,3,31,27,'2016-04-27 13:06:35',''),
	(168,5,1,28,'2016-04-27 13:06:36',''),
	(169,4,2,28,'2016-04-27 13:06:36',''),
	(170,2,3,28,'2016-04-27 13:06:36',''),
	(171,3,4,28,'2016-04-27 13:06:36',''),
	(172,3,5,28,'2016-04-27 13:06:36',''),
	(173,3,30,28,'2016-04-27 13:06:36',''),
	(174,3,31,28,'2016-04-27 13:06:36',''),
	(175,5,1,29,'2016-04-27 13:06:36',''),
	(176,4,2,29,'2016-04-27 13:06:36',''),
	(177,2,3,29,'2016-04-27 13:06:36',''),
	(178,3,4,29,'2016-04-27 13:06:36',''),
	(179,3,5,29,'2016-04-27 13:06:36',''),
	(180,3,30,29,'2016-04-27 13:06:36',''),
	(181,3,31,29,'2016-04-27 13:06:36',''),
	(182,5,1,30,'2016-04-27 13:06:36',''),
	(183,4,2,30,'2016-04-27 13:06:36',''),
	(184,2,3,30,'2016-04-27 13:06:36',''),
	(185,3,4,30,'2016-04-27 13:06:36',''),
	(186,3,5,30,'2016-04-27 13:06:36',''),
	(187,3,30,30,'2016-04-27 13:06:36',''),
	(188,3,31,30,'2016-04-27 13:06:36',''),
	(189,5,1,31,'2016-04-27 13:06:36',''),
	(190,4,2,31,'2016-04-27 13:06:36',''),
	(191,2,3,31,'2016-04-27 13:06:36',''),
	(192,3,4,31,'2016-04-27 13:06:36',''),
	(193,3,5,31,'2016-04-27 13:06:36',''),
	(194,3,30,31,'2016-04-27 13:06:36',''),
	(195,3,31,31,'2016-04-27 13:06:36','');

/*!40000 ALTER TABLE `qualityratings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;

INSERT INTO `roles` (`id`, `name`, `description`)
VALUES
	(1,'login','Login privileges, granted after account confirmation'),
	(2,'admin','Administrative user, has access to everything.'),
	(3,'dealer','this is a delaer');

/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table roles_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `roles_users`;

CREATE TABLE `roles_users` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `fk_role_id` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `roles_users` WRITE;
/*!40000 ALTER TABLE `roles_users` DISABLE KEYS */;

INSERT INTO `roles_users` (`user_id`, `role_id`)
VALUES
	(1,1),
	(2,1),
	(4,1),
	(5,1),
	(6,1),
	(7,1),
	(8,1),
	(9,1),
	(10,1),
	(11,1),
	(12,1),
	(13,1),
	(14,1),
	(15,1),
	(16,1),
	(17,1),
	(18,1),
	(19,1),
	(20,1),
	(21,1),
	(22,1),
	(23,1),
	(24,1),
	(25,1),
	(26,1),
	(27,1),
	(28,1),
	(29,1),
	(30,1),
	(31,1),
	(32,1),
	(33,1),
	(34,1),
	(35,1),
	(36,1),
	(37,1),
	(38,1),
	(39,1),
	(40,1),
	(41,1),
	(42,1),
	(43,1),
	(44,1),
	(45,1),
	(46,1),
	(47,1),
	(48,1),
	(49,1),
	(50,1),
	(51,1),
	(52,1),
	(53,1),
	(54,1),
	(55,1),
	(56,1),
	(57,1),
	(58,1),
	(59,1),
	(60,1),
	(61,1),
	(62,1),
	(63,1),
	(64,1),
	(65,1),
	(66,1),
	(67,1),
	(68,1),
	(69,1),
	(70,1),
	(71,1),
	(72,1),
	(73,1),
	(74,1),
	(75,1),
	(76,1),
	(77,1),
	(78,1),
	(79,1),
	(80,1),
	(81,1),
	(82,1),
	(83,1);

/*!40000 ALTER TABLE `roles_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table technicalspecs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `technicalspecs`;

CREATE TABLE `technicalspecs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(512) NOT NULL,
  `productcategory_id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `technicalspecs` WRITE;
/*!40000 ALTER TABLE `technicalspecs` DISABLE KEYS */;

INSERT INTO `technicalspecs` (`id`, `name`, `productcategory_id`, `timestamp`)
VALUES
	(1,'capacity',3,'2016-03-03 22:36:29');

/*!40000 ALTER TABLE `technicalspecs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table technicalspecvalues
# ------------------------------------------------------------

DROP TABLE IF EXISTS `technicalspecvalues`;

CREATE TABLE `technicalspecvalues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(256) NOT NULL,
  `technicalspec_id` int(11) NOT NULL,
  `productitem_id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `technicalspecvalues` WRITE;
/*!40000 ALTER TABLE `technicalspecvalues` DISABLE KEYS */;

INSERT INTO `technicalspecvalues` (`id`, `value`, `technicalspec_id`, `productitem_id`, `timestamp`)
VALUES
	(1,'200c',1,6,'2016-03-03 23:09:15');

/*!40000 ALTER TABLE `technicalspecvalues` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tickets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tickets`;

CREATE TABLE `tickets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` varchar(256) DEFAULT NULL,
  `subject` varchar(256) DEFAULT NULL,
  `isactive` int(11) DEFAULT '0',
  `date` datetime DEFAULT NULL,
  `tickettype_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tickets_tickettypes1_idx` (`tickettype_id`),
  KEY `fk_tickets_users1_idx` (`user_id`),
  CONSTRAINT `fk_tickets_tickettypes1` FOREIGN KEY (`tickettype_id`) REFERENCES `tickettypes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tickets_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tickettypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tickettypes`;

CREATE TABLE `tickettypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table user_tokens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_tokens`;

CREATE TABLE `user_tokens` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `user_agent` varchar(40) NOT NULL,
  `token` varchar(40) NOT NULL,
  `created` int(10) unsigned NOT NULL,
  `expires` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_token` (`token`),
  KEY `fk_user_id` (`user_id`),
  KEY `expires` (`expires`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `user_tokens` WRITE;
/*!40000 ALTER TABLE `user_tokens` DISABLE KEYS */;

INSERT INTO `user_tokens` (`id`, `user_id`, `user_agent`, `token`, `created`, `expires`)
VALUES
	(216,23,'f4055e68e22ed5e5def8846d58158ecb27400df6','0c4f4a79655369bd848952057f483965e78abfb4',1462334984,1463198984),
	(228,51,'47657a2a0f7fccf08c1aa979d5c1d9785aeadb5d','6f26e263dc0dbd5702d3629cc2ce820042b24b71',1462534775,1463398775);

/*!40000 ALTER TABLE `user_tokens` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table usernotifications
# ------------------------------------------------------------

DROP TABLE IF EXISTS `usernotifications`;

CREATE TABLE `usernotifications` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type_id` int(11) unsigned NOT NULL,
  `item_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `order_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `usernotifications` WRITE;
/*!40000 ALTER TABLE `usernotifications` DISABLE KEYS */;

INSERT INTO `usernotifications` (`id`, `type_id`, `item_id`, `user_id`, `date`, `order_id`)
VALUES
	(1,3,17,51,'2016-05-03 05:36:26',0),
	(2,1,16,51,'2016-05-03 05:40:50',0),
	(3,1,24,51,'2016-05-03 05:41:07',0),
	(4,2,16,51,'2016-05-03 05:41:14',0),
	(5,3,16,51,'2016-05-03 05:41:36',0),
	(6,1,20,51,'2016-05-03 23:08:09',43),
	(7,2,20,51,'2016-05-03 23:12:34',43),
	(8,3,20,51,'2016-05-03 23:13:28',43);

/*!40000 ALTER TABLE `usernotifications` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table usernotificationtypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `usernotificationtypes`;

CREATE TABLE `usernotificationtypes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `usernotificationtypes` WRITE;
/*!40000 ALTER TABLE `usernotificationtypes` DISABLE KEYS */;

INSERT INTO `usernotificationtypes` (`id`, `type`)
VALUES
	(1,'Quality Tested'),
	(2,'Shipped'),
	(3,'Delivered'),
	(4,'Cancelled'),
	(5,'Executed');

/*!40000 ALTER TABLE `usernotificationtypes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(256) DEFAULT NULL,
  `username` varchar(256) DEFAULT NULL,
  `password` varchar(256) DEFAULT NULL,
  `phonenumber` varchar(256) DEFAULT NULL,
  `firstname` varchar(512) NOT NULL,
  `lastname` varchar(512) NOT NULL,
  `picture` varchar(256) NOT NULL,
  `isdealer` int(11) NOT NULL DEFAULT '0',
  `location` varchar(512) NOT NULL,
  `locationlat` varchar(512) NOT NULL,
  `locationlng` varchar(512) NOT NULL,
  `place_id` varchar(512) NOT NULL,
  `deliveryradius` int(11) NOT NULL,
  `hash` varchar(256) DEFAULT NULL,
  `isverified` int(11) DEFAULT '0',
  `logins` int(11) DEFAULT NULL,
  `last_login` int(11) DEFAULT NULL,
  `isadmin` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `email`, `username`, `password`, `phonenumber`, `firstname`, `lastname`, `picture`, `isdealer`, `location`, `locationlat`, `locationlng`, `place_id`, `deliveryradius`, `hash`, `isverified`, `logins`, `last_login`, `isadmin`)
VALUES
	(22,'dealer@dealer.com','123','d3878714692d58aae32790db0f4b8ebc21bcd813a01219cec7d4746754f37989','123','Dealer_1','','none',1,'','','','',0,NULL,1,NULL,1462258278,0),
	(23,'admin@admin.com','1234','58cdd509b5d0aafc1df861610fc693dec351578a93acb54aa0453c93d4b0fefd','1234','Admin','Admin','none',0,'','','','',0,NULL,1,NULL,1462334984,1),
	(51,'vineethkumart@gmail.com','9538092344','e19ead0e6fd98035d3ee8082cda6f041565e0127e8058eacaec8178348b21512','9538092344','Vineethkumar','kumart','none',0,'','','','',0,NULL,1,NULL,1462534775,0),
	(79,'sooraj@gmail.com','9447394253','e19ead0e6fd98035d3ee8082cda6f041565e0127e8058eacaec8178348b21512','9447394253','Sooraj','Krishan','none',0,'','','','',0,NULL,1,NULL,1462268400,0),
	(80,'sandeep@gmail.com','1234567899','e19ead0e6fd98035d3ee8082cda6f041565e0127e8058eacaec8178348b21512','1234567899','Sanddep','Krishan','none',0,'','','','',0,NULL,1,NULL,1462268803,0),
	(81,'vineet@gmail.cm','1111111111','e19ead0e6fd98035d3ee8082cda6f041565e0127e8058eacaec8178348b21512','1111111111','Vineeth','Kumar','none',0,'','','','',0,NULL,0,NULL,NULL,0),
	(82,'shashi@gmail.com','4985361014','a4b2c56c0e82bdfec8c3548e3ababc0e162567c33631415232adf4354beccc44','4985361014','Shashi','Kuttan','none',0,'','','','',0,NULL,1,NULL,1462433657,0),
	(83,'sandeepkris2003@gmail.com','9496463697','b131a040161836eb506727d411ce042e1bbc72f23eccdacd751b8f3f89e838d6','9496463697','Unni','Kuttan','none',0,'','','','',0,NULL,1,NULL,1462463847,0);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wishlists
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wishlists`;

CREATE TABLE `wishlists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  `productitem_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `wishlists` WRITE;
/*!40000 ALTER TABLE `wishlists` DISABLE KEYS */;

INSERT INTO `wishlists` (`id`, `timestamp`, `user_id`, `productitem_id`)
VALUES
	(1,'2016-05-05 14:21:01',51,26),
	(3,'2016-05-05 14:22:14',51,20);

/*!40000 ALTER TABLE `wishlists` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
