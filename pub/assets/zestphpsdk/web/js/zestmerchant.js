'use strict';

(function($) {
  var zestMerchant = window.zestMerchant || (window.zestMerchant = {});
  // Initialization function to check the types
  zestMerchant.init = function(apiKey,loanAmmount,toggleExpanded,widgetDiv){
    zestMerchant.apiKey = null;
    zestMerchant.loanAmmount = 0;
    zestMerchant.toggleExpanded = false;
    zestMerchant.widgetDiv=widgetDiv;
    console.log(widgetDiv);
    if(typeof apiKey === 'string' ){
      zestMerchant.apiKey = apiKey;
    }
    if(typeof loanAmmount === 'number' ){
      zestMerchant.loanAmmount = loanAmmount;
    }
    if(typeof toggleExpanded !== 'boolean' ){
      zestMerchant.toggleExpanded = toggleExpanded;
    }
  };
  // Function to get the quotes of product.
  zestMerchant.getQuote = function(apiKey,loanAmmount,toggleExpanded,widgetDiv,auth_token){
    // replace the api key and loan ammount and initialize if params are provided.
    //console.log(loanAmmount);
    if (apiKey && loanAmmount && widgetDiv){
      zestMerchant.init(apiKey,loanAmmount,toggleExpanded,widgetDiv);
    }
    // Get the data from webservice for emi and quotes
    //'http://staging.zestmoney.in:8082/quote?apiKey=

    if(zestMerchant.apiKey && zestMerchant.loanAmmount>0){
      $.ajax({
         // url: 'http://staging.zestmoney.in:8082/quote?MerchantId='+zestMerchant.apiKey+'&LoanAmount='+zestMerchant.loanAmmount,
          url: emiUrl+'?MerchantId='+zestMerchant.apiKey+'&LoanAmount='+zestMerchant.loanAmmount,
          type: 'GET',
          headers: { 'Authorization': auth_token.token_type+' '+auth_token.access_token },
          success: function(data){
            zestMerchant.quotes = data.Quotes;
            zestMerchant.makeWidget();
          },
          error: function(data) { 
              console.log(data); 
        //zestMerchant.makeErrorWidget();
              // $(zestMerchant.widgetDiv).html(data.statusText);
          }
      });
      // $.get().done(function (data) {
      //
      // });
    }

  };

//function to make widget html for Error inside the div.
  zestMerchant.makeErrorWidget= function(){
    $(zestMerchant.widgetDiv).html('<span class="widgetHeader" onClick="zestMerchant.toggleAccordion();"></span>');
    zestMerchant.makeWidgetHeader();
    $(zestMerchant.widgetDiv).append('<div id="togglecont"><ul id="panel"></ul></div>');
    var bodyHtml = 'ZestMoney EMI is not applicable for this product.';
    $(zestMerchant.widgetDiv+' #panel').append(bodyHtml);
    if(!zestMerchant.toggleExpanded){
        $(zestMerchant.widgetDiv+' #togglecont').css('display','none');
      }
  };

  // function to make widget html inside the div.
  zestMerchant.makeWidget= function(){
    $(zestMerchant.widgetDiv).html('<span class="widgetHeader" onClick="zestMerchant.toggleAccordion();"></span>');
    //$(zestMerchant.widgetDiv).html('<span class="widgetHeader"></span>');
    zestMerchant.makeWidgetHeader();
    zestMerchant.makeBody();
  };
  // function to make widget header html which will be swapped based on toggleHeader
  zestMerchant.makeWidgetHeader = function(){
    var headerhtml = '<span style="display: inline-block;border: 1px solid #000000;padding: 5px;border-radius: 3px;font-weight: 600;margin-right: 5px;">EMI</span><span class="cardless">Cardless EMI from <span style="font-size:20px; color:#20a797"><span style="font-size:14px">&#x20B9;</span>'+zestMerchant.quotes[zestMerchant.quotes.length-1].MonthlyInstallment+'</span></span><a href="javascript:void(0);" onClick="zestMerchant.toggleAccordion(); id="toggleHeader">View Plans</a>';
    $(zestMerchant.widgetDiv+' .widgetHeader').html(headerhtml);
    //  headerhtml = '<h5><img src="'+logourl+'logo.svg" width="22"> <span class="cardless">Cardless EMI from &#x20B9; '+zestMerchant.quotes[zestMerchant.quotes.length-1].MonthlyInstallment+'</span><a href="javascript:void(0);" id="toggleHeader">View Plans</a></h5>';
    //$(zestMerchant.widgetDiv+' .widgetHeader').html(headerhtml);
  };
  // function to make body of widget, the panel will be hidden if toggleHeader is false.
  zestMerchant.makeBody = function(){
    $(zestMerchant.widgetDiv).append('<div id="togglecont"><div id="panel"></div></div>');
    var bodyHtml = '';
    bodyHtml = bodyHtml + '<div class="panel panel-default" style="margin-top: 10px"><div class="panel-heading table-responsive">Monthly EMI Chart</div><div class="panel-body">';
    bodyHtml = bodyHtml + '<table class="table"><thead><tr><th>Tenure</th><th>EMI</th></tr></thead><tbody>';
    zestMerchant.quotes.forEach(function(quote){
      //bodyHtml = bodyHtml+'<li><span>'+quote.IntallmentCount+' months</span> &nbsp;&nbsp;&nbsp; &#x20B9;'+quote.MonthlyInstallment+'/month</li>';
      bodyHtml = bodyHtml+'<tr><td>'+quote.IntallmentCount+' months</td><td>&#x20B9;'+quote.MonthlyInstallment+'/month</td></tr>';
    });
    //bodyHtml = bodyHtml+'<li class="stext">Or Design your own EMI Plan</li>';
    bodyHtml = bodyHtml+'<tr><td>Or Design your own EMI Plan</td> <td></td> </tr>';
    bodyHtml = bodyHtml + '</tbody></table>';
    bodyHtml = bodyHtml +'<p class="emisel">Select "Cardless EMI" at Checkout</p>';
    bodyHtml = bodyHtml + '<span style="color: grey;" class="pull-right"><i>Powered by <img src="'+logourl+'logo.svg" width="22"><span style="font-weight:600"> ZestMoney</span></i></span>';
    bodyHtml = bodyHtml + '</div></div>';
    $(zestMerchant.widgetDiv+' #panel').append(bodyHtml);
    //var bottomtext = '<h5><span class="bottomtext">Select ZestMoney at Checkout</span></h5>';
    //$(zestMerchant.widgetDiv+' #togglecont').append(bottomtext);
    if(!zestMerchant.toggleExpanded){
      $(zestMerchant.widgetDiv+' #togglecont').css('display','none');
    }
  };
  // function to handle the accordion toggle functionality.
  zestMerchant.toggleAccordion = function(){
    $(zestMerchant.widgetDiv+' #togglecont').slideToggle('slow',function(){
      if(zestMerchant.toggleExpanded){
        zestMerchant.toggleExpanded = false;
        zestMerchant.makeWidgetHeader();
      }
      else{
        zestMerchant.toggleExpanded = true;
        zestMerchant.makeWidgetHeader();
      }
    });
    return false;
  };

})(jQuery);
