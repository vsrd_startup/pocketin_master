'use strict';

(function($) {
  var zestMerchant = window.zestMerchant || (window.zestMerchant = {});
  // Initialization function to check the types
  zestMerchant.init = function(apiKey,loanAmmount,toggleExpanded,widgetDiv){
    zestMerchant.apiKey = null;
    zestMerchant.loanAmmount = 0;
    zestMerchant.toggleExpanded = true;
    zestMerchant.widgetDiv=widgetDiv;
    if(typeof apiKey === 'string' ){
      zestMerchant.apiKey = apiKey;
    }
    if(typeof loanAmmount === 'number' ){
      zestMerchant.loanAmmount = loanAmmount;
    }
    if(typeof toggleExpanded !== 'boolean' ){
      zestMerchant.toggleExpanded = toggleExpanded;
    }
  };
  // Function to get the quotes of product.
  zestMerchant.getQuote = function(apiKey,loanAmmount,toggleExpanded,widgetDiv,downpaymentamount = 0){
    // replace the api key and loan ammount and initialize if params are provided.
    if (apiKey && loanAmmount && widgetDiv){
      zestMerchant.init(apiKey,loanAmmount,toggleExpanded,widgetDiv);
    }
    // Get the data from webservice for emi and quotes
    //'http://staging.zestmoney.in:8082/quote?apiKey=
     if(downpaymentamount != 0){
             var ajaxurl =  emiUrl+'?MerchantId='+zestMerchant.apiKey+'&LoanAmount='+zestMerchant.loanAmmount+'&DownpaymentAmount='+downpaymentamount;
          }else{
             var ajaxurl = emiUrl+'?MerchantId='+zestMerchant.apiKey+'&LoanAmount='+zestMerchant.loanAmmount;
          }

    if(zestMerchant.apiKey && zestMerchant.loanAmmount>0){
      $.ajax({
         // url: 'http://staging.zestmoney.in:8082/quote?MerchantId='+zestMerchant.apiKey+'&LoanAmount='+zestMerchant.loanAmmount,
          url: ajaxurl,
          type: 'GET',
          success: function(data){  
            zestMerchant.quotes = data.Quotes;
            zestMerchant.makeWidget();
            $('.zest-modal-title').html("Pay just Rs."+ (Number(data.DownpaymentAmount) + Number(data.ProcessingFee))+"/- &amp; rest in EMIs");
            $('span.zest-bold.first').html('Rs.'+(Number(data.DownpaymentAmount) + Number(data.ProcessingFee))+'/-');
            $('span.zest-bold.second').html('Rs.'+data.Quotes.last().MonthlyInstallment+'/-');
          },
          error: function(data) { 
              console.log(data); //or whatever second
          }
      });
    }

  };
  // function to make widget html inside the div.
  zestMerchant.makeWidget= function(){
    $(zestMerchant.widgetDiv).html('<span class="widgetHeader" onClick="zestMerchant.toggleAccordion();"></span>');
    zestMerchant.makeWidgetHeader();
    zestMerchant.makeBody();
  };
  // function to make widget header html which will be swapped based on toggleHeader
  zestMerchant.makeWidgetHeader = function(){
    var headerhtml = '';

    if (zestMerchant.toggleExpanded){
      headerhtml = '<h5><a href="javascript:void(0);" class="active" id="toggleHeader" > <img src="'+baseSecureUrl+'logo.svg" width="22"> <span>  ZestMoney lets you create an EMI without a credit card. No hidden fees. Instant online approval. Down payment required.</span></a></h5>';

    }
    else{
      headerhtml = '<h5><a href="javascript:void(0);" id="toggleHeader"> <img src="'+baseSecureUrl+'logo.svg" width="22"> <span>Pay with Zest as little as &#x20B9; '+zestMerchant.quotes[zestMerchant.quotes.length-1].MonthlyInstallment+'/month</span></a></h5>';
    }
    $(zestMerchant.widgetDiv+' .widgetHeader').html(headerhtml);
  };
  // function to make body of widget, the panel will be hidden if toggleHeader is false.
  zestMerchant.makeBody = function(){
    $(zestMerchant.widgetDiv).append('<div id="togglecont"><ul id="panel"></ul></div>');
    var bodyHtml = '';
    var bodyHtml_result = '';
    zestMerchant.quotes.forEach(function(quote){
      bodyHtml = bodyHtml+'<th>'+quote.IntallmentCount+' months</th>';
      bodyHtml_result = bodyHtml_result + '<td> Rs.'+quote.MonthlyInstallment+'</td>';  
    });
    bodyHtml = bodyHtml+'<th rowspan="2" class="zest-cell-1">Or Design your own EMI Plan</th>';
    if($('table.zest-emi tbody tr').length == 0){
      $('table.zest-emi tbody').append("<tr><th>Tenure</th>"+bodyHtml+"</tr>");
      $('table.zest-emi tbody').append("<tr><th>EMI</th>"+bodyHtml_result+"</tr>");
    }
    if(!zestMerchant.toggleExpanded){
      $(zestMerchant.widgetDiv+' #togglecont').css('display','none');
    }
  };
  // function to handle the accordion toggle functionality.
  zestMerchant.toggleAccordion = function(){
    $(zestMerchant.widgetDiv+' #togglecont').slideToggle('slow',function(){
      if(zestMerchant.toggleExpanded){
        zestMerchant.toggleExpanded = false;
        zestMerchant.makeWidgetHeader();
      }
      else{
        zestMerchant.toggleExpanded = true;
        zestMerchant.makeWidgetHeader();
      }
    });
    return false;
  };

  zestMerchant.linkwidget = function(siteurl,price,page,downpaymentamount = 0){
      if(downpaymentamount != 0){
             var linkajaxurl =  siteurl+'zestmoney/zestpay/linkwidget?price='+price+'&page='+page+'&downpaymentamount='+downpaymentamount;
          }else{
              var linkajaxurl =  siteurl+'zestmoney/zestpay/linkwidget?price='+price+'&page='+page;
          }
      $.ajax({
              url: linkajaxurl,
              type: 'GET',
              success: function(data){
                console.log(data);
                document.getElementById('zest-modal-btn').innerHTML = data;
              },
              error: function(data) { 
                  console.log(data); 
              }
          });
  };
})(jQuery);
