var adminApp = angular.module('piadmin', ['ngCookies']);

adminApp.service("authenticationService", ['$window', function($window) {
	this.GUESTAPIKEY = "pocketin_guest_api_key";
	$window.sessionStorage["pocketin_admin_info"] = null;
	this.loggeduser = angular.fromJson($window.sessionStorage["pocketin_admin_info"]) || null;

	this.loginUser = function(user) {
		this.loggeduser = user;
		//$cookieStore.put('loggeduser', this.loggeduser);
		$window.sessionStorage["pocketin_admin_info"] = angular.toJson(user);
	}

	this.logoutuser = function() {
		this.loggeduser = null;
		$window.sessionStorage["pocketin_admin_info"] = null;
	}

	this.getApiKey = function() {
		if (this.loggeduser == null) {
			return this.GUESTAPIKEY;
		}
		return this.loggeduser.token;
	}

	this.userLogged = function() {
		if (this.loggeduser == null) {
			return 0;
		}
		if ($window.sessionStorage["pocketin_admin_info"] == null) {
			return 0;
		}

		return 1;
	}
}]);


adminApp.service("ServerCallService", ['authenticationService', '$http', '$q', '$timeout', '$rootScope',  function(authenticationService, $http, $q, $timeout, $rootScope) {
	this.MakeServerCall = function(url, method, param) {
		$rootScope.xhr = 1;
		// Add the API key of the logged in User
		param.apikey = authenticationService.getApiKey();
		var deferred = $q.defer();
		$http({
			url: url,
			method: method,
			params: param,
		}).then(function(response) {
			var status = 1;
			if (response.data.list && response.data.list.status != undefined) {
				status = response.data.list.status;
			}
			var op = {
				status: status,
				data: response.data.list
			};
			$rootScope.xhr = 0;
			//$timeout(function(){
			//    deferred.resolve(op);
			//},2000);
			deferred.resolve(op);
		}, function(response) {
			var op = {
				status: -1,
				data: undefined
			}
			$rootScope.xhr = 0;
			deferred.resolve(op);
		});
		return deferred.promise;
		//return $q.when("Hello World!");
	}
}]);


adminApp.controller('AdminController', function($q, $http, $scope, ServerCallService, authenticationService, $window, $rootScope, $timeout) {
	$scope.isticketshow = 0;
	$rootScope.xhr = 0;
	$scope.adminLogged = false;
	$scope.modelloaded = false;
	$scope.model = {};
	$scope.modelopdone = false;
	ctrl = this;
	$scope.superuser = 0;

	$scope.loadList = function(custom) {
		var deferred = $q.defer();
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'GET', {
				page_no: 1,
				limit: 100,
				method: 'getAllProducts',
				custom: custom,
			})
			.then(function(response) {
				deferred.resolve(response.data);
			});
		return deferred.promise;
	}

	$scope.LoadPending = function() {
		$scope.loadList("pendingapproval").then(function(list) {
			$scope.pa_list = list;
			console.log(list);
		});
	}

	$scope.loadAll = function(op) {
		$scope.loadList("all").then(function(list) {
			$scope.all_list = list;
			console.log(list);
		});
	}

	// Get the User Details from the Server
	ServerCallService.MakeServerCall('../v2/Apiv2/Authentication', 'GET', {
			method: 'whoami',
			role: 'admin',
		})
		.then(function(op) {
			op = op.data;
			console.log(op);
			if (op.status == 1) {
				authenticationService.loginUser({
					name: op.firstname + " " + op.lastname,
					firstname: op.firstname,
					mobile: op.mobile,
					token: op.token,
					user_id: op.user_id
				});
				if (authenticationService.loggeduser) {
					$scope.dealername = authenticationService.loggeduser.firstname;
					$scope.adminLogged = true;
					/*
					$scope.loadList("pendingdelivery").then(function(list) {
						$scope.pd_list = list;
					});

					$scope.loadList("pendingpayment").then(function(list) {
						$scope.pp_list = list;
					});

					$scope.loadList('soldandpaid', 0).then(function(list) {
						$scope.sp_list = list;
					});*/

					if (authenticationService.loggeduser.firstname == "Admin") {
					$scope.superuser = 1;
				}

				}
			} else {
				authenticationService.logoutuser();
			}
		});


	$scope.subcat = -1;
	$scope.subcats = [{
		name: "---Please select Main Category---",
		id: "-1"
	}];

	ServerCallService.MakeServerCall('../v2/Apiv2/User', 'GET', {
			method: 'getParentCategories',
		})
		.then(function(response) {
			$scope.maincats = response.data;
		});

	$scope.getSubCat = function() {
		if ($scope.maincat == "-1") {
			$scope.subcat = -1;
			$scope.subcats = [{
				name: "---Please select Main Category---",
				id: "-1"
			}];
		} else {
			ServerCallService.MakeServerCall('../v2/Apiv2/User', 'GET', {
					method: 'getProducttCategoriesByParentID',
					parentcat_id: $scope.maincat,
				})
				.then(function(response) {
					$scope.subcats = response.data;
					$scope.subcat = $scope.subcats[0].id;
					//console.log($scope.subcats);
					$scope.getQualityValues();
				});
		}
	};

	$scope.addNewProduct = function() {
		var qvs = [];
		angular.forEach($scope.quality, function(q) {
			var qv = {};
			qv.index = q.qid;
			qv.value = $scope.qv[q.qid];
			qvs.push(qv);
		})
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'POST', {
				method: 'addProductByDealer',
				maincat: $scope.maincat,
				productcategory_id: $scope.subcat,
				name: $scope.productname,
				saleprice: $scope.saleprice,
				manufacturingyear: $scope.year,
				pid: ctrl.pid,
				qualityindexes: angular.toJson(qvs),
				user_id: authenticationService.loggeduser.user_id,
			})
			.then(function(response) {
				//console.log(response);
			});


	}

	$scope.Login = function() {
		ServerCallService.MakeServerCall('../v2/Apiv2/Authentication', 'POST', {
				method: 'login',
				isadmin: 1,
				username: $scope.username,
				password: $scope.password
			})
			.then(function(response) {
				//console.log(response);
				if (response.status == 1 && response.data.status == 1) {
					//$scope.message = "Successfully logged in";
					// Make the fields disabled
					authenticationService.loginUser({
						firstname: response.data.profile.firstname,
						mobile: response.data.profile.phonenumber,
						token: response.data.token,
						user_id: response.data.profile.user_id
					});
					$scope.dealername = authenticationService.loggeduser.firstname;
					$scope.adminLogged = true;
					if (authenticationService.loggeduser) {
					$scope.dealername = authenticationService.loggeduser.firstname;
					$scope.adminLogged = true;
					ServerCallService.MakeServerCall('../v2/Apiv2/User', 'GET', {method: 'getUserOrders'}).
					then(function(op) {
					    if (op.status == 1) {
					        $scope.orders = op.data.orders;
					    } else {
					        console.log("API call failed");
					    }
					});

					/*
					$scope.loadList("pendingdelivery").then(function(list) {
						$scope.pd_list = list;
					});

					$scope.loadList("pendingpayment").then(function(list) {
						$scope.pp_list = list;
					});

					$scope.loadList('soldandpaid', 0).then(function(list) {
						$scope.sp_list = list;
					});
					*/

				}
				} else {
					$scope.error = response.data.message;
				}
			});
	}

	$scope.logout = function() {
		ServerCallService.MakeServerCall('../logout', 'GET', {})
        .then(function(op) {
            authenticationService.logoutuser();
			$scope.dealername = "";
			$scope.adminLogged = false;
        });
	}

	$scope.LoadModel = function() {
		//var modelno = $scope.modelnumber;
		//$scope.model = {}
		//$scope.model.number = modeno;
		$scope.modelloaded = true;
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'GET', {
				method: 'getModel',
				'modelnumber': $scope.model.number
			})
			.then(function(response) {
				//console.log(response);
				if (response.status == 1 && response.data.status == 1) {
					$scope.message = "Model found";
					$scope.model = response.data;
					console.log($scope.model);
					$scope.model.action = "Modify Model";
					// Make the fields disabled
				} else {
					$scope.message = "Model Not found. Create a new model";
					var mn = $scope.model.number;
					$scope.model = {};
					$scope.model.number = mn;
					$scope.model.action = "Create Model";
				}
			});
	}

	$scope.CreateOrModifyModel = function() {
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'POST', {
				method: 'CreateOrModifyModel',
				'modelnumber': $scope.model.number,
				'name': $scope.model.name,
				'shortdescription': $scope.model.shortdescription,
				'description': $scope.model.description,
				'mrp': $scope.model.mrp,
			})
			.then(function(response) {
				//console.log(response);
				if (response.status == 1 && response.data.status == 1) {
					$scope.modelopdone = true;
					$scope.modelloaded = false;
					$scope.model.id = response.data.modelid;
					// Make the fields disabled
				} else {
					$scope.model.action = "Failed";
				}
			});
	}
	$scope.ApproveProduct = function(item) {
		var isfactory = (item.isfactory == true || item.isfactory == 1) ? 1 : 0;
		console.log(isfactory);
		console.log("Approve");
		console.log(item.warehouseid);
		item.approving = 1;
		item.success = 0;
		item.failed = 0;
		item.invalidid = 0;
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'POST', {
				method: 'markAsApproved',
				'productitem_id': item.productitem_id,
				'title': item.product_title,
				'description': item.product_description,
				'saleprice': item.saleprice,
				'mrp': item.mrp,
				isfactory: isfactory,
				'longdescription': item.longdescription,
				'technicalspec': item.technicalspec,
				'conditionspec': item.conditionspec,
				'warehouseid':item.warehouseid,
				'user_id': authenticationService.loggeduser.user_id
			})
			.then(function(response) {
				if (response.status == 1) {
					item.success = 1;
					item.approving = 0;
				} else if (response.status == 9){
					item.invalidid = 1
					item.failed = 1;
				}
				 else {
					item.failed = 1;
					console.log(response);
				}
			},
			function(op){
				console.log(op);
				item.failed = 1;
			});
	}
	$scope.ModifyProduct = function(item) {
		var isfactory = (item.isfactory == true || item.isfactory == 1) ? 1 : 0;
		item.approving = 1;
		item.success = 0;
		item.failed = 0;
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'POST', {
				method: 'updateProductItem',
				'productitem_id': item.productitem_id,
				'title': item.product_title,
				'description': item.product_description,
				'saleprice': item.saleprice,
				'mrp': item.mrp,
				'isfactory': isfactory,
				'longdescription': item.longdescription,
				'technicalspec': item.technicalspec,
				'conditionspec': item.conditionspec,
				'user_id': authenticationService.loggeduser.user_id
			})
			.then(function(response) {
				if (response.status == 1) {
					item.success = 1;
					item.approving = 0;
				} else {
					item.failed = 1;
					console.log(response);
				}
			},
			function(op){
				console.log(op);
				item.failed = 1;
			});
	}


	$scope.performAction = function(action, item) {
		item.disable = 1;
		item.action = "Please wait...";
		if (action == "Order Executed" || action == "Cacellation Processed") {
			return;
		}
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'POST', {method: 'performAction', action: action, id: item.id})
		.then(function(op) {
			if (op.status == 1) {
				item.action = op.data.action;
				item.status = op.data.status;
				item.disable = 0;
			} else {
				item.action = "Failed";
				item.disable = 1;
				console.log(op);
			}
		})
	}

	$scope.performDelAction = function(action, item) {
		console.log("delaction");
		console.log(item);
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'POST', {method: 'performAction', action: action, id: item.suborder})
		.then(function(op) {
			if (action == "Mark Shipped") {
				console.log("shipped");
				item.shipped = 1
			} else if (action == "Mark Delivered"){
				console.log("delvrd");
				item.delivered = 1;
			}
		})
	}

	$scope.VerifyDelivery = function(item) {
		console.log("verify");
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'POST', {method: 'performAction', action: 'Delivery Verified', id: item.orderid})
		.then(function(op) {
			console.log("done");
			item.verified = 1;
		})
	}

	$scope.loadCart = function(id) {
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'GET', {method: 'getUserCart'})
		.then(function(op){
			console.log(op);
			if (op.status == 1){
				$scope.cart = op.data.cart;
				console.log($scope.cart);
			}

		});
	}

	$scope.saveCartComment = function(item) {
		console.log(item);
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'POST', {method: 'saveCartComment', id:item.cartid, comment: item.comment})
		.then(function(op){
			console.log(op);
			if (op.status == 1){
				console.log("done");
			}

		});
	}

	$scope.saveWishlistComment = function(item) {
		console.log(item);
		console.log("Save comment");
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'POST', {method: 'wishListSave', id:item.id, comment: item.comment})
		.then(function(op){
			console.log(op);
			if (op.status == 1){
				console.log("done");
			}

		});
	}

	$scope.loadWishlist = function(id) {
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'GET', {method: 'getUserWishlist'})
		.then(function(op){
			console.log(op);
			if (op.status == 1){
				$scope.wish = op.data.wish;
			}
		});
	}

	$scope.loadUserSeen = function(id) {
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'GET', {method: 'loadUserSeen'})
		.then(function(op){
			console.log(op);
			if (op.status == 1){
				$scope.seen = op.data.seen;
			}
		});
	}
	$scope.loadHotness = function(id) {
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'GET', {method: 'loadProductHotness'})
		.then(function(op){
			console.log(op);
			if (op.status == 1){
				$scope.hotness = op.data.hotness;
			}
		});
	}

	$scope.dormant = 0;
	$scope.loadCustOrder = function() {
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'GET', {method: 'getFinishedOrders', type: 'active'}).
		then(function(op) {
		    if (op.status == 1) {
		    	console.log(op);
		        $scope.corders = op.data.orders;
		        $scope.corders_copy = $scope.corders;
		        if ($scope.corders.isdormant == 1)
		        	{$scope.dormant= 1;}
		    } else {
		        console.log("API call failed");
		    }
		});
	}
	$scope.commentdone = "";
	$scope.saveComment = function(oid, cmnt) {
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'POST', {method: 'updateOrderComment', orderid: oid, comment:cmnt}).
		then(function(op) {
		    if (op.status == 1) {
		    	$scope.commentdone = "Saved";
		    } else {
		    	$scope.commentdone = "Failed";
		    }
		});
	}
	$scope.slotbooking = 0;
	$scope.alreadybookedorder=0;
	$scope.bookDeliverySlot = function(order) {
		$scope.slotbooking = 1;
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'POST', {method: 'bookDeliverySlot', orderid: order.order_id, d2: order.s2, t2: order.t2}).
		then(function(op) {
		    if (op.status == 1) {
		    	// order.slot.date1 = order.s1;
		    	// order.slot.time1 = order.t1;
		    	order.slot.date = order.s2;
		    	order.slot.time = order.t2;
		    	$scope.slotbooking = 0;
		    	order.slotmodify = 0;
		    	order.slotbooked = 1;
		    } else if(op.status == 9) {
		    	// slot already booked
		    	$scope.slotmodify = 0;
		    	$scope.slotbooking = 0;
		    	$scope.alreadybookedorder=op.data.alreadybookedorder;
		    			    	console.log($scope.alreadybookedorder);

		    }
		});
	}


	$scope.searchOrder = function() {
		console.log("here");
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'GET', {method: 'searchOrders', scat: $scope.scat, stext: $scope.stext}).
		then(function(op) {
		    if (op.status == 1) {
		    	console.log(op);
		    	$scope.corders = op.data.orders;
		        $scope.corders_copy = $scope.corders;
		    } else {
		        console.log("API call failed");
		    }});
	}

	$scope.searchProduct = function() {
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'GET', {method: 'searchProducts', product_id: $scope.pid}).
		then(function(op) {
		    if (op.status == 1) {
		    	$scope.all_list = op.data;
		    } else {
		        console.log("API call failed");
		    }});
	}

	$scope.ZestDeliver = function(order) {
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'GET', {method: 'deliverZestEmi', order: order}).
		then(function(op) {
		    if (op.status == 1) {
		    	console.log("done");
		    	console.log(op);
		    } else {
		        console.log("API call failed");
		    }});
	}
	$scope.getEmiStatus = function(order) {
		console.log("here");
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'GET', {method: 'getZestEMIStatus', orderid: order.order_id}).
		then(function(op) {
		    if (op.status == 1) {
		    	console.log("done");
		    	order.showemistatus = 1;
		    	order.emistatus = op.data.emi;
		    } else {
		        console.log("API call failed");
		    }});
	}
	$scope.ZestCancel = function(order) {
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'GET', {method: 'cancelZestEmi', order: order}).
		then(function(op) {
		    if (op.status == 1) {
		    	console.log("done");
		    	console.log(op);
		    } else {
		        console.log("API call failed");
		    }});
	}
	$scope.ZestRefund = function(order, amount) {
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'GET', {method: 'refundZestEmi', order: order, amount:amount}).
		then(function(op) {
		    if (op.status == 1) {
		    	console.log("done");
		    	console.log(op);
		    } else {
		        console.log("API call failed");
		    }});
	}

	$scope.Filterme = function(id) {
		if (id == 0) {
			$scope.corders = $scope.corders_copy;
		} else if (id == 1) {
			// Get All orders
			ServerCallService.MakeServerCall('../v2/Apiv2/User', 'GET', {
				method: 'getFinishedOrders',
				type: 'delivered'
			}).
			then(function(op) {
				if (op.status == 1) {
					console.log(op);
					$scope.corders = op.data.orders;
				} else {
					console.log("API call failed");
				}
			});

		} else if (id == 2) {
			// Get All orders
			ServerCallService.MakeServerCall('../v2/Apiv2/User', 'GET', {
				method: 'getFinishedOrders',
				type: 'cancelled'
			}).
			then(function(op) {
				if (op.status == 1) {
					console.log(op);
					$scope.corders = op.data.orders;
				} else {
					console.log("API call failed");
				}
			});
		}

		/*
		$scope.corders = [];
		if (id == 1) {
			angular.forEach($scope.corders_copy, function(o) {
				angular.forEach(o.items, function(item) {
					if (item.status == 'Order Placed') {
						$scope.corders.push(o);
					}
				})
			})

		} else if (id == 2) {
			angular.forEach($scope.corders_copy, function(o) {
				angular.forEach(o.items, function(item) {
					if (item.status == 'Order Confirmed') {
						$scope.corders.push(o);
					}
				})
			})

		} else if (id == 3) {
			angular.forEach($scope.corders_copy, function(o) {
				angular.forEach(o.items, function(item) {
					if (item.status == 'Shipped') {
						$scope.corders.push(o);
					}
				})
			})
		} else if (id == 4) {
			angular.forEach($scope.corders_copy, function(o) {
				angular.forEach(o.items, function(item) {
					if (item.status == 'Delivered') {
						$scope.corders.push(o);
					}
				})
			})
		} else if (id == 4) {
		} else if (id == 0) {
			$scope.corders = $scope.corders_copy;
		}*/
 	}

 	$scope.resizeImages = function() {
 		console.log("here");
 		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'POST', {method: 'resizeImages', id: $scope.prodid})
		.then(function(op){
			console.log(op);
			if (op.status == 1){
				$scope.imstatus = "Done";
			} else {
				$scope.imstatus = "Failed";
			}
		});
 	}

 	$scope.cancelOrderItem = function(order, item) {
        ServerCallService.MakeServerCall('../v2/Apiv2/User', 'POST', {method: 'cancelOrder', order_id: order, 'item_id': item.id})
        .then(function(op) {
            //console.log("done");
            if (op.status == 1) {
            	item.canstatus = "Done";
            } else {
            	item.canstatus = "Failed";
            }
        })
    }
    $scope.tot=0;
    $scope.checkReferral = function(order) {
        ServerCallService.MakeServerCall('../v2/Apiv2/User', 'GET', {method: 'checkReferral', order_id: order})
        .then(function(op) {
            console.log("Referral");
            console.log(op);
            if (op.status == 1) {
            console.log("Done");
            $scope.referrals = op.data;
            $scope.discount = 0;
        	angular.forEach($scope.referrals, function(r) {
            if (r.ruser_id != null && $scope.discount < 500 && r.availed == 0) {
                $scope.discount += 100;
            }
        })}else{
        		console.log("Failed")
        	}
        })
    }

    $scope.hideProductFromCustomer = function(item) {
    	console.log("here");
    	ServerCallService.MakeServerCall('../v2/Apiv2/User', 'POST', {method: 'HideProduct', id: item.productitem_id})
        .then(function(op) {
        	console.log(op);
            //console.log("done");
            item.hidden = 1;
            if (op.status == 1) {
            	item.hidestatus = "Hidden";
            } else {
            	item.hidestatus = "Failed";
            }
        })
    }



    $scope.unHideProductFromCustomer = function(item) {
    	console.log("Unhide");
    	ServerCallService.MakeServerCall('../v2/Apiv2/User', 'POST', {method: 'UnHideProduct', id: item.productitem_id})
        .then(function(op) {
        	
            console.log("Unhide done");
            item.hidden = 0;
            if (op.status == 1) {
            	item.hidestatus = "Hide Product from Customer";
            } else {
            	item.hidestatus = "Failed";
            }
        })
    }

     $scope.markSold = function(item) {
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'POST', {method: 'markSold', id: item.productitem_id})
		.then(function(op){
			if (op.status == 1) {
				item.sold = 1;
			}
		});
	}

	 $scope.markUnSold = function(item) {
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'POST', {method: 'markUnSold', id: item.productitem_id})
		.then(function(op){
			if (op.status == 1) {
				item.unsold = 1;
				item.sold = 0;
			}
		});
	}



	$scope.saveTicketComment = function(tid, cmnt, cmntr, comments) {
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'POST', {method: 'addTicketComment', ticket_id: tid, commenter:cmntr, comment:cmnt}).
		then(function(op) {
			comments.push(op.data);
			$scope.dticket.status = "Processing"
			$scope.comment = "";
		});
	};


	$scope.currentticketpage = 0;
	$scope.getAllTickets = function() {
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'GET', {method: 'getAllTickets', page: $scope.currentticketpage})
			.then(function(response) {
				$scope.tickets = response.data;
				if ($scope.tickets.length == 0) {
					$scope.tickets = null;
				} else {
					$scope.showpagination = 1;
					console.log($scope.tickets);
				}


			});
	};

	$scope.getPendingTickets = function(){
		$scope.cmntr = authenticationService.loggeduser.firstname;
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'GET', {method: 'getPendingTickets'})
		.then(function(response) {
			$scope.tickets = response.data;
			if ($scope.tickets.length == 0) {
				$scope.tickets = null;
			} else {
				$scope.showpagination  = 0;
				console.log($scope.tickets);
			}

		});
	};

	$scope.addTicket = function() {
		$scope.newticket = 1;
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'POST', {
			method: 'addTicket',
			order_id: $scope.neworderid,
			user_id: $scope.neworder.user_id,
			commenter: $scope.neworder.username,
			subject: $scope.subject,
			location: $scope.location,
			comment: $scope.comment,
			}).then(function(response) {
    			$timeout(function() {$scope.newticket = 0;}, 2000);
				$scope.newticket = 2;
				$scope.newtick.$setPristine();
				$scope.neworderid= "";
				$scope.neworder = null;
				$scope.subject = "";
				$scope.comment = "";
				$scope.location = "";
			});
	};


	$scope.expenseadded = 0;
	$scope.addRepairExpense = function(ticket) {
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'POST', {
			method: 'addRepairExpense',
			ticket_id: ticket,
			rexpense: $scope.rexpense
			}).then(function(response) {
				$scope.expenseadded = 1;
				$timeout(function() {$scope.expenseadded = 0;}, 2000);
				$scope.rexpense = "";
			});
	};



	$scope.showTicketDetails = function(ticket) {
		//$scope.detailshow = 1;
		$scope.dticket = ticket;
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'GET', {method: 'getAllComments', ticket_id: ticket.id})
    	.then(function(response) {
			$scope.comments = response.data;
			ServerCallService.MakeServerCall('../v2/Apiv2/User', 'GET', {method: 'getOrderDetails', order_id: ticket.order_id})
			.then(function(response) {
				$scope.order = response.data.order;
				console.log($scope.order);
			});
			$scope.showtdetails = 1;
		});
	}
	$scope.setTicketStatus = function() {
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'POST', {method: 'setTicketStatus', ticketid: $scope.dticket.id, status: $scope.dticket.nextstatus, tech: $scope.tech})
		.then(function(response) {
			$scope.dticket.tech = $scope.tech;
			$scope.dticket.status = $scope.dticket.nextstatus;
			$scope.dticket.nextstatus = response.data.nextstatus;
		});
	}


	$scope.getOrderDetails = function(oid) {
		console.log("details");
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'GET', {method: 'getOrderDetails', order_id: oid})
		.then(function(response) {
			$scope.neworder = response.data.order;
			console.log($scope.neworder);
		});
	}

//Customer call begins here

$scope.getCallerStats = function() {
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'GET', {method: 'getCallStats'})
		.then(function(response) {
			if (response.status == 1) {
				console.log(response.data);
				$scope.stats = response.data;
			}
		});
	};

$scope.getCallerStats();
$scope.calls = undefined;
$scope.newcall = 0;
$scope.product = '';
$scope.link = '';
$scope.comment = '';
	$scope.saveCallComment = function(call) {
		console.log("here");
		ServerCallService.MakeServerCall('../v2/Apiv2/User','POST',{method:'saveCallComment', id: call.id, comment: call.comment})
		.then(function(op) {
			call.showedit = 0;
		})
	}
	$scope.addCustomerCall = function(){
    	ServerCallService.MakeServerCall('../v2/Apiv2/User','POST',{
    		method: 'addCustomerCall',
    		purpose: $scope.purpose,
    		product: $scope.product,
    		name: $scope.name,
    		phone: $scope.phone,
    		source: $scope.source,
    		range: $scope.range,
    		link: $scope.link,
    		comment: $scope.comment,})
    		.then(function(response) {
    			$scope.newcall = 1;
    			$timeout(function() {$scope.newcall = 0;}, 1000);
    			$scope.wp.$setPristine();
    			$scope.name = "";
    			$scope.phone = "";
    			$scope.source = "OLX";
    			$scope.purpose = 'sale';
    			$scope.range = "";
    			$scope.comment = "";
    			$scope.product = "";
    			$scope.link = "";
    	});
    }
	$scope.currentpage = 0;
    $scope.getAllCalls = function() {
    	console.log($scope.currentpage);
        ServerCallService.MakeServerCall('../v2/Apiv2/User', 'GET', {method: 'getCallDetails',page: $scope.currentpage})
			.then(function(response) {
				if (response.data.length == 0) {
					$scope.calls = undefined;
				} else {
					$scope.calls = response.data;
					console.log($scope.calls)
				}
			});
	};


	$scope.markRegistered = function() {
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'GET', {method: 'markRegisteredUsers'})
			.then(function(response) {
				if (response.status == 1) {
					$scope.getAllCalls();
				}
			});
	}
 	//Customer call ends here



 	//Callback

$scope.getCallBack = function() {
		console.log("Callback details");
        ServerCallService.MakeServerCall('../v2/Apiv2/User', 'GET', {method: 'getCallBack'})
			.then(function(response) {

					$scope.callback = response.data;
					console.log($scope.callback);

			});
	};


$scope.showCallBack = function(cb) {
		$scope.showdetails = 1;
		$scope.b = cb;
	}

$scope.updateCallBack = function() {
	    console.log('Update callback')
		var called = ($scope.b.called == true || $scope.b.called == 1) ? 1 : 0;
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'POST',{method: 'updateCallBack',id:$scope.b.id, comments: $scope.b.comments, called: called, name: $scope.b.name})
		.then(function(op) {
 			if (op.status == 1) {
				$scope.showcbstat  = 1;
				$timeout(function() {$scope.shocbstat = 0;}, 2000);
			}
		});
	}

 	// Order/User statistics
 	$scope.search_date = new Date().toISOString().slice(0, 10);
 	$scope.search_month = new Date().toISOString().slice(0, 7);
 	$scope.search_year = new Date().toISOString().slice(0, 4);
 	$scope.getStatistics = function($search) {
 		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'GET', {method: 'getStatistics', searching: $search, search_date:$scope.search_date, search_month:$scope.search_month, search_year:$scope.search_year})
			.then(function(op) {
				if (op.status == 1) {
					$scope.statistics = op.data;
					console.log(op.data);
				}
		});
 	}

 	/*$scope.srchfield = 'sellid';
 	$scope.searchSale = function() {
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'POST', {method: 'searchSaleProducts', field: $scope.srchfield, value: $scope.srchvalue})
		.then(function(op) {
			if (op.status == 1) {
				$scope.sells = op.data;
				console.log("search");
				console.log($scope.sells);
			}
		});
	};*/

 	$scope.currentsellpage = 0;
 	$scope.srchfield = 'sellid';
 	$scope.getPendingSell = function($search) {
 		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'GET', {method: 'getPendingSell', page: $scope.currentsellpage, showoption: $scope.showoption, field: $scope.srchfield, value: $scope.srchvalue})
			.then(function(op) {
				console.log($scope.srchfield);
				console.log($scope.srchvalue);
				console.log ("search");
			if (op.status == 1) {
					$scope.sells = op.data;
				}
				if ($scope.sells.length == 0){
					$scope.sells = null;

				}
			$scope.srchvalue = undefined;
			$scope.ss.$setPristine();
		});
 	}
 	$scope.setsellmodalimage = function(images, im) {
 		$scope.sellmodalimages = images;
 		$scope.sellmodalimagesrev = $scope.sellmodalimages.slice();
 		$scope.sellmodalimagesrev.reverse();
 		$scope.sellmodalimage = im;
 	}

 	$scope.nextsellmodalimage = function(image) {
 		found = 0;
 		angular.forEach($scope.sellmodalimages, function(im) {
 			if (found == 1) {
 				$scope.sellmodalimage = im;
 				found = 0;
 			}
 			if (im == image) {
 				found = 1;
 			}
 		})
 	}

 	$scope.prevsellmodalimage = function(image) {
 		found = 0;
 		angular.forEach($scope.sellmodalimagesrev, function(im) {
 			if (found == 1) {
 				$scope.sellmodalimage = im;
 				found = 0;
 			}
 			if (im == image) {
 				found = 1;
 			}
 		})

 	}
 	$scope.approveSellPrice = function(s) {
 		console.log("save");
 		id = s.id;
 		price = s.approvedprice;
 		comment = s.admincomment;
  		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'POST', {method: 'approveSellPrice', id:id, approvedprice:price, admincomment:comment})
			.then(function(op) {
				if (op.status == 1) {
					console.log(op);
					s.isapproved = 1;
				}
		});
 	}

 	$scope.saveSaleComment = function(s) {
 		id = s.id;
 		comment = s.admincomment;
 		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'POST', {method: 'saveSaleComment', id:id,  admincomment:comment})
			.then(function(op) {
				if (op.status == 1) {
				}
		});
 	}
 	$scope.markProcured = function(s) {
 		id = s.id;
 		price = s.approvedprice;
 		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'POST', {method: 'markProcured', id:id, approvedprice:price})
			.then(function(op) {
				if (op.status == 1) {
					console.log(op);
					s.isprocured = 1;
				}
		});
 	}
 	$scope.markContacted = function(s) {
 		id = s.id;
 		price = s.approvedprice;
 		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'POST', {method: 'markContacted', id:id, approvedprice: price})
			.then(function(op) {
				if (op.status == 1) {
					s.customercalled = 1;
				}
		});
 	}

 	$scope.markRejected = function(s) {
 		id = s.id;
 		rejectreason = s.rejectreason;
 		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'POST', {method: 'markRejected', id:id, rejectreason: rejectreason})
			.then(function(op) {
				if (op.status == 1) {
					console.log(op);
					s.isrejected = 1;
				}
		});
 	}

 	    $scope.getProcurementDetails = function() {
    	console.log("procurementsss");
        ServerCallService.MakeServerCall('../v2/Apiv2/User', 'GET', {method: 'getProcurementDetails'})
			.then(function(response) {
				console.log("procurementsss");
				if (response.data.length == 0) {
					$scope.procurements = undefined;
				} else {
					$scope.procurements = response.data;
					console.log($scope.procurements);
					console.log("procurements");
				}
			});
	};


	$scope.getCouponDetails = function() {
		console.log("Coupon details");
        ServerCallService.MakeServerCall('../v2/Apiv2/User', 'GET', {method: 'getCouponDetails'})
			.then(function(response) {
				if (response.data.length == 0) {
					$scope.coupon = undefined;
				} else {
					$scope.coupon = response.data;
				}
			});
	};

	$scope.getCouponDiscount = function() {
		console.log("Apply Coupon");
        ServerCallService.MakeServerCall('../v2/Apiv2/User', 'GET', {method: 'getCouponDiscount', coupon: coupon})
			.then(function(response) {
				if (response.status == 9){
					$scope.invalidcoupon = 1;
				}else {
					$scope.details = response.data;
					console.log ($scope.details);
				}

			});
	};

	$scope .couponexpired = 0;
	$scope.markCouponExpired = function(cp) {
 		id = cp.id;
 		console.log("Mark Expired");
 		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'POST', {method: 'markCouponExpired', id:id})
			.then(function(op) {
				if (op.status == 1) {
					console.log(op);
					$scope.getCouponDetails();
					$scope.couponexpired = 1;
					$timeout(function() {$scope.couponexpired = 0;}, 2000);
					console.log("Done");
				}
		});
 	}

//Callback
	$scope.getCallBack = function() {
		console.log("Callback details");
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'GET', {method: 'getCallBack'})
			.then(function(response) {
					$scope.callback = response.data;
			});
	};



//Buyback

	//$scope.bb.pickup_date = new Date();
	$scope.newrequest = 0;
	$scope.addBuyBack = function() {
				ServerCallService.MakeServerCall('../v2/Apiv2/User', 'POST',
			{method: 'addBuyBack',
			input: angular.toJson($scope.bb)}).
		then(function(op) {
			if (op.status == 1) {
				$scope.bb = {};
				$scope.newrequest = 1;
				$timeout(function() {$scope.newrequest = 0;}, 2000);
				 $scope.neworder = null;
			    $scope.newbb.$setPristine();
			}
		});
	}


	$scope.disablesubmit = 0;
	$scope.priceadded = 0;
	$scope.addBbPrice = function() {
		console.log("Price update");
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'POST',
			{method: 'modifyBbPrice',
			 bb_id   :  $scope.mr.id,
			 bb_price: 	$scope.mr.bb_price,}).
		then(function(op)
		 {
 			if (op.status == 1) {
				$scope.mr.price = '';
				$scope.disablesubmit = 1;
				$scope.priceadded = 1;
				$scope.mr.status = "Approved";
				$timeout(function() {$scope.priceadded = 0;}, 2000);
			    $scope.bbprice.$setPristine();
			    //$scope.getBuyBackDetails();
			}
		});
	}

	$scope.pickupdone = 0;
	$scope.addBbPickup = function() {
		console.log ("Pickup ");
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'POST',
			{method      : 'markBbPickup',
			 bb_id       : $scope.mr.id}).
		then (function(op)
			{ if (op.status == 1){
				$scope.pickupdone = 1;
				$scope.mr.status = "Pickup Done";
				$timeout(function() {$scope.pickupdone = 0;}, 2000);
				//$scope.getBuyBackDetails();
			}

			});

	}

	$scope.reject = 0;
	$scope.addBbRejected = function() {
		console.log ("Rejected");
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'POST',
			{method      : 'markBbRejected',
			 bb_id       : $scope.mr.id,
			 reason      : $scope.mr.rejectreason}).
		then (function(op)
			{ if (op.status == 1){
				$scope.reject = 1;
				$scope.mr.status = "Rejected";
				$scope.mr.isrejected=1;
				$timeout(function() {$scope.reject = 0;}, 2000);
				//$scope.getBuyBackDetails();
			}

			});
	}


	//Coupon

	$scope.newcoupon = 0;
	$scope.createCoupon = function() {
			console.log("Coupon");
			ServerCallService.MakeServerCall('../v2/Apiv2/User', 'POST',
			{method: 'createCoupon',
			input: angular.toJson($scope.c)}).
		then(function(op) {
			if (op.status == 1) {
				$scope.c = {};
				$scope.isuserid = "";
				$scope.newcoupon = 1;
				$timeout(function() {$scope.newcoupon = 0;}, 2000);
			    $scope.coupon.$setPristine();
			}
		});
	}

	$scope.reopen = 0;
	$scope.bbReopen = function() {
		console.log ("Reopen");
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'POST',
			{method      : 'reopenBb',
			 bb_id       : $scope.mr.id}).
		then (function(op)
			{ if (op.status == 1){
				$scope.reopen = 1;
				$scope.mr.isrejected = null;
				$scope.mr.status = 'Processing';
				//$scope.getBuyBackDetails();
			}

			});

	}

	$scope.techassigned= 0;
	$scope.addBbTech = function() {
		console.log ("Assign Tech");
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'POST',
			{method      : 'addBbTech',
			 bb_id       : $scope.mr.id,
			 tech      : $scope.mr.tech}).
		then (function(op)
			{ if (op.status == 1){
				$scope.techassigned = 1;
				$scope.mr.status = "Tech Assigned";
				$timeout(function() {$scope.techassigned = 0;}, 2000);
			}

			});
	}


	//Delivery slots

$scope.getDeliveryDetails = function() {
		console.log("Delivery details");
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'GET', {method: 'getDelSlotInfo'})
			.then(function(response) {
					$scope.slots = response.data;
					angular.forEach($scope.slots, function(sl) {
						angular.forEach(sl, function(s) {
							s.address = angular.fromJson(s.address)
						});
					});
					console.log($scope.slots);
			});
        /*ServerCallService.MakeServerCall('../v2/Apiv2/User', 'GET', {method: 'getDeliveryDetails'})
			.then(function(response) {
				if (response.data.length == 0) {
					$scope.slots = undefined;
				} else {
					$scope.slots = response.data;
					console.log($scope.slot);
				}
			});*/


	};



	$scope.getBuyBackDetails = function() {

        ServerCallService.MakeServerCall('../v2/Apiv2/User', 'GET', {method: 'getBuyBackDetails'})
			.then(function(response) {
				if (response.data.length == 0) {
					$scope.buyback = undefined;
				} else {
					$scope.buyback = response.data;
				}
			});
	};

	$scope.showRequestDetails = function(bb) {
		//$scope.detailshow = 1;
		console.log("show");
		$scope.mr = bb;
		$scope.showtdetails = 1;
	}


	$scope.markReturned = function(item) {
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'POST', {method: 'markReturned', id: item.suborder})
		.then(function(op){
			if (op.status == 1) {
				console.log("Returned");
				item.onthespotreturn = 1
			}
		});
	}

	$scope.getConversion = function(item) {
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'GET', {method: 'getConversion'})
		.then(function(op){
			if (op.status == 1) {
				console.log(op.data);
				$scope.conversion = op.data;
			}
		});
	}
});

adminApp.directive('ngConfirmClick', [
  function(){
    return {
      priority: -1,
      restrict: 'A',
      link: function(scope, element, attrs){
        element.bind('click', function(e){
          var message = attrs.ngConfirmClick || "Are you Sure?";
          if(message && !confirm(message)){
            e.stopImmediatePropagation();
            e.preventDefault();
          }
        });
      }
    }
  }
]);

adminApp.filter('todate', function() {
    return function(input) {
    	return input;
        input = new Date(input);
        // Convert to IST
        input.setTime(input.getTime() + (330 * 60 * 1000));
        input = input.toISOString();
        return input;
    };
});


adminApp.filter('toslottime', function() {
    return function(slot) {
    	time = slot;
    	if (slot == "t1") {
    		time = "AM: 7.30 - 9.30";
    	} else if (slot == "t2") {
    		time = "AM: 8.30 - 10.30";
    	} else if (slot == "t3") {
    		time = "AM: 9.30 - 11.30";
    	} else if (slot == "t4") {
    		time = "AM: 10.30 - 12.30";
    	} else if (slot == "t5") {
    		time = "PM: 4.00 - 6.00";
    	} else if (slot == "t6") {
    		time = "PM: 5.00 - 7.00";
    	} else if (slot == "t7") {
    		time = "PM: 6.00 - 8.00";
    	} else if (slot == "t8") {
    		time = "PM: 7.00 - 9.00";
    	}
        return time;
    };
});

adminApp.filter('newlines', function() {
  return function(text) {
    if (text == undefined) {
        return "";
    }
    return text.split(/\n/g);
  };
});
