var del = angular.module('del', []);

del.service("authenticationService", ['$window', function($window) {
	this.GUESTAPIKEY = "pocketin_guest_api_key";
	$window.sessionStorage["pocketin_admin_info"] = null;
	this.loggeduser = angular.fromJson($window.sessionStorage["pocketin_admin_info"]) || null;

	this.loginUser = function(user) {
		this.loggeduser = user;
		//$cookieStore.put('loggeduser', this.loggeduser);
		$window.sessionStorage["pocketin_admin_info"] = angular.toJson(user);
	}

	this.logoutuser = function() {
		console.log("Logout");
		this.loggeduser = null;
		$window.sessionStorage["pocketin_admin_info"] = null;
	}

	this.getApiKey = function() {
		if (this.loggeduser == null) {
			return this.GUESTAPIKEY;
		}
		return this.loggeduser.token;
	}

	this.userLogged = function() {
		if (this.loggeduser == null) {
			return 0;
		}
		if ($window.sessionStorage["pocketin_admin_info"] == null) {
			return 0;
		}

		return 1;
	}
}]);

del.service("ServerCallService", ['authenticationService', '$http', '$q', '$timeout', '$rootScope',  function(authenticationService, $http, $q, $timeout, $rootScope) {
	this.MakeServerCall = function(url, method, param) {
		$rootScope.xhr = 1;
		// Add the API key of the logged in User
		param.apikey = authenticationService.getApiKey();
		var deferred = $q.defer();
		$http({
			url: url,
			method: method,
			params: param,
		}).then(function(response) {
			var status = 1;
			if (response.data.list && response.data.list.status != undefined) {
				status = response.data.list.status;
			}
			var op = {
				status: status,
				data: response.data.list
			};
			$rootScope.xhr = 0;
			//$timeout(function(){
			//    deferred.resolve(op);
			//},2000);
			deferred.resolve(op);
		}, function(response) {
			var op = {
				status: -1,
				data: undefined
			}
			$rootScope.xhr = 0;
			deferred.resolve(op);
		});
		return deferred.promise;
		//return $q.when("Hello World!");
	}
}]);

del.controller('DeliveryController', function($scope, ServerCallService, authenticationService, $timeout) {
	console.log("here");
	$scope.superuser = 0;
	$scope.loggedin = false;
	ServerCallService.MakeServerCall('v2/Apiv2/Authentication', 'GET', {method: 'whoami', role: 'admin',})
		.then(function(op) {
			op = op.data;
			if (op.status == 1) {
				authenticationService.loginUser({
					name: op.firstname + " " + op.lastname,
					firstname: op.firstname,
					mobile: op.mobile,
					token: op.token,
					user_id: op.user_id
				});
				$scope.name = authenticationService.loggeduser.firstname;
				$scope.loggedin = true;
				if (authenticationService.loggeduser.firstname == "Admin") {
					$scope.superuser = 1;
				}
			} else {
				authenticationService.logoutuser();
			}
		});


	$scope.Login = function() {
		ServerCallService.MakeServerCall('v2/Apiv2/Authentication', 'POST', {
				method: 'login',
				isadmin: 1,
				username: $scope.username,
				password: $scope.password})
			.then(function(response) {
				if (response.status == 1) {
					authenticationService.loginUser({
						firstname: response.data.profile.firstname,
						mobile: response.data.profile.phonenumber,
						token: response.data.token,
						user_id: response.data.profile.user_id
					});
					$scope.name = authenticationService.loggeduser.firstname;
					$scope.loggedin = true;
				} else {
					$scope.error = response.data.message;
				}
			});
	}

	$scope.logout = function() {
		ServerCallService.MakeServerCall('logout', 'GET', {})
        .then(function(op) {
            authenticationService.logoutuser();
			$scope.loggedin = false;
        });
	}

	

	$scope.dormant = 0;
	$scope.loadCustOrder = function() {
		ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', {method: 'getFinishedOrders', type: 'active'}).
		then(function(op) {
		    if (op.status == 1) {
		    	console.log(op);
		    	console.log("Customer orders");
		        $scope.corders = op.data.orders;
		        $scope.corders_copy = $scope.corders;
		        if ($scope.corders.isdormant == 1)
		        	{$scope.dormant= 1;}
		    } else {
		        console.log("API call failed");
		    }
		});
	}

$scope.loadCustOrder();
	
	$scope.getDeliveryDetails = function() {
		console.log("Delivery details");
        ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', {method: 'getDeliveryDetails'})
			.then(function(response) {
				if (response.data.length == 0) {
					$scope.slots = undefined;
				} else {
					$scope.slots = response.data;
					console.log($scope.slot);
				}
			});
	};


	$scope.searchOrder = function() {
		console.log("search");
		ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', {method: 'searchOrders', scat: $scope.scat, stext: $scope.stext}).
		then(function(op) {
		    if (op.status == 1) {
		    	console.log(op);
		    	$scope.corders = op.data.orders;
		        $scope.corders_copy = $scope.corders;
		    } else {
		        console.log("API call failed");
		    }});
	}


	$scope.commentdone = "";
	$scope.saveComment = function(oid, cmnt) {
		ServerCallService.MakeServerCall('v2/Apiv2/User', 'POST', {method: 'updateOrderComment', orderid: oid, comment:cmnt}).
		then(function(op) {
		    if (op.status == 1) {
		    	$scope.commentdone = "Saved";
		    } else {
		    	$scope.commentdone = "Failed";
		    }
		});
	}

	$scope.tot=0;
    $scope.checkReferral = function(order) {
        ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', {method: 'checkReferral', order_id: order}) 
        .then(function(op) {
            console.log("Referral");
            console.log(op);
            if (op.status == 1) {
            console.log("Done");
            $scope.referrals = op.data;
            $scope.discount = 0;
        	angular.forEach($scope.referrals, function(r) {
            if (r.ruser_id != null && $scope.discount < 500 && r.availed == 0) {
                $scope.discount += 100;
            }
        })}else{
        		console.log("Failed")
        	}
        })
    }

    $scope.performAction = function(action, item) {
		item.disable = 1;
		item.action = "Please wait...";
		if (action == "Order Executed" || action == "Cacellation Processed") {
			return;
		}
		ServerCallService.MakeServerCall('v2/Apiv2/User', 'POST', {method: 'performAction', action: action, id: item.id})
		.then(function(op) {
			if (op.status == 1) {
				item.action = op.data.action;
				item.status = op.data.status;
				item.disable = 0;
			} else {
				item.action = "Failed";
				item.disable = 1;
				console.log(op);
			}
		})
	}




});



del.directive('ngConfirmClick', [
  function(){
    return {
      priority: -1,
      restrict: 'A',
      link: function(scope, element, attrs){
        element.bind('click', function(e){
          var message = attrs.ngConfirmClick || "Are you Sure?";
          if(message && !confirm(message)){
            e.stopImmediatePropagation();
            e.preventDefault();
          }
        });
      }
    }
  }
]);

del.filter('todate', function() {
    return function(input) {
        input = new Date(input);
        // Convert to IST
        input.setTime(input.getTime() + (330 * 60 * 1000));
        input = input.toISOString();
        return input;
    };
});

del.filter('toslottime', function() {
    return function(slot) {
    	if (slot == "t1") {
    		time = "AM: 7.30 - 9.30";
    	} else if (slot == "t2") {
    		time = "AM: 8.30 - 10.30";
    	} else if (slot == "t3") {
    		time = "AM: 9.30 - 11.30";
    	} else if (slot == "t4") {
    		time = "AM: 10.30 - 12.30";
    	} else if (slot == "t5") {
    		time = "PM: 4.00 - 6.00";
    	} else if (slot == "t6") {
    		time = "PM: 5.00 - 7.00";
    	} else if (slot == "t7") {
    		time = "PM: 6.00 - 8.00";
    	} else if (slot == "t8") {
    		time = "PM: 7.00 - 9.00";
    	}
        return time;
    };
});

del.filter('newlines', function() {
  return function(text) {
    if (text == undefined) {
        return "";
    }
    return text.split(/\n/g);
  };
});


// export to csv
del.directive('exportToCsv',function(){
return {
    restrict: 'A',
    link: function (scope, element, attrs) {
        var el = element[0];
        element.bind('click', function(e) {
            var table = e.target.nextElementSibling;
            var csvString = '';
            for(var i=0; i<table.rows.length;i++){
                var rowData = table.rows[i].cells;
                for(var j=0; j<rowData.length;j++){
                	var d = String(rowData[j].innerHTML).replace(/<[^>]+>/gm, '');
                	d = d.replace(/,/g , " ");
                	d = '"' + d + '"';
                	//d = d.replace(/\n/g , "");
                    csvString = csvString + d + ",";
                }
                csvString = csvString.substring(0,csvString.length - 1);
                csvString = csvString + "\n";
            }
            csvString = csvString.substring(0, csvString.length - 1);
            var a = $('<a/>', {
                style:'display:none',
                href:'data:application/octet-stream;base64,'+btoa(csvString),
                download:'account.csv'
            }).appendTo('body')
            a[0].click()
            a.remove();
        });
    }
}
});
