var dealerApp = angular.module('pidealer', ['ngCookies', 'flow',]);

dealerApp.config(['flowFactoryProvider', function(flowFactoryProvider) {
	flowFactoryProvider.defaults = {
		target: '../uploadprodimage',
		permanentErrors: [404, 500, 501],
		maxChunkRetries: 3,
		chunkRetryInterval: 5000,
		simultaneousUploads: 4,
		testChunks: false,
	};
	flowFactoryProvider.on('catchAll', function(event) {
		//console.log('catchAll', arguments);
	});
	// Can be used with different implementations of Flow.js
	// flowFactoryProvider.factory = fustyFlowFactory;
}]);

dealerApp.service("authenticationService", ['$window', function($window) {
	this.GUESTAPIKEY = "pocketin_guest_api_key";
	$window.sessionStorage["pocketin_admin_info"] = null;
	this.loggeduser = angular.fromJson($window.sessionStorage["pocketin_admin_info"]) || null;

	this.loginUser = function(user) {
		this.loggeduser = user;
		//$cookieStore.put('loggeduser', this.loggeduser);
		$window.sessionStorage["pocketin_admin_info"] = angular.toJson(user);
	}

	this.logoutuser = function() {
		this.loggeduser = null;
		$window.sessionStorage["pocketin_admin_info"] = null;
	}

	this.getApiKey = function() {
		if (this.loggeduser == null) {
			return this.GUESTAPIKEY;
		}
		return this.loggeduser.token;
	}

	this.userLogged = function() {
		if (this.loggeduser == null) {
			return 0;
		}
		if ($window.sessionStorage["pocketin_admin_info"] == null) {
			return 0;
		}

		return 1;
	}
}]);



dealerApp.service("ServerCallService", ['authenticationService', '$http', '$q', '$timeout', function(authenticationService, $http, $q, $timeout) {
	this.MakeServerCall = function(url, method, param) {
		// Add the API key of the logged in User
		param.apikey = authenticationService.getApiKey();
		var deferred = $q.defer();
		$http({
			url: url,
			method: method,
			params: param,
		}).then(function(response) {
			var status = 1;
			if (response.data.list != undefined && response.data.list.status != undefined) {
				status = response.data.list.status;
			}
			var op = {
				status: status,
				data: response.data.list
			};
			//$timeout(function(){
			//    deferred.resolve(op);
			//},2000);
			deferred.resolve(op);
		}, function(response) {
			var op = {
				status: -1,
				data: undefined
			}
			deferred.resolve(op);
		});
		return deferred.promise;
		//return $q.when("Hello World!");
	}
}]);

dealerApp.controller('DealerController', function($http, $scope, ServerCallService, authenticationService, $window, $location, $q) {
	this.pid = undefined;
	$scope.success = 0;
	$scope.dealerLogged = false;
	ctrl = this;
	$scope.warehouseinfofetched = 0;
	$scope.winfofailed = 0;

	this.initupload = function($file) {
		if(!{png:1,gif:1,jpg:1,jpeg:1}[$file.getExtension()]) {
			console.log("Not a valid file. only png, jpg, gif and jpeg");
			$window.alert("Not a valid file. only png, jpg, gif and jpeg");
			return false;
		}
	}

	this.loadItems = function() {
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'GET', {
				method: 'getProductsByDealer',
				dealer_id: authenticationService.loggeduser.user_id,
			})
			.then(function(response) {
				console.log(response);
				$scope.items = response.data;
			});
	}

	// Get the User Details from the Server
	ServerCallService.MakeServerCall('../v2/Apiv2/Authentication', 'GET', {
			method: 'whoami',
			role: 'dealer'
		})
		.then(function(op) {
			op = op.data;
			console.log(op);
			if (op.status == 1) {
				authenticationService.loginUser({
					name: op.firstname + " " + op.lastname,
					firstname: op.firstname,
					mobile: op.mobile,
					token: op.token,
					user_id: op.user_id
				});
				if (authenticationService.loggeduser) {
					$scope.dealername = authenticationService.loggeduser.firstname;
					$scope.dealerLogged = true;
					ctrl.loadItems();
				}
			} else {
				authenticationService.logoutuser();
			}
		});
	$scope.subcat = -1;
	$scope.subcats = [{
		name: "---Please select Main Category---",
		id: "-1"
	}];

	ServerCallService.MakeServerCall('../v2/Apiv2/User', 'GET', {
			method: 'getParentCategories',
		})
		.then(function(response) {
			$scope.maincats = response.data;
		});


	$scope.getQualityValues = function() {
		$scope.qv = [];
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'GET', {
				method: 'getQualityIndexValues',
				cat_id: $scope.subcat,
			})
			.then(function(response) {
				$scope.quality = response.data;
			});
	}

	$scope.getSubCat = function() {
		if ($scope.maincat == "-1") {
			$scope.subcat = -1;
			$scope.subcats = [{
				name: "---Please select Main Category---",
				id: "-1"
			}];
		} else {
			ServerCallService.MakeServerCall('../v2/Apiv2/User', 'GET', {
					method: 'getProducttCategoriesByParentID',
					parentcat_id: $scope.maincat,
				})
				.then(function(response) {
					$scope.subcats = response.data;
					$scope.subcat = $scope.subcats[0].id;
					$scope.getQualityValues();
				});
		}
	};
	$scope.fetchWarehouseInfo = function() {
		console.log("fetch");
		$scope.alreadyuploaded  = 0;
		$scope.winfofailed = 0;
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'GET', {
				method: 'getWarehouseInfo',
				wid: $scope.wid,
			})
			.then(function(response) {
				if (response.status == 0) {
					console.log("failed");
					$scope.winfofailed = 1;
				} else if(response.status == 9) {
					$scope.alreadyuploaded = 1;
					$scope.alreadyuploadedid = response.data.pid;
				}
				else {
					$scope.winfo = response.data;
					$scope.warehouseinfofetched = 1;
					$scope.maincat = $scope.winfo.maincat;
					ServerCallService.MakeServerCall('../v2/Apiv2/User', 'GET', {
							method: 'getProducttCategoriesByParentID',
							parentcat_id: $scope.maincat,
						})
						.then(function(response) {
							console.log($scope.winfo);
							$scope.subcats = response.data;
							$scope.subcat = $scope.winfo.product_cat;
							$scope.productname = $scope.winfo.product_title;
							$scope.getQualityValues();
						});
				}
			});

	}

	$scope.markSold = function(item) {
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'POST', {method: 'markSold', id: item.productitem_id})
		.then(function(op){
			if (op.status == 1) {
				item.issold = 1;
			}
		});
	}

	$scope.stockcount = 1;
	$scope.addNewProduct = function() {
		var qvs = [];
		angular.forEach($scope.quality, function(q) {
			var qv = {};
			qv.index = q.qid;
			qv.value = $scope.qv[q.qid];
			qvs.push(qv);
		})
		$scope.adding = 1;
		var isfactory_1 = ($scope.isfactory == true || $scope.isfactory == 1) ? 1 : 0;
		var brandnew = ($scope.brandnew == true || $scope.brandnew == 1) ? 1 : 0;
		console.log(brandnew);
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'POST', {
				method: 'addProductByDealer',
				maincat: $scope.maincat,
				productcategory_id: $scope.subcat,
				name: $scope.productname,
				description: $scope.productdesc,
				saleprice: 0,//$scope.saleprice,
				manufacturingyear: 0,//$scope.year,
				isfactory: isfactory_1,
				brandnew: brandnew,
				pid: ctrl.pid,
				qualityindexes: angular.toJson(qvs),
				user_id: authenticationService.loggeduser.user_id,
				stockcount: $scope.stockcount,
				compressorwarranty: 0,//$scope.warranty,
				mrp: $scope.mrp,
				longdescription : $scope.longdescription,
				technicalspec : $scope.technicalspec,
				conditionspec : $scope.conditionspec,
				warehouseid: $scope.wid
			})
			.then(function(response) {
				console.log(response);
				if (response.status == 1 && response.data.status == 1) {
					console.log(response.data);
					$scope.success = 1;
					$scope.adding = 0;
					ctrl.pid = undefined;
				}
			});
	}

	$scope.Login = function() {
		ServerCallService.MakeServerCall('../v2/Apiv2/Authentication', 'POST', {
				method: 'login',
				isdealer: 1,
				username: $scope.username,
				password: $scope.password
			})
			.then(function(response) {
				if (response.status == 1 && response.data.status == 1) {
					//$scope.message = "Successfully logged in";
					// Make the fields disabled
					authenticationService.loginUser({
						firstname: response.data.profile.firstname,
						mobile: response.data.profile.phonenumber,
						token: response.data.token,
						user_id: response.data.profile.user_id
					});
					$scope.dealername = authenticationService.loggeduser.firstname;
					$scope.dealerLogged = true;
					ctrl.loadItems();
				} else {
					$scope.error = response.data.message;
				}
			});
	}

	$scope.logout = function() {
        ServerCallService.MakeServerCall('../logout', 'GET', {})
        .then(function(op) {
            authenticationService.logoutuser();
			$scope.dealername = "";
			$scope.dealerLogged = false;
			$scope.error = "";
        });
	}

	this.removeimage = function(file) {
		file.removefailed = 0;
		var name = file.name;
		ServerCallService.MakeServerCall("../removeprodimage", "GET", {
				name: name,
				pid: ctrl.pid
			})
			.then(function(response) {
				console.log(response);
				if (response.status == 1) {
					file.cancel();
				} else {
					console.log("failed to remove the image");
					file.removefailed = 1;
				}

			});
	}
	$scope.imageadded = false;

	this.uploadimage = function($flow) {
		$scope.completedupload = false;
		$scope.imageadded = true;
		if (ctrl.pid == undefined) {
			console.log('uploading image');
			//ctrl.pid = Math.floor((Math.random() * 1000) + 1);
			ServerCallService.MakeServerCall("../initiatedealerupload", "GET", {dealer_id: authenticationService.loggeduser.user_id})
			.then(function(response) {
				console.log(response);
				if (response.status == 1) {
					ctrl.pid = response.data.pid;
					$flow.opts.query = {
						pid: ctrl.pid
					};
					$flow.upload();
				} else {
					console.log("Failed to get the Dealer transaction ID");
					$window.alert("Failed to get the Dealer transaction ID. Please refresh the page and try again");
				}
			});
		} else {
			$flow.opts.query = {
				pid: ctrl.pid
			};
			$flow.upload();
		}
	}

	this.uploadfailed = function($file) {
		console.log("failed to upload" +  $file);
	}
	this.uploadcomplete = function() {
		console.log("upload completed for all files");
		$scope.completedupload = true;
	}

	$scope.shootdate = new Date();
	$scope.uploadPhotoStatus = function(date, wlisttext) {
		console.log(date);
		$scope.photodone = 0;
		//var wlist = wlisttext.split("\n");
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'GET', {method: 'uploadPhotoStatus',date: date, list: wlisttext})
        .then(function(op) {
            console.log(op);
		$scope.photodone = 1;
        });
	}
	$scope.getUploadStatus = function(all) {
		ServerCallService.MakeServerCall('../v2/Apiv2/User', 'GET', {method: 'getPendingUploads', all:all})
        .then(function(op) {
            console.log(op);
            $scope.upending = op.data;
        });
	}

	$scope.uploadProduct = function(wid){
		$scope.wid = wid;
		$scope.fetchWarehouseInfo();
	}

});

dealerApp.directive('ngConfirmClick', [
  function(){
    return {
      priority: -1,
      restrict: 'A',
      link: function(scope, element, attrs){
        element.bind('click', function(e){
          var message = attrs.ngConfirmClick || "Are you Sure?";
          if(message && !confirm(message)){
            e.stopImmediatePropagation();
            e.preventDefault();
          }
        });
      }
    }
  }
]);
