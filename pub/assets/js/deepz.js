var dapp = angular.module('drapp', []);

dapp.service("authenticationService", ['$window', function($window) {
	this.GUESTAPIKEY = "pocketin_guest_api_key";
	$window.sessionStorage["pocketin_admin_info"] = null;
	this.loggeduser = angular.fromJson($window.sessionStorage["pocketin_admin_info"]) || null;

	this.loginUser = function(user) {
		this.loggeduser = user;
		//$cookieStore.put('loggeduser', this.loggeduser);
		$window.sessionStorage["pocketin_admin_info"] = angular.toJson(user);
	}

	this.logoutuser = function() {
		this.loggeduser = null;
		$window.sessionStorage["pocketin_admin_info"] = null;
	}

	this.getApiKey = function() {
		if (this.loggeduser == null) {
			return this.GUESTAPIKEY;
		}
		return this.loggeduser.token;
	}

	this.userLogged = function() {
		if (this.loggeduser == null) {
			return 0;
		}
		if ($window.sessionStorage["pocketin_admin_info"] == null) {
			return 0;
		}

		return 1;
	}
}]);

dapp.service("ServerCallService", ['authenticationService', '$http', '$q', '$timeout', '$rootScope',  function(authenticationService, $http, $q, $timeout, $rootScope) {
	this.MakeServerCall = function(url, method, param) {
		$rootScope.xhr = 1;
		// Add the API key of the logged in User
		param.apikey = authenticationService.getApiKey();
		var deferred = $q.defer();
		$http({
			url: url,
			method: method,
			params: param,
		}).then(function(response) {
			var status = 1;
			if (response.data.list && response.data.list.status != undefined) {
				status = response.data.list.status;
			}
			var op = {
				status: status,
				data: response.data.list
			};
			$rootScope.xhr = 0;
			//$timeout(function(){
			//    deferred.resolve(op);
			//},2000);
			deferred.resolve(op);
		}, function(response) {
			var op = {
				status: -1,
				data: undefined
			}
			$rootScope.xhr = 0;
			deferred.resolve(op);
		});
		return deferred.promise;
		//return $q.when("Hello World!");
	}
}]);

dapp.controller('PatientCtrlr', function($scope, ServerCallService, authenticationService, $timeout) {
	$scope.showcontent = 1;
	// Load the products
	$scope.newpatient = 0;
	$scope.addPatient = function() {
		ServerCallService.MakeServerCall('v2/Apiv2/User', 'POST',
			{ method: 'add_patient',
			 patient: angular.toJson($scope.patient)}).
		then(function(op) {
			console.log(op);
			if (op.status == 1) {
				$scope.newpatient = 1;
				$scope.wp.$setPristine();
				$scope.patient = null;

			}
		});
	}

	$scope.showPatient = function(p) {
		$scope.showdetails = 1;
		$scope.details = p;
	}

	$scope.modifyPatient = function() {
		ServerCallService.MakeServerCall('v2/Apiv2/User', 'POST',
			{method: 'modify_patient', patient: angular.toJson($scope.details)}


			 ).
		then(function(op) {
			console.log(op);
			if (op.status == 1) { console.log("done");
				$scope.mp.$setPristine();
				//$scope.details = null;
			}
		});
	}
	$scope.getAllPatients = function() {
		ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', {method: 'get_all_patients'})
		.then(function(op) {
			if (op.status == 1) {
				$scope.patients = op.data;
				console.log(op);
				console.log("Done")
			}
		});
	};

	$scope.searchPatient = function() {
		//$scope.searchfield = 'name';
		//searchfield= $scope.searchfield;
		ServerCallService.MakeServerCall('v2/Apiv2/User', 'POST', {method: 'search_patient', field: $scope.searchfield, value: $scope.searchvalue})
		.then(function(op) {
			if (op.status == 1) {
				$scope.patients = op.data;
				$scope.showcontent = 2;
				console.log(op);
			}
		});
	};
	//$scope.getAllPatients();

	/* Login related function, keep it here */
	ServerCallService.MakeServerCall('v2/Apiv2/Authentication', 'GET', {method: 'whoami', role: 'admin',})
		.then(function(op) {
			op = op.data;
			if (op.status == 1) {
				authenticationService.loginUser({
					name: op.firstname + " " + op.lastname,
					firstname: op.firstname,
					mobile: op.mobile,
					token: op.token,
					user_id: op.user_id
				});
				$scope.name = authenticationService.loggeduser.firstname;
				$scope.loggedin = true;
			} else {
				authenticationService.logoutuser();
			}
		});




	$scope.Login = function() {
		ServerCallService.MakeServerCall('v2/Apiv2/Authentication', 'POST', {
				method: 'login',
				isadmin: 1,
				username: $scope.username,
				password: $scope.password})
			.then(function(response) {
				if (response.status == 1) {
					authenticationService.loginUser({
						firstname: response.data.profile.firstname,
						mobile: response.data.profile.phonenumber,
						token: response.data.token,
						user_id: response.data.profile.user_id
					});
					$scope.name = authenticationService.loggeduser.firstname;
					$scope.loggedin = true;
				} else {
					$scope.error = response.data.message;
				}
			});
	}
});

dapp.directive('ngConfirmClick', [
  function(){
    return {
      priority: -1,
      restrict: 'A',
      link: function(scope, element, attrs){
        element.bind('click', function(e){
          var message = attrs.ngConfirmClick || "Are you Sure?";
          if(message && !confirm(message)){
            e.stopImmediatePropagation();
            e.preventDefault();
          }
        });
      }
    }
  }
]);
