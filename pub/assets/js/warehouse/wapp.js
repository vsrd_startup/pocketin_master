var piwap = angular.module('piwap', []);

piwap.service("authenticationService", ['$window', function($window) {
	this.GUESTAPIKEY = "pocketin_guest_api_key";
	$window.sessionStorage["pocketin_admin_info"] = null;
	this.loggeduser = angular.fromJson($window.sessionStorage["pocketin_admin_info"]) || null;

	this.loginUser = function(user) {
		this.loggeduser = user;
		//$cookieStore.put('loggeduser', this.loggeduser);
		$window.sessionStorage["pocketin_admin_info"] = angular.toJson(user);
	}

	this.logoutuser = function() {
		this.loggeduser = null;
		$window.sessionStorage["pocketin_admin_info"] = null;
	}

	this.getApiKey = function() {
		if (this.loggeduser == null) {
			return this.GUESTAPIKEY;
		}
		return this.loggeduser.token;
	}

	this.userLogged = function() {
		if (this.loggeduser == null) {
			return 0;
		}
		if ($window.sessionStorage["pocketin_admin_info"] == null) {
			return 0;
		}

		return 1;
	}
}]);

piwap.service("ServerCallService", ['authenticationService', '$http', '$q', '$timeout', '$rootScope',  function(authenticationService, $http, $q, $timeout, $rootScope) {
	this.MakeServerCall = function(url, method, param) {
		$rootScope.xhr = 1;
		// Add the API key of the logged in User
		param.apikey = authenticationService.getApiKey();
		var deferred = $q.defer();
		$http({
			url: url,
			method: method,
			params: param,
		}).then(function(response) {
			var status = 1;
			if (response.data.list && response.data.list.status != undefined) {
				status = response.data.list.status;
			}
			var op = {
				status: status,
				data: response.data.list
			};
			$rootScope.xhr = 0;
			//$timeout(function(){
			//    deferred.resolve(op);
			//},2000);
			deferred.resolve(op);
		}, function(response) {
			var op = {
				status: -1,
				data: undefined
			}
			$rootScope.xhr = 0;
			deferred.resolve(op);
		});
		return deferred.promise;
		//return $q.when("Hello World!");
	}
}]);

piwap.controller('WarehouseCtrl', function($scope, ServerCallService, authenticationService, $timeout) {
	// Load the products
	$scope.showdetails = 0;
	$scope.currentpage = 0;
	$scope.dp = {};
	$scope.getAllProducts = function() {
		console.log("all");
		ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', {method: 'getWarehouseProducts', page: $scope.currentpage})
		.then(function(response) {
			if (response.status == 1) {
				$scope.products = response.data;
				console.log($scope.products);
			}
		});
	};

	$scope.getStats = function() {
		ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', {method: 'getWarehouseStats'})
		.then(function(response) {
			if (response.status == 1) {
				console.log(response.data);
				$scope.stat = response.data;
			}
		});
	};

	// Load the Products and warehouse stat
	$scope.getAllProducts();
	$scope.getStats();

	$scope.product = {};
	$scope.product.product_cat = "Refrigerator";
	$scope.product.arrived_from = "Shabaz";
	$scope.product.arrival_date = new Date();
	$scope.addProduct = function() {
		var unboxed = ($scope.product.unboxed == true || $scope.product.unboxed == 1) ? 1 : 0;
		ServerCallService.MakeServerCall('v2/Apiv2/User', 'POST',
			{method: 'warehouseAddProduct',
			 warehouse_id:  $scope.product.warehouse_id,
			 product_cat:   $scope.product.product_cat,
			 product_title: $scope.product.product_title,
			 arrival_date:  $scope.product.arrival_date,
			 arrived_from:  $scope.product.arrived_from,
			 procure_cost:  $scope.product.procure_cost,
			 modelno:       $scope.product.modelno,
			 unboxed	 :  unboxed}).
		then(function(op) {
			if (op.status == 1) {
				console.log("Warehouse");
				console.log($scope.product);
				$scope.product.stage = "Arrived";
				$scope.products.unshift($scope.product);
				$scope.product = {};
				$scope.product.product_cat = "Refrigerator";
				$scope.product.arrived_from = "Shabaz";
				$scope.product.arrival_date = new Date();
				$scope.wp.$setPristine();
			}
		});
	}

	$scope.modifyProduct = function() {
		ServerCallService.MakeServerCall('v2/Apiv2/User', 'POST',
			{method: 'warehouseModifyProduct',
			 id: 			$scope.dp.id,
			 warehouse_id:  $scope.dp.warehouse_id,
			 product_cat:   $scope.dp.product_cat,
			 product_title: $scope.dp.product_title,
			 arrival_date:  $scope.dp.arrival_date,
			 arrived_from:  $scope.dp.arrived_from,
			 procure_cost:  $scope.dp.procure_cost,
			 stage:         $scope.dp.stage,
			 saleprice:     $scope.dp.saleprice,
			 product_id:    $scope.dp.product_id,
			 modelno:       $scope.dp.modelno,
			 product_condition: $scope.dp.product_condition,}).
		then(function(op) { console.log($scope.dp);
 			if (op.status == 1) {
				$scope.showmstat  = 1;
				$timeout(function() {$scope.showmstat = 0;}, 2000);
			}
		});
	}

	$scope.srchfield = 'warehouse_id';

	$scope.searchProduct = function() {
		ServerCallService.MakeServerCall('v2/Apiv2/User', 'POST', {method: 'searchWarehouseProducts', field: $scope.srchfield, value: $scope.srchvalue})
		.then(function(op) {
			if (op.status == 1) {
				$scope.products = op.data;
				console.log(op);
				console.log($scope.products);
			}
		});
	};

	$scope.getVendorDetails = function() {
		console.log("View vendors");
        ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', {method: 'getVendorDetails'})
			.then(function(response) {
				if (response.data.length == 0) {
					$scope.vendors = undefined;
				} else {
					$scope.vendors = response.data;
				}
			});
	};
	$scope.getVendorDetails();


	$scope.showProductDetails = function(p) {
		$scope.showdetails = 1;
		$scope.dp = p;

		// Get the Product Stages
		ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', {method: 'getWarehouseProductStages'})
		.then(function(response) {
			if (response.status == 1) {
				$scope.stages = response.data;
			}
		});
	}

	ServerCallService.MakeServerCall('v2/Apiv2/Authentication', 'GET', {method: 'whoami', role: 'admin',})
		.then(function(op) {
			op = op.data;
			if (op.status == 1) {
				authenticationService.loginUser({
					name: op.firstname + " " + op.lastname,
					firstname: op.firstname,
					mobile: op.mobile,
					token: op.token,
					user_id: op.user_id
				});
				$scope.name = authenticationService.loggeduser.firstname;
				$scope.loggedin = true;
			} else {
				authenticationService.logoutuser();
			}
		});


	$scope.Login = function() {
		ServerCallService.MakeServerCall('v2/Apiv2/Authentication', 'POST', {
				method: 'login',
				isadmin: 1,
				username: $scope.username,
				password: $scope.password})
			.then(function(response) {
				if (response.status == 1) {
					authenticationService.loginUser({
						firstname: response.data.profile.firstname,
						mobile: response.data.profile.phonenumber,
						token: response.data.token,
						user_id: response.data.profile.user_id
					});
					$scope.name = authenticationService.loggeduser.firstname;
					$scope.loggedin = true;
				} else {
					$scope.error = response.data.message;
				}
			});
	}

	$scope.prs = [];
	$scope.pr = {};
	$scope.pr.date = new Date();
	$scope.addProcurement = function() {
		$scope.pr.disablebutton = 1;
		ServerCallService.MakeServerCall('v2/Apiv2/User', 'POST',
			{method: 'addProcurementDetails',
			input: angular.toJson($scope.pr)}).
		then(function(op) {
			if (op.status == 1) {
				$scope.prs.push($scope.pr);
				$scope.pr = {};
				$scope.pr.date = new Date();
			    $scope.p.$setPristine();
			    $scope.pr.disablebutton = 0;
			}
		});
	}

	$scope.exp = [];
	$scope.ex = {};
	$scope.ex.date = new Date();
	$scope.addExpense = function() {
		console.log ("Expense");
		$scope.ex.disablebutton = 1;
		ServerCallService.MakeServerCall('v2/Apiv2/User', 'POST',
			{method: 'addExpenseDetails',
			input: angular.toJson($scope.ex)}).
		then(function(op) {
			if (op.status == 1) {
				$scope.exp.push($scope.ex);
				$scope.ex = {};
				$scope.ex.date = new Date();
			    $scope.expense.$setPristine();
			    $scope.ex.disablebutton = 0;
			}
		});
	}


	$scope.getProcurementDetails = function() {
    	console.log("procurementsss");
        ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', {method: 'getProcurementDetails'})
			.then(function(response) {
				console.log("procurements");
				if (response.data.length == 0) {
					$scope.procurements = undefined;
				} else {
					$scope.procurements = response.data;
					console.log($scope.procurements);
					console.log("procurements");
				}
			});
	};


	// $scope.getVendorDetails = function() {
	// 	console.log("View vendors");
 //        ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', {method: 'getVendorDetails'})
	// 		.then(function(response) {
	// 			if (response.data.length == 0) {
	// 				$scope.vendors = undefined;
	// 			} else {
	// 				$scope.vendors = response.data;
	// 			}
	// 		});
	// };
	// $scope.getVendorDetails();


	$scope.getProductCategory = function() {
		console.log("View categories");
        ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', {method: 'getProductCategories'})
			.then(function(response) {
				if (response.data.length == 0) {
					$scope.category = undefined;
				} else {
					$scope.category = response.data;
					console.log($scope.category);
				}
			});
	};

	$scope.getProductCategory();



	$scope.getExpenseDetails = function() {
        ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', {method: 'getExpenseDetails'})
			.then(function(response) {
				if (response.data.length == 0) {
					$scope.expenses = undefined;
				} else {
					$scope.expenses = response.data;
				}
			});
	};
	
});



piwap.directive('ngConfirmClick', [
  function(){
    return {
      priority: -1,
      restrict: 'A',
      link: function(scope, element, attrs){
        element.bind('click', function(e){
          var message = attrs.ngConfirmClick || "Are you Sure?";
          if(message && !confirm(message)){
            e.stopImmediatePropagation();
            e.preventDefault();
          }
        });
      }
    }
  }
]);

piwap.filter('todate', function() {
    return function(input) {
        input = new Date(input);
        // Convert to IST
        input.setTime(input.getTime() + (330 * 60 * 1000));
        input = input.toISOString();
        return input;
    };
});

piwap.filter('newlines', function() {
  return function(text) {
    if (text == undefined) {
        return "";
    }
    return text.split(/\n/g);
  };
});

// export to csv
piwap.directive('exportToCsv',function(){
return {
    restrict: 'A',
    link: function (scope, element, attrs) {
        var el = element[0];
        element.bind('click', function(e) {
            var table = e.target.nextElementSibling;
            var csvString = '';
            for(var i=0; i<table.rows.length;i++){
                var rowData = table.rows[i].cells;
                for(var j=0; j<rowData.length;j++){
                	var d = String(rowData[j].innerHTML).replace(/<[^>]+>/gm, '');
                	d = d.replace(/,/g , " ");
                    csvString = csvString + d + ",";
                }
                csvString = csvString.substring(0,csvString.length - 1);
                csvString = csvString + "\n";
            }
            csvString = csvString.substring(0, csvString.length - 1);
            var a = $('<a/>', {
                style:'display:none',
                href:'data:application/octet-stream;base64,'+btoa(csvString),
                download:'warehouse_stock.csv'
            }).appendTo('body')
            a[0].click()
            a.remove();
        });
    }
}
});
