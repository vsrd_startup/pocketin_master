var mainApp = angular.module('pocketin', ['ngToast','ngCookies', 'pocketinControllers']);
// Manage login and logout
mainApp.service('authenticationService', ['$window', '$log', '$http', '$rootScope', function($window, $log, $http, $rootScope) {
    // TODO: Add functionality to share local storage between tabs
    // TODO: Add functionality to have lifetime for local storage
    this.GUESTAPIKEY = "pocketin_guest_api_key";

    var _this = this;

    this.loggedUser = function() {
        var user = $window.localStorage.getItem("pocketin_user_info") || null;
        return angular.fromJson(user);
    }

    this.userIn = function() {
        var user = $window.localStorage.getItem("pocketin_user_info") || null;
        return (user != null);
    }

    this.loginUser = function(user) {
        $window.localStorage.setItem("pocketin_user_info", angular.toJson(user));
        // Show the user actin div for one time
        //$("#useraction").click();
       /* setTimeout(function() {
            $('#useractiondiv').fadeOut('fast');
        }, 3000); // <-- time in milliseconds*/
    }

    this.logoutuser = function() {
        $window.localStorage.removeItem("pocketin_user_info");
    }

    this.getApiKey = function() {
        if (_this.userIn()) {
            return _this.loggedUser().token;
        }

        return _this.GUESTAPIKEY;
    }


    // Load the User from server if there is no local user Logged in
    this.loadUser = function() {
        // TODO: check the cookie lifetime
        if (this.userIn()) {
            //$log.log("User Already logged in");
            return;
        }

        // Else, make server call to get logged in user.
        // Session Storage has scope for the current session only.
        $http({
                url: 'v2/Apiv2/Authentication',
                method: 'GET',
                params: { method: 'whoami' },
            }).then(function(response) {
                //$log.log("success");
                op = response.data.list;
                if (op.status == 1) {
                    _this.loginUser({
                        name: op.firstname + " " + op.lastname,
                        firstname: op.firstname,
                        mobile: op.mobile,
                        token: op.token,
                        user_id: op.user_id
                    });
                }
            }, function(op) {
            });
    };

    this.loadUser();
}]);

mainApp.service("notificationService", ['authenticationService', 'ServerCallService', '$q', function(authenticationService, ServerCallService, $q) {
    this.nots = null;
    this.notcount = 0;
    var _this  = this;

    this.loadNotification = function() {
        var deferred = $q.defer();
        if (!authenticationService.userIn()) {
            deferred.resolve(null);
            return deferred.promise;
        }


        ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', {method: 'getUserNotifications', unseen: '1'})
            .then(function(op) {
                if (op.status == 1) {
                    _this.nots = op.data.nots;
                    _this.notcount = _this.nots.length;
                    deferred.resolve(op.data.nots);
                }
            });

        return deferred.promise;
    }

    this.seenNotification = function() {
        var deferred = $q.defer();
        if (!authenticationService.userIn()) {
            deferred.resolve(null);
            return deferred.promise;
        }

        ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', {
                method: 'seenNotification'
            })
            .then(function(op) {
                if (op.status == 1) {
                    deferred.resolve(null);
                }
            });

        return deferred.promise;
    }

}]);

// Toast Service. Displays the Toast
mainApp.service("ToastService", function() {
    this.showToast = function(image, productname, productdesscription, action) {
        content = `<div class="added-to-cart toast">
                        <div class="added-item-thumb">
                            <img class="cart-d-image" src=` + image + `>
                        </div>
                        <div class="added-item-details">
                            <h5 class="added-item-name">` + productname + `</h5>
                            <span class="product-description">` + productdesscription + `</span>
                        </div>
                        <label>` + action + `</label>
                    </div>`
        return content;
    };
});

mainApp.service('OrderService', function() {
    this.orderactive = 0;
    this.order = {};
});

mainApp.service('WishlistService', ['authenticationService', 'ServerCallService', 'ToastService', 'ngToast', function(authenticationService, ServerCallService, ToastService, ngToast) {
    this.add = function(item, addorremove) {
        if (authenticationService.userIn() == 0) {
            item.inwishlist = 0;
            $('#signupform').modal({backdrop: 'static', keyboard: false, show: true});
            /*ngToast.create({
                content: "Please Login or Register",
                className: 'success',
                dismissOnTimeout: true,
                timeout: 1700,
                dismissOnClick: false,
                dismissButton: true,});*/
        } else {
            ServerCallService.MakeServerCall('v2/Apiv2/User', 'POST', {method: 'addToWishlist', productitem_id: item.productitem_id, addorremove: addorremove})
            .then(function(op) {
                if (addorremove == 1){
                content = ToastService.showToast(
                    "assets/images/products/thumbnail/" + item.image_full,
                    item.product_title,
                    item.product_description
                    ,"Added to wishlist");
                ngToast.create({
                    className: 'none',
                    content: content,
                    dismissOnTimeout: true,
                    timeout: 1500,
                    dismissOnClick: true,});
            } else {
                content = ToastService.showToast(
                    "assets/images/products/thumbnail/" + item.image_full,
                    item.product_title,
                    item.product_description
                    ,"Removed from wishlist");
                ngToast.create({
                    className: 'none',
                    content: content,
                    dismissOnTimeout: true,
                    timeout: 1500,
                    dismissOnClick: true,});
            }
            });
        }
    }
}]);

// Angular Service to Make Server Call
mainApp.service("ServerCallService", ['authenticationService', '$http', '$q', '$timeout', '$log', function(authenticationService, $http, $q, $timeout, $log) {
        this.MakeServerCall =  function(url, method, param) {
            // Add the API key of the logged in User
            param.apikey = authenticationService.getApiKey();
            var deferred = $q.defer();
            //$log.log(url + "/" + param.method);
            $http({
                url: url,
                method: method,
                params: param,
            }).then(function(response) {
                var status = 1;
                if (response.data.list != undefined && response.data.list.status != undefined) {
                    status = response.data.list.status;
                }
                var op = {
                    status: status,
                    data: response.data.list
                };
                //$timeout(function(){
                //    deferred.resolve(op);
                //},2000);
                deferred.resolve(op);
            }, function(response) {
                var op = {
                    status: -1,
                    data: undefined
                }
                deferred.resolve(op);
            });
            return deferred.promise;
            //return $q.when("Hello World!");
        }
}]);


mainApp.service('CartService', ['ServerCallService', '$cookieStore', 'authenticationService', function(ServerCallService, $cookieStore, authenticationService) {
    // If the User is logged in, get the items from the Server
    this.cart_id = undefined;
    this.cartitems = $cookieStore.get('pocketinCart') || [];
    this.cartcount = this.cartitems.length;
    this.checkoutitems = undefined;
    this.buynowitem = undefined;
    //console.log(this.cartitems);
    var _this = this;

    this.frameCart = function() {
        //console.log("cart");
        //console.log(this.cartitems);
        _this = this;
        if (this.cartitems === undefined) {
            this.cartitems = $cookieStore.get('pocketinCart') || [];
            this.cartcount = this.cartitems.length;
        }

        if (authenticationService.userIn() == 1) {
            ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', {
                    method: 'getCartItems',
            })
            .then(function(op) {
                if (op.status == 0) {
                    // No cart exists for the User
                    local_cart = [];
                    angular.forEach(_this.cartitems, function(item) {
                        local_cart.push(item.product_id);
                    });
                    ServerCallService.MakeServerCall('v2/Apiv2/User', 'POST', { method:'addToCart', user_id: authenticationService.loggedUser().user_id, item_ids: angular.toJson(local_cart)})
                    .then(function(op) {
                        //console.log("Pushed ");
                        //console.log(local_cart);
                    });
                } else if (op.status == 1) {
                    // Get the cart ID
                    _this.cart_id = op.data.cart_id;
                    //console.log("cart id " + _this.cart_id);

                    // Check if we have anything in the cookie, if so Get a Union of products
                    var local_cart = [];
                    var server_cart = [];
                    angular.forEach(_this.cartitems, function(item) {
                        local_cart.push(item.product_id);
                    });

                    angular.forEach(op.data.items, function(item) {
                        server_cart.push(item.product_id);
                    });
                    local_cart.sort();
                    server_cart.sort();
                    //console.log(local_cart);
                    //console.log(server_cart);
                    var carts_same = (local_cart.length == server_cart.length) && local_cart.every(function(element, index) {
                        return element === server_cart[index];
                    });
                    if (carts_same) {
                        // Carts match, nothing to do
                        //console.log('carts match');
                    } else {
                        //console.log("Carts dont match. Getting union");
                        //console.log(op.data.items);
                        // Update the server cart with difference
                        diff_cart = [];
                        angular.forEach(local_cart, function(id) {
                            if (server_cart.indexOf(id) == -1) {
                                diff_cart.push(id);
                            }
                        });
                        ServerCallService.MakeServerCall('v2/Apiv2/User', 'POST', { method:'addToCart', user_id: authenticationService.loggedUser().user_id, item_ids: angular.toJson(diff_cart)})
                        .then(function(op) {
                            //console.log("diff push");
                            //console.log(diff_cart);
                        });

                        angular.forEach(server_cart, function(item) {
                            if (local_cart.indexOf(item) == -1) {
                                cartitem = {
                                    product_id: item,
                                };
                                //console.log("uninit cart");
                                //console.log(_this.cartitems);
                                _this.cartitems.push(cartitem);
                                _this.cartcount = _this.cartitems.length;
                            }
                        });

                        //console.log(_this.cartitems);
                        $cookieStore.put('pocketinCart', _this.cartitems);
                    }
                }
            });
        }
    }

    this.frameCart();
    this.addToCart = function(id) {
        cartitem = {
            product_id: id,
        };
        // Check if item is already there in the cart, if so, prevent duplicate items
        alreadypushed = false;
        angular.forEach(this.cartitems, function(item) {
            if (item.product_id == id) {
                alreadypushed = true;
            }
        });

        if (!alreadypushed) {
            this.cartitems.push(cartitem);
            this.cartcount = this.cartcount + 1;
            $cookieStore.put('pocketinCart', this.cartitems);

            if (authenticationService.userIn() == 1) {
                ServerCallService.MakeServerCall('v2/Apiv2/User', 'POST', { method:'addToCart', user_id: authenticationService.loggedUser().user_id, item_ids: angular.toJson([id])})
            .then(function(op) {
            });
            } else {
                ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', { method:'addToCartNoLogin', prodid: id})
            .then(function(op) {
            });
            }
        }

        return alreadypushed;
    };

    this.removeFromCart = function(id) {
        if (authenticationService.userIn() == 1) {
            ServerCallService.MakeServerCall('v2/Apiv2/User', 'POST', {method:'removeFromCart', 'user_id': authenticationService.loggedUser().user_id, item_ids:angular.toJson([id])})
            .then(function(op) {
                //console.log(op);
            });
        }

        this.cartitems = this.cartitems.filter(function(obj) {
            return obj.product_id != id;
        });

        this.cartcount = this.cartcount - 1;
        $cookieStore.put('pocketinCart', this.cartitems);
    };

    this.getCartItems = function() {
        return ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', {
            method: 'getProductsByID',
            ids: angular.toJson(this.cartitems)
        });
    };

    this.clearCart = function() {
        this.cartitems = [];
        this.cartcount = 0;
        $cookieStore.put('pocketinCart', this.cartitems);
    }

    return this;
}]);

// Routing
/*
mainApp.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
    $routeProvider
        .when('/user/login', {
            templateUrl: 'user/login',
             resolve: {
                auth: ["$q", "authenticationService", function($q, authenticationService) {
                  var userInfo = authenticationService.loggedUser();
                  if (userInfo != null) {
                    return $q.reject({ authenticated: true });
                  } else {
                    return $q.when(userInfo);
                  }
                }],
            },
        })
}]);
*/

mainApp.config(function($routeProvider, $httpProvider) {
    $httpProvider.interceptors.push('myHttpInterceptor');
});

//XXX: Set the Error pages here
mainApp.factory('myHttpInterceptor', ['$q', '$location', '$window', '$log', function ($q, $location, $window, $log) {
    return {
        'responseError': function(errorResponse) {
            $window.sessionStorage['error'] = angular.toJson(errorResponse);
            switch (errorResponse.status) {
           case 401: // Unauthorized
                $window.localStorage.removeItem("pocketin_user_info");
                //location.replace("error");
                break;
            case 403: //Forbidden
                //location.replace("error");
                break;
            case 404: // Not Found
                //location.replace("error");
                break;
           case 500: // Internal Server Error
                //location.replace("error");
                break;
            }
            return $q.reject(errorResponse);
        },
    };
}]);

mainApp.filter('removeSpaces', [function() {
    return function(string) {
        if (!angular.isString(string)) {
            return string;
        }
        return string.replace(/[\s]/g, '');
    };
}]);

mainApp.filter('toproducturl', function ($filter) {
    return function (value) {
        value = $filter('lowercase')(value);
        if (!value) return 'product';
        value = value.replace(/ /g, '-');
        value = value.replace(/\./g, '-');
        value = value.replace(/\(/g, '-');
        value = value.replace(/\)/g, '-');
        value = value.replace(/\(/g, '-');
        return value;

    };
});

var routeLoadingIndicator = function($rootScope) {
    return {
        restrict:'E',
        template: "<div class='loader' ng-if='isRouteLoading'><img src='assets/images/loader.gif'></div>",
        //template: "<div class='col-lg-12 loading' ng-if='isRouteLoading'><p>Loading Page, Please wait.. <i class='fa fa-cog fa-spin'></i></p></div>",
        link:function(scope, elem, attrs){
            scope.isRouteLoading = false;

            $rootScope.$on('$routeChangeStart', function(){
                scope.isRouteLoading = true;
            });

            $rootScope.$on('$routeChangeSuccess', function(){
                scope.isRouteLoading = false;
            });
        }
    };
};

routeLoadingIndicator.$inject = ['$rootScope'];

mainApp.run(["$rootScope", "$location", function($rootScope, $location) {
  $rootScope.$on("$routeChangeSuccess", function(userInfo) {
  });

  $rootScope.$on("$routeChangeError", function(event, current, previous, eventObj) {
    if (eventObj.authenticated === false) {
        // Remember the path he was going, and redirect after log in
        $rootScope.customerpath = $location.path();
        $location.path("user/login");
    } else if (eventObj.authenticated === true) {
         $location.path("");
    }

  });
}]);


mainApp.directive('routeLoadingIndicator', routeLoadingIndicator);

mainApp.directive('specialProducts', function() {
    return {
        restrict: 'E',
        templateUrl: 'assets/templates/special-products.html'
    };
});

mainApp.directive('scrollOnClick', function() {
  return {
    restrict: 'A',
    link: function(scope, $elm) {
      $elm.on('click', function() {
        //$("body").animate({scrollTop: $elm.offset().top}, "slow");
        $("html, body").animate({ scrollTop: 0 }, "slow");
      });
    }
  }
});


mainApp.directive('ngConfirmClick', [
  function() {
    return {
      priority: -1,
      restrict: 'A',
      link: function(scope, element, attrs){
        element.bind('click', function(e){
          var message = attrs.ngConfirmClick || "Are you Sure?";
          if(message && !confirm(message)){
            e.stopImmediatePropagation();
            e.preventDefault();
          }
        });
      }
    }
  }
]);

mainApp.filter("sanitize", ['$sce', function($sce) {
  return function(htmlCode) {
    return $sce.trustAsHtml(htmlCode);
  }
}]);

mainApp.filter('newlines', function() {
  return function(text) {
    if (text == undefined) {
        return "";
    }
    return text.split(/\n/g);
  };
});

mainApp.directive('imageonload', function() {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                element.bind('load', function() {
                    //call the function that was passed
                    scope.$apply(attrs.imageonload);
                });
            }
        };
    })

// export to csv
mainApp.directive('exportToCsv',function(){
return {
    restrict: 'A',
    link: function (scope, element, attrs) {
        var el = element[0];
        element.bind('click', function(e){
            var table = e.target.nextElementSibling;
            var csvString = '';
            for(var i=0; i<table.rows.length;i++){
                var rowData = table.rows[i].cells;
                for(var j=0; j<rowData.length;j++){
                    csvString = csvString + rowData[j].innerHTML + ",";
                }
                csvString = csvString.substring(0,csvString.length - 1);
                csvString = csvString + "\n";
            }
            csvString = csvString.substring(0, csvString.length - 1);
            var a = $('<a/>', {
                style:'display:none',
                href:'data:application/octet-stream;base64,'+btoa(csvString),
                download:'emailStatistics.csv'
            }).appendTo('body')
            a[0].click()
            a.remove();
        });
    }
}
});
/*mainApp.directive('ngClearNots', function ($log) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            element.on('focus', function () {
                $('#clearnots').click();
            });
        }
    }
});*/
