var ctrlApp = angular.module('pocketinControllers', ['ngCookies', 'ngToast','ngRoute', 'flow', 'ngSidebarJS']);

ctrlApp.config(['flowFactoryProvider', function(flowFactoryProvider) {
    flowFactoryProvider.defaults = {
        target: 'uploadsellimage',
        permanentErrors: [404, 500, 501],
        maxChunkRetries: 3,
        chunkRetryInterval: 5000,
        simultaneousUploads: 4,
        testChunks: false,
    };
    flowFactoryProvider.on('catchAll', function(event) {
    });
}]);

ctrlApp.controller('TimedModalctrl', ['$scope', 'ServerCallService','authenticationService', '$timeout', '$window', function($scope, ServerCallService, authenticationService, $timeout, $window) {
    console.log("here modal");
    $scope.reqCallback = function(cbnumber, item) {
        console.log("Callback")
        ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', {method: 'reqCallback', number:cbnumber, product:0})
        .then(function(op) {
            if (op.status == 1) {
                $("#closecbmodal").click();
            } else {
                console.log("failed");
            }
        });
    }
}]);

/*********************************************************
 * Login/Signup Controllers
 **********************************************************/
ctrlApp.controller('LoginSignupController', ['$rootScope', '$scope', 'ServerCallService','authenticationService', 'CartService', 'notificationService', '$location', function($rootScope, $scope, ServerCallService, authenticationService, CartService, notificationService, $location) {
    $scope.user = {};
    $scope.canedit = true;
    var _this = this;

    $scope.resetSignupModal = function() {
        $scope.otp = "";
        $scope.canedit = true;
        $scope.signupform.$setPristine();
        $scope.message = "";
        $scope.otpshow = false;
        $scope.user = {};
    }

this.signup = function() {
        $scope.otpshow = false;
        $scope.message = "Registering, please wait..";
        $scope.canedit = false;
        //console.log("signup");
        ctrl = this;
        ServerCallService.MakeServerCall('v2/Apiv2/Authentication', 'POST',
            { method: 'signupCustomer',
                email: $scope.user.email,
            phonenumber: $scope.user.phonenumber,
            password: $scope.user.password,
            firstname: $scope.user.firstname,
            lastname: $scope.user.lastname,
            picture: 'none',}
            )
            .then(function(response) {
                if(response.status == 1) {
                    $scope.message = "Enter the OTP sent to your mobile";
                    $scope.user.username = $scope.user.phonenumber;
                    $scope.otpshow = true;
                } else if (response.status == 9) {
                    $scope.message = "You are already registered!!";
                    $scope.canedit = true;
                }
            });
    };

    this.getLoggedUser = function() {
        if (authenticationService.userIn()) {
            $scope.loggeduser = authenticationService.loggedUser().firstname;
            if ($scope.loggeduser.length > 7) {
                $scope.loggeduser = $scope.loggeduser.substring(0, 7) + "..";
            }
            return $scope.loggeduser;
        }
        return "";
    }

    this.verifyotp = function(otp) {
        ServerCallService.MakeServerCall('v2/Apiv2/Authentication', 'POST',
                { method: 'verifyOTP',
                    phonenumber: $scope.user.phonenumber,
            password: $scope.user.password,
            otp: otp}
            )
            .then(function(response) {
                if(response.data.status == 1) {
                    $scope.message = "Registration succeessful.";
                    // Log in the User
                    authenticationService.loginUser({firstname:$scope.user.firstname, mobile:$scope.user.phonenumber, token: response.data.token, user_id: response.data.user_id});
                    // Frame the Cart
                    CartService.frameCart();

                    $scope.user = {};
                    $scope.otp = "";
                    $scope.canedit = true;
                    $scope.signupform.$setPristine();
                    $("#closesignup").click();
                    $scope.message = "";
                    $scope.otpshow = false;
                    if (window.location.pathname.includes('login')) {
                        location.replace('https://www.refabd.com/cart');
                    }
                } else {
                    $scope.message = "OTP not matching, Please enter again.";
                    //$scope.canedit = true;
                }
            });
    }

    this.userLogged = function() {
        return (authenticationService.userIn());
    }

    $scope.requestotp = false;

    this.login = function() {
        $scope.canedit = false;
        $scope.message = "Logging in, please wait..";
        ServerCallService.MakeServerCall('v2/Apiv2/Authentication', 'POST',
                { method: 'login',
                    username: $scope.user.username,
                    password: $scope.user.password
                }
            )
            .then(function(response) {
                //console.log(response);
                if (response.status == 1) {
                    authenticationService.loginUser({firstname:response.data.profile.firstname, mobile:response.data.profile.phonenumber, token: response.data.token, user_id: response.data.profile.user_id});
                    $scope.user = {};

                    $scope.loginform.$setPristine();
                    // Frame the Cart
                    CartService.frameCart();

                    // Load notificatin
                    notificationService.loadNotification();
                    // If there is a path set in customer path, redirect there
                    /*if ($scope.customerpath != undefined) {
                        location.replace($rscope.customerpath);
                        $scope.customerpath = undefined;
                    }*/
                    location.replace('');
                    $scope.loginform.$setPristine();
                    $scope.canedit = true;
                    $scope.message = "";
                    $("#regmodalclose").click();
                } else if(response.status == 0 ) {
                    $scope.message = "Please Verify the Login Credentials.";
                    $scope.loginform.$setPristine();
                    $scope.canedit = true;
                } else if (response.status == 2) {
                    // User not verified
                    $scope.message = "Sorry, Mobile number not verfied.";
                    $scope.requestotp = true;
                    $scope.loginform.$setPristine();
                    $scope.canedit = true;
                }
            });

    }

    this.logout = function() {
        $scope.message = "";
        $scope.user = {};
        $scope.requestotp = false;
        $scope.loginform.$setPristine();
        $scope.canedit = true;
        authenticationService.logoutuser();
        notificationService.nots = null;
        notificationService.notcount = 0;
        $scope.loggeduser = undefined;
        $scope.canedit = true;
        ServerCallService.MakeServerCall('logout', 'GET', {})
        .then(function(op) {
            CartService.clearCart();
        });
    }

    $scope.showotp = false;
    $scope.showpwd = false;
    $scope.done = false;

    $scope.getotp = function(username) {
        ServerCallService.MakeServerCall('v2/Apiv2/Authentication', 'POST', {method:'getOTPForForgotpwd', username: username})
            .then(function(op) {
                if (op.status == 1) {
                    $scope.showotp = true;
                    $scope.message = "";
                } else {
                    $scope.message = "Not a valid User, Please create an Account";
                }
            });

    }

    $scope.verifyotp = function() {
        ServerCallService.MakeServerCall('v2/Apiv2/Authentication', 'POST', {method:'verifyOTPForForgotpwd', username: $scope.username, otp:$scope.otp})
            .then(function(op) {
                if (op.status == 1) {
                    $scope.showpwd = true;
                    $scope.message = "";
                } else {
                    $scope.message = "Failed to verify OTP.";
                }
            });
    }

    $scope.verifyotpandlogin = function() {
        ServerCallService.MakeServerCall('v2/Apiv2/Authentication', 'POST', {method:'verifyOTPForForgotpwd', username: $scope.user.username, otp:$scope.otp})
            .then(function(op) {
                if (op.status == 1) {
                  _this.login();
                  $scope.user = {};
                  console.log("here");
                  location.reload();
                } else {
                    $scope.message = "Failed to verify OTP.";
                }
            });
    }

    $scope.changepwd = function() {
        ServerCallService.MakeServerCall('v2/Apiv2/Authentication', 'POST', {method:'changePassword', username: $scope.username, pwd:$scope.pwd})
            .then(function(op) {
                if (op.status == 1) {
                    $scope.message = "Password changed successfully.";
                    $scope.message1 = "You can close this and login with new password!";
                    $scope.showotp = false;
                    $scope.showpwd = false;
                    $scope.done = true;
                    $scope.username = "";
                    $scope.pwd = "";
                } else {
                    $scope.message = "Failed to change the password, please try again";
                }
            });
    }

    $scope.clearForgotPwdForm = function() {
        $scope.showotp = false;
        $scope.showpwd = false;
        $scope.done = false;
        $scope.message = "";
        $scope.message1 = "";
        $scope.forgot1.$setPristine();
        $scope.forgot2.$setPristine();
        $scope.forgot3.$setPristine();
    }
}]);
/*********************************************************
 * Home Page controllers
 **********************************************************/

ctrlApp.controller('TopQualityController', ['$http', '$scope', 'ServerCallService','WishlistService', function($http, $scope, ServerCallService, WishlistService) {
    $scope.title = "Top Quality Products";
    ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', {method: 'getSpecialProducts', type: 'Quality'})
    .then(function(response) {
        ////console.log(response)
        $scope.items = response.data.products;
    });

    this.addtowishlist = function(item, addorremove) {
           WishlistService.add(item, addorremove);
    }
}]);

ctrlApp.controller('SoldController', ['$http', '$scope', 'ServerCallService','WishlistService', function($http, $scope, ServerCallService, WishlistService) {
    $scope.title = "Recently Sold Products";
    ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', {method: 'getSpecialProducts', type: 'Sold'})
    .then(function(response) {
        $scope.items = response.data.products;
    });

    this.addtowishlist = function(item, addorremove) {
           WishlistService.add(item, addorremove);
    }
}]);

ctrlApp.controller('NewArrivalsController', ['$http', '$scope', 'ServerCallService','WishlistService', function($http, $scope, ServerCallService, WishlistService) {
    $scope.title = "New Arrivals";
    ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', {method: 'getSpecialProducts', type: 'New'})
    .then(function(response) {
        $scope.items = response.data.products;
    });

    this.addtowishlist = function(item, addorremove) {
           WishlistService.add(item, addorremove);
    }


}]);

ctrlApp.controller('RelatedItemsController', ['ServerCallService', '$scope', 'WishlistService', function(ServerCallService, $scope, WishlistService) {
    $scope.title = "Related to the Item you are viewing";
    //console.log("related");
    $scope.$watch('Model.dataLoaded', function(dataLoaded) {
        if (dataLoaded) {
            //console.log("data loaded");
            //console.log($scope.catid);
            ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', {method: 'getSpecialProducts', type: 'Recommended', catid: $scope.catid, currentproduct: $scope.item.id})
            .then(function(response) {
                $scope.items = response.data.products;
            });
        }
    });

    this.addtowishlist = function(item, addorremove) {
           WishlistService.add(item, addorremove);
    }
}]);

ctrlApp.controller('ItemHistoryController', ['$scope', '$window','ServerCallService','WishlistService', function($scope, $window, ServerCallService, WishlistService) {
    $scope.title = "Recently Viewed Products";
    var history = angular.fromJson($window.localStorage.getItem("pocketin_user_history")) || null;
    if (history == null) {
        $scope.items = null;
        $scope.itemspresent = 0;
    } else {
        $scope.itemspresent = 1;
        ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', { method: 'getProductsByID', ids: angular.toJson(history)})
        .then(function(op) {
            $scope.items = op.data;
            //console.log($scope.items);
        })
    }

    this.addtowishlist = function(item, addorremove) {
           WishlistService.add(item, addorremove);
    }

}]);

/*********************************************************
 * Collection Page controllers

 function CollectionsController($http, $scope) {
 } is another way of defining controller


 ctrlApp.controller('CollectionsController', ['$scope', 'items', function($scope, op) {
 //console.log(op);
 if (op.status == -1) {
 //console.log("Failed to Call API");
 }
 $scope.items = op.data;
 }]);
 **********************************************************/


ctrlApp.controller('collectionController', ['$scope', 'ServerCallService', function($scope, ServerCallService) {
     ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', {method: 'getProductCategories'})
    .then(function(op) {
        $scope.items = op.data;
    });
}]);

/*
   CollectionsController.resolve = {
   items : function($q, $http, $timeout) {
   var deferred = $q.defer();
   $http({
   url: 'v2/Apiv2/User',
   method: 'GET',
   params: {
   method: 'getProducttCategories',
   },
   }).then(function(response) {
   var op = {status:1, data:response.data.productcategories};
   $timeout(function(){
   deferred.resolve(op);
   },2000);
//deferred.resolve(op);
}, function(response) {
var op = {status:-1, data: response.data};
deferred.resolve(op);
});
return deferred.promise;
},

};
 */
/*********************************************************
 * Category Page controllers
 **********************************************************/
ctrlApp.controller('CategoryController', ['$http', '$scope', 'ServerCallService', 'WishlistService', function($http, $scope, ServerCallService, WishlistService) {
    $scope.initializeCatId =function(catid) {
        $scope.catid = catid;
        $scope.loadProduct();
    }
    $scope.catloading = 1;
    $scope.itemlimit = 10;
    $scope.page_no = 1;
    $scope.hideoos = 0;

    $scope.itembegin = 1;
    $scope.itemend = $scope.itembegin + $scope.itemlimit - 1;
    $scope.currentpage = 1;
    $scope.totalpages = 1;
    $scope.totalitems = $scope.itemlimit;
    $scope.pages = [];

    $scope.currentsort = "Latest Arrivals";
    $scope.sorttypes = ['Latest Arrivals', 'Price: Low to High', 'Price: High to Low', 'Top Quality'];

    $scope.loadProduct = function() {
        ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', { method: 'getProductsByCategory', page_no: $scope.page_no, limit: $scope.itemlimit, sorttype: $scope.currentsort, category_id: $scope.catid, hideoos:$scope.hideoos})
        .then(function(op) {
            $scope.items = op.data.products;
            //console.log(op);
            $scope.catname = op.data.catname;
            $scope.totalpages = op.data.paging.totalpages;
            for (var i = 1; i <= $scope.totalpages; i++) {
                $scope.pages.push(i);
            }
            $scope.totalitems = op.data.paging.totalitems;
            if ($scope.totalitems < $scope.itemend) {
                $scope.itemend = $scope.totalitems;
            }
            if ($scope.itemend == 0) {
                $scope.itembegin = 0;
            }
            $scope.catloading = 0;
        });
    }

    $scope.goToPage = function(page, ooschange) {
        if (ooschange == undefined) {
            ooschange = 0;
        }
        if (page > $scope.totalpages) {
            return;
        }
        if (page == 0) {
            return;
        }
        $scope.catloading = 1;
        ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', { method: 'getProductsByCategory', page_no: page, limit: $scope.itemlimit, sorttype: $scope.currentsort , category_id: $scope.catid, hideoos:$scope.hideoos})
        .then(function(op) {
            $scope.items = op.data.products;
            $scope.currentpage = page;
            $scope.page_no = page;
            $scope.itembegin = (page-1) * $scope.itemlimit + 1;
            $scope.itemend = $scope.itembegin + $scope.itemlimit - 1;

            if (ooschange) {
                $scope.totalpages = op.data.paging.totalpages;
                $scope.pages = [];
                for (var i = 1; i <= $scope.totalpages; i++) {
                    $scope.pages.push(i);
                }
                $scope.totalitems = op.data.paging.totalitems;
            }

            if ($scope.totalitems < $scope.itemend) {
                $scope.itemend = $scope.totalitems;
            }
            $scope.catloading = 0;
        });
    }

    $scope.sortProducts = function(sortmethod) {
        if (sortmethod == $scope.currentsort) {
            return;
        }

        $scope.currentsort = sortmethod;
        // Populate the products
        $scope.goToPage(1);
    }

    $scope.changeOos = function() {
        $scope.goToPage(1, 1);
    }

    $scope.addtowishlist = function(item, addorremove) {
           WishlistService.add(item, addorremove);
    }

}]);

ctrlApp.controller('CartCountController', ['CartService' , function(CartService) {
    this.getCartCount = function() {
        return CartService.cartcount;
    };
}]);

ctrlApp.controller('NotificationController', ['notificationService', '$scope', function(notificationService, $scope) {
    $scope.notclass = "";
    if (notificationService.nots == null) {
        notificationService.loadNotification()
        .then(function(op) {
            //console.log("notification loaded");
            //console.log(notificationService.nots);
        });
    } else {
    }

    this.getCount = function() {
        return notificationService.notcount;
    }

    this.getNots = function() {
        return notificationService.nots;
    }

    this.seenNotification = function() {
        //console.log("clearing");
        notificationService.seenNotification()
        .then(function(op) {
            //console.log("notification seen");
            $scope.notclass = "inactivecircle";
        })
    }

}]);



ctrlApp.controller('QualityValuesController', ['$scope', function($scope){

}]);


ctrlApp.controller('ProductsController', ['$scope', 'CartService', 'ServerCallService', 'WishlistService', '$location', '$window', function($scope, CartService, ServerCallService, WishlistService, $location, $window) {
    $scope.Model = {
        dataLoaded: false,
    };
    $scope.selectedimage = '';
    ////console.log(angular.toJson(this.parray));


    $scope.initializeProd = function(pid) {
        $scope.productid = pid;
        $scope.loadProdcut();
        $scope.loadQualityIndexValues();
    }

    $scope.loadProdcut = function() {
        ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', { log: 1, method: 'getProductsByID', ids: angular.toJson([{product_id: $scope.productid}])})
        .then(function(op) {
            op = op.data[0];
            $scope.item = op;
            $scope.catname = op.catname;
            $scope.catid = op.catid;
            /*//console.log("item is");*/
            ////console.log($scope.item);
            $scope.selectedimage = "assets/images/products/normal/" + $scope.item.images[0].image_full;
            $scope.selectedimagezoom = "assets/images/products/zoom/" + $scope.item.images[0].image_full;
            $scope.Model.dataLoaded = true;

            // If product is not approved, redirect to error page.
            if ($scope.item.isapproved == 0) {
                $window.sessionStorage['error']=angular.toJson({status:'Invalid Product', statusText: 'Product not approved'})
                //console.log( $window.sessionStorage['error']);
                $location.path("error");
            }
                    // Add the product in the history list
            var history = angular.fromJson($window.localStorage.getItem("pocketin_user_history")) || [];

            // Make sure no duplicate items are pushed
            var alreadypushed = false;
            angular.forEach(history, function(item) {
                if (item.product_id == $scope.productid) {
                    alreadypushed = true;
                }
            });

            // Append the product id to the history
            if (!alreadypushed) {
                history.unshift({product_id: $scope.productid});
                if (history.length == 6) {
                    history = history.slice(0, -1);
                }

                // Save History
                $window.localStorage.setItem("pocketin_user_history", angular.toJson(history));
            }
        });
    }
    $scope.loadQualityIndexValues = function() {
        ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', {method: 'getQualityIndexKeyValueForProduct', product_id: $scope.productid})
        .then(function(op) {
        if (op.status == 1) {
        $scope.quality = op.data.items;
        }
        });
    }
     $scope.reqcbdone = 0;
     $scope.reqCallback = function(cbnumber, item) {
        console.log("Callback")
        ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', {method: 'reqCallback', number:cbnumber, product:item})
        .then(function(op) {
            if (op.status == 1) {
                $scope.reqcbdone = 1;
            } else {
                console.log("failed");
            }
        });
    }

$scope.showImageLoader = function(im) {
    $scope.selectedimage = "assets/images/catloading.gif";
    $scope.selectedimageobj = im;
}

$scope.changeBigImage = function() {
    if ($scope.selectedimageobj == undefined) {
        return;
    }
    angular.forEach($scope.item.images, function(im) {
        im.activeorpassive = "";
    })
    $scope.selectedimageobj.activeorpassive = "active";
    $scope.selectedimage = "assets/images/products/normal/" + $scope.selectedimageobj.image_full;
    $scope.selectedimagezoom = "assets/images/products/zoom/" + $scope.selectedimageobj.image_full;
}

$scope.addtowishlist = function(item, addorremove) {
           WishlistService.add(item, addorremove);
    }

$scope.buyNow = function(item) {
    //$location.path("buynow/" +  item.productitem_id)
    //$window.location.href = "https://www.pocketin.in/#/buynow/" + item.productitem_id;
    //location.replace("buynow/" + item.productitem_id);
    id = item.productitem_id;
    alreadypushed = CartService.addToCart(id);
    location.replace('https://www.refabd.com/cart');
}
}]);

/*ctrlApp.controller('FilterController', ['$http', '$scope', '$routeParams', function($http, $scope, $routeParams) {
    $http({
        url: 'v2/Apiv2/User',
        method: 'GET',
        params: {
            method: 'getFiltersByCategory',
        productcategory_id: $routeParams.item
        },
    }).then(function(response) {
        $scope.filters = response.data.filters;
        //console.log($scope.item);
    }, function(response) {
        //console.log("API Call failed");
    });
}]);
*/
ctrlApp.controller('SearchController', ['$scope', 'ServerCallService', function($scope, ServerCallService) {
    $scope.query = "";
    $scope.selected_cat_id = 0; // Means All
    $scope.search_focus = "All Items";
    ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', {method: 'getProductCategories'})
    .then(function(op) {
        $scope.cats = op.data;
        //$scope.cats.push({name: 'All Items', 'id': 0});
    });

    $scope.setSearchFocus = function(cat) {
        $scope.search_focus = cat.name;
        $scope.selected_cat_id = cat.id;
    }

    $scope.displaySearchResult = function() {
        window.location = "search/" + $scope.selected_cat_id + "/" + $scope.query;
    }
}]);

ctrlApp.controller('SearchDisplayController', ['$scope', '$window', 'ServerCallService', function($scope, $window, ServerCallService) {
    $scope.initSearch = function(query, cat, items) {
        $scope.query = query;//$window.sessionStorage.getItem("pocketin_search_query") || "";
        $scope.cat = cat; //$window.sessionStorage.getItem("pocketin_search_cat") || "";

        $scope.itemlimit = 9;
        $scope.page_no = 1;

        $scope.itembegin = 1;
        $scope.itemend = $scope.itembegin + $scope.itemlimit - 1;

        $scope.currentpage = 1;
        $scope.totalpages = 1;
        $scope.totalitems = $scope.itemlimit;
        $scope.pages = [];

        $scope.currentsort = "Latest Arrivals";
        $scope.sorttypes = ['Latest Arrivals', 'Price: Low to High', 'Price: High to Low', 'Top Quality'];

        $scope.items = angular.fromJson(items);
    }
}]);


ctrlApp.controller('CartManagementController', ['CartService','ToastService','$scope', 'ngToast', function(CartService,ToastService, $scope, ngToast) {
    $scope.addToCart = function(item) {
        id = item.productitem_id
        alreadypushed = CartService.addToCart(id);
        var action = "Item Added to Cart";
        if (alreadypushed) {
            action = "Item is already in Cart";
        }

        //console.log("image" + item.images[0].image_full);
        content = ToastService.showToast(
            "assets/images/products/thumbnail/" + item.images[0].image_full,
            item.product_title,
            item.product_description
            ,action);

        ngToast.create({
            className: 'none',
            content: content,
            dismissOnTimeout: true,
            timeout: 1500,
            dismissOnClick: true,
        });
    }

    $scope.removeFromCart = function(id) {
        CartService.removeFromCart(id);
    }
}]);

ctrlApp.controller('CartDisplayController', ['$scope', 'CartService', 'WishlistService', '$location', function($scope, CartService, WishlistService, $location){
    $scope.disableoneplusoffer = 0;
    $scope.oneplusitem = 0;
    $scope.total = 0;
    $scope.sold = 0;
    $scope.cartempty = true;
    $scope.loading = true;
    $scope.OFFERGIVING = 5;
    $scope.offer = 0;
    CartService.getCartItems().then(function(op) {
        $scope.loading = false;
        $scope.cart = op.data;
        CartService.checkoutitems = $scope.cart;
        CartService.buyingfromcart = true;
        $scope.cartfetching = false;
        angular.forEach($scope.cart, function(item) {
            $scope.total += parseInt(item.saleprice);
            $scope.sold = $scope.sold || item.issold == 1;
        });
        if (!$scope.disableoneplusoffer && $scope.cart.length > 1) {
            $scope.oneplusitem = 1;
            $scope.offer = $scope.OFFERGIVING;
        }
        if ($scope.total != 0) {
            $scope.cartempty = false;
        }
    });

    $scope.removeFromCart = function(id, saleprice) {
        $scope.offer = 0;
        $scope.oneplusitem = 0;
        CartService.removeFromCart(id);
        $scope.cart = $scope.cart.filter(function(obj) {
            return obj.productitem_id != id;
        });
        CartService.checkoutitems = $scope.cart;
        $scope.sold = 0;
        angular.forEach($scope.cart, function(item) {
            $scope.sold = $scope.sold || item.issold == 1;
        });
        $scope.total -= parseInt(saleprice);
        if (!$scope.disableoneplusoffer && $scope.cart.length > 1) {
            $scope.oneplusitem = 1;
            $scope.offer = $scope.OFFERGIVING;
        }
        if ($scope.total == 0) {
            $scope.cartempty = true;
        }
    }

   $scope.addtowishlist = function(item, addorremove) {
           WishlistService.add(item, addorremove);
    }

    $scope.checkout = function() {
        location.replace("checkout");
        //$location.path("checkout")
    }
}]);

ctrlApp.controller('WishlistController', ['ServerCallService','$scope','authenticationService', 'CartService','$window', function(ServerCallService, $scope, authenticationService, CartService, $window) {
    if (!authenticationService.userIn()) {
        $scope.message = "User not logged in";
    } else {
        ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', {method: 'getWishlistedItems'})
    .then(function(op) {
        $scope.items = op.data;
        //console.log($scope.items);
        /*angular.forEach(op.data, function(item) {
          //console.log(item[0]);
          $scope.items.push(item[0]);
          });*/
    });
    }

    $scope.removeItem = function(wishid) {
        ServerCallService.MakeServerCall('v2/Apiv2/User', 'POST', {method:'removeFromWishlist', id: wishid})
    .then(function(op) {
        //console.log("done");
        $scope.items = $scope.items.filter(function(item) {
            return item.wishid != wishid;
        });
    });
    }

    $scope.buyNow = function(item) {
    //$window.location.href = "https://www.pocketin.in/#/buynow/" + item.productitem_id;
        //location.replace("buynow/" + item.productitem_id);
        id = item.productitem_id;
        alreadypushed = CartService.addToCart(id);
        location.replace('https://www.refabd.com/cart');
    }

}]);

ctrlApp.controller('SellRequestController', ['ServerCallService','$scope','authenticationService', 'CartService','$window', function(ServerCallService, $scope, authenticationService, CartService, $window) {
    $scope.nosells = 0;
    ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', {method: 'getCustomerSellRequests'})
    .then(function(op) {
        $scope.sells = op.data;
        if ($scope.sells.length == 0) {
            $scope.nosells = 1;
        }
    })
}]);

ctrlApp.controller('ReferralController', ['ServerCallService','$scope','authenticationService', 'CartService','$timeout', function(ServerCallService, $scope, authenticationService, CartService, $timeout) {
    $scope.norefs = 0;
    $scope.referbutton = "Refer and Earn";
    $scope.nr = {};

    ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', {method: 'getCustomerReferrals'})
    .then(function(op) {
        $scope.refers = op.data;
        $scope.points = 0;
        angular.forEach($scope.refers, function(r) {
            if (r.ruser_id != null && $scope.points < 500 && r.availed == 0) {
                $scope.points += 100;
            }
        })
        if ($scope.refers.length == 0) {
            $scope.norefs = 1;
        }
    })

    $scope.addnewReferral = function() {
        $scope.referbutton = "Posting..";
        ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', {method: 'addCustomerReferral', name:$scope.nr.rname, mob: $scope.nr.rmobile})
    .then(function(op) {
        console.log("Refer");
        if (op.status == 3) {
           $scope.alreadyregstrd = 1;
           $timeout(function() {$scope.alreadyregstrd = 0;}, 3000);
        } else {
            if ($scope.refers.length == 0) {
                $scope.norefs = 0;
            }
            $scope.refers.unshift($scope.nr);
        }
        $scope.nr = {};
        $scope.newreferform.$setPristine();
        $scope.referbutton = "Refer and Earn";
    })

    }
}]);


ctrlApp.controller('ServiceController', ['ServerCallService','$scope','authenticationService', 'CartService','$timeout', function(ServerCallService, $scope, authenticationService, CartService, $timeout) {

    $scope.clearorderform = function() {
        $scope.newticket = 0;
        $scope.service.$setPristine();
        $scope.neworderid= "";
        $scope.neworder = null;
        $scope.subject = "";
        $scope.comment = "";
        $scope.location = "";
        $scope.getallTickets();
    }

   $scope.addTicket = function() {
        $scope.newticket = 1;
        ServerCallService.MakeServerCall('v2/Apiv2/User', 'POST', {
            method: 'addTicket',
            order_id: $scope.neworderid,
            user_id: $scope.neworder.user_id,
            commenter: $scope.neworder.username,
            subject: $scope.subject,
            location: $scope.location,
            comment: $scope.comment,
            warranty: $scope.neworder.status,
            }).then(function(response) {
                $scope.newticket = 2;
                $timeout(function() {$scope.clearorderform()}, 3000);
            });
    };

    
    $scope.ordernotfound = 0;
    $scope.getOrderDetails = function(oid) {
        $scope.ordernotfound = 0;
        //console.log("Order details");
        ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', {method: 'getOrderDetailsByUserId', order_id: oid})
        .then(function(response) {
            if (response.status == 9) {
                $scope.ordernotfound = 1;
                console.log("Not found");

            } else {
                $scope.neworder = response.data.order;

                }
            //}
        });
    }


   $scope.getallTickets = function(){
        console.log("Get tickets");
        $scope.show  = 0;
        ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', {method: 'getServiceRequests'})
        .then(function(response) {
            $scope.tickets = response.data;
            if ($scope.tickets.length == 0) {
                $scope.tickets = null;
            } else {
                $scope.show  = 1;
                console.log($scope.tickets);
                

            }
           
        });
    };
  $scope.getallTickets();
}]);


ctrlApp.filter('todate', function() {
    return function(input) {
        input = new Date(input);
        // Convert to IST
        input.setTime(input.getTime() + (330 * 60 * 1000));
        input = input.toISOString();
        return input;
    };
});


ctrlApp.controller('CheckoutController', ['$rootScope', '$scope', 'ServerCallService', 'CartService', 'authenticationService', '$location', '$window', function($rootScope, $scope, ServerCallService, CartService, authenticationService, $location, $window) {
    $scope.initZest = function() {
        ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', {method:'getZestObject',})
        .then(function(op) {
            $scope.zest = op.data;
            if($scope.total>$scope.zest.minamount && $scope.total<$scope.zest.maxamount) {
                zestMerchant.getQuote($scope.zest.clientid, Math.floor($scope.total),false,'#zest-emi-widget',1000);
            }
    })};

    $scope.OFFERGIVING = 5;
    $scope.offer = 0;
    $scope.action = "Confirm Order";
    $scope.noaddress = 0;
    $scope.selectedaddress = {};

    $scope.initCart = function() {
           $scope.total = 0;
        if (CartService.checkoutitems) {
            $scope.cart = CartService.checkoutitems;
            angular.forEach($scope.cart, function(item) {
                $scope.total += parseInt(item.saleprice);
            });
            if ($scope.cart.length > 1) {
                $scope.offer = $scope.OFFERGIVING;
                $scope.total = $scope.total * (100-$scope.OFFERGIVING)/100
            }
        } else {
        CartService.getCartItems().then(function(op) {
            $scope.cart = op.data;
            CartService.buyingfromcart = true;
            angular.forEach($scope.cart, function(item) {
                $scope.total += parseInt(item.saleprice);
            });
            if ($scope.cart.length > 1) {
                $scope.offer = $scope.OFFERGIVING;
                $scope.total = $scope.total * (100-$scope.OFFERGIVING)/100
            }
        });
        }
    }
    $scope.initCart();
    $scope.addresses = [];
    $scope.loadAddress = function() {
        $scope.newaddress = {};
        ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', {method: 'getSavedAddresses',})
        .then(function(response) {
            //console.log(response);
            if(response.status == 1) {
                $scope.addresses = response.data.address;
                $scope.user = response.data.user;
                $scope.newaddress.name = $scope.user.name;
                $scope.newaddress.email = $scope.user.email;
                $scope.newaddress.phonenumber = Number($scope.user.phonenumber);
                if ($scope.addresses.length > 0) {
                    $scope.addresses[0].selected = 1;
                    $scope.addresses[0].class = "btn btn-use-address";
                    $scope.selectedaddress = $scope.addresses[0];
                } else {
                    $scope.addnewaddress = 1;
                    $scope.noaddress = 1;
                }
            } else {
                //console.log("API Call failed");
            }
        });
    }
    if (authenticationService.userIn()) {
        $scope.loadAddress();
    }

    $scope.userloggedin = function() {
        if (authenticationService.userIn()) {
            return 1;
        }
        return 0;
    }


    $scope.selectAddress = function(address) {
        $scope.selectedaddress = address;
        angular.forEach($scope.addresses, function(addr) {
            addr.selected = 0;
            addr.class = "btn btn-address-default";
        });
        address.selected = 1;
        address.class = "btn btn-use-address";
    }

    $scope.invalidcoupon = 0;
    $scope.cdisc = 0;
    $scope.couponid = 0;
    $scope.getCouponDiscount = function() {
        console.log("Apply Coupon");
        ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', {method: 'getCouponDiscount', coupon: $scope.coupon})
            .then(function(response) {
                if (response.status == 9){
                    $scope.invalidcoupon = 1;
                    console.log("Invalid");
                }else {
                    var coupon = response.data.coupon;
                    if (coupon.ispercentage) {
                        $scope.cdisc = coupon.discount;
                        $scope.couponid = coupon.id;
                        console.log("coupon id: ", $scope.couponid);
                        $scope.total = $scope.total * (100-coupon.discount)/100
                    }
                }
            });
    };

    $scope.saveNewAddressAfterLogin = function() {
        ServerCallService.MakeServerCall('v2/Apiv2/User', 'POST', {
            method: 'addNewAddress',
            user_id:authenticationService.loggedUser().user_id,
            name: $scope.newaddress.name,
            email: $scope.newaddress.email,
            address_line_one: $scope.newaddress.address_line_one,
            address_line_two: $scope.newaddress.address_line_two,
            phonenumber: $scope.newaddress.phonenumber,
            pincode:$scope.newaddress.pincode,
            city:$scope.newaddress.city,
            state:$scope.newaddress.state,
            country:$scope.newaddress.country,
            //isdefault: address.isdefault ? 1 : 0,
            landmark: $scope.newaddress.landmark,
        }).then(function(response) {
            if (response.status == 1) {
                $scope.newaddress.addressid = response.data.address_id;
                //console.log($scope.newaddress.addressid);
                $scope.addnewaddress = 0;
                $scope.newaddress.selected = 1;
                $scope.newaddress.class = "btn btn-use-address";
                $scope.selectedaddress = $scope.newaddress;
                angular.forEach($scope.addresses, function(addr) {
                    addr.selected = 0;
                });
                $scope.addresses.push($scope.newaddress);
                $scope.newaddress = {};
                $scope.newaddrform.$setPristine();
                $scope.noaddress = 0;
                $scope.newaddress.name = $scope.selectedaddress.name;
                $scope.newaddress.email = $scope.selectedaddress.email;
                $scope.newaddress.phonenumber = $scope.selectedaddress.phonenumber;
            }
        });
    }

    $scope.saveNewAddress = function() {
        console.log("here");
        $scope.newaddress.country = "India";
        $scope.newaddress.state ="Karnataka";
        $scope.newaddress.city = "Bangalore";
        // Register the user if not already registered
        if ($scope.userloggedin() == 0) {
            ServerCallService.MakeServerCall('v2/Apiv2/Authentication', 'POST',
                { method: 'LoginOrRegister',
                    email: $scope.newaddress.email,
                    phonenumber: $scope.newaddress.phonenumber,
                    password: $scope.newaddress.phonenumber,
                    firstname: $scope.newaddress.name,
                    lastname: '',
                    picture: 'none',}
                ).then(function(response) {
                    if(response.status == 1) {
                        // Registered successfully, login here
                        authenticationService.loginUser({firstname:$scope.newaddress.name, mobile:$scope.newaddress.phonenumber, token: response.data.token, user_id: response.data.user_id});
                        // Frame the Cart
                        CartService.frameCart();
                        $scope.saveNewAddressAfterLogin();
                    } else if (response.status == 9) {
                        location.replace('https://www.refabd.com/login');
                    } else {
                        console.log("failed");
                    }
            });
        } else {
            $scope.saveNewAddressAfterLogin();
        }
    }
    $scope.banks = [];
    $scope.emiplans = [];
    $scope.populate_emi_details = function() {
        console.log("emi pop");
        var options = {
            'key': 'rzp_live_koMmDyJJsb3l71',
        };
        var rzp1 = new Razorpay(options);
        // Fetch the EMI plans
        rzp1.once('ready', function() {
        //console.log(rzp1.methods.emi_plans);
        var plans = rzp1.methods.emi_plans;        //console.log(rzp1.methods);
        $scope.emiplans = plans;
        //console.log(plans);
        var bank_codes = Object.getOwnPropertyNames(plans);

        //console.log(rzp1.methods.netbanking);
        angular.forEach(bank_codes, function(bcode) {
            var b = {};
            b.code = bcode;
            b.bank = rzp1.methods.netbanking[bcode];
            if (b.bank == undefined) {
                b.bank = b.code;
            }
            $scope.banks.push(b);
        });
            console.log($scope.banks);
        })
    };
    $scope.populate_emi_details();

    $scope.getEmiDetails = function(code) {
        $scope.sel_emi = $scope.emiplans[code];
        $scope.emi = [];
        var obj = $scope.sel_emi.plans;
        Object.keys(obj).forEach(function(p) {
            console.log(p, obj[p]);
            var o = {};
            o.month = p;
            o.interest = obj[p];
            o.installment = Razorpay.emi.calculator(Math.floor($scope.total * 100), p, obj[p])/100;
            $scope.emi.push(o);
        });
    }

$scope.orderplaced = false;
$scope.confirmOrder = function(paymentmethod) {
    // Make sure that address is selected
    if (jQuery.isEmptyObject($scope.selectedaddress) || $scope.selectedaddress.addressid == -1) {
        $scope.order = {};
        $scope.order.title = "Sorry!!";
        $scope.order.msg1 = "Please Select a Delivery Address";
        $scope.order.msg2 = "";
        $("#orderplacedtrigger").click();
        return;
    } else if ($scope.cart.length == 0) {
        $scope.order = {};
        $scope.order.title = "Sorry!!";
        $scope.order.msg1 = "Your Cart is empty";
        $scope.order.msg2 = "Please add items to the cart";
        $("#orderplacedtrigger").click();
        return;
    }
    var order = [];
    angular.forEach($scope.cart, function(item) {
        var order_item = {};
        order_item.id = item.productitem_id;
        order.push(order_item);
    });

    $scope.options = {
        'key': 'rzp_live_koMmDyJJsb3l71',
        //'key': 'rzp_test_OIocG7cUvwrV2z',
        'amount': Math.floor($scope.total * 100), // In paise
        'name': $scope.selectedaddress.name,
        'description': 'Refabd Purchase Payment',
        'image': 'assets/images/icons/refabd_payment_icon.jpg',
        'handler': function(transaction) {
            $scope.transactionHandler(transaction);
        },
        'prefill': {
            //'name': $scope.user.name,
            'email': $scope.selectedaddress.email,
            'contact': $scope.selectedaddress.phonenumber
        },
        'theme': {
            'color': "#47C9AF",
            'emi_mode': true
                //"color": "#d61b5e",
                //"image_padding": false,
        },
        'modal': {
            'ondismiss': function() {
            },
        },
    };

    if (paymentmethod == 0) {
        $scope.action = "Placing Order..";
        $scope.loading = 1;
        ServerCallService.MakeServerCall('v2/Apiv2/User', 'POST', {method:'orderProducts', address_id:$scope.selectedaddress.addressid, productitems:angular.toJson(order), buyfromcart: CartService.buyingfromcart, offer: $scope.offer, coupon: $scope.couponid})
        .then(function(op) {
            if (op.status == 1) {
                $scope.orderplaced = true;
                $scope.loading = 0;
                $scope.action = "Confirm Order";
                // Clear the Cart if the order was placed from the cart
                if (CartService.buyingfromcart) {
                    CartService.clearCart();
                }
                var OrderService = {};
                OrderService.address = $scope.selectedaddress;
                OrderService.id = op.data.order_id;
                OrderService.offer = $scope.offer;
                OrderService.itemcount = order.length;
                OrderService.amount = $scope.total;
                OrderService.items = $scope.cart;
                OrderService.orderactive = 1;
                OrderService.paymentmethod = "cod";
                OrderService.cdisc = $scope.cdisc;
                $window.localStorage.setItem("pocketin_success_order", angular.toJson(OrderService));
                location.replace('completeorder/success/'+op.data.order_id);
            } else {
                $scope.action = "Confirm Order";
                $scope.loading = 0;
            }
        });
    } else if(paymentmethod == 1) {
        var rzp1 = new Razorpay($scope.options);
        rzp1.open();
    } else if (paymentmethod == 2) {
        $scope.options.prefill.method = "emi";
        var rzp1 = new Razorpay($scope.options);
        rzp1.open();
    } else if (paymentmethod == 3) {
        console.log("zest");
        var zest = {}
        zest.email = $scope.selectedaddress.email
        zest.user_id = authenticationService.loggedUser().user_id;
        zest.total = Math.floor($scope.total);
        zest.name = authenticationService.loggedUser().firstname;
        zest.mobile = $scope.selectedaddress.phonenumber;
        $scope.action = "Placing Order..";
        $scope.loading = 1;
        ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', {method:'processZestPayment', address_id:$scope.selectedaddress.addressid, productitems:angular.toJson(order), buyfromcart: CartService.buyingfromcart, offer: $scope.offer, coupon: $scope.couponid, zest:angular.toJson(zest)})
        .then(function(op) {
            //$scope.loading = 0;
            location.replace(op.data.url);
        });
    }


    $scope.transactionHandler = function(transaction) {
        $("#processingpayment").click();
        $scope.action = "Placing Order..";
        $scope.loading = 1;
        console.log("in success hanlder");
        console.log(transaction.razorpay_payment_id);
        ServerCallService.MakeServerCall('v2/Apiv2/User', 'POST', {method:'CapturePaymentAndOrderProducts', paymentid: transaction.razorpay_payment_id, amount:$scope.total*100,  address_id:$scope.selectedaddress.addressid, productitems:angular.toJson(order), buyfromcart: CartService.buyingfromcart,offer: $scope.offer, coupon:$scope.coupon})
        .then(function(op) {
            if (op.status == 1) {
                console.log("payment success");
                console.log(op);
                $scope.orderplaced = true;
                $scope.loading = 0;
                $scope.action = "Confirm Order";
                // Clear the Cart if the order was placed from the cart
                if (CartService.buyingfromcart) {
                    CartService.clearCart();
                }
                $scope.order = {};
                $scope.order.title = "Thank You!";
                //$scope.order.msg1 = "Your order is placed successfully and will be delivered in couple of days.";
                //$scope.order.msg2 = "Please check the Order Page for Delivery Status.";
                var OrderService = {};
                OrderService.address = $scope.selectedaddress;
                OrderService.id = op.data.order_id;
                OrderService.offer = $scope.offer;
                OrderService.itemcount = order.length;
                OrderService.amount = $scope.total;
                OrderService.items = $scope.cart;
                OrderService.orderactive = 1;
                OrderService.paymentmethod = "online";
                OrderService.cdisc = $scope.cdisc;
                $window.localStorage.setItem("pocketin_success_order", angular.toJson(OrderService));
                $("#processingpaymentmodal").hide();
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
                location.replace('completeorder/success/'+op.data.order_id);
            } else {
                $scope.action = "Confirm Order";
                $scope.loading = 0;
            }
        });
    }
}
}]);

ctrlApp.controller('CompleteOrder', ['$window','$scope', function($window, $scope) {
        var OrderService = angular.fromJson($window.localStorage.getItem("pocketin_success_order")) || null;
        console.log(OrderService);
        if (OrderService != null) {
            $scope.address = OrderService.address;
            $scope.orderid = OrderService.id;
            $scope.itemcount = OrderService.itemcount
            $scope.amount = OrderService.amount
            $scope.items = OrderService.items;
            $scope.paymentmethod = OrderService.paymentmethod;
            $scope.offer = OrderService.offer;
            $scope.cdisc = OrderService.cdisc;
            $window.localStorage.setItem("pocketin_success_order", null);
        } else {
            location.replace('.');
        }
}]);

ctrlApp.controller('UserAccountController', ['$scope','$http','ServerCallService', 'authenticationService', function($scope,$http,ServerCallService, authenticationService) {
    $scope.EditSaveUserClass="btn btn-xs btn-edit";
    $scope.EditSaveUser = "Edit";

    $scope.EditSaveUserLastNameClass = "btn btn-xs btn-edit";
    $scope.EditSaveUserLastName = "Edit";

    $scope.EditSavePhoneClass = "btn btn-xs btn-edit";
    $scope.EditSavePhone = "Edit";

    $scope.EditSaveEmailClass = "btn btn-xs btn-edit";
    $scope.EditSaveEmail = "Edit";

    $scope.EditSavePasswordClass = "btn btn-xs btn-edit";
    $scope.EditSavePassword = "Change";

    $scope.userReadOnlyText = true;
    $scope.userReadOnlyTextLastName = true;
    $scope.phoneNumberReadOnly = true;
    $scope.emailReadOnly = true;
    $scope.passwordReadOnly = true;

    $scope.EnableVerify = false;

    $scope.sendotpfails = true;
    $scope.emailexistserror = false;

    ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', { method: 'getUserDetail',})
        .then(function(response) {
            if(response.status == 1) {
                //console.log($scope.pagetype);
                if ($scope.pagetype == 1) {
                     $("#order").click();
                }else if ($scope.pagetype == 2) {
                     $("#wishlst").click();
                }else if ($scope.pagetype == 3) {
                    $("#address").click();
                }else if ($scope.pagetype == 4) {
                    $("#nots").click();
                }else if ($scope.pagetype == 5) {
                    $("#myselltrigger").click();
                }else if ($scope.pagetype == 6) {
                    $("#myreferraltrigger").click();
                }else if ($scope.pagetype == 7) {
                    $("#servicerequest").click();
                }
                $scope.updatedUserName = response.data.firstname;
                $scope.updatedUserNameLast = response.data.lastname;
                $scope.phonenumber = parseInt(response.data.phonenumber);
                $scope.email = response.data.email;
                $scope.origemail = $scope.email;
                $scope.Verified = response.data.isverified;
            } else {
                //console.log("API Call failed");
            }
        });

    $scope.resetuserdetails = function()
    {
        ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', { method: 'getUserDetail',})
            .then(function(response){
                if(response.status == 1) {
                    $scope.updatedUserName = response.data.firstname;
                    $scope.phonenumber = parseInt(response.data.phonenumber);
                    $scope.email = response.data.email;
                    $scope.Verified = response.data.isverified;
                } else {
                    //console.log("API Call failed");
                }
            }
            );
    }

    $scope.editSaveUserName = function(fun)
    {
        if(fun == "Edit")
        {
            $scope.userReadOnlyText=false;
            $scope.EditSaveUser="Save";
            $scope.EditSaveUserClass="btn btn-xs btn-save";
            $('#fname').focus();
            $scope.activeorpassive1 = "active";
        } else {
            $scope.activeorpassive1 = "";
            ServerCallService.MakeServerCall('v2/Apiv2/User', 'POST', { method: 'updateUserName', firstname: $scope.updatedUserName,})
                .then(function(response) {
                    if(response.status == 1) {
                        $scope.userReadOnlyText=true;
                        $scope.EditSaveUser="Edit";
                        $scope.EditSaveUserClass="btn btn-xs btn-edit";
                        authenticationService.loginUser({firstname:$scope.updatedUserName, mobile:authenticationService.loggedUser().mobile, token: authenticationService.loggedUser().token, user_id: authenticationService.loggedUser().user_id});
                    } else {
                        //console.log("API Call failed");
                    }
                });

        }
    }
    $scope.editSaveUserLastName = function()
    {
        if($scope.EditSaveUserLastName == "Edit")
        {
            $scope.userReadOnlyTextLastName = false;
            $scope.EditSaveUserLastName="Save";
            $scope.EditSaveUserLastNameClass="btn btn-xs btn-save";
            $('#lname').focus();
            $scope.activeorpassive2 = "active";

        } else if($scope.EditSaveUserLastName == "Save")
        {
            $scope.activeorpassive2 = "";
            ServerCallService.MakeServerCall('v2/Apiv2/User', 'POST', {method: 'updateUserNameLast', lastname: $scope.updatedUserNameLast})
                .then(function(response) {
                    if(response.status == 1) {
                        $scope.userReadOnlyTextLastName = true;
                        $scope.EditSaveUserLastName="Edit";
                        $scope.EditSaveUserLastNameClass="btn btn-xs btn-edit";
                  } else {
                        //console.log("API Call failed");
                    }
                });
        }
    }

    $scope.editSavePhoneNumber = function()
    {
        if($scope.EditSavePhone == "Edit")
        {
            $scope.msg = "";
            $scope.Verified=false;
            $scope.phoneNumberReadOnly=false;
            $scope.EditSavePhone="Send OTP";
            $scope.EditSavePhoneClass="btn btn-xs btn-save";
            $('#phone').focus();
            $scope.activeorpassive3 = "active";
        }

        else if($scope.EditSavePhone == "Send OTP")
        {
            $scope.EnableVerify=true;
            $scope.sendotpfails=false;
            $scope.verifyotpfails=false;
            $('#phoneotp').focus();
            ServerCallService.MakeServerCall('v2/Apiv2/Authentication', 'POST',{ method: 'sendotp', user_id: authenticationService.loggedUser().user_id, phonenumber: $scope.phonenumber, 'to_non_user': '1'})
                .then(function(response) {
                    //console.log(response);
                    if(response.status == 1) {
                        // If otp sent, change button to verify, show text box to enter otp and make phonenumber readonly
                        $scope.EditSavePhone="Verify";
                        //$scope.phoneNumberReadOnly=true;
                        $scope.EnableVerify=true;
                    } else {
                        // If otp fails, show otp error msg to retry
                        $scope.sendotpfails=true;
                        $scope.EnableVerify=false;
                        $scope.msg = response.data.message;
                        $scope.EditSavePhone="Edit";
                        $scope.EditSavePhoneClass="btn btn-xs btn-edit";
                        $scope.EnableVerify=false;
                        $scope.phoneNumberReadOnly=true;
                        $scope.phonenumber = authenticationService.loggedUser().mobile;
                        $scope.PhoneOTP = "";
                    }
                });
        }
        else if($scope.EditSavePhone == "Verify")
        {
            $scope.activeorpassive3 = "";
            ServerCallService.MakeServerCall('v2/Apiv2/Authentication', 'POST', { method: 'verifyAndUpdatePhoneNumber', phonenumber: $scope.phonenumber, otp: $scope.PhoneOTP,})
                .then(function(response) {
                    if(response.status == 1) {
                        $scope.EditSavePhone="Edit";
                        $scope.EditSavePhoneClass="btn btn-xs btn-edit";
                        $scope.EnableVerify=false;
                        $scope.phoneNumberReadOnly=true;
                        $scope.PhoneOTP = "";
                        $scope.msg = "";
                        authenticationService.loginUser({firstname:authenticationService.loggedUser().firstname, mobile:$scope.phonenumber, token: authenticationService.loggedUser().token, user_id: authenticationService.loggedUser().user_id});
                } else {
                        $scope.msg = "Failed to Verify OTP. Please enter again";
                        //console.log("API Call failed");
                    }
                });
        }
    }

    $scope.editSaveEmailAddr= function()
    {
        if($scope.EditSaveEmail == "Edit")
        {

            $scope.emailexistserror=false;
            $scope.emailReadOnly=false;
            $scope.EditSaveEmail="Save";
            $scope.EditSaveEmailClass="btn btn-xs btn-save";
            $scope.msgemail = "";
            $('#useremail').focus();
            $scope.activeorpassive4 = "active";

        } else if($scope.EditSaveEmail == "Save")
        {
            $scope.activeorpassive4 = "";
            $scope.emailexistserror=false;
            ServerCallService.MakeServerCall('v2/Apiv2/User', 'POST',
                    { method: 'updateEmail',
                        email: $scope.email,
                    }
                    )
                .then(function(response) {
                    if(response.status == 1) {
                        //console.log(response);
                        $scope.emailReadOnly=true;
                        $scope.EditSaveEmail="Edit";
                        $scope.EditSaveEmailClass="btn btn-xs btn-edit";
                        $scope.origemail = $scope.email;
                    } else {
                        //console.log("API Call failed");
                        $scope.msgemail = response.data.message;
                        $scope.EditSaveEmail="Edit";
                        $scope.EditSaveEmailClass="btn btn-xs btn-edit";
                        $scope.emailReadOnly=true;
                        $scope.email = $scope.origemail;
                    }
                });
        }
    }

    $scope.editSavePassword = function()
    {
        if($scope.EditSavePassword == "Change")
        {
            $('#pswd').focus();
            $('#pswd').attr("placeholder", "");
            $scope.activeorpassive5 = "active";
            $scope.passwordReadOnly = false;
            $scope.EditSavePasswordClass = "btn btn-xs btn-save";
            $scope.EditSavePassword = "Save";
        }
        else if($scope.EditSavePassword == "Save")
        {
               $scope.activeorpassive5 = "";
                ServerCallService.MakeServerCall('v2/Apiv2/User', 'POST',
                    { method: 'updatePassword', password: $scope.newpassword,})
                    .then(function(response) {
                        if(response.status == 1) {
                            $scope.newpassword = "";
                            $scope.EditSavePassword="Change";
                            $scope.EditSavePasswordClass="btn btn-xs btn-edit";
                            $scope.passwordReadOnly=true;
                            $('#pswd').attr("placeholder", "********");
                            $('#pswd').blur();
                        } else {
                            //console.log("API Call failed");
                            $scope.msgpwd = response.data.message;
                            $scope.EditSavePassword="Change";
                            $scope.EditSavePasswordClass="btn btn-xs btn-edit";
                            $scope.passwordReadOnly=true;
                        }
                    });
        }
    }

    $scope.cancelOrderItem = function(order, item) {
        ServerCallService.MakeServerCall('v2/Apiv2/User', 'POST', {method: 'cancelOrder', order_id: order, 'item_id': item.id})
        .then(function(op) {
            //console.log("done");
            item.iscancelled = true;
            item.status = "Cancelled";
            item.date = op.data.date;
        })
    }

}]);
ctrlApp.controller('UserAddressController', ['$scope', '$routeParams', '$http', 'ServerCallService', function($scope, $routeParams, $http, ServerCallService) {
    $scope.showeditaddr = false;
    $scope.shownewaddr = false;
    ////console.log($routeParams.item);
    ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', {
            method: 'getSavedAddresses',
        })
        .then(function(response) {
            if (response.status == 1) {
                $scope.items = response.data.address;
                $scope.user = response.data.user;
                //console.log(response.data);
            } else {
                //console.log("API Call failed");
            }
        });

    $scope.editaddress = function(id) {
        $scope.showeditaddr = true;
        $scope.shownewaddr = false;
        $scope.selectedaddress = {};

        angular.forEach($scope.items, function(add) {
            if (add.addressid == id) {
                $scope.selectedaddress  = add;
                return;
            }
        });
        //console.log($scope.selectedaddress);
    }

    $scope.newAddress = function() {
        $scope.shownewaddr = true;
        $scope.showeditaddr = false;
        $scope.newaddress = {};
    }

    $scope.updateaddress = function() {
        ServerCallService.MakeServerCall('v2/Apiv2/User', 'POST', {
            //name', 'email', 'phonenumber', 'address_line_one', 'address_line_two', 'pincode', 'landmark'));
                method: 'updateSavedAddress',
                id: $scope.selectedaddress.addressid,
                name:$scope.selectedaddress.name,
                email:$scope.selectedaddress.email,
                phonenumber:$scope.selectedaddress.phonenumber,
                address_line_one: $scope.selectedaddress.address_line_one,
                address_line_two: $scope.selectedaddress.address_line_two,
                pincode: $scope.selectedaddress.pincode,
                landmark: $scope.selectedaddress.landmark,
            })
            .then(function(response) {
                if (response.status == 1) {
                    //console.log(response);
                    $scope.showeditaddr = false;
                } else {
                    //console.log("API Call failed");
                }
            });
    }

    $scope.saveNewAddress = function() {
        $scope.newaddress.country = "India";
        $scope.newaddress.state ="Karnataka";
        $scope.newaddress.city = "Bangalore";
        ServerCallService.MakeServerCall('v2/Apiv2/User', 'POST', {
            method: 'addNewAddress',
            name: $scope.newaddress.name,
            email: $scope.newaddress.email,
            address_line_one: $scope.newaddress.address_line_one,
            address_line_two: $scope.newaddress.address_line_two,
            phonenumber: $scope.newaddress.phonenumber,
            pincode:$scope.newaddress.pincode,
            city:$scope.newaddress.city,
            state:$scope.newaddress.state,
            country:$scope.newaddress.country,
            //isdefault: address.isdefault ? 1 : 0,
            landmark: $scope.newaddress.landmark,
        }).then(function(response) {
            if (response.status == 1) {
                //console.log("done");
                $scope.newaddress.addressid = response.data.address_id;
                $scope.items.push($scope.newaddress);
                $scope.newaddress = {};
                $scope.shownewaddr = false;
                $scope.newaddrform.$setPristine();
            }
        });
    }

    $scope.hideeditaddress = function() {
        $scope.showeditaddr = false;
    }

    $scope.hidesaveaddress = function() {
        $scope.shownewaddr = false;
    }


    $scope.removeaddress = function(id) {
        ServerCallService.MakeServerCall('v2/Apiv2/User', 'POST', { method: 'removesavedaddress', id: id,})
            .then(function(response) {
                //console.log(response);
                if (response.status == 1) {
                    // Remove address from the list
                    $scope.items = $scope.items.filter(function(addr) {
                        return addr.addressid != id;
                    });
                    $scope.showeditaddr = false;
                } else {
                    //console.log("API Call failed");
                }
            });
    }
}]);


ctrlApp.controller('UserOrderController', ['$scope', 'ServerCallService', function($scope, ServerCallService) {
    // Load the orders
    ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', {method: 'getUserOrders'}).
    then(function(op) {
        if (op.status == 1) {
            $scope.orders = op.data.orders;
        } else {
            //console.log("API call failed");
        }
    });
}]);

ctrlApp.controller('OrderDetailsController', ['$scope', 'ServerCallService', function($scope, ServerCallService) {
    // Load the order details
    $scope.loadOrderDetails = function(orderid) {
        $scope.order_id = orderid;
        ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', {method: 'getOrderDetails', 'order_id': $scope.order_id}).
        then(function(op) {
            if (op.status == 1) {
                $scope.order = op.data.order;
            } else {
            }
        });
    }
}]);

ctrlApp.controller('UserNotificationController', ['ServerCallService', '$scope', function(ServerCallService, $scope) {
    ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', {method: 'getUserNotifications', unseen: '0'})
            .then(function(op) {
                if (op.status == 1) {
                    $scope.nots = op.data.nots;
                }
            });
}]);

ctrlApp.controller('ComplaintsController', ['ServerCallService', '$scope', function(ServerCallService, $scope) {
    $scope.newcomplaint = {}
    $scope.ticketcreated = 0;
    $scope.submitmsg = "Submit";
    $scope.getActiveComplaints = function() {
        ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', {method: 'getComplaints', active: '1'})
                .then(function(op) {
                    if (op.status == 1) {
                        $scope.nots = op.data.nots;
                    }
        });
    }

    $scope.getAllComplaints = function() {
        ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', {method: 'getComplaints', active: '0'})
                .then(function(op) {
                    if (op.status == 1) {
                        $scope.nots = op.data.nots;
                    }
        });
    }

    $scope.createTicket = function() {
        $scope.invalidorder = 0;
        ServerCallService.MakeServerCall('v2/Apiv2/User', 'POST',
            { method: 'createTicket',
              subject: $scope.newcomplaint.subject,
              message: $scope.newcomplaint.message,
              order_id: $scope.newcomplaint.order_id})
                .then(function(op) {
                    if (op.status == 1) {
                        $scope.ticketcreated = 1;
                        $scope.submitmsg = "Ticket Created";
                    } else if (op.status == 0) {
                        $scope.invalidorder = 1;
                    }
        });
    }
}]);


ctrlApp.controller('SellController', ['ServerCallService', '$scope', '$timeout', '$window', 'authenticationService', 'flowFactory',  function(ServerCallService, $scope, $timeout, $window, authenticationService, flowFactory) {
    // Create flow object
    if (!authenticationService.userIn()) {
        $('#regmodalclose').hide();
        $('#signupform').modal({backdrop: 'static', keyboard: false, show: true});
            //location.replace('login');
            //return;
    }
    $scope.sellFlowObj = flowFactory.create({
         target: 'uploadsellimage'
    });
    ctrl = this;
    ctrl.pid = undefined;
    $scope.product = {};
    $scope.selling = 0;
    $scope.sold = 0;
    $scope.product.subcat = 'fridge';
    $scope.setSubCategory = function() {
        if ($scope.product.maincat == 'appliances') {
            $scope.product.subcat = 'fridge';
        } else {
            $scope.product.subcat = 'bed';
        }
    }

    $scope.sellProduct = function() {
        $scope.selling = 1;
        $scope.product.sellid = ctrl.pid;
        $scope.product.user_id = authenticationService.loggedUser().user_id;
        ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', {method: 'sellProduct', product: angular.toJson($scope.product)})
            .then(function(op) {
                if (op.status == 1) {
                    $scope.selling = 0;
                    $scope.sellFlowObj.cancel();
                    ctrl.pid = undefined;
                    $scope.sold = 1;
                    $scope.sellform.$setPristine();
                    $scope.product = {};
                    $scope.product.subcat = 'fridge';
                    $scope.product.maincat = 'appliances';
                    $scope.product.brand = 'lg';
                    $timeout(function() {$scope.sold = 0;}, 2000);
                    $scope.sellid = op.data.sellid;
                    $("#sellsuccesstrigger").click();
                } else {
                    console.log("failed");

                }
        });
    }

    $scope.initupload = function($file) {
        if(!{png:1,gif:1,jpg:1,jpeg:1}[$file.getExtension()]) {
            $window.alert("Not a valid file. only png, jpg, gif and jpeg");
            return false;
        }
        if ($file.file.size >= 2097152) {
            $scope.sell = {};
            $scope.sell.title = "Failed to Upload Image";
            $scope.sell.msg1 = "Image size is larger than 2MB. Please reduce the size and upload.";
            $scope.sell.msg2 = "WhatsApp the images at 70-226-30-270 or email us at care@pocketin.in";
            $("#selltrigger").click();
            //$window.alert("File size is larger than 2MB. Please reduce the image size or WhatsApp the pictures to 70-226-30-270");
            return false;
        }
    }

    $scope.removeimage = function(file) {
        file.removefailed = 0;
        var name = file.name;
        ServerCallService.MakeServerCall("removesellimage", "GET", {
                name: name,
                pid: ctrl.pid
            })
            .then(function(response) {
                console.log(response);
                if (response.status == 1) {
                    file.cancel();
                } else {
                    console.log("failed to remove the image");
                    file.removefailed = 1;
                }

            });
    }
    $scope.imageadded = false;
    $scope.completedupload = false;
    $scope.uploadfailed = false;

    $scope.uploadimage = function($flow) {
        if (!authenticationService.userIn()) {
            location.replace('login');
            return;
        }
        $scope.completedupload = false;
        if (ctrl.pid == undefined) {
            ServerCallService.MakeServerCall("initiatesellproduct", "GET", {user_id: authenticationService.loggedUser().user_id})
            .then(function(response) {
                if (response.status == 1) {
                    ctrl.pid = response.data.pid;
                    $flow.opts.query = {
                        pid: ctrl.pid
                    };
                    $flow.upload();
                    $scope.imageadded = true;
                } else {
                    $window.alert("Failed to upload the image. Please refresh the page and try again!!");
                }
            });
        } else {
            $flow.opts.query = {
                pid: ctrl.pid
            };
            $flow.upload();
            $scope.imageadded = true;
        }
    }

    $scope.uploadfailed = function($file) {
        $scope.uploadfailed = true;
    }

    $scope.uploadcomplete = function() {
        $scope.completedupload = true;
    }
}]);


ctrlApp.controller('errorController', ['$scope', '$window', '$sce', function($scope, $window, $sce) {
    var status = undefined;
    //console.log($window.sessionStorage['error']);
    if ($window.sessionStorage['error'] != "null") {
       status = angular.fromJson($window.sessionStorage['error']);
       console.log(status);
    }

    if (status == undefined) {
        location.replace('');
    } else {
        $scope.status = status.statusText;
        $scope.servererror = status.data;
        if (status.config) {
           $scope.debugtext = status.config.params;
        } else {
            $scope.debugtext = "";
        }
    }

    $window.sessionStorage['error'] = "null";

    $scope.trustAsHtml = function(string) {
        return $sce.trustAsHtml(string);
    };

}]);

ctrlApp.controller('homeController', ['$scope', '$window', 'authenticationService', '$rootScope','$location', function($scope, $window, authenticationService, $rootScope, $location) {
    $rootScope.homepage = 1;
    $window.sessionStorage['error'] = "null";
    // Show the welcome signup modal if user is not logged in
    /*if ($window.sessionStorage['welcomed'] == undefined) {
        $window.sessionStorage['welcomed'] = 'no';
    }*/

   /* angular.element($window).bind('resize', function () {
        $rootScope.ismobile = $window.innerWidth <= 400;
    });*/

   //$rootScope.ismobile =  $window.innerWidth <= 430;
   if( $window.navigator.userAgent.match(/Android/i)
         || $window.navigator.userAgent.match(/webOS/i)
         || $window.navigator.userAgent.match(/iPhone/i)
         || $window.navigator.userAgent.match(/iPad/i)
         || $window.navigator.userAgent.match(/iPod/i)
         || $window.navigator.userAgent.match(/BlackBerry/i)
         || $window.navigator.userAgent.match(/Windows Phone/i) ){
        $rootScope.ismobile = true;
    } else {
        $rootScope.ismobile = false;
    }

    if (!authenticationService.userIn()/* && $window.sessionStorage['welcomed'] == 'no'*/){
            //$('#signupform').modal({backdrop: 'static', keyboard: false, show: true});
    }
}]);
