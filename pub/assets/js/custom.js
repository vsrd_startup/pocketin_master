$(function(){
        // Check the initial Poistion of the Sticky Header
        var menutop = $('.main_menu').offset().top;
        var mobmenutop = $('.addboxinmobile').offset().top;

        $(window).scroll(function(){
                if($(window).scrollTop() > menutop ) {
                        $('.main_menu').css({position: 'fixed', top: '0px', background:'#69b4df', width:'100%'});
                        $(".main_menu a").hover(
                        	function () { $(this).css({"border-bottom-color":"#fff"});},
                        	function () { $(this).css({"border-bottom-color":"transparent"});
                        });

                } else {
                        $('.main_menu').css({position: 'static', top: '0px', background:'transparent'});
                         $(".main_menu a").hover(
                        	function () { $(this).css({"border-bottom-color":"#69b4df"});},
                        	function () { $(this).css({"border-bottom-color":"transparent"});
                        });
                }
                if($(window).scrollTop() > mobmenutop ) {
                    if($(window).width() < 500) {
                        $('.addboxinmobile').css({position: 'fixed', top: '0px',background: '#fff', width:'100%'});
                    }
                } else {
                    if($(window).width() < 500) {
                        $('.addboxinmobile').css({position: 'static', top: '0px', width:'inherit'});
                    }
                }
        });
});
