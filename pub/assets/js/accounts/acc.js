var piacc = angular.module('piacc', []);

piacc.service("authenticationService", ['$window', function($window) {
	this.GUESTAPIKEY = "pocketin_guest_api_key";
	$window.sessionStorage["pocketin_admin_info"] = null;
	this.loggeduser = angular.fromJson($window.sessionStorage["pocketin_admin_info"]) || null;

	this.loginUser = function(user) {
		this.loggeduser = user;
		//$cookieStore.put('loggeduser', this.loggeduser);
		$window.sessionStorage["pocketin_admin_info"] = angular.toJson(user);
	}

	this.logoutuser = function() {
		console.log("Logout");
		this.loggeduser = null;
		$window.sessionStorage["pocketin_admin_info"] = null;
	}

	this.getApiKey = function() {
		if (this.loggeduser == null) {
			return this.GUESTAPIKEY;
		}
		return this.loggeduser.token;
	}

	this.userLogged = function() {
		if (this.loggeduser == null) {
			return 0;
		}
		if ($window.sessionStorage["pocketin_admin_info"] == null) {
			return 0;
		}

		return 1;
	}
}]);

piacc.service("ServerCallService", ['authenticationService', '$http', '$q', '$timeout', '$rootScope',  function(authenticationService, $http, $q, $timeout, $rootScope) {
	this.MakeServerCall = function(url, method, param) {
		$rootScope.xhr = 1;
		// Add the API key of the logged in User
		param.apikey = authenticationService.getApiKey();
		var deferred = $q.defer();
		$http({
			url: url,
			method: method,
			params: param,
		}).then(function(response) {
			var status = 1;
			if (response.data.list && response.data.list.status != undefined) {
				status = response.data.list.status;
			}
			var op = {
				status: status,
				data: response.data.list
			};
			$rootScope.xhr = 0;
			//$timeout(function(){
			//    deferred.resolve(op);
			//},2000);
			deferred.resolve(op);
		}, function(response) {
			var op = {
				status: -1,
				data: undefined
			}
			$rootScope.xhr = 0;
			deferred.resolve(op);
		});
		return deferred.promise;
		//return $q.when("Hello World!");
	}
}]);

piacc.controller('AccountsController', function($scope, ServerCallService, authenticationService, $timeout) {
	console.log("here");
	$scope.superuser = 0;
	$scope.loggedin = false;
	ServerCallService.MakeServerCall('v2/Apiv2/Authentication', 'GET', {method: 'whoami', role: 'admin',})
		.then(function(op) {
			op = op.data;
			if (op.status == 1) {
				authenticationService.loginUser({
					name: op.firstname + " " + op.lastname,
					firstname: op.firstname,
					mobile: op.mobile,
					token: op.token,
					user_id: op.user_id
				});
				$scope.name = authenticationService.loggeduser.firstname;
				$scope.loggedin = true;
				if (authenticationService.loggeduser.firstname == "Admin") {
					$scope.superuser = 1;
				}
			} else {
				authenticationService.logoutuser();
			}
		});


	$scope.Login = function() {
		ServerCallService.MakeServerCall('v2/Apiv2/Authentication', 'POST', {
				method: 'login',
				isadmin: 1,
				username: $scope.username,
				password: $scope.password})
			.then(function(response) {
				if (response.status == 1) {
					authenticationService.loginUser({
						firstname: response.data.profile.firstname,
						mobile: response.data.profile.phonenumber,
						token: response.data.token,
						user_id: response.data.profile.user_id
					});
					$scope.name = authenticationService.loggeduser.firstname;
					$scope.loggedin = true;
				} else {
					$scope.error = response.data.message;
				}
			});
	}

	$scope.logout = function() {
		ServerCallService.MakeServerCall('logout', 'GET', {})
        .then(function(op) {
            authenticationService.logoutuser();
			$scope.loggedin = false;
        });
	}

	$scope.pr = {};
	$scope.pr.date = new Date();
	$scope.addProcurement = function() {
		$scope.newprocurement = 0;
		$scope.pr.disablebutton = 1;
		ServerCallService.MakeServerCall('v2/Apiv2/User', 'POST',
			{method: 'addProcurementDetails',
			input: angular.toJson($scope.pr)}).
		then(function(op) {
			if (op.status == 1) {
				$scope.pr = {};
				$scope.newprocurement = 1;
				$timeout(function() {$scope.newprocurement = 0;}, 2000);
				$scope.pr.date = new Date();
			    $scope.p.$setPristine();
			    $scope.pr.disablebutton = 0;
			}
		});
	}

	$scope.getProcurementDetails = function() {
    	console.log("procurementsss");
    	$scope.newprocurement = 0;
        ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', {method: 'getProcurementDetails'})
			.then(function(response) {
				console.log("procurements");
				if (response.data.length == 0) {
					$scope.procurements = undefined;
				} else {
					$scope.procurements = response.data;
					console.log($scope.procurements);
					console.log("procurements");
				}
			});
	};

	$scope.exp = [];
	$scope.ex = {};
	$scope.ex.date = new Date();
	
	$scope.addExpense = function() {
		console.log ("Expense");
		$scope.newexpense = 0;
		$scope.ex.disablebutton = 1;
		ServerCallService.MakeServerCall('v2/Apiv2/User', 'POST',
			{method: 'addExpenseDetails',
			input: angular.toJson($scope.ex)}).
		then(function(op) {
			if (op.status == 1) {
				$scope.exp.push($scope.ex);
				$scope.ex = {};
				$scope.newexpense = 1;
				$timeout(function() {$scope.newexpense = 0;}, 2000);
				$scope.ex.date = new Date();
			    $scope.expense.$setPristine();
			    $scope.ex.disablebutton = 0;
			}
		});
	}


		$scope.addVendor = function() {
		$scope.newvendor = 0;
		ServerCallService.MakeServerCall('v2/Apiv2/User', 'POST',
			{method: 'addVendorDetails',
			input: angular.toJson($scope.vr)}).
		then(function(op) {
			if (op.status == 1) {
				$scope.vr = {};
				$scope.vendor.$setPristine();
				$scope.newvendor = 1;
				$timeout(function() {$scope.newvendor = 0;}, 2000);
			}
		});
	}

	$scope.getVendorDetails = function() {
		console.log("View vendors");
        ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', {method: 'getVendorDetails'})
			.then(function(response) {
				if (response.data.length == 0) {
					$scope.vendors = undefined;
				} else {
					$scope.vendors = response.data;
				}
			});
	};
	$scope.getVendorDetails();



	$scope.getExpenseDetails = function() {
		console.log("View expenses");
        ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', {method: 'getExpenseDetails'})
			.then(function(response) {
				if (response.data.length == 0) {
					$scope.expenses = undefined;
				} else {
					$scope.expenses = response.data;
				}
			});
	};

	
	$scope.emp = {};
	$scope.emp.join_date = new Date();
	$scope.addEmployee = function() {
		console.log("emp");
		console.log($scope.emp);
		$scope.newemployee = 0;
		$scope.emp.disablebutton = 1;
		ServerCallService.MakeServerCall('v2/Apiv2/User', 'POST',
			{method: 'addEmployeeDetails',
			input: angular.toJson($scope.emp)}).
		then(function(op) {
			if (op.status == 1) {
				$scope.emp = {};
				$scope.newemployee = 1;
				$timeout(function() {$scope.newemployee = 0;}, 2000);
			    $scope.addemp.$setPristine();
			    $scope.emp.disablebutton = 0;
			}
		});
	
	}

$scope.getEmployeeDetails = function() {
		console.log("Emp details");
        ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', {method: 'getEmployeeDetails'})
			.then(function(response) {
				if (response.data.length == 0) {
					$scope.employees = undefined;
				} else {
					$scope.employees = response.data;
				}
				console.log($scope.employees);
			});
	};

	$scope.att = {};
	$scope.att.date = new Date();
	$scope.addLeave = function() {
		console.log("Leave");
		$scope.newleave = 0;
		$scope.att.disablebutton = 1;
		ServerCallService.MakeServerCall('v2/Apiv2/User', 'POST',
			{method: 'addLeave',
			input: angular.toJson($scope.att)}).
		then(function(op) {
			if (op.status == 1) {
				$scope.att = {};
				$scope.newleave = 1;
				$timeout(function() {$scope.newleave = 0;}, 2000);
			    $scope.leave.$setPristine();
			    $scope.att.disablebutton = 0;
			}
		});
	}
	
	$scope.search_date = new Date().toISOString().slice(0, 10);
	$scope.getLeaveDetails = function() {
		console.log("Leave details");
        ServerCallService.MakeServerCall('v2/Apiv2/User', 'GET', {method: 'getLeaveDetails'})
			.then(function(response) {
				if (response.data.length == 0) {
					$scope.leaves = undefined;
				} else {
					$scope.leaves = response.data;
					console.log($scope.leaves);
				}
			});
	};


});



piacc.directive('ngConfirmClick', [
  function(){
    return {
      priority: -1,
      restrict: 'A',
      link: function(scope, element, attrs){
        element.bind('click', function(e){
          var message = attrs.ngConfirmClick || "Are you Sure?";
          if(message && !confirm(message)){
            e.stopImmediatePropagation();
            e.preventDefault();
          }
        });
      }
    }
  }
]);

piacc.filter('todate', function() {
    return function(input) {
        input = new Date(input);
        // Convert to IST
        input.setTime(input.getTime() + (330 * 60 * 1000));
        input = input.toISOString();
        return input;
    };
});

piacc.filter('newlines', function() {
  return function(text) {
    if (text == undefined) {
        return "";
    }
    return text.split(/\n/g);
  };
});


// export to csv
piacc.directive('exportToCsv',function(){
return {
    restrict: 'A',
    link: function (scope, element, attrs) {
        var el = element[0];
        element.bind('click', function(e) {
            var table = e.target.nextElementSibling;
            var csvString = '';
            for(var i=0; i<table.rows.length;i++){
                var rowData = table.rows[i].cells;
                for(var j=0; j<rowData.length;j++){
                	var d = String(rowData[j].innerHTML).replace(/<[^>]+>/gm, '');
                	d = d.replace(/,/g , " ");
                	d = '"' + d + '"';
                	//d = d.replace(/\n/g , "");
                    csvString = csvString + d + ",";
                }
                csvString = csvString.substring(0,csvString.length - 1);
                csvString = csvString + "\n";
            }
            csvString = csvString.substring(0, csvString.length - 1);
            var a = $('<a/>', {
                style:'display:none',
                href:'data:application/octet-stream;base64,'+btoa(csvString),
                download:'account.csv'
            }).appendTo('body')
            a[0].click()
            a.remove();
        });
    }
}
});
